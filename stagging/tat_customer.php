<?php

  ini_set('max_execution_time', 0);
  ini_set('memory_limit', -1);
  date_default_timezone_set('Asia/Jakarta');

  // DB_CRM_INFO connection
  $host1 = '192.168.240.101';
  // $host1 = 'DESKTOP-HRV29SG\SQLEXPRESS';
  $opsi = array('UID' => 'usr-crm', 'PWD' => 'p@ssw0rd', 'database' => 'db_crm_info');
  // $opsi = array('UID' => 'aengganteng', 'PWD' => 'aengganteng', 'database' => 'CRM');
  $conn1 = sqlsrv_connect($host1, $opsi);
  if(!$conn1){
    echo "Failed Connect DB_CRM_INFO \n";
    exit;
  } else {
    echo "Succeed Connect DB_CRM_INFO \n";
  }

  // DB_DSS connection
  // $host2 = '192.168.240.107'; //dev
  $host2 = '172.16.100.35'; // prod
  // $host2 = 'DESKTOP-HRV29SG\SQLEXPRESS'; // prod
  $opsi2 = array('UID' => 'usr_dss', 'PWD' => 'p@ssw0rd', 'database' => 'db_dss');
  // $opsi2 = array('UID' => 'aengganteng', 'PWD' => 'aengganteng', 'database' => 'db_dss');
  $conn2 = sqlsrv_connect($host2, $opsi2);
  if(!$conn2){
  	echo "Failed Connect DB_DSS \n"; exit;
  } else {
  	echo "Succeed Connect DB_DSS \n";
  }

  $date1 = date("Y-m-d", strtotime("-1 days"));
  $date2 = date("Y-m-d H:i:s");

  $customer_crm = "SELECT ID_CUSTOMER, COMPANY_NAME from V_Customer";
  $exec_customer_crm = sqlsrv_query($conn1, $customer_crm);
  $data_cust_crm = array();
  $id_cust_crm = array();
  while ($arr = sqlsrv_fetch_array($exec_customer_crm)) {
    $data_cust_crm[] = $arr;
    $id_cust_crm[] = $arr["ID_CUSTOMER"];
  }

  $check_cust_dss = "SELECT DISTINCT(TAT_C_CODE) AS ID_CUSTOMER FROM TAT_CUSTOMER";
  $exec_check_cust_dss = sqlsrv_query($conn2, $check_cust_dss);
  $id_cust_dss = array();
  while ($arr = sqlsrv_fetch_array($exec_check_cust_dss)) {
    $id_cust_dss[] = $arr["ID_CUSTOMER"];
  }


  $insert = $update = 0;

  for ($i=0; $i < count($id_cust_crm); $i++) {
    if(in_array($id_cust_crm[$i], $id_cust_dss)){
      // UPDATE
      $sql_update = "UPDATE TAT_CUSTOMER SET TAT_C_NAME = '{$data_cust_crm[$i]["COMPANY_NAME"]}', TAT_C_UPDATE = '{$date2}' WHERE TAT_C_CODE = '{$id_cust_crm[$i]}'";
      $update_dss = sqlsrv_query($conn2, $sql_update);
      $update += 1;
    } else {
      // INSERT
      $sql_insert = "INSERT INTO TAT_CUSTOMER (TAT_C_CODE, TAT_C_NAME, TAT_C_INSERT) VALUES (
        '{$data_cust_crm[$i]["ID_CUSTOMER"]}',
        '{$data_cust_crm[$i]["COMPANY_NAME"]}',
        '{$date2}'
      )";
      $insert_dss = sqlsrv_query($conn2, $sql_insert);
      $insert += 1;
    }
  }

  $log = $date2. " | TOTAL_DATA ".count($data_cust_crm). ' | INSERT TOTAL : '.$insert.' | UPDATE TOTAL : '.$update. ' ';
  echo $log;


 ?>
