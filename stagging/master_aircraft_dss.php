<?php

  ini_set('max_execution_time', 0);
  $dss_db = sqlsrv_connect(
                  "192.168.240.107",
                  // "172.16.100.35",
                  array(
                    "Database"=>"db_dss",
                    "UID"=>"usr_dss",
                    "PWD"=>"p@ssw0rd"
                  )
                );
  // local aeng
  // $dss_db = sqlsrv_connect(
  //                 "DESKTOP-HRV29SG\SQLEXPRESS",
  //                 array(
  //                   "Database"=>"db_dss",
  //                   "UID"=>"aengganteng",
  //                   "PWD"=>"aengganteng"
  //                 )
  //               );
  $crm_db = sqlsrv_connect(
              "192.168.240.101",
              array(
                "Database"=>"db_crm_info",
                "UID"=>"usr-crm",
                "PWD"=>"p@ssw0rd"
              )
            );
  if($dss_db == false || $crm_db == false){
    die( print_r( sqlsrv_errors(), true));
  }

  $ac_crm = sqlsrv_query($crm_db, "SELECT * FROM [dbo].[V_All_AC_Master]");
  $data_ac_crm = array();
  $ac_reg_crm = array();
  while ($arr = sqlsrv_fetch_array($ac_crm, SQLSRV_FETCH_ASSOC)) {
    $data_ac_crm[] = $arr;
    $ac_reg_crm[] = $arr["ACREG"];
  }
  $insert = $update = $insert_type_count = $insert_reg = $update_reg = 0;
  $ac_dss = sqlsrv_query($dss_db, "SELECT LTRIM(RTRIM(acreg)) AS acreg FROM m_acreg");
  $ac_reg_dss = array();
  while ($arr = sqlsrv_fetch_array($ac_dss, SQLSRV_FETCH_ASSOC)) {
    $ac_reg_dss[] = $arr["acreg"];
  }

  for ($i=0; $i < count($ac_reg_crm); $i++) {
    $q_check_type = sqlsrv_query($dss_db, "select * from [dbo].[m_actype] where actype like '%".$data_ac_crm[$i]["ACTYPE"]."'");
    $arr_check_type = sqlsrv_fetch_array($q_check_type, SQLSRV_FETCH_ASSOC);
    $customer = "";
    if($data_ac_crm[$i]["CUSTOMER_CODE"] == "GIA"){
      $customer = "GA";
    } else if ($data_ac_crm[$i]["CUSTOMER_CODE"] == "CTV"){
      $customer = "CITILINK";
    } else {
      $customer = $data_ac_crm[$i]["CUSTOMER"];
    }
    if(in_array($ac_reg_crm[$i], $ac_reg_dss)){
      if(count($arr_check_type) < 1){
        $insert_type = sqlsrv_query($dss_db, "insert into m_actype (actype, created_at) values ('".$data_ac_crm[$i]["ACTYPE"]."', '".date("Y-m-d H:i:s")."'); SELECT SCOPE_IDENTITY()");
        sqlsrv_next_result($insert_type);
        sqlsrv_fetch($insert_type);
        $insertId = sqlsrv_get_field($insert_type, 0);
        $insert_type_count += 1;
      } else {
        $insertId = $arr_check_type["actype_id"];
      }
      $update_type = sqlsrv_query($dss_db, "update m_acreg set actype_id = '".$insertId."', own = '".$customer."' , acreg_created_at = '".date("Y-m-d H:i:s")."' where acreg = '".$data_ac_crm[$i]["ACREG"]."'");
      $update += 1;
    } else {
      if(count($arr_check_type) < 1){

        $insert_type = sqlsrv_query($dss_db, "insert into m_actype (actype, created_at) values ('".$data_ac_crm[$i]["ACTYPE"]."', '".date("Y-m-d H:i:s")."'); SELECT SCOPE_IDENTITY()");
        sqlsrv_next_result($insert_type);
        sqlsrv_fetch($insert_type);
        $insertId = sqlsrv_get_field($insert_type, 0);
        $insert_type_count += 1;
        if($insert_type){
          $insert_reg = sqlsrv_query($dss_db, "insert into m_acreg (actype_id, own, acreg, acreg_created_at) values (
              '".$insertId."',
              '".$customer."',
              '".$data_ac_crm[$i]["ACREG"]."',
              '".date("Y-m-d H:i:s")."'
          )");
        }
      } else {
        $insert_reg = sqlsrv_query($dss_db, "insert into m_acreg (actype_id, own, acreg, acreg_created_at) values (
            '".$arr_check_type["actype_id"]."',
            '".$customer."',
            '".$data_ac_crm[$i]["ACREG"]."',
            '".date("Y-m-d H:i:s")."'
        )");
      }
      $insert += 1;
    }
  }

  $log = "STAGGING MASTER AIRCRAFT | TOTAL DATA : ".count($data_ac_crm)." | INSERT AC REG : ".$insert." | UPDATE AC REG : ".$update." | INSERT TYPE : ".$insert_type_count. "  ";

echo $log;
echo file_put_contents('C:\Application\Sched_DSS\log\sync_master_ac.txt', $log . "\r\n", FILE_APPEND | LOCK_EX);

 ?>
