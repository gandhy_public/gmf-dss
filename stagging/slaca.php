<?php
//=================================== coba ==============================================\\
ini_set('max_execution_time', 0);
ini_set('memory_limit', -1);

// $hostname_ = '192.168.240.107';
$hostname_ = '172.16.100.35';
$opsi_ = array('UID' => 'usr_dss', 'PWD' => 'p@ssw0rd', 'database' => 'db_dss');
$con_ = sqlsrv_connect($hostname_, $opsi_);
if (!$con_) {
    echo "Gagal Koneksi sd";
} else {
    echo 'berhasil';
}
//=================================== end coba ==========================================

//log
$dt = array();
$error = array();
$banyak = 0;
$tambah = 0;
$ubah = 0;
$gagal = 0;


//set grouping
$materials = "SELECT DISTINCT material FROM scc_trans";
$dataMaterials = sqlsrv_query($con_, $materials);

while ($dataMaterial = sqlsrv_fetch_array($dataMaterials)) {

    $dtQuery = "SELECT top 2 material, posting_date, entered_at, ess
                FROM scc_trans
	            JOIN scv_part_number ON REPLACE( LEFT ( scc_trans.material, CHARINDEX( ':', scc_trans.material ) ), ':', '' ) = scv_part_number.part_number
                WHERE material = '{$dataMaterial['material']}'
                ORDER BY posting_date DESC";


    $dt = sqlsrv_query($con_, $dtQuery);
    $groups = array();
    while ($dt1 = sqlsrv_fetch_array($dt)) {
        array_push($groups, $dt1);
    }
    
    if (count($groups) > 1) {
        $idMaterial = substr($groups[0]['material'], 0, strpos($groups[0]['material'], ":"));

        $groupQuery = "SELECT DISTINCT material FROM scc_group_sla where material = '{$groups[0]['material']}'";
        $connectionGroups = sqlsrv_query($con_, $groupQuery);
        $dataGroups = sqlsrv_fetch_array($connectionGroups);

        $diff = date_diff(new DateTime(date('Y-m-d', strtotime($groups[0]['posting_date']))), new DateTime(date('Y-m-d', strtotime($groups[1]['posting_date']))));

        if ($dataGroups == null) {
            if ($diff->days < 21 and $groups[0]['ess'] == '1') {
                $updateGroup = "INSERT INTO scc_group_sla (material, grouping) VALUES ('{$groups[0]['material']}', 'SL 2 24 Jam')";
            } elseif ($diff->days >= 21 and $groups[0]['ess'] == '1') {
                $updateGroup = "INSERT INTO scc_group_sla (material, grouping) VALUES ('{$groups[0]['material']}', 'SL 1 3 Jam')";
            } elseif ($groups[0]['ess'] == '2') {
                $updateGroup = "INSERT INTO scc_group_sla (material, grouping) VALUES ('{$groups[0]['material']}', 'SL 3 72 Jam')";
            } else {
                $updateGroup = "INSERT INTO scc_group_sla (material, grouping) VALUES ('{$groups[0]['material']}', 'SL 4 72 Jam')";
            }
        } else {
            if ($diff->days < 21 and $groups[0]['ess'] == '1') {
                $updateGroup = "UPDATE scc_group_sla SET grouping='SL 2 24 Jam' WHERE material='{$groups[0]['material']}'";
            } elseif ($diff->days >= 21 and $groups[0]['ess'] == '1') {
                $updateGroup = "UPDATE scc_group_sla SET grouping='SL 1 3 Jam' WHERE material='{$groups[0]['material']}'";
            } elseif ($groups[0]['ess'] == '2') {
                $updateGroup = "UPDATE scc_group_sla SET grouping='SL 3 72 Jam' WHERE material='{$groups[0]['material']}'";
            } else {
                $updateGroup = "UPDATE scc_group_sla SET grouping='SL 4 72 Jam' WHERE material='{$groups[0]['material']}'";
            }
        }

        $connectionUpdateGroup = sqlsrv_query($con_, $updateGroup);

        if ($connectionUpdateGroup === false) {
            array_push($error, $groups[0]['material']);
            $gagal += 1;
        } else {
            $ubah += 1;
        }
    }
}

//set performance

ini_set('max_execution_time', 0);
$data = "select * from scc_trans WHERE performance IS NULL ORDER BY id";
$data = sqlsrv_query($con_, $data);

while ($row = sqlsrv_fetch_array($data)) {

    $banyak += 1;
    $id = $row['id'];
    $material = substr($row['material'], 0, strpos($row['material'], ":"));
    $docMaterial = $row['doc_material'];
    $postingDate = $row['posting_date'];
    $enteredAt = $row['entered_at'];
    $funcLocation = $row['func_location'];
    $reqDate = $row['req_date'];
    $timeReq = $row['time_req'];
    $status = is_null($row['status']) ? 0 : $row['status'];
    $performance = is_null($row['performance']) ? 0 : $row['performance'];

    $calculateDate = date_diff(new DateTime(date('Y-m-d', strtotime($reqDate))), new DateTime(date('Y-m-d', strtotime($postingDate))));

    $grouping = "select grouping from dbo.scc_group_sla WHERE dbo.scc_group_sla.material ='{$row['material']}'";
    $sccGroups = sqlsrv_query($con_, $grouping);
    $sccGroup = sqlsrv_fetch_array($sccGroups);
    $timeGrouping = substr($sccGroup['grouping'], -6, 2);

    $calculateTime = date_diff(new DateTime(date('Y-m-d', strtotime($enteredAt))), new DateTime(date('Y-m-d', strtotime($timeReq))));

    $cek = "select ID from scc_trans where ID={$row['ID']}";
    $cek = sqlsrv_query($con_, $cek);
    $cek = sqlsrv_fetch_array($cek);

    $totalCalculate = ($calculateDate->format("%R%a") * 24) + ($calculateTime->format("%R%h"));

    if (abs((int)$totalCalculate) <= (int)$timeGrouping) {
        $update = "UPDATE scc_trans SET performance =100, status='ON TIME' WHERE id ={$id}";

        $update = sqlsrv_query($con_, $update);
        if ($update === false) {
            array_push($error, floatval($id));
            $gagal += 1;
        } else {
            $ubah += 1;
        }
    } else {
        $update = "UPDATE scc_trans SET performance =0, status='DELAY'WHERE id ={$id}";

        $update = sqlsrv_query($con_, $update);
        if ($update === false) {
            array_push($error, floatval($d1));
            $gagal += 1;
        } else {
            $ubah += 1;
        }
    }

}

//===================================== MEMBUAT LOG DATA ===============================
$log = date("Y-m-d") . ' | TOTAL DATA: ' . $banyak . ' | UPDATE DATA: ' . $ubah . ' FAILED: ' . $gagal;
$error = date("Y-m-d") . ' | ID ERROR: ' . json_encode($error);
echo file_put_contents('C:\Application\Sched_DSS\log\sink_user_log_slaca.txt', $log . "\r\n", FILE_APPEND | LOCK_EX);
echo file_put_contents('C:\Application\Sched_DSS\error\sink_user_log_slaca.txt', $error . "\r\n", FILE_APPEND | LOCK_EX);
