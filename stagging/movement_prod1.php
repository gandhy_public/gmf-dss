<?php
//=================================== coba ==============================================\
$hostname = '192.168.240.100';
$opsi = array('UID' => 'usr_dboard', 'PWD' => 'p@ssw0rd', 'database' => 'db_MROSystem');
$con = sqlsrv_connect($hostname, $opsi);
if (!$con) {
    echo "Gagal Koneksi movement prod1";
}else{
	echo 'berhasil';
}

// $hostname_ = '192.168.240.107'; //dev
$hostname_ = '172.16.100.35'; //prod
$opsi_ = array('UID' => 'usr_dss', 'PWD' => 'p@ssw0rd', 'database' => 'db_dss');
$con_ = sqlsrv_connect($hostname_, $opsi_);
if (!$con_) {
    echo "Gagal Koneksi dss";
}else{
	echo 'berhasil';
}
//=================================== end coba ==========================================

ini_set('max_execution_time', 0); 

$kemarin =  date('Y-m-d',strtotime("-1 days"));
$data = "select * from TBL_AC_MOVEMENT_PROD1 where COL_PLAN_DEPARTURE_DATE = '{$kemarin}' order by COL_IDX asc";
$data =  sqlsrv_query($con, $data);

$dt = array();
$banyak = 0;
$tambah = 0;
$ubah = 0;
$gagal = 0;
$get['error'] = array();

while ($row = sqlsrv_fetch_array($data)){
	$banyak +=1;
	$d1 = $row['COL_IDX'];
	$d2 = $row['COL_CARRIER_CODE'];
	$d3 = $row['COL_FLIGHT_NUMBER'];
	$d4 = $row['COL_DEPARTURE_NUMBER'];
	$d5 = $row['COL_DEPARTURE_STATION'];
	$d6 = $row['COL_PLAN_DEPARTURE_DATE']->format('Y-m-d');
	$d7 = $row['COL_AIRCRAFT_REGISTRATION'];
	$d8 = $row['COL_CHOX_OFF_DATE']->format('Y-m-d');
	$d9 = $row['COL_CHOX_OFF_TIME']->format('H:i:s');
	$d10 = $row['COL_WHEELS_OFF_DATE']->format('Y-m-d');
	$d11 = $row['COL_WHEELS_OFF_TIME']->format('H:i:s');
	$d12 = $row['COL_EST_DEP_DATE']->format('Y-m-d');
	$d13 = $row['COL_EST_DEP_TIME']->format('H:i:s');
	$d14 = $row['COL_EST_WHEELS_OFF_DATE']->format('Y-m-d');
	$d15 = $row['COL_EST_WHEELS_OFF_TIME']->format('H:i:s');
	$d16 = $row['COL_CHOX_ON_DATE']->format('Y-m-d');
	$d17 = $row['COL_CHOX_ON_TIME']->format('H:i:s');
	$d18 = $row['COL_WHEELS_ON_DATE']->format('Y-m-d');
	$d19 = $row['COL_WHEELS_ON_TIME']->format('H:i:s');
	$d20 = $row['COL_EST_ARR_DATE']->format('Y-m-d');
	$d21 = $row['COL_EST_ARR_TIME']->format('H:i:s');
	$d22 = $row['COL_EST_WHEELS_ON_DATE']->format('Y-m-d');
	$d23 = $row['COL_EST_WHEELS_ON_TIME']->format('H:i:s');
	$d24 = $row['COL_ARR_STATION'];
	$d25 = $row['COL_FLIGHT_TYPE'];
	$d26 = $row['COL_ARR_STAND_LOC'];
	$d27 = $row['COL_DEP_STAND_LOC'];
	$d28 = $row['COL_CANCEL_INDICATOR'];
	$d29 = $row['COL_ARR_TERMINAL'];
	$d30 = $row['COL_DEP_TERMINAL'];
	$d31 = $row['COL_FLAG'];
	$d32 = $row['COL_DUPL'];
	$d32 = str_replace( "'", " ", $d32 ); 
	$d32 = str_replace( '"', '', $d32 );
	$d33 = $row['COL_ENTRY_DATE']->format('Y-m-d H:i:s');
	$d34 = $row['COL_KEY'];
	$d34 = str_replace( "'", " ", $d34 ); 
	$d34 = str_replace( '"', '', $d34 );
	$d35 = $row['REFERENCE_NUMBER'];
	$d36 = $row['TIMESTAMP']->format('Y-m-d H:i:s');
	
	$cek = "select COL_IDX from TBL_AC_MOVEMENT_PROD1 where COL_IDX={$row['COL_IDX']}";
	$cek =  sqlsrv_query($con_, $cek);
	$cek = sqlsrv_fetch_array($cek);
	$cek = count($cek);
	if($cek == 0){
			$insert = "INSERT INTO TBL_AC_MOVEMENT_PROD1(
	COL_IDX,COL_CARRIER_CODE,COL_FLIGHT_NUMBER,COL_DEPARTURE_NUMBER,COL_DEPARTURE_STATION,COL_PLAN_DEPARTURE_DATE,COL_AIRCRAFT_REGISTRATION,COL_CHOX_OFF_DATE,COL_CHOX_OFF_TIME,COL_WHEELS_OFF_DATE,
	COL_WHEELS_OFF_TIME,COL_EST_DEP_DATE,COL_EST_DEP_TIME,COL_EST_WHEELS_OFF_DATE,COL_EST_WHEELS_OFF_TIME,COL_CHOX_ON_DATE,COL_CHOX_ON_TIME,COL_WHEELS_ON_DATE,COL_WHEELS_ON_TIME,COL_EST_ARR_DATE,
	COL_EST_ARR_TIME,COL_EST_WHEELS_ON_DATE,COL_EST_WHEELS_ON_TIME,COL_ARR_STATION,COL_FLIGHT_TYPE,COL_ARR_STAND_LOC,COL_DEP_STAND_LOC,COL_CANCEL_INDICATOR,COL_ARR_TERMINAL,COL_DEP_TERMINAL,
	COL_FLAG,COL_DUPL,COL_ENTRY_DATE,COL_KEY,REFERENCE_NUMBER,TIMESTAMP
	) VALUES (
	{$d1},'{$d2}',
	'{$d3}','{$d4}',
	'{$d5}',
	'{$d6}',
	'{$d7}','{$d8}',
	'{$d9}',
	'{$d10}',
	'{$d11}','{$d12}','{$d13}','{$d14}','{$d15}','{$d16}','{$d17}','{$d18}','{$d19}','{$d20}',
	'{$d21}','{$d22}','{$d23}','{$d24}','{$d25}','{$d26}','{$d27}','{$d28}','{$d29}','{$d30}',
	'{$d31}','{$d32}','{$d33}','{$d34}','{$d35}'
	,'{$d36}'
	)";
	
	$insert =  sqlsrv_query($con_, $insert);
		if($insert === false){
			array_push($get['error'], floatval($d1));
			$gagal +=1;
		}else{
			$tambah +=1;
		}
	}else{
			$update = "UPDATE TBL_AC_MOVEMENT_PROD1 SET
			COL_CARRIER_CODE='{$d2}',COL_FLIGHT_NUMBER='{$d3}',COL_DEPARTURE_NUMBER='{$d4}',COL_DEPARTURE_STATION='{$d5}',COL_PLAN_DEPARTURE_DATE='{$d6}',COL_AIRCRAFT_REGISTRATION='{$d7}',COL_CHOX_OFF_DATE='{$d8}',COL_CHOX_OFF_TIME='{$d9}',COL_WHEELS_OFF_DATE='{$d10}',
			COL_WHEELS_OFF_TIME='{$d11}',COL_EST_DEP_DATE='{$d12}',COL_EST_DEP_TIME='{$d13}',COL_EST_WHEELS_OFF_DATE='{$d14}',COL_EST_WHEELS_OFF_TIME='{$d15}',COL_CHOX_ON_DATE='{$d16}',COL_CHOX_ON_TIME='{$d17}',COL_WHEELS_ON_DATE='{$d18}',COL_WHEELS_ON_TIME='{$d19}',COL_EST_ARR_DATE='{$d20}',
			COL_EST_ARR_TIME='{$d21}',COL_EST_WHEELS_ON_DATE='{$d22}',COL_EST_WHEELS_ON_TIME='{$d23}',COL_ARR_STATION='{$d24}',COL_FLIGHT_TYPE='{$d25}',COL_ARR_STAND_LOC='{$d26}',COL_DEP_STAND_LOC='{$d27}',COL_CANCEL_INDICATOR='{$d28}',COL_ARR_TERMINAL='{$d29}',COL_DEP_TERMINAL='{$d30}',
			COL_FLAG='{$d31}',COL_DUPL='{$d32}',COL_ENTRY_DATE='{$d33}',COL_KEY='{$d34}',REFERENCE_NUMBER='{$d35}',TIMESTAMP='{$d36}'
			WHERE COL_IDX={$d1}

			";
			
			$update =  sqlsrv_query($con_, $update);
			if($update === false){
			array_push($get['error'], floatval($d1));
				$gagal +=1;
			}else{
				$ubah +=1;
			}
		
	}
}




//===================================== MEMBUAT LOG DATA ===============================
$log = date("Y-m-d") . ' | TOTAL DATA: ' . $banyak . ' | INSERT DATA: ' . $tambah . ' | UPDATE DATA: ' . $ubah . ' FAILED: ' . $gagal;
$error = date("Y-m-d") . ' | ID ERROR: ' . json_encode($get['error']);
echo file_put_contents('C:\Application\Sched_DSS\log\sink_user_log_movementprod1.txt', $log . "\r\n", FILE_APPEND | LOCK_EX);
echo file_put_contents('C:\Application\Sched_DSS\error\sink_user_log_movementprod1.txt', $error . "\r\n", FILE_APPEND | LOCK_EX);
