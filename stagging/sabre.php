<?php
//=================================== coba ==============================================\
$hostname = '192.168.240.100';
$opsi = array('UID' => 'usr_dboard', 'PWD' => 'p@ssw0rd', 'database' => 'db_dboard');
$con = sqlsrv_connect($hostname, $opsi);
if (!$con) {
    echo "Gagal Koneksi SABRE";
}else{
	echo 'berhasil';
}

$hostname_ = '192.168.240.107'; // dev
$hostname_ = '172.16.100.35'; // prod
$opsi_ = array('UID' => 'usr_dss', 'PWD' => 'p@ssw0rd', 'database' => 'db_dss');
$con_ = sqlsrv_connect($hostname_, $opsi_);
if (!$con_) {
    echo "Gagal Koneksi DSS";
}else{
	echo 'berhasil';
}
//=================================== end coba ==========================================

ini_set('max_execution_time', 0); 

$kemarin =  date('d-M-y',strtotime("-1 days"));
$data = "select * from TBL_FLT_SABRE_HIST where DEP_DATE_ACT_UTC = '{$kemarin}' order by FLIGHT_LEG_ID asc";
// $data = "select * from TBL_FLT_SABRE_HIST where MONTH(DEP_DATE_ACT_UTC) >= 1 and YEAR(DEP_DATE_ACT_UTC) >= 2017 and MONTH(DEP_DATE_ACT_UTC) <= 12 and YEAR(DEP_DATE_ACT_UTC) <= 2017 order by FLIGHT_LEG_ID asc";
$data =  sqlsrv_query($con, $data);

$dt = array();
$banyak = 0;
$tambah = 0;
$ubah = 0;
$gagal = 0;
$get['error'] = array();

while ($row = sqlsrv_fetch_array($data)){
	$banyak +=1;
	$d1 = $row['FLIGHT_LEG_ID'];
	$d2 = $row['FLT_NBR'];
	$d3 = $row['DEP_DATE_SCHED_UTC'];
	$d4 = $row['DEP_TIME_SCHED_UTC'];
	$d5 = $row['DEP_DATE_SCHED_LOCAL'];
	$d6 = $row['DEP_TIME_SCHED_LOCAL'];
	$d7 = $row['DEP_STN_SCHED'];
	$d8 = $row['ARR_STN_SCHED'];
	$d9 = $row['ARR_DATE_SCHED_UTC'];
	$d10 = $row['ARR_TIME_SCHED_UTC'];
	$d11 = $row['ARR_DATE_SCHED_LOCAL'];
	$d12 = $row['ARR_TIME_SCHED_LOCAL'];
	$d13 = $row['AC_REG'];
	$d14 = $row['AC_TYPE'];
	$d15 = $row['DEP_DATE_ACT_UTC'];
	$d16 = $row['DEP_TIME_ACT_UTC'];
	$d17 = $row['DEP_DATE_ACT_LOCAL'];
	$d18 = $row['DEP_TIME_ACT_LOCAL'];
	$d19 = $row['DEP_STN_ACT'];
	$d20 = $row['ARR_STN_ACT'];
	$d21 = $row['ARR_DATE_ACT_UTC'];
	$d22 = $row['ARR_TIME_ACT_UTC'];
	$d23 = $row['ARR_DATE_ACT_LOCAL'];
	$d24 = $row['ARR_TIME_ACT_LOCAL'];
	$d25 = $row['DELAYDESCR'];
	$d25 = str_replace( "'", " ", $d25 ); 
	$d25 = str_replace( '"', '', $d25 );
	$d26 = $row['REMARK_XML4OPS'];
	$d26 = str_replace( "'", " ", $d26 ); 
	$d26 = str_replace( '"', '', $d26 );
	$d27 = $row['REMARK'];
	$d27 = str_replace( "'", " ", $d27 ); 
	$d27 = str_replace( '"', '', $d27 );
	$d28 = $row['CARGO_WEIGHT'];
	$d29 = $row['MAIL_WEIGHT'];
	$d30 = $row['BAG_PIECES'];
	$d31 = $row['BAG_WEIGHT'];
	$d32 = $row['LEG_TYPE'];
	$d33 = $row['LEG_STATE'];
	$d34 = $row['SEQ_NO'];
	$d35 = $row['SYSDATEUTC'];
	$d36 = $row['AIRBORNE_DT'];
	$d37 = $row['LANDING_DT'];
	$d38 = $row['B_DEP_DT'];
	$d39 = $row['B_DEP_LOCAL_DT'];
	$d40 = $row['B_AIRBORNE_DT'];
	$d41 = $row['B_LANDING_DT'];
	$d42 = $row['B_ARR_DT'];
	$d43 = $row['A_DEP_DT'];
	$d44 = $row['A_AIRBORNE_DT'];
	$d45 = $row['A_LANDING_DT'];
	$d46 = $row['A_ARR_DT'];
	$d47 = $row['DEP_TIME_SCHEDULE'];
	$d48 = $row['DEP_TIME_ACTUAL'];
	$d49 = $row['DELAYDEP1'];
	$d50 = $row['DELAYDEP2'];
	$d51 = $row['ARR_TIME_SCHEDULE'];
	$d52 = $row['ARR_TIME_ACTUAL'];
	$d53 = $row['DELAYARR1'];
	$d54 = $row['DELAYARR2'];
	$d55 = $row['AIRCRAFT_BODY'];
	$d56 = $row['GROUND_TIME_STANDARD'];
	$d57 = $row['CODE_DELAY'];
	$d58 = $row['CODEDELAY1'];
	$d59 = $row['DELAYTIME1'];
	$d60 = $row['CODEDELAY2'];
	$d61 = $row['DELAYTIME2'];
	$d62 = $row['CODEDELAY3'];
	$d63 = $row['DELAYTIME3'];
	$d64 = $row['CODEDELAY4'];
	$d65 = $row['DELAYTIME4'];
	$d66 = $row['F_PAX'];
	$d67 = $row['C_PAX'];
	$d68 = $row['Y_PAX'];
	$d69 = $row['AC_VER'];
	$d70 = $row['TIME_DELAY'];
	$d71 = $row['DEP_DATE_SCHED_UTC_LC'];
	$d72 = $row['DEP_TIME_SCHED_UTC_LC'];
	$d73 = $row['ARR_DATE_SCHED_UTC_LC'];
	$d74 = $row['ARR_TIME_SCHED_UTC_LC'];
	$d75 = $row['DEP_DATE_ACT_UTC_LC'];
	$d76 = $row['DEP_TIME_ACT_UTC_LC'];
	$d77 = $row['ARR_DATE_ACT_UTC_LC'];
	$d78 = $row['ARR_TIME_ACT_UTC_LC'];
	$d79 = $row['DEP_TIME_ACTUAL_LC'];
	$d80 = $row['ARR_TIME_ACTUAL_LC'];
	$d81 = $row['DEP_TIME_SCHEDULE_LC'];
	$d82 = $row['ARR_TIME_SCHEDULE_LC'];
	$d83 = $row['DELAYDEP1_LC'];
	$d84 = $row['DELAYDEP2_LC'];
	$d85 = $row['DELAYARR1_LC'];
	$d86 = $row['DELAYARR2_LC'];
	$d87 = $row['AIRBORNE_DT_LC'];
	$d88 = $row['LANDING_DT_LC'];
	$d89 = $row['B_DEP_DT_LC'];
	$d90 = $row['B_AIRBORNE_DT_LC'];
	$d91 = $row['B_LANDING_DT_LC'];
	$d92 = $row['B_ARR_DT_LC'];
	$d93 = $row['A_DEP_DT_LC'];
	$d94 = $row['A_AIRBORNE_DT_LC'];
	$d95 = $row['A_LANDING_DT_LC'];
	$d96 = $row['A_ARR_DT_LC'];
	$d97 = $row['TECH_DELAY'];
	$d98 = $row['PILOT'];
	$d98 = str_replace( "'", " ", $d98 ); 
	$d98 = str_replace( '"', '', $d98 );
	
	$cek = "select FLIGHT_LEG_ID from TBL_FLT_SABRE where FLIGHT_LEG_ID={$row['FLIGHT_LEG_ID']}";
	$cek =  sqlsrv_query($con_, $cek);
	$cek = sqlsrv_fetch_array($cek);
	$cek = count($cek);
	if($cek == 0){
			$insert = "INSERT INTO TBL_FLT_SABRE(
	FLIGHT_LEG_ID,FLT_NBR,DEP_DATE_SCHED_UTC,DEP_TIME_SCHED_UTC,DEP_DATE_SCHED_LOCAL,DEP_TIME_SCHED_LOCAL,DEP_STN_SCHED,ARR_STN_SCHED,ARR_DATE_SCHED_UTC,ARR_TIME_SCHED_UTC,
	ARR_DATE_SCHED_LOCAL,ARR_TIME_SCHED_LOCAL,AC_REG,AC_TYPE,DEP_DATE_ACT_UTC,DEP_TIME_ACT_UTC,DEP_DATE_ACT_LOCAL,DEP_TIME_ACT_LOCAL,DEP_STN_ACT,ARR_STN_ACT,
	ARR_DATE_ACT_UTC,ARR_TIME_ACT_UTC,ARR_DATE_ACT_LOCAL,ARR_TIME_ACT_LOCAL,DELAYDESCR,REMARK_XML4OPS,REMARK,CARGO_WEIGHT,MAIL_WEIGHT,BAG_PIECES,
	BAG_WEIGHT,LEG_TYPE,LEG_STATE,SEQ_NO,SYSDATEUTC,AIRBORNE_DT,LANDING_DT,B_DEP_DT,B_DEP_LOCAL_DT,B_AIRBORNE_DT,
	B_LANDING_DT,B_ARR_DT,A_DEP_DT,A_AIRBORNE_DT,A_LANDING_DT,A_ARR_DT,DEP_TIME_SCHEDULE,DEP_TIME_ACTUAL,DELAYDEP1,DELAYDEP2,
	ARR_TIME_SCHEDULE,ARR_TIME_ACTUAL,DELAYARR1,DELAYARR2,AIRCRAFT_BODY,GROUND_TIME_STANDARD,CODE_DELAY,CODEDELAY1,DELAYTIME1,CODEDELAY2,
	DELAYTIME2,CODEDELAY3,DELAYTIME3,CODEDELAY4,DELAYTIME4,F_PAX,C_PAX,Y_PAX,AC_VER,TIME_DELAY,
	DEP_DATE_SCHED_UTC_LC,DEP_TIME_SCHED_UTC_LC,ARR_DATE_SCHED_UTC_LC,ARR_TIME_SCHED_UTC_LC,DEP_DATE_ACT_UTC_LC,DEP_TIME_ACT_UTC_LC,ARR_DATE_ACT_UTC_LC,ARR_TIME_ACT_UTC_LC,DEP_TIME_ACTUAL_LC,ARR_TIME_ACTUAL_LC,
	DEP_TIME_SCHEDULE_LC,ARR_TIME_SCHEDULE_LC,DELAYDEP1_LC,DELAYDEP2_LC,DELAYARR1_LC,DELAYARR2_LC,AIRBORNE_DT_LC,LANDING_DT_LC,B_DEP_DT_LC,B_AIRBORNE_DT_LC,
	B_LANDING_DT_LC,B_ARR_DT_LC,A_DEP_DT_LC,A_AIRBORNE_DT_LC,A_LANDING_DT_LC,A_ARR_DT_LC,TECH_DELAY,PILOT
	) VALUES(
	{$d1},'{$d2}','{$d3}','{$d4}','{$d5}','{$d6}','{$d7}','{$d8}','{$d9}','{$d10}',
	'{$d11}','{$d12}','{$d13}','{$d14}','{$d15}','{$d16}','{$d17}','{$d18}','{$d19}','{$d20}',
	'{$d21}','{$d22}','{$d23}','{$d24}','{$d25}','{$d26}','{$d27}','{$d28}','{$d29}','{$d30}',
	'{$d31}','{$d32}','{$d33}','{$d34}','{$d35}','{$d36}','{$d37}','{$d38}','{$d39}','{$d40}',
	'{$d41}','{$d42}','{$d43}','{$d44}','{$d45}','{$d46}','{$d47}','{$d48}','{$d49}','{$d50}',
	'{$d51}','{$d52}','{$d53}','{$d54}','{$d55}','{$d56}','{$d57}','{$d58}','{$d59}','{$d60}',
	'{$d61}','{$d62}','{$d63}','{$d64}','{$d65}','{$d66}','{$d67}','{$d68}','{$d69}','{$d70}',
	'{$d71}','{$d72}','{$d73}','{$d74}','{$d75}','{$d76}','{$d77}','{$d78}','{$d79}','{$d80}',
	'{$d81}','{$d82}','{$d83}','{$d84}','{$d85}','{$d86}','{$d87}','{$d88}','{$d89}','{$d90}',
	'{$d91}','{$d92}','{$d93}','{$d94}','{$d95}','{$d96}','{$d97}','{$d98}'
	)";
	
	$insert =  sqlsrv_query($con_, $insert);
		if($insert === false){
			array_push($get['error'], floatval($d1));
			$gagal +=1;
		}else{
			$tambah +=1;
		}
	}else{
			$update = "UPDATE TBL_FLT_SABRE SET
			FLT_NBR='{$d2}',DEP_DATE_SCHED_UTC='{$d3}',DEP_TIME_SCHED_UTC='{$d4}',DEP_DATE_SCHED_LOCAL='{$d5}',DEP_TIME_SCHED_LOCAL='{$d6}',DEP_STN_SCHED='{$d7}',ARR_STN_SCHED='{$d8}',ARR_DATE_SCHED_UTC='{$d9}',ARR_TIME_SCHED_UTC='{$d10}',
			ARR_DATE_SCHED_LOCAL='{$d11}',ARR_TIME_SCHED_LOCAL='{$d12}',AC_REG='{$d13}',AC_TYPE='{$d14}',DEP_DATE_ACT_UTC='{$d15}',DEP_TIME_ACT_UTC='{$d16}',DEP_DATE_ACT_LOCAL='{$d17}',DEP_TIME_ACT_LOCAL='{$d18}',DEP_STN_ACT='{$d19}',ARR_STN_ACT='{$d20}',
			ARR_DATE_ACT_UTC='{$d21}',ARR_TIME_ACT_UTC='{$d22}',ARR_DATE_ACT_LOCAL='{$d23}',ARR_TIME_ACT_LOCAL='{$d24}',DELAYDESCR='{$d25}',REMARK_XML4OPS='{$d26}',REMARK='{$d27}',CARGO_WEIGHT='{$d28}',MAIL_WEIGHT='{$d29}',BAG_PIECES='{$d30}',
			BAG_WEIGHT='{$d31}',LEG_TYPE='{$d32}',LEG_STATE='{$d33}',SEQ_NO='{$d34}',SYSDATEUTC='{$d35}',AIRBORNE_DT='{$d36}',LANDING_DT='{$d37}',B_DEP_DT='{$d38}',B_DEP_LOCAL_DT='{$d39}',B_AIRBORNE_DT='{$d40}',
			B_LANDING_DT='{$d41}',B_ARR_DT='{$d42}',A_DEP_DT='{$d43}',A_AIRBORNE_DT='{$d44}',A_LANDING_DT='{$d45}',A_ARR_DT='{$d46}',DEP_TIME_SCHEDULE='{$d47}',DEP_TIME_ACTUAL='{$d48}',DELAYDEP1='{$d49}',DELAYDEP2='{$d50}',
			ARR_TIME_SCHEDULE='{$d51}',ARR_TIME_ACTUAL='{$d52}',DELAYARR1='{$d53}',DELAYARR2='{$d54}',AIRCRAFT_BODY='{$d55}',GROUND_TIME_STANDARD='{$d56}',CODE_DELAY='{$d57}',CODEDELAY1='{$d58}',DELAYTIME1='{$d59}',CODEDELAY2='{$d60}',
			DELAYTIME2='{$d61}',CODEDELAY3='{$d62}',DELAYTIME3='{$d63}',CODEDELAY4='{$d64}',DELAYTIME4='{$d65}',F_PAX='{$d66}',C_PAX='{$d67}',Y_PAX='{$d68}',AC_VER='{$d69}',TIME_DELAY='{$d70}',
			DEP_DATE_SCHED_UTC_LC='{$d71}',DEP_TIME_SCHED_UTC_LC='{$d72}',ARR_DATE_SCHED_UTC_LC='{$d73}',ARR_TIME_SCHED_UTC_LC='{$d74}',DEP_DATE_ACT_UTC_LC='{$d75}',DEP_TIME_ACT_UTC_LC='{$d76}',ARR_DATE_ACT_UTC_LC='{$d77}',ARR_TIME_ACT_UTC_LC='{$d78}',DEP_TIME_ACTUAL_LC='{$d79}',ARR_TIME_ACTUAL_LC='{$d80}',
			DEP_TIME_SCHEDULE_LC='{$d81}',ARR_TIME_SCHEDULE_LC='{$d82}',DELAYDEP1_LC='{$d83}',DELAYDEP2_LC='{$d84}',DELAYARR1_LC='{$d85}',DELAYARR2_LC='{$d86}',AIRBORNE_DT_LC='{$d87}',LANDING_DT_LC='{$d88}',B_DEP_DT_LC='{$d89}',B_AIRBORNE_DT_LC='{$d90}',
			B_LANDING_DT_LC='{$d91}',B_ARR_DT_LC='{$d92}',A_DEP_DT_LC='{$d93}',A_AIRBORNE_DT_LC='{$d94}',A_LANDING_DT_LC='{$d95}',A_ARR_DT_LC='{$d96}',TECH_DELAY='{$d97}',PILOT='{$d98}'
			WHERE FLIGHT_LEG_ID={$d1}

			";
			
			$update =  sqlsrv_query($con_, $update);
			if($update === false){
			array_push($get['error'], floatval($d1));
				$gagal +=1;
			}else{
				$ubah +=1;
			}
	}
}




//===================================== MEMBUAT LOG DATA ===============================
$log = date("Y-m-d") . ' | TOTAL DATA: ' . $banyak . ' | INSERT DATA: ' . $tambah . ' | UPDATE DATA: ' . $ubah . ' FAILED: ' . $gagal;
$error = date("Y-m-d") . ' | ID ERROR: ' . json_encode($get['error']);
echo file_put_contents('C:\Application\Sched_DSS\log\sink_user_log.txt', $log . "\r\n", FILE_APPEND | LOCK_EX);
echo file_put_contents('C:\Application\Sched_DSS\error\sink_user_log.txt', $error . "\r\n", FILE_APPEND | LOCK_EX);
