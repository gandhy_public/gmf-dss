<?php 

ini_set('max_execution_time', 0);

// DB_DBOARD connection
$host1 = '172.16.100.35';
$opsi = array('UID' => 'usr_dss', 'PWD' => 'p@ssw0rd', 'database' => 'db_shil');
$conn1 = sqlsrv_connect($host1, $opsi);
if(!$conn1){
	echo "Failed Connect db_shil \n";
} else {

	echo "Succeed Connect db_shil \n";
}

// DB_DSS connection
$host2 = '192.168.240.107'; // dev
// $host2 = '172.16.100.35'; // prod
$opsi2 = array('UID' => 'usr_dss', 'PWD' => 'p@ssw0rd', 'database' => 'db_dss');
$conn2 = sqlsrv_connect($host2, $opsi2);
if(!$conn2){
	echo "Failed Connect DB_DSS \n";
} else {
	echo "Succeed Connect DB_DSS \n";
}

$date1 = date("Y-m-d", strtotime("-1 days"));
$date2 = date("Y-m-d");

$dt_dboard = "SELECT
		* 
	FROM
		[dbo].[TBLHIL_SWIFT] 
	";
	// WHERE
	// CASE 
	// 	WHEN ISDATE( DateOccur ) = '1' THEN
	// 	CAST ( DateOccur AS DATE ) 
	// END BETWEEN '{$date1}' AND '".$date2."'
	// AND
	// CASE 
	// 	WHEN ISDATE( DateClose ) = '1' THEN
	// 	CAST ( DateClose AS DATE )
	// END BETWEEN '{$date1}' AND '{$date2}'
	// END BETWEEN '2018-01-17' AND '2018-07-19'

$res = sqlsrv_query($conn1, $dt_dboard);

while ($i = sqlsrv_fetch_object($res)) {
	$ii[] = $i;
}

$ins = $inf = $upd = 0;
$dtf = '';

$db_dss_check = "
	SELECT DISTINCT
		notification
	FROM
		sap_hil_d3
	";
	// WHERE
	// CASE
	// 	WHEN ISDATE( created_on ) = '1' THEN
	// 	CAST ( created_on AS DATE ) 
	// END BETWEEN '".$date1."' AND '".$date2."'
	// END BETWEEN '2018-01-17' AND '2018-07-19'

$db_dss_check = sqlsrv_query($conn2, $db_dss_check);

while ($i = sqlsrv_fetch_array($db_dss_check)) {
	$jj[] = $i;
}

// Create single array
if (!empty($jj)) {
	$jj = array_column($jj, 'notification');
}

for ($j=0; $j < count($ii); $j++) { 
	if(count($ii) > 0) {  
		if(in_array($ii[$j]->itemID, $jj)) {

				$db_dss_upd = "UPDATE sap_hil_d3
					SET system_status = '".$ii[$j]->status_D3."',
					status = '{$ii[$j]->Status}',
					upd_date = GETDATE()
					WHERE notification = '".$ii[$j]->itemID."' ";
					
				$db_dss_upd = sqlsrv_query($conn2, $db_dss_upd);

				$upd += 1;

		} else {

			$description = str_replace("'", "''", $ii[$j]->Description);
			$db_dss_ins = "INSERT INTO sap_hil_d3 (
				notification, 
				description,
				system_status,
				status,
				created_on,
				completion,
				functional_location,
				req_start,
				ins_date
			)
			VALUES
				(
				'".$ii[$j]->itemID."',
				'".$description."',
				'".$ii[$j]->status_D3."',
				'".$ii[$j]->Status."',
				'".$ii[$j]->DateOccur."',
				'".$ii[$j]->DateClose."',
				'".$ii[$j]->acreg."',
				'".$ii[$j]->DueDate."',
				GETDATE()
			)";
			$db_dss_ins = sqlsrv_query($conn2, $db_dss_ins);
			
			$ins += 1;
		}
	}

}

$log = date("Y-m-d H:i:s") . ' | TOTAL DATA: ' . count($ii) . ' | INSERT DATA: ' . $ins . ' | UPDATE DATA: ' . $upd . ' '; 
echo $log;
echo file_put_contents('C:\Application\Sched_DSS\log\sink_logHIL.txt', $log . "\r\n", FILE_APPEND | LOCK_EX);

?>