<?php
    ini_set('max_execution_time', 0);

    $global_array = array();

    // connection_1 MCDR
    $conn_month = mysqli_connect("172.16.100.105", "usr-dss", "P@ssw0rd", "db_dss");

    //connection_2 IMESYS
    $conn_week = sqlsrv_connect("192.168.240.100", array("Database"=>"Imesys", "UID"=>"usr_dboard", "PWD"=>"p@ssw0rd"));
    if( $conn_week === false ) {
        die( print_r( sqlsrv_errors(), true));
    }



    $date1months = date('Y-m-d',strtotime("-1 months"));
    $date1 = date('Y-m-d',strtotime("-7 days"));
    $date2 = date('Y-m-d');
    // $date1months = "2018-10-01";
    // $date1 = "2018-02-28";
    // $date2 = "2018-10-31";

    //////////////////////////////////////////////////////////////// WEEKLY ////////////////////////////////////////////////////////////////

    $sql1 = "SELECT airborneDate, acReg, SUM(total_revenue) as total_revenue
            from(
            	SELECT
            		airborneDate,
            		acReg,
            		COUNT (revenue) as total_revenue
            	FROM
            		aircraftflightlog
            	where airborneDate BETWEEN '$date1' and '$date2'
            	GROUP BY acReg, airborneDate
            ) a

            GROUP BY acReg, airborneDate
            ORDER BY  acReg DESC";
    $q3 = sqlsrv_query($conn_week, $sql1);
    $data = [];
    //cretate temp table;
    mysqli_query($conn_month ,"DROP TABLE IF EXISTS temp_aircraftflightlog");
    $sqlDdl = "CREATE TABLE temp_aircraftflightlog (
                  ta_id int(11) NOT NULL AUTO_INCREMENT,
                  airborneDate date NULL,
                  acReg varchar(100) NULL,
                  total_revenue int(11) NULL,
                  PRIMARY KEY (ta_id)
                )";
    $execDdl = mysqli_query($conn_month, $sqlDdl);
    $insert = $update = 0;
    if($execDdl){
        while ($arr = sqlsrv_fetch_array( $q3, SQLSRV_FETCH_ASSOC)) {
          $insertTemp = mysqli_query($conn_month, "insert into temp_aircraftflightlog(airborneDate, acReg, total_revenue)
                        values ('".$arr["airborneDate"]->format('Y-m-d')."', '".$arr["acReg"]."', '".$arr["total_revenue"]."')");
          if($insertTemp === false){
            throw new Exception("Server Error");
          }
        }
        mysqli_commit($conn_month);
        $q4 = mysqli_query($conn_month, "SELECT
              	*
              FROM
              	(
              		SELECT
              			MonthEval,
              			Reg,
              			ACtype,
              			sum(RevFC) AS total_revenue,
              			SUM(delay) AS delay,
              			sum(cancel) AS cancel,
              			case
              				WHEN RevFC > 0
              			  THEN (
              					(
              						SUM(RevFC) - (SUM(delay) + SUM(cancel))
              					) / SUM(RevFC)
              				) * 100
              				WHEN RevFC = 0
              				THEN 0
              				ELSE 0
              			end AS result
              		FROM
              			(
              				SELECT
              					a.airborneDate AS MonthEval,
              					a.acReg AS Reg,
              					b.ACtype,
              					SUM(a.total_revenue) AS RevFC,
              					0 AS delay,
              					0 AS cancel
              				FROM
              					temp_aircraftflightlog a
              				JOIN (
              					SELECT DISTINCT
              						ACType,
              						Reg
              					FROM
              						mcdrnew
              				) b ON a.acReg = b.Reg
              				WHERE
              					a.airborneDate BETWEEN '".$date1."'
              						AND '".$date2."'
              				GROUP BY
              					a.acReg,
              					a.airborneDate,
              					b.ACtype
              				UNION
              					SELECT
              						b.DateEvent AS MonthEval,
              						b.Reg AS Reg,
              						b.ACtype,
              						0 AS RevFC,
              						count(b.DCP) AS delay,
              						0 AS cancel
              					FROM mcdrnew b
              					WHERE
              						b.DateEvent BETWEEN '".$date1."'
              						AND '".$date2."'
              					AND b.DCP = 'D'
              					GROUP BY
              						b.Reg ,
              						b.DateEvent,
              						b.ACtype
              					UNION
              						SELECT
              							b.DateEvent AS MonthEval,
              							b.Reg AS Reg,
              							b.ACtype,
              							0 AS RevFC,
              							0 AS delay,
              							count(b.DCP) AS cancel
              						FROM mcdrnew b
              						WHERE
              						b.DateEvent BETWEEN '".$date1."'
              						AND '".$date2."'
              						AND b.DCP = 'C'
              						GROUP BY
              							b.Reg,
              							b.DateEvent,
              							b.ACtype
              			) A
              		GROUP BY
              			Reg,
              			MonthEval,
              			Actype
              	) ab
              ORDER BY
              	ab.total_revenue"
          );

        $weekly_guest = array();
        $same_guest_weekly = array();
        while ($arr = mysqli_fetch_assoc($q4)) {
          $weekly_guest[] = $arr;
          $same_guest_weekly[] = array(
            "date" => $arr["MonthEval"],
            "acType" => $arr["ACtype"],
            "Reg" => $arr["Reg"]
          );
        }

        $check_dss_weekly = mysqli_query($conn_month, "select dr_date, dr_actype, dr_acreg from dr_final where dr_date between '".$date1."' and '".$date2."' and dr_range_type = 'weekly'");
        $same_dss_weekly = array();
        while ($arr = mysqli_fetch_assoc($check_dss_weekly)) {
          $same_dss_weekly[] = array(
            "date" => $arr["dr_date"],
            "acType" => $arr["dr_actype"],
            "Reg" => $arr["dr_acreg"]
          );
        }

        for ($i=0; $i < count($same_guest_weekly); $i++) {
          if(in_array($same_guest_weekly[$i], $same_dss_weekly)){
            $sql_update = "update dr_final
                           set dr_total_rev = ".$weekly_guest[$i]["total_revenue"].",
                           dr_total_delay = ".$weekly_guest[$i]["delay"].",
                           dr_total_cancel = ".$weekly_guest[$i]["cancel"].",
                           dr_result = ".floatval($weekly_guest[$i]["result"])."
                           where dr_date = '".$weekly_guest[$i]["MonthEval"]."'
                           and dr_actype = '".$weekly_guest[$i]["ACtype"]."'
                           and dr_acreg = '".$weekly_guest[$i]["Reg"]."'";
            $exec = mysqli_query($conn_month, $sql_update);
            $update += 1;
          } else{
            $sql_insert = "insert into dr_final
                            (dr_date, dr_actype, dr_acreg, dr_total_rev, dr_total_delay, dr_total_cancel, dr_result, dr_range_type)
                            values
                            ('".$weekly_guest[$i]["MonthEval"]."',
                            '".$weekly_guest[$i]["ACtype"]."',
                            '".$weekly_guest[$i]["Reg"]."',
                            '".$weekly_guest[$i]["total_revenue"]."',
                            '".$weekly_guest[$i]["delay"]."',
                            '".$weekly_guest[$i]["cancel"]."',
                            '".floatval($weekly_guest[$i]["result"])."',
                            'weekly'
                            )";
             $exec = mysqli_query($conn_month, $sql_insert);
             $insert += 1;
          }
        }
         $log_week = "STAGING WEEKLY FROM ".$date1." TO ".$date2. " | TOTAL_DATA ".count($weekly_guest). ' | INSERT TOTAL : '.$insert.' | UPDATE TOTAL : '.$update. ' ';
         file_put_contents('C:\Application\Sched_DSS\log\log_dispatch_2.txt', $log_week.PHP_EOL , FILE_APPEND | LOCK_EX);
         echo $log_week."<br>";

    } else {
      echo "failed";
    }
    mysqli_query($conn_month ,"DROP TABLE temp_aircraftflightlog");

    //////////////////////////////////////////////////////////////// WEEKLY ////////////////////////////////////////////////////////////////






    //////////////////////////////////////////////////////////////// MONTHLY ////////////////////////////////////////////////////////////////
    $q1 = mysqli_query(
      $conn_month, "SELECT MonthEval,
                                       Reg,
                                       Actype,
                                       sum(RevFC) AS total_revenue,
                                       SUM(delay) AS delay,
                                       sum(cancel) AS cancel,
                                       CASE
                                      	WHEN SUM(RevFC) > 0 THEN (
                                      	(
                                      		(
                                      			SUM(RevFC) - (SUM(delay) + SUM(cancel))
                                      		) / SUM(RevFC)
                                      	) * 100)
                                      	ELSE 0
                                       END AS result
                                      FROM
                                      	(
                                      		SELECT
                                      			a.MonthEval,
                                      			a.Reg,
                                      			a.Actype,
                                      			sum(a.RevFC) AS RevFC,
                                      			0 AS delay,
                                      			0 AS cancel
                                      		FROM
                                      			tbl_monthlyfhfc a
                                      		WHERE
                                      			a.MonthEval BETWEEN '".$date1months."'
                                      		AND '".$date2."'
                                      		GROUP BY
                                      			a.MonthEval,
                                      			a.Actype,
                                      			a.Reg
                                      		UNION
                                      				SELECT
                                      				a.MonthEval,
                                      				a.Reg,
                                      				b.Actype,
                                      				0 AS RevFC,
                                      				count(b.DCP) AS delay,
                                      				0 AS cancel
                                      			FROM
                                      				tbl_monthlyfhfc a
                                      			JOIN mcdrnew b ON a.Reg = b.Reg
                                      			AND SUBSTR(a.MonthEval, '1', '7') = SUBSTR(b.DateEvent, '1', '7')
                                      			WHERE
                                      			a.MonthEval BETWEEN '".$date1months."'
                                      			AND '".$date2."'
                                      			AND b.DCP = 'D'
                                      			GROUP BY
                                      				a.MonthEval,
                                      				b.Actype,
                                      				b.Reg
                                      			UNION
                                      				SELECT
                                      					a.MonthEval,
                                      					a.Reg,
                                      					b.Actype,
                                      					0 AS RevFC,
                                      					0 AS delay,
                                      					count(b.DCP) AS cancel
                                      				FROM
                                      					tbl_monthlyfhfc a
                                      				JOIN mcdrnew b ON a.Reg = b.Reg
                                      				AND SUBSTR(a.MonthEval, '1', '7') = SUBSTR(b.DateEvent, '1', '7')
                                      				WHERE
                                      				a.MonthEval BETWEEN '".$date1months."'
                                      				AND '".$date2."'
                                      				AND b.DCP = 'C'
                                      				GROUP BY
                                      					a.MonthEval,
                                      					b.Actype,
                                      					b.Reg
                                      	) A
                                      GROUP BY
                                      	MonthEval,
                                      	Actype,
                                      	Reg
                                      ORDER BY
                                      	Reg DESC");
      $ter_monthly = array();
      $same_ter_monthly = array();
      while ($arr = mysqli_fetch_assoc($q1)) {
        $ter_monthly[] = $arr;
        $same_ter_monthly[] = array(
          "date" => $arr["MonthEval"],
          "acType" => $arr["Actype"],
          "Reg" => $arr["Reg"],
        );
      }

      $same_dss_monthly = array();
      $check_dss = "select dr_date, dr_actype, dr_acreg from dr_final where dr_date between '".$date1months."' and '".$date2."' and dr_range_type = 'monthly'";
      $exec = mysqli_query($conn_month, $check_dss);
      while ($arr = mysqli_fetch_assoc($exec)) {
        $same_dss_monthly[] = array(
          "date" => $arr["dr_date"],
          "acType" => $arr["dr_actype"],
          "Reg" => $arr["dr_acreg"]
        );
      }

      $insert = $update = 0;

      for ($i=0; $i < count($same_ter_monthly); $i++) {
        if(in_array($same_ter_monthly[$i], $same_dss_monthly)){
          $sql_update = "update dr_final
                         set dr_total_rev = ".$ter_monthly[$i]["total_revenue"].",
                         dr_total_delay = ".$ter_monthly[$i]["delay"].",
                         dr_total_cancel = ".$ter_monthly[$i]["cancel"].",
                         dr_result = ".$ter_monthly[$i]["result"]."
                         where dr_date = '".$ter_monthly[$i]["MonthEval"]."'
                         and dr_actype = '".$ter_monthly[$i]["Actype"]."'
                         and dr_acreg = '".$ter_monthly[$i]["Reg"]."'";
          $exec = mysqli_query($conn_month, $sql_update);
          $update += 1;
        } else{
          $sql_insert = "insert into dr_final
                          (dr_date, dr_actype, dr_acreg, dr_total_rev, dr_total_delay, dr_total_cancel, dr_result, dr_range_type)
                          values
                          ('".$ter_monthly[$i]["MonthEval"]."',
                          '".$ter_monthly[$i]["Actype"]."',
                          '".$ter_monthly[$i]["Reg"]."',
                          '".$ter_monthly[$i]["total_revenue"]."',
                          '".$ter_monthly[$i]["delay"]."',
                          '".$ter_monthly[$i]["cancel"]."',
                          '".$ter_monthly[$i]["result"]."',
                          'monthly'
                          )";
           $exec = mysqli_query($conn_month, $sql_insert);
           $insert += 1;
        }
      }

      $log = "STAGING MONTHLY FROM ".$date1months." TO ".$date2. " | TOTAL_DATA ".count($ter_monthly). ' | INSERT TOTAL : '.$insert.' | UPDATE TOTAL : '.$update. ' ';
      $myfile2 = file_put_contents('C:\Application\Sched_DSS\log\log_dispatch_2.txt', $log.PHP_EOL , FILE_APPEND | LOCK_EX);
      echo $log;
