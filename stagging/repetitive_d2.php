<?php 

ini_set('max_execution_time', 0);
ini_set('memory_limit', -1);

// DB_DBOARD connection
$host1 = '192.168.240.100';
$opsi1 = array('UID' => 'usr_dboard', 'PWD' => 'p@ssw0rd', 'database' => 'db_dboard');
$conn1 = sqlsrv_connect($host1, $opsi1);
if (!$conn1) {
	echo "Failed Connect DB_DBOARD \n";
} else {
	echo "Succeed Connect DB_DBOARD \n";
}

// DB_DSS connection
// $host2 = '192.168.240.107'; //dev
$host2 = '172.16.100.35'; // prod
$opsi2 = array('UID' => 'usr_dss', 'PWD' => 'p@ssw0rd', 'database' => 'db_dss');
$conn2 = sqlsrv_connect($host2, $opsi2);
if(!$conn2){
	echo "Failed Connect DB_DSS \n";
} else {
	echo "Succeed Connect DB_DSS \n";
}

$date = date('d-m-Y');

$dt_reps = "SELECT 
				qmnum AS notification,
				erdat AS created_on,
				erzeit AS created_at,
				tplnr AS funloc,
				strmn AS req_start,
				sttxt AS sys_status,
				qmdab AS completion,
				qmzab AS comp_time,
				aufnr AS order_number,
				qmtxt AS description
			FROM
				[dbo].[TBL_REP_PROBLEM]
			WHERE 
				qmart = 'D2'
				AND qmnum LIKE 'T%' 
				-- AND qmnum = 'T1223441'
				AND CONVERT(VARCHAR,update_date,105) = '{$date}'
			";
$dt_reps = sqlsrv_query($conn1, $dt_reps);
$dt_rep1 = array();
while ($dt_rep = sqlsrv_fetch_array($dt_reps)) {
    array_push($dt_rep1, $dt_rep);
}

print_r($dt_rep1);

$dt_nt_dss = "SELECT DISTINCT notification 
			FROM [dbo].[sap_repetitive_d2]";
$dt_nt_dss = sqlsrv_query($conn2, $dt_nt_dss);
$dt_nt_dss1 = array();
while ($dt_nt_ds = sqlsrv_fetch_array($dt_nt_dss)) {
    array_push($dt_nt_dss1, $dt_nt_ds['notification']);
}

$ins = $upd = 0;
$now = date('Y-m-d h:i:s');

for ($i = 0; $i < count($dt_rep1); $i++) {
    if (in_array($dt_rep1[$i]['notification'], $dt_nt_dss1)) {
		$dt_dss_upd = "
		UPDATE sap_repetitive_d2 
			SET system_status = '{$dt_rep1[$i]['sys_status']}',
			completion = '{$dt_rep1[$i]['completion']}',
			comp_time = '{$dt_rep1[$i]['comp_time']}',
			updated_on = $now
		WHERE 
			notification = '{$dt_rep1[$i]['notification']}';
		";
		$dt_dss_upd = sqlsrv_query($conn2, $dt_dss_upd);
		$upd += 1;
	} else {
		$description = str_replace("'", "''", $dt_rep1[$i]['description']);
        $dt_dss_ins = "INSERT INTO sap_repetitive_d2 
                    (order_number,
                    notification,
                    description,
                    system_status,
                    created_on,
                    created_at,
                    completion,
                    comp_time,
                    functional_location,
                    notif_date,
                    fault_code,
                    req_start,
                    updated_on) 
                    VALUES ('{$dt_rep1[$i]['order_number']}',
					'{$dt_rep1[$i]['notification']}',
					'{$description}',
					'{$dt_rep1[$i]['sys_status']}',
					'{$dt_rep1[$i]['created_on']}',
					'{$dt_rep1[$i]['created_at']}',
					'{$dt_rep1[$i]['completion']}',
					'{$dt_rep1[$i]['comp_time']}',
					'{$dt_rep1[$i]['funloc']}',
					null ,
					null ,
					'{$dt_rep1[$i]['req_start']}',
					'{$now}'
					)";

		$dt_dss_ins = sqlsrv_query($conn2, $dt_dss_ins);
		$ins += 1;
	}
}

$log = date("Y-m-d H:i:s") . ' | TOTAL DATA: ' . count($ii) . ' | INSERT DATA: ' . $ins . ' | UPDATE DATA: ' . $upd . ' '; 
echo $log;
echo file_put_contents('C:\Application\Sched_DSS\log\sink_repetitiveD2.txt', $log . "\r\n", FILE_APPEND | LOCK_EX);