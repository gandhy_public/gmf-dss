<?php 

ini_set('max_execution_time', 0);
ini_set('memory_limit', -1);

// DB_CRM_INFO connection
$host1 = '172.16.100.35'; // prod
$opsi = array('UID' => 'usr_dss', 'PWD' => 'p@ssw0rd', 'database' => 'db_crm_info');
$conn1 = sqlsrv_connect($host1, $opsi);
if(!$conn1){
	echo "Failed Connect DB_CRM_INFO \n";
} else {

	echo "Succeed Connect DB_CRM_INFO \n";
}

// DB_DSS connection
// $host2 = '192.168.240.107'; //dev
$host2 = '172.16.100.35'; // prod
$opsi2 = array('UID' => 'usr_dss', 'PWD' => 'p@ssw0rd', 'database' => 'db_dss');
$conn2 = sqlsrv_connect($host2, $opsi2);
if(!$conn2){
	echo "Failed Connect DB_DSS \n";
} else {
	echo "Succeed Connect DB_DSS \n";
}

$date1 = date("Ymd", strtotime("-1 days"));
$date2 = date("Ymd");

$dt_tat_air = "SELECT
					IWERK AS PI_PLANT,
					REVNR AS REVISION,
					TPLNR AS AIRCRAFT_REG,
					REVTX AS REVISION_DESCRIPTION,
					REVTY AS REVISION_TYPE,
					REVBD AS BASIC_START_DATE,
					REVED AS BASIC_FINISH_DATE,
					DATEDIFF( DAY, ( CASE WHEN ISDATE( REVBD ) = 1 THEN CAST ( REVBD AS DATE ) END ), 
						( CASE WHEN ISDATE( REVED ) = 1 THEN CAST ( REVED AS DATE ) END ) ) AS PLANT_TAT,
					UDATE_SMR AS ACT_START_DATE,
					ATSDATE_SMR AS ACT_FINISH_DATE,
					DATEDIFF( DAY, ( CASE WHEN ISDATE( UDATE_SMR ) = 1 THEN CAST ( UDATE_SMR AS DATE ) END ), 
						( CASE WHEN ISDATE( ATSDATE_SMR ) = 1 THEN CAST ( ATSDATE_SMR AS DATE ) END ) ) AS ACT_TAT,
					VAPLZ AS MAIN_WORK_CENTER,	
					VAWRK AS PLANT,
					TXT04 AS LAST_UPDATE
				FROM
					[dbo].[M_REVISION] 
				WHERE
					IWERK = '1000' 
					AND VAWRK IN ( 'GAH1', 'GAH2', 'GAH3', 'GAH4' )
					AND 
					(
						(UDATE_SMR BETWEEN '{$date1}' AND '{$date2}' OR ATSDATE_SMR BETWEEN '{$date1}' AND '{$date2}')
						OR 
						(REVBD BETWEEN '{$date1}' AND '{$date2}' OR REVED BETWEEN '{$date1}' AND '{$date2}')
						OR
						(AEDAT BETWEEN '{$date1}' AND '{$date2}')
					)
				";

$dt_tat_air = sqlsrv_query($conn1, $dt_tat_air);
while ($i = sqlsrv_fetch_array($dt_tat_air)) {
	$dt_tat[] = $i;
	$rev_tat[] = $i['REVISION'];
}
// print_r($dt_tat); exit();

$db_dss_tat = "SELECT DISTINCT REVISION FROM TAT_AIRFRAME";
$db_dss_tat = sqlsrv_query($conn2, $db_dss_tat);
while ($i = sqlsrv_fetch_array($db_dss_tat)) {
	$dt_dss_tat[] = $i['REVISION'];
}


$ins = $upd = 0;

for ($i=0; $i < count($rev_tat); $i++) { 
	if(in_array($rev_tat[$i], $dt_dss_tat)) {
		$db_dss_upd = "UPDATE TAT_AIRFRAME 
						SET 
							BASIC_START_DATE 	= '{$dt_tat[$i]['BASIC_START_DATE']}',
							BASIC_FINISH_DATE 	= '{$dt_tat[$i]['BASIC_FINISH_DATE']}',
							PLANT_TAT 			= '{$dt_tat[$i]['PLANT_TAT']}',
							ACT_START_DATE 		= '{$dt_tat[$i]['ACT_START_DATE']}',
							ACT_FINISH_DATE 	= '{$dt_tat[$i]['ACT_FINISH_DATE']}', 
							ACT_TAT 			= '{$dt_tat[$i]['ACT_TAT']}',
							MAIN_WORK_CENTER 	= '{$dt_tat[$i]['MAIN_WORK_CENTER']}',	
							PLANT 				= '{$dt_tat[$i]['PLANT']}',
							LAST_UPDATE			= '{$dt_tat[$i]['LAST_UPDATE']}',
							UPD_DATE 			= GETDATE()
						WHERE
							REVISION = '{$dt_tat[$i]['REVISION']}'; ";
		$db_dss_upd = sqlsrv_query($conn2, $db_dss_upd);
		$upd += 1;
	} else {
		$db_dss_ins = "INSERT INTO TAT_AIRFRAME 
			VALUES ('{$dt_tat[$i]['PI_PLANT']}',
					'{$dt_tat[$i]['REVISION']}',
					'{$dt_tat[$i]['REVISION_DESCRIPTION']}',
					'{$dt_tat[$i]['REVISION_TYPE']}',
					NULL,
					'{$dt_tat[$i]['AIRCRAFT_REG']}',
					'{$dt_tat[$i]['BASIC_START_DATE']}',
					'{$dt_tat[$i]['BASIC_FINISH_DATE']}',
					'{$dt_tat[$i]['PLANT_TAT']}',
					'{$dt_tat[$i]['ACT_START_DATE']}',
					'{$dt_tat[$i]['ACT_FINISH_DATE']}',
					'{$dt_tat[$i]['ACT_TAT']}',
					'{$dt_tat[$i]['MAIN_WORK_CENTER']}',
					'{$dt_tat[$i]['PLANT']}',
					'{$dt_tat[$i]['LAST_UPDATE']}', GETDATE(), NULL, NULL);" ;
		$db_dss_ins = sqlsrv_query($conn2, $db_dss_ins);
		$ins += 1;
	}
}

$log = date("Y-m-d H:i:s") . ' | TOTAL DATA: ' . count($dt_tat) . ' | INSERT DATA: ' . $ins . ' | UPDATE DATA: ' . $upd . ' '; 
echo $log;
echo file_put_contents('C:\Application\Sched_DSS\log\sink_logTAT_Airframe.txt', $log . "\r\n", FILE_APPEND | LOCK_EX);

?>