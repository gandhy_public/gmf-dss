<?php
//=================================== coba ==============================================\
// $hostname = '192.168.240.100';
// $opsi = array('UID' => 'usr_dboard', 'PWD' => 'p@ssw0rd', 'database' => 'db_dboard');
// $con = sqlsrv_connect($hostname, $opsi);
// if (!$con) {
//     echo "Gagal Koneksi sd";
// }else{
// 	echo 'berhasil';
// }

// $hostname_ = '192.168.240.107'; // dev
$hostname_ = '172.16.100.35'; // prod
$opsi_ = array('UID' => 'usr_dss', 'PWD' => 'p@ssw0rd', 'database' => 'db_dss');
$con_ = sqlsrv_connect($hostname_, $opsi_);
if (!$con_) {
    echo "Gagal Koneksi sd";
} else {
    echo 'berhasil';
}
//=================================== end coba ==========================================

$data = "select * from sap_repetitive_d5";
$data = sqlsrv_query($con_, $data);

$dt = array();
$banyak = 0;
$tambah = 0;
$ubah = 0;
$gagal = 0;

while ($row = sqlsrv_fetch_array($data)) {
    $banyak += 1;
    $d1 = $row['order_number'];
    $d2 = $row['notification'];
    $d3 = $row['description'];
    $d4 = $row['system_status'];
    $d5 = $row['created_on'];
    $d6 = $row['created_at'];
    $d7 = $row['completion'];
    $d8 = $row['comp_time'];
    $d9 = $row['functional_location'];
    $d10 = $row['notif_date'];
    $d11 = $row['fault_code'];
    $d12 = $row['req_start'];

    $month = date('m', strtotime($d5));

    $query = "select notification from repetitive_d5_final where notification={$row['notification']} AND MONTH(CONVERT(date, created_on, 104))='{$month}'";

    echo "<pre>";
    echo "query: " . $query;
    echo "</pre>";

    $cek = sqlsrv_query($con_, $query);
    $found = false;
    if (($cek = sqlsrv_query($con_, $query)) !== false) {
        while ($obj = sqlsrv_fetch_object($cek)) {
            $found = true;
            echo "obj:" . $obj->notification . '<br />';
        }
    }
    $cek = sqlsrv_fetch_array($cek);
    $cek = count($cek);
    if (!$found) {
        // echo "insert data";
        $insert = "INSERT INTO repetitive_d5_final
        (
            [order],notification,description,system_status,created_on,functional_location,notif_date,fault_code,req_start,ins_date
		) 
        VALUES
        (
            '{$d1}','{$d2}','{$d3}','{$d4}','{$d5}','{$d9}','{$d10}','{$d11}','{$d12}',GETDATE()
        )";

        $insert = sqlsrv_query($con_, $insert);
        if ($insert) {
            echo "-insert sukses-";
        } else {
            echo "-insert gagal-";
        }
        $tambah += 1;

    } else {
        // echo "update data";
        $update = "UPDATE repetitive_d5_final 
            SET [order]='{$d1}',
                description='{$d3}',
                system_status='{$d4}',
                created_on='{$d5}',
                functional_location='{$d9}',
                notif_date='{$d10}',
                fault_code='{$d11}',
                req_start='{$d12}',
                upd_date=GETDATE()
            WHERE notification='{$d2}' AND MONTH(CONVERT(date, created_on, 104))='{$month}'";

        $update = sqlsrv_query($con_, $update);
        // echo $update;

        if ($update) {
            echo "-update sukses-";
        } else {
            echo "-update gagal-";
        }

        $ubah += 1;
    }
// echo $row['notification']." - ".$row['system_status']."\n";
// echo "<pre>";
// print_r($row);
// echo "</pre>";
}
$gagal = $banyak - ($tambah + $ubah);


//===================================== MEMBUAT LOG DATA ===============================
$log = date("Y-m-d") . ' | TOTAL DATA: ' . $banyak . ' | INSERT DATA: ' . $tambah . ' | UPDATE DATA: ' . $ubah . ' FAILED: ' . $gagal;
echo file_put_contents('C:\Application\Sched_DSS\log\sink_user_log_repetitive.txt', $log . "\r\n", FILE_APPEND | LOCK_EX);
