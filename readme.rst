###################
GMF - DECISION SUPPORT SYSTEM
###################

computer program application that analyzes business data and presents
it so that users can make business decisions more easily.
It is an "informational application" (to distinguish it from an "operational application"
that collects the data in the course of normal business operation).
