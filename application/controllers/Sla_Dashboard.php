<?php
defined('BASEPATH') or exit('no direct script access allowed');

/**
 *
 */
class Sla_dashboard extends MY_Controller
{

    private $data_sla = array();


    function __construct()
    {
        # code...
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_menu");
        $this->load->model("M_Sla_CA");
        $this->load->helper("color");
    }


    public function index()
    {
        $data = array(
            "content" => "sla/v_sla_dashboard",
            "title" => "SLA Component Access",
            "small_tittle" => "",
            "breadcrumb" => [""],
            "menu" => $this->M_menu->tampil(),
            'dataRobbing' => $this->M_Sla_CA->countRobbing(),
            'acRegs' => $this->M_Sla_CA->acRegs(),
        );
        $this->load->view("layouts", $data);
    }

    private function getAllDataSla()
    {
      $data = $this->M_Sla_CA->get_all_sla();
      $this->data_sla = $data;
    }

    private function get_spesific_date($date, $type)
    {
        if (($timestamp = strtotime($date)) !== false) {
          $php_date = getdate($timestamp);
          $date = "";
          switch ($type) {
            case 'day':  $date = date("d", $timestamp);  break;
            case 'month':  $date = date("m", $timestamp);  break;
            case 'year':  $date = date("Y", $timestamp);  break;
            default: $date = false; break;
          }
          return $date;
        }
        else {
          return false;
        }
    }

    private function getAllDataSlaWhere($month, $group)
    {
      $datas = $this->data_sla;
      $result = array();
      $total_data_per_sl = 0;
      $total_data_delay = 0;
      foreach ($datas as $data) {
        if($this->get_spesific_date($data["posting_date"], "month") == $month){
          if($data["grouping"] == $group){
            if($data["status"] == "DELAY"){
                $total_data_delay++;
            }
            $total_data_per_sl++;
          }
        }
      }
      return array("data_per_sl" => $total_data_per_sl, "total_data_delay" => $total_data_delay);
    }

    // protected function convertNumberToMonth($numbers)
    // {
    //   $result = array();
    //   foreach ($numbers as $data) {
    //     $result[""]
    //   }
    // }

    public function getDataGrafikDash_new()
    {
      $this->getAllDataSla();
      $result = array();
      $months = range(1, date("m"));
      $result_sl1 = array(); $result_sl2 = array(); $result_sl3 = array(); $result_sl4 = array();
      foreach ($months as $month) {
        $sl4 = $this->getAllDataSlaWhere($month, "SL 4 72 Jam");
        $sl3 = $this->getAllDataSlaWhere($month, "SL 3 72 Jam");
        $sl2 = $this->getAllDataSlaWhere($month, "SL 2 24 Jam");
        $sl1 = $this->getAllDataSlaWhere($month, "SL 1 3 Jam");
        $result_per_sl1 = $sl1["data_per_sl"];
        $delay_per_sl1  = $sl1["total_data_delay"];
        $result_per_sl2 = $sl2["data_per_sl"];
        $delay_per_sl2  = $sl2["total_data_delay"];
        $result_per_sl3 = $sl3["data_per_sl"];
        $delay_per_sl3  = $sl3["total_data_delay"];
        $result_per_sl4 = $sl4["data_per_sl"];
        $delay_per_sl4  = $sl4["total_data_delay"];
        $result_sl1[]   = ($result_per_sl1 == 0) ? 0 : round((($result_per_sl1-$delay_per_sl1)/$result_per_sl1)*100, 2);
        $result_sl2[]   = ($result_per_sl2 == 0) ? 0 : round((($result_per_sl2-$delay_per_sl2)/$result_per_sl2)*100, 2);
        $result_sl3[]   = ($result_per_sl3 == 0) ? 0 : round((($result_per_sl3-$delay_per_sl3)/$result_per_sl3)*100, 2);
        $result_sl4[]   = ($result_per_sl4 == 0) ? 0 : round((($result_per_sl4-$delay_per_sl4)/$result_per_sl4)*100, 2);
      }
      echo json_encode(
        array(
          "months" => $months,
          "data" => array(
            "SL_4_72_Jam" => $result_sl4,
            "SL_3_72_Jam" => $result_sl3,
            "SL_2_24_Jam" => $result_sl2,
            "SL_1_3_Jam" => $result_sl1
          )
        )
      );
    }

    public function produceSumArray($datas)
    {
        $sumArray = array();

        foreach ($datas as $k => $subArray) {
            foreach ($subArray as $id => $value) {
                array_key_exists($id, $sumArray) ? $sumArray[$id] += $value : $sumArray[$id] = $value;
            }
        }

        foreach ($sumArray as $key => $hasilakhir) {
            $sumArray[$key] = (float)number_format((float)$hasilakhir / count($datas), 2, '.', '');
        }

        return $sumArray;
    }

    public function getDataGrafikDash()
    {
      ini_set("max_execution_time", 0);
        $input = array(
            'status' => $this->input->post('status'),
            'startPeriode' => $this->input->post('startPeriode'),
            'endPeriode' => $this->input->post('endPeriode'),
        );

        $categories = $this->M_Sla_CA->getNameMonth($input);

        $acregs = $this->M_Sla_CA->funcLocation();

        //general Grafik
        $sl1Alls = array();
        $sl2Alls = array();
        $sl3Alls = array();
        $sl4Alls = array();
        foreach ($acregs as $acreg) {
            $sl1 = $this->M_Sla_CA->getDataGrafik($input, 'SL1', $acreg);
            array_push($sl1Alls, $sl1);
            $sl2 = $this->M_Sla_CA->getDataGrafik($input, 'SL2', $acreg);
            array_push($sl2Alls, $sl2);
            $sl3 = $this->M_Sla_CA->getDataGrafik($input, 'SL3', $acreg);
            array_push($sl3Alls, $sl3);
            $sl4 = $this->M_Sla_CA->getDataGrafik($input, 'SL4', $acreg);
            array_push($sl4Alls, $sl4);
        }
        echo "<pre>";
          print_r($sl1Alls);
        echo "</pre>"; exit;
        $sl1Result = $this->produceSumArray($sl1Alls);
        $sl2Result = $this->produceSumArray($sl2Alls);
        $sl3Result = $this->produceSumArray($sl3Alls);
        $sl4Result = $this->produceSumArray($sl4Alls);

        $result = array(
            'categories' => $categories,
            'sl1' => $sl1Result,
            'sl2' => $sl2Result,
            'sl3' => $sl3Result,
            'sl4' => $sl4Result);

        echo json_encode($result);
    }

    public function getDetailGrafik()
    {
        $startPeriode = $this->input->post('startPeriode');
        $endPeriode = $this->input->post('endPeriode');

        $pecah1 = explode('-',  $startPeriode);
        $pecah2 = explode('-',  $endPeriode);

        if ($this->input->post('startPeriode') == null and $this->input->post('endPeriode') == null) {
            $startPeriode = $this->input->post('startPeriode');
            $endPeriode = $this->input->post('endPeriode');
        } else {
            $startPeriode = date('Y-m', strtotime('01-' . $this->input->post('startPeriode')));
            $endPeriode = date('Y-m', strtotime('01-' . $this->input->post('endPeriode')));
        }
        $input = array(
            'status' => $this->input->post('status'),
            'acregDefault' => $this->input->post('acregDefault'),
            'acregSearch' => $this->input->post('acregSearch'),
            'grafik' => $this->input->post('grafik'),
            'startPeriode' => $startPeriode,
            'endPeriode' => $endPeriode,
            'slType' => $this->input->post('slType')
        );

        $categories = $this->M_Sla_CA->getNameMonth($input);

        if ($input['acregSearch'][$input['grafik']] == null) {
            $acreg = $input['acregDefault'];
        } else {
            $acreg = $input['acregSearch'][$input['grafik']];
        }

        $sl1 = $this->M_Sla_CA->getDataGrafik($input, 'SL1', $acreg);
        $sl2 = $this->M_Sla_CA->getDataGrafik($input, 'SL2', $acreg);
        $sl3 = $this->M_Sla_CA->getDataGrafik($input, 'SL3', $acreg);
        $sl4 = $this->M_Sla_CA->getDataGrafik($input, 'SL4', $acreg);

        switch ($input['slType']) {
            case 'SL 1 3 Jam':
                $finalResult = array(
                    'sl1' => $sl1,
                    'sl2' => null,
                    'sl3' => null,
                    'sl4' => null
                );
                break;
            case 'SL 2 24 Jam':
                $finalResult = array(
                    'sl1' => null,
                    'sl2' => $sl2,
                    'sl3' => null,
                    'sl4' => null
                );
                break;
            case 'SL 3 72 Jam':
                $finalResult = array(
                    'sl1' => null,
                    'sl2' => null,
                    'sl3' => $sl3,
                    'sl4' => null
                );
                break;
            case 'SL 4 72 Jam':
                $finalResult = array(
                    'sl1' => null,
                    'sl2' => null,
                    'sl3' => null,
                    'sl4' => $sl4
                );
                break;
            default:
                $finalResult = array(
                    'sl1' => $sl1,
                    'sl2' => $sl2,
                    'sl3' => $sl3,
                    'sl4' => $sl4
                );
        }

        $result = array('categories' => $categories, 'acreg' => $acreg, 'series' => $finalResult);

        echo json_encode($result);

    }

    public function detailSlTable()
    {
        if ($this->input->post('startPeriode') == null and $this->input->post('endPeriode') == null) {
            $startPeriode = $this->input->post('startPeriode');
            $endPeriode = $this->input->post('endPeriode');
        } else {
            $startPeriode = date('Y-m', strtotime('01-' . $this->input->post('startPeriode')));
            $endPeriode = date('Y-m', strtotime('01-' . $this->input->post('endPeriode')));
        }

        $input = array(
            'acreg' => $this->input->post('acreg'),
            'startPeriode' => $startPeriode,
            'endPeriode' => $endPeriode,
            'slType' => $this->input->post('slType'),
            'week' => $this->input->post('week'),
        );

        $columns = array(
			0 => 'order_number',
            1 => 'func_location',
            2 => 'material',
            3 => 'posting_date',
            4 => 'time_req',
            5 => 'availableDay',
            6 => 'reservationTime',
            7 => 'grouping',
            8 => 'performance',
            9 => 'status',
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search,
            'order_column' => $order_column,
            'order_dir' => $order_dir,
        );

        $send = $this->M_Sla_CA->showSlTable($data, $input);

        echo $send;

    }

    function detailRobbingTable()
    {
        $month = $this->input->post('month');

        $list = $this->M_Sla_CA->get_datatables($month);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d-m-Y', strtotime($field->created_on));
            $row[] = $field->material;
            $row[] = $field->description;
            $row[] = $field->donor_func_loc;
            $row[] = $field->receive_func_loc;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_Sla_CA->count_all(),
            "recordsFiltered" => $this->M_Sla_CA->count_filtered($month),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
