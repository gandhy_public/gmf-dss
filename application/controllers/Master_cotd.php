<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Master_cotd extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("M_menu");
        $this->load->model("M_cotd");
        $this->load->model("M_solution_mitigasi");
        $this->load->model("M_view_cotd_mitigation");
    }

    function index()
    {
        $data = array(
            "content" => "ctd/master_cotd",
            "title" => "Master Data Contribution of Technical Delay",
            "small_tittle" => "",
            "breadcrumb" => ["Master Data Contribution of Technical Delay"],
            "target_garuda" => $this->M_cotd->get_val_garuda('PK'),
            "target_citylink" => $this->M_cotd->get_val_citylink('QG'),
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    public function data_list($p_start = "", $p_end = "")
    {
        if ($p_start == null and $p_end == null) {
            $p_start = '01-' . date('Y');
            $p_end = date('m-Y');
        }
        $target_garuda = $this->M_cotd->get_val_garuda('PK');
        $target_citylink = $this->M_cotd->get_val_citylink('QG');

        if ($p_start == "" || $p_start == 0 || $p_end == "" || $p_end == 0) {
            $list = $this->M_view_cotd_mitigation->get_datatables();
            $recordsGA = $this->M_view_cotd_mitigation->count_all("GA", $target_garuda->target_val);
            $recordsCT = $this->M_view_cotd_mitigation->count_all("CITILINK", $target_citylink->target_val);
            $recordsTotal = $recordsGA + $recordsCT;
            $recordsFiltered = $recordsTotal;
            // $recordsFiltered = $this->M_view_cotd_mitigation->count_filtered("cotd", $target_garuda->target_val);
        } else {
            $list = $this->M_view_cotd_mitigation->get_datatables($p_start, $p_end);
            $recordsGA = $this->M_view_cotd_mitigation->count_all("GA", $target_garuda->target_val, $p_start, $p_end);
            $recordsCT = $this->M_view_cotd_mitigation->count_all("CITILINK", $target_garuda->target_val, $p_start, $p_end);

            $recordsTotal = $recordsGA + $recordsCT;
            $recordsFiltered = $recordsTotal;
            // $recordsFiltered = $this->M_view_cotd_mitigation->count_filtered("cotd", $target_garuda->target_val, $p_start, $p_end);
        }

        $arrMonths = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $data = array();
        $no = $_POST['start'];
        $values = array();
        foreach ($list as $data) {
            if ($data->aircraft == "GA") {
                if ($data->actual >= $target_garuda->target_val) {
                    array_push($values, $data);
                }
            } else {
                if ($data->actual >= $target_citylink->target_val) {
                    array_push($values, $data);
                }
            }
        }

        $data = array();
        foreach ($values as $val) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $arrMonths[$val->bulan - 1] . " " . $val->tahun;
            $row[] = $val->aircraft;
            $row[] = $val->ac_type;
            if ($val->aircraft == "GA") {
                $row[] = $target_garuda->target_val;
            } else {
                $row[] = $target_citylink->target_val;
            }
            $row[] = number_format($val->actual, 6);

            $mitigasi = $this->M_solution_mitigasi->get_mitigasi($val->bulan, $val->tahun, $val->ac_type, 'cotd');

            $row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="ajax_edit(' . "'" . $val->bulan . "'" . ',' . "'" . $val->tahun . "'" . ',' . "'" . $val->ac_type . "'" . ')"><i class="glyphicon glyphicon-pencil"></i></a>';
            // <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="ajax_delete('."'".$val->target_id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            if (!empty($mitigasi)) {
                $row[] = true;
            } else {
                $row[] = false;
            }

            array_push($data, $row);
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        );

        $this->output->set_output(json_encode($output));
    }

    public function save()
    {
        $yearNow = date('Y');
        $sukses = FALSE;
        $message = "";

        if (!empty($this->input->post('terget_garuda'))) {
            $dataGaruda['target_val'] = $this->input->post('terget_garuda');
            $dataGaruda['target_year'] = $yearNow;
            $dataGaruda['target_type'] = 'COTD';
            $dataGaruda['target_aircraft'] = 'PK';

            $resultGaruda = $this->M_cotd->_emptyData($yearNow, 'COTD', 'PK');
            if (empty($resultGaruda)) {
                $this->M_cotd->save($dataGaruda);
                $sukses = TRUE;
                $message = "Insert Data Sukses";
            } else {
                $this->M_cotd->update(
                    array('target_id' => $resultGaruda[0]->target_id),
                    array('target_val' => $dataGaruda['target_val']));
                $sukses = TRUE;
                $message = "Update Data Sukses";
            }
        }

        if (!empty($this->input->post('terget_citylink'))) {
            $data['target_val'] = $this->input->post('terget_citylink');
            $data['target_year'] = $yearNow;
            $data['target_type'] = 'COTD';
            $data['target_aircraft'] = 'QG';

            $result = $this->M_cotd->_emptyData($yearNow, 'COTD', 'QG');
            if (empty($result)) {
                $this->M_cotd->save($data);
                $sukses = TRUE;
                $message = "Insert Data Sukses";
            } else {
                $this->M_cotd->update(
                    array('target_id' => $result[0]->target_id),
                    array('target_val' => $data['target_val']));
                $sukses = TRUE;
                $message = "Update Data Sukses";
            }
        }


        echo json_encode(array("sukses" => $sukses, "message" => $message));
    }

    public function save_modal()
    {
        $sukses = FALSE;
        $message = "";

        $data['mitigasi_why'] = $this->input->post('input_why');
        $data['mitigasi_solution'] = $this->input->post('input_solution');
        // $data['mitigasi_fk_id'] = "";

        if (empty($this->input->post('mitigasi_id'))) {
            $data['mitigasi_date'] = $this->input->post('mitigasi_date') . "-01 " . date('h:i:s');
            $data['mitigasi_aircraft'] = $this->input->post('mitigasi_aircraft');
            $data['mitigasi_type'] = "cotd";

            $id = $this->M_solution_mitigasi->save($data);
            if ($id) {
                $sukses = TRUE;
                $message = "Insert Data Sukses";
            } else {
                $sukses = FALSE;
                $message = "Insert Data Gagal";
            }
        } else {
            if (empty($this->input->post('input_why')) || $this->input->post('input_why') == "" && empty($this->input->post('input_solution')) || $this->input->post('input_solution') == "") {

                $status_ = $this->M_solution_mitigasi->delete($this->input->post('mitigasi_id'));

                $sukses = $status_;
                if ($status_) {
                    $message = "Delete Data Sukses";
                } else {
                    $message = "Delete Data Gagal";
                }
            } else {
                $this->M_solution_mitigasi->update(
                    array('mitigasi_id' => $this->input->post('mitigasi_id')),
                    $data);
                $sukses = TRUE;
                $message = "Update Data Sukses";
            }
        }

        echo json_encode(array("sukses" => $sukses, "message" => $message));
    }

    public function show_update()
    {
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $aircraft = $this->input->post('aircraft');

        $data['mitigasi'] = $this->M_view_cotd_mitigation->get_mitigasi($month, $year, $aircraft);

        $data['mitigasi_solution'] = $this->M_solution_mitigasi->get_mitigasi($month, $year, $aircraft, 'cotd');

        if (!empty($data['mitigasi_solution'])) {
            $data['edit'] = true;
        } else {
            $data['edit'] = false;
        }

        $this->output->set_output(json_encode($data));
    }

}
