<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kpi_unit_Dash extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->helper("date");
        $this->load->model("M_menu");
        $this->load->helper("hierarchyitem_helper");
        $this->load->helper("kpiformula");
        $this->load->model("M_kpi_unit_dashboard");
        $this->load->model("M_kpi_unit_temp");
        $this->load->model("M_kpi_unit");
        $this->load->model("M_kpi_corporate");
        $this->load->model("M_kpi_sla_master");
    }

    public function showAllRole()
    {
      $theRole = $this->M_kpi_unit->show_all_role();
      return $theRole;
    }

    function index()
    {
        $data = array(
            "content" => "kpi/V_dash_unit_kpi",
            "title" => "KPI Unit",
            "title_box" => "KPI Unit Dashboard",
            "small_tittle" => "",
            "breadcrumb" => ["KPI Unit"],
            "allRole" => $this->showAllRole(),
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }

    public function calculate()
    {
      $grups = $this->M_kpi_corporate->get_grup();
      $role = $this->input->get("role_id");
      $out = array();
      $totalScore = $this->get_all_total($role);
      foreach ($grups as $grup) {
        // $query = $this->M_kpi_unit_dashboard
        //         ->all_data_by_role(date("m"), date("Y"), $role, $grup->kpi_group_id, "grup");
        // $queryAll = $this->M_kpi_unit_dashboard
        //         ->all_data_by_role(date("m"), date("Y"), $role, $grup->kpi_group_id);
        // $content = $this->generatePageTreeTableActual($query, date("m"), date("Y"));
        
        $datak=$this->countOnBoardunit($grup->kpi_group_id,$role);

        
        $out[] = array(
          "group" => (array("name" => $grup->kpi_group_name, "data" => $datak)),
        );
      }
      echo json_encode(array("totalScore" => $totalScore ,"data"=>$out));
    }

    public function lastYear($maxLevel, $name,$role)
    {
      $lastYear = date("Y", strtotime(date("Y-m-d")." -1 year"));
      $date=date('m');
      $result=array();
    //   $dataMentah = $this->M_kpi_corporate->get($lastYear, date("m"), $id, "name");
      $kpiinstok=$this->M_kpi_corporate->cek_unit($name,$lastYear,$role);
      
      if(count($kpiinstok)!=0){
          
          $finalTotalScores=$this->CalculationTotal($maxLevel,$kpiinstok[0]->kia_id,$role,$date,$lastYear);
          $result['lastmonth']=$finalTotalScores['score'];
          $result['lastyear']=$finalTotalScores['scoreYtd'];

      }else{
          $result['lastmonth']=0;
          $result['lastyear']=0;
      }
      

    //   return $this->getActualLastYear($dataMentah, date("m"), $lastYear, $type);
    return $result;
    }

    public function trendChart($id)
    {
      $months = range(01, date("m"));
      foreach ($months as $month) {
        $role = $this->input->get("role_id");
        $dataMentah = $this->M_kpi_unit_dashboard->all_data_by_role($month, date("Y"), $role, $id, "single");
        $targetPecah = explode("|", $this->getChart($dataMentah, $month, date("Y"), "target"));
        if(count($targetPecah) > 1){
          $upper[] = floatval($targetPecah[0]);
          $lower[] = floatval($targetPecah[1]);
          $target = array(
            "isStabilize" => true,
            "upper_target" => $upper,
            "lower_target" => $lower
          );
        } else {
          $onlyTarget[] = floatval($targetPecah[0]);
          $target = array(
            "isStabilize" => false,
            "target" => $onlyTarget
          );
        }
        $actual[] = $this->getChart($dataMentah, $month, date("Y"), "actual");
        $actualYtd[] = $this->getChart($dataMentah, $month, date("Y"), "actualYtd");
      }
      return array("actual" => $actual, "actualYtd" => $actualYtd, "target" => $target);
    }

    // public function getChart($datas, $month, $nowYear, $type, $depth = 1, $parent = 0)
    // {
    //     if ($depth > 1000) return '';
    //     $deepChild = 1;
    //     $array = array();
    //     $target = array();
    //     $corporate = array();
    //     $out = array();
    //     for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
    //         if ($datas[$i]->kia_parent_id == $parent) {
    //             $target = explode("|", $datas[$i]->kiad_target);
    //             $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
    //             if ($this->M_kpi_unit_temp->getDeepChild($datas[$i]->kia_id, $datas[$i]->role_id) == 0) {
    //                 if ($datas[$i]->kia_parent_id == 0) {
    //                     if (in_array($datas[$i]->kia_name, $corporate)) {
    //                     } else {
    //                       if ($depth == "1") {
    //                           $weight1 = $this->M_kpi_unit_temp->countWeight($datas[$i]->kia_id, $nowYear, $month, $datas[$i]->role_id);
    //                       } else {
    //                           $weight1 = ($datas[$i]->kia_weight == null || empty($datas[$i]->kia_weight)) ? 0 : number_format($datas[$i]->kia_weight, 2);
    //                       }
    //                       $_actual = number_format(score(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight) * $weight1 / 100, 2);
    //                       $_achievment = number_format(score(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2);
    //                       $_actualYtd = number_format(score(achievment($datas[$i]->kiad_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight) * $weight1 / 100, 2);
    //                       $_achievmentYtd = number_format(score(achievment($datas[$i]->kiad_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2);
    //                       switch ($type) {
    //                         case 'actual':
    //                           $out = $datas[$i]->kiad_actual;
    //                           break;
    //                         case 'actualYtd':
    //                           $out = $datas[$i]->kiad_actual_ytd;
    //                           break;
    //                         case 'target':
    //                           $out =  (empty($datas[$i]->kiad_target) ? $datas[$i]->kia_target : $datas[$i]->kiad_target);
    //                           break;
    //                         default:
    //                           $out = false;
    //                           break;
    //                       }
    //                     }
    //                 }
    //             } else {
    //                 $maxLevel = $this->M_kpi_unit_temp->maxLevel($datas[$i]->kia_id, $datas[$i]->role_id);
    //                 if ($depth == "1") {
    //                     $weight = $this->M_kpi_unit_temp->countWeight($datas[$i]->kia_id, $nowYear, $month, $datas[$i]->role_id);
    //                 } else {
    //                     $weight = ($datas[$i]->kia_weight == null || empty($datas[$i]->kia_weight)) ? 0 : number_format($datas[$i]->kia_weight, 2);
    //                 }
    //                 if ($maxLevel != "1" && $depth != "1") {
    //                     $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $totalScore += number_format(($newTotal * $temp_id["weight"]) / 100, 2);
    //                         $totalScoreYtd += number_format(($newTotalYtd * $temp_id["weight"]) / 100, 2);
    //                     }
    //                 } elseif ($depth == "1" && $maxLevel != "1") {
    //                     $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $totalScore += number_format((($newTotal * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                     }
    //                 } else {
    //                     $totalScore = $this->M_kpi_unit_temp->getTotalScoreWhere($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
    //                     $totalScoreYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
    //                 }
    //                 $parentScore = number_format(($totalScore * $weight) / 100, 2);
    //                 $parentScoreYtd = number_format(($totalScoreYtd * $weight) / 100, 2);
    //                 if ($depth == 1) {
    //                   switch ($type) {
    //                     case 'actual':
    //                       $out = floatval($parentScore);
    //                       break;
    //                     case 'actualYtd':
    //                       $out = floatval($parentScoreYtd);
    //                       break;
    //                     case 'target':
    //                       $out =  (empty($datas[$i]->kiad_target) ? $datas[$i]->kia_target : $datas[$i]->kiad_target);
    //                       break;
    //                     default:
    //                       $out = false;
    //                       break;
    //                   }
    //                 }
    //             }
    //             $this->generatePageTreeTableActual($datas, $month, $nowYear, $depth + 1, $datas[$i]->kia_id);
    //         }
    //     }

    //     return $out;
    // }

    // public function getActualLastYear($datas, $month, $nowYear, $type, $depth = 1, $parent = 0)
    // {
    //     if ($depth > 1000) return '';
    //     $deepChild = 1;
    //     $array = array();
    //     $target = array();
    //     $corporate = array();
    //     $out = 0;
    //     for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
    //         if ($datas[$i]->kia_parent_id == $parent) {
    //             $target = explode("|", $datas[$i]->kiad_target);
    //             $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
    //             if ($this->M_kpi_unit_temp->getDeepChild($datas[$i]->kia_id, $datas[$i]->role_id) == 0) {
    //                 if ($datas[$i]->kia_parent_id == 0) {
    //                     if (in_array($datas[$i]->kia_name, $corporate)) {
    //                     } else {
    //                       if ($depth == "1") {
    //                           $weight1 = $this->M_kpi_unit_temp->countWeight($datas[$i]->kia_id, $nowYear, $month, $datas[$i]->role_id);
    //                       } else {
    //                           $weight1 = ($datas[$i]->kia_weight == null || empty($datas[$i]->kia_weight)) ? 0 : number_format($datas[$i]->kia_weight, 2);
    //                       }
    //                       $_actual = number_format(score(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight) * $weight1 / 100, 2);
    //                       $_achievment = number_format(score(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2);
    //                       $_actualYtd = number_format(score(achievment($datas[$i]->kiad_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight) * $weight1 / 100, 2);
    //                       $_achievmentYtd = number_format(score(achievment($datas[$i]->kiad_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2);
    //                       switch ($type) {
    //                         case 'monthly':
    //                           $out = $datas[$i]->kiad_actual;
    //                           break;
    //                         case 'year_to_date':
    //                           $out = $datas[$i]->kiad_actual_ytd;
    //                           break;
    //                         default:
    //                           $out = false;
    //                           break;
    //                       }
    //                     }
    //                 }
    //             } else {
    //                 $maxLevel = $this->M_kpi_unit_temp->maxLevel($datas[$i]->kia_id, $datas[$i]->role_id);
    //                 if ($depth == "1") {
    //                     $weight = $this->M_kpi_unit_temp->countWeight($datas[$i]->kia_id, $nowYear, $month, $datas[$i]->role_id);
    //                 } else {
    //                     $weight = ($datas[$i]->kia_weight == null || empty($datas[$i]->kia_weight)) ? 0 : number_format($datas[$i]->kia_weight, 2);
    //                 }
    //                 if ($maxLevel != "1" && $depth != "1") {
    //                     $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["kia_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["kia_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $totalScore += number_format(($newTotal * $temp_id["weight"]) / 100, 2);
    //                         $totalScoreYtd += number_format(($newTotalYtd * $temp_id["weight"]) / 100, 2);
    //                     }
    //                 } elseif ($depth == "1" && $maxLevel != "1") {
    //                     $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["kia_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["kia_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $totalScore += number_format((($newTotal * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                     }
    //                 } else {
    //                     $totalScore = $this->M_kpi_unit_temp->getTotalScoreWhere($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
    //                     $totalScoreYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
    //                 }
    //                 $parentScore = number_format(($totalScore * $weight) / 100, 2);
    //                 $parentScoreYtd = number_format(($totalScoreYtd * $weight) / 100, 2);
    //                 if ($depth == 1) {
    //                   switch ($type) {
    //                     case 'monthly':
    //                       $out = floatval($parentScore);
    //                       break;
    //                     case 'year_to_date':
    //                       $out = floatval($parentScoreYtd);
    //                       break;
    //                     default:
    //                       $out = false;
    //                       break;
    //                   }
    //                 }
    //             }
    //             $this->generatePageTreeTableActual($datas, $month, $nowYear, $depth + 1, $datas[$i]->kia_id);
    //         }
    //     }

    //     return $out;
    // }

    // function get_all_total_final($datas, $finalTotalScore=0, $depth = 1, $parent = 0)
    // {
    //     $month = $this->input->get("month");
    //     $nowYear = $this->input->get("year");

    //     if ($depth > 1000) return '';
    //     $deepChild = 1;
    //     $tree = '';
    //     $globalFinalScore = $finalTotalScore;
    //     for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
    //         if ($datas[$i]->kia_parent_id == $parent) {
    //             $target = explode("|", $datas[$i]->kiad_target);
    //             $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
    //             $actualYtd = (($datas[$i]->kiad_actual_ytd == null || empty($datas[$i]->kiad_actual_ytd)) ? "-" : $datas[$i]->kiad_actual_ytd);
    //             $tree .= "<tr data-id='" . $datas[$i]->kia_id . "' data-parent='" . $datas[$i]->kia_parent_id . "' data-level='" . $depth . "'> <td data-column='name'> L" . $depth . "&nbsp;&nbsp;-" . str_repeat('&nbsp;&nbsp;&nbsp;', $depth);
    //             if ($this->M_kpi_unit_temp->getDeepChild($datas[$i]->kia_id, $datas[$i]->role_id) == 0) {
    //                 $globalFinalScore += number_format(score(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2);
    //             } else {
    //                 $maxLevel = $this->M_kpi_unit_temp->maxLevel($datas[$i]->kia_id, $datas[$i]->role_id);
    //                 if ($depth == "1") {
    //                     $weight = $this->M_kpi_unit_temp->countWeight($datas[$i]->kia_id, $nowYear, $month, $datas[$i]->role_id);
    //                 } else {
    //                     $weight = ($datas[$i]->kia_weight == null || empty($datas[$i]->kia_weight)) ? 0 : number_format($datas[$i]->kia_weight, 2);
    //                 }
    //                 if ($maxLevel != "1" && $depth != "1") {
    //                     $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $totalScore += number_format(($newTotal * $temp_id["weight"]) / 100, 2);
    //                         $totalScoreYtd += number_format(($newTotalYtd * $temp_id["weight"]) / 100, 2);
    //                     }
    //                 } elseif ($depth == "1" && $maxLevel != "1") {
    //                     $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $totalScore += number_format((($newTotal * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                     }
    //                 } else {
    //                     $totalScore = $this->M_kpi_unit_temp->getTotalScoreWhere($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
    //                     $totalScoreYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
    //                 }
    //                 $parentScore = number_format(($totalScore * $weight) / 100, 2);
    //                 $parentScoreYtd = number_format(($totalScoreYtd * $weight) / 100, 2);
    //                 $globalFinalScore += $totalScore;
    //             }
    //             $this->get_all_total_final($datas, $globalFinalScore, $depth + 1, $datas[$i]->kia_id);
    //         }
    //     }
    //     return $globalFinalScore;
    // }

    // public function generatePageTreeTableActual($datas, $month, $nowYear, $depth = 1, $parent = 0)
    // {
    //     if ($depth > 1000) return '';
    //     $deepChild = 1;
    //     $array = array();
    //     $target = array();
    //     $corporate = array();
    //     for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
    //         if ($datas[$i]->kia_parent_id == $parent) {
    //             $target = explode("|", $datas[$i]->kiad_target);
    //             $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
    //             if ($this->M_kpi_unit_temp->getDeepChild($datas[$i]->kia_id, $datas[$i]->role_id) == 0) {
    //                 if ($datas[$i]->kia_parent_id == 0) {
    //                     if (in_array($datas[$i]->kia_name, $corporate)) {
    //                     } else {
    //                       if ($depth == "1") {
    //                           $weight1 = $this->M_kpi_unit_temp->countWeight($datas[$i]->kia_id, $nowYear, $month, $datas[$i]->role_id);
    //                       } else {
    //                           $weight1 = ($datas[$i]->kia_weight == null || empty($datas[$i]->kia_weight)) ? 0 : number_format($datas[$i]->kia_weight, 2);
    //                       }
    //                       $_actual = number_format(score(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight) * $weight1 / 100, 2);
    //                       $_achievment = number_format(score(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2);
    //                       $_actualYtd = number_format(score(achievment($datas[$i]->kiad_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight) * $weight1 / 100, 2);
    //                       $_achievmentYtd = number_format(score(achievment($datas[$i]->kiad_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2);
    //                       array_push($corporate, array(
    //                         "name" => $datas[$i]->kia_name,
    //                         "monthly" => array(
    //                           "actual" => $datas[$i]->kiad_actual,
    //                           "achievment" => $_achievment,
    //                           "target" => (empty($datas[$i]->kiad_target) ? $datas[$i]->kia_target : $datas[$i]->kiad_target),
    //                           "last_year_date" => $this->lastYear($datas[$i]->kia_name, "monthly")
    //                         ),
    //                         "ytd" => array(
    //                           "actual_ytd" => $datas[$i]->kiad_actual_ytd,
    //                           "achievment_ytd" => $_achievmentYtd,
    //                           "target_ytd" => (empty($datas[$i]->kiad_target_ytd) ? $datas[$i]->kia_target_ytd : $datas[$i]->kiad_target_ytd),
    //                           "last_year_date" => $this->lastYear($datas[$i]->kia_name, "year_to_date")
    //                         ),
    //                         "chart" => $this->trendChart($datas[$i]->kia_id)
    //                       ));
    //                     }
    //                 }
    //             } else {
    //                 $maxLevel = $this->M_kpi_unit_temp->maxLevel($datas[$i]->kia_id, $datas[$i]->role_id);
    //                 if ($depth == "1") {
    //                     $weight = $this->M_kpi_unit_temp->countWeight($datas[$i]->kia_id, $nowYear, $month, $datas[$i]->role_id);
    //                 } else {
    //                     $weight = ($datas[$i]->kia_weight == null || empty($datas[$i]->kia_weight)) ? 0 : number_format($datas[$i]->kia_weight, 2);
    //                 }
    //                 if ($maxLevel != "1" && $depth != "1") {
    //                     $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $totalScore += number_format(($newTotal * $temp_id["weight"]) / 100, 2);
    //                         $totalScoreYtd += number_format(($newTotalYtd * $temp_id["weight"]) / 100, 2);
    //                     }
    //                 } elseif ($depth == "1" && $maxLevel != "1") {
    //                     $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
    //                         $totalScore += number_format((($newTotal * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                     }
    //                 } else {
    //                     $totalScore = $this->M_kpi_unit_temp->getTotalScoreWhere($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
    //                     $totalScoreYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
    //                 }
    //                 $parentScore = number_format(($totalScore * $weight) / 100, 2);
    //                 $parentScoreYtd = number_format(($totalScoreYtd * $weight) / 100, 2);
    //                 if ($depth == 1) {

    //                     if (in_array($datas[$i]->kia_name, $corporate)) {
    //                     } else {
    //                       array_push($corporate, array(
    //                           "name" => $datas[$i]->kia_name,
    //                           "monthly" => array(
    //                             "actual" => $parentScore,
    //                             "achievment" => $totalScore,
    //                             "target" => (empty($datas[$i]->kia_target) ? $datas[$i]->kiad_target : $datas[$i]->kia_target),
    //                             "last_year_date" => $this->lastYear($datas[$i]->kia_name, "monthly")
    //                           ),
    //                           "ytd" => array(
    //                             "actual_ytd" => $parentScoreYtd,
    //                             "achievment_ytd" => $totalScoreYtd,
    //                             "target_ytd" => (empty($datas[$i]->kia_target_ytd) ? $datas[$i]->kiad_target_ytd : $datas[$i]->kia_target_ytd),
    //                             "last_year_date" => $this->lastYear($datas[$i]->kia_name, "year_to_date")
    //                           ),
    //                           "chart" => $this->trendChart($datas[$i]->kia_id)
    //                       ));
    //                     }
    //                 }
    //             }
    //             $this->generatePageTreeTableActual($datas, $month, $nowYear, $depth + 1, $datas[$i]->kia_id);
    //         }
    //     }
    //     return $corporate;
    // }

    
    public function get_all_total($role_id)
    {

      $year= date('Y');
      $month = date('m');
    //   $role_id = $this->input->get("role_id");
      $dataMentah = $this->M_kpi_sla_master->getZerosParent_unit($year,$role_id);
    //   echo json_encode($dataMentah);exit;
      $loops=0;
      $temp=0;
      foreach ($dataMentah as $key ) {
          # code...
          $maxLevel = $this->M_kpi_sla_master->maxLevel_unit($key['kia_id']);
          $finalTotalScores=$this->CalculationTotal($maxLevel,$key['kia_id'],$key['role_id'],$month,$year);
          $temp+=$finalTotalScores['score'];
          $loops+=1;
      }
      
    //   $content =number_format(($temp/$loops),2);
    return number_format($temp,2);
    //   echo json_encode(array("total" => number_format($temp,2)));
    
    }

    public function countOnBoardunit($id,$role){
        $corporate = array();
        $year=date('Y');
        $mon=date('m');
        $totalScore=0;
        $kpisearch=$this->M_kpi_corporate->findKpiUnitByGroup($role,$id,$year);
        

        if(count($kpisearch)!=0){

            foreach ($kpisearch as $keys ) {
                # code...
                $maxLevel_ga = $this->M_kpi_sla_master->maxLevel_unit($keys->kia_id);
                $finalTotalScores_ga=$this->CalculationTotal($maxLevel_ga,$keys->kia_id,$role,$mon,$year);

                $targetPecah = explode("|",$keys->kia_target);
                if(count($targetPecah)>1){
                    $target = array(
                              "isStabilize" => true);
                }else{
                    $target = array(
                            "isStabilize" => false);
                }

                $totalScore+=$finalTotalScores_ga['score'];
                $last=$this->lastYear($maxLevel_ga, $keys->kia_name,$role);
              
                 array_push($corporate, array(
                                "name" => $keys->kia_name,
                                "monthly" => array(
                                  "actual" => number_format($finalTotalScores_ga['score'],2),
                                  "achievment" => number_format($finalTotalScores_ga['score']-$keys->kia_target,2),
                                //   "achievment" => $finalTotalScores_ga['acv'],
                                  "target" => $keys->kia_target,
                                  "last_year_date" => number_format($last['lastmonth'],2),
                                ),
                                "ytd" => array(
                                  "actual_ytd" => number_format($finalTotalScores_ga['scoreYtd'],2),
                                  "achievment_ytd" => number_format($finalTotalScores_ga['scoreYtd']- $keys->kia_target_ytd,2),
                                //   "achievment_ytd" => $finalTotalScores_ga['acvYtd'],
                                  "target_ytd" => $keys->kia_target_ytd,
                                  "last_year_date" => number_format($last['lastyear'],2),
                                ),"chart" => array('target'=>$target)));
            }

        }
        // echo json_encode($kpisearch[0]->ki_id);exit;
        // array_push($corporate,array('totalScore'=>$totalScore));


        return $corporate;
    }

    

    public function CalculationTotal($maxLevel,$ki_id,$role_id,$month,$year)
    {
        # code...
                    
                    $AcvTemp=array();
                    $AcvTempYtd=array();
                    $arrayTemp=array();
                    $parentArr=array();
                    $uppers=array();
                    $lowers=array();
                    $uppersYtd=array();
                    $lowersYtd=array();
                    
                    
                    if($maxLevel==0){
                        $oke=0;$okeYtd=0;$acvment=0;$acvmentYtd=0;
                        $dataM = $this->M_kpi_sla_master->listActualWhere_unitz($ki_id,$role_id,$month,$year);$z=0;
                         $acvment+=number_format(achievment($dataM[$z]->kiad_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name),2);
                         $acvmentYtd+=number_format(achievment($dataM[$z]->kiad_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name),2);
                         $oke+=number_format(score(achievment($dataM[$z]->kiad_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2);
                         $okeYtd+=number_format(score(achievment($dataM[$z]->kiad_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2) ;
                        //  return $finalTotalScore= array('score'=>$oke,'scoreYtd'=>$okeYtd);
                         if($dataM[0]->fa_function_name=="Stabilize"){
                            $targets=explode("|",$dataM[0]->kia_target);
                            $targetsYtd=explode("|",$dataM[0]->kia_target_ytd);
                            array_push($uppers,$targets[0]);
                            array_push($lowers,$targets[1]);
                            array_push($uppersYtd,$targetsYtd[0]);
                            array_push($lowersYtd,$targetsYtd[1]);
                            return $finalTotalScore= array('score'=>$tempTotal,'scoreYtd'=>$tempTotalYtd,'target'=>array('up'=>$uppers,'low'=>$lowers),'targetYtd'=>array('upYtd'=>$uppersYtd,'lowYtd'=>$lowersYtd),'acv'=>$acvment,'acvYtd'=> $acvmentYtd);
                        }else{
                         return $finalTotalScore= array('score'=>$oke,'scoreYtd'=>$okeYtd,'target'=>(float)$dataM[0]->kia_target,'targetYtd'=>(float)$dataM[0]->kia_target_ytd,'acv'=>$acvment,'acvYtd'=> $acvmentYtd);
                        }
                         exit;
                    }

                    $dataZ = $this->M_kpi_sla_master->listActualWhere_unitz($ki_id,$role_id,$month,$year);
                    for ($k=$maxLevel; $k >=1 ; $k--) { 
                        # code...

                        $items = $this->M_kpi_sla_master->getPenghuni_unit($k,$ki_id);                        
                        $parent = $this->M_kpi_sla_master->getGroupLevelParent_unit($k,$ki_id);                     
                       
                       
                        if($k==$maxLevel){
                            
                             foreach ($parent as $key ) {
                                # code...
                                $tempTotalYtd=0;
                                $tempTotal=0;
                                $acvment=0;
                                $acvmentYtd=0;
                                for ($p=0; $p < count($items) ; $p++) { 
                                    # code...
                                    if($items[$p]['kia_parent_id']==$key['kia_parent_id']){
                                       $dataM = $this->M_kpi_sla_master->listActualWhere_unitz($items[$p]['kia_id'],$role_id,$month,$year);
                                    //    echo json_encode($dataM);exit;
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                           $acvment+=number_format(achievment($dataM[$z]->kiad_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name),2);
                                           $acvmentYtd+=number_format(achievment($dataM[$z]->kiad_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name),2);
                                           $tempTotal+=number_format(score(achievment($dataM[$z]->kiad_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2);
                                           $tempTotalYtd+=number_format(score(achievment($dataM[$z]->kiad_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2) ;
                                       }
                                    }
                                }
                                $arrayTemp[$key['kia_parent_id']] = $tempTotal;
                                $parentArr[$key['kia_parent_id']] = $tempTotalYtd;
                                $AcvTemp[$key['kia_parent_id']] = $acvment;
                                $AcvTempYtd[$key['kia_parent_id']] = $acvmentYtd;
                            }

                        }
                        else{
                            $tempFuck=0;
                            $tempFuckYtd=0;
                            $tempAcv=0;
                            $tempAcvYtd=0;

                            foreach ($parent as $keys ) {
                                # code...
                                for ($q=0; $q < count($items) ; $q++) { 
                                    if (array_key_exists($items[$q]['kia_id'],$arrayTemp) && $items[$q]['kia_parent_id']==$keys['kia_parent_id']){
                                        $tempFuck+=$arrayTemp[$items[$q]['kia_id']]*($items[$q]['kia_weight']/100);
                                        $tempFuckYtd+=$parentArr[$items[$q]['kia_id']]*($items[$q]['kia_weight']/100);
                                        $tempAcv+=$AcvTemp[$items[$q]['kia_id']]*($items[$q]['kia_weight']/100);
                                        $tempAcvYtd+=$AcvTempYtd[$items[$q]['kia_id']]*($items[$q]['kia_weight']/100);
                                    }elseif( $items[$q]['kia_parent_id']==$keys['kia_parent_id']){
                                       $dataM = $this->M_kpi_sla_master->listActualWhere_unitz($items[$q]['kia_id'],$role_id,$month,$year);
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                           $acvment+=number_format(achievment($dataM[$z]->kiad_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name),2);
                                           $acvmentYtd+=number_format(achievment($dataM[$z]->kiad_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name),2);
                                           $tempFuck+=number_format(score(achievment($dataM[$z]->kiad_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2);
                                           $tempFuckYtd+=number_format(score(achievment($dataM[$z]->kiad_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2) ;
                                       }
                                    }
                                }
                               
                                $arrayTemp[$keys['kia_parent_id']] = $tempFuck;
                                $parentArr[$keys['kia_parent_id']] = $tempFuckYtd;
                                $AcvTemp[$keys['kia_parent_id']] = $tempAcv;
                                $AcvTempYtd[$keys['kia_parent_id']] = $tempAcvYtd;
                                $tempFuck=0;
                                $tempFuckYtd=0;
                                $tempAcv=0;
                                $tempAcvYtd=0;
                                
                            }

                            

                        }
                        
                    }
                    if($dataZ[0]->fa_function_name=="Stabilize"){
                            $targets=explode("|",$dataZ[0]->kia_target);
                            $targetsYtd=explode("|",$dataZ[0]->kia_target_ytd);
                            array_push($uppers,$targets[0]);
                            array_push($lowers,$targets[1]);
                            array_push($uppersYtd,$targetsYtd[0]);
                            array_push($lowersYtd,$targetsYtd[1]);
                            return $finalTotalScore= array('score'=>$arrayTemp[$ki_id],'scoreYtd'=>$parentArr[$ki_id],'target'=>array('up'=>$uppers,'low'=>$lowers),'targetYtd'=>array('upYtd'=>$uppersYtd,'lowYtd'=>$lowersYtd),'acv'=>$AcvTemp[$ki_id],'acvYtd'=> $AcvTempYtd[$ki_id]);
                        }else{

                            return $finalTotalScore= array('score'=>$arrayTemp[$ki_id],'scoreYtd'=>$parentArr[$ki_id],'target'=>(float)$dataZ[0]->kia_target,'targetYtd'=>(float)$dataZ[0]->kia_target_ytd,'acv'=>$AcvTemp[$ki_id],'acvYtd'=> $AcvTempYtd[$ki_id]);
                        }

                    // return $finalTotalScore= array('score'=>$arrayTemp[$ki_id],'scoreYtd'=>$parentArr[$ki_id]);
    }


    /// Grafik 
    public function getGrafikValue(){
        $nameItemKpi=$this->input->post('names',true);
        $role_id=$this->input->post('role_id',true);
        // echo json_encode($_POST);exit;
        // $nameItemKpi="Finance Corporate";
        $startMonth=1;
        $lastMonth=date("m");
        $year=date('Y');

        $arrMonths = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        $kpi_item=$this->M_kpi_corporate->getItemKpiUnitbyName($nameItemKpi,$year,$role_id);
        

        $print['actual']=array();
        $print['actualYtd']=array();
        $print['target']=array();
        $print['targetYtd']=array();

        $maxLevel = $this->M_kpi_sla_master->maxLevel_unit($kpi_item->kia_id);
        $dataZ = $this->M_kpi_sla_master->listActualWhere_unitz($kpi_item->kia_id,$role_id,$lastMonth,$year);
        
        // echo json_encode($dataZ[0]->fa_function_name);exit;
        for ($i=$startMonth; $i <=$lastMonth ; $i++) { 
            # code...
            $bln=$arrMonths[($i-1)]."-".date('Y');

            $finalTotalScores=$this->CalculationTotal($maxLevel,$kpi_item->kia_id,$role_id,$i,$year);

            array_push($print['actual'],$finalTotalScores['score']);
            array_push($print['actualYtd'],$finalTotalScores['scoreYtd']);
            array_push($print['target'],$finalTotalScores['target']);
            array_push($print['targetYtd'],$finalTotalScores['targetYtd']);
            
        }
        if($dataZ[0]->fa_function_name=="Stabilize"){
            $upArr=array();
            $lowArr=array();
            $upArrYtd=array();
            $lowArrYtd=array();
            
            for ($i=0; $i < count($print['target']); $i++) { 
                # code...
                array_push($upArr,(float)$print['target'][$i]['up'][0]);
                array_push($lowArr,(float)$print['target'][$i]['low'][0]);
                array_push($upArrYtd,(float)$print['targetYtd'][$i]['upYtd'][0]);
                array_push($lowArrYtd,(float)$print['targetYtd'][$i]['lowYtd'][0]);
            }
            $print['target']=array('up'=>$upArr,'low'=>$lowArr);
            $print['targetYtd']=array('upYtd'=>$upArrYtd,'lowYtd'=>$lowArrYtd);
        }
        echo json_encode($print);
    }

}
