<?php
/**
 * Created by PhpStorm.
 * User: ucup
 * Date: 5/7/18
 * Time: 1:40 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('memory_limit', '1024M'); // or you could use 1G
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
ob_implicit_flush(true);

class Sla_Upload extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_Sla_CA");
        $this->load->library('form_validation');

        $this->load->helper("file");
    }

    public function index($error = NULL)
    {
        $data = array(
            "content" => "sla/v_sla_upload",
            "title" => "SLA Component Access Upload",
            "small_tittle" => "",
            "breadcrumb" => ["SLA Component Access Upload"],
            "menu" => $this->M_menu->tampil(),
            'action' => site_url('upload/proses'),
            'judul' => set_value('judul'),
            'error' => $error['error'] // ambil parameter error
        );
        $this->load->view("layouts", $data);
    }

    public function tableGroupSLACA()
    {

        $columns = array(
            0 => 'material',
            1 => 'grouping',
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search,
            'order_column' => $order_column,
            'order_dir' => $order_dir,
        );

        $send = $this->M_Sla_CA->showDatatable($data);

        echo $send;

    }

    public function uploadFile()
    {
        $this->load->library('PHPExcel');

        $path = 'assets/upload-slaca/';

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'xlsx|xls|svg';
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('files')) {
            $data = array('upload_data' => $this->upload->data());
        } else {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
            exit();
        }
//        echo "Data: " . json_encode($data) . "\n";
        if (!empty($data['upload_data']['file_name'])) {
            $import_xls_file = $data['upload_data']['file_name'];
        } else {
            $import_xls_file = 0;
        }
//        echo "File Name: " . $import_xls_file . "\n";
        $inputFileName = $path . $import_xls_file;
//        echo "Path File Name:" . $inputFileName . "\n";
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
            $dataOut['sukses'] = true;
        } catch (Exception $e) {
            if (!empty($inputFileName)) {
                unlink($inputFileName);
            }
            $dataOut['sukses'] = false;
            $dataOut['message'] = "Error Read Data";
            echo json_encode($dataOut);
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                . '": ' . $e->getMessage());
        }
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

        $arrayCount = count($allDataInSheet);
        $flag = 0;

        $createArray = array('Material', 'Grouping');

        $SheetDataKey = array();
        foreach ($allDataInSheet as $dataInSheet) {
            foreach ($dataInSheet as $key => $value) {
                if (in_array(trim($value), $createArray)) {
                    $SheetDataKey[trim($value)] = $key;
                }
            }
        }

        $makeArray = array('Material' => 'Material', 'Grouping' => 'Grouping');

        $data = array_diff_key($makeArray, $SheetDataKey);

        // echo "<pre>"; print_r($allDataInSheet); echo "</pre>"; exit;

        if (empty($data)) {
            $flag = 1;
        }
        $fetchData = array();
        if ($flag == 1) {
            for ($i = 2; $i <= $arrayCount; $i++) {
                $material = $SheetDataKey['Material'];
                $grouping = $SheetDataKey['Grouping'];
                $material = filter_var(trim($allDataInSheet[$i][$material]), FILTER_SANITIZE_STRING);
                $grouping = filter_var(trim($allDataInSheet[$i][$grouping]), FILTER_SANITIZE_STRING);

                if (strpos(strtolower($grouping), "unknown") === false) {
                    $fetchData[] = array(
                        'material' => $material,
                        'grouping' => $grouping);
                }
            }

            $data['data_mro'] = $fetchData;
            if ($dataOut['sukses']) {
                $this->M_Sla_CA->insertData($fetchData);
                $dataOut['message'] = "Sukses";
            }
        } else {
            echo "Please import correct file";
        }

        if (!empty($inputFileName)) {
            unlink($inputFileName);
        }

        echo json_encode($dataOut);
    }

    public function exportExcel()
    {
        // $data = array(
        //     'title' => 'Format Upload Data SLA Component Access',
        //     'sccGroups' => $this->M_Sla_CA->getSccGroups()
        // );

        $list = $this->M_Sla_CA->getSccGroups();

        $heading = array('Material', 'Grouping');
        $this->load->library('PHPExcel');
        //Create a new Object
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setTitle('Data TAT Component Access');
        //Loop Heading
        $rowNumberH = 1;
        $colH = 'A';
        foreach ($heading as $h) {
            $objPHPExcel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
            $colH++;
        }
        //Loop Result
        $totn = count($list);
        $maxrow = $totn + 1;
        $row = 2;
        foreach ($list as $key) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $key->material);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $key->grouping);
            $row++;
        }
        //Freeze pane
        $objPHPExcel->getActiveSheet()->freezePane('A2');
        //Cell Style
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:B' . $maxrow)->applyFromArray($styleArray);
        //Save as an Excel BIFF (xls) file


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Format Upload Data TAT Component Access.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        ob_start();
        $objWriter->save('php://output');
        exit;

        // $this->load->view('sla/export_example_xls', $data);
    }

}
