<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 */
class C_upload_sla_avability extends MY_Controller
{

    public function __construct()
    {
        # code...
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_menu");
        $this->load->model("M_sla_component_avability");
    }

    //==============================================================================
    public function index()
    {
        # code...
        $data = array(
            "content" => "SLA Component Availability/v_upload_sla_avability",
            "title" => "SLA Component Availability",
            "small_tittle" => "",
            "breadcrumb" => ["SLA Component Availability"],
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }
    //==============================================================================

    //===================================Store Files===========================================
    public function storefiles()
    {
        // code...
        $this->load->library('PHPExcel');

        // Type Chossen
        $template = $this->input->post('tipe_template', true);

        // Destintion Folder tujuan
        $destination_folder = 'assets/upload/excel_files';
        $new_name_file = date("Ymd_His") . '_' . $_FILES['files']['name'];

        $file = "$destination_folder/$new_name_file";


        // insert Data base
        move_uploaded_file($_FILES['files']['tmp_name'], $file);
        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        //get only the Cell Collection
        // $cell_collection = $objPHPExcel->getActiveSheet(0)->getCellCollection();
        $cell_collection = $objPHPExcel->setActiveSheetIndex(0)->getCellCollection();

        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            //The header will/should be in row 1 only. of course, this can be modified to suit your need
            $arr_data[$row][$column] = $data_value;
        }

        //send the data in an array format
        $data['values'] = $arr_data;

        unlink($file);

        // print_r($data);exit();

        if ($template == "database_pn" && count($data['values'][1]) == 10) {
            // code...
            if ($this->data_pn($data)) {
                // code...
                $data_ret = array(
                    "data" => 'success',
                    "msg" => 'Sukses Upload Data'
                );
            } else {
                $data_ret = array(
                    "data" => 'error',
                    "msg" => 'Upload Data Error'
                );
            }
            echo json_encode($data_ret);
        } elseif ($template == "database_mtype" && count($data['values'][1]) == 2) {
            if ($this->data_mtype($data)) {
                // code...
                $data_ret = array(
                    "data" => 'success',
                    "msg" => 'Sukses Upload Data'
                );
            } else {
                $data_ret = array(
                    "data" => 'error',
                    "msg" => 'Upload Data Error'
                );
            }
            echo json_encode($data_ret);
        } else {
            $data_ret = array(
                "data" => 'warning',
                "msg" => 'Pilih Template yang sesuai'
            );
            echo json_encode($data_ret);
        }

    }


    function data_pn($data)
    {
        // code...
        $make_array = array();
        for ($i = 2; $i <= count($data['values']); $i++) {
            // code...
            $temp = array("pnreg" => filter_var(trim($data['values'][$i]['A']), FILTER_SANITIZE_STRING),
                "ess" => filter_var(trim($data['values'][$i]['B']), FILTER_SANITIZE_STRING),
                "part_number" => filter_var(trim($data['values'][$i]['C']), FILTER_SANITIZE_STRING),
                "alternate" => filter_var(trim($data['values'][$i]['D']), FILTER_SANITIZE_STRING),
                "description" => filter_var(trim($data['values'][$i]['E']), FILTER_SANITIZE_STRING),
                "cap" => filter_var(trim($data['values'][$i]['F']), FILTER_SANITIZE_STRING),
                "skema" => filter_var(trim($data['values'][$i]['H']), FILTER_SANITIZE_STRING),
                "aircraft" => filter_var(trim($data['values'][$i]['G']), FILTER_SANITIZE_STRING),
                "tot_float_spare" => filter_var(trim($data['values'][$i]['I']), FILTER_SANITIZE_STRING),
                "remarks" => filter_var(trim($data['values'][$i]['J']), FILTER_SANITIZE_STRING));
            array_push($make_array, $temp);
        }
        // echo json_encode($make_array);exit();
        $sql = $this->M_sla_component_avability->save_pn($make_array);
        return $sql;
    }

    public function test()
    {
        // code...
        // $arrayName = array({"pnreg":"87292325V04A320","ess":"2","part_number":"87292325V04","alternate":"","description":"AEVC-AVIONICS EQUIPMENT VENTILATION COMPUTER","cap":"NO","skema":"A320","aircraft":"PBTH AVAILABILITY","tot_float_spare":"0","remarks":""});
    }

    function data_mtype($data)
    {
        // code...
        $make_array = array();
        for ($i = 2; $i <= count($data['values']); $i++) {
            // code...
            $temp = array('part_number' => filter_var(trim($data['values'][$i]['A']), FILTER_SANITIZE_STRING),
                'mtype' => filter_var(trim($data['values'][$i]['B']), FILTER_SANITIZE_STRING));
            array_push($make_array, $temp);
        }
        $sql = $this->M_sla_component_avability->save_mtype($make_array);
        return $sql;
    }

    public function datatable_pn()
    {
        // code...
        $list = $this->M_sla_component_avability->get_datatables_pn();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->pnreg;
            $row[] = $val->ess;
            $row[] = $val->part_number;
            $row[] = $val->alternate;
            $row[] = $val->description;
            $row[] = $val->cap;
            $row[] = $val->aircraft;
            $row[] = $val->skema;
            $row[] = $val->tot_float_spare;
            $row[] = $val->remarks;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_sla_component_avability->count_all_pn(),
            "recordsFiltered" => $this->M_sla_component_avability->count_filtered_pn(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function datatable_mtype()
    {
        // code...
        $list = $this->M_sla_component_avability->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->part_number;
            $row[] = $val->mtype;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_sla_component_avability->count_all(),
            "recordsFiltered" => $this->M_sla_component_avability->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function master_target()
    {
      // code...
      $query=$this->M_sla_component_avability->get_aircraft();
      $aircraft_tipe=array();
      foreach ($query as $key ) {
        // code...
        $data=explode('-',$key['actype']);
        array_push($aircraft_tipe,$data[0]);
      }
      $diff_array=array_unique($aircraft_tipe);

      echo json_encode($diff_array);
    }

    public function storetarget()
    {
      // code...
      $aircraft=$this->input->post('aircraft',true);
      $val_target=$this->input->post('val_target',true);
      $year_target=$this->input->post('year_target',true);

      // cek Avability In database
      $query=$this->M_sla_component_avability->get_avability_target($aircraft,$year_target);
      if($query==0){
        $insert_db=$this->M_sla_component_avability->store_target_sla($aircraft,$val_target,$year_target);
        $data=array('info'=>'success','msg'=> 'Values Target Success Inserted ');
      }else{
        $data=array('info'=>'warning','msg'=> 'Value Target Already Inserted For This Aircraft : '.$aircraft);
      }
      echo json_encode($data);
    }

    public function datatable_target()
    {
      // code...
      $draw = $_REQUEST['draw'];
      $length = $_REQUEST['length'];
      $starts = $_REQUEST['start'];
      $search = $_REQUEST['search']['value'];

      $data = array(
          'draw' => $draw,
          'length' => $length,
          'start' => $starts,
          'search' => $search,
          'year'=>date('Y')
      );

      $query=$this->M_sla_component_avability->load_datatable_target($data);
      $jml=$this->M_sla_component_avability->count_load_datatable_target($data);
      if(count($query)!=0){
      $no=$data['start']+1;
      foreach ($query as $key) {
        // code...
        $print['data'][]=array(
             $no++,
             $key['type_aircraft'],
             $key['target_aircraft']. "%",
             "<button type=button id=edit_target name=edit_target  data-id='".$key['id_target_sla']."'class='btn btn-info btn-flat btn-xs' ><i class='fa fa-pencil' ></i></button>&nbsp<button type=button id=del_target name=del_target  data-id='".$key['id_target_sla']."'class='btn btn-danger btn-flat btn-xs' ><i class='fa fa-trash' ></i></button>"
           );
      }
      }else{
        $print['data']=array();
      }
       $print['draw']= $data['draw'];
       $print['recordsFiltered']=$jml;
       $print['recordsTotal']  = $jml;
      echo json_encode($print);
    }

    public function edit_val_target()
    {
      // code...
      $param=$this->input->post('param');
      $query=$this->M_sla_component_avability->get_edit_data($param);
      echo json_encode($query);
    }

    public function editstoretarget()
    {
      // code...
      $id=$this->input->post('aircraft_model_id');
      $target=$this->input->post('target_edit');

      $query=$this->M_sla_component_avability->update_value_target($id,$target);
      if($query){
        $data=array('info'=>'success','msg'=> 'Edit Values Target Success  ');
      }else{
        $data=array('info'=>'error','msg'=> 'Edit Values Target Failed ');
      }
      echo json_encode($data);
    }

    public function delstoretarget()
    {
      // code...
      $id=$this->input->post('param',true);
      $query=$this->M_sla_component_avability->delete_value_target($id);
      if($query){
        $data=array('info'=>'success','msg'=> 'Delete Values Target Success  ');
      }else{
        $data=array('info'=>'error','msg'=> 'Delete Values Target Failed ');
      }
      echo json_encode($data);
    }
}
