<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class M_tat_airframe extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('default', true);
        $this->db2 = $this->load->database('dev_gmf', true);
    }

    function get_chart_accumulation($input, $date, $query, $mon)
    {

        $sql = "SELECT * FROM ( SELECT  DATEPART(YEAR, ta.act_start_date) as ac_year,
	        DATEPART(MONTH, ta.act_start_date) as ac_month,
	        Sum(isnull(cast(ta.plant_tat as float),0)) as planttat,
	        Sum(isnull(cast(ta.act_tat as float),0)) as acttat
	        FROM    tat_airframe ta
                join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
                join dbo.m_actype mt on ma.actype_id=mt.actype_id
	        WHERE   ta.act_start_date >= '" . $date['start'] . "'
	        AND     ta.act_start_date <= '" . $date['end'] . "'
	        and 	ta.act_finish_date is not null
          and mt.actype like '%" . $input['air_type'] . "'
          and mt.fleet_type in (" . $input['fleet'] . ")
          and ta.plant in (" . $input['tat_type'] . ")
          " . $query . "
	        GROUP BY DATEPART(YEAR, ta.act_start_date), DATEPART(MONTH, ta.act_start_date) ) chart
          WHERE ac_month='$mon'";
        $sql = $this->db2->query($sql);
        return $sql->result();
    }

    function get_chart_ga($query, $date, $input)
    {
        $sql = $this->db2->query("SELECT mt.actype as actype
          from dbo.tat_airframe ta join dbo.m_acreg ma
          on ta.aircraft_reg=ma.acreg join dbo.m_actype mt
          on ma.actype_id=mt.actype_id
          where ta.basic_start_date BETWEEN '" . $date['start'] . "' AND '" . $date['end'] . "'
          and mt.actype like '%" . $input['air_type'] . "'
          and mt.fleet_type in (" . $input['fleet'] . ")
          and ta.plant in (" . $input['tat_type'] . ")
          " . $query . "
          group by mt.actype");
        return $sql->result_array();
    }
    // function get_chart_nonga($date)
    // {
    //     $sql = $this->db2->query("SELECT mt.actype as actype from dbo.tat_airframe ta
    //       join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
    //       join dbo.m_actype mt on ma.actype_id=mt.actype_id
    //       where ma.own not in ('GA','CITILINK') AND
    //       ta.basic_start_date BETWEEN '".$date['start']."' AND '".$date['end']."'
    //       group by mt.actype");
    //     return $sql->result_array();
    // }

//     function get_total_chart($dataType,$jenis,$date)
//     {
//       $sql = $this->db2->query("SELECT DATEPART(YEAR, act_start_date) as ac_year,
//        DATEPART(MONTH, act_start_date) as ac_month,
//         Sum(isnull(cast(plant_tat+1 as float),0)) as planttat,
//         Sum(isnull(cast(act_tat+1 as float),0)) as acttat
// from dbo.tat_airframe ta
// join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
// join dbo.m_actype mt on ma.actype_id=mt.actype_id
// where act_finish_date is not null
// AND act_start_date >= '".$date['start']."'
// AND act_start_date <= '".$date['end']."'
// and mt.actype ='" . $dataType['name'] . "' and ma.own='".$jenis."'
// GROUP BY DATEPART(YEAR, act_start_date), DATEPART(MONTH, act_start_date)");
//         // $sql = $this->db2->query("SELECT (plant_tat+1) as plant,(act_tat+1) as act ,act_finish_date as categori from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg join dbo.m_actype mt on ma.actype_id=mt.actype_id where act_finish_date is not null and mt.actype ='" . $dataType['name'] . "' and ma.own='$jenis'"); //((plant_tat+1)/(act_tat+1))*100 as accumulation
//         return $sql->result();
//     }

    // function get_chart_citi()
    // {
    //     $sql = $this->db2->query("SELECT mt.actype as actype from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg join dbo.m_actype mt on ma.actype_id=mt.actype_id where ma.own ='CITILINK' group by mt.actype");
    //     return $sql->result_array();
    // }
    //
    // function get_total_chart_citi($dataType)
    // {
    //     $sql = $this->db2->query("SELECT ((plant_tat+1)/(act_tat+1))*100 as accumulation ,act_finish_date as categori from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg join dbo.m_actype mt on ma.actype_id=mt.actype_id where act_finish_date is not null and mt.actype ='" . $dataType['name'] . "' and ma.own='CITILINK'");
    //     return $sql->result();
    // }


    function get_table_ga_det($type, $data)
    {
        $query = "SELECT * from (
                SELECT ROW_NUMBER() OVER (ORDER BY aircraft_type) AS RowNum, mt.actype_id as tipe,
                ta.aircraft_reg as register,
                ta.revision_description as inspect,
                ta.plant_tat as target,
                ta.plant_tat as cus_agreed,
                ta.act_tat as actual
                from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
                join dbo.m_actype mt on ma.actype_id=mt.actype_id
                where act_finish_date is not null and ma.own in ('" . $type . "')
              ) N WHERE ";
        if ($data['search'] != '') {
            $query .= " N.register like '%" . $data['search'] . "%' OR
                          N.inspect like '%" . $data['search'] . "%' OR
                          N.target LIKE  '%" . $data['search'] . "%' OR
                          N.actual LIKE  '%" . $data['search'] . "%' OR
                          N.cus_agreed LIKE '%" . $data['search'] . "%' AND";
        }
        $query .= " N.RowNum >'" . $data['start'] . "'
                       AND N.RowNum <= '" . ($data['length'] + $data['start']) . "'";

        $query_count = "SELECT * from (
                        SELECT ROW_NUMBER() OVER (ORDER BY aircraft_type) AS RowNum, mt.actype_id as tipe,
                        ta.aircraft_reg as register,
                        ta.revision_description as inspect,
                        ta.plant_tat as target,
                        ta.plant_tat as cus_agreed,
                        ta.act_tat as actual
                        from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
                        join dbo.m_actype mt on ma.actype_id=mt.actype_id
                        where act_finish_date is not null and ma.own in ('" . $type . "')
                      ) N ";
        if ($data['search'] != '') {
            $query_count .= "WHERE N.register like '%" . $data['search'] . "%' OR
                                  N.inspect like '%" . $data['search'] . "%' OR
                                  N.target LIKE  '%" . $data['search'] . "%' OR
                                  N.actual LIKE  '%" . $data['search'] . "%' OR
                                  N.cus_agreed LIKE '%" . $data['search'] . "%'";
        }
        $hasil = $this->db2->query($query);
        $hasil2 = $this->db2->query($query_count);

        $output = array('data' => $hasil->result_array(),
            'jml' => $hasil2->num_rows());
        return $output;
        // $sql = $this->db2->query("SELECT mt.actype_id as tipe,ta.aircraft_reg as register,ta.revision_description as inspect,ta.plant_tat as target,ta.plant_tat as cus_agreed,ta.act_tat as actual from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg join dbo.m_actype mt on ma.actype_id=mt.actype_id where act_finish_date is not null and ma.own in ('$type')");
        // return $sql->result_array();
    }

    function get_table_ga_det_non($data)
    {
        $query = "SELECT * from (
              SELECT ROW_NUMBER() OVER (ORDER BY aircraft_type) AS RowNum,
              mt.actype_id as tipe,
              ta.aircraft_reg as register,
              ta.revision_description as inspect,
              ta.plant_tat as target,
              ta.plant_tat as cus_agreed,
              ta.act_tat as actual
              from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
              join dbo.m_actype mt on ma.actype_id=mt.actype_id
              where act_finish_date is not null
              and ma.own not in ('GA','CITILINK')
              ) N WHERE ";
        if ($data['search'] != '') {
            $query .= " N.register like '%" . $data['search'] . "%' OR
                          N.inspect like '%" . $data['search'] . "%' OR
                          N.target LIKE  '%" . $data['search'] . "%' OR
                          N.actual LIKE  '%" . $data['search'] . "%' OR
                          N.cus_agreed LIKE '%" . $data['search'] . "%' AND";
        }
        $query .= " N.RowNum >'" . $data['start'] . "'
                       AND N.RowNum <= '" . ($data['length'] + $data['start']) . "'";

        $query_count = "SELECT * from (
                      SELECT ROW_NUMBER() OVER (ORDER BY aircraft_type) AS RowNum,
                      mt.actype_id as tipe,
                      ta.aircraft_reg as register,
                      ta.revision_description as inspect,
                      ta.plant_tat as target,
                      ta.plant_tat as cus_agreed,
                      ta.act_tat as actual
                      from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
                      join dbo.m_actype mt on ma.actype_id=mt.actype_id
                      where act_finish_date is not null
                      and ma.own not in ('GA','CITILINK')
                      ) N ";
        if ($data['search'] != '') {
            $query_count .= "WHERE N.register like '%" . $data['search'] . "%' OR
                                  N.inspect like '%" . $data['search'] . "%' OR
                                  N.target LIKE  '%" . $data['search'] . "%' OR
                                  N.actual LIKE  '%" . $data['search'] . "%' OR
                                  N.cus_agreed LIKE '%" . $data['search'] . "%'";
        }
        $hasil = $this->db2->query($query);
        $hasil2 = $this->db2->query($query_count);

        $output = array('data' => $hasil->result_array(),
            'jml' => $hasil2->num_rows());
        return $output;
        // $sql = $this->db2->query("SELECT mt.actype_id as tipe,ta.aircraft_reg as register,ta.revision_description as inspect,ta.plant_tat as target,ta.plant_tat as cus_agreed,ta.act_tat as actual from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg join dbo.m_actype mt on ma.actype_id=mt.actype_id where act_finish_date is not null and ma.own not in ('GA','CITILINK')");
        // return $sql->result_array();
    }

    function get_value_ga($type)
    {
        $sql = $this->db2->query("SELECT mt.actype as tipe,ta.aircraft_reg as register,ta.revision_description as inspect,ta.plant_tat as target,ta.act_tat as actual,DATEDIFF(DAY,ta.act_start_date, GETDATE()) as selisih from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg join dbo.m_actype mt on ma.actype_id=mt.actype_id where act_finish_date is null and act_start_date is not null and ma.own = '" . $type . "'");
        return $sql->result_array();
    }

    function get_value_non()
    {
        $sql = $this->db2->query("SELECT mt.actype as tipe,ta.aircraft_reg as register,ta.revision_description as inspect,ta.plant_tat as target,ta.act_tat as actual, DATEDIFF(DAY,ta.act_start_date, GETDATE()) as selisih from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg join dbo.m_actype mt on ma.actype_id=mt.actype_id where act_finish_date is null and act_start_date is not null and ma.own not in ('GA','CITILINK')");
        return $sql->result_array();
    }

    function get_aircraft()
    {
        $sql = $this->db2->query("SELECT DISTINCT case
                when own = 'GA' then 'GA'
                when own = 'CITILINK' then 'CITILINK'
                else 'NON GA'
                end as own
                from m_acreg group by own");
        return $sql->result_array();
    }

    function get_airtype($param)
    {

        $sql = $this->db2->query("SELECT own,actype from m_acreg ma join m_actype mt on ma.actype_id=mt.actype_id where own in ('" . $param . "') group by own,mt.actype");
        return $sql->result_array();
    }

    function get_airtype_nonga($param)
    {

        $sql = $this->db2->query("SELECT own,actype from m_acreg ma join m_actype mt on ma.actype_id=mt.actype_id where own not in ('GA','CITILINK') or own in ('" . $param . "') group by own,mt.actype");
        return $sql->result_array();
    }

    function get_value_ga_datatable($input, $date, $data)
    {

        $query = "SELECT * from (
        SELECT ROW_NUMBER() OVER (ORDER BY mt.actype) AS RowNum,
        mt.actype as tipe,ta.aircraft_reg as register,
        ta.revision_description as inspect,
        ta.plant_tat as target,
        ta.act_tat as actual,
        DATEDIFF(DAY,ta.act_start_date, GETDATE()) as selisih,case
                when ma.own = 'GA' then 'GA'
                when ma.own = 'CITILINK' then 'CITILINK'
                else 'NON GA'
                end as own
            from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
            join dbo.m_actype mt on ma.actype_id=mt.actype_id
            where act_finish_date is null
            and basic_start_date BETWEEN '" . $date['start'] . "' AND '" . $date['end'] . "'
            and act_start_date is not null
            and ma.own in ('" . $input['param_airc'] . "')
            and mt.actype like '%" . $input['air_type'] . "'
            and mt.fleet_type in (" . $input['fleet'] . ")
            and ta.plant in (" . $input['tat_type'] . ")
              ) N WHERE ";
        if ($data['search'] != '') {
            $query .= " N.tipe like '%" . $data['search'] . "%' OR
                          N.register like '%" . $data['search'] . "%' OR
                          N.inspect LIKE  '%" . $data['search'] . "%' OR
                          N.target LIKE  '%" . $data['search'] . "%' OR
                          N.own LIKE  '%" . $data['search'] . "%' OR
                          N.actual LIKE '%" . $data['search'] . "%' AND";
        }
        $query .= " N.RowNum >'" . $data['start'] . "'
                       AND N.RowNum <= '" . ($data['length'] + $data['start']) . "'";

        $query_count = "SELECT * from (
                SELECT ROW_NUMBER() OVER (ORDER BY mt.actype) AS RowNum,
                mt.actype as tipe,ta.aircraft_reg as register,
                ta.revision_description as inspect,
                ta.plant_tat as target,
                ta.act_tat as actual,
                DATEDIFF(DAY,ta.act_start_date, GETDATE()) as selisih,case
                        when ma.own = 'GA' then 'GA'
                        when ma.own = 'CITILINK' then 'CITILINK'
                        else 'NON GA'
                        end as own
                    from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
                    join dbo.m_actype mt on ma.actype_id=mt.actype_id
                    where act_finish_date is null
                    and basic_start_date BETWEEN '" . $date['start'] . "' AND '" . $date['end'] . "'
                    and act_start_date is not null
                    and ma.own in ('" . $input['param_airc'] . "')
                    and mt.actype like '%" . $input['air_type'] . "'
                    and mt.fleet_type in (" . $input['fleet'] . ")
                    and ta.plant in (" . $input['tat_type'] . ")
                      ) N ";
        if ($data['search'] != '') {
            $query_count .= "WHERE N.tipe like '%" . $data['search'] . "%' OR
                                  N.register like '%" . $data['search'] . "%' OR
                                  N.inspect LIKE  '%" . $data['search'] . "%' OR
                                  N.target LIKE  '%" . $data['search'] . "%' OR
                                  N.own LIKE  '%" . $data['search'] . "%' OR
                                  N.actual LIKE '%" . $data['search'] . "%' ";
        }

        $hasil = $this->db2->query($query);
        $hasil2 = $this->db2->query($query_count);

        $output = array('data' => $hasil->result_array(),
            'jml' => $hasil2->num_rows());
        return $output;

    }

    function get_value_ga_by($input, $date, $query)
    {
        $sql = $this->db2->query("SELECT mt.actype as tipe,ta.aircraft_reg as register,
        ta.revision_description as inspect,ta.plant_tat as target,ta.act_tat as actual,
        DATEDIFF(DAY,ta.act_start_date, GETDATE()) as selisih,case
              when ma.own = 'GA' then 'GA'
              when ma.own = 'CITILINK' then 'CITILINK'
              else 'NON GA'
              end as own
          from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
          join dbo.m_actype mt on ma.actype_id=mt.actype_id
          where act_finish_date is null
          and basic_start_date BETWEEN '" . $date['start'] . "' AND '" . $date['end'] . "'
          and act_start_date is not null
          and ma.own in ('" . $input['param_airc'] . "')
          and mt.actype like '%" . $input['air_type'] . "'
          and mt.fleet_type in (" . $input['fleet'] . ")
          and ta.plant in (" . $input['tat_type'] . ")
          " . $query . "");
        return $sql->result_array();
    }

    function get_value_nonga_by($input, $date)
    {
        $sql = $this->db2->query("SELECT mt.actype as tipe,
        ta.aircraft_reg as register,
        ta.revision_description as inspect,
        ta.plant_tat as target,
        ta.act_tat as actual,
        DATEDIFF(DAY,ta.act_start_date, GETDATE()) as selisih,case
              when ma.own = 'GA' then 'GA'
              when ma.own = 'CITILINK' then 'CITILINK'
              else 'NON GA'
              end as own
          from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
          join dbo.m_actype mt on ma.actype_id=mt.actype_id
          where act_finish_date is null
          and basic_start_date BETWEEN '" . $date['start'] . "' AND '" . $date['end'] . "'
          and act_start_date is not null
          and mt.actype like '%" . $input['air_type'] . "'
          and mt.fleet_type in (" . $input['fleet'] . ")
          and ta.plant in (" . $input['tat_type'] . ")
          and (ma.own not in ('GA','CITILINK')
          or ma.own in ('" . $input['param_airc'] . "'))");
        return $sql->result_array();
    }

    function get_value_nonga_datatable($input, $date, $data)
    {
        $query = "SELECT * from (
        SELECT ROW_NUMBER() OVER (ORDER BY mt.actype) AS RowNum,
        mt.actype as tipe,
          ta.aircraft_reg as register,
          ta.revision_description as inspect,
          ta.plant_tat as target,
          ta.act_tat as actual,
          DATEDIFF(DAY,ta.act_start_date, GETDATE()) as selisih,case
                when ma.own = 'GA' then 'GA'
                when ma.own = 'CITILINK' then 'CITILINK'
                else 'NON GA'
                end as own
            from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
            join dbo.m_actype mt on ma.actype_id=mt.actype_id
            where act_finish_date is null
            and basic_start_date BETWEEN '" . $date['start'] . "' AND '" . $date['end'] . "'
            and act_start_date is not null
            and mt.actype like '%" . $input['air_type'] . "'
            and mt.fleet_type in (" . $input['fleet'] . ")
            and ta.plant in (" . $input['tat_type'] . ")
            and (ma.own not in ('GA','CITILINK')
            or ma.own in ('" . $input['param_airc'] . "'))
              ) N WHERE ";
        if ($data['search'] != '') {
            $query .= " N.tipe like '%" . $data['search'] . "%' OR
                          N.register like '%" . $data['search'] . "%' OR
                          N.inspect LIKE  '%" . $data['search'] . "%' OR
                          N.target LIKE  '%" . $data['search'] . "%' OR
                          N.own LIKE  '%" . $data['search'] . "%' OR
                          N.actual LIKE '%" . $data['search'] . "%' AND";
        }
        $query .= " N.RowNum >'" . $data['start'] . "'
                       AND N.RowNum <= '" . ($data['length'] + $data['start']) . "'";

        $query_count = "SELECT * from (
                SELECT ROW_NUMBER() OVER (ORDER BY mt.actype) AS RowNum,
                mt.actype as tipe,
                  ta.aircraft_reg as register,
                  ta.revision_description as inspect,
                  ta.plant_tat as target,
                  ta.act_tat as actual,
                  DATEDIFF(DAY,ta.act_start_date, GETDATE()) as selisih,case
                        when ma.own = 'GA' then 'GA'
                        when ma.own = 'CITILINK' then 'CITILINK'
                        else 'NON GA'
                        end as own
                    from dbo.tat_airframe ta join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
                    join dbo.m_actype mt on ma.actype_id=mt.actype_id
                    where act_finish_date is null
                    and basic_start_date BETWEEN '" . $date['start'] . "' AND '" . $date['end'] . "'
                    and act_start_date is not null
                    and mt.actype like '%" . $input['air_type'] . "'
                    and mt.fleet_type in (" . $input['fleet'] . ")
                    and ta.plant in (" . $input['tat_type'] . ")
                    and (ma.own not in ('GA','CITILINK')
                    or ma.own in ('" . $input['param_airc'] . "'))
                      ) N  ";
        if ($data['search'] != '') {
            $query_count .= "WHERE N.tipe like '%" . $data['search'] . "%' OR
                                  N.register like '%" . $data['search'] . "%' OR
                                  N.inspect LIKE  '%" . $data['search'] . "%' OR
                                  N.target LIKE  '%" . $data['search'] . "%' OR
                                  N.own LIKE  '%" . $data['search'] . "%' OR
                                  N.actual LIKE '%" . $data['search'] . "%'";
        }

        $hasil = $this->db2->query($query);
        $hasil2 = $this->db2->query($query_count);

        $output = array('data' => $hasil->result_array(),
            'jml' => $hasil2->num_rows());
        return $output;

    }

    function get_total_chart_2($dataType, $query, $date, $mon)
    {
        $sql = $this->db2->query("SELECT * from (
              SELECT DATEPART(YEAR, act_start_date) as ac_year,
                     DATEPART(MONTH, act_start_date) as ac_month,
                    Sum(isnull(cast(plant_tat as float),0)) as planttat,
                    Sum(isnull(cast(act_tat as float),0)) as acttat
              from dbo.tat_airframe ta
              join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
              join dbo.m_actype mt on ma.actype_id=mt.actype_id
              where act_finish_date is not null
              AND act_start_date >= '" . $date['start'] . "'
              AND act_start_date <= '" . $date['end'] . "'
              and mt.actype ='" . $dataType['name'] . "'
              " . $query . "
              GROUP BY DATEPART(YEAR, act_start_date), DATEPART(MONTH, act_start_date)
              ) T where ac_month = '" . $mon . "'");
        return $sql->row_array();
    }

    function get_total_chart_nonga($dataType, $date, $mon)
    {
        $sql = $this->db2->query("SELECT * from (
                SELECT DATEPART(YEAR, act_start_date) as ac_year,
                       DATEPART(MONTH, act_start_date) as ac_month,
                      Sum(isnull(cast(plant_tat as float),0)) as planttat,
                      Sum(isnull(cast(act_tat as float),0)) as acttat
                from dbo.tat_airframe ta
                join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
                join dbo.m_actype mt on ma.actype_id=mt.actype_id
                where act_finish_date is not null
                AND act_start_date >= '" . $date['start'] . "'
                AND act_start_date <= '" . $date['end'] . "'
                and mt.actype ='" . $dataType['name'] . "' and ma.own not in ('GA','CITILINK')
                GROUP BY DATEPART(YEAR, act_start_date), DATEPART(MONTH, act_start_date)
                ) T where ac_month = '" . $mon . "'");
        return $sql->result();
    }

    function cek_month_chart($jenis, $date)
    {
        $sql = $this->db2->query("SELECT DATEPART(YEAR, act_start_date) as ac_year,
             DATEPART(MONTH, act_start_date) as ac_month
      from dbo.tat_airframe ta
      join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
      join dbo.m_actype mt on ma.actype_id=mt.actype_id
      where act_finish_date is not null
      AND act_start_date >= '" . $date['start'] . "'
      AND act_start_date <= '" . $date['end'] . "'
      AND ma.own='" . $jenis . "'
      GROUP BY DATEPART(YEAR, act_start_date), DATEPART(MONTH, act_start_date)");
        return $sql->result_array();
    }

    function cek_month_chart_nonga($jenis, $date)
    {
        $sql = $this->db2->query("SELECT DATEPART(YEAR, act_start_date) as ac_year,
             DATEPART(MONTH, act_start_date) as ac_month
      from dbo.tat_airframe ta
      join dbo.m_acreg ma on ta.aircraft_reg=ma.acreg
      join dbo.m_actype mt on ma.actype_id=mt.actype_id
      where act_finish_date is not null
      AND act_start_date >= '" . $date['start'] . "'
      AND act_start_date <= '" . $date['end'] . "'
      AND ma.own not in ('GA','CITILINK')
      GROUP BY DATEPART(YEAR, act_start_date), DATEPART(MONTH, act_start_date)");
        return $sql->result_array();
    }
}
