<?php
defined("BASEPATH") or exit('no direct script access allowed');

class Tat_engine_apu_maintenance extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_menu");
        $this->load->model("M_tat_apu_engine_maint");
    }

    public function index()
    {
        $data = array(
            "content" => "tat_engine_apu_maintenance/index",
            // "content" => "tat_engine_apu_maintenance/v_table",
            "title" => "TAT Maintenance Engine & APU ",
            "small_tittle" => "",
            "breadcrumb" => ["TAT Engine & APU Maintenance"],
            "event_engine" => $this->M_tat_apu_engine_maint->count_event_closed("CFM"),
            "event_apu" => $this->M_tat_apu_engine_maint->count_event_closed("GTCP"),
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }


    public function data_list_closed($prod_type = "", $month = "")
    {
        $list_month = array("", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec");
        if ($month != -1) {
            $awal = explode("-", $month);
            $awal_bulan = $awal[0];
            $awal_tahun = $awal[1];
            $arrMonths = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
            $index = array_search($awal_bulan, $arrMonths);
        }
        // $where_like = "(status LIKE '%TECO%' OR status LIKE '%CLSD%')";
        $where_like = "ACT_FINISH IS NOT NULL"; //CONVERT(DATE, M_REVISION.REVED, 112) IS NOT NULL

        if ($month != -1) {
            $list = $this->M_tat_apu_engine_maint->get_datatables(((int)$index) + 1,$where_like, $prod_type);
            $recordsTotal = $this->M_tat_apu_engine_maint->count_all(((int)$index) + 1,$where_like, $prod_type);
            $recordsFiltered = $this->M_tat_apu_engine_maint->count_filtered(((int)$index) + 1,$where_like, $prod_type);
        } else {
            $list = $this->M_tat_apu_engine_maint->get_datatables(-1,$where_like, "");
            $recordsTotal = $this->M_tat_apu_engine_maint->count_all(-1,$where_like, "");
            $recordsFiltered = $this->M_tat_apu_engine_maint->count_filtered(-1,$where_like, "");
        }

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
            $no++;
            $row = array();
            // $row[] = $no;
            $nilai =number_format(($val->PERFORMANCE)*100, 2);
            if($nilai > 100 ){
              $nilai = 100;
            }
            for($i=0; $i<=count($list_month); $i++){
                if($i==$val->MONTH){
                    $conv_date = $list_month[$i];
                }
            }
            $row[] = $conv_date; //date("M", strtotime($val->REVISION))
            $row[] = $val->ESN;// maintenance type
            $row[] = $val->REVTX;
            $row[] = $val->CUSTOMER;
            $row[] = $val->CUST_TYPE;
            $row[] = $val->PRODUCT_TYPE;
            $row[] = $val->ENGINE_APU;
            $row[] = $val->TARGET_TAT;
            $row[] = $val->ACT_TAT;
            $row[] = $nilai.' %';
            // if (strpos($val->CUSTOMER, 'GARUDA-INDONESIA') !== false ||
            //     strpos($val->CUSTOMER, 'GARUDA INDONESIA') !== false ||
            //     strpos($val->CUSTOMER, 'CITILINK') !== false) {
            //   $row[] = 'GA';
            // }else{
            //   $row[] = 'NGA';
            // }
            // $row[] = $val->PROD_TYPE;
            // $row[] = $val->AIR_REG;
            // $row[] = $val->TARGET_TAT;
            // $row[] = $val->ACTUAL_TAT;
            // $row[] = $val->ACHIEVEMENT . '%';
            if ($val->ACT_TAT > $val->TARGET_TAT) {
                $row[] = true;
            } else {
                $row[] = false;
            }
            // $row[] = $val->status;

            // $row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="ajax_edit('."'".$val->id_tm_final."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';
            // <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="ajax_delete('."'".$val->target_id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        );

        $this->output->set_output(json_encode($output));
    }

    public function data_list_progress($prod_type = "")
    {

        // $where_like = "(status NOT LIKE '%TECO%' OR status NOT LIKE '%CLSD%')";
        $where_like = "ACT_FINISH IS NULL";

        if ($prod_type == "") {
            $list = array();
            $recordsTotal = 0;
            $recordsFiltered = 0;
        } else {
            $list = $this->M_tat_apu_engine_maint->get_datatables(-1,$where_like, $prod_type); //$where_like
            $recordsTotal = $this->M_tat_apu_engine_maint->count_all(-1,$where_like, $prod_type); //$where_like
            $recordsFiltered = $this->M_tat_apu_engine_maint->count_filtered(-1,$where_like, $prod_type); //$where_like
        }

        // $list_month = array(
        //     '1' => 'Jan',
        //     '2' => 'Feb',
        //     '3' => 'Mar',
        //     '4' => 'Apr',
        //     '5' => 'May',
        //     '6' => 'Jun',
        //     '7' => 'Jul',
        //     '8' => 'Aug',
        //     '9' => 'Sep',
        //     '10' => 'Oct',
        //     '11' => 'Nov',
        //     '12' => 'Des'
        // );
        $list_month = array("", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec");

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
            $no++;
            // $awal = $val->DATE_START;
            // $akhir = $val->DATE_FINISH;
            // $total = $akhir - $awal;
            $row = array();
            for($i=0; $i<=count($list_month); $i++){
                if($i==$val->MONTH){
                    $conv_date = $list_month[$i];
                }
            }

            $row[] = $conv_date;
            $row[] = $val->ESN;
            $row[] = $val->REVTX;
            $row[] = $val->CUSTOMER;
            $row[] = $val->CUST_TYPE;
            $row[] = $val->PRODUCT_TYPE;
            $row[] = $val->ENGINE_APU;
            $row[] = $val->TARGET_TAT;
            $row[] = $val->TOTAL_DAY;
            // $row[] = $val->PERFORMANCE.' %';
            // $row[] = $no;
            // $row[] = date("M", strtotime($val->START_DATE_REVISION));
            // $row[] = $val->ESN;
            // $row[] = $val->MAINT_TYPE;
            // $row[] = $val->CUSTOMER;
            // $row[] = $val->CUSTOMER_TYPE;
            // // if (strpos($val->CUSTOMER, 'GARUDA-INDONESIA') !== false ||
            // //     strpos($val->CUSTOMER, 'GARUDA INDONESIA') !== false ||
            // //     strpos($val->CUSTOMER, 'CITILINK') !== false) {
            // //   $row[] = 'GA';
            // // }else{
            // //   $row[] = 'NGA';
            // // }
            // $row[] = $val->PROD_TYPE;
            // $row[] = $val->AIR_REG;
            // $row[] = $val->TARGET_TAT;
            // $datetime1 = new DateTime($val->START_DATE_REVISION);
            // $datetime2 = new DateTime();
            // $interval = date_diff($datetime1, $datetime2);
            // $row[] = $interval->format('%a days');
            // $row[] = $val->DIFF_DATE;
            if ($val->TOTAL_DAY > $val->TARGET_TAT) {
                $row[] = true;
            } else {
                $row[] = false;
            }
            // $row[] = date_diff("2013-03-15", "2013-03-15", false);
            // $row[] = $val->actual;
            // $row[] = $val->achievment;
            // $row[] = $val->status;

            // $row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="ajax_edit('."'".$val->id_tm_final."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';
            // <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="ajax_delete('."'".$val->target_id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        );

        $this->output->set_output(json_encode($output));
    }

    public function getCountAchievement()
    {

    }

    public function getDataChart()
    {
        $tipe = $this->input->post('type');
        $month_now = date('m');
        $colors = array('#fd9214', '#05354D');
        $data = array();
        $dtTarget = array();

        $arrMonths = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $categories = $this->M_tat_apu_engine_maint->getDataMonth();
        $month = array();
        $id_month = array();
        $dtengine = array();
        $con_year = substr(date('Y'), 2);

        for ($i = 0; $i < $month_now; $i++) {
            // $month[]= $arrMonths[(int)$i];
            $month[] = date('M', mktime(0, 0, 0, (int)$i + 1, 10)) . '-' . $con_year;
            // $id_month[]= $value;
            $dtengine[] = 0;
            $dtTarget[] = 100;
        }
        // foreach ($categories as $key => $value) {
        //   // $month[]= $arrMonths[(int)$value->bulan-1];
        //   $id_month[]= $value;
        // }
        $engines = $this->M_tat_apu_engine_maint->getDataChart($tipe);
        foreach ($engines as $key => $value) {
            $id_month[] = $value->bulan;
            $value_nilai = floatval(number_format(($value->nilai)*100, 2));
            if($value_nilai > 100){
              $value_nilai = 100;
            }
            $dtengine[$value->bulan - 1] = $value_nilai;
            $dtTarget[$key] = 100;
        }
        // $engines = $this->M_tat_apu_engine_maint->getDataChart('CFM');
        // foreach ($engines as $key => $value) {
        //   $dtengine[]= (int)$value->nilai;
        //   $dtTarget[]= 100;
        // }

        // $apus = $this->M_tat_apu_engine_maint->getDataChart('NGA');
        // $dtapus = array();
        // foreach ($apus as $key => $value) {
        //  $dtapus[]=(int)$value->nilai;
        // }

        $target = array();
        $target['name'] = 'Target';
        $target['data'] = $dtTarget;
        $target['color'] = $colors[0];
        $target['marker'] = array('enabled' => false);
        array_push($data, $target);

        $engine = array();
        $engine['name'] = 'Performance';
        $engine['id_month'] = $id_month;
        $engine['data'] = $dtengine;
        $engine['color'] = $colors[1];
        array_push($data, $engine);

        $result = array('categories' => $month, 'series' => $data);
        // echo '<pre>';
        // print_r($result);
        // echo '</pre>';
        echo json_encode($result);
    }

    public function getDataChartAPU()
    {

        $month_now = date('m');
        $colors = array('#fd9214', '#8EC3A7');
        $data = array();
        $dtTarget = array();

        $arrMonths = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $categories = $this->M_tat_apu_engine_maint->getDataMonth();

        $month = array();
        $id_month = array();
        $dtapus = array();

        for ($i = 0; $i < $month_now; $i++) {
            // $month[]= $arrMonths[(int)$i];
            $month[] = date('M', mktime(0, 0, 0, (int)$i + 1, 10)) . '-' . date('Y');
            // $id_month[]= $value;
            $dtapus[] = 0;
            $dtTarget[] = 100;
        }

        // foreach ($categories as $key => $value) {
        //   $id_month[]= $value;
        //   // $month[]= $arrMonths[(int)$value->bulan-1];
        // }
        // $engines = $this->M_tat_apu_engine_maint->getDataChart('GA');
        // $dtengine = array();
        // foreach ($engines as $key => $value) {
        //  $dtengine[]= (int)$value->nilai;
        // }

        $apus = $this->M_tat_apu_engine_maint->getDataChart('GTCP');
        foreach ($apus as $key => $value) {
            $id_month[] = $value->bulan;
            $dtapus[$value->bulan - 1] = floatval(number_format(($value->nilai)*100, 2));
            $dtTarget[$key] = 100;
        }

        $target = array();
        $target['name'] = 'Target';
        $target['data'] = $dtTarget;
        $target['color'] = $colors[0];
        $target['marker'] = array('enabled' => false);
        array_push($data, $target);

        $apu = array();
        $apu['name'] = 'Performance';
        $apu['id_month'] = $id_month;
        $apu['data'] = $dtapus;
        $apu['color'] = $colors[1];
        array_push($data, $apu);


        $result = array('categories' => $month, 'series' => $data);
        // echo '<pre>';
        // print_r($result);
        // echo '</pre>';
        echo json_encode($result);
    }

    public function tableGrafikUp($month)
    {

        $arrMonths = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $index = array_search($month, $arrMonths);

        echo json_encode($index + 1);
    }



        public function data_list()
        {

            $where_like = "CONVERT(DATE, A.MONTH, 112) IS NOT NULL"; //CONVERT(DATE, M_REVISION.REVED, 112) IS NOT NULL

            $list = $this->M_tat_apu_engine_maint->get_datatables_(-1, $where_like);
            $recordsTotal = $this->M_tat_apu_engine_maint->count_all_(-1, $where_like);
            $recordsFiltered = $this->M_tat_apu_engine_maint->count_filtered_(-1, $where_like);

            $data = array();
            $no = $_POST['start'];
            foreach ($list as $val) {
                $no++;
                $row = array();
                // $row[] = $no;
                $row[] = $val->MONTH; //date("M", strtotime($val->REVISION))
                $row[] = $val->ESN;
                $row[] = $val->CUSTOMER;
                $row[] = $val->CUST_TYPE;
                $row[] = $val->PRODUCT_TYPE;
                $row[] = $val->ENGINE_APU;
                $row[] = $val->MAINT_TYPE;
                $row[] = $val->TARGET_TAT;
                $row[] = $val->ACT_TAT;
                $row[] = $val->PERFORMANCE;
                // $datetime1 = new DateTime($val->START_DATE_REVISION);
                // $datetime2 = new DateTime();
                // $interval = date_diff($datetime1, $datetime2);
                // $row[] = $interval->format('%a days');
                // $row[] = date_diff("2013-03-15", "2013-03-15", false);
                // $row[] = $val->actual;
                // $row[] = $val->achievment;
                // $row[] = $val->status;

                // $row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="ajax_edit('."'".$val->id_tm_final."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';
                // <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="ajax_delete('."'".$val->target_id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

                $data[] = $row;
            }

            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
                "data" => $data
            );

            $this->output->set_output(json_encode($output));
        }
}
