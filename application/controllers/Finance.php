<?php if(!defined('BASEPATH')) exit('No direct access allowed');
//require_once $_SERVER['DOCUMENT_ROOT'].'/GMF/dss-repo/application/libraries/spout/src/Spout/Autoloader/autoload.php';
require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
date_default_timezone_set("Asia/Jakarta");

class Finance extends MY_Controller{

    function __construct(){
        parent:: __construct();
            $this->desc = 'Finance Dashboard';
            $this->url_breadcrumb = '';
            $this->load->model('M_finance');
            $this->load->library('PHPExcel');
            $this->db_dss = $this->load->database('dev_gmf', TRUE);
    }

    public function aaa()
    {
        print("<pre>".print_r($_SESSION,true)."</pre>");
    }

    function index(){
        $data = array(
            "content"       => "finance/index",
            "title"         => "Finance Dashboard",
            "small_tittle"  => "",
            "breadcrumb"    => ["Finance Dashboard"],
            "menu"          => $this->M_menu->tampil()
          );
        $this->load->view("layouts", $data);
    }

    function master_data(){
        $data = array(
            "content"       => "finance/master-data",
            "title"         => "Master Data Finance",
            "small_tittle"  => "",
            "breadcrumb"    => ["Master Data Finance"],
            "menu"          => $this->M_menu->tampil()
          );
        $this->load->view("layouts", $data);
    }

    function list_finance(){
        $list = $this->M_finance->get_datatables_finance();
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $finance) {
            $no++;
            $download = site_url('assets/Finance/').$finance->name_file;
            $waktu_upload = explode(".", $finance->tgl_data);
            $waktu_upload = $waktu_upload[0];
            $row   = array();
            $row[] = $no;
            $row[] = $finance->name_file;
            $row[] = $finance->upload_by;
            $row[] = $waktu_upload;
            $row[] = "<center>
            <a href='{$download}'><button class='btn btn-success btn-xs'>Download File</button></a>
            </center>";
            /*$row[] = $finance->tipe;
            $row[] = $finance->dinas;
            $row[] = '<center>'.$finance->period.'</center>';
            $row[] = '<center>'.$finance->year.'</center>';
            $row[] = '<right>'.$finance->value.'</right>';*/
            $data[] = $row;
        }
        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->M_finance->count_all_finance(),
            "recordsFiltered" => $this->M_finance->count_filtered_finance(),
            "data"            => $data,
        );
        echo json_encode($output);
    }

    public function upload_proses()
    {
        $url_redir  = site_url('index.php/Finance/master_data');
        $format     = date('Y-m-d&H-i-s');
        $fileName   = $format.'_'.$_FILES['files']['name'];
        $fileName   = str_replace(" ", "_", $fileName);
        $destination_folder = 'assets/Finance/';
        $file = "$destination_folder/$fileName";

        $file_type = explode(".", $_FILES['files']['name']);
        $file_type = strtolower($file_type[1]);
        if($file_type != "xlsx"){
            echo "<script language='JavaScript'>
                    swal({
                        title: 'Erorr',
                        text: 'Type file is not .xlsx',
                        icon: 'error',
                    }).then((value) => {
                        window.location.href='".$url_redir."'
                    });
                </script>";
        }else{
            //upload file ke server
            move_uploaded_file($_FILES['files']['tmp_name'], $file);

            $file_path = './assets/Finance/'.$fileName;//$_FILES['files']['tmp_name'];
            $reader = ReaderFactory::create(Type::XLSX);
            $reader->open($file_path);

            foreach ($reader->getSheetIterator() as $sheet) {
                if (strtolower($sheet->getName()) == "database_table_2018 (new)") {
                    foreach ($sheet->getRowIterator() as $row) {
                        $data[] = $row;
                    }
                }
            }

            $data_batch = [];
            for ($brs = 1; $brs < count($data); $brs++){ //Read a row of data into an array
                //Sesuaikan sama nama kolom tabel di database
                $data_arr = array(
                    //"id"          => $rowData[0][0],
                    "indikator"     => $data[$brs][1],
                    "tipe"          => $data[$brs][2],
                    "dinas"         => $data[$brs][3],
                    "fiscal_period" => $data[$brs][4],
                    "fiscal_year"   => $data[$brs][5],
                    "value"         => $data[$brs][6],
                    "tgl_data"      => date('Y-m-d H:i:0'),
                    "name_file"     => $fileName,
                    "upload_by"     => $_SESSION['username'],
                );
                $data_batch[] = $data_arr;
            }
            /*$cek = $this->M_finance->fetch('fin_data',['tgl_data'],['tgl_data LIKE' => date('Y-m').'%'],0,1);
            if (count($cek['data']) > 0) {
                $delete = $this->M_finance->delete('fin_data',['tgl_data LIKE' => date('Y-m').'%']);
                if ($delete['codestatus'] != 'S') {
                    echo "<script language='JavaScript'>
                    swal({
                        title: 'Upload Failed',
                        text: 'Gagal Menghapus Data Sebelumnya',
                        icon: 'error',
                    }).then((value) => {
                        window.location.href='".$url_redir."'
                    });
                </script>";die();
                }
            }*/
            $insert   = $this->db_dss->insert_batch('fin_data', $data_batch);
            //print("<pre>".print_r($data_batch,true)."</pre>");die();

            $reader->close();
            //die();
            if($insert) {
                echo "<script language='JavaScript'>
                    swal({
                        title: 'Upload Succesfully',
                        text: 'Upload File is Succesfully',
                        icon: 'success',
                    }).then((value) => {
                        window.location.href='".$url_redir."'
                    });
                </script>";
            }else{
                echo "<script language='JavaScript'>
                    swal({
                        title: 'Upload Failed',
                        text: 'Upload File is Failed',
                        icon: 'error',
                    }).then((value) => {
                        window.location.href='".$url_redir."'
                    });
                </script>";
            }
        }
    }

    function do_upload(){
        $format     = date('Y-m-d&H-i-s');
        $fileName   = $format.'_'.$_FILES['files']['name'];
        $config['upload_path']      = './assets/Finance/'; //buat folder dengan nama assets di root folder
        $config['file_name']        = $fileName;
        $config['allowed_types']    = 'xls|xlsx|csv';

        $this->load->library('upload');
        $this->upload->initialize($config);

        $destination_folder = 'assets/Finance/';
        $file = "$destination_folder/$fileName";

        $thn = date('Y');
        //$sheetname = 'Data Tes '.$thn;
        $sheetname = 'Database_Table_2018 (new)';

        if(! $this->upload->do_upload('files') )
        $this->upload->display_errors();

        //$media = $this->upload->data('files');
        move_uploaded_file($_FILES['files']['tmp_name'], $file);
        $inputFileName = './assets/Finance/'.$fileName;

        try {
            $objReader = PHPExcel_IOFactory::load($path);
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setLoadSheetsOnly($sheetname);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet($sheetname);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $cek = $this->M_finance->fetch('fin_data',['tgl_data'],['tgl_data LIKE' => date('Y-m').'%'],0,1);
        if (count($cek['data']) > 0) {
            $delete = $this->M_finance->delete('fin_data',['tgl_data LIKE' => date('Y-m').'%']);
            if ($delete['codestatus'] != 'S') {
                echo json_encode(array("status" => FALSE));die();
            }
        }

        $data_batch = [];
        for ($row = 2; $row <= $highestRow; $row++){ //Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);

            //Sesuaikan sama nama kolom tabel di database
            $data = array(
                //"id"          => $rowData[0][0],
                "indikator"     => $rowData[0][1],
                "tipe"          => $rowData[0][2],
                "dinas"         => $sheet->getCellByColumnAndRow(3,$row)->getOldCalculatedValue(),
                "fiscal_period" => $rowData[0][4],
                "fiscal_year"   => $sheet->getCellByColumnAndRow(5,$row)->getOldCalculatedValue(),
                "value"         => $sheet->getCellByColumnAndRow(6,$row)->getOldCalculatedValue(),
                "tgl_data"      => date('Y-m-d')
            );
            $data_batch[] = $data;

        }

        $this->db_dss->insert_batch('fin_data', $data_batch);
        echo json_encode(array("status" => TRUE));
    }

    function delete_all(){
        $this->db_dss->empty_table('fin_data');
        echo json_encode(array("status" => TRUE));
    }

}
