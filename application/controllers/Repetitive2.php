<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//tes

class Repetitive2 extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("M_menu");
        $this->load->model("M_repetitive2","M_repetitive");
        $this->load->model("M_hil");
    }

    function index()
    {
        $data = array(
            "content" => "repetitive/v_repetitive2",
            "title" => "Repetitive Problem Closing Rate",
            "titleGrafik" => "General Chart",
            "small_tittle" => "",
            "breadcrumb" => ["Repetitive Problem Closing Rate"],
            "menu" => $this->M_menu->tampil(),
            'countGaruda' => count($this->M_repetitive->getDataGaruda()),
            'countCitilink' => count($citilinks = $this->M_repetitive->getDataCitilink()),
            'startPeriode' => '01-' . date('Y'),
            'endPeriode' => date('m-Y'),
        );
        $this->load->view("layouts", $data);
    }


        function tesGA(){
          $tes = count($this->M_repetitive->getDataGaruda());
          echo json_encode($tes);
        }


    function master_manufacture()
    {
        $query = $this->M_hil->master_manufacture();
        echo json_encode($query);
    }

    function master_type($id)
    {
        $query = $this->M_hil->master_reg($id);
        echo json_encode($query);
    }

    public function actualGaruda()
    {
        // $allDatas = $this->M_repetitive->getAllData('GA');
        // $garudas = $this->M_repetitive->getDataGarudaPeriode();
        $getRasioActype = $this->M_repetitive->getRasio('GA');
        $data_open = ($getRasioActype['Data_Open']);
        $data_total = ($getRasioActype['TOTAL']);
        $actualGaruda = $data_open / $data_total;

        return number_format($actualGaruda, 2);

    }

    public function actualCitilink()
    {
        // $allDatas = $this->M_repetitive->getAllData('Citilink');
        // $citilinks = $this->M_repetitive->getDataCitilinkPeriode();

        // $actualCitilink = count($citilinks) / count($allDatas);
        $getRasioActype = $this->M_repetitive->getRasio('CITILINK');
        $data_open = ($getRasioActype['Data_Open']);
        $data_total = ($getRasioActype['TOTAL']);
        $actualCitilink = $data_open / $data_total;

        return number_format($actualCitilink, 2);

    }

    function targetActual()
    {

        $value = array();
        $value_target = number_format($this->M_repetitive->getTarget(), 2);
        $value_garuda = $this->actualGaruda();
        $value_citilink = $this->actualCitilink();

        $value = array('target' => $value_target, 'garuda' => $value_garuda, 'citilink' => $value_citilink);
        array_push($value, $value);
        echo json_encode($value);
    }

    public function tableGaruda()
    {

        $columns = array(
            0 => 'notif',
            1 => 'actype',
            2 => 'acreg',
            3 => 'problem',
            4 => 'firstOccureDate',
            5 => 'targetAction',
            6 => 'status'
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search,
            'order_column' => $order_column,
            'order_dir' => $order_dir,
        );

        $send = $this->M_repetitive->showDatatable($data, 'Garuda');

        echo $send;

    }

    public function tableCitilink()
    {
        $columns = array(
          0 => 'notif',
          1 => 'actype',
          2 => 'acreg',
          3 => 'problem',
          4 => 'firstOccureDate',
          5 => 'targetAction',
          6 => 'status'
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search,
            'order_column' => $order_column,
            'order_dir' => $order_dir,
        );

        $send = $this->M_repetitive->showDatatable($data, 'Citilink');

        echo $send;

    }

    public function getDataChartTrend()
    {

        if($this->input->post('list_manufacture') == 00){
            $list_manufacture = '';
        }else{
            $list_manufacture = $this->input->post('list_manufacture');
        }
        if($this->input->post('list_aircraft_type') == 00){
            $list_aircraft_type = '';
        }else{
            $list_aircraft_type = $this->input->post('list_aircraft_type');
        }
        $input = array(
            'list_aircraft_type' => $list_aircraft_type,
            'list_manufacture' => $list_manufacture,
            'startPeriode' => date('Y-m-d', strtotime('01-' . $this->input->post('startPeriode'))),
            'endPeriode' => date('Y-m-d', strtotime('01-' . $this->input->post('endPeriode'))),
        );

        $categories = $this->M_repetitive->getNameMonth($input);

        $colors = array('#05354D', '#8EC3A7');

        //id GA = 12
        //id CITILINK = 11

        if ($input['list_manufacture'] == '12') {
            $garudas = $this->M_repetitive->getDataChart('Garuda', $input);
            $citilinks = null;
        } elseif ($input['list_manufacture'] == '11') {
            $garudas = null;
            $citilinks = $this->M_repetitive->getDataChart('Citilink', $input);
        } else {
            $garudas = $this->M_repetitive->getDataChart('Garuda', $input);
            $citilinks = $this->M_repetitive->getDataChart('Citilink', $input);
        }

        $result = array('categories' => $categories, 'data_garuda' => $garudas, 'data_citilink' => $citilinks);

        echo json_encode($result);
    }

    public function detailTrendData()
    {
        $input = array(
            'list_aircraft_type' => $this->input->post('list_aircraft_type'),
        );

        $aircraft = $this->input->post('aircraft');
        if ($this->input->post('month') == 'Feb') {
            $month = '02';
        } else {
            $month = date('m', strtotime($this->input->post('month')));
        }

        $columns = array(
            0 => 'notif',
            1 => 'actype',
            2 => 'acreg',
            3 => 'problem',
            4 => 'firstOccureDate',
            5 => 'lastOccureDate',
            6 => 'status',
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search,
            'order_column' => $order_column,
            'order_dir' => $order_dir,
        );

        $send = $this->M_repetitive->showDatatableDetail($data, $aircraft, $month, date('Y'), $input);

        echo $send;

    }

    public function changeColorGaruda()
    {
        if ($this->actualGaruda() >= $this->M_repetitive->getTarget()) {
            $color = '#F24738';
        } else {
            $color = '#8EC3A7';
        }

        return $color;
    }

    public function changeColorCitilink()
    {
        if ($this->actualCitilink() >= $this->M_repetitive->getTarget()) {
            $color = '#F24738';
        } else {
            $color = '#8EC3A7';
        }

        return $color;
    }


    public function viewMitigation()
    {
        $notification = (int)$this->input->post('id');

        $data = $this->M_repetitive->getMitigation($notification);

        if(count($data) != 0){

          foreach ($data as $key) {
              $why = $key['mitigasi_why'];
              $solusi = $key['mitigasi_solution'];
          }
        }else{
          $why = "";
          $solusi = "";
        }
        echo json_encode(array('why' => $why, 'solusi' => $solusi));
    }
}
