<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class User_master extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_user");
        $this->load->model("M_role");
    }

    public function index()
    {

        $data = array(
            "content" => "user_master/user_view",
            "title" => "User Managements",
            "small_tittle" => "",
            "breadcrumb" => ["User Managements"],
            "menu" => $this->M_menu->tampil(),
            "data_role" => $this->M_user->get_all_role()
        );
        $this->load->view("layouts", $data);
    }

    public function count_approved()
    {
      $total_approved = $this->M_user->count_approval();
      echo json_encode(array("total" => $total_approved->total));
    }

    public function approved()
    {
      $user_id = $this->input->post("user_id");
      $change = $this->M_user->approved($user_id);
      echo json_encode(
        array(
          "success" => (($change) ? true : false)
        )
      );
    }

    public function show_user_approval()
    {
      $data = $this->M_user->show_user_approval();
      echo json_encode($data);
    }

    public function datatable()
    {

        $columns = array(
            0 => "user_id",
            2 => "username",
            6 => "role",
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']["value"];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            "draw" => $draw,
            "length" => $length,
            "start" => $start,
            "search" => $search,
            "order_column" => $order_column,
            "order_dir" => $order_dir,
            "tipe" => $this->input->post("tipe"),
        );

        $send = $this->M_user->show_datatable($data);
        echo $send;
    }

    public function checkLdap($username)
    {
        $dn = "DC=gmf-aeroasia,DC=co,DC=id";
        $ldapconn = ldap_connect("192.168.240.57") or die ("Could not connect to LDAP server.");
        if ($ldapconn) {
            ldap_set_option(@$ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option(@$ldap, LDAP_OPT_REFERRALS, 0);
            $ldapbind = ldap_bind($ldapconn, "ldap", "aeroasia");
            @$sr = ldap_search($ldapconn, $dn, "samaccountname=$username");
            @$srmail = ldap_search($ldapconn, $dn, "mail=$username@gmf-aeroasia.co.id");
            @$info = ldap_get_entries($ldapconn, @$sr);
            @$infomail = ldap_get_entries($ldapconn, @$srmail);
            @$usermail = substr(@$infomail[0]["mail"][0], 0, strpos(@$infomail[0]["mail"][0], '@'));
            // @$bind = @ldap_bind($ldapconn, $info[0][dn], $password);
            if ((@$info[0]["samaccountname"][0] == $username) OR (@$usermail == $username)) {
                return true;
            } else {
                return false;
            }
        } else {
            echo "LDAP Connection trouble,, please try again 2/3 time";
        }
    }

    public function add()
    {

      if($this->M_user->isUnique($this->input->post('username')) == false){
        echo json_encode(
            array(
                'message' => false,
                'isUnique' => false
            )
        );
        exit();
      }
        $data = array(
            'username' => $this->input->post('username'),
            'user_isactive' => "1"
        );

        $errors = array();
        $success = true;
        $checkLdap = $this->checkLdap($data["username"]);
        if ($checkLdap) {
            $id = $this->M_user->input_user($data, 'user');
            $data1 = array(
                'role_id' => $this->input->post('role'),
                'user_id' => $id,
            );
            $res = $this->M_user->input_role($data1, 'user_role');
            $success = true;
        } else {
            $success = false;
            array_push($errors, "LDAP for Username not found");
        }
        echo json_encode(
            array(
                'message' => $success,
                'errors' => $errors
            )
        );
    }

    public function edit()
    {

        $data = array(
            'username' => $this->input->post('username')
        );
        $id = $this->input->post('id_user');
        $this->M_user->edit_user($data, 'user', $id);

        $data1 = array(
            'role_id' => $this->input->post('role'),
        );

        $this->M_user->edit_role($data1, 'user_role', $id);
        echo json_encode(
            array(
                'message' => "success",
            )
        );
    }


    public function delete_user($id)
    {
        echo json_encode(
            array("success" => ($this->M_user->del_user($id)) ? true : false)
        );
    }


    public function view()
    {
        $id = $this->input->post('id');
        $data = $this->M_user->get_user($id);
        echo json_encode(array(
            'message' => 'succes',
            'user_id' => $data->user_id,
            'role'=> $data->role
        ));
        echo json_encode($data);
    }

    public function user_json($id = null)
    {
      if($id == null){
        $data = $this->M_user->get_all_user();
      } else {
        $data = $this->M_user->get_all_user($id);
      }
      echo json_encode($data);
    }

    public function delete_unit()
    {
      $kpi_unit_id = $this->input->post("id");
      $delete = $this->M_user->delete_unit_where($kpi_unit_id);
      $data["success"] = (($delete) ? true : false);
      echo json_encode($data);
    }

    //DIMAS
    public function datatables_new() {
        $tipe = $this->input->post('tipe');
        $list = $this->M_user->get_datatables_new($tipe);
            $data = array();
            $no   = $_POST['start'];
            foreach ($list as $user) {
                    $no++;
                    $row   = array();
                    $row[] = '<center>'.$no.'</center>';
                    $row[] = '<center>'.$user->username.'</center>';
                    $row[] = '<center>'.$user->role_name.'</center>';
                    $row[] = '<center> <button type="button" data-id=' . $user->user_id . ' class="btn btn-danger btn-flat" id="del_user" name="button"><i class="fa fa-trash-o"></i></button>
			<a data-id=' . $user->user_id . '  class="btn btn-default btn-flat" id="edit_user" name="button"><i class="fa fa-wrench"></i></a> </center>';
                    $data[] = $row;
        }
        $output = array(
                    "draw"            => $_POST['draw'],
                    "recordsTotal"    => $this->M_user->count_all_new($tipe),
                    "recordsFiltered" => $this->M_user->count_filtered_new($tipe),
                    "data"            => $data,
        );
        echo json_encode($output);
    }
    function detail_view()
    {
        $id = $this->input->post('id');
        $data = $this->M_user->get_user($id);
        echo json_encode($data);
    }

    function delete_user_new()
    {
        $data = array(
            'user_isactive' => '0'
        );
        $this->M_user->delete_user_new(array('user_id' => $this->input->post('id_user')), $data);
        echo json_encode(array("status" => TRUE));
    }

}
