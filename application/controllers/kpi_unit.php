<?php

defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');

class Kpi_unit extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        // $this->load->helper("unitassign_helper");
        $this->load->helper("hierarchyitem_helper");
        $this->load->helper("date");
        $this->load->model("M_kpi_unit");
        $this->load->model("M_kpi_sla_master");
    }

    public function buildTree(array $elements, $parentId = 0)
    {
        $kpiItem = array();

        foreach ($elements as $element) {
            if ($element->kia_parent_id == $parentId) {
                $children = $this->buildTree($elements, $element->kia_id);
                if ($children) {
                    $element->children = $children;
                }
                $kpiItem[] = $element;
            }
        }

        return $kpiItem;
    }

    public function showAllRole()
    {
      $theRole = $this->M_kpi_unit->show_all_role();
      return $theRole;
    }

    public function index()
    {
        $isLocked = $this->M_kpi_unit->check_lock();
        $role = $this->session->userdata("new_role_id");
        $formula = $this->M_kpi_unit->getAllFormula();
        // $allItem = $this->M_kpi_unit->getAllItemKpi($role);
        // $recursive = $this->buildTree($allItem);
        $deep_child = $this->M_kpi_unit->get_deep_child($role);
        $data = array(
            "content" => "kpi/v_kpi_unit",
            "title" => "KPI Management",
            "formula" => $formula,
            "allRole" => $this->showAllRole(),
            "deep_child" => $deep_child,
            "small_tittle" => "(Unit)",
            "breadcrumb" => ["KPI Management"],
            "menu" => $this->M_menu->tampil(),
            "isLocked" => $isLocked,
            "role_access" => $this->session->userdata("role"),
            "new_role_access" => $this->session->userdata("new_role"),
        );
        $this->load->view("layouts", $data);
    }

    public function allkpi($year, $role)
    {
        // if($year == ""){
        //   $year = date("Y");
        // }
        // $role = $this->session->userdata("roleid");
        $tahun = $this->M_kpi_unit->getAllItemKpi($role, $year);
        $cek = generatePageTreeUnit($tahun);
        echo "<select class='form-control' id='parent' name='level'>";
        echo "<option data-level='1' value=''>Is Parent</option>";
        echo $cek;
        echo "</select>";
    }

    public function listGrup()
    {
      $data = $this->M_kpi_unit->loadGrup();
      $string = "";
      $no = 1;
      $role_id = $this->input->get('role');
      foreach ($data as $list) {
        $string .= "<tr>";
        $string .= "<td style='text-align:center;'>".$no."</td>";
        $string .= "<td style='text-align:center;'>".$list->kpi_group_name."</td>";
        $string .= "<td style='text-align:center;'>
                      <button class='btn btn-default btn-flat' data-name='".$list->kpi_group_name."' data-id='".$list->kpi_group_id."' data-roleid='".$role_id."' id='manage_grup'><i class='fa fa-pencil'></i></button>

                   </td>";
                   // <button class='btn bg-navy btn-flat' data-id='".$list->kpi_group_id."' data-roleid='".$role_id."' id='edit_group'><i class='fa fa-wrench'></i></button>
                   // <button class='btn btn-danger btn-flat' data-id='".$list->kpi_group_id."' data-roleid='".$role_id."' id='delete_group'><i class='fa fa-trash'></i></button>
        $string .= "</tr>";
        $no++;
      }
      $arr = array("success" => true, "html" => $string);
      echo json_encode($arr);
    }

    public function member_group($id,$role)
    {
      $data = $this->M_kpi_unit->member_group_where($id,$role);
      echo json_encode($data);
    }

    public function save_member()
    {
      $kpi_group_id = $this->input->post("kpi_group_id");
      $ki_id = $this->input->post("ki_id");
      $role = $this->input->post("role");
      $update = $this->M_kpi_unit->set_member_where($ki_id, $kpi_group_id,$role);
      $result = array();
      $result["success"] = $update;
      echo json_encode($result);
    }

    public function list_parent($role)
    {
      $data = $this->M_kpi_unit->get_all_parent($role);
      echo json_encode($data);
    }

    public function insert()
    {
        $insert = $this->M_kpi_unit->saveKpi();
        echo json_encode($insert);
    }

    public function list_with_detail()
    {
        $allKpi = $this->M_kpi_unit->getAllItemKpiWithDetail();
        $role = $this->input->post("role");
        $nowYear = $this->input->post("year");
        $isLocked = $this->M_kpi_unit->check_lock();
        $html = $this->generatePageTreeTableUnit($allKpi, $nowYear, $role, $isLocked);
        echo json_encode(array(
            "success" => true,
            "htmlBuild" => $html
        ));
    }

    public function manage_detail()
    {
        $save = $this->M_kpi_unit->saveDetailItem();
        $result = array();
        if ($save) {
            $result["success"] = true;
        } else {
            $result["success"] = false;
        }
        echo json_encode($result);
    }

    public function detail_item($ki_id, $year)
    {
        $data = $this->M_kpi_unit->getDetailWhere($ki_id, $year);
        echo json_encode($data);
    }

    public function all_list_unit()
    {
        $allListUnit = $this->M_kpi_unit->list_unit();
        echo json_encode($allListUnit);
    }

    // public function edit_item($id)
    // {
    //     $data = $this->M_kpi_unit->edit($id);
    //     echo json_encode($data);
    // }

    public function insert_actual()
    {
        $allListUnit = $this->M_kpi_unit->add_value_actual();
        echo json_encode($allListUnit);
    }

    public function delete_item($kiad_id)
    {
        $data = $this->M_kpi_unit->delete_kia_where($kiad_id);
        echo json_encode($data);
    }

    public function get_weight_item($kia_id)
    {
        $data = $this->M_kpi_unit->edit($kia_id);
        echo json_encode($data);
    }

    public function duplicate()
    {
        $copy_year1 = $this->input->post("copy_year1");
        $copy_year2 = $this->input->post("copy_year2");
        $role_id = $this->session->userdata("new_role_id");

        $data = $this->M_kpi_unit->select_kpi_where_year($copy_year1, $role_id);
        $tree = $this->buildTree($data);
        $copy_kpi = $this->M_kpi_unit->copy_kpi($tree);
        echo json_encode(array("success" => true));
    }

    public function get_ytd()
    {
      $act = $this->input->post("act");
      $kia_name_act = $this->input->post("kia_name_act");
      $month = $this->input->post("month");
      $year = $this->input->post("year");
      $fa_function_name = $this->input->post("fa_function_name");
      $data_ytd = $this->M_kpi_unit->get_ytd($fa_function_name, $act, $kia_name_act, $month, $year);
      echo json_encode(array("kiad_actual_ytd" => number_format($data_ytd, 2)));
      // $tes = array($act, $kia_name_act, $month, $year);
    }

    // TAMBAHAN DIMAS
    public function remove_group($id){
        $data = array(
            'kpi_group_id' => null
        );
        $this->M_kpi_unit->remove_group(array('kia_id' => $id), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_item($id){
        $cek    = $this->M_kpi_unit->cek($id);
        $month  = date('m');
        $year   = date('Y');
        foreach ($cek as $key) {
            // if($key->kia_level == '' && $key->kia_parent_id == 0){
                $data = $this->M_kpi_unit->edit($id);
            // }else{
                // $data = $this->M_kpi_unit->edit_from_assign($id,$month,$year);
                // echo "<pre>"; print_r($data); echo "</pre>"; exit();
            // }
        }

        echo json_encode($data);
    }

    public function get_weight_item_detail($kia_id){
        $data = $this->M_kpi_unit->show_value_month($kia_id);
        echo json_encode($data);
    }



    function generatePageTreeTableUnit($datas,$year,$role, $isLocked, $depth = 1, $parent = 0){
    // echo json_encode($datas);exit;

    
    if($depth > 1000) return '';
    $deepChild = 1;
    $tree = '';
    for($i=0, $ni=count($datas); $i < $ni; $i++){
      if($datas[$i]->kia_parent_id == $parent){
        $target = explode("|", $datas[$i]->kiad_target);
        $targetYear = explode("|", $datas[$i]->kia_target);
        $targetYtd = explode("|", $datas[$i]->kiad_target_ytd);
        $targetYearYtd = explode("|", $datas[$i]->kia_target_ytd);
        $tree .= "<tr data-id='".$datas[$i]->kia_id."' data-parent='".$datas[$i]->kia_parent_id."' data-level='".$depth."'> <td data-column='name'> L".$depth."&nbsp;&nbsp;-".str_repeat('&nbsp;&nbsp;&nbsp;', $depth);
        $tree .= $datas[$i]->kia_name."</td>";
        $tree .= "<td>".$datas[$i]->kia_uom."</td>";
        $weight = 0;
        if($depth == 1){
        $weight = 0;
      }  else{
        $weight = $datas[$i]->kia_weight;
      }

      $buttonEdit = "<button data-id='".$datas[$i]->kia_id."' data-level='".$depth."' data-formula='".$datas[$i]->fa_function_name."' id='editKpi' class='btn btn-default btn-flat'><i class='fa fa-pencil'></i></button>";
        if(getDeepChildUnit($datas[$i]->kia_id,$role) == 0){
          $tree .= "<td>".$datas[$i]->kf_name."</td>";
          $tree .= "<td>".(($datas[$i]->kiad_weight != null || $datas[$i]->kiad_weight != "" || $datas[$i]->kiad_weight == "0") ? $datas[$i]->kiad_weight : $datas[$i]->kia_weight)."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kiad_target && count($target) == 1) ? $datas[$i]->kiad_target : " ".(!empty($datas[$i]->kia_target && count($targetYear) == 1) ? $datas[$i]->kia_target : "<span class='text-danger'>-</span>"))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kiad_target_ytd && count($targetYtd) == 1) ? $datas[$i]->kiad_target_ytd : " ".(!empty($datas[$i]->kia_target_ytd && count($targetYearYtd) == 1) ? $datas[$i]->kia_target_ytd : "<span class='text-danger'>-</span>"))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kiad_target && count($target) > 1) ? $target[0] : " ".(!empty($datas[$i]->kia_target && count($targetYear) > 1) ? $targetYear[0] : "<span class='text-danger'>-</span>" ))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kiad_target && count($target) > 1) ? $target[1] : " ".(!empty($datas[$i]->kia_target && count($targetYear) > 1) ? $targetYear[1] : "<span class='text-danger'>-</span>" ))."</td>";
          // YTD
          $tree .= "<td>".(!empty($datas[$i]->kiad_target_ytd && count($targetYtd) > 1) ? $targetYtd[0] : " ".(!empty($datas[$i]->kia_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[0] : "<span class='text-danger'>-</span>" ))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kiad_target_ytd && count($targetYtd) > 1) ? $targetYtd[1] : " ".(!empty($datas[$i]->kia_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[1] : "<span class='text-danger'>-</span>" ))."</td>";
          // END
          $tree .= "<td>".(!empty($datas[$i]->kiad_limit) ? $datas[$i]->kiad_limit : "<span class='text-danger'>-</span>")."</td>";
          if($isLocked == false || $this->session->userdata("new_role") == "td_corporate" || $this->session->userdata("role") == "Super Admin"){
            $tree .= "<td>";
            $tree .= "<button type='button' onclick='detail_item(\"".$datas[$i]->fa_function_name."\",\"".$datas[$i]->kia_uom."\",\"".$datas[$i]->kia_name."\",\"".$datas[$i]->kia_id."\",\"".$depth."\",\"".$datas[$i]->kia_target_ytd."\", \"".$datas[$i]->kia_target."\")' class='btn btn-flat bg-navy'><i class='fa fa-wrench'></i></button>";
            if(empty($datas[$i]->ki_id)){
              $tree .= "<button type='button' id='delete' data-id='".$datas[$i]->kia_id."' class='btn btn-flat btn-danger'><i class='fa fa-trash'></i></button>";
            }
            $tree .= $buttonEdit;
            $tree .= "</td>";
          }
        } else{
        //   $maxLevel = $this->M_kpi_sla_master->maxLevel($datas[$i]->kia_id);
        //   $finalTotalScores=$this->FunctionName($maxLevel,$datas[$i]->kia_id);

          $tree .= "<td>Aggregate Score</td>";
          $tree .= "<td>".(($datas[$i]->kia_weight != null || $datas[$i]->kia_weight != "" || $datas[$i]->kia_weight == "0") ? $weight : "<span class='text-danger'>0</span>")."</td>";
          $tree .= "<td>".(($datas[$i]->kia_target != null && $datas[$i]->kia_target != "" && $datas[$i]->kia_target != "0" && count($targetYear) == 1) ? $datas[$i]->kia_target : "<span class='text-danger'>-</span>")."</td>";
          $tree .= "<td>".(($datas[$i]->kia_target_ytd != null && $datas[$i]->kia_target_ytd != "" && $datas[$i]->kia_target_ytd != "0" && count($targetYearYtd) == 1) ? $datas[$i]->kia_target_ytd : "<span class='text-danger'>-</span>")."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kia_target && count($targetYear) > 1) ? $targetYear[0] : "<span class='text-danger'>-</span>" )."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kia_target && count($targetYear) > 1) ? $targetYear[1] : "<span class='text-danger'>-</span>" )."</td>";
          // YTD
          $tree .= "<td>".(!empty($datas[$i]->kia_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[0] : "<span class='text-danger'>-</span>" )."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kia_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[1] : "<span class='text-danger'>-</span>" )."</td>";
          // END
          $tree .= "<td>"."<span class='text-danger'>-</span>"."</td>";
          if($isLocked == false || $this->session->userdata("new_role") == "td_corporate" || $this->session->userdata("role") == "Super Admin"){
            $tree .= "<td>";
            if(empty($datas[$i]->ki_id)){
              $tree .= "<button type='button' id='delete' data-id='".$datas[$i]->kia_id."' class='btn btn-flat btn-danger'><i class='fa fa-trash'></i></button>";
            }
            // $tree .= "<button type='button' id='delete' data-id='".$datas[$i]->kia_id."' class='btn btn-flat btn-danger'><i class='fa fa-trash'></i></button>";
            $tree .= $buttonEdit;
            $tree .= "</td>";
          }
        }
        $tree .= "</tr>";
       $tree .= $this->generatePageTreeTableUnit($datas, $year,$role,$isLocked, $depth+1, $datas[$i]->kia_id);
     }
   }
   return $tree;
  }


  

}


?>
