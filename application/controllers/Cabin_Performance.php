<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Cabin_Performance extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("M_cabin_performance");
        date_default_timezone_set('Asia/Jakarta');
    }

    function dashboard()
    {
        $data = array(
            "content" => "Cabin Performance/index",
            "title" => "Cabin Performance",
            "title_box" => "Cabin Performance Status",
            "small_tittle" => "",
            "breadcrumb" => ["Cabin Performance"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function master_data()
    {
        $data = array(
            "content" => "Cabin Performance/master_data",
            "title" => "Master Data Cabin Performance",
            "small_tittle" => "",
            "breadcrumb" => ["Master Data  Cabin Performance"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }


//=====================================================COMBOBOX=====================================================\\

    function combo_aircraft_type()
    {
        $data = $this->M_cabin_performance->combo_aircraft_type();
        echo json_encode($data);
    }

    function combo_aircraft_reg($id_aircraft_type)
    {
        if (count($id_aircraft_type) == 0) {
            $id_aircraft_type = "00";
        } else if (count($id_aircraft_type) == 1) {
            $id_aircraft_type = $id_aircraft_type;
        } else {
            $id_aircraft_type = str_replace("-", " ", strtoupper($id_aircraft_type));
            $id_aircraft_type = explode('%20', $id_aircraft_type);
        }
        $data = $this->M_cabin_performance->combo_aircraft_reg($id_aircraft_type);
        echo json_encode($data);
    }


//=====================================================AKUMULASI=====================================================\\

    function data_akumulasi($tgl1, $tgl2)
    {
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $thn = $pecah['0'];
        $target = $this->M_cabin_performance->akumulasi_target($thn);
        $in_perform = $this->M_cabin_performance->akumulasi_interior_performance($format_tgl1, $format_tgl2);
        $ex_perform = $this->M_cabin_performance->akumulasi_exterior_performance($format_tgl1, $format_tgl2);
        $funct_perform = $this->M_cabin_performance->akumulasi_functionality_performance($format_tgl1, $format_tgl2);
        $count_performance_by_item = $this->M_cabin_performance->count_performance_by_item($format_tgl1, $format_tgl2);

        $data_in = array();
        $data_ex = array();
        $data_funct = array();
        $target_all = 0;

        foreach ($target as $target_key) {
            if ($target_key['target_val'] == '') {
                $target_all = 0;
            } else {
                $target_all += $target_key['target_val'];
            }
        }

        foreach ($in_perform as $in_perform_key) {
            $categories = $in_perform_key['name_manf'];
            $tipe = 'Interior';
            if (count($in_perform) == 0) {
                $in_performance = 0;
                $performance_in_count = 0;
            } else {
                $in_performance = round(((($in_perform_key['total_item'] - ($in_perform_key['total_defect'] + $in_perform_key['total_dirty'])) / $in_perform_key['total_item']) * 100), 2);
                $performance_in_count = count($in_performance);
            }
            $value = array('tipe' => $tipe, 'categories' => $categories, 'data_performance' => $in_performance, 'count_perform' => $performance_in_count);
            array_push($data_in, $value);
        }


        foreach ($ex_perform as $ex_perform_key) {
            $categories = $ex_perform_key['name_manf'];
            $tipe = 'Exterior';
            if (count($ex_perform) == 0) {
                $ex_performance = 0;
                $performance_ex_count = 0;
            } else {
                $ex_performance = round(($ex_perform_key['result_value'] / $ex_perform_key['jumlah_trans_exterior']), 2);
                $performance_ex_count = count($ex_performance);
            }
            $value = array('tipe' => $tipe, 'categories' => $categories, 'data_performance' => $ex_performance, 'count_perform' => $performance_ex_count);
            array_push($data_ex, $value);
        }


        //===================================DATA FUNCTIONAL===================================\\
        /* CODING LAMA
        foreach ($funct_perform as $funct_perform_key) {
            $categories = $funct_perform_key['name_manf'];
            $tipe = 'Functionality';
            if (count($funct_perform) == 0) {
                $funct_performance = 0;
                $performance_funct_count = 0;
            } else {
                $funct_performance = round(((($funct_perform_key['total_item'] - $funct_perform_key['total_defect']) / $funct_perform_key['total_item']) * 100), 2);
                $performance_funct_count = count($funct_performance);
            }
            $value = array('tipe' => $tipe, 'categories' => $categories, 'data_performance' => $funct_performance, 'count_perform' => $performance_funct_count);
            array_push($data_funct, $value);
        }*/
        /* CODING BARU */
        $array_functional_performance = [];
        foreach ($funct_perform as $key) {
            $id_man = $key['id'];
            $man = $key['name_manf'];
            $id_item = $key['id_item'];
            $nama = $key['nama_item'];
            $perform = ($key['total_item_sub'] - $key['total_defects_item_sub']) / $key['total_item_sub'];
            $value = array('id' => $id_man, 'name_manf' => $man, 'id_item' => $id_item, 'nama_item' => $nama, 'perform' => $perform);
            $array_functional_performance[] = $value;
        }
        $groups_functional = array();
        foreach ($array_functional_performance as $item) {
            $key = $item['id_item'];
            if (!isset($groups_functional[$key])) {
                $groups_functional[$key] = array(
                    'id_man' => $item['id'],
                    'name_manf' => $item['name_manf'],
                    'id_item' => $key,
                    'nama_item' => $item['nama_item'],
                    'perform' => $item['perform'],
                    'count' => count($key),
                );
            } else {
                $groups_functional[$key]['id_man'] = $groups_functional[$key]['id_man'];
                $groups_functional[$key]['name_manf'] = $groups_functional[$key]['name_manf'];
                $category = $groups_functional[$key]['id_item'];
                $groups_functional[$key]['nama_item'] = $groups_functional[$key]['nama_item'];
                $groups_functional[$key]['perform'] = $groups_functional[$key]['perform'] + $item['perform'];
            }
        }
        $array_groups_functional = [];
        foreach ($groups_functional as $groups_functional_key => $groups_functional_value) {
            foreach ($count_performance_by_item as $count_performance_by_item_key) {
                if ($count_performance_by_item_key['id_item'] == $groups_functional[$groups_functional_key]['id_item']) {
                    $id_man = $groups_functional[$groups_functional_key]['id_man'];
                    $man = $groups_functional[$groups_functional_key]['name_manf'];
                    $perform = round((($groups_functional[$groups_functional_key]['perform'] / $count_performance_by_item_key['count']) * 100), 2);
                    $value = array('id_man' => $id_man, 'name_manf' => $man, 'perform' => $perform);
                    $array_groups_functional[] = $value;
                }
            }
        }
        $array_data_functional = array();
        foreach ($array_groups_functional as $item) {
            $key = $item['id_man'];
            if (!isset($array_data_functional[$key])) {
                $array_data_functional[$key] = array(
                    'id_man' => $key,
                    'name_manf' => $item['name_manf'],
                    'perform' => $item['perform'],
                    'count' => count($array_groups_functional)
                );
            } else {
                $category = $array_data_functional[$key]['id_man'];
                $array_data_functional[$key]['name_manf'] = $array_data_functional[$key]['name_manf'];
                $array_data_functional[$key]['perform'] = ($array_data_functional[$key]['perform'] + $item['perform']);
            }
        }
        foreach ($array_data_functional as $key) {
            $perform = $key['perform'] / $key['count'];
            $value = array('tipe' => 'Functionality', 'categories' => $key['name_manf'], 'data_performance' => $perform, 'count_perform' => count($array_data_functional));
            $data_funct[] = $value;
        }

        //===========================PENGGABUNGAN DATA INTERIOR, EXTERIOR DAN FUNCTIONALITY===========================\\
        $merge = array_merge($data_in, $data_ex, $data_funct);
        $groups = array();
        $group_categories = array();
        $group_data_target = array();
        $group_data_performance = array();
        $group_global = array();
        foreach ($merge as $item) {
            $key = $item['categories'];
            if (!isset($groups[$key])) {
                $groups[$key] = array(
                    'categories' => $key,
                    'count_perform' => $item['count_perform'],
                    'data_performance' => $item['data_performance'],
                );
            } else {
                $category = $groups[$key]['categories'];
                $groups[$key]['count_perform'] = $groups[$key]['count_perform'] + $item['count_perform'];
                $groups[$key]['data_performance'] = $groups[$key]['data_performance'] + $item['data_performance'];
                $data_performance = round(($groups[$key]['data_performance'] / $groups[$key]['count_perform']), 2);
            }
        }

        //===========================Data FIX===========================\\
        array_push($group_categories, $key);
        array_push($group_data_target, $target_all);
        array_push($group_data_performance, $data_performance);
        echo json_encode(array('categories' => $group_categories, 'data_target' => $group_data_target, 'data_performance' => $group_data_performance));
    }

//==================================================INTERIOR==================================================\\

    function data_interior($tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $categories = array();
        $value_target = array();
        $value_performance = array();
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $thn = $pecah['0'];
        $id_aircraft_type = $this->input->post('aircraft_type');
        $id_aircraft_reg = $this->input->post('aircraft_reg');

        if (count($id_aircraft_type) == 1) {
            $id_aircraft_type = $id_aircraft_type;
        } else if ($id_aircraft_type == '') {
            $id_aircraft_type = 00;
        } else {
            $id_aircraft_type = str_replace("-", " ", strtoupper($id_aircraft_type));
            $id_aircraft_type = explode('%20', $id_aircraft_type);
        }
        if (count($id_aircraft_reg) == 1) {
            $id_aircraft_reg = $id_aircraft_reg;
        } else if ($id_aircraft_reg == '') {
            $id_aircraft_reg = 00;
        } else {
            $id_aircraft_reg = str_replace("-", " ", strtoupper($id_aircraft_reg));
            $id_aircraft_reg = explode('%20', $id_aircraft_reg);
        }
        $interior_target = $this->M_cabin_performance->data_interior_target($thn);
        $interior_performance = $this->M_cabin_performance->data_interior_performance($id_aircraft_type, $id_aircraft_reg, $format_tgl1, $format_tgl2);

        foreach ($interior_target as $interior_target_key => $interior_target_value) {
            foreach ($interior_performance as $interior_performance_key) {
                $name = $interior_performance_key['tipe_aircraft'];
                $target = round(($interior_target_value['target']), 1);
                $performance = round(((($interior_performance_key['total_item'] - ($interior_performance_key['total_defect'] + $interior_performance_key['total_dirty'])) / $interior_performance_key['total_item']) * 100), 2);
                array_push($categories, $name);
                array_push($value_target, $target);
                array_push($value_performance, $performance);
            }
        }
        echo json_encode(array("categories" => $categories, "data_target" => $value_target, "data_performance" => $value_performance));
    }

    function reg_interior($id_aircraft_type, $id_aircraft_reg)
    {
        $query = $this->M_cabin_performance->reg_interior($id_aircraft_type, $id_aircraft_reg);
        echo json_encode($query);
    }

    function aspek_interior($id_aircraft_reg, $tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $data = array();
        $id = array();
        $categories = array();
        $value_target = array();
        $value_performance = array();
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $thn = $pecah['0'];
        $aspek_interior_target = $this->M_cabin_performance->aspek_interior_target($thn);
        $aspek_interior_performance = $this->M_cabin_performance->aspek_interior_performance($id_aircraft_reg, $format_tgl1, $format_tgl2);
        $master_interior = $this->M_cabin_performance->master_interior();
        $jumlah_interior = $this->M_cabin_performance->jumlah_interior($id_aircraft_reg, $format_tgl1, $format_tgl2);

        foreach ($jumlah_interior as $jumlah_interior_key) {
            $total_data = $jumlah_interior_key['jumlah_item'];
        }

        if (count($aspek_interior_target) == 0) {
            foreach ($master_interior as $master_interior_key => $master_interior_value) {
                $id = $master_interior_value['id_item'];
                $name = $master_interior_value['nama_item'];
                $target = 0;
                $performance = 0;
                foreach ($aspek_interior_performance as $aspek_interior_performance_key) {
                    if ($aspek_interior_performance_key['id_item'] == $master_interior[$master_interior_key]['id_item']) {
                        $performance = round((((($aspek_interior_performance_key['total_item'] - ($aspek_interior_performance_key['total_defect'] + $aspek_interior_performance_key['total_dirty'])) / $aspek_interior_performance_key['total_item']) * 100) / $total_data), 2);
                    }
                }
                $value = array("id_item" => $id, "categories" => $name, "data_target" => $target, "data_performance" => $performance);
                array_push($data, $value);
            }
        } else {
            foreach ($aspek_interior_target as $aspek_interior_target_key => $aspek_interior_target_value) {
                $id = $aspek_interior_target_value['id_item'];
                $name = $aspek_interior_target_value['nama_item'];
                $target = round(($aspek_interior_target_value['target']), 1);
                $performance = 0;
                foreach ($aspek_interior_performance as $aspek_interior_performance_key) {
                    if ($aspek_interior_performance_key['id_item'] == $aspek_interior_target[$aspek_interior_target_key]['id_item']) {
                        $performance += round((((($aspek_interior_performance_key['total_item'] - ($aspek_interior_performance_key['total_defect'] + $aspek_interior_performance_key['total_dirty'])) / $aspek_interior_performance_key['total_item']) * 100) / $total_data), 2);
                    }
                    if ($performance == 0) {
                        $performance += 100;
                    }
                }
                $value = array("id_item" => $id, "categories" => $name, "data_target" => $target, "data_performance" => $performance);
                array_push($data, $value);
            }
        }

        echo json_encode($data);
    }

    function detail_sub_interior($id_reg, $id_item, $tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $data = array();
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $query = $this->M_cabin_performance->detail_sub_interior($id_reg, $id_item, $format_tgl1, $format_tgl2);
        echo json_encode($query);
    }

    function mitigasi_interior($id_reg, $id_item, $tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $last_date = date('t');
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $type_mitigasi = 'Interior';
        $query = $this->M_cabin_performance->mitigasi_interior($id_reg, $id_item, $type_mitigasi, $format_tgl1, $format_tgl2);
        echo json_encode($query);
    }


//==================================================EXTERIOR==================================================\\

    function data_exterior($tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $categories = array();
        $value_target = array();
        $value_performance = array();
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $thn = $pecah['0'];
        $id_aircraft_type = $this->input->post('aircraft_type');
        $id_aircraft_reg = $this->input->post('aircraft_reg');

        if (count($id_aircraft_type) == 1) {
            $id_aircraft_type = $id_aircraft_type;
        } else if ($id_aircraft_type == '') {
            $id_aircraft_type = 00;
        } else {
            $id_aircraft_type = str_replace("-", " ", strtoupper($id_aircraft_type));
            $id_aircraft_type = explode('%20', $id_aircraft_type);
        }
        if (count($id_aircraft_reg) == 1) {
            $id_aircraft_reg = $id_aircraft_reg;
        } else if ($id_aircraft_reg == '') {
            $id_aircraft_reg = 00;
        } else {
            $id_aircraft_reg = str_replace("-", " ", strtoupper($id_aircraft_reg));
            $id_aircraft_reg = explode('%20', $id_aircraft_reg);
        }
        $exterior_target = $this->M_cabin_performance->data_exterior_target($thn);
        $exterior_performance = $this->M_cabin_performance->data_exterior_performance($id_aircraft_type, $id_aircraft_reg, $format_tgl1, $format_tgl2);

        foreach ($exterior_target as $exterior_target_key => $exterior_target_value) {
            foreach ($exterior_performance as $exterior_performance_key) {
                $name = $exterior_performance_key['tipe_aircraft'];
                $target = round(($exterior_target_value['target']), 1);
                $performance = round(($exterior_performance_key['result_value'] / $exterior_performance_key['jumlah_trans_exterior']), 1);
                array_push($categories, $name);
                array_push($value_target, $target);
                array_push($value_performance, $performance);
            }
        }
        echo json_encode(array("categories" => $categories, "data_target" => $value_target, "data_performance" => $value_performance));
    }

    function reg_exterior($id_aircraft_type, $id_aircraft_reg)
    {
        $query = $this->M_cabin_performance->reg_exterior($id_aircraft_type, $id_aircraft_reg);
        echo json_encode($query);
    }


    function aspek_exterior($id_aircraft_reg, $tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $data = array();
        $id = array();
        $categories = array();
        $value_target = array();
        $value_performance = array();
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $thn = $pecah['0'];
        $aspek_exterior_target = $this->M_cabin_performance->aspek_exterior_target($thn);
        $aspek_exterior_performance = $this->M_cabin_performance->aspek_exterior_performance($id_aircraft_reg, $format_tgl1, $format_tgl2);
        $master_exterior = $this->M_cabin_performance->master_exterior();
        // $jumlah_exterior                = $this->M_cabin_performance->jumlah_exterior($id_aircraft_reg, $format_tgl1, $format_tgl2);
        // foreach ($jumlah_exterior as $jumlah_exterior_key){
        //   $total_data = $jumlah_exterior_key['jumlah_item'];
        // }

        if (count($aspek_exterior_target) == 0) {
            foreach ($master_exterior as $master_exterior_key => $master_exterior_value) {
                $id = $master_exterior_value['id_item'];
                $name = $master_exterior_value['nama_item'];
                $target = 0;
                $performance = 0;
                foreach ($aspek_exterior_performance as $aspek_exterior_performance_key) {
                    if ($aspek_exterior_performance_key['id_item'] == $master_exterior[$master_exterior_key]['id_item']) {
                        $performance += round((($aspek_exterior_performance_key['result_value'] / $aspek_exterior_performance_key['jumlah_trans_exterior']) / $total_data), 2);
                    }
                }
                $value = array("id_item" => $id, "categories" => $name, "data_target" => $target, "data_performance" => $performance);
                array_push($data, $value);
            }
        } else {
            foreach ($aspek_exterior_target as $aspek__exterior_target_key => $aspek_exterior_target_value) {
                $id = $aspek_exterior_target_value['id_item'];
                $name = $aspek_exterior_target_value['nama_item'];
                $target = round(($aspek_exterior_target_value['target']), 1);
                $performance = 0;
                foreach ($aspek_exterior_performance as $aspek_exterior_performance_key) {
                    if ($aspek_exterior_performance_key['id_item'] == $aspek_exterior_target[$aspek__exterior_target_key]['id_item']) {
                        $performance += round(($aspek_exterior_performance_key['result_value'] / $aspek_exterior_performance_key['jumlah_trans_exterior']), 2);
                    }
                }
                $value = array("id_item" => $id, "categories" => $name, "data_target" => $target, "data_performance" => $performance);
                array_push($data, $value);
            }
        }
        echo json_encode($data);
    }

    function detail_sub_exterior($id_reg, $id_item, $tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $data = array();
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $query = $this->M_cabin_performance->detail_sub_exterior($id_reg, $id_item, $format_tgl1, $format_tgl2);
        echo json_encode($query);
    }

    function mitigasi_exterior($id_reg, $id_item, $tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $type_mitigasi = 'Exterior';
        $query = $this->M_cabin_performance->mitigasi_exterior($id_reg, $id_item, $type_mitigasi, $format_tgl1, $format_tgl2);
        echo json_encode($query);
    }


//==================================================FUNCTIONALITY==================================================\\

    function data_functional_by_reg($tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $categories = array();
        $value_target = array();
        $value_performance = array();
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah1 = explode("-", $tgl1);
        $thn = $pecah1['0'];
        $id_aircraft_type = $this->input->post('aircraft_type');
        $id_aircraft_reg = $this->input->post('aircraft_reg');


        if (count($id_aircraft_type) == 1) {
            $id_aircraft_type = $id_aircraft_type;
        } else if ($id_aircraft_type == '') {
            $id_aircraft_type = 00;
        } else {
            $id_aircraft_type = str_replace("-", " ", strtoupper($id_aircraft_type));
            $id_aircraft_type = explode('%20', $id_aircraft_type);
        }
        if (count($id_aircraft_reg) == 1) {
            $id_aircraft_reg = $id_aircraft_reg;
        } else if ($id_aircraft_reg == '') {
            $id_aircraft_reg = 00;
        } else {
            $id_aircraft_reg = str_replace("-", " ", strtoupper($id_aircraft_reg));
            $id_aircraft_reg = explode('%20', $id_aircraft_reg);
        }
        $functional_target = $this->M_cabin_performance->data_functional_target_by_reg($thn);
        $functional_performance = $this->M_cabin_performance->data_functional_performance_by_reg($id_aircraft_type, $id_aircraft_reg, $format_tgl1, $format_tgl2);

        foreach ($functional_target as $functional_target_key => $functional_target_value) {
            foreach ($functional_performance as $functional_performance_key) {
                $id = $functional_performance_key['id_aircraft'];
                $name = $functional_performance_key['tipe_aircraft'];
                $target = round(($functional_target_value['target']), 2);
                $performance = round(((($functional_performance_key['total_item'] - $functional_performance_key['total_defect']) / $functional_performance_key['total_item']) * 100), 2);
                array_push($categories, $name);
                array_push($value_target, $target);
                array_push($value_performance, $performance);
            }
        }
        echo json_encode(array("categories" => $categories, "data_target" => $value_target, "data_performance" => $value_performance));
    }

    function data_functional_by_item($tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $categories = array();
        $value_target = array();
        $value_performance = array();
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah1 = explode("-", $tgl1);
        $thn = $pecah1['0'];

        $functional_target = $this->M_cabin_performance->data_functional_target_by_item($thn);
        $functional_performance = $this->M_cabin_performance->data_functional_performance_by_item($format_tgl1, $format_tgl2);
        $count_performance_by_item = $this->M_cabin_performance->count_performance_by_item($format_tgl1, $format_tgl2);

        $array_functional_performance = [];
        foreach ($functional_performance as $key) {
            $id = $key['id_item'];
            $nama = $key['nama_item'];
            $perform = ($key['total_item_sub'] - $key['total_defects_item_sub']) / $key['total_item_sub'];
            $value = array('id_item' => $id, 'nama_item' => $nama, 'perform' => $perform);
            $array_functional_performance[] = $value;
        }


        $groups = array();
        foreach ($array_functional_performance as $item) {
            $key = $item['id_item'];
            if (!isset($groups[$key])) {
                $groups[$key] = array(
                    'id_item' => $key,
                    'nama_item' => $item['nama_item'],
                    'perform' => $item['perform'],
                    'count' => count($key),
                );
            } else {
                $category = $groups[$key]['id_item'];
                $groups[$key]['nama_item'] = $groups[$key]['nama_item'];
                $groups[$key]['perform'] = $groups[$key]['perform'] + $item['perform'];
            }
        }

        $array_groups = [];
        foreach ($groups as $groups_key => $groups_value) {
            foreach ($count_performance_by_item as $count_performance_by_item_key) {
                if ($count_performance_by_item_key['id_item'] == $groups[$groups_key]['id_item']) {
                    $id = $count_performance_by_item_key['id_item'];
                    $name = $count_performance_by_item_key['nama_item'];
                    $perform = round((($groups[$groups_key]['perform'] / $count_performance_by_item_key['count']) * 100), 2);
                    $value = array('id_item' => $id, 'nama_item' => $name, 'perform' => $perform);
                    $array_groups[] = $value;
                }
            }
        }

        $array_data = [];
        foreach ($array_groups as $array_groups_key => $array_groups_value) {
            foreach ($functional_target as $functional_target_key) {
                if ($functional_target_key['id_item'] == $array_groups[$array_groups_key]['id_item']) {
                    $id = $array_groups[$array_groups_key]['id_item'];
                    $name = $array_groups[$array_groups_key]['nama_item'];
                    $perform = $array_groups[$array_groups_key]['perform'];
                    $target = $functional_target_key['target'];
                    $value = array('id_item' => $id, 'nama_item' => $name, 'perform' => $perform, 'target' => $target);
                    array_push($categories, $name);
                    array_push($value_target, $target);
                    array_push($value_performance, $perform);
                }
            }
        }

        echo json_encode(array("categories" => $categories, 'data_target' => $value_target, 'data_performance' => $value_performance));
    }


    function reg_functional($id_aircraft_type, $id_aircraft_reg)
    {
        $query = $this->M_cabin_performance->reg_functional($id_aircraft_type, $id_aircraft_reg);
        echo json_encode($query);
    }


    function aspek_functional($id_aircraft_type, $tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $data = array();
        $id = array();
        $categories = array();
        $value_target = array();
        $value_performance = array();
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $thn = $pecah['0'];
        $aspek_functional_target = $this->M_cabin_performance->aspek_functional_target($thn);
        $aspek_functional_performance = $this->M_cabin_performance->aspek_functional_performance($id_aircraft_type, $format_tgl1, $format_tgl2);
        $master_functional = $this->M_cabin_performance->master_functional();

        if (count($aspek_functional_target) == 0) {
            foreach ($master_functional as $master_functional_key => $master_functional_value) {
                $id = $master_functional_value['id_item'];
                $name = $master_functional_value['nama_item'];
                $target = 0;
                $performance = 0;
                foreach ($aspek_functional_performance as $aspek_functional_performance_key) {
                    if ($aspek_functional_performance_key['id_item'] == $master_functional[$master_functional_key]['id_item']) {
                        $performance += round(((($aspek_functional_performance_key['total_item'] - $aspek_functional_performance_key['total_defect']) / $aspek_functional_performance_key['total_item']) * 100), 1);
                    }
                }
                $value = array("id_item" => $id, "categories" => $name, "data_target" => $target, "data_performance" => $performance);
                array_push($data, $value);
            }
        } else {
            foreach ($aspek_functional_target as $aspek_functional_target_key => $aspek_functional_target_value) {
                $id = $aspek_functional_target_value['id_item'];
                $name = $aspek_functional_target_value['nama_item'];
                $target = round(($aspek_functional_target_value['target']), 1);
                $performance = 0;
                foreach ($aspek_functional_performance as $aspek_functional_performance_key) {
                    if ($aspek_functional_performance_key['id_item'] == $aspek_functional_target[$aspek_functional_target_key]['id_item']) {
                        $performance += round(((($aspek_functional_performance_key['total_item'] - $aspek_functional_performance_key['total_defect']) / $aspek_functional_performance_key['total_item']) * 100), 1);
                    }
                }
                $value = array("id_item" => $id, "categories" => $name, "data_target" => $target, "data_performance" => $performance);
                array_push($data, $value);
            }
        }
        echo json_encode($data);
    }

    function detail_sub_functional_reg($id_type, $id_item, $tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $query = $this->M_cabin_performance->detail_sub_functional_reg($id_type, $id_item, $format_tgl1, $format_tgl2);
        echo json_encode($query);
    }

    function detail_sub_functional_by_item($id_item, $tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $query = $this->M_cabin_performance->detail_sub_functional_by_item($id_item, $format_tgl1, $format_tgl2);
        echo json_encode($query);
    }

    function mitigasi_functional_by_item($id_item, $tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $type_mitigasi = 'Functionality';
        $query = $this->M_cabin_performance->mitigasi_functional_by_item($id_item, $type_mitigasi, $format_tgl1, $format_tgl2);
        echo json_encode($query);
    }

    function mitigasi_functional_by_reg($id_type, $id_item, $tgl1, $tgl2)
    {
        $tgl1 = explode("-", $tgl1);
        $tgl1 = $tgl1['1'] . '-' . $tgl1['0'];
        $tgl2 = explode("-", $tgl2);
        $tgl2 = $tgl2['1'] . '-' . $tgl2['0'];
        $last_date = date('t', strtotime($tgl2));
        $format_tgl1 = $tgl1 . '-01';
        $format_tgl2 = $tgl2 . '-' . $last_date;
        $pecah = explode("-", $tgl1);
        $query = $this->M_cabin_performance->mitigasi_functional_by_reg($id_type, $id_item, $format_tgl1, $format_tgl2);
        echo json_encode($query);
    }

//==================================================MASTER DATA TARGET==================================================\\

    function data_target_cabin($thn)
    {
        $data = $this->M_cabin_performance->show_target_cabin($thn);
        echo json_encode($data);
    }

    function update_target_cabin()
    {
        $target_id = $this->input->post('target_id');
        $target_val = $this->input->post('target_val');
        if ($target_val > 100) {
            echo json_encode(array("status" => FALSE));
        } else {
            $data = array(
                'target_val' => $target_val,
                'target_year' => date('Y'),
                'target_type' => 'Cabin',
                'target_aircraft' => '',
            );
            $this->M_cabin_performance->update_target_cabin(array('target_id' => $this->input->post('target_id')), $data);
            echo json_encode(array("status" => TRUE));
        }

    }

//==================================================MITIGASI INTERIOR==================================================\\

    // function master_mitigasi_interior($tgl1, $tgl2){
    //   $last_date   = date('t');
    //   $format_tgl1 = $tgl1.'-01';
    //   $format_tgl2 = $tgl2.'-'.$last_date;
    //   $pecah       = explode("-", $tgl1);
    //   $thn         = $pecah['0'];
    //   $target      = $this->M_cabin_performance->mitigasi_interior_target($thn);
    //   $perform     = $this->M_cabin_performance->mitigasi_interior_perform($format_tgl1, $format_tgl2);

    //   $tampung     = array();

    //   foreach ($perform as $perform_key => $perform_value){
    //     $id_manufacture  = $perform_value['id_manufacture'];
    //     $name_manf       = $perform_value['name_manf'];
    //     $id_type         = $perform_value['id_type'];
    //     $name_aircraft   = $perform_value['name_aircraft'];
    //     $id_reg          = $perform_value['id_reg'];
    //     $name_ac_reg     = $perform_value['name_ac_reg'];
    //     foreach ($target as $target_key){
    //       $nilai_target    = $target_key['target'];
    //       $id_item         = $target_key['id_item'];
    //       $nama_item       = $target_key['nama_item'];
    //       if($perform[$perform_key]['id_item'] ==  $target_key['id_item']){
    //         $nilai_perform   =  round(((($perform[$perform_key]['total_item']-($perform[$perform_key]['total_defect']+$perform[$perform_key]['total_dirty']))/$perform[$perform_key]['total_item']) * 100), 2);
    //       }else{
    //         $nilai_perform   = 0;
    //       }
    //       if($nilai_perform < $nilai_target){
    //         $value = array('id_manufacture' => $id_manufacture, 'name_manf' => $name_manf, 'id_type' => $id_type, 'id_item' => $id_item, 'nama_item' => $nama_item, 'name_aircraft' => $name_aircraft, 'id_reg' => $id_reg, 'name_ac_reg' => $name_ac_reg, 'nilai_target' => $nilai_target, 'nilai_perform' => $nilai_perform);
    //             array_push($tampung, $value);
    //       }
    //     }
    //   }

    //   echo json_encode($tampung);
    // }

    function master_mitigasi_interior($date1, $date2)
    {
        $last_date = date('t', strtotime($date2));
        $pecah1 = explode("-", $date1);
        $pecah2 = explode("-", $date2);
        $thn = $pecah1['1'];
        $bln1 = $pecah1['0'];
        $bln2 = $pecah2['0'];
        $target = $this->M_cabin_performance->mitigasi_interior_target($thn);
        $perform = $this->M_cabin_performance->mitigasi_interior_perform($bln1, $bln2, $thn);

        $tampung = array();

        $groups = array();
        foreach ($perform as $item) {
            $key = $item['id_reg'];
            if (!isset($groups[$key])) {
                $groups[$key] = array(
                    'id_manufacture' => $item['id_manufacture'],
                    'name_manf' => $item['name_manf'],
                    'id_type' => $item['id_type'],
                    'name_aircraft' => $item['name_aircraft'],
                    'id_reg' => $key,
                    'name_ac_reg' => $item['name_ac_reg'],
                    'id_item' => $item['id_item'],
                    'nama_item' => $item['nama_item'],
                    'total_defect' => $item['total_defect'],
                    'total_dirty' => $item['total_dirty'],
                    'total_item' => $item['total_item'],
                    'modified_date' => $item['modified_date'],
                );
            } else {
                $category = $groups[$key]['id_reg'];
                $groups[$key]['total_defect'] = $groups[$key]['total_defect'] + $item['total_defect'];
                $groups[$key]['total_dirty'] = $groups[$key]['total_dirty'] + $item['total_dirty'];
                $groups[$key]['total_item'] = $groups[$key]['total_item'] + $item['total_item'];
            }
        }


        foreach ($target as $target_key => $target_value) {
            foreach ($groups as $perform_key) {
                if ($target[$target_key]['id_item'] == $perform_key['id_item']) {
                    $id_manufacture = $perform_key['id_manufacture'];
                    $name_manf = $perform_key['name_manf'];
                    $id_type = $perform_key['id_type'];
                    $name_aircraft = $perform_key['name_aircraft'];
                    $id_reg = $perform_key['id_reg'];
                    $name_ac_reg = $perform_key['name_ac_reg'];
                    $id_item = $perform_key['id_item'];
                    $nama_item = $perform_key['nama_item'];
                    $tgl = $perform_key['modified_date'];
                    $nilai_target = $target[$target_key]['target'];
                    $nilai_perform = round(((($perform_key['total_item'] - ($perform_key['total_defect'] + $perform_key['total_dirty'])) / $perform_key['total_item']) * 100), 2);
                    if ($nilai_perform < $nilai_target) {
                        $value = array('id_manufacture' => $id_manufacture, 'name_manf' => $name_manf, 'id_type' => $id_type, 'name_aircraft' => $name_aircraft, 'id_reg' => $id_reg, 'name_ac_reg' => $name_ac_reg, 'id_item' => $id_item, 'nama_item' => $nama_item, 'date' => $tgl, 'nilai_target' => $nilai_target, 'nilai_perform' => $nilai_perform);
                        array_push($tampung, $value);
                    } else {
                    }
                }
            }
        }
        echo json_encode($tampung);
    }

    function add_mitigation_in()
    {
        $data = array(
            'cabin_mitigation_date' => $this->input->post('cabin_mitigation_date'),
            'actype' => $this->input->post('actype'),
            'acreg' => $this->input->post('acreg'),
            'type_mitigasi' => $this->input->post('type_mitigasi'),
            'item' => $this->input->post('item'),
            'nm_item' => $this->input->post('nm_item'),
            'cabin_mitigation_why' => $this->input->post('cabin_mitigation_why'),
            'cabin_mitigation_solution' => $this->input->post('cabin_mitigation_solution'),
        );
        $this->M_cabin_performance->save_mitigation_in($data);
        echo json_encode(array("status" => TRUE));
    }

    function update_mitigation_in()
    {
        $cabin_mitigation_id = $this->input->post('cabin_mitigation_id');
        $data = array(
            'cabin_mitigation_date' => $this->input->post('cabin_mitigation_date'),
            'actype' => $this->input->post('actype'),
            'acreg' => $this->input->post('acreg'),
            'type_mitigasi' => $this->input->post('type_mitigasi'),
            'item' => $this->input->post('item'),
            'cabin_mitigation_why' => $this->input->post('cabin_mitigation_why'),
            'cabin_mitigation_solution' => $this->input->post('cabin_mitigation_solution'),
        );
        $this->M_cabin_performance->update_mitigation_in(array('cabin_mitigation_id' => $this->input->post('cabin_mitigation_id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    function show_mitigation_in()
    {
        $tgl = $this->input->get('date');
        $actype = $this->input->get('id_type');
        $id_reg = $this->input->get('id_reg');
        $id_item = $this->input->get("id_item");
        $data = $this->M_cabin_performance->show_mitigation_in($tgl, $actype, $id_item);
        echo json_encode($data);
    }

    function data_mitigation($date1, $date2)
    {
        $last_date = date('t', strtotime($date2));
        $pecah1 = explode("-", $date1);
        $pecah2 = explode("-", $date2);
        $thn = $pecah1['1'];
        $bln1 = $pecah1['0'];
        $bln2 = $pecah2['0'];
        $data = $this->M_cabin_performance->data_mitigation($bln1, $bln2, $thn);
        echo json_encode($data);
    }

//==================================================MITIGASI EXTERIOR==================================================\\

    function master_mitigasi_exterior($date1, $date2)
    {
        $last_date = date('t', strtotime($date2));
        $pecah1 = explode("-", $date1);
        $pecah2 = explode("-", $date2);
        $thn = $pecah1['1'];
        $bln1 = $pecah1['0'];
        $bln2 = $pecah2['0'];
        $target = $this->M_cabin_performance->mitigasi_exterior_target($thn);
        $perform = $this->M_cabin_performance->mitigasi_exterior_perform($bln1, $bln2, $thn);

        $tampung = array();

        $groups = array();
        foreach ($perform as $item) {
            $key = $item['id_reg'];
            if (!isset($groups[$key])) {
                $groups[$key] = array(
                    'id_manufacture' => $item['id_manufacture'],
                    'name_manf' => $item['name_manf'],
                    'id_type' => $item['id_type'],
                    'name_aircraft' => $item['name_aircraft'],
                    'id_reg' => $key,
                    'name_ac_reg' => $item['name_ac_reg'],
                    'id_item' => $item['id_item'],
                    'nama_item' => $item['nama_item'],
                    'te_date' => $item['te_date'],
                    'result_value' => $item['result_value'],
                    'jumlah_trans_exterior' => $item['jumlah_trans_exterior'],
                );
            } else {
                $category = $groups[$key]['id_reg'];
                $groups[$key]['result_value'] = $groups[$key]['result_value'] + $item['result_value'];
                $groups[$key]['jumlah_trans_exterior'] = $groups[$key]['total_dirty'] + $item['jumlah_trans_exterior'];
            }
        }

        foreach ($target as $target_key => $target_value) {
            foreach ($groups as $perform_key) {
                if ($target[$target_key]['id_item'] == $perform_key['id_item']) {
                    $id_manufacture = $perform_key['id_manufacture'];
                    $name_manf = $perform_key['name_manf'];
                    $id_type = $perform_key['id_type'];
                    $name_aircraft = $perform_key['name_aircraft'];
                    $id_reg = $perform_key['id_reg'];
                    $name_ac_reg = $perform_key['name_ac_reg'];
                    $id_item = $perform_key['id_item'];
                    $nama_item = $perform_key['nama_item'];
                    $tgl = $perform_key['te_date'];
                    $nilai_target = $target[$target_key]['target'];
                    $nilai_perform = round(($perform_key['result_value'] / $perform_key['jumlah_trans_exterior']), 2);

                    if ($nilai_perform < $nilai_target) {
                        $value = array('id_manufacture' => $id_manufacture, 'name_manf' => $name_manf, 'id_type' => $id_type, 'name_aircraft' => $name_aircraft, 'id_reg' => $id_reg, 'name_ac_reg' => $name_ac_reg, 'id_item' => $id_item, 'nama_item' => $nama_item, 'date' => $tgl, 'nilai_target' => $nilai_target, 'nilai_perform' => $nilai_perform);
                        array_push($tampung, $value);
                    } else {
                    }
                }
            }
        }
        echo json_encode($tampung);
    }

    function add_mitigation_ext()
    {

        $data = array(
            'cabin_mitigation_date' => $this->input->post('cabin_mitigation_date'),
            'actype' => $this->input->post('actype'),
            'acreg' => $this->input->post('acreg'),
            'type_mitigasi' => $this->input->post('type_mitigasi'),
            'item' => $this->input->post('item'),
            'nm_item' => $this->input->post('nm_item'),
            'cabin_mitigation_why' => $this->input->post('cabin_mitigation_why'),
            'cabin_mitigation_solution' => $this->input->post('cabin_mitigation_solution'),
        );
        $this->M_cabin_performance->save_mitigation_ext($data);
        echo json_encode(array("status" => TRUE));
    }

    function update_mitigation_ext()
    {
        $cabin_mitigation_id = $this->input->post('cabin_mitigation_id');
        $data = array(
            'cabin_mitigation_date' => $this->input->post('cabin_mitigation_date'),
            'actype' => $this->input->post('actype'),
            'acreg' => $this->input->post('acreg'),
            'type_mitigasi' => $this->input->post('type_mitigasi'),
            'item' => $this->input->post('item'),
            'nm_item' => $this->input->post('nm_item'),
            'cabin_mitigation_why' => $this->input->post('cabin_mitigation_why'),
            'cabin_mitigation_solution' => $this->input->post('cabin_mitigation_solution'),
        );
        $this->M_cabin_performance->update_mitigation_ext(array('cabin_mitigation_id' => $this->input->post('cabin_mitigation_id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    function show_mitigation_ext($tgl, $id_type, $id_item)
    {
        $data = $this->M_cabin_performance->show_mitigation_ext($tgl, $id_type, $id_item);
        echo json_encode($data);
    }

//================================================MITIGASI FUNCTIONALITY================================================\\

    function master_mitigasi_functionality($date1, $date2)
    {
        /*$tgl1           = date('Y-m', strtotime('01-' . $date1));
          $tgl2           = date('Y-m', strtotime('01-' . $date2));
          $last_date        = date('t', strtotime($date2));
          $format_tgl1   = $tgl1 . '-01';
          $format_tgl2   = $tgl2 . '-' . $last_date;
          $pecah1           = explode("-", $date1);
          $pecah2           = explode("-", $date2);
          $thn              = $pecah1['1'];
          $bln1             = $pecah1['0'];
          $bln2             = $pecah2['0'];
          $target           = $this->M_cabin_performance->mitigasi_functionality_target($thn);
          $perform          = $this->M_cabin_performance->mitigasi_functionality_perform($bln1, $bln2, $thn);

          $tampung = array();



          echo '<pre>';
          print_r($perform);
          echo '</pre>';
          die;

          $groups = array();
          foreach ($perform as $item) {
              $key = $item['id_reg'];
              if (!isset($groups[$key])) {
                  $groups[$key] = array(
                      'id_manufacture' => $item['id_manufacture'],
                      'name_manf'      => $item['name_manf'],
                      'id_type'        => $item['id_type'],
                      'name_aircraft'  => $item['name_aircraft'],
                      'id_reg'         => $key,
                      'name_ac_reg'    => $item['name_ac_reg'],
                      'id_item'        => $item['id_item'],
                      'nama_item'      => $item['nama_item'],
                      'modified_date'  => $item['modified_date'],
                      'total_item'     => $item['total_item'],
                      'total_defect'   => $item['total_defect'],
                  );
              } else {
                  $category = $groups[$key]['id_reg'];
                  $groups[$key]['total_item']   = $groups[$key]['total_item'] + $item['total_item'];
                  $groups[$key]['total_defect']   = $groups[$key]['total_defect'] + $item['total_defect'];
              }
          }

          /*foreach ($target as $target_key => $target_value) {
              foreach ($perform as $perform_key) {
                  if ($target[$target_key]['id_item'] == $perform_key['id_item']) {
                      $id_manufacture = $perform_key['id_manufacture'];
                      $name_manf = $perform_key['name_manf'];
                      $id_type = $perform_key['id_type'];
                      $name_aircraft = $perform_key['name_aircraft'];
                      $id_reg = $perform_key['id_reg'];
                      $name_ac_reg = $perform_key['name_ac_reg'];
                      $id_item = $perform_key['id_item'];
                      $nama_item = $perform_key['nama_item'];
                      $tgl = $perform_key['modified_date'];
                      $nilai_target = $target[$target_key]['target'];
                      $nilai_perform = round(((($perform_key['total_item'] - $perform_key['total_defect']) / $perform_key['total_item']) * 100), 2);
                      if ($nilai_perform < $nilai_target) {
                          $value = array('id_manufacture' => $id_manufacture, 'name_manf' => $name_manf, 'id_type' => $id_type, 'name_aircraft' => $name_aircraft, 'id_reg' => $id_reg, 'name_ac_reg' => $name_ac_reg, 'id_item' => $id_item, 'nama_item' => $nama_item, 'date' => $tgl, 'nilai_target' => $nilai_target, 'nilai_perform' => $nilai_perform);
                          //$value = array('id_item' => $id_item, 'nama_item' => $nama_item, 'date' => $tgl, 'nilai_target' => $nilai_target, 'nilai_perform' => $nilai_perform);
                          array_push($tampung, $value);
                      } else {
                      }
                  }
              }
          }

          foreach ($target as $target_key => $target_value) {
              foreach ($perform as $perform_key) {
                  if ($target[$target_key]['id_item'] == $perform_key['id_item']) {
                      $id_item = $perform_key['id_item'];
                      $nama_item = $perform_key['nama_item'];
                      $bulan = $perform_key['bulan'];
                      $tahun = $perform_key['tahun'];
                      $tgl = $tahun.'-'.$bulan;
                      $nilai_target = $target[$target_key]['target'];
                      $nilai_perform = round(((($perform_key['total_item'] - $perform_key['total_defect']) / $perform_key['total_item']) * 100), 2);
                      if ($nilai_perform < $nilai_target) {
                          $value = array('id_item' => $id_item, 'nama_item' => $nama_item, 'date' => $tgl, 'nilai_target' => $nilai_target, 'nilai_perform' => $nilai_perform);
                          array_push($tampung, $value);
                      } else {
                      }
                  }
              }
          } */

        /*echo "<pre>";
        print_r($tampung);
        echo "</pre>";
        die;*/
        $last_date = date('t', strtotime($date2));
        $pecah1 = explode("-", $date1);
        $pecah2 = explode("-", $date2);
        $thn = $pecah1['1'];
        $bln1 = $pecah1['0'];
        $bln2 = $pecah2['0'];
        $tampung = array();

        $functional_target = $this->M_cabin_performance->data_functional_target_by_item($thn);
        $functional_performance = $this->M_cabin_performance->mitigasi_functionality_perform($bln1, $bln2, $thn);
        $count_performance_by_item = $this->M_cabin_performance->count_performance_mitigasi_functionality_by_item($bln1, $bln2, $thn);

        $array_functional_performance = [];
        foreach ($functional_performance as $key) {
            $id = $key['id_item'];
            $nama = $key['nama_item'];
            $tgl = $key['modified_date'];
            $perform = ($key['total_item_sub'] - $key['total_defects_item_sub']) / $key['total_item_sub'];
            $value = array('id_item' => $id, 'nama_item' => $nama, 'perform' => $perform, 'date' => $tgl);
            $array_functional_performance[] = $value;
        }


        $groups = array();
        foreach ($array_functional_performance as $item) {
            $key = $item['id_item'];
            if (!isset($groups[$key])) {
                $groups[$key] = array(
                    'id_item' => $key,
                    'nama_item' => $item['nama_item'],
                    'perform' => $item['perform'],
                    'date' => $item['date'],
                    'count' => count($key),
                );
            } else {
                $category = $groups[$key]['id_item'];
                $groups[$key]['nama_item'] = $groups[$key]['nama_item'];
                $groups[$key]['date'] = $groups[$key]['date'];
                $groups[$key]['perform'] = $groups[$key]['perform'] + $item['perform'];
            }
        }

        $array_groups = [];
        foreach ($groups as $groups_key => $groups_value) {
            foreach ($count_performance_by_item as $count_performance_by_item_key) {
                if ($count_performance_by_item_key['id_item'] == $groups[$groups_key]['id_item']) {
                    $id = $count_performance_by_item_key['id_item'];
                    $name = $count_performance_by_item_key['nama_item'];
                    $tgl = $groups[$groups_key]['date'];
                    $perform = round((($groups[$groups_key]['perform'] / $count_performance_by_item_key['count']) * 100), 2);
                    $value = array('id_item' => $id, 'nama_item' => $name, 'perform' => $perform, 'date' => $tgl);
                    $array_groups[] = $value;
                }
            }
        }

        $array_data = [];
        foreach ($array_groups as $array_groups_key => $array_groups_value) {
            foreach ($functional_target as $functional_target_key) {
                if ($functional_target_key['id_item'] == $array_groups[$array_groups_key]['id_item']) {
                    $id = $array_groups[$array_groups_key]['id_item'];
                    $name = $array_groups[$array_groups_key]['nama_item'];
                    $perform = $array_groups[$array_groups_key]['perform'];
                    $tgl = $array_groups[$array_groups_key]['date'];
                    $target = $functional_target_key['target'];
                    if ($perform < $target) {
                        $value = array('id_item' => $id, 'nama_item' => $name, 'perform' => $perform, 'target' => $target, 'date' => $tgl);
                        array_push($tampung, $value);
                    } else {
                    }
                    /*$value      = array('id_item' => $id, 'nama_item' => $name, 'perform' => $perform, 'target' => $target, 'date' => $tgl);
                    array_push($tampung, $value);*/
                }
            }
        }

        /*echo "<pre>";
        print_r($tampung);
        echo "</pre>";
        die;*/

        echo json_encode($tampung);
    }

    function add_mitigation_funct()
    {
        if ($this->input->post('actype') == 'undefined') {
            $actype = '0';
        } else {
            $actype = $this->input->post('actype');
        }

        $data = array(
            'cabin_mitigation_date' => $this->input->post('cabin_mitigation_date'),
            'actype' => $actype,
            'acreg' => $this->input->post('acreg'),
            'type_mitigasi' => $this->input->post('type_mitigasi'),
            'item' => $this->input->post('item'),
            'nm_item' => $this->input->post('nm_item'),
            'cabin_mitigation_why' => $this->input->post('cabin_mitigation_why'),
            'cabin_mitigation_solution' => $this->input->post('cabin_mitigation_solution'),
        );
        $this->M_cabin_performance->save_mitigation_funct($data);
        echo json_encode(array("status" => TRUE));
    }

    function update_mitigation_funct()
    {
        $cabin_mitigation_id = $this->input->post('cabin_mitigation_id');
        if ($this->input->post('actype') == 'undefined') {
            $actype = '0';
        } else {
            $actype = $this->input->post('actype');
        }

        $data = array(
            'cabin_mitigation_date' => $this->input->post('cabin_mitigation_date'),
            'actype' => $actype,
            'acreg' => $this->input->post('acreg'),
            'type_mitigasi' => $this->input->post('type_mitigasi'),
            'item' => $this->input->post('item'),
            'nm_item' => $this->input->post('nm_item'),
            'cabin_mitigation_why' => $this->input->post('cabin_mitigation_why'),
            'cabin_mitigation_solution' => $this->input->post('cabin_mitigation_solution'),
        );
        $this->M_cabin_performance->update_mitigation_funct(array('cabin_mitigation_id' => $this->input->post('cabin_mitigation_id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    function show_mitigation_funct($tgl, $id_item)
    {
        $data = $this->M_cabin_performance->show_mitigation_funct($tgl, $id_item);
        echo json_encode($data);
    }


}
