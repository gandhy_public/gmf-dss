<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_dispatch_reability extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper('encript');
        $this->load->helper('color');
        $this->load->model('M_m_dispatch_reability');
    }

    public function index()
    {
        $data = array(
            'content' => 'dispatch/m_dispatch_reability',
            'title' => 'Dispatch Reability',
            'small_tittle' => '',
            'breadcrumb' => 'Dispatch Reability',
            'menu' => $this->M_menu->tampil(),
            'jan' => $this->M_m_dispatch_reability->getByMonthTarget('1'),
            'feb' => $this->M_m_dispatch_reability->getByMonthTarget('2'),
            'mar' => $this->M_m_dispatch_reability->getByMonthTarget('3'),
            'apr' => $this->M_m_dispatch_reability->getByMonthTarget('4'),
            'may' => $this->M_m_dispatch_reability->getByMonthTarget('5'),
            'jun' => $this->M_m_dispatch_reability->getByMonthTarget('6'),
            'jul' => $this->M_m_dispatch_reability->getByMonthTarget('7'),
            'aug' => $this->M_m_dispatch_reability->getByMonthTarget('8'),
            'sep' => $this->M_m_dispatch_reability->getByMonthTarget('9'),
            'oct' => $this->M_m_dispatch_reability->getByMonthTarget('10'),
            'nov' => $this->M_m_dispatch_reability->getByMonthTarget('11'),
            'dec' => $this->M_m_dispatch_reability->getByMonthTarget('12'),
            'year' => date('Y'),
            'month_from' => date('01-' . 'Y'),
            'month_to' => date('m-Y'),
            'acTypes' => $this->M_m_dispatch_reability->acTypes(),
        );

        $this->load->view('layouts', $data);
    }

    public function addTarget()
    {
        $input = array(
            'aircraftType' => $this->input->post('aircraft_type'),
            'target' => $this->input->post('target'),
        );

        $data = array(
            'dr_target_date' => date('Y-m' . '-01'),
            'dr_target_type' => $this->input->post('aircraft_type'),
            'dr_target_val' => $this->input->post('target'),
        );
        $this->M_m_dispatch_reability->inputTarget($data, 'dr_target');
        redirect('index.php/Master_dispatch_reability');
    }

    public function editTarget()
    {
        $id = $this->input->post('id');
        $data = $this->M_m_dispatch_reability->getTargetById($id);
        echo json_encode($data);
    }

    public function updateTarget()
    {
        $input = array(
            'id' => $this->input->post('id'),
            'dr_target_date' => date('Y-m-d'),
            'dr_target_type' => $this->input->post('aircraft_type'),
            'dr_target_val' => $this->input->post('dr_target_val'),
        );

        $this->M_m_dispatch_reability->updateTarget($input);
        redirect('index.php/Master_dispatch_reability');
    }

    public function updateCommentMitigation()
    {
        $id = $this->input->post('dr_id');
        $mitigations = $this->db->select('*')
            ->from('dr_final')
            ->where('dr_id', $id)
            ->get()->result();
        foreach ($mitigations as $mitigation) {
            $aircraft = $mitigation->dr_actype;
            $date = $mitigation->dr_date;
        }
        $why = $this->input->post('mitigasi_why');
        $solution = $this->input->post('mitigasi_solution');
        $where = array('mitigasi_fk_id' => $id);
        $data = array(
            'mitigasi_fk_id' => $id,
            'mitigasi_why' => $why,
            'mitigasi_solution' => $solution,
            'mitigasi_aircraft' => $aircraft,
            'mitigasi_date' => $date,
            'mitigasi_type' => 'dispatch',
        );
        $this->M_m_dispatch_reability->updateCommentMitigation($where, $data, 'tbl_mitigasi');
        redirect('index.php/Master_dispatch_reability');
    }

    public function addCommentMitigation()
    {
        $id = $this->input->post('dr_id');
        $mitigations = $this->db->select('*')
            ->from('dr_final')
            ->where('dr_id', $id)
            ->get()->result();
        foreach ($mitigations as $mitigation) {
            $aircraft = $mitigation->dr_actype;
            $date = $mitigation->dr_date;
        }
        $why = $this->input->post('mitigasi_why');
        $solution = $this->input->post('mitigasi_solution');

        $data = array(
            'mitigasi_fk_id' => $id,
            'mitigasi_why' => $why,
            'mitigasi_solution' => $solution,
            'mitigasi_aircraft' => $aircraft,
            'mitigasi_date' => $date,
            'mitigasi_type' => 'dispatch',
        );
        $this->M_m_dispatch_reability->inputCommentMitigation($data, 'tbl_mitigasi');
        redirect('index.php/Master_dispatch_reability');
    }

    function data_targets()
    {
        $columns = array(
            0 => 'dr_target_type',
            1 => 'dr_target_val',
            2 => 'id',
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search,
            'order_column' => $order_column,
            'order_dir' => $order_dir,
        );

        $target = $this->M_m_dispatch_reability->showDatatableTarget($data);

        echo $target;
    }

    function delete_target($id)
    {
        echo json_encode(
            array("success" => ($this->M_m_dispatch_reability->deleteTarget($id)) ? true : false)
        );
    }

    function data_mitigations()
    {
        $month_from = date('Y-m', strtotime('01-' . $this->input->post('month_from')));
        $month_to = date('Y-m', strtotime('01-' . $this->input->post('month_to')));

        $columns = array(
            0 => 'dr_id',
            1 => 'dr_date',
            2 => 'dr_actype',
            3 => 'dr_actype',
            4 => 'dr_acreg',
            5 => 'dr_target_val',
            6 => 'dr_result',
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search,
            'order_column' => $order_column,
            'order_dir' => $order_dir,
            'month_from' => $month_from,
            'month_to' => $month_to,
        );

        $send = $this->M_m_dispatch_reability->showDatatableMitigation($data);

        echo $send;
    }

    public function getCommentMitigation()
    {
        $dr_id = $this->input->get('dr_id');
        $data = $this->M_m_dispatch_reability->getMitigationById($dr_id);
        echo json_encode($data);
    }

    function update_comment_mitigation()
    {
        $dr_id = $this->input->post('dr_id');
        $mitigasi_why = $this->input->post('why');
        $mitigasi_solution = $this->input->post('solution');

        $data = $this->M_m_dispatch_reability->update_comment_mitigation($dr_id, $mitigasi_why, $mitigasi_solution);

        echo json_encode($data);
    }

    //TAMBAHAN DIMAS
    function list_target(){
            $list = $this->M_m_dispatch_reability->get_datatables_target();
            $data = array();
            $no   = $_POST['start'];
            foreach ($list as $target) {
                    $no++;
                    $row   = array();
                    $row[] = '<center>'.$no.'</center>';
                    $row[] = $target->dr_target_type;
                    $row[] = '<center>'.$target->dr_target_val.'</center>';
                    $row[] = '<center>
                                <a class="btn btn-success" href="javascript:void(0)" onclick="edit_target('."'".$target->id."'".')">
                                <i class="fa fa-edit"></i>
                                </a>
                                <a class="btn btn-danger"  href="javascript:void(0)" onclick="delete_target('."'".$target->id."'".')">
                                <i class="fa fa-trash"></i>
                                </a>
                              </center>';
                    $data[] = $row;
        }
        $output = array(
                    "draw"            => $_POST['draw'],
                    "recordsTotal"    => $this->M_m_dispatch_reability->count_all_target(),
                    "recordsFiltered" => $this->M_m_dispatch_reability->count_filtered_target(),
                    "data"            => $data,
        );
        echo json_encode($output);
    }

    function addTarget_new(){
        $input = array(
            'aircraftType' => $this->input->post('aircraft_type'),
            'target' => $this->input->post('target'),
        );

        $data = array(
            'dr_target_date' => date('Y-m' . '-01'),
            'dr_target_type' => $this->input->post('aircraft_type'),
            'dr_target_val' => $this->input->post('target'),
        );
        $this->M_m_dispatch_reability->inputTarget_new($data);
        echo json_encode(array('status' => TRUE));
    }

    function editTarget_new($id){
        $data = $this->M_m_dispatch_reability->getTargetById_new($id);
        echo json_encode($data);
    }

    function updateTarget_new(){
        $data = array(
            'dr_target_date' => date('Y-m-d'),
            'dr_target_type' => $this->input->post('aircraft_type'),
            'dr_target_val'  => $this->input->post('target'),
        );
        $this->M_m_dispatch_reability->updateTarget_new(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    function delete_target_new($id){
        $this->M_m_dispatch_reability->deleteTarget_new($id);
        echo json_encode(array("status" => TRUE));
    }

}
