<?php

defined("BASEPATH") or exit('no direct script access allowed');

class Cotd extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("M_menu");
        $this->load->model("M_cotd_dashboard");
        $this->load->helper("date");
    }

    function index()
    {
        $data = array(
            "content" => "cotd/dashboard",
            "title" => "Contribution of Technical Delay",
            "small_tittle" => "",
            "breadcrumb" => ["Contribution of Technical Delay"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }


    public function view_garuda()
    {
        $aircraft = $this->input->post("aircraft");
        if ($aircraft == '') {
            $all_reg = $this->M_cotd_dashboard->get_reg_all('garuda');
            $temp_reg = array();
            foreach ($all_reg as $key => $val) {
                $reg = $val['acreg'];
                $temp_reg[] = $reg;
            }
            $reg = "'" . join("','", $temp_reg) . "'";
        } else if (in_array("All", $aircraft)) {
            unset($aircraft[0]);
            $all_reg = $this->M_cotd_dashboard->get_reg_all('garuda');
            $temp_reg = array();
            foreach ($all_reg as $key => $val) {
                $reg = $val['acreg'];
                $temp_reg[] = $reg;
            }
            $reg = "'" . join("','", $temp_reg) . "'";
        } else {
            $aircraft = "'" . join("','", $aircraft) . "'";
            $all_reg = $this->M_cotd_dashboard->get_reg($aircraft);
            $temp_reg = array();
            foreach ($all_reg as $key => $val) {
                $reg = $val['acreg'];
                $temp_reg[] = $reg;
            }
            $reg = "'" . join("','", $temp_reg) . "'";
        }

        $data['data'] = array();
        $data['datap'] = array();
        $data['datat'] = array();
        $rata_ = "";

        $start = $this->input->post("start");
        $e_start = explode('-', $start);
        $b_start = floatval($e_start[0]);
        $t_start = floatval($e_start[1]);
        $end = $this->input->post("end");
        $e_end = explode('-', $end);
        $b_end = floatval($e_end[0]);
        $t_end = floatval($e_end[1]);

        for ($i = $b_start; $i <= $b_end; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $gv_value = $this->M_cotd_dashboard->get_garuda_grafik($bulan, $t_end, $reg);

            $totaltech = is_null($gv_value['TOTALTECH']) ? 0 : $gv_value['TOTALTECH'];
            $totaldelay = is_null($gv_value['TOTALDELAY']) ? 0 : $gv_value['TOTALDELAY'];
            $flightdelay = is_null($gv_value['FLIGHTDELAY']) ? 0 : $gv_value['FLIGHTDELAY'];
            $totaldeparture = is_null($gv_value['TOTALDEPARTURE']) ? 0 : $gv_value['TOTALDEPARTURE'];
            $rumus = is_null($gv_value['ACTUAL_GA']) ? 0 : $gv_value['ACTUAL_GA'];
            $rumus = array(floatval(number_format($rumus, 2)));
            $valuet = $bulan . '-' . $t_end;
            array_push($data['data'], $rumus);
            array_push($data['datat'], $valuet);
        }

        $json[] = $data;
        $tahun = date("Y");
        $target = $this->M_cotd_dashboard->get_target($tahun, 'garuda');
        $target = $target['target_year'];
        echo json_encode(
            array(
                'grafik' => $json,
                'target' => $target,
            )
        );
    }


    public function view_detail_garuda()
    {
        $aircraft = $this->input->post("aircraft");
        // echo count($aircraft);
        $ac_type = '';
        if ($aircraft == '') {
            $aircraft = "";
        } else if (in_array("All", $aircraft)) {
            unset($aircraft[0]);
            foreach ($aircraft as $key => $val) {
                $ac_type = $this->M_cotd_dashboard->cek_actype($val);
                $temp_type[] = $ac_type['actype'];
            }
            $aircraft = "'" . join("','", $temp_type) . "'";
        } else {
            foreach ($aircraft as $key => $val) {
                $ac_type = $this->M_cotd_dashboard->cek_actype($val);
                $temp_type[] = $ac_type['actype'];
            }
            $aircraft = "'" . join("','", $temp_type) . "'";
        }

        $date = $this->input->post("date");
        //$grafik = $this->M_cotd_dashboard->get_detail_garuda($date,$reg);
        $all_actype = $this->M_cotd_dashboard->get_detail_actype($date, '');
        $dataTable = array();
        $json = array();

        $v_mitigasi = $this->M_cotd_dashboard->v_mitigasi($date, $aircraft, 'GA');
        if (isset($v_mitigasi)) {
            $status = 'Ada Data';
            foreach ($v_mitigasi as $gv_key => $gv_value) {
                // $table = $this->M_cotd_dashboard->get_detail_garuda($date, $reg);
                $rumus = is_null($gv_value['actual']) ? 0 : $gv_value['actual'];

                $rumus = floatval(number_format($rumus, 2));
                $date1 = explode('-', $this->input->post("date"));
                $dataTable['ac_type'] = $gv_value['ac_type'];
                $dataTable['bulan'] = $date1[0];
                $dataTable['actual'] = $rumus;

                $json[] = $dataTable;
            }
        } else {
            $status = 'Tidak ada data';
            $json[] = '';
        }


        $awal = explode("-", $date);
        $awal_tahun = $awal[1];
        $target = $this->M_cotd_dashboard->get_target($awal_tahun, 'garuda');
        $target = $target['target_year'];
        echo json_encode(
            array(
                'tabel' => $json,
                'target' => $target,
                'status' => $status,
            )
        );
    }

    public function view_actual_garuda()
    {

        $all_reg = $this->M_cotd_dashboard->get_reg_all('garuda');
        $temp_reg = array();
        foreach ($all_reg as $key => $val) {
            $reg = $val['acreg'];
            $temp_reg[] = $reg;
        }
        $reg = "'" . join("','", $temp_reg) . "'";

        $tahun = date("Y");
        $start = '01-' . $tahun;
        $end = date("m-Y");
        // $grafik = $this->M_cotd_dashboard->get_garuda($start,$end,$reg);

        $target = $this->M_cotd_dashboard->get_target($tahun, 'garuda');
        $target = str_replace(',', '.', is_null($target['target_year']) ? 0 : floatval(number_format($target['target_year'], 2)));

        $rata_ = "";

        $e_end = explode('-', $end);
        $b_end = floatval($e_end[0]);
        $t_end = floatval($e_end[1]);

        $count = 0;
        for ($i = 1; $i <= $b_end; $i++) {
            $count += 1;
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $gv_value = $this->M_cotd_dashboard->get_garuda_grafik($bulan, $t_end, $reg);

            $rumus = is_null($gv_value['ACTUAL_GA']) ? 0 : $gv_value['ACTUAL_GA'];
            $rata_ += $rumus;
        }
        $rata2_ = $rata_ / $count;
        $rata2 = str_replace(',', '.', is_null($rata2_) ? 0 : floatval(number_format($rata2_, 4)));

        if ($rata2_ <= $target) {
            $rata2 = '<span class="text-green">' . $rata2 . '</span>';
            $target_ = '<span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>';
        } else {
            $rata2 = '<span class="text-red">' . $rata2 . '</span>';
            $target_ = '<span class="description-percentage text-red"><i class="fa fa-caret-down"></i></span>';
        }
        echo json_encode(
            array(
                'actual' => $rata2,
                'target' => $target,
                'panah' => $target_,
            )
        );
    }


    public function view_citilink()
    {
        // $start= $this->input->post("start") ;
        // $end= $this->input->post("end") ;
        // $delay = $this->M_cotd_dashboard->get_citilink_delay($start,$end);


        $aircraft = $this->input->post("aircraft");
        if ($aircraft == '') {
            $all_reg = $this->M_cotd_dashboard->get_reg_all('citilink');
            $temp_reg = array();
            foreach ($all_reg as $key => $val) {
                $reg = $val['acreg'];
                $temp_reg[] = $reg;
            }
            $reg = "'" . join("','", $temp_reg) . "'";
        } else if (in_array("All", $aircraft)) {
            unset($aircraft[0]);
            $all_reg = $this->M_cotd_dashboard->get_reg_all('citilink');
            $temp_reg = array();
            foreach ($all_reg as $key => $val) {
                $reg = $val['acreg'];
                $temp_reg[] = $reg;
            }
            $reg = "'" . join("','", $temp_reg) . "'";
        } else {
            $aircraft = "'" . join("','", $aircraft) . "'";
            $all_reg = $this->M_cotd_dashboard->get_reg_citilink($aircraft);
            $temp_reg = array();
            foreach ($all_reg as $key => $val) {
                $reg = $val['acreg'];
                $temp_reg[] = $reg;
            }
            $reg = "'" . join("','", $temp_reg) . "'";
        }


        // $all = $this->M_cotd_dashboard->get_citilink_all($start,$end,$reg);
        $data['data'] = array();
        $data['datap'] = array();
        $data['datat'] = array();


        $start = $this->input->post("start");
        $e_start = explode('-', $start);
        $b_start = floatval($e_start[0]);
        $t_start = floatval($e_start[1]);
        $end = $this->input->post("end");
        $e_end = explode('-', $end);
        $b_end = floatval($e_end[0]);
        $t_end = floatval($e_end[1]);

        for ($i = $b_start; $i <= $b_end; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $gv_value = $this->M_cotd_dashboard->get_citilink_all_grafik($bulan, $t_end, $reg);
            $gv_value1 = $this->M_cotd_dashboard->get_citilink_delay($bulan, $t_end, $reg);

            $all_ = is_null($gv_value['totaldepar']) ? 0 : $gv_value['totaldepar'];
            $delay_ = is_null($gv_value1['totaltech']) ? 0 : $gv_value1['totaltech'];
            if ($all_ == 0 || $delay_ == 0) {
                $rumus = 0;
            } else {
                $rumus = $delay_ / $all_;
            }
            $rumus = array(floatval(number_format($rumus, 5)));
            $valuet = $bulan . '-' . $t_end;
            array_push($data['data'], $rumus);
            array_push($data['datat'], $valuet);
        }

        $json[] = $data;
        $tahun = date("Y");
        $target = $this->M_cotd_dashboard->get_target($tahun, 'citilink');
        $target = $target['target_year'];
        echo json_encode(
            array(
                'grafik' => $json,
                'target' => $target,
            )
        );
    }


    public function view_detail_citilink()
    {
        $date = $this->input->post("date");

        $awal = explode("-", $date);
        $bulan = $awal[0];
        $tahun = $awal[1];

        $aircraft = $this->input->post("aircraft");

        if ($aircraft == '') {
            $aircraft = "";
        } else if (in_array("All", $aircraft)) {
            unset($aircraft[0]);
            foreach ($aircraft as $key => $val) {
                $ac_type = $this->M_cotd_dashboard->cek_actype($val);
                $temp_type[] = $ac_type['actype'];
            }
            $aircraft = "'" . join("','", $temp_type) . "'";
        } else {
            foreach ($aircraft as $key => $val) {
                $ac_type = $this->M_cotd_dashboard->cek_actype($val);
                $temp_type[] = $ac_type['actype'];
            }
            $aircraft = "'" . join("','", $temp_type) . "'";
        }

        $json = array();
        $target = $this->M_cotd_dashboard->get_target($tahun, 'citilink');
        $target = is_null($target['target_year']) ? 0 : $target['target_year'];

        $v_mitigasi = $this->M_cotd_dashboard->v_mitigasi($date, $aircraft, 'CITILINK');
        if (isset($v_mitigasi)) {
            $status = 'Ada Data';
            foreach ($v_mitigasi as $gv_key => $gv_value) {
                // $table = $this->M_cotd_dashboard->get_detail_garuda($date, $reg);
                $rumus = is_null($gv_value['actual']) ? 0 : $gv_value['actual'];

                $rumus = floatval(number_format($rumus, 2));
                $date1 = explode('-', $this->input->post("date"));
                $dataTable['ac_type'] = $gv_value['ac_type'];
                $dataTable['bulan'] = $date1[0];
                $dataTable['actual'] = $rumus;

                $json[] = $dataTable;
            }
        } else {
            $status = "Tidak ada data";
            $json[] = '';
        }


        echo json_encode(
            array(
                'tabel' => $json,
                'target' => $target,
                'status' => $status,
            )
        );
    }


    public function view_actual_citilink()
    {


        $tahun = date("Y");
        $start = '01-' . $tahun;
        $end = date("m-Y");


        $all_reg = $this->M_cotd_dashboard->get_reg_all('citilink');
        $temp_reg = array();
        foreach ($all_reg as $key => $val) {
            $reg = $val['acreg'];
            $temp_reg[] = $reg;
        }
        $reg = "'" . join("','", $temp_reg) . "'";
        $rata_ = "";
        $count = 0;
        // $account = count($all);

        $target = $this->M_cotd_dashboard->get_target($tahun, 'citilink');
        $target = str_replace(',', '.', is_null($target['target_year']) ? 0 : floatval(number_format($target['target_year'], 2)));


        $e_end = explode('-', $end);
        $b_end = floatval($e_end[0]);
        $t_end = floatval($e_end[1]);

        for ($i = 1; $i <= $b_end; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $gv_value = $this->M_cotd_dashboard->get_citilink_all_grafik($bulan, $t_end, $reg);
            $gv_value1 = $this->M_cotd_dashboard->get_citilink_delay($bulan, $t_end, $reg);

            $all_ = is_null($gv_value['totaldepar']) ? 0 : $gv_value['totaldepar'];
            $delay_ = is_null($gv_value1['totaltech']) ? 0 : $gv_value1['totaltech'];
            if ($all_ == 0 || $delay_ == 0) {
                $rumus = 0;
            } else {
                $rumus = $delay_ / $all_;
            }

            $rata_ += $rumus;
            $count += 1;
        }
        $rata2_ = $rata_ / $count;

        $rata2 = str_replace(',', '.', is_null($rata2_) ? 0 : floatval(number_format($rata2_, 4)));

        if ($rata2_ <= $target) {
            $rata2 = '<span class="text-green">' . $rata2 . '</span>';
            $target_ = '<span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>';
        } else {
            $rata2 = '<span class="text-red">' . $rata2 . '</span>';
            $target_ = '<span class="description-percentage text-red"><i class="fa fa-caret-down"></i></span>';
        }
        echo json_encode(
            array(
                'actual' => $rata2,
                'target' => $target,
                'panah' => $target_,
            )
        );
    }


    public function view_ata_total()
    {
        $aircraft = $this->input->post("aircraft");
        if ($aircraft == '') {
            $reg = '';
        } elseif (in_array("All", $aircraft)) {
            unset($aircraft[0]);
            $reg = '';

        } else {
            $aircraft = "'" . join("','", $aircraft) . "'";
            $all_reg = $this->M_cotd_dashboard->get_reg($aircraft);
            $temp_reg = array();
            foreach ($all_reg as $key => $val) {
                $reg = $val['acreg'];
                $temp_reg[] = $reg;
            }
            $reg = "'" . join("','", $temp_reg) . "'";
        }

        $start = $this->input->post("start");
        $end = $this->input->post("end");

        // $bulan_tahun= $this->input->post("bulan_tahun") ;
        $aircraft = $this->input->post("tipe");


        $data['data'] = array();
        $data['kategori'] = array();
        $json_builder = array();

        $dataChart = array();
        if ($this->input->post('status') == 'table') {
            $pesawat = $this->M_cotd_dashboard->get_pesawat_table($start, $end, $aircraft, $reg);
        } else {
            $pesawat = $this->M_cotd_dashboard->get_pesawat($start, $end, $aircraft, $reg);
        }
        $atatdm = $this->M_cotd_dashboard->get_atatdm($start, $end, $aircraft, $reg);
        $cek = 0;
        foreach ($pesawat as $key => $gv_value) {
            $dataChart['name'] = $gv_value['ACtype'];
            if ($this->input->post('status') == 'table') {
                $dataChart['reg'] = $gv_value['Reg'];
            }
            foreach ($atatdm as $gv_key1 => $gv_value1) {
                $cek = $this->M_cotd_dashboard->get_cek_ata_total($gv_value['ACtype'], $gv_value1['ATAtdm'], $start, $end, $aircraft);
                if (isset($cek)) {
                    array_push($data['data'], floatval(number_format($cek['total'], 2)));
                } else {
                    array_push($data['data'], floatval(0));
                }
            };
            $dataChart['data'] = $data['data'];
            $dataChart['type'] = 'spline';
            $json_builder[] = $dataChart;
            array_splice($data['data'], 0);
        }

        foreach ($atatdm as $gv_key1 => $gv_value1) {
            array_push($data['kategori'], $gv_value1['ATAtdm']);
        }


        echo json_encode(
            array(
                'grafik' => $json_builder,
                'kategori' => $data['kategori']
            )
        );
    }

    public function view_ata_jam()
    {
        $aircraft = $this->input->post("aircraft");
        if ($aircraft == '') {
            $reg = '';
        } elseif (in_array("All", $aircraft)) {
            unset($aircraft[0]);
            $reg = '';

        } else {
            $aircraft = "'" . join("','", $aircraft) . "'";
            $all_reg = $this->M_cotd_dashboard->get_reg($aircraft);
            $temp_reg = array();
            foreach ($all_reg as $key => $val) {
                $reg = $val['acreg'];
                $temp_reg[] = $reg;
            }
            $reg = "'" . join("','", $temp_reg) . "'";
        }
        $start = $this->input->post("start");
        $end = $this->input->post("end");
        $aircraft = $this->input->post("tipe");


        $data['data'] = array();
        $data['kategori'] = array();

        $json_builder = array();

        $dataChart = array();
        if ($this->input->post('status') == 'table') {
            $pesawat = $this->M_cotd_dashboard->get_pesawat_table($start, $end, $aircraft, $reg);
        } else {
            $pesawat = $this->M_cotd_dashboard->get_pesawat($start, $end, $aircraft, $reg);
        }
        $atatdm = $this->M_cotd_dashboard->get_atatdm($start, $end, $aircraft, $reg);
        $cek = 0;
        foreach ($pesawat as $key => $gv_value) {
            $dataChart['name'] = $gv_value['ACtype'];
            if ($this->input->post('status') == 'table') {
                $dataChart['reg'] = $gv_value['Reg'];
            }
            foreach ($atatdm as $gv_key1 => $gv_value1) {
                $cek = $this->M_cotd_dashboard->get_cek_ata_jam($gv_value['ACtype'], $gv_value1['ATAtdm'], $start, $end, $aircraft);
                if (isset($cek)) {
                    array_push($data['data'], floatval(number_format($cek['total'], 2)));
                } else {
                    array_push($data['data'], floatval(0));
                }

            };
            $dataChart['type'] = 'spline';
            $dataChart['data'] = $data['data'];
            $json_builder[] = $dataChart;
            array_splice($data['data'], 0);
        }

        foreach ($atatdm as $gv_key1 => $gv_value1) {
            array_push($data['kategori'], $gv_value1['ATAtdm']);
        }


        $json[] = $data;
        echo json_encode(
            array(
                'grafik' => $json_builder,
                'kategori' => $data['kategori'],
            )
        );
    }


    public function view_depar_total()
    {
        $aircraft = $this->input->post("aircraft");
        if ($aircraft == '') {
            $reg = '';
        } elseif (in_array("All", $aircraft)) {
            unset($aircraft[0]);
            $reg = '';

        } else {
            $aircraft = "'" . join("','", $aircraft) . "'";
            $all_reg = $this->M_cotd_dashboard->get_reg($aircraft);
            $temp_reg = array();
            foreach ($all_reg as $key => $val) {
                $reg = $val['acreg'];
                $temp_reg[] = $reg;
            }
            $reg = "'" . join("','", $temp_reg) . "'";
        }
        $start = $this->input->post("start");
        $end = $this->input->post("end");
        $aircraft = $this->input->post("tipe");

        $data['data'] = array();
        $data['departure'] = array();
        $data['pesawat'] = array();
        $json_builder = array();
        $cek = 0;

        $dataChart = array();
        if ($this->input->post('status') == 'table') {
            $pesawat = $this->M_cotd_dashboard->get_pesawat_table($start, $end, $aircraft, $reg);
        } else {
            $pesawat = $this->M_cotd_dashboard->get_pesawat($start, $end, $aircraft, $reg);
        }
        $departure = $this->M_cotd_dashboard->get_departure($start, $end, $aircraft, $reg);
        $tes = 0;
        foreach ($pesawat as $key => $gv_value) {
            $dataChart['name'] = $gv_value['ACtype'];
            if ($this->input->post('status') == 'table') {
                $dataChart['reg'] = $gv_value['Reg'];
            }
            foreach ($departure as $gv_key1 => $gv_value1) {
                $cek = $this->M_cotd_dashboard->get_cek_depar_total($gv_value['ACtype'], $gv_value1['DepSta'], $start, $end, $aircraft);
                if (isset($cek)) {
                    array_push($data['data'], floatval(number_format($cek['total'], 2)));
                } else {
                    array_push($data['data'], floatval(0));
                }

            };
            $dataChart['type'] = 'spline';
            $dataChart['data'] = $data['data'];
            $json_builder[] = $dataChart;
            array_splice($data['data'], 0);
        }

        foreach ($departure as $gv_key1 => $gv_value1) {
            array_push($data['departure'], $gv_value1['DepSta']);
        }


        $json[] = $data;

        echo json_encode(
            array(
                'grafik' => $json_builder,
                'kategori' => $data['departure'],
            )
        );
    }


    public function view_depar_jam()
    {
        $aircraft = $this->input->post("aircraft");
        if ($aircraft == '') {
            $reg = '';
        } elseif (in_array("All", $aircraft)) {
            unset($aircraft[0]);
            $reg = '';

        } else {
            $aircraft = "'" . join("','", $aircraft) . "'";
            $all_reg = $this->M_cotd_dashboard->get_reg($aircraft);
            $temp_reg = array();
            foreach ($all_reg as $key => $val) {
                $reg = $val['acreg'];
                $temp_reg[] = $reg;
            }
            $reg = "'" . join("','", $temp_reg) . "'";
        }
        $start = $this->input->post("start");
        $end = $this->input->post("end");
        $aircraft = $this->input->post("tipe");


        $data['data'] = array();
        $data['departure'] = array();
        $data['pesawat'] = array();
        $dataChart['data'] = array();
        $json_builder = array();
        $cek = 0;

        $dataChart = array();
        if ($this->input->post('status') == 'table') {
            $pesawat = $this->M_cotd_dashboard->get_pesawat_table($start, $end, $aircraft, $reg);
        } else {
            $pesawat = $this->M_cotd_dashboard->get_pesawat($start, $end, $aircraft, $reg);
        }
        $departure = $this->M_cotd_dashboard->get_departure($start, $end, $aircraft, $reg);
        $tes = 0;
        foreach ($pesawat as $key => $gv_value) {
            $dataChart['name'] = $gv_value['ACtype'];
            if ($this->input->post('status') == 'table') {
                $dataChart['reg'] = $gv_value['Reg'];
            }
            foreach ($departure as $gv_key1 => $gv_value1) {
                $cek = $this->M_cotd_dashboard->get_cek_depar_jam($gv_value['ACtype'], $gv_value1['DepSta'], $start, $end, $aircraft);
                // $count =count($cek);
                // echo $count;
                if (isset($cek)) {
                    array_push($data['data'], floatval(number_format($cek['total'], 2)));
                } else {
                    array_push($data['data'], floatval(0));
                }
            };
            $dataChart['type'] = 'spline';
            $dataChart['data'] = $data['data'];
            $json_builder[] = $dataChart;
            array_splice($data['data'], 0);
        }

        foreach ($departure as $gv_key1 => $gv_value1) {
            array_push($data['departure'], $gv_value1['DepSta']);
        }


        $json[] = $data;

        echo json_encode(
            array(
                'grafik' => $json_builder,
                'kategori' => $data['departure'],
            )
        );
    }


    public function view_mitigasi()
    {
        $tipe = $this->input->post("tipe");
        $date = $this->input->post("date");


        $data = $this->M_cotd_dashboard->get_mitigasi($tipe, $date);
        if (isset($data)) {
            $why = $data['mitigasi_why'];
            $solusi = $data['mitigasi_solution'];
        } else {
            $why = 'mitigation is empty';
            $solusi = 'mitigation is empty';
        }


        echo json_encode(
            array(
                'why' => $why,
                'solusi' => $solusi,
            )
        );
    }


    public function combo_actype()
    {
        $data = $this->M_cotd_dashboard->get_actype();

        echo json_encode(
            array(
                'data' => $data,
            )
        );
    }


}
