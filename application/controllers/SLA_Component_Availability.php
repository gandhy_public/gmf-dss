<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class SLA_Component_Availability extends MY_Controller
{

    private $data_all_robbing = array();
    private $data_all_target = array();
    private $data_all_graph = array();

    function __construct()
    {
        parent::__construct();
        set_time_limit(0);
        $this->load->database();
        $this->load->model("M_menu");
        $this->load->model("M_sla_component_avability");
    }


    function index()
    {
        $data = array(
            "content" => "SLA Component Availability/v_sla_component_avability",
            "title" => "SLA Component Availability",
            "small_tittle" => "",
            "breadcrumb" => ["SLA Component Availability"],
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }

    public function actualvsterget()
    {
        // code...
        $year = date('Y');
        $non_ga = 'NON GA';
        $ga = 'GA';
        $citilink = 'CITILINK';

        $result_ga = $this->M_sla_component_avability->count_ess($ga, $year);
        $result_non_ga = $this->M_sla_component_avability->count_ess($non_ga, $year);
        $result_citilink = $this->M_sla_component_avability->count_ess($citilink, $year);

        $rob_ga = $this->M_sla_component_avability->count_robbing($ga, $year);
        $rob_non_ga = $this->M_sla_component_avability->count_robbing($non_ga, $year);
        $rob_citilink = $this->M_sla_component_avability->count_robbing($citilink, $year);
        if ($result_non_ga['jml'] == 0) {
            $act_non_ga = 0;
        } else {
            $act_non_ga = ((abs($result_non_ga['jml']) - $rob_non_ga) / abs($result_non_ga['jml'])) * 100;
        };
        if ($result_ga['jml'] == 0) {
            // code...
            $act_ga = 0;
        } else {
            $act_ga = ((abs($result_ga['jml']) - $rob_ga) / abs($result_ga['jml'])) * 100;
        };

        if ($result_citilink['jml'] == 0) {
            // code...
            $act_citilink = 0;
        } else {
            $act_citilink = ((abs($result_citilink['jml']) - $rob_citilink) / abs($result_citilink['jml'])) * 100;
        };


        $data = array(
            'act_non_ga' => number_format($act_non_ga, 1),
            'act_ga' => number_format($act_ga, 1),
            'act_citilink' => number_format($act_citilink, 1),
            'target' => number_format(99, 1));

        echo json_encode($data);

    }

    public function aircraft()
    {
        // code...
        $query = $this->M_sla_component_avability->aircraft_ess();
        echo json_encode($query);
    }

    // private function is_in_array($array, $key, $key_value){
    //       $within_array = false;
    //       foreach( $array as $k=>$v ){
    //         if( is_array($v) ){
    //             $within_array = $this->is_in_array($v, $key, $key_value);
    //             if( $within_array == true ){
    //                 break;
    //             }
    //         } else {
    //                 if( $v == $key_value && $k == $key ){
    //                         $within_array = true;
    //                         break;
    //                 }
    //         }
    //       }
    //       return $within_array;
    // }


    public function get_aircraft()
    {
      $data_aircraft = $this->M_sla_component_avability->get_aircraft_where_own();
      return $data_aircraft;
    }

    public function get_all_robbing($type = 1)
    {
      if ($type != 1) {
        $all_robbing = $this->M_sla_component_avability->get_all_robbing_graph();
      } else {
        $all_robbing = $this->M_sla_component_avability->get_robbing_where();
      }
      $this->data_all_robbing = $all_robbing;
    }

    private function get_all_target()
    {
      $all_target = $this->M_sla_component_avability->get_all_target();
      $this->data_all_target = $all_target;
      // echo json_encode($this->data_all_target);
    }

    private function get_all_robbing_where($own, $actype, $year)
    {
      $data_all_robbing = $this->data_all_robbing;
      $result = 0;
      foreach ($data_all_robbing as $data) {
        $data_year = substr($data["created_on"],0,4);
        if($data["own"] == $own && $data["actype"] == $actype && $data_year == $year){
          $result++;
        }
      }
      return $result;
    }

    private function get_all_target_where($actype, $year)
    {
      $data_all_target = $this->data_all_target;
      foreach ($data_all_target as $data) {
        // echo $data["type_aircraft"]." - ".$actype."<br>";
        if((strpos($actype, $data["type_aircraft"]) !== false) && ($data["year_aircraft"] == $year)){
          return $data["target_aircraft"];
          break;
        }
      }

    }

    public function type_pesawat()
    {
      $ess = array();
      $data_all = $this->M_sla_component_avability->get_all();
      $this->get_all_robbing();
      $this->get_all_target();
      // echo json_encode($this->data_all_robbing); exit;
      $aircraft = $this->get_aircraft();
      $final_result = array();
      foreach ($aircraft as $data) {
        $quantity = 0;
        $total_rows = 0;
        $tes_own = null;
        $robbing = 0;
        foreach ($data_all as $data_ess) {
          if($data_ess["own"] == $data["own"] && $data_ess["actype"] == $data["actype"]){
            $quantity = abs($data_ess["quantity"]);
            $total_rows = $data_ess["total_rows"];
            $tes_own = $data_ess["own"];
            $robbing = $this->get_all_robbing_where($data_ess["own"], $data_ess["actype"], date("Y"));
          }
        }
        $min = $quantity - $robbing;
        if($min != 0 && $quantity != 0){
          $actual = (($quantity - $robbing) / $quantity) * 100;
        } else {
          $actual = 0;
        }
        $actual_final = (($actual > 100) ? 100 : $actual);
        $target = $this->get_all_target_where($data["actype"], date("Y"));
        switch ($data["own"]) {
          case 'GA': $final_result["GA"][] = array("own" => $data["own"], "actype" => $data["actype"], "quantity" => $quantity, "total_rows" => $total_rows, "own2" => $tes_own, "robbing" => $robbing, "actual" => $actual_final, "target" => $target); break;
          case 'CITILINK': $final_result["CITILINK"][] = array("own" => $data["own"], "actype" => $data["actype"], "quantity" => $quantity, "total_rows" => $total_rows, "own2" => $tes_own, "robbing" => $robbing, "actual" => $actual_final, "target" => $target); break;
          default : $final_result["NGA"][] = array("own" => $data["own"], "actype" => $data["actype"], "quantity" => $quantity, "total_rows" => $total_rows, "own2" => $tes_own, "robbing" => $robbing, "actual" => $actual_final, "target" => $target); break;
        }
      }
      echo json_encode(array("target_global" => 99, "data_scc" => $final_result));
    }

    private function get_all_trans($year)
    {
        //echo $year;die;
      $data = $this->M_sla_component_avability->get_all_trans_to_graph($year);
      $this->data_all_graph = $data;
    }

    public function get_spesific_date($date, $type)
    {
        if (($timestamp = strtotime($date)) !== false) {
          $php_date = getdate($timestamp);
          $date = "";
          switch ($type) {
            case 'day':  $date = date("d", $timestamp);  break;
            case 'month':  $date = date("m", $timestamp);  break;
            case 'year':  $date = date("Y", $timestamp);  break;
            default: $date = false; break;
          }
          return $date;
        }
        else {
          return false;
        }
    }

    private function get_all_trans_where($month, $customer)
    {
      $result = 0;
      $datas = $this->data_all_graph;
      foreach ($datas as $data) {
        $posting_date_month = $this->get_spesific_date($data["posting_date"], "month");
        $own = $data["own"];
        if($month == $posting_date_month && $customer == $own){
          $result += $data["quantity"];
        }
      }
      return $result;
    }

    private function get_all_robbing_graph_where($month, $customer)
    {
      $result = 0;
      $datas = $this->data_all_robbing;
      foreach ($datas as $data) {
        $created_date_month = $this->get_spesific_date($data["new_created_on"], "month");
        $own = $data["new_own"];
        if($month == $created_date_month && $customer == $own){
          $result++;
        }
      }
      return $result;
    }

    public function searchgrafik()
    {
        
        $this->get_all_robbing("uvuvwevwevwe ugwemubwem osas");
        // echo json_encode($this->data_all_graph); exit;
        $robbing = $this->data_all_robbing;
        $start=$this->input->post('periode1',TRUE);
        $end=$this->input->post('periode2',TRUE);
        $final_start = substr($start,0,2);
        $final_end = substr($end,0,2);
        $months = range($final_start, $final_end);
        $result = array();
        $result_month = array();

        $pecah = explode('-', $end);

        $status=$this->input->post('status',TRUE);
       
        if($status == 0){
            $this->get_all_trans(date('Y'));
        }else{
            $this->get_all_trans($pecah[1]);
        }
       

        // echo 'tgl awal'.$final_start;
        // echo '<br>';
        // echo 'tgl ahir'.$pecah;
        // die;

        foreach ($months as $month) {
          $ga_quantity = $this->get_all_trans_where($month, "GA");
          $cit_quantity = $this->get_all_trans_where($month, "CITILINK");
          $nga_quantity = $this->get_all_trans_where($month, "NON_GA");
          $ga_robbing = $this->get_all_robbing_graph_where($month, "GA");
          $cit_robbing = $this->get_all_robbing_graph_where($month, "CITILINK");
          $nga_robbing = $this->get_all_robbing_graph_where($month, "NON_GA");
          $ga_actual = (($ga_quantity == 0) ? 0 : round((($ga_quantity-$ga_robbing)/$ga_quantity)*100, 2));
          $cit_actual = (($cit_quantity == 0) ? 0 : round((($cit_quantity-$cit_robbing)/$cit_quantity)*100, 2));
          $nga_actual = (($nga_quantity == 0) ? 0 : round((($nga_quantity-$nga_robbing)/$nga_quantity)*100, 2));
          $result["GA"][] = $ga_actual;
          $result["CITILINK"][] = $cit_actual;
          $result["NON_GA"][] = $nga_actual;
          $result_month[] = ((strlen($month) < 2) ? "0".$month : $month)."-".date("Y");
        }
        echo json_encode(array("month"=>$result_month, "data" => $result));
    }


    // public function type_pesawat()
    // {
    //     // code...
    //     $year = date('Y');
    //     $aircraft_ga = 'GA';
    //     $aircraft_non_ga = 'NON GA';
    //     $aircraft_citilink = 'CITILINK';
    //     $non_ga = array('maskapai' => 'NON GA');
    //     $ga = array('maskapai' => 'GA');
    //     $citilink = array('maskapai' => 'CITILINK');
    //
    //     $temp_total_ga = 0;
    //     $loop_ga = 0;
    //     $final_temp_ga = "";
    //     $data=array();
    //     $datas = $this->M_sla_component_avability->value_target_aircraft($year);
    //     foreach ($datas as $key) {
    //       // code...
    //       array_push($data,$data[$key['type_aircraft']]=$key['target_aircraft']);
    //     }
    //     $final_citilink=array();
    //     $final_ga=array();
    //     $final_non_ga=array();
    //     $query_ga = $this->M_sla_component_avability->aircraft_ess($year, $aircraft_ga);
    //     foreach ($query_ga as $key) {
    //         // code...
    //         $type = $key['actype'];
    //         // $data = array('B737NG' => 98.2, 'A330' => 98.3, 'ATR' => 99.5, 'CPJ' => 99.5);
    //         $data_search = explode("-", $key['actype']);
    //         $result = number_format(isset($data[$data_search[0]]) ? $data[$data_search[0]] : 0,1);
    //         $data_ess = $this->M_sla_component_avability->ess_by_aicrafttype($aircraft_ga, $type, $year);
    //         $data_robbing = $this->M_sla_component_avability->robbing_by_aicrafttype($aircraft_ga, $type, $year);
    //         if ($data_ess['jml'] == 0) {
    //             $actual = 0;
    //         } else {
    //             $actual = number_format((((abs($data_ess['jml']) - $data_robbing) / abs($data_ess['jml'])) * 100), 1);
    //         }
    //         $loop_ga += 1;
    //         $temp_total_ga += $actual;
    //
    //         if($actual >= $result){
    //             $val_col = '#8EC3A7';
    //         }else{
    //             $val_col = '#F24738';
    //         }
    //         $ok=(($actual > 100) ? 100 : $actual);
    //         $temp=array(
    //             'actype'=> $key['actype'],
    //             'color'=> $val_col,
    //             'actual'=> $ok,
    //             'result'=> $result
    //         );
    //         array_push($final_ga,$temp);
    //
    //         // $temp = "<div class='wadah col-md-12'>
    //         //             <div class='box box-widget widget-user' style='border-radius: 11px;'>
    //         //               <div class='widget-user-header bg-navy' style='padding: 2px;height: 20px;'>
    //         //                 <h3 class='title-manufacture aircraft_title hoverclass' style='font-size: 15px;'  onclick=aircraft('$key[actype]','GA')>$key[actype] <i class='fa fa-ellipsis-v pull-right' style='font-size: 10px;padding-right: 7px;padding-top: 4px;'></i></h3>
    //         //               </div>
    //         //               <div class='box-footer' style='padding: 0px;'>
    //         //                 <div class='row'>
    //         //                   <div class='col-sm-6 border-right' style='padding-left: 13px;'>
    //         //                     <div class='description-block'>
    //         //                       <h5 class='description-header colors_nonga' style='font-size:15px;margin-left: 10px;color:$val_col'>".(($actual > 100) ? 100 : $actual)."%</h5>
    //         //                       <span class='description-text' style='font-size: 12px;margin-left: 10px;'>Actual</span>
    //         //                     </div>
    //         //                   </div>
    //         //                   <div class='col-sm-6' style='padding-left: 5px;'>
    //         //                     <div class='description-block'>
    //         //                       <h5 class='description-header' style='font-size:15px'>$result%</h5>
    //         //                       <span class='description-text' style='font-size: 12px;margin-left:0px;'>Target</span>
    //         //                     </div>
    //         //                   </div>
    //         //                 </div>
    //         //               </div>
    //         //             </div>";
    //
    //         // $final_temp_ga = $final_temp_ga . $temp;
    //     }
    //
    //     $temp_total_nga = 0;
    //     $loop_nga = 0;
    //
    //
    //     $final_temp_n_ga = "";
    //     $query_n_ga = $this->M_sla_component_avability->aircraft_ess($year, $aircraft_non_ga);
    //     foreach ($query_n_ga as $key) {
    //         // code...
    //         $type = $key['actype'];
    //         // $data = array('B737NG' => 98.2, 'A330' => 98.3, 'ATR' => 99.5, 'CPJ' => 99.5);
    //         $data_search = explode("-", $key['actype']);
    //         $result = number_format(isset($data[$data_search[0]]) ? $data[$data_search[0]] : 0,1);
    //         $data_ess = $this->M_sla_component_avability->ess_by_aicrafttype($aircraft_non_ga, $type, $year);
    //         $data_robbing = $this->M_sla_component_avability->robbing_by_aicrafttype($aircraft_non_ga, $type, $year);
    //         // print_r($data_ess);exit;
    //         if ($data_ess['jml'] == 0) {
    //             $actual = "0";
    //         } else {
    //             $actual = number_format((((abs($data_ess['jml']) - $data_robbing) / abs($data_ess['jml'])) * 100), 1);
    //         }
    //
    //         $loop_nga += 1;
    //         $temp_total_nga += $actual;
    //
    //         if($actual >= $result){
    //             $val_col = '#8EC3A7';
    //         }else{
    //             $val_col = '#F24738';
    //         }
    //         $temp = array(
    //             'actype' => $key['actype'],
    //             'color' => $val_col,
    //             'actual' => (($actual > 100) ? 100 : $actual),
    //             'result' => $result
    //         );
    //         array_push($final_non_ga, $temp);
    //
    //         // $temp = "<div class='wadah col-md-12'>
    //         //             <div class='box box-widget widget-user' style='border-radius: 11px;'>
    //         //               <div class='widget-user-header bg-navy' style='padding: 2px;height: 20px;'>
    //         //                 <h3 class='title-manufacture aircraft_title hoverclass' style='font-size: 15px;'  onclick=aircraft('$key[actype]','NON-GA')>$key[actype] <i class='fa fa-ellipsis-v pull-right' style='font-size: 10px;padding-right: 7px;padding-top: 4px;'></i></h3>
    //         //               </div>
    //         //               <div class='box-footer' style='padding: 0px;'>
    //         //                 <div class='row'>
    //         //                   <div class='col-sm-6 border-right' style='padding-left: 13px;'>
    //         //                     <div class='description-block'>
    //         //                       <h5 class='description-header colors_nonga' style='font-size:15px;margin-left: 10px;color:$val_col'>".(($actual > 100) ? 100 : $actual)."%</h5>
    //         //                       <span class='description-text' style='font-size: 12px;margin-left: 10px;'>Actual</span>
    //         //                     </div>
    //         //                   </div>
    //         //                   <div class='col-sm-6' style='padding-left: 5px;'>
    //         //                     <div class='description-block'>
    //         //                       <h5 class='description-header' style='font-size:15px'>$result%</h5>
    //         //                       <span class='description-text' style='font-size: 12px;margin-left:0px;'>Target</span>
    //         //                     </div>
    //         //                   </div>
    //         //                 </div>
    //         //               </div>
    //         //             </div>";
    //         // $final_temp_n_ga = $final_temp_n_ga . $temp;
    //     }
    //
    //     $temp_total_citilink = 0;
    //     $loop_citilink = 0;
    //     $final_temp_ct = "";
    //     $query_citilink = $this->M_sla_component_avability->aircraft_ess($year, $aircraft_citilink);
    //     foreach ($query_citilink as $key) {
    //         // code...
    //         // $data = array('B737NG' => 98.2, 'A330' => 98.3, 'ATR' => 99.5, 'CPJ' => 99.5);
    //         $data_search = explode("-", $key['actype']);
    //         $result = number_format(isset($data[$data_search[0]]) ? $data[$data_search[0]] : 0,1);
    //         $type = $key['actype'];
    //         $data_ess = $this->M_sla_component_avability->ess_by_aicrafttype($aircraft_citilink, $type, $year);
    //         $data_robbing = $this->M_sla_component_avability->robbing_by_aicrafttype($aircraft_citilink, $type, $year);
    //         if ($data_ess['jml'] == 0 ) {
    //             $actual = "0";
    //         } else {
    //             $actual = number_format((((abs($data_ess['jml']) - $data_robbing) / abs($data_ess['jml'])) * 100), 1);
    //         }
    //         $loop_citilink += 1;
    //         $temp_total_citilink += $actual;
    //
    //         if($actual >= $result){
    //             $val_col = '#8EC3A7';
    //         }else{
    //             $val_col = '#F24738';
    //         }
    //         $temp = array(
    //             'actype' => $key['actype'],
    //             'color' => $val_col,
    //             'actual' => (($actual > 100) ? 100 : $actual),
    //             'result' => $result
    //         );
    //         array_push($final_citilink, $temp);
    //
    //         // $temp = "<div class='wadah col-md-12'>
    //         //             <div class='box box-widget widget-user' style='border-radius: 11px;'>
    //         //               <div class='widget-user-header bg-navy' style='padding: 2px;height: 20px;'>
    //         //                 <h3 class='title-manufacture aircraft_title hoverclass' style='font-size: 15px;'  onclick=aircraft('$key[actype]','CITILINK')>$key[actype] <i class='fa fa-ellipsis-v pull-right' style='font-size: 10px;padding-right: 7px;padding-top: 4px;'></i></h3>
    //         //               </div>
    //         //               <div class='box-footer' style='padding: 0px;'>
    //         //                 <div class='row'>
    //         //                   <div class='col-sm-6 border-right' style='padding-left: 13px;'>
    //         //                     <div class='description-block'>
    //         //                       <h5 class='description-header colors_nonga' style='font-size:15px;margin-left: 10px;color:$val_col'>".(($actual > 100) ? 100 : $actual)."%</h5>
    //         //                       <span class='description-text' style='font-size: 12px;margin-left: 10px;'>Actual</span>
    //         //                     </div>
    //         //                   </div>
    //         //                   <div class='col-sm-6' style='padding-left: 5px;'>
    //         //                     <div class='description-block'>
    //         //                       <h5 class='description-header' style='font-size:15px'>$result%</h5>
    //         //                       <span class='description-text' style='font-size: 12px;margin-left:0px;'>Target</span>
    //         //                     </div>
    //         //                   </div>
    //         //                 </div>
    //         //               </div>
    //         //             </div>";
    //         // $final_temp_ct = $final_temp_ct . $temp;
    //     }
    //
    //     $data = array('n_ga' => $final_non_ga,
    //                  'ga' => $final_ga,
    //                  'citilinks' => $final_citilink,
    //                  'g_ga' => number_format($loop_ga == 0 ? 0 : ($temp_total_ga / $loop_ga), 2),
    //                  'g_citilink' => number_format($loop_citilink == 0 ? 0 : ($temp_total_citilink / $loop_citilink), 2),
    //                  'g_non_ga' => number_format($loop_nga == 0 ? 0 : ($temp_total_nga / $loop_nga), 2),
    //                  'g_target' => 99);
    //
    //     echo json_encode($data);
    // }

    public function sladetailaircraft()
    {
        // code...
        $aircraft = $this->input->post('param_aircraft', true);
        $type = $this->input->post('param_type', true);

        $year = date('Y');
        $query = $this->M_sla_component_avability->skema_by_aicrafttype($aircraft, $type, $year);
        // $html_result = " <div class='col-md-12'>";
        $data=array();
        $datas = $this->M_sla_component_avability->value_target_aircraft($year);

        foreach ($datas as $key) {
          array_push($data,$data[$key['type_aircraft']]=$key['target_aircraft']);
        }

        $data_search = explode("-", $type);
        $result = isset($data[$data_search[0]]) ? $data[$data_search[0]] : 0;

        $pbth=0;
        $count_robbing_pbth=0;
        $count_actual_pbth=0;
        $jml_actual=0;
        $jml_robbing=0;

        $print=array();

        foreach ($query as $key) {
            // code...
            $query_e = $this->M_sla_component_avability->count_ess_by_skema($aircraft, $type, $year, $key['skema']);
            $query_r = $this->M_sla_component_avability->count_robbing_by_skema($aircraft, $type, $year, $key['skema']);
            if ($query_e['jml'] == 0) {
                // code...
                $actual = 0;
            } else {
                $actual = number_format((((abs($query_e['jml']) - $query_r) / abs($query_e['jml'])) * 100), 1);
            }
            if ($actual < $result) {
                // code...
                $class_color = 'progress-bar-red';
            } else {
                // code...
                $class_color = 'progress-bar-green';
            }

            $oke=explode(' ',$key['skema']);
            if(count($oke)!=0){
              if($oke[0]=="PBTH"){
                $pbth+=1;
                $jml_actual=$jml_actual+$query_e['jml'] ;
                $jml_robbing=$jml_robbing+$query_r;
              }else{
                $actual = number_format($actual, 2);
                $temp = array(
                    'skema' => $key['skema'],
                    'aktual' => (($actual > 100) ? 100 : $actual),
                    'color'=> $class_color,
                    'robbing' => $query_r
                );
                array_push($print,$temp);
              }
            }

        }

        // When Loop data PBTH available !=0
        if($pbth>0){
          if($jml_actual==0){
            $actual_tot=0;
          }else{
            $actual_tot= number_format((((abs($jml_actual)- $jml_robbing) / abs($jml_actual)) * 100), 1);
          }
          if ($actual_tot < $result) {
              // code...
              $class_color = 'progress-bar-red';
          } else {
              // code...
              $class_color = 'progress-bar-green';
          }
          $actual_tot = number_format($actual_tot, 2);
            $temp = array(
                'skema' => 'PBTH',
                'aktual' => (($actual_tot > 100) ? 100 : $actual_tot),
                'color' => $class_color,
                'robbing' => $jml_robbing
            );
            array_push($print, $temp);
        }

        $result = number_format($result, 2);


        $data = array('prints' => $print,
                'target'=> (($result > 100) ? 100 : $result)
                );
        echo json_encode($data);
    }

    // public function searchgrafik()
    // {
    //   // code...
    //   // print_r($_POST);exit();
    //   $start=$this->input->post('periode1',TRUE);
    //   $oke=explode('-',$start);
    //   $param=$oke[1]."-".$oke[0];
    //   $end=$this->input->post('periode2',TRUE);
    //   $oke=explode('-',$end);
    //   $param2=$oke[1]."-".$oke[0];
    //   $query=$this->M_sla_component_avability->getNameMonth($param,$param2);
    //
    //   $actual_ga=array();
    //   $actual_non_ga=array();
    //   $actual_citilink=array();
    //   $target=array();
    //   $bulan=array();
    //
    //   foreach ($query as $key ) {
    //     // code...
    //     $thn=substr($key,0,4);
    //     $bln=substr($key,5,2);
    //     $ess_ga=$this->M_sla_component_avability->get_ess_by_month_and_aircraft($bln,$thn,'GA');
    //     $ess_n_ga=$this->M_sla_component_avability->get_ess_by_month_and_aircraft($bln,$thn,'NON-GA');
    //     $ess_citilink=$this->M_sla_component_avability->get_ess_by_month_and_aircraft($bln,$thn,'CITILINK');
    //     $result_ess_ga = $ess_ga['jml'] == null ? 0 : $ess_ga['jml'];
    //     $result_ess_non_ga = $ess_n_ga['jml'] == null ? 0 : $ess_n_ga['jml'];
    //     $result_ess_citilink = $ess_citilink['jml'] == null ? 0 : $ess_citilink['jml'];
    //
    //     $rob_ga=$this->M_sla_component_avability->get_robbing_by_month_and_aircraft($bln,$thn,'GA');
    //     $rob_non_ga=$this->M_sla_component_avability->get_robbing_by_month_and_aircraft($bln,$thn,'NON-GA');
    //     $rob_citilink=$this->M_sla_component_avability->get_robbing_by_month_and_aircraft($bln,$thn,'CITILINK');
    //
    //     // echo $result_ess_ga."||".$rob_ga."<br>";
    //     if ($result_ess_ga==0) {
    //       // code...
    //       $temp_act_ga=0;
    //     }else{
    //       $temp_act_ga=abs(number_format((((abs($result_ess_ga)-$rob_ga)/abs($result_ess_ga))*100),2));
    //     }
    //     if ($result_ess_non_ga==0) {
    //       // code...
    //       $temp_act_non_ga=0;
    //     }else{
    //       $temp_act_non_ga=abs(number_format((((abs($result_ess_non_ga)-$rob_non_ga)/abs($result_ess_non_ga))*100),2));
    //     }
    //     if ($result_ess_citilink==0) {
    //       // code...
    //       $temp_act_citilink=0;
    //     }else{
    //         $temp_act_citilink=abs(number_format((((abs($result_ess_citilink)-$rob_citilink)/abs($result_ess_citilink))*100),2));
    //     }
    //
    //     array_push($bulan,$bln.'-'.$thn);
    //     array_push($actual_ga,(($temp_act_ga > 100) ? 100 : $temp_act_ga));
    //     array_push($actual_non_ga,(($temp_act_non_ga > 100) ? 100 : $temp_act_non_ga));
    //     array_push($actual_citilink,(($temp_act_citilink > 100) ? 100 : $temp_act_citilink));
    //     array_push($target,99);
    //
    //   }
    //
    //   $data = array('bulan' => $bulan ,
    //                 'actual_ga' => $actual_ga,
    //                 'actual_non_ga' => $actual_non_ga,
    //                 'actual_citilink' => $actual_citilink,
    //                 'target' => $target
    //                 );
    //
    //   echo json_encode($data);
    // }

    public function detail_on_table_grafik()
    {
        // code...
        // print_r($_POST);exit();
        $date = $this->input->post('param1', TRUE);
        $aircraft = $this->input->post('param2', TRUE);
        // print_r($date);exit;
        $thn = substr($date, 3, 4);
        $bln = substr($date, 0, 2);
        // echo $thn."||".$bln; exit();
        $query = $this->M_sla_component_avability->get_aircraft_type($aircraft, $thn, $bln);
        $no = 1;
        $dataa = array();
        foreach ($query as $key) {
            // code...
            $row = array();
            $row[] = $no++;
            $row[] = $key['actype'];
            $row[] = '<button type="button" class="btn btn-block btn-success btn-xs detail" data-aircraft="' . $aircraft . '" data-actype="' . $key['actype'] . '" data-bln="' . $bln . '" data-thn="' . $thn . '">Detail</button>';
            $dataa[] = $row;
        }
        $data = array('data' => $dataa);
        echo json_encode($data);

    }

    public function detai_on_month()
    {
        // code...
        // print_r($_POST);
        $aircraft = $this->input->post('aircrf', TRUE);
        $type = $this->input->post('type', TRUE);
        // $reg = $this->input->post('func_loc', TRUE);
        $year = $this->input->post('year', TRUE);
        $bln = $this->input->post('month', TRUE);
        $query = $this->M_sla_component_avability->skema_in_reg_by_aircraft_and_date($aircraft, $type, $year, $bln);
        $html_result = " <div class='col-md-12'>";
        $data = array('B737NG' => 98.2, 'A330' => 98.3, 'ATR' => 99.5, 'CPJ' => 99.5, 'CRJ' => 80);
        $data_search = explode("-", $type);
        $result = isset($data[$data_search[0]]) ? $data[$data_search[0]] : 99;
        $pbth = 0;
        $jml_actual = 0;
        $jml_robbing = 0;
        $print=array();
        foreach ($query as $key) {
            // code...
            // $query_e = $this->M_sla_component_avability->get_ess_by_month_and_aircraft_reg($aircraft, $type, $reg, $year, $bln, $key['skema']);
            $query_e = $this->M_sla_component_avability->get_ess_by_month_and_aircraft_reg($aircraft, $type, $year, $bln, $key['skema']);
            // $query_r = $this->M_sla_component_avability->get_rob_by_month_and_aircraft_reg($aircraft, $type, $reg, $year, $bln, $key['skema']);
            $query_r = $this->M_sla_component_avability->get_rob_by_month_and_aircraft_reg($aircraft, $type, $year, $bln, $key['skema']);
            if ($query_e['jml'] == NULL || $query_e['jml'] == 0) {
                // code...
                $actual = 0;
            } else {
                $actual = number_format((((abs($query_e['jml']) - $query_r) / abs($query_e['jml'])) * 100), 1);
            }

            if ($actual < $result) {
                // code...
                $class_color = 'progress-bar-red';
            } else {
                // code...
                $class_color = 'progress-bar-green';
            }

            // Pecah Ketika Skema PBTH
            $oke = explode(' ', $key['skema']);
            if (count($oke) != 0) {
                if ($oke[0] == "PBTH") {
                    $pbth += 1;
                    $jml_actual = $jml_actual + $query_e['jml'];
                    $jml_robbing = $jml_robbing + $query_r;
                }else{
                    $temp = array(
                        'skema' => $key['skema'],
                        'aktual' => (($actual > 100) ? 100 : $actual),
                        'color' => $class_color,
                        'robbing' => $query_r,
                        'aircraft'=>$aircraft,
                        'type'=>$type,
                        'year'=>$year,
                        'month'=>$bln
                    );
                    array_push($print, $temp);
                }
            }

        }

        // When $pbth != 0 bearrti ada nilai pbth
        if($pbth>0){
            if($jml_actual!=0){
                $actual = number_format((((abs($jml_actual) - $jml_robbing) / abs($jml_actual)) * 100), 1);
            }else{
                $actual=0;
            }
            if ($actual < $result) {
                // code...
                $class_colors = 'color-progres-red';
            } else {
                // code...
                $class_colors = 'color-progres-blue';
            }
            $temp = array(
                'skema' => 'PBTH',
                'aktual' => (($actual > 100) ? 100 : $actual),
                'color' => $class_color,
                'robbing' => $jml_robbing,
                'aircraft' => $aircraft,
                'type' => $type,
                'year' => $year,
                'month' => $bln
            );
            array_push($print, $temp);
        }

        $data = array('prints' => $print,
        'target'=>$result);
        echo json_encode($data);
    }


// Start Export File Xls Data Upload

    public function export_xls_mtype()
    {
        // code...
        // $data = array(
        //     'title' => 'Format Upload MTYPE SLA Component Avability',
        //     'excels' => $this->M_sla_component_avability->get_export_mtype_xls()
        // );
        //
        // $this->load->view('SLA Component Availability/export_xls_mtype', $data);

        $list = $this->M_sla_component_avability->get_export_mtype_xls();
        $heading = array('PART NUMBER', 'MTYPE');
        $this->load->library('PHPExcel');
        //Create a new Object
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setTitle('Format MTYPE SLA Comp Avail');
        //Loop Heading
        $rowNumberH = 1;
        $colH = 'A';
        foreach ($heading as $h) {
            $objPHPExcel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
            $colH++;
        }
        //Loop Result
        $totn = count($list);
        $maxrow = $totn + 1;
        $row = 2;
        // echo "<pre>";
        //   print_r($list);
        // echo "</pre>";
        // exit;
        foreach ($list as $key) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $key->part_number);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $key->mtype);
            $row++;
        }
        //Freeze pane
        $objPHPExcel->getActiveSheet()->freezePane('A2');
        //Cell Style
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:B' . $maxrow)->applyFromArray($styleArray);
        //Save as an Excel BIFF (xls) file


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Format MTYPE Component Availability.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        ob_start();
        $objWriter->save('php://output');
        exit;
    }

    public function export_xls_datapn()
    {
        // // code...
        // $data = array(
        //     'title' => 'Format Upload DATA PN SLA',
        //     'excels' => $this->M_sla_component_avability->get_export_datapn_xls()
        // );
        //
        // // echo json_encode($data); exit();
        // $this->load->view('SLA Component Availability/export_xls_datapn', $data);
        $list = $this->M_sla_component_avability->get_export_datapn_xls();
        $heading = array('PN REG', 'ESS', 'Part Number', 'Alternate', 'Description', 'Cap', 'Aircraft', 'Skema', 'Tot Float Spare', 'Remarks');
        $this->load->library('PHPExcel');
        //Create a new Object
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setTitle('Format DATA PN SLA Comp Avail');
        //Loop Heading
        $rowNumberH = 1;
        $colH = 'A';
        foreach ($heading as $h) {
            $objPHPExcel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
            $colH++;
        }
        //Loop Result
        $totn = count($list);
        $maxrow = $totn + 1;
        $row = 2;
        // echo "<pre>";
        //   print_r($list);
        // echo "</pre>";
        // exit;
        foreach ($list as $key) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $key->pnreg);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $key->ess);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $key->part_number);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $key->alternate);
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $key->description);
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $key->cap);
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $key->aircraft);
            $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $key->skema);
            $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $key->tot_float_spare);
            $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $key->remarks);
            $row++;
        }
        //Freeze pane
        $objPHPExcel->getActiveSheet()->freezePane('A2');
        //Cell Style
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:J' . $maxrow)->applyFromArray($styleArray);
        //Save as an Excel BIFF (xls) file


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Format DATA PN Component Availability.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        ob_start();
        $objWriter->save('php://output');
        exit;

    }
// End Export File Xls Data Upload


    public function TableDetailRobbing(){
        $aircraft=$this->input->post('aircrafts',true);
        $type=$this->input->post('types',true);
        $bln=$this->input->post('months',true);
        $year=$this->input->post('years',true);
        $key=$this->input->post('skemas',true);


        $query=$this->M_sla_component_avability->get_datatables_rob_month($aircraft, $type, $bln, $year, $key);
        $jml=$this->M_sla_component_avability->count_all_rob_month($aircraft, $type, $bln, $year, $key);

        if(count($query)!=0){
          $no=$_REQUEST['start']+1;
          foreach ($query as $key) {
            // code...
            $print['data'][]=array(
              $no,
              $key['material'],
              $key['description'],
              $key['donor_func_loc'],
              $key['receive_func_loc'],
              $key['created_on']
            );
            $no++;
          }
        }else{
          $print['data']=array();
        }
        $print['draw']= $_REQUEST['draw'];
        $print['recordsFiltered']=$jml;
        $print['recordsTotal']  = $jml;
        echo json_encode($print);
    }

}
