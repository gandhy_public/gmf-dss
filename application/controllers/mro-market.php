<style>
    .wadah{
        padding-left: 5px;
        padding-right: 5px;
    }
</style>

<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>


<section class="content">
	<div class="box">
		<div class="box-header with-border">
		  <h3 class="box-title"><?= $title ?></h3>
		  <div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
					title="Collapse">
			  <i class="fa fa-minus"></i></button>
		  </div>
		</div>
		<div class="box-body">
		  <div class="row">
            <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box-header with-border" style="border-bottom: 1px solid #0f2233;">
                    <div class="wadah col-lg-2 col-md-2 col-sm-12 col-xs-12" style="margin-top: 50px;">
                        <div class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="year1" placeholder="Start Year" value="<?= $year_from ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="year2" placeholder="End Year" value="<?= $year_to ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <select id="g1_aircraft_type" class="form-control" style="width: 100%;">
                              <option value="all" disabled selected>Aircraft Types</option>
                              <option value="all">All</option>
                              <?php foreach ($aircraft_types as $item): ?>
                                <option value="<?php echo $item ?>" ><?= $item; ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select id="g1_engine_type" class="form-control" style="width: 100%;">
                              <option value="all" selected="selected">Engine Type</option>
                              <option value="all">All</option>
                              <?php foreach ($engine_types as $item): ?>
                                <option value="<?php echo $item['id_engine_family'] ?>"><?php echo $item['name'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select id="g1_region" class="form-control" style="width: 100%;">
                              <option value="all" selected="selected">Region</option>
                              <option value="all">All</option>
                              <?php foreach ($regions as $item): ?>
                                  <option value="<?php echo $item['id_region'] ?>"><?php echo $item['region'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select id="g1_operator" class="form-control" style="width: 100%;">
                              <option value="all" selected="selected">Operator Name</option>
                              <option value="all">All</option>
                              <?php foreach ($operators as $item): ?>
                                  <option value="<?php echo $item['id_operator'] ?>"><?php echo $item['name'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                          <button id="g1_submit" class="btn btn-block bg-navy">
                            <i class="fa fa-search"></i> Search
                          </button>
                        </div>
                    </div>
                    <div class="wadah col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div id="grafik1"></div>
                    </div>
                </div>
            </div>

           <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box-header with-border" style="border-bottom: 1px solid #0f2233;">

                    <div class="wadah col-lg-2 col-md-2 col-sm-12 col-xs-12" style="margin-top: 50px;">
                        <div class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="year3" placeholder="Start Year" value="<?= $year_from ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="year4" placeholder="End Year" value="<?= $year_to ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <select id="g2_aircraft_type" class="form-control" style="width: 100%;">
                              <option value="all" disabled selected>Aircraft Types</option>
                              <option value="all">All</option>
                              <?php foreach ($aircraft_types as $item): ?>
                                <option value="<?= $item ?>" ><?= $item; ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select id="g2_engine_type" class="form-control" style="width: 100%;">
                              <option value="all" selected="selected">Engine Type</option>
                              <option value="all">All</option>
                              <?php foreach ($engine_types as $item): ?>
                                <option value="<?php echo $item['id_engine_family'] ?>"><?php echo $item['name'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select id="g2_operator" class="form-control" style="width: 100%;">
                              <option value="all" selected="selected">Operator Name</option>
                              <option value="all">All</option>
                              <?php foreach ($operators as $item): ?>
                                  <option value="<?php echo $item['id_operator'] ?>"><?php echo $item['name'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                          <button id="g2_submit" class="btn btn-block bg-navy">
                            <i class="fa fa-search"></i> Search
                          </button>
                        </div>
                    </div>
                    <div class="wadah col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div id="grafik2"></div>
                    </div>
                </div>
            </div>

            <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box-header with-border" style="border-bottom: 1px solid #fff;">

                    <div class="wadah col-lg-2 col-md-2 col-sm-12 col-xs-12" style="margin-top: 50px;">
                        <div class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="year5" placeholder="Start Year" value="<?= $year_from?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="year6" placeholder="End Year" value="<?= $year_to ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <select id="g3_engine_type" class="form-control" style="width: 100%;">
                              <option value="all" selected="selected">Engine Type</option>
                              <option value="all">All</option>
                              <?php foreach ($engine_types as $item): ?>
                                <option value="<?php echo $item['id_engine_family'] ?>"><?php echo $item['name'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select id="g3_region" class="form-control" style="width: 100%;">
                              <option value="all" selected="selected">Region</option>
                              <option value="all">All</option>
                              <?php foreach ($regions as $item): ?>
                                  <option value="<?php echo $item['id_region'] ?>"><?php echo $item['region'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select id="g3_operator" class="form-control" style="width: 100%;">
                              <option value="all" selected="selected">Operator Name</option>
                              <option value="all">All</option>
                              <?php foreach ($operators as $item): ?>
                                  <option value="<?php echo $item['id_operator'] ?>"><?php echo $item['name'] ?></option>
                              <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group">
                          <button id="g3_submit" class="btn btn-block bg-navy">
                            <i class="fa fa-search"></i> Search
                          </button>
                        </div>
                    </div>
                    <div class="wadah col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div id="grafik3"></div>
                    </div>
                </div>
            </div>
		</div>
		</div>
	</div>
</section>

<script>

  //Date range picker
  for(var i=1; i<=6; i++){
    $('#year'+i).datepicker({
      format: "yyyy",
      viewMode: "years",
      minViewMode: "years",
      autoclose: true
    });
  }

$(document).on("click", "#g1_submit", function(e){
   var year_from = $("#year1").val();
   var year_to = $("#year2").val();
   var aircraft_type = $('#g1_aircraft_type').find(":selected").val();
   var engine_type = $('#g1_engine_type').find(":selected").val();
   var region = $('#g1_region').find(":selected").val();
   var operator = $('#g1_operator').find(":selected").val();

   $.ajax({
     url: "<?= base_url(); ?>index.php/mro_dashboard/get_data_by_year",
     data: {'year_from': year_from, 'year_to': year_to, 'aircraft_type': aircraft_type, 'engine_type_id': engine_type, 'region_id': region, 'operator_id': operator},
     type: 'POST',
     dataType: "JSON",
     success: function(data){
       Highcharts.chart('grafik1', {
           chart: {
               type: 'column',
               height:450,
           },
           title: {
               text: 'YEAR'
           },
           credits:{
               enabled: false
           },
           xAxis: {
               categories: data.categories
           },
           yAxis: {
               min: 0,
               title: {
                   text: 'Total Cost'
               },
           },
           tooltip: {
               headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
               pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                   '<td style="padding:0"><b>USD {point.y}</b></td></tr>',
               footerFormat: '</table>',
               shared: true,
               useHTML: true
           },
           plotOptions: {
               column: {
                   stacking: 'normal',
                   dataLabels: {
                       enabled: false,
                       color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                   }
               }
           },
           series: data.series
       });
    }
   });
});

$(document).on("click", "#g2_submit", function(e){
   var year_from = $("#year3").val();
   var year_to = $("#year4").val();
   var aircraft_type = $('#g2_aircraft_type').find(":selected").val();
   var engine_type = $('#g2_engine_type').find(":selected").val();
   var operator = $('#g2_operator').find(":selected").val();



    $.ajax({
      url: "<?= base_url(); ?>index.php/mro_dashboard/get_data_by_region",
      data: {'year_from': year_from, 'year_to': year_to, 'aircraft_type': aircraft_type, 'engine_type_id': engine_type, 'operator_id': operator},
      type: 'POST',
      dataType: "JSON",
      success: function(data){
        Highcharts.chart('grafik2', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'REGION'
            },
            credits:{
                enabled: false
            },
            xAxis: {
                categories:data.categories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Cost'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>USD {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
           series: data.series
         });
      }
     });
  });

$(document).on("click", "#g3_submit", function(e){
   var year_from = $("#year5").val();
   var year_to = $("#year6").val();
   var engine_type = $('#g3_engine_type').find(":selected").val();
   var region_id = $('#g3_region').find(":selected").val();
   var operator = $('#g3_operator').find(":selected").val();

   // alert("year from: " + year_from + ", year to: " + year_to
   // +" engine_type: "+ engine_type + ", operator: " + operator +", region: " + region);

   $.ajax({
     url: "<?= base_url(); ?>index.php/mro_dashboard/get_data_by_segment",
     data: {'year_from': year_from, 'year_to': year_to, 'engine_type_id': engine_type, 'region_id':region_id, 'operator_id': operator},
     type: 'POST',
     dataType: "JSON",
     success: function(data){
       Highcharts.chart('grafik3', {
           chart: {
               type: 'column'
           },
           credits:{
               enabled: false
           },
           title: {
               text: 'BUSINESS SEGMENT'
           },
           xAxis: {
             categories:data.categories,
             crosshair: true
           },
           yAxis: {
               min: 0,
               title: {
                   text: 'Total Cost'
               }
           },
           tooltip: {
               headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
               pointFormat: '<tr><td style="color:{series.color};padding:0"></td>' +
                   '<td style="padding:0"><b>USD {point.y}</b></td></tr>',
               footerFormat: '</table>',
               shared: true,
               useHTML: true
           },
           legend: {
               enabled: false
           },
           series: [{
               name: data.name,
               data: data.data,
               color: data.color
           }]
       });
     }
   });

 });

 // series: [{
 //     name: data.name,
 //     data: [
 //         ['Airframe Heavy', 589.09],
 //         ['Components', 206.37],
 //         ['Engine Maintenance', 206.37],
 //         ['Line Maintenance', 285.31],
 //         ['Modifications', 748.85]
 //     ],
 //     color: '#4cc3d9'
 // }]
</script>
