<?php
defined('BASEPATH') or exit ('no direct script access allowed');

/**
 *
 */
class Kpi_slabaseops extends MY_Controller
{

  function __construct()
  {
    # code...
    parent::__construct();
    $this->load->database();
    $this->load->helper("url");
    $this->load->helper("encript");
    $this->load->helper("kpiformula_helper");
    $this->load->model("M_menu");
    $this->load->model("M_csi_dashboard");
    $this->load->model("M_slabaseops");
    $this->load->model("M_kpi_sla_master");
    $this->load->helper("kpiformula");
  }

  public function index()
  {
    # code...
    $data = array(
      "content" => "kpi/kpi_slabaseops",
      "title" => "KPI SLA BASE OPS",
      "small_tittle" => "",
      "breadcrumb" => [""],
      "menu" => $this->M_menu->tampil()
    );
    $this->load->view("layouts", $data);
  }

  public function get_item_chart() {
    $s_date= $this->input->post("date_start");
    $type = $this->input->post('type');
    $count = 0;
    $data = array();
    $item = $this->M_slabaseops->get_item_chart($type, $s_date);

    $result['data'] = $item;
    $result['length'] = count($item);

    echo json_encode($result);
  }

  public function get_chart_detail() {
    // $s_date = '01-'.date('Y');
    // $e_date = date('m-Y');

    $s_date= $this->input->post("date_start");
    $e_date= $this->input->post("date_end");
    $child = $this->input->post('id_item');
    $ki_id = $this->input->post('ki_id');
    $awal = explode("-",$s_date);
    $awal_bulan = $awal[0];
    $awal_tahun = $awal[1];
    $colors = array('#fd9214', '#05354D', '#ffc44f');
    $data = array();
    $dtTarget = array();
    $dtTargetLower = array();
    $dtTargetUpper = array();

    $arrMonths = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    // $arrMonths = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

    $row_kpi_item = $this->M_slabaseops->get_by_id($ki_id);
    $chart = $this->M_slabaseops->get_data_chart_item($child, $s_date, $e_date);
    $res_target = $this->M_slabaseops->get_data_target_item($ki_id, $s_date, $e_date);

    
    $month = array();
    $id_month = array();
    $dtkpi = null;
    $title_y = '';
    
    if (!empty($chart)) {
      foreach ($chart as $key => $value) {
        $month[]= $arrMonths[(int)$value->bulan-1].'-'.$awal_tahun;
        $dtkpi[]= (float)$value->avg_;
        if($value->ki_uom != ''){
          $title_y = $value->ki_uom;
        }else{
          $title_y = '';
        }
        
      }
    }
    // echo json_encode($child);exit;

    $text_target = "Target";
    if (strtolower($row_kpi_item->kf_name) == "stabilize") {
      $text_target = "Target Lower";
      foreach ($res_target as $key => $value) {
        $found_t = strpos($value->target, '|');
        if (!$found_t) {
          $dtTargetLower[]= (float)$value->target;
          $dtTargetUpper[]= null;
        }else{
          $trgt = explode("|", $value->target);
          $target_lower = $trgt[0];
          $target_upper = $trgt[1];
          $dtTargetLower[]= (float)$target_lower;
          $dtTargetUpper[]= (float)$target_upper;
        }
      }
    }else{
      $text_target = "Target";
      foreach ($res_target as $key => $value) {
        $dtTarget[]= (float)$value->target;
      }
    }

    // if (!empty($dtTarget)) {

      if (strtolower($row_kpi_item->kf_name) == "stabilize") {
        $targetUpp = array();
        $targetUpp['name'] = 'Target Upper';
        $targetUpp['data'] = $dtTargetUpper;
        $targetUpp['color'] = $colors[0];
        $targetUpp['marker'] = array('enabled' => false);
        array_push($data, $targetUpp);

        $target = array();
        $target['name'] = $text_target;
        $target['data'] = $dtTargetLower;
        $target['color'] = $colors[2];
        $target['marker'] = array('enabled' => false);
        array_push($data, $target);

        $kpi = array();
        $kpi['name'] = 'Actual';
        $kpi['data'] = $dtkpi;
        $kpi['color'] = $colors[1];
        $kpi['type'] = 'areaspline';
        $kpi['lineWidth'] = 1;
        array_push($data, $kpi);
      }else{
        $target = array();
        $target['name'] = $text_target;
        $target['data'] = $dtTarget;
        $target['color'] = $colors[0];
        $target['marker'] = array('enabled' => false);
        array_push($data, $target);

        $kpi = array();
        $kpi['name'] = 'Actual';
        $kpi['data'] = $dtkpi;
        $kpi['color'] = $colors[1];
        $kpi['type'] = 'areaspline';
        $kpi['lineWidth'] = 1;
        array_push($data, $kpi);
      }

      $result = array('categories' => $month, 'series' => $data, 'title_y' => $title_y);
      echo json_encode($result);
    // }
  }

  public function get_chart_all() {
    $s_date = '01-'.date('Y');
    $e_date = date('m-Y');
    $this->get_chart_("SLA Base Operationss", $s_date, $e_date);
  }

  public function get_chart_ga() {
    $s_date= $this->input->post("date_start");
    $e_date= $this->input->post("date_end");
    $this->get_chart_('SLA Base', $s_date, $e_date);
    // $this->get_chart_('CSF GA', $s_date, $e_date);
  }

  public function get_chart_nga() {
    $s_date= $this->input->post("date_start");
    $e_date= $this->input->post("date_end");
    $this->get_chart_('CSF NGA', $s_date, $e_date);
  }

  public function get_chart_($type="", $s_date, $e_date){
    $akhir = explode("-",$e_date);
    $awal = explode("-",$s_date);
    $akhir_bulan = $akhir[0];
    $awal_bulan = $awal[0];
    $akhir_tahun = $akhir[1];
    $awal_tahun = $awal[1];
    $colors = array('#fd9214', '#05354D');
    $data = array();
    $dtTarget = array();
    $month = array();
    $id_month = array();
    $dtkpi = null;

    $arrMonths = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    // $arrMonths = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

    
    if ($type == "SLA Base Operationss") {
      
      // $akhir_month = date('m');
      // $akhir_year = date('Y');
      $year = date('Y');
      // for ($i=1; $i<=$akhir_month ; $i++) {
        //   $data_temp_id = $this->M_kpi_sla_master->getTotalScoreNotLevelOneWhere($kpi_item->ki_id, $i, $akhir_year, '2');
        //   $totalScore = 0;
        //   foreach ($data_temp_id as $temp_id) {
          //     $newTotal = $this->M_kpi_sla_master->getTotalScoreWhere($temp_id["ki_id"], $i, $akhir_year, '2');
          //     $totalScore += number_format(($newTotal*$temp_id["weight"])/100, 2);
          //   }
          
          //   $month[]= $arrMonths[$i-1].'-'.$akhir_year;
          //   $dtkpi[] = array(floatval(is_null($totalScore)?0:number_format($totalScore, 2)));
          //   $dtTarget[]= 100;
          // }
          $kpi_item_ga = $this->M_slabaseops->get_by_name('SLA Base');
          $kpi_item_nga = $this->M_slabaseops->get_by_name('CSF NGA');
          for ($i=1; $i<=$akhir_bulan ; $i++) {
            // $totalScore_ga = $this->M_kpi_sla_master->getTotalScoreWhere($kpi_item_ga->ki_id, $i, $akhir_tahun, '1');
            // $totalScore_nga = $this->M_kpi_sla_master->getTotalScoreWhere($kpi_item_nga->ki_id, $i, $akhir_tahun, '1');
            $maxLevel_ga = $this->M_kpi_sla_master->maxLevel($kpi_item_ga->ki_id);
            $maxLevel_nga = $this->M_kpi_sla_master->maxLevel($kpi_item_nga->ki_id);
            $finalTotalScores_ga=$this->FunctionName($maxLevel_ga,$kpi_item_ga->ki_id,$i,$year);
            $finalTotalScores_nga=$this->FunctionName($maxLevel_nga,$kpi_item_nga->ki_id,$i,$year);

            
            $month[]= $arrMonths[$i-1].'-'.$akhir_tahun;
            $dtkpi[]= ($finalTotalScores_ga['score']+$finalTotalScores_nga['score'])/2;
            $dtTarget[]= 100;
          }

          // for ($i=1; $i<=$akhir_bulan ; $i++) {
          //   $totalScore = $this->M_kpi_sla_master->getTotalScoreWhere($kpi_item_nga->ki_id, $i, $akhir_tahun, '1');
            
          //   $month[]= $arrMonths[$i-1].'-'.$akhir_tahun;
          //   $dtkpi[]= (float)$totalScore;
          //   $dtTarget[]= 100;
          // }


        }else{
          $kpi_item = $this->M_slabaseops->get_by_name($type);
          for ($i=$awal_bulan; $i<=$akhir_bulan ; $i++) {
                // $totalScore = $this->M_kpi_sla_master->getTotalScoreWhere($kpi_item->ki_id, $i, $akhir_tahun, '1');
                $maxLevel_ga = $this->M_kpi_sla_master->maxLevel($kpi_item->ki_id);
                $finalTotalScores=$this->FunctionName($maxLevel_ga,$kpi_item->ki_id,$i,$akhir_tahun);  
                
            $month[]= $arrMonths[$i-1].'-'.$akhir_tahun;
            $dtkpi[]= (float)$finalTotalScores['score'];
            $dtTarget[]= 100;
          }
    }



    // if (empty($type) || $type=="") {
    //   $chart = $this->M_slabaseops->get_chart('SLA BGA', $s_date, $e_date);
    //   $chart_nga = $this->M_slabaseops->get_chart('SLA BGA', $s_date, $e_date);
    //   $dtnga = array();
    //   if (!empty($chart)) {
    //     foreach ($chart_nga as $key => $val) {
    //       $dtnga[$key]= (float)$val->avg_;
    //     }

    //     foreach ($chart as $key => $value) {
    //       $month[]= $arrMonths[(int)$value->bulan-1];
    //       $dtkpi[]= ((float)$value->avg_+$dtnga[$key])/2;
    //       $dtTarget[]= 100;
    //     }
    //   }
    // }else{
    //   $chart = $this->M_slabaseops->get_chart($type, $s_date, $e_date);

    //   if (!empty($chart)) {
    //     foreach ($chart as $key => $value) {
    //       $month[]= $arrMonths[(int)$value->bulan-1];
    //       $dtkpi[]= (float)$value->avg_;
    //       $dtTarget[]= 100;
    //     }
    //   }
    // }

    $target = array();
    $target['name'] = 'Target';
    $target['data'] = $dtTarget;
    $target['color'] = $colors[0];
    array_push($data, $target);

    $kpi = array();
    $kpi['name'] = $akhir_tahun;
    $kpi['data'] = $dtkpi;
    $kpi['color'] = $colors[1];
    array_push($data, $kpi);

    $result = array('categories' => $month, 'series' => $data);
    echo json_encode($result);
  }

  public function FunctionName($maxLevel,$ki_id,$mon,$year)
    {
        # code...
                    // $finalTotalScore=0;
                    $arrayTemp=array();
                    $parentArr=array();
                    for ($k=$maxLevel; $k >=1 ; $k--) { 
                        # code...

                        $items = $this->M_kpi_sla_master->getPenghuni($k,$ki_id);                        
                        $parent = $this->M_kpi_sla_master->getGroupLevelParent($k,$ki_id);
                       
                       
                        if($k==$maxLevel){
                            
                             foreach ($parent as $key ) {
                                # code...
                                $tempTotalYtd=0;
                                $tempTotal=0;
                                for ($p=0; $p < count($items) ; $p++) { 
                                    # code...
                                    if($items[$p]['ki_parent_id']==$key['ki_parent_id']){
                                       $dataM = $this->M_slabaseops->listActualWhere($items[$p]['ki_id'],$mon,$year);
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                           $tempTotal+=number_format(score(achievment($dataM[$z]->kid_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2);
                                           $tempTotalYtd+=number_format(score(achievment($dataM[$z]->kid_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2) ;
                                       }
                                    }
                                }
                                $arrayTemp[$key['ki_parent_id']] = $tempTotal;
                                $parentArr[$key['ki_parent_id']] = $tempTotalYtd;
                            }

                        }
                        else{
                            $tempFuck=0;
                            $tempFuckYtd=0;

                            foreach ($parent as $keys ) {
                                # code...
                                for ($q=0; $q < count($items) ; $q++) { 
                                    if (array_key_exists($items[$q]['ki_id'],$arrayTemp) && $items[$q]['ki_parent_id']==$keys['ki_parent_id']){
                                        $tempFuck+=$arrayTemp[$items[$q]['ki_id']]*($items[$q]['ki_weight']/100);
                                        $tempFuckYtd+=$parentArr[$items[$q]['ki_id']]*($items[$q]['ki_weight']/100);
                                    }elseif( $items[$q]['ki_parent_id']==$keys['ki_parent_id']){
                                       $dataM = $this->M_slabaseops->listActualWhere($items[$q]['ki_id'],$mon,$year);
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                           $tempFuck+=number_format(score(achievment($dataM[$z]->kid_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2);
                                           $tempFuckYtd+=number_format(score(achievment($dataM[$z]->kid_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2) ;
                                       }
                                    }
                                }
                               
                                $arrayTemp[$keys['ki_parent_id']] = $tempFuck;
                                $parentArr[$keys['ki_parent_id']] = $tempFuckYtd;
                                // $parentArr[$key['ki_parent_id']] = $tempTotal*$items[$q]['ki_id'];
                                $tempFuck=0;
                                $tempFuckYtd=0;
                                
                            }

                            

                        }
                        
                    }

                    return $finalTotalScore= array('score'=>$arrayTemp[$ki_id],'scoreYtd'=>$parentArr[$ki_id]);
    }
}
