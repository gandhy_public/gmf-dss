<?php

defined("BASEPATH") or exit('no direct script access allowed');

class M_cabin extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_menu");
        $this->load->model("M_cabin_performance");
    }

    public function index()
    {
        $data = array(
            "content" => "Cabin Performance/master_data",
            "title" => "Cabin Master Data",
            "small_tittle" => "",
            "breadcrumb" => ["Cabin Master Data"],
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }

    // public function test()
    // {
    //   # code...
    //    $data = array('target_val'=>75,'target_year' => '2018', 'target_type'=>'Cabin' );
    //   $sql=$this->M_cabin_performance->insert_target($data);
    //   echo $sql;
    // }

    public function store()
    {
        # code...

        $target = $this->input->post('target', true);
        $year = $this->input->post('year', true);

        $data = array('target_val' => $target, 'target_year' => $year, 'target_type' => 'Cabin');
        if ($target == '') {
            # code...
            $msg = array('notif' => 'warning',
                'msg' => 'Data Target Kosong'
            );
        } else {
            $sql = $this->M_cabin_performance->get_target($year, 'Cabin');
            if ($sql == 1) {
                # code...
                if ($this->M_cabin_performance->update_target($data)) {
                    $msg = array('notif' => 'success',
                        'msg' => 'Update Data Sukses '
                    );
                }
            } else {
                if ($this->M_cabin_performance->insert_target($data)) {
                    $msg = array('notif' => 'success',
                        'msg' => 'Insert Data Sukses'
                    );
                }
            }
        }
        echo json_encode($msg);
    }

    public function get_target()
    {
        $in['tahun'] = date("Y");
        $cek = $this->M_cabin_performance->cek_target($in);
        // $value = str_replace('"','',$cek);
        $value = (int)$cek;
        echo json_encode($value);
    }

}

?>
