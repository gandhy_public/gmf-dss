<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Master_RPCR extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("M_menu");
        $this->load->model("m_repetitive_mitigation");
        $this->load->model("Master_Repetitive_hill");
    }

    function index()
    {
        $data = array(
            "content" => "repetitive/v_master_data",
            "title" => "Master Data Repetitive Problem Closing Rate",
            "small_tittle" => "",
            "breadcrumb" => ["Master Data Repetitive Problem Closing Rate"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }


    public function data_list($p_start = "", $p_end = "")
    {
        // $start_date = explode("-", $p_start, 2);
        // echo $start_date[0];
        if ($p_start == "" || $p_start == 0 || $p_end == "" || $p_end == 0) {
            $list = $this->m_repetitive_mitigation->get_datatables();
            $recordsTotal = $this->m_repetitive_mitigation->count_all();
            $recordsFiltered = $this->m_repetitive_mitigation->count_filtered();
        } else {
            $list = $this->m_repetitive_mitigation->get_datatables($p_start, $p_end);
            $recordsTotal = $this->m_repetitive_mitigation->count_all($p_start, $p_end);
            $recordsFiltered = $this->m_repetitive_mitigation->count_filtered($p_start, $p_end);
        }

        // $list = $this->m_repetitive_mitigation->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
            $no++;
            $row = array();
            $row[] = '<center>'.$no.'</center>';
            $row[] = '<center>'.$val->notification.'</center>';
            $row[] = '<center>'.$val->functional_location.'</center>';
            $row[] = $val->description;
            $row[] = '<center>'.$val->own.'</center>';
            $row[] = '<center>'.$val->actype.'</center>';
            $row[] = '<center>'.date("d-m-Y", strtotime($val->notif_date)).'</center>';
            $row[] = '<center>'.date("d-m-Y", strtotime($val->req_start)).'</center>';
            if ($val->mitigasi_fk_id != null) {
                $row[] = true;
            } else {
                $row[] = false;
            }
            $row[] = $val->system_status;
            $row[] = '<center>'.date("d-m-Y", strtotime($val->created_on)).'</center>';;
            $row[] = '<center><a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="ajax_edit(' . "'" . $val->notification . "'" . ')"><i class="glyphicon glyphicon-pencil"></i></a></center>';
            // <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="ajax_delete('."'".$val->target_id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->m_repetitive_mitigation->count_all(),
            "recordsFiltered" => $this->m_repetitive_mitigation->count_filtered(),
            "data" => $data
        );

        $this->output->set_output(json_encode($output));
    }

    public function target_rpcr()
    {
        $in['target'] = $this->input->post('target');
        $in['tahun'] = $this->input->post('year');
        $success = '';
        $cek = $this->Master_Repetitive_hill->cek_target($in);
        if ($cek == '') {
            $insert = $this->Master_Repetitive_hill->add_target_rpcr($in);
            $success = '1';
        } else {
            $update = $this->Master_Repetitive_hill->update_target_rpcr($in);
            $success = '2';
        }
        echo json_encode($success);
    }

    public function get_target()
    {
        $in['tahun'] = date("Y");

        $cek = $this->Master_Repetitive_hill->cek_target($in);
        // $value = str_replace('"','',$cek);
        $value = (float)$cek;
        echo json_encode($value);

    }

    public function show_update($id)
    {
        $mitigasi = $this->m_repetitive_mitigation->get_mitigasi_by_id($id);
        if (!empty($mitigasi)) {
            if (strtolower($mitigasi->mitigasi_type) == "repetitive") {
                $data['mitigasi'] = $mitigasi;
            }
        }
        $data['repetitive_d5'] = $this->m_repetitive_mitigation->get_by_id($id);

        $this->output->set_output(json_encode($data));
    }

    public function save_modal()
    {
        $data['mitigasi_why'] = $this->input->post('input_why');
        $data['mitigasi_solution'] = $this->input->post('input_solution');
        $data['mitigasi_date'] = date('Y-m-d h:i:s');
        $data['mitigasi_aircraft'] = $this->input->post('mitigasi_aircraft');
        $data['mitigasi_type'] = "repetitive";
        $data['mitigasi_fk_id'] = $this->input->post('mitigasi_fk_id');

        if (!empty($data['mitigasi_why']) && !empty($data['mitigasi_solution'])) {
            $mitigationAction = $this->m_repetitive_mitigation->update_mitigasi($data);            
            echo json_encode(array("sukses" => 'success', "message" => $mitigationAction));
        } else {
            echo json_encode(array("sukses" => 'success', "message" => 'No Data Changed'));            
        }


    }

}
