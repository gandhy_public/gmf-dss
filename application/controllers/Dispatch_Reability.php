<?php
ini_set('max_execution_time', 0);
defined("BASEPATH") or exit('no direct script access allowed');

/**
 *
 */
class Dispatch_Reability extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->helper("color");
        $this->load->model("M_dispatch_reability");
    }

    public function index()
    {
        $acTypeGa = $this->allActype("GA");
        $acTypeQg = $this->allActype("CITILINK");
        $data = array(
            "content" => "dispatch/index",
            "title" => "Dispatch Reliability",
            "small_tittle" => "Dashboard",
            "breadcrumb" => ["Dispatch Reliability"],
            "menu" => $this->M_menu->tampil(),
            "acTypeGa" => $acTypeGa,
            "acTypeQg" => $acTypeQg,
            'startDateMonthly' => date('Y'),
            'endDateMonthly' => date('Y'),
            'currentWeek' => date('W'),
        );
        $this->load->view("layouts", $data);
    }

    public function showAllActype()
    {
      $acType = $this->allActype();
      echo json_encode($acType);
    }

    public function allActype($manufacture)
    {
      $acType = $this->M_dispatch_reability->getAllAcType($manufacture);
      return $acType;
    }

    public function dataReg($acType = "")
    {
        $dataAcReg = $this->M_dispatch_reability->getAcRegWhere($acType);
        echo json_encode($dataAcReg);
    }

    private function convertMonth($months)
    {
        $arr = [];
        foreach ($months as $month) {
            switch ($month) {
                case 1:
                    $arr[] = "Jan";
                    break;
                case 2:
                    $arr[] = "Feb";
                    break;
                case 3:
                    $arr[] = "Mar";
                    break;
                case 4:
                    $arr[] = "Apr";
                    break;
                case 5:
                    $arr[] = "May";
                    break;
                case 6:
                    $arr[] = "Jun";
                    break;
                case 7:
                    $arr[] = "Jul";
                    break;
                case 8:
                    $arr[] = "Aug";
                    break;
                case 9:
                    $arr[] = "Sep";
                    break;
                case 10:
                    $arr[] = "Oct";
                    break;
                case 11:
                    $arr[] = "Nov";
                    break;
                case 12:
                    $arr[] = "Des";
                    break;
                default:
                    $arr[] = false;
                    break;
            }
        }
        return $arr;
    }

    public function coloring()
    {
        $background_colors = array('#009688', '#05354D', '#E6E7E8', '#F2C573', '#8EC3A7');
        $rand_background = $background_colors[array_rand($background_colors)];
        return $rand_background;
    }

    public function coloring2($no)
    {
        $arrColor = array(
            '#E91E63', '#AB47BC', '#7E57C2', '#5C6BC0',
            '#42A5F5', '#29B6F6', '#26C6DA', '#26A69A',
            '#66BB6A', '#CDDC39', '#FFC107', '#FF9800',
            '#FF5722', '#795548', '#607D8B', '#CDDC39',
            '#EF5350', '#7E57C2', '#4DD0E1', '#26A69A'
        );
        shuffle($arrColor);
        return $arrColor[$no];
    }

    public function chartDispatchMonthly($type)
    {
      $style = array(
          "fontWeight" => "contrast",
          "fontSize" => "10px"
      );
        switch ($type) {
            case 'monthly':
                $date1 = '1';
                $date2 = '12';

                if ($date1 > $date2) {
                    echo json_encode(array("success" => false));
                    exit();
                }
//                $year1 = explode("-", $this->input->post("date1"))[1];
//                $year2 = explode("-", $this->input->post("date2"))[1];
                $year1 = $this->input->post('date1');
                $year2 = $this->input->post('date2');
                $limit = $year2 - $year1;
                if ($limit > 5) {
                    echo json_encode(array("success" => false));
                    exit();
                }
                $colorRand = randomColor();
                $output = array();
                $limitYear = range(0, $limit);
                $months = range($date1, $date2);

                $series = array();
                $datanya = array();
                $noColor = 0;
                foreach ($limitYear as $data) {
                    $nowYear = intval(Date("Y"));
                    $year = $year2 - $data;
                    $data = $this->M_dispatch_reability->getDataYear($year, $months);
                    // fillColor: {
                    //     linearGradient: [0, 0, 0, 200],
                    //     stops: [
                    //         [0, Highcharts.getOptions().colors[0]],
                    //         [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    //     ]
                    // }
                    $series[] = array(
                        "type" => 'column',
                        "fillColor" => array(
                          "linearGradient" => array(0, 0, 0, 300),
                          "stops" => array(
                            array(0, $colorRand[$noColor]),
                            array(1, "#fff3"),
                          )
                        ),
                        "name" => $year,
                        "data" => $data,
                        "color" => $colorRand[$noColor],
                        "marker" => array("symbol" => "symbol"),
                        "dataLabels" => array("align" => "left", "enabled" => true, "rotation" => 270, "x" => 0, "y" => -10, "style"=>$style)
                    );
                    $noColor++;
                }

                $targetData = $this->M_dispatch_reability->dispatchTarget(date("Y"), $months);
                $target = array(
                    "name" => "target",
                    "data" => $targetData,
                    "width" => 1,
                    "color" => "#fd9214",
                    "marker" => array("symbol" => "symbol", "enabled" => false)
                );
                array_push($series, $target);
                rsort($series);

                $output = array(
                    "title" => array("text" => "Service Level Monthly"),
                    "exporting" => array("enabled" => false),
                    "credits" => array("enabled" => false),
                    "xAxis" => array(
                      "categories" => $this->convertMonth($months),
                      "labels" => array(
                        "style" => array(
                          "fontSize" => "15px"
                        )
                       )
                    ),
                    "yAxis" => array(
                      "lineWidth" => 1,
                      "min" => 50,
                      "max" => 101,
                      "title" => array(
                        "text" => "Percentage (%)"
                      ),
                      "labels" => array(
                        "style" => array(
                          "fontSize" => "15px"
                        )
                       )
                    ),
                    "series" => $series
                );
                echo json_encode($output);
                break;
            case 'weekly':
                $date1 = explode("-", $this->input->post("date1"))[0];
                $date2 = explode("-", $this->input->post("date2"))[0];

                if ($date1 > $date2) {
                    echo json_encode(array("success" => false));
                    exit();
                }
                // $weekNow = $this->M_dispatch_reability->nowWeek()->week;
                $dateNow = date("Y-m-d");
                $create = new DateTime($dateNow);
                $weekNow = $create->format("W");

                $colorRand = randomColor();
                $output = array();
                $weeks = range($date1, $date2);
                // echo "<pre>";
                // print_r($weeks);
                // echo "</pre>";
                // exit;
                $series = array();
                $datanya = array();
                $noColor = 0;
                $actype = $this->M_dispatch_reability->getAllAcTypeTwo();
                $arrX = "";
                foreach ($actype as $type) {
                    $data = $this->M_dispatch_reability->getDataWeek($type->dr_actype, $weeks, $weekNow);
                    $series[] = array(
                        "name" => $type->dr_actype,
                        "data" => $data["result"],
                        "type" => "column",
                        "color" => $this->coloring2($noColor),
                        "marker" => array("symbol" => "symbol"),
                        "dataLabels" => array("align" => "left", "enabled" => true, "rotation" => 270, "x" => 0, "y" => -10, "style"=>$style)
                    );

                    $arrX = $data["xAxist"];
                    $noColor++;
                }

                $targetData = $this->M_dispatch_reability->dispatchTargetWeekly(date("Y"), $weeks, $weekNow);

                $target = array(
                    "name" => "target",
                    "data" => $targetData,
                    "width" => 1,
                    "color" => "#fd9214",
                    "marker" => array("symbol" => "symbol", "enabled" => false)
                );
                array_push($series, $target);
                // rsort($series);

                $output = array(
                    "title" => array("text" => "Service Level Weekly"),
                    "exporting" => array("enabled" => false),
                    "credits" => array("enabled" => false),
                    "xAxis" => array(
                      "categories" => $arrX,
                      "labels" => array(
                        "style" => array(
                          "fontSize" => "15px"
                        )
                       )
                    ),
                    "yAxis" => array(
                        "lineWidth" => 1,
                        "min" => 50,
                        "max" => 101,
                        "title" => array(
                          "text" => "Percentage (%)"
                        ),
                        "labels" => array(
                          "style" => array(
                            "fontSize" => "15px"
                          )
                         )
                      ),
                    "series" => $series
                );

                echo json_encode($output);

                break;

            default:

                break;
        }
    }

    public function chartDetail()
    {
        $month = $this->input->post("month");
        $year = $this->input->post("year");
        $currentType = $this->input->post("currentType");
        $currentReg = $this->input->post("currentReg");
        $result = $this->M_dispatch_reability->getDetailWhere($month, $year, $currentType, $currentReg);
        echo json_encode($result);
    }

    public function chartDetailAgainHuhkaa()
    {
        $month = $this->input->post("month");
        $year = $this->input->post("year");
        $actype = $this->input->post("actype");
        $acreg = $this->input->post("acreg");
        $result = $this->M_dispatch_reability->getDetailAgain($month, $year, $actype, $acreg);
        echo json_encode($result);
    }

    public function chartDetailAgainDua()
    {
        $month = $this->input->post("date");
        $actype = $this->input->post("actype");
        $currentReg = $this->input->post("currentReg");
        $result = $this->M_dispatch_reability->getDetailAgainDua($month, $actype, $currentReg);
        echo json_encode($result);
    }

    public function getMitigation($id)
    {
        $result = $this->M_dispatch_reability->getMitigationWhere($id);
        echo json_encode($result);
    }
}
