<?php
defined("BASEPATH") or exit('no direct script access allowed');

/**
 *
 */
class Csi_controller extends MY_Controller
{

    public function __construct()
    {
        # code...
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_menu");
        $this->load->model('Csi_model');
    }

//==============================================================================
    public function index()
    {
        # code...
        $data = array(
            "content" => "csi/view_csi_semester",
            "title" => "CSI Data Upload",
            "small_tittle" => "",
            "breadcrumb" => ["CSI Data Upload"],
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }

//==============================================================================
    public function store_customer()
    {
        # code...
        $today = date("Y-m-d H:i:s");
        $name = $this->input->post('input_customer', true);
        $id = $this->input->post('id_c', true);
        if ($id != '') {
            # code...
            if ($this->Csi_model->update_customer($name, $id, $today)) {
                $data = array(
                    "notif" => 'success',
                    "msg" => 'Update nama Customer Sukses'
                );
            } else {
                # code...
                $data = array(
                    "notif" => 'error',
                    "msg" => 'Gagal Update nama Customer Sukses'
                );
            }

        } else {
            if ($name == '') {
                # code...
                $data = array(
                    "notif" => 'warning',
                    "msg" => 'Field Kosong'
                );
            } else {
                $sql = $this->Csi_model->add_customer($name, $today);
                if ($sql) {
                    # code...
                    $data = array(
                        "notif" => 'success',
                        "msg" => 'Sukses Input Data'
                    );
                } else {
                    $data = array(
                        "notif" => 'error',
                        "msg" => 'Failed Input Data'
                    );
                }
            }
        }
        echo json_encode($data);
    }

//==============================================================================
    public function data_customer()
    {
        # code...
        $columns = array(
            0 => 'id_customer'
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search,
            'order_column' => $order_column,
            'order_dir' => $order_dir
        );
        $print = array();
        $total = $this->db->count_all_results('csi_customer');
        $print['draw'] = $data['draw'];
        $print['recordsTotal'] = $print['recordsFiltered'] = $total;
        $print['data'] = array();


        $sql = $this->Csi_model->get_customer($data);
        $no = $data['start'] + 1;
        foreach ($sql as $key) {
            # code...
            $print['data'][] = array(
                $no++,
                $key['name_customer'],
                "<button type='button' data-id='" . $key['id_customer'] . "' data-nama='" . $key['name_customer'] . "' class='btn btn-primary btn-xs' name=edit_customer id=edit_customer ><i class='fa fa-edit' ></i></button>
            <button type='button' data-id='" . $key['id_customer'] . "' class='btn btn-danger btn-xs' id=del_customer name=del_customer><i class='fa fa-trash-o'></i></button>"
            );
        }
        echo json_encode($print);
    }

//==============================================================================
    public function deletecustomer($id)
    {
        # code...
        if ($this->Csi_model->del_customer($id)) {
            # code...
            $data_ret = array(
                "data" => 'Sukses',
                "msg" => 'Data Deleted'
            );
        } else {
            $data_ret = array(
                "data" => 'Gagal',
                "msg" => 'Failed to delete'
            );
        }
        echo json_encode($data_ret);
    }

//==============================================================================
    public function delete($id)
    {
        # code...
        if ($this->Csi_model->rollback_func($id)) {
            $data_ret = array(
                "data" => 'Sukses',
                "msg" => 'Data Deleted'
            );
        } else {
            $data_ret = array(
                "data" => 'Gagal',
                "msg" => 'Failed to delete'
            );
        }
        echo json_encode($data_ret);
    }

//==============================================================================
    public function view($id_act)
    {
        # code...

        $data_customer = $this->Csi_model->get_name_customer($id_act);
        foreach ($data_customer as $key1) {
            // code...
            $hasil = array();
            array_push($hasil, $key1['name_customer']);
            $data2 = $this->Csi_model->get_org($id_act, $key1['id_customer']);
            foreach ($data2 as $key) {
                // code...
                array_push($hasil, $key['com_csi'], $key['bqs_csi'], $key['otd_csi'], $key['pca_csi'], $key['ss_csi'], $key['oss_csi'], $key['fm_csi'], $key['qa_csi'], $key['total_score']);
            }
            $hasil = array_filter($hasil);
            // $temp[]=array($hasil);
            $temp[] = array(implode('","', $hasil));
        }
        // $arrayName[] = array("1","2","3","4","5");
        $arrayName[] = array(1, 2, 3, 4, 5);
        $arrayName[] = array(1, 2, 3, 4, 5);
        $data = array('data' => $arrayName);
        // echo count($data[10])."<br>";
        echo json_encode($arrayName);
    }

//==============================================================================

    public function datatable()
    {
        # code...

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        // $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search
            // 'order_column' => $order_column
        );
        $total = $this->Csi_model->count_in_datatable($data);
        $sqld = $this->Csi_model->show_in_datatable($data);
        $no = $data['start'] + 1;
        if (count($sqld) != 0) {
            foreach ($sqld as $key) {
                # code...
                $print['data'][] = array(
                    $no++,
                    $key['create_date'],
                    // $key['type'],
                    $key['date_type'],
                    // "<button type='button' data-id='".$key['id_history']."' class='btn btn-success btn-flat' id=view name=view><i class='fa fa-eye'></i></button>  &nbsp
                    "<button type='button' data-id='" . $key['id_history'] . "' class='btn btn-danger btn-flat' id=del name=del><i class='fa fa-trash-o'></i></button>"
                );
            }
        } else {
            $print['data'] = array();
        }
        $print['draw'] = $data['draw'];
        $print['recordsTotal'] = $print['recordsFiltered'] = $total;
        echo json_encode($print);
    }

//==============================================================================

    public function FunctionName()
    {
        # code...
        $q = '2010-01';
        $id_act = $this->Csi_model->cek_date($q);
        echo $id_act;
    }

//==============================================================================
    public function storefiles()
    {
        # code...
        $this->load->library('PHPExcel');
        // Destintion Folder tujuan
        $destination_folder = 'assets/upload';

        $tempNamaCustomer = array();

        // Data From POST
        $periode = $this->input->post('periode', true);
        if ($periode == 'monthly') {
            # code...
            $n_isi = array('5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5');
            $for_date = $this->input->post('mounth', true);
            if ($for_date == '') {
                # code...
                $data_ret[] = array(
                    "data" => 'warning',
                    "msg" => 'Pastikan Kolom Terisi Semua'
                );
                echo json_encode($data_ret);
                exit();
            }
            if ($this->Csi_model->cek_date($for_date, $periode) != 0) {
                $data_ret[] = array(
                    "data" => 'warning',
                    "msg" => 'Tanggal Sudah Dipakai'
                );
                echo json_encode($data_ret);
                exit();
            }
        } else {
            # code...
            $n_isi = array('5', '5', '5', '6', '6', '5', '5', '5', '5', '6', '5', '5', '6', '6', '4', '4', '4', '4', '5');
            $for_date = $this->input->post('year', true) . '-' . $this->input->post('semester', true);
            if ($this->input->post('year', true) == '' || $this->input->post('semester', true) == '') {
                # code...
                $data_ret[] = array(
                    "data" => 'warning',
                    "msg" => 'Pastikan Kolom Terisi Semua'
                );
                echo json_encode($data_ret);
                exit();
            }
            if ($this->Csi_model->cek_date($for_date, $periode) != 0) {
                $data_ret[] = array(
                    "data" => 'warning',
                    "msg" => 'Tanggal Sudah Dipakai'
                );
                echo json_encode($data_ret);
                exit();
            }
        }

        // Change name files
        $new_name_file = date("Ymd_His") . '_' . $_FILES['files']['name'];
        $file = "$destination_folder/$new_name_file";

        $today = date("Y-m-d H:i:s");

        // insert Data base
        move_uploaded_file($_FILES['files']['tmp_name'], $file);
        $id_act = $this->Csi_model->cek_return_files_upload(2, $file, $periode, $for_date, $today);
        // exit();

        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        //get only the Cell Collection
        // $cell_collection = $objPHPExcel->getActiveSheet(0)->getCellCollection();
        $cell_collection = $objPHPExcel->setActiveSheetIndex(0)->getCellCollection();

        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            //The header will/should be in row 1 only. of course, this can be modified to suit your need
            $arr_data[$row][$column] = $data_value;
        }

        //send the data in an array format
        $data['values'] = $arr_data;

        unlink($file);
        $loop = 1;
        while ($data['values'][$loop][$this->getNameFromNumber(2)] != 'Total (All Customer)') {
            # code...
            $loop++;
        }
        // echo $loop;
        // echo print_r($data);
        // echo count($data['values'][4]);
        // exit();

        $level_org = ''; // level Org Key Custumer = 1, High= 2, general custumer=3

        if ($periode == 'semesterly') {
            # code...
            // if (count($data['values'][4])!=124) {
            //   # code...
            //   if($this->Csi_model->rollback_func($id_act)){
            //     $data_ret[]=array(
            //         "data" => 'warning',
            //         "msg" => 'Template Yang Diupload Bukan Template Semesterly'
            //     );
            //     echo json_encode($data_ret);
            //     exit();
            //   }
            // }
            $notifsemesterly = 0;
            for ($i = 4; $i < $loop; $i++) { //count($data['values'])-62
                # code...
                // Cek Header Key Customer , high val, general custumer
                if ($data['values'][$i][$this->getNameFromNumber(2)] == "Key Customer") {
                    # code...
                    $tempKey = $i;
                    $level_org = 1;
                } elseif ($data['values'][$i][$this->getNameFromNumber(2)] == "High Value Brand Customer") {
                    # code...
                    $tempHigh = $i;
                    $level_org = 2;
                } elseif ($data['values'][$i][$this->getNameFromNumber(2)] == "General Customer") {
                    # code...
                    $tempGen = $i;
                    $level_org = 3;
                } elseif ($i != $tempKey || $i != $tempHigh || $i != $tempGen) {
                    # code...
                    // Cek Data base Custumer
                    $id_customer = $this->Csi_model->cek_availabe_customer($data['values'][$i][$this->getNameFromNumber(2)]);
                    // print_r($id_customer);
                    array_push($tempNamaCustomer, $data['values'][$i][$this->getNameFromNumber(2)]);
                    if (!empty($id_customer->id_customer)) {
                        $sql2 = $this->define_org_type($i, $n_isi, $data, $id_act, $level_org, $id_customer->id_customer, $today);
                        $notifsemesterly = $notifsemesterly + 1;
                    }
                }

    }
    // print_r($tempNamaCustomer);
    // echo "string".$notifsemesterly."Storong".($loop-8);
    // exit();
    if ($notifsemesterly==($loop-7) && $id_act!='') {
      # code....
      $data_ret[]=array(
          "data" => 'Sukses',
          "msg" => 'Sukses Input Semesterly  '
      );
    }else{
      if($this->Csi_model->rollback_func($id_act)){
      $data_ret[]=array(
          "data" => 'Gagal',
          "msg" => 'Gagal Input SemesterLy / Cek Nama Customer Anda '
      );
    }
    }

        } else {
            if (count($data['values'][4]) != 74) {
                # code...
                if ($this->Csi_model->rollback_func($id_act)) {
                    $data_ret[] = array(
                        "data" => 'warning',
                        "msg" => 'Template Yang Diupload Bukan Template mountly'
                    );
                    echo json_encode($data_ret);
                    exit();
                }
            }
            $notifmonthly = 0;
            for ($i = 4; $i < $loop; $i++) {
                # code...
                // Cek Header Key Customer , high val, general custumer
                if ($data['values'][$i][$this->getNameFromNumber(2)] == "Key Customer") {
                    # code...
                    $tempKey = $i;
                    $level_org = 1;
                } elseif ($data['values'][$i][$this->getNameFromNumber(2)] == "High Value Brand Customer") {
                    # code...
                    $tempHigh = $i;
                    $level_org = 2;
                } elseif ($data['values'][$i][$this->getNameFromNumber(2)] == "General Customer") {
                    # code...
                    $tempGen = $i;
                    $level_org = 3;
                } elseif ($i != $tempKey || $i != $tempHigh || $i != $tempGen) {
                    # code...
                    // Cek Data base Custumer
                    $id_customer = $this->Csi_model->cek_availabe_customer($data['values'][$i][$this->getNameFromNumber(2)]);
                    // print_r($id_customer);
                    if (!empty($id_customer->id_customer)) {
                        $sql2 = $this->def_org_mountly($i, $n_isi, $data, $id_act, $level_org, $id_customer->id_customer, $today);
                        $notifmonthly = $notifmonthly + $sql2;
                    }
                }

            }

            // print_r($tempNamaCustomer);
            // echo "string".$notifmonthly."Storong".($loop-7);
            // exit();
            if ($notifmonthly == ($loop - 7) && $id_act != '') {
                # code...
                $data_ret[] = array(
                    "data" => 'Sukses',
                    "msg" => 'Sukses Input Monthly'
                );

            } else {
                //
                // Roll Back
                if ($this->Csi_model->rollback_func($id_act)) {
                    $data_ret[] = array(
                        "data" => 'Gagal',
                        "msg" => 'Gagal Input Monthly / Cek Nama Customer '
                    );
                }
            }
        }
        echo json_encode($data_ret);
    }

    function getNameFromNumber($c)
    {
        $c = intval($c);
        if ($c <= 0) return '';
        $letter = '';
        while ($c != 0) {
            $p = ($c - 1) % 26;
            $c = intval(($c - $p) / 26);
            $letter = chr(65 + $p) . $letter;
        }
        return $letter;
    }


function define_org_type($i,$n_isi,$data,$file,$level_org,$id_customer,$today)
{
  # code...
  // inisialisasi Array Untuk menampung data per rows dari
  $sql_5_1=array(); // digunakan untuk 5
  $sql_5=array(); // digunakan untuk 5
  $sql_6_1=array(); // digunakan untuk menampung
  $sql_6_2=array(); // digunakan untuk menampung
  $sql_4_1=array(); // digunakan untuk menampung
  $sql_4_2=array(); // digunakan untuk menampung
  $bus=0;$sus=0;
  $temp_arr=array(); // digunakan untuk menampung data dari tiap organisasi seblum masuk pada sql tujuan
  // if ($i==5) {
    # code...
    // ====== Notif =======
    // loop digunakan untuk identifikasi Mulai kolom Excel 'C'
    $loop=3;
    for ($j=0; $j < count($n_isi) ; $j++) {
      # code...
      $total=0;
        if ($n_isi[$j]==5) {
          # code...
          $col1=$this->getNameFromNumber($loop);
          $col2=$this->getNameFromNumber($loop+1);
          $col3=$this->getNameFromNumber($loop+2);
          $col4=$this->getNameFromNumber($loop+3);
          $col5=$this->getNameFromNumber($loop+4);
          $total=($data['values'][$i][$col1]+$data['values'][$i][$col2]+$data['values'][$i][$col3]+$data['values'][$i][$col4]+$data['values'][$i][$col5])/5;
            if ($j==18) {
              # code...
              $temp_arr = array(
                        'id_customer'=> $id_customer,
                        'id_type_org_csi'=>($j+1),
                        'com_csi' =>$data['values'][$i][$col1],
                        'bqs_csi' =>$data['values'][$i][$col2],
                        'pca_csi' =>$data['values'][$i][$col3],
                        'fm_csi' =>$data['values'][$i][$col4],
                        'oss_csi' =>$data['values'][$i][$col5],
                        'id_history_usr_action'=>$file,
                        'level_customer'=> $level_org,
                        'total_score'=> $total,
                        'create_date'=> $today,
                        'update_date'=> $today
                      );
              array_push($sql_5_1,$temp_arr);
            }else{
              $temp_arr = array(
                        'id_customer'=> $id_customer,
                        'id_type_org_csi'=>($j+1),
                        'com_csi' =>$data['values'][$i][$col1],
                        'bqs_csi' =>$data['values'][$i][$col2],
                        'otd_csi' =>$data['values'][$i][$col3],
                        'pca_csi' =>$data['values'][$i][$col4],
                        'ss_csi' =>$data['values'][$i][$col5],
                        'id_history_usr_action'=>$file,
                        'level_customer'=> $level_org,
                        'total_score'=> $total,
                        'create_date'=> $today,
                        'update_date'=> $today
                      );
              array_push($sql_5,$temp_arr);
            }

            $loop=$loop+6;
        }elseif ($n_isi[$j]==6) {
          # code...
          $col1=$this->getNameFromNumber($loop);
          $col2=$this->getNameFromNumber($loop+1);
          $col3=$this->getNameFromNumber($loop+2);
          $col4=$this->getNameFromNumber($loop+3);
          $col5=$this->getNameFromNumber($loop+4);
          $col6=$this->getNameFromNumber($loop+5);
          $total=($data['values'][$i][$col1]+$data['values'][$i][$col2]+$data['values'][$i][$col3]+$data['values'][$i][$col4]+$data['values'][$i][$col5]+$data['values'][$i][$col6])/6;
          if ($j==9||$j==11) {
            # code...
            $temp_arr = array(
                        'id_customer'=> $id_customer,
                        'id_type_org_csi'=>($j+1),
                        'com_csi' =>$data['values'][$i][$col1],
                        'bqs_csi' =>$data['values'][$i][$col2],
                        'otd_csi' =>$data['values'][$i][$col3],
                        'pca_csi' =>$data['values'][$i][$col4],
                        'ss_csi' =>$data['values'][$i][$col5],
                        'fm_csi' =>$data['values'][$i][$col6],
                        'id_history_usr_action'=>$file,
                        'level_customer'=> $level_org,
                        'total_score'=> $total,
                        'create_date'=> $today,
                        'update_date'=> $today
                      );
            array_push($sql_6_1,$temp_arr);
          }else{
            $temp_arr = array(
                        'id_customer'=> $id_customer,
                        'id_type_org_csi'=>($j+1),
                        'com_csi' =>$data['values'][$i][$col1],
                        'bqs_csi' =>$data['values'][$i][$col2],
                        'otd_csi' =>$data['values'][$i][$col3],
                        'pca_csi' =>$data['values'][$i][$col4],
                        'ss_csi' =>$data['values'][$i][$col5],
                        'oss_csi' =>$data['values'][$i][$col6],
                        'id_history_usr_action'=>$file,
                        'level_customer'=> $level_org,
                        'total_score'=> $total,
                        'create_date'=> $today,
                        'update_date'=> $today
                      );
            array_push($sql_6_2,$temp_arr);
          }
          $loop=$loop+7;
        }elseif($n_isi[$j]==4){
          # code...
          $col1=$this->getNameFromNumber($loop);
          $col2=$this->getNameFromNumber($loop+1);
          $col3=$this->getNameFromNumber($loop+2);
          $col4=$this->getNameFromNumber($loop+3);
          $total=($data['values'][$i][$col1]+$data['values'][$i][$col2]+$data['values'][$i][$col3]+$data['values'][$i][$col4])/4;

                if ($j == 15) {
                    # code...
                    $temp_arr = array(
                        'id_customer' => $id_customer,
                        'id_type_org_csi' => ($j + 1),
                        'com_csi' => $data['values'][$i][$col1],
                        'bqs_csi' => $data['values'][$i][$col2],
                        'pca_csi' => $data['values'][$i][$col3],
                        'fm_csi' => $data['values'][$i][$col4],
                        'id_history_usr_action' => $file,
                        'level_customer' => $level_org,
                        'total_score' => $total,
                        'create_date' => $today,
                        'update_date' => $today
                    );
                    array_push($sql_4_1, $temp_arr);
                } else {
                    $temp_arr = array(
                        'id_customer' => $id_customer,
                        'id_type_org_csi' => ($j + 1),
                        'com_csi' => $data['values'][$i][$col1],
                        'bqs_csi' => $data['values'][$i][$col2],
                        'pca_csi' => $data['values'][$i][$col3],
                        'oss_csi' => $data['values'][$i][$col4],
                        'id_history_usr_action' => $file,
                        'level_customer' => $level_org,
                        'total_score' => $total,
                        'create_date' => $today,
                        'update_date' => $today
                    );
                    array_push($sql_4_2, $temp_arr);
                }
                $loop = $loop + 5;
            }
            if ($j < 12) {
                # code...
                $bus = $bus + $total;
            } else {
                $sus = $sus + $total;
            }

        }
        $abus = ($bus / 12);
        $asus = ($sus / 7);
        $buw = ($abus * 3.5) / 5;
        $suw = ($asus * 1.5) / 5;
        $final = $buw + $suw;

        $data = array(
            'id_customer' => $id_customer,
            'id_history_action' => $file,
            'avg_bus' => $abus,
            'avg_sus' => $asus,
            'busw' => $buw,
            'susw' => $suw,
            'finalscore' => $final
        );

        $sql_final_score = $this->Csi_model->insert_final_score($data);
        // echo $sql_final_score;

        $sql = $this->Csi_model->insert_data_org_item($sql_5_1, $sql_5, $sql_4_1, $sql_4_2, $sql_6_1, $sql_6_2);
        return $sql;

    }


    function def_org_mountly($i, $n_isi, $data, $file, $level_org, $id_customer, $today)
    {
        # code...
        $sql_5 = array();
        $bus = 0;
        $sus = 0;
        // ====== Notif =======
        // loop digunakan untuk identifikasi Mulai kolom Excel 'C'
        $loop = 3;
        for ($j = 0; $j < count($n_isi); $j++) {
            # code...
            $total = 0;
            $col1 = $this->getNameFromNumber($loop);
            $col2 = $this->getNameFromNumber($loop + 1);
            $col3 = $this->getNameFromNumber($loop + 2);
            $col4 = $this->getNameFromNumber($loop + 3);
            $col5 = $this->getNameFromNumber($loop + 4);

            $total = ($data['values'][$i][$col1] + $data['values'][$i][$col2] + $data['values'][$i][$col3] + $data['values'][$i][$col4] + $data['values'][$i][$col5]) / 5;
            $temp_arr = array(
                'id_customer' => $id_customer,
                'id_type_org_csi' => $j,
                'com_csi' => $data['values'][$i][$col1],
                'bqs_csi' => $data['values'][$i][$col2],
                'otd_csi' => $data['values'][$i][$col3],
                'pca_csi' => $data['values'][$i][$col4],
                'qa_csi' => $data['values'][$i][$col5],
                'id_history_usr_action' => $file,
                'level_customer' => $level_org,
                'total_score' => $total,
                'create_date' => $today,
                'update_date' => $today
            );
            array_push($sql_5, $temp_arr);
            $loop = $loop + 6;
            $bus = $bus + $total;
        }
        $abus = ($bus / 12);
        $buw = ($abus * 3.5) / 5;
        $final = $abus;
        $data = array(
            'id_customer' => $id_customer,
            'id_history_action' => $file,
            'avg_bus' => $abus,
            'busw' => $buw,
            'finalscore' => $final
        );
        $sql_final_score = $this->Csi_model->insert_final_score($data);
        $sql = $this->Csi_model->insert_data_org_mountly($sql_5);
        return $sql;
    }

}
