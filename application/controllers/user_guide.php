<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class user_guide extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $data = array(
            "content" => "user-guide/index",
            "title" => "User Guide",
            "title_box" => "User Guide DSS Customer Perspective",
            "small_tittle" => "",
            "breadcrumb" => ["User Guide"],
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }

}
