<?php
defined('BASEPATH') or exit('No Direct script access allowed');
ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');

/**
 *
 */
class C_kpi_corporate extends MY_Controller
{

    function __construct()
    {
        // code...
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->helper("date");
        $this->load->model("M_menu");
        $this->load->helper("hierarchyitem_helper");
        $this->load->helper("kpiformula");
        $this->load->model('M_kpi_corporate');
        $this->load->model('M_kpi_sla_master');
        $this->load->model("M_slabaseops");
    }

    public function calculate()
    {
        $grups = $this->M_kpi_corporate->get_grup();        
        $out = array();
        $finalAllTotal=$this->countTotal();
        foreach ($grups as $grup) {
          $datak=$this->countOnBoard($grup->kpi_group_id);
        //   $maxLevel_ga = $this->M_kpi_sla_master->maxLevel($datak->ki_id);
        //   $finalTotalScores_ga=$this->FunctionName($maxLevel_ga,$datak->ki_id,$i,$year);
        // //   echo json_encode($datak);exit;
        //   $dataMentah = $this->M_kpi_corporate->get(date("Y"), date("m"), $grup->kpi_group_id, "grup");
        //   $dataMentahAll = $this->M_kpi_corporate->get(date("Y"), date("m"), $grup->kpi_group_id);
        //   //   $maxLevel = $this->M_kpi_sla_master->maxLevel($grup->ki_id);
        //   //   $finalTotalScores_ga=$this->FunctionName($maxLevel,$grup->ki_id,date("m"),date("Y"));
          
        //   $content = $this->generatePageTreeTableActual($dataMentah, date("m"), date("Y"));
        //   $totalScore = $this->get_all_total_final($dataMentahAll, date("m"));
          
          $out[] = array(
            "group" => (array("name" => $grup->kpi_group_name, "data" => $datak)),
          );
        //   echo json_encode($content);exit;
        }
        // $out["totalScore"] = $totalScore;
        echo json_encode(array("totalScore" => $finalAllTotal, "data" => $out));
    }

    // public function trendChart($id)
    // {
    //   $months = range(01, date("m"));
    //   foreach ($months as $month) {
    //     $dataMentah = $this->M_kpi_corporate->get(date("Y"), $month, $id, "single");
    //     $targetPecah = explode("|", $this->getChart($dataMentah, $month, date("Y"), "target"));
    //     if(count($targetPecah) > 1){
    //       $upper[] = floatval($targetPecah[0]);
    //       $lower[] = floatval($targetPecah[1]);
    //       $target = array(
    //         "isStabilize" => true,
    //         "upper_target" => $upper,
    //         "lower_target" => $lower
    //       );
    //     } else {
    //       $onlyTarget[] = floatval($targetPecah[0]);
    //       $target = array(
    //         "isStabilize" => false,
    //         "target" => $onlyTarget
    //       );
    //     }
    //     $actual[] = $this->getChart($dataMentah, $month, date("Y"), "actual");
    //     $actualYtd[] = $this->getChart($dataMentah, $month, date("Y"), "actualYtd");
    //   }
    //   return array("actual" => $actual, "actualYtd" => $actualYtd, "target" => $target);
    // }

    public function lastYear($maxLevel, $idnya)
    {
      $lastYear = date("Y", strtotime(date("Y-m-d")." -1 year"));
      $date=date('m');
      $result=array();
    //   $dataMentah = $this->M_kpi_corporate->get($lastYear, date("m"), $id, "name");
      $kpiinstok=$this->M_kpi_corporate->cek($idnya,$lastYear);
      
      if(count($kpiinstok)!=0){
          $finalTotalScores=$this->FunctionName($maxLevel,$kpiinstok[0]->ki_id,$date,$lastYear);
          $result['lastmonth']=$finalTotalScores['score'];
          $result['lastyear']=$finalTotalScores['scoreYtd'];

      }else{
          $result['lastmonth']=0;
          $result['lastyear']=0;
      }
      

    //   return $this->getActualLastYear($dataMentah, date("m"), $lastYear, $type);
    return $result;
    }

    // public function getActualLastYear($datas, $month, $nowYear, $type, $depth = 1, $parent = 0)
    // {
    //     $month = $month;
    //     $nowYear = $nowYear;

    //     if ($depth > 1000) return '';
    //     $deepChild = 1;
    //     $tree = '';
    //     $array = array();
    //     $corporate = array();
    //     $target = array();
    //     $oke = '';
    //     $loop = 0;
    //     $out = 0;
    //     for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
    //         if ($datas[$i]->ki_parent_id == $parent) {
    //             $target = explode("|", $datas[$i]->kid_target);
    //             $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
    //             if ($this->M_kpi_sla_master->getDeepChild($datas[$i]->ki_id) == 0) {
    //                 if ($datas[$i]->ki_parent_id == 0) {
    //                     if (in_array(array("name" => $datas[$i]->ki_name), $corporate)) {
    //                     } else {
    //                         if ($depth == "1") {
    //                             $weight1 = $this->M_kpi_sla_master->countWeight($datas[$i]->ki_id, $nowYear, $month);
    //                         } else {
    //                             $weight1 = ($datas[$i]->ki_weight == null || empty($datas[$i]->ki_weight)) ? 0 : number_format($datas[$i]->ki_weight, 2);
    //                         }
    //                         switch ($type) {
    //                           case 'monthly':
    //                             $out = floatval(number_format(score(achievment($datas[$i]->kid_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kid_limit, $datas[$i]->kid_weight) * $weight1 / 100, 2));
    //                             break;
    //                           case 'year_to_date':
    //                             $out = floatval(number_format(score(achievment($datas[$i]->kid_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kid_limit, $datas[$i]->kid_weight) * $weight1 / 100, 2));
    //                             break;
    //                           default:
    //                             $out = false;
    //                             break;
    //                         }
    //                     }
    //                 }
    //             } else {
    //                 $maxLevel = $this->M_kpi_sla_master->maxLevel($datas[$i]->ki_id);
    //                 if ($depth == "1") {
    //                     $weight = $this->M_kpi_sla_master->countWeight($datas[$i]->ki_id, $nowYear, $month);
    //                 } else {
    //                     $weight = ($datas[$i]->ki_weight == null || empty($datas[$i]->ki_weight)) ? 0 : number_format($datas[$i]->ki_weight, 2);
    //                 }
    //                 if ($maxLevel != "1" && $depth != "1") {
    //                     $data_temp_id = $this->M_kpi_sla_master->getTotalScoreNotLevelOneWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_sla_master->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScore += number_format(($newTotal * $temp_id["weight"]) / 100, 2);
    //                         $newTotalYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                     }
    //                 } elseif ($depth == "1" && $maxLevel != "1") {
    //                     $data_temp_id = $this->M_kpi_sla_master->getTotalScoreNotLevelOneWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScoreYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_sla_master->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScore += number_format((($newTotal * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                         $newTotalYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                     }
    //                 } else {
    //                     $totalScore = $this->M_kpi_sla_master->getTotalScoreWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScoreYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                 }
    //                 $parentScore = number_format(($totalScore * $weight) / 100, 2);
    //                 $parentScoreYtd = number_format(($totalScoreYtd * $weight) / 100, 2);
    //                 if ($depth == 1) {
    //                   switch ($type) {
    //                     case 'monthly':
    //                       $out = floatval($parentScore);
    //                       break;
    //                     case 'year_to_date':
    //                       $out = floatval($parentScoreYtd);
    //                       break;
    //                     default:
    //                       $out = false;
    //                       break;
    //                   }
    //                 }
    //             }
    //             // Recursive Function
    //             $this->generatePageTreeTableActual($datas, $month, $nowYear, $type, $depth + 1, $datas[$i]->ki_id);
    //         }
    //     }
    //     return $out;
    // }

    // public function getChart($datas, $month, $nowYear, $type, $depth = 1, $parent = 0)
    // {
    //     $month = $month;
    //     $nowYear = $nowYear;

    //     if ($depth > 1000) return '';
    //     $deepChild = 1;
    //     $tree = '';
    //     $array = array();
    //     $corporate = array();
    //     $target = array();
    //     $oke = '';
    //     $loop = 0;
    //     $out = array();
    //     for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
    //         if ($datas[$i]->ki_parent_id == $parent) {
    //             $target = explode("|", $datas[$i]->kid_target);
    //             $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
    //             if ($this->M_kpi_sla_master->getDeepChild($datas[$i]->ki_id) == 0) {
    //                 if ($datas[$i]->ki_parent_id == 0) {
    //                     if (in_array(array("name" => $datas[$i]->ki_name), $corporate)) {
    //                     } else {
    //                         if ($depth == "1") {
    //                             $weight1 = $this->M_kpi_sla_master->countWeight($datas[$i]->ki_id, $nowYear, $month);
    //                         } else {
    //                             $weight1 = ($datas[$i]->ki_weight == null || empty($datas[$i]->ki_weight)) ? 0 : number_format($datas[$i]->ki_weight, 2);
    //                         }
    //                         switch ($type) {
    //                           case 'actual':
    //                             $out = floatval(number_format(score(achievment($datas[$i]->kid_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kid_limit, $datas[$i]->kid_weight) * $weight1 / 100, 2));
    //                             break;
    //                           case 'actualYtd':
    //                             $out = floatval(number_format(score(achievment($datas[$i]->kid_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kid_limit, $datas[$i]->kid_weight) * $weight1 / 100, 2));
    //                             break;
    //                           case 'target':
    //                             $out =  (empty($datas[$i]->kid_target) ? $datas[$i]->ki_target : $datas[$i]->kid_target);
    //                             break;
    //                           default:
    //                             $out = false;
    //                             break;
    //                         }
    //                     }
    //                 }
    //             } else {
    //                 $maxLevel = $this->M_kpi_sla_master->maxLevel($datas[$i]->ki_id);
    //                 if ($depth == "1") {
    //                     $weight = $this->M_kpi_sla_master->countWeight($datas[$i]->ki_id, $nowYear, $month);
    //                 } else {
    //                     $weight = ($datas[$i]->ki_weight == null || empty($datas[$i]->ki_weight)) ? 0 : number_format($datas[$i]->ki_weight, 2);
    //                 }
    //                 if ($maxLevel != "1" && $depth != "1") {
    //                     $data_temp_id = $this->M_kpi_sla_master->getTotalScoreNotLevelOneWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_sla_master->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScore += number_format(($newTotal * $temp_id["weight"]) / 100, 2);
    //                         $newTotalYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                     }
    //                 } elseif ($depth == "1" && $maxLevel != "1") {
    //                     $data_temp_id = $this->M_kpi_sla_master->getTotalScoreNotLevelOneWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScoreYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_sla_master->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScore += number_format((($newTotal * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                         $newTotalYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                     }
    //                 } else {
    //                     $totalScore = $this->M_kpi_sla_master->getTotalScoreWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScoreYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                 }
    //                 $parentScore = number_format(($totalScore * $weight) / 100, 2);
    //                 $parentScoreYtd = number_format(($totalScoreYtd * $weight) / 100, 2);
    //                 if ($depth == 1) {
    //                   switch ($type) {
    //                     case 'actual':
    //                       $out = floatval($parentScore);
    //                       break;
    //                     case 'actualYtd':
    //                       $out = floatval($parentScoreYtd);
    //                       break;
    //                     case 'target':
    //                       $out = $datas[$i]->ki_target;
    //                       break;
    //                     default:
    //                       $out = false;
    //                       break;
    //                   }
    //                 }
    //             }
    //             // Recursive Function
    //             $this->generatePageTreeTableActual($datas, $month, $nowYear, $type, $depth + 1, $datas[$i]->ki_id);
    //         }
    //     }

    //     // Create Return Value
    //     $data = array('name' => $corporate, 'month' => $array, 'target' => $target);
    //     return $out;
    // }

    // function get_all_total_final($datas, $month, $finalTotalScore=0, $depth = 1, $parent = 0)
    // {
    //     // $month = $this->input->get("month");
    //     $nowYear = date("Y");

    //     if ($depth > 1000) return '';
    //     $deepChild = 1;
    //     $globalFinalScore = $finalTotalScore;
    //     for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
    //         if ($datas[$i]->ki_parent_id == $parent) {
    //             $target = explode("|", $datas[$i]->kid_target);
    //             $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
    //             $actualYtd = (($datas[$i]->kiad_actual_ytd == null || empty($datas[$i]->kiad_actual_ytd)) ? "-" : $datas[$i]->kiad_actual_ytd);
    //             if ($this->M_kpi_sla_master->getDeepChild($datas[$i]->ki_id) == 0) {
    //               $globalFinalScore += number_format(score(achievment($datas[$i]->kid_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kid_limit, $datas[$i]->kid_weight), 2);
    //             } else {
    //                 $maxLevel = $this->M_kpi_sla_master->maxLevel($datas[$i]->ki_id);
    //                 if ($depth == "1") {
    //                     $weight = $this->M_kpi_sla_master->countWeight($datas[$i]->ki_id, $nowYear, $month);
    //                 } else {
    //                     $weight = ($datas[$i]->ki_weight == null || empty($datas[$i]->ki_weight)) ? 0 : number_format($datas[$i]->ki_weight, 2);
    //                 }
    //                 if ($maxLevel != "1" && $depth != "1") {
    //                     $data_temp_id = $this->M_kpi_sla_master->getTotalScoreNotLevelOneWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_sla_master->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $newTotalYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScore += number_format(($newTotal * $temp_id["weight"]) / 100, 2);
    //                         $totalScoreYtd += number_format(($newTotalYtd * $temp_id["weight"]) / 100, 2);
    //                     }
    //                 } elseif ($depth == "1" && $maxLevel != "1") {
    //                     $data_temp_id = $this->M_kpi_sla_master->getTotalScoreNotLevelOneWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScoreYtd = 0;
    //                     $totalScore = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_sla_master->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $newTotalYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScore += number_format((($newTotal * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                     }
    //                 } else {
    //                     $totalScore = $this->M_kpi_sla_master->getTotalScoreWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScoreYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                 }
    //                 $parentScore = number_format(($totalScore * $weight) / 100, 2);
    //                 $parentScoreYtd = number_format(($totalScoreYtd * $weight) / 100, 2);
    //                 $globalFinalScore += $totalScore;
    //             }
    //             $this->get_all_total_final($datas, $month, $globalFinalScore, $depth + 1, $datas[$i]->ki_id);
    //         }
    //     }

    //     return $globalFinalScore;
    // }

    // public function generatePageTreeTableActual($datas, $month, $nowYear, $depth = 1, $parent = 0)
    // {
    //     $month = $month;
    //     $nowYear = $nowYear;

    //     if ($depth > 1000) return '';
    //     $deepChild = 1;
    //     $tree = '';
    //     $array = array();
    //     $corporate = array();
    //     $target = array();
    //     $oke = '';
    //     $loop = 0;
    //     for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
    //         if ($datas[$i]->ki_parent_id == $parent) {
    //             $target = explode("|", $datas[$i]->kid_target);
    //             $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
    //             if ($this->M_kpi_sla_master->getDeepChild($datas[$i]->ki_id) == 0) {
    //                 if ($datas[$i]->ki_parent_id == 0) {
    //                     if (in_array(array("name" => $datas[$i]->ki_name), $corporate)) {
    //                     } else {
    //                         if ($depth == "1") {
    //                             $weight1 = $this->M_kpi_sla_master->countWeight($datas[$i]->ki_id, $nowYear, $month);
    //                         } else {
    //                             $weight1 = ($datas[$i]->ki_weight == null || empty($datas[$i]->ki_weight)) ? 0 : number_format($datas[$i]->ki_weight, 2);
    //                         }
    //                         array_push($corporate, array(
    //                           "name" => $datas[$i]->ki_name,
    //                           "monthly" => array(
    //                             "actual" => number_format(score(achievment($datas[$i]->kid_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kid_limit, $datas[$i]->kid_weight) * $weight1 / 100, 2),
    //                             "achievment" => number_format(score(achievment($datas[$i]->kid_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kid_limit, $datas[$i]->kid_weight), 2),
    //                             "target" => $datas[$i]->ki_target,
    //                             "last_year_date" => $this->lastYear($datas[$i]->ki_name, "monthly")
    //                           ),
    //                           "ytd" => array(
    //                             "actual_ytd" => number_format(score(achievment($datas[$i]->kid_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kid_limit, $datas[$i]->kid_weight) * $weight1 / 100, 2),
    //                             "achievment_ytd" => number_format(score(achievment($datas[$i]->kid_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kid_limit, $datas[$i]->kid_weight), 2),
    //                             "target_ytd" => $datas[$i]->ki_target_ytd,
    //                             "last_year_date" => $this->lastYear($datas[$i]->ki_name, "year_to_date")
    //                           ),
    //                           "chart" => $this->trendChart($datas[$i]->ki_id)
    //                         ));
    //                     }
    //                 }
    //             } else {
    //                 $maxLevel = $this->M_kpi_sla_master->maxLevel($datas[$i]->ki_id);
    //                 if ($depth == "1") {
    //                     $weight = $this->M_kpi_sla_master->countWeight($datas[$i]->ki_id, $nowYear, $month);
    //                 } else {
    //                     $weight = ($datas[$i]->ki_weight == null || empty($datas[$i]->ki_weight)) ? 0 : number_format($datas[$i]->ki_weight, 2);
    //                 }
    //                 if ($maxLevel != "1" && $depth != "1") {
    //                     $data_temp_id = $this->M_kpi_sla_master->getTotalScoreNotLevelOneWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_sla_master->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScore += number_format(($newTotal * $temp_id["weight"]) / 100, 2);
    //                         $newTotalYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                     }
    //                 } elseif ($depth == "1" && $maxLevel != "1") {
    //                     $data_temp_id = $this->M_kpi_sla_master->getTotalScoreNotLevelOneWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScoreYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScore = 0;
    //                     $totalScoreYtd = 0;
    //                     foreach ($data_temp_id as $temp_id) {
    //                         $newTotal = $this->M_kpi_sla_master->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScore += number_format((($newTotal * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                         $newTotalYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($temp_id["ki_id"], $month, $nowYear, $depth);
    //                         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
    //                     }
    //                 } else {
    //                     $totalScore = $this->M_kpi_sla_master->getTotalScoreWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                     $totalScoreYtd = $this->M_kpi_sla_master->getTotalScoreYtdWhere($datas[$i]->ki_id, $month, $nowYear, $depth);
    //                 }
    //                 $parentScore = number_format(($totalScore * $weight) / 100, 2);
    //                 $parentScoreYtd = number_format(($totalScoreYtd * $weight) / 100, 2);
    //                 if ($depth == 1) {
    //                     if (in_array(array("name" => $datas[$i]->ki_name), $corporate)) {
    //                     } else {
    //                         array_push($corporate, array(
    //                             "name" => $datas[$i]->ki_name,
    //                             "monthly" => array(
    //                               "actual" => $parentScore,
    //                               "achievment" => $totalScore,
    //                               "target" => $datas[$i]->ki_target,
    //                               "last_year_date" => $this->lastYear($datas[$i]->ki_name, "monthly")
    //                             ),
    //                             "ytd" => array(
    //                               "actual_ytd" => $parentScoreYtd,
    //                               "achievment_ytd" => $totalScoreYtd,
    //                               "target_ytd" => $datas[$i]->ki_target_ytd,
    //                               "last_year_date" => $this->lastYear($datas[$i]->ki_name, "year_to_date")
    //                             ),
    //                             "chart" => $this->trendChart($datas[$i]->ki_id)
    //                         ));
    //                     }
    //                 }
    //             }
    //             // Recursive Function
    //             $this->generatePageTreeTableActual($datas, $month, $nowYear, $depth + 1, $datas[$i]->ki_id);
    //         }
    //     }

    //     // Create Return Value
    //     return $corporate;
    // }

    function buildTree($elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['ki_parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['ki_id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

//==============================================================================
    public function index()
    {
        # code...
        $data = array(
            "content" => "kpi/v_kpi_corporate",
            "title" => "Dashboard KPI Corporate",
            "small_tittle" => "",
            "breadcrumb" => ["Dashboard KPI Corporate"],
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }

    public function oke()
    {
        // code...
        $query = $this->M_kpi_corporate->get_target_parent(2019);
        echo json_encode($query);
    }

    public function FunctionName($maxLevel,$ki_id,$mon,$year)
    {
        # code...
                    // $finalTotalScore=0;
                    $AcvTemp=array();
                    $AcvTempYtd=array();
                    $arrayTemp=array();
                    $parentArr=array();
                    $uppers=array();
                    $lowers=array();
                    $uppersYtd=array();
                    $lowersYtd=array();
                    $dataZ = $this->M_slabaseops->listActualWhere($ki_id,$mon,$year);
                    // echo json_encode($dataZ);exit;
                    if($maxLevel==0){
                        $tempTotalYtd=0;
                        $tempTotal=0;
                        $acvment=0;
                        $acvmentYtd=0;
                        $dataM = $this->M_slabaseops->listActualWhere($ki_id,$mon,$year);
                        // echo json_encode($dataM);exit;
                        // if(count($dataM)!=0){
                        $tempTotal+=number_format(score(achievment($dataM[0]->kid_target, $dataM[0]->kiad_actual, $dataM[0]->fa_function_name), $dataM[0]->kid_limit, $dataM[0]->kid_weight), 2);
                        $acvment+=number_format(achievment($dataM[0]->kid_target, $dataM[0]->kiad_actual, $dataM[0]->fa_function_name), 2);
                        $tempTotalYtd+=number_format(score(achievment($dataM[0]->kid_target_ytd, $dataM[0]->kiad_actual_ytd, $dataM[0]->fa_function_name), $dataM[0]->kid_limit, $dataM[0]->kid_weight), 2) ;
                        $acvmentYtd+=number_format(achievment($dataM[0]->kid_target_ytd, $dataM[0]->kiad_actual_ytd, $dataM[0]->fa_function_name), 2) ;
                        
                        if($dataM[0]->fa_function_name=="Stabilize"){
                            $targets=explode("|",$dataM[0]->ki_target);
                            $targetsYtd=explode("|",$dataM[0]->ki_target_ytd);
                            array_push($uppers,$targets[0]);
                            array_push($lowers,$targets[1]);
                            array_push($uppersYtd,$targetsYtd[0]);
                            array_push($lowersYtd,$targetsYtd[1]);
                            return $finalTotalScore= array('score'=>$tempTotal,'scoreYtd'=>$tempTotalYtd,'target'=>array('up'=>$uppers,'low'=>$lowers),'targetYtd'=>array('upYtd'=>$uppersYtd,'lowYtd'=>$lowersYtd),'acv'=>$acvment,'acvYtd'=> $acvmentYtd);
                        }else{
                            return $finalTotalScore= array('score'=>$tempTotal,'scoreYtd'=>$tempTotalYtd,'target'=>(float)$dataM[0]->kid_target,'targetYtd'=>(float)$dataM[0]->kid_target_ytd,'acv'=>$acvment,'acvYtd'=> $acvmentYtd);
                        }
                        exit;
                    }else{                    
                    for ($k=$maxLevel; $k >=1 ; $k--) { 
                        # code...

                        $items = $this->M_kpi_sla_master->getPenghuni($k,$ki_id);                        
                        $parent = $this->M_kpi_sla_master->getGroupLevelParent($k,$ki_id);
                       
                       
                        if($k==$maxLevel){
                            
                             foreach ($parent as $key ) {
                                # code...
                                $tempTotalYtd=0;
                                $tempTotal=0;
                                $acvment=0;
                                $acvmentYtd=0;
                                for ($p=0; $p < count($items) ; $p++) { 
                                    # code...
                                    if($items[$p]['ki_parent_id']==$key['ki_parent_id']){
                                       $dataM = $this->M_slabaseops->listActualWhere($items[$p]['ki_id'],$mon,$year);
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                           $tempTotal+=number_format(score(achievment($dataM[$z]->kid_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2);
                                           $tempTotalYtd+=number_format(score(achievment($dataM[$z]->kid_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2) ;
                                           $acvment+=number_format(achievment($dataM[$z]->kid_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), 2);
                                           $acvmentYtd+=number_format(achievment($dataM[$z]->kid_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name),2) ;
                                       }
                                    }
                                }
                                $arrayTemp[$key['ki_parent_id']] = $tempTotal;
                                $parentArr[$key['ki_parent_id']] = $tempTotalYtd;
                                $AcvTemp[$key['ki_parent_id']] = $acvment;
                                $AcvTempYtd[$key['ki_parent_id']] = $acvmentYtd;
                            }

                        }
                        else{
                            $tempFuck=0;
                            $tempFuckYtd=0;
                            $tempAcv=0;
                            $tempAcvYtd=0;

                            foreach ($parent as $keys ) {
                                # code...
                                for ($q=0; $q < count($items) ; $q++) { 
                                    if (array_key_exists($items[$q]['ki_id'],$arrayTemp) && $items[$q]['ki_parent_id']==$keys['ki_parent_id']){
                                        $tempFuck+=$arrayTemp[$items[$q]['ki_id']]*($items[$q]['ki_weight']/100);
                                        $tempFuckYtd+=$parentArr[$items[$q]['ki_id']]*($items[$q]['ki_weight']/100);
                                        $tempAcv+=$AcvTemp[$items[$q]['ki_id']]*($items[$q]['ki_weight']/100);
                                        $tempAcvYtd+=$AcvTempYtd[$items[$q]['ki_id']]*($items[$q]['ki_weight']/100);
                                    }elseif( $items[$q]['ki_parent_id']==$keys['ki_parent_id']){
                                       $dataM = $this->M_slabaseops->listActualWhere($items[$q]['ki_id'],$mon,$year);
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                           $tempFuck+=number_format(score(achievment($dataM[$z]->kid_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2);
                                           $tempFuckYtd+=number_format(score(achievment($dataM[$z]->kid_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2) ;
                                           $tempAcv+=number_format(achievment($dataM[$z]->kid_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), 2);
                                           $tempAcvYtd+=number_format(achievment($dataM[$z]->kid_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name),2) ;
                                       }
                                    }
                                }
                               
                                $arrayTemp[$keys['ki_parent_id']] = $tempFuck;
                                $parentArr[$keys['ki_parent_id']] = $tempFuckYtd;
                                $AcvTemp[$keys['ki_parent_id']] = $tempAcv;
                                $AcvTempYtd[$keys['ki_parent_id']] = $tempAcvYtd;
                                // $parentArr[$key['ki_parent_id']] = $tempTotal*$items[$q]['ki_id'];
                                $tempFuck=0;
                                $tempFuckYtd=0;
                                $tempAcv=0;
                                $tempAcvYtd=0;
                                
                            }

                            

                        }
                        
                    }
                }
                 if($dataZ[0]->fa_function_name=="Stabilize"){
                            $targets=explode("|",$dataZ[0]->ki_target);
                            $targetsYtd=explode("|",$dataZ[0]->ki_target_ytd);
                            array_push($uppers,$targets[0]);
                            array_push($lowers,$targets[1]);
                            array_push($uppersYtd,$targetsYtd[0]);
                            array_push($lowersYtd,$targetsYtd[1]);
                            return $finalTotalScore= array('score'=>$arrayTemp[$ki_id],'scoreYtd'=>$parentArr[$ki_id],'target'=>array('up'=>$uppers,'low'=>$lowers),'targetYtd'=>array('upYtd'=>$uppersYtd,'lowYtd'=>$lowersYtd),'acv'=>$AcvTemp[$ki_id],'acvYtd'=> $AcvTempYtd[$ki_id]);
                        }else{

                            return $finalTotalScore= array('score'=>$arrayTemp[$ki_id],'scoreYtd'=>$parentArr[$ki_id],'target'=>(float)$dataZ[0]->ki_target,'targetYtd'=>(float)$dataZ[0]->ki_target_ytd,'acv'=>$AcvTemp[$ki_id],'acvYtd'=> $AcvTempYtd[$ki_id]);
                        }
    }


    public function getGrafikValue(){
        $nameItemKpi=$this->input->post('names',true);
        // echo json_encode($_POST);exit;
        // $nameItemKpi="Finance Corporate";
        $startMonth=1;
        $lastMonth=date("m");
        $year=date('Y');

        $arrMonths = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        $kpi_item=$this->M_kpi_corporate->getItemKpibyName($nameItemKpi);
        // echo json_encode($kpi_item->ki_target);exit;

        $print['actual']=array();
        $print['actualYtd']=array();
        $print['target']=array();
        $print['targetYtd']=array();

        $maxLevel = $this->M_kpi_sla_master->maxLevel($kpi_item->ki_id);
        $dataZ = $this->M_slabaseops->listActualWhere($kpi_item->ki_id,$startMonth,$year);
        // echo json_encode($dataZ[0]->fa_function_name);exit;
        for ($i=$startMonth; $i <=$lastMonth ; $i++) { 
            # code...
            $bln=$arrMonths[($i-1)]."-".date('Y');

            $finalTotalScores=$this->FunctionName($maxLevel,$kpi_item->ki_id,$i,$year);
            // echo json_encode($finalTotalScores);exit;

            array_push($print['actual'],$finalTotalScores['score']);
            array_push($print['actualYtd'],$finalTotalScores['scoreYtd']);
            array_push($print['target'],$finalTotalScores['target']);
            array_push($print['targetYtd'],$finalTotalScores['targetYtd']);
            
        }
        if($dataZ[0]->fa_function_name=="Stabilize"){
            $upArr=array();
            $lowArr=array();
            $upArrYtd=array();
            $lowArrYtd=array();
            
            for ($i=0; $i < count($print['target']); $i++) { 
                # code...
                array_push($upArr,(float)$print['target'][$i]['up'][0]);
                array_push($lowArr,(float)$print['target'][$i]['low'][0]);
                array_push($upArrYtd,(float)$print['targetYtd'][$i]['upYtd'][0]);
                array_push($lowArrYtd,(float)$print['targetYtd'][$i]['lowYtd'][0]);
            }
            $print['target']=array('up'=>$upArr,'low'=>$lowArr);
            $print['targetYtd']=array('upYtd'=>$upArrYtd,'lowYtd'=>$lowArrYtd);
        }
        echo json_encode($print);
    }



    public function countOnBoard($id){
        $corporate = array();
        $year=date('Y');
        $mon=date('m');
        $totalScore=0;
        $kpisearch=$this->M_kpi_corporate->findKpiByGroup($id,$year);
        

        if(count($kpisearch)!=0){

            foreach ($kpisearch as $keys ) {
                # code...
                $maxLevel_ga = $this->M_kpi_sla_master->maxLevel($keys->ki_id);
                $finalTotalScores_ga=$this->FunctionName($maxLevel_ga,$keys->ki_id,$mon,$year);

                $targetPecah = explode("|",$keys->ki_target);
                if(count($targetPecah)>1){
                    $target = array(
                              "isStabilize" => true);
                }else{
                    $target = array(
                            "isStabilize" => false);
                }

                $totalScore+=$finalTotalScores_ga['score'];
                $last=$this->lastYear($maxLevel_ga,$keys->ki_name);
              
                 array_push($corporate, array(
                                "name" => $keys->ki_name,
                                "monthly" => array(
                                  "actual" => number_format($finalTotalScores_ga['score'],2),
                                  "achievment" => number_format($finalTotalScores_ga['score']-$keys->ki_target,2),
                                //   "achievment" => $finalTotalScores_ga['acv'],
                                  "target" => $keys->ki_target,
                                  "last_year_date" => number_format($last['lastmonth'],2),
                                ),
                                "ytd" => array(
                                  "actual_ytd" => number_format($finalTotalScores_ga['scoreYtd'],2),
                                  "achievment_ytd" => number_format($finalTotalScores_ga['scoreYtd']- $keys->ki_target_ytd,2),
                                //   "achievment_ytd" => $finalTotalScores_ga['acvYtd'],
                                  "target_ytd" => $keys->ki_target_ytd,
                                  "last_year_date" => number_format($last['lastyear'],2),
                                ),"chart" => array('target'=>$target)));
            }

        }
        // echo json_encode($kpisearch[0]->ki_id);exit;
        // array_push($corporate,array('totalScore'=>$totalScore));


        return $corporate;
    }


    function countTotal(){

        $year=date('Y');
        $date=date('m');
        $Alltotal=0;
        $kpi=$this->M_kpi_corporate->kpiOnGroup($year);

        if(count($kpi)!=0){
            foreach ($kpi as $key ) {
                # code...
                $maxLevel = $this->M_kpi_sla_master->maxLevel($key->ki_id);
                $finalTotalScores=$this->FunctionName($maxLevel,$key->ki_id,$date,$year);
                $Alltotal+=$finalTotalScores['score'];
            }
        }

        return $Alltotal;
    }

}
