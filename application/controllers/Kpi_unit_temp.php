<?php

/**
 *
 */
class Kpi_unit_temp extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("date");
        $this->load->model("M_kpi_unit_temp");
        $this->load->helper("kpiformula");
        $this->load->model("M_kpi_sla_master");
    }

    public function index()
    {
        $data = array(
            "content" => "kpi/v_kpi_unit_temp",
            "title" => "KPI SLA Master",
            // "formula" => $formula,
            // "allItem" => $allItem,
            // "allRole" => $allRole,
            // "deep_child" => $deep_child,
            "small_tittle" => "(Unit)",
            "breadcrumb" => ["KPI SLA Master"],
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }

    public function updateActual($id)
    {
        $update = $this->M_kpi_unit_temp->update_actual_where($id);
        if ($update) {
            echo json_encode(array("success" => true));
        } else {
            echo json_encode(array("success" => false));
        }
    }

    // function generatePageTreeTableActual($datas, $depth = 1, $parent = 0){
    //
    //   $month = $this->input->get("month");
    //   $role_id = $this->input->get("role_id");
    //   $nowYear = date("Y");
    //
    //   if($depth > 1000) return '';
    //   $deepChild = 1;
    //   $tree = '';
    //   for($i=0, $ni=count($datas); $i < $ni; $i++){
    //     if($datas[$i]->kia_parent_id == $parent){
    //       $target = explode("|", $datas[$i]->kiad_target);
    //       $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
    //       $tree .= "<tr data-id='".$datas[$i]->kia_id."' data-parent='".$datas[$i]->kia_parent_id."' data-level='".$depth."'> <td data-column='name'> L".$depth."&nbsp;&nbsp;-".str_repeat('&nbsp;&nbsp;&nbsp;', $depth);
    //       $tree .= $datas[$i]->kia_name."</td>";
    //       $tree .= "<td>".$actual."</td>";
    //       $tree .= "<td>".$datas[$i]->kia_uom."</td>";
    //       if($this->M_kpi_unit_temp->getDeepChild($datas[$i]->kia_id,$role_id) == 0){
    //         $tree .= "<td>".number_format(achievment( $datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), 2)."%"."</td>";
    //         $tree .= "<td>".number_format(score(achievment( $datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2)."%"."</td>";
    //         $tree .= "<td></td>";
    //         $tree .= "<td><center><button type='button' class='btn btn-flat bg-navy' id='add_actual' data-kia_name='".$datas[$i]->kia_name."' data-uom = '".$datas[$i]->kia_uom."' data-id='".$datas[$i]->kiad_id."' data-actual='".$datas[$i]->kiad_actual."'><i class='fa fa-pencil'></i></button><center></td>";
    //       } else{
    //         $maxLevel = $this->M_kpi_unit_temp->maxLevel($datas[$i]->kia_id,$role_id);
    //         if($depth == "1"){
    //           $weight = $this->M_kpi_unit_temp->countWeight($datas[$i]->kia_id, $nowYear, $month,$role_id);
    //         } else {
    //           $weight = ($datas[$i]->kia_weight == null || empty($datas[$i]->kia_weight)) ? 0 : number_format($datas[$i]->kia_weight, 2);
    //         }
    //         if($maxLevel != "1" && $depth != "1"){
    //           $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth,$role_id);
    //           $totalScore = 0;
    //           foreach ($data_temp_id as $temp_id) {
    //             $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear,$role_id, $depth);
    //             $totalScore += number_format(($newTotal*$temp_id["weight"])/100, 2);
    //           }
    //         } elseif ($depth == "1") {
    //           $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth,$role_id);
    //           $totalScore = 0;
    //           foreach ($data_temp_id as $temp_id) {
    //             $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear,$role_id, $depth);
    //             $totalScore += number_format((($newTotal*$temp_id["weight"])/100)*$temp_id["weight"]/100, 2);
    //           }
    //         }else{
    //           $totalScore = $this->M_kpi_unit_temp->getTotalScoreWhere($datas[$i]->kia_id, $month, $nowYear,$role_id, $depth);
    //         }
    //         $parentScore = number_format(($totalScore*$weight)/100, 2);
    //         $tree .= "<td>-</td>";
    //         $tree .= "<td>".$parentScore."%</td>";
    //         $tree .= "<td>".$totalScore."%</td>";
    //         $tree .= "<td><center>-</center></td>";
    //       }
    //       $tree .= "</tr>";
    //       $tree .= $this->generatePageTreeTableActual($datas, $depth+1, $datas[$i]->kia_id);
    //     }
    //   }
    //   return $tree;
    // }

    // function generatePageTreeTable($datas, $depth = 1, $parent = 0){
    //
    //   $month = $this->input->get("month");
    //   $role_id = $this->input->get("role_id");
    //   $nowYear = date("Y");
    //
    // 	if($depth > 1000) return '';
    //   $deepChild = 1;
    // 	$tree = '';
    // 	for($i=0, $ni=count($datas); $i < $ni; $i++){
    // 		if($datas[$i]->kia_parent_id == $parent){
    //       $target = explode("|", $datas[$i]->kiad_target);
    //       $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
    // 			$tree .= "<tr data-id='".$datas[$i]->kia_id."' data-parent='".$datas[$i]->kia_parent_id."' data-level='".$depth."'> <td data-column='name'> L".$depth."&nbsp;&nbsp;-".str_repeat('&nbsp;&nbsp;&nbsp;', $depth);
    // 			$tree .= $datas[$i]->kia_name."</td>";
    // 			$tree .= "<td>".$actual."</td>";
    //       $tree .= "<td>".$datas[$i]->kia_uom."</td>";
    //       if($this->M_kpi_unit_temp->getDeepChild($datas[$i]->kia_id) == 0){
    //         $tree .= "<td>".number_format(achievment( $datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), 2)."%"."</td>";
    //         $tree .= "<td>".number_format(score(achievment( $datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kia_weight), 2)."%"."</td>";
    //         $tree .= "<td></td>";
    //         $tree .= "<td><center><button type='button' class='btn btn-flat bg-navy' id='add_actual' data-kia_name='".$datas[$i]->kia_name."' data-uom = '".$datas[$i]->kia_uom."' data-id='".$datas[$i]->kiad_id."' data-actual='".$datas[$i]->kiad_actual."'><i class='fa fa-pencil'></i></button><center></td>";
    //       } else{
    //         $totalScore = $this->M_kpi_unit_temp->getTotalScoreWhere($datas[$i]->kia_id, $month, $nowYear, $role_id);
    //         $weight = ($datas[$i]->kia_weight == null || empty($datas[$i]->kia_weight)) ? 0 : number_format($datas[$i]->kia_weight, 2);
    //         $parentScore = number_format(($totalScore*$weight)/100, 2);
    //         $tree .= "<td>-</td>";
    //         $tree .= "<td>".$parentScore."%</td>";
    //         $tree .= "<td>".$totalScore."%</td>";
    //         $tree .= "<td><center>-</center></td>";
    //       }
    //       $tree .= "</tr>";
    // 			$tree .= $this->generatePageTreeTable($datas, $depth+1, $datas[$i]->kia_id);
    // 		}
    // 	}
    // 	return $tree;
    // }

    function generatePageTreeTableActual($datas, $depth = 1, $parent = 0)
    {
        $month = $this->input->get("month");
        $nowYear = $this->input->get("year");

        if ($depth > 1000) return '';
        $deepChild = 1;
        $tree = '';
        for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
            if ($datas[$i]->kia_parent_id == $parent) {
                $target = explode("|", $datas[$i]->kiad_target);
                $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
                $actualYtd = (($datas[$i]->kiad_actual_ytd == null || empty($datas[$i]->kiad_actual_ytd)) ? "-" : $datas[$i]->kiad_actual_ytd);
                $tree .= "<tr data-id='" . $datas[$i]->kia_id . "' data-parent='" . $datas[$i]->kia_parent_id . "' data-level='" . $depth . "'> <td data-column='name'> L" . $depth . "&nbsp;&nbsp;-" . str_repeat('&nbsp;&nbsp;&nbsp;', $depth);
                if ($this->M_kpi_unit_temp->getDeepChild($datas[$i]->kia_id, $datas[$i]->role_id) == 0) {
                    $tree .= $datas[$i]->kia_name . "</td>";
                    $tree .= "<td>" . $datas[$i]->kia_uom . "</td>";
                    $tree .= "<td style='".(($actual > $datas[$i]->kiad_target) ? "color:green;" : "color:red;")."'>" . $actual . "</td>";
                    //target
                    $tree .= "<td>".$datas[$i]->kiad_target."</td>";
                    //Achievement
                    $tree .= "<td>" . number_format(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), 2) . "%" . "</td>";
                    //Score
                    $tree .= "<td>" . number_format(score(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2) . "%" . "</td>";
                    // total score
                    $tree .= "<td></td>";

                    $tree .= "<td  style='".(($actualYtd > $datas[$i]->kiad_target_ytd) ? "color:green;" : "color:red;")."'>" . $actualYtd . "</td>";
                    //target ytd
                    $tree .= "<td>".$datas[$i]->kiad_target_ytd."</td>";
                    //Achievement Ytd
                    $tree .= "<td>" . number_format(achievment($datas[$i]->kiad_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), 2) . "%" . "</td>";
                    //Score Ytd
                    $tree .= "<td>" . number_format(score(achievment($datas[$i]->kiad_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2) . "%" . "</td>";
                    // total score ytd
                    $tree .= "<td></td>";

                    //action
                    $tree .= "<td>
                                <center>
                                  <button type='button' class='btn btn-flat bg-navy' id='add_actual'
                                  data-kia_name='" . $datas[$i]->kia_name . "' data-uom = '" . $datas[$i]->kia_uom . "'
                                  data-id='" . $datas[$i]->kiad_id . "' data-actual='" . $datas[$i]->kiad_actual . "'
                                  data-actual_ytd='" . $datas[$i]->kiad_actual_ytd . "'
                                  data-formula='".$datas[$i]->fa_function_name."'>
                                    <i class='fa fa-pencil'></i>
                                  </button>
                                <center>
                              </td>";
                    //
                } else {
                    $maxLevel = $this->M_kpi_sla_master->maxLevel_unit($datas[$i]->kia_id);
                    $finalTotalScores=$this->FunctionName($maxLevel,$datas[$i]->kia_id,$datas[$i]->role_id);
                  

                    // $maxLevel = $this->M_kpi_unit_temp->maxLevel($datas[$i]->kia_id, $datas[$i]->role_id);
                    // if ($depth == "1") {
                    //     $weight = $this->M_kpi_unit_temp->countWeight($datas[$i]->kia_id, $nowYear, $month, $datas[$i]->role_id);
                    // } else {
                    //     $weight = ($datas[$i]->kia_weight == null || empty($datas[$i]->kia_weight)) ? 0 : number_format($datas[$i]->kia_weight, 2);
                    // }
                    // if ($maxLevel != "1" && $depth != "1") {
                    //     $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
                    //     $totalScore = 0;
                    //     $totalScoreYtd = 0;
                    //     foreach ($data_temp_id as $temp_id) {
                    //         $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
                    //         $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
                    //         $totalScore += number_format(($newTotal * $temp_id["weight"]) / 100, 2);
                    //         $totalScoreYtd += number_format(($newTotalYtd * $temp_id["weight"]) / 100, 2);
                    //     }
                    // } elseif ($depth == "1" && $maxLevel != "1") {
                    //     $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
                    //     $totalScore = 0;
                    //     $totalScoreYtd = 0;
                    //     foreach ($data_temp_id as $temp_id) {
                    //         $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
                    //         $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
                    //         $totalScore += number_format((($newTotal * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
                    //         $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
                    //     }
                    // } else {
                    //     $totalScore = $this->M_kpi_unit_temp->getTotalScoreWhere($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
                    //     $totalScoreYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
                    // }
                    // $parentScore = number_format(($totalScore * $weight) / 100, 2);
                    // $parentScoreYtd = number_format(($totalScoreYtd * $weight) / 100, 2);
                   

                    $tree .= $datas[$i]->kia_name . "</td>";
                    $tree .= "<td>" . $datas[$i]->kia_uom . "</td>";
                    $tree .= "<td style='".(($actual > $datas[$i]->kia_target) ? "color:green;" : "color:red;")."'>" . $actual . "</td>";
                    //target
                    $tree .= "<td>".$datas[$i]->kia_target."</td>";
                    // Achievement
                    $tree .= "<td>-</td>";
                    // //Score
                    if($depth!=1){
                        $tree .= "<td>" . number_format($finalTotalScores['score']*($datas[$i]->kia_weight/100),2) . "%</td>";
                    }else{
                        $tree .= "<td>" . number_format($finalTotalScores['score'],2) . "%</td>";
                    }
                    //Total Score
                    $tree .= "<td>" . number_format($finalTotalScores['score'],2) . "%</td>";

                    $tree .= "<td style='".(($actualYtd > $datas[$i]->kia_target_ytd) ? "color:green;" : "color:red;")."'>" . $actualYtd . "</td>";
                    //target ytd
                    $tree .= "<td>".$datas[$i]->kia_target_ytd."</td>";
                    //Achievement Ytd
                    $tree .= "<td>-</td>";
                    //Score Ytd
                    if($depth!=1){
                    $tree .= "<td>". number_format($finalTotalScores['scoreYtd']*($datas[$i]->kia_weight/100),2) ."</td>";
                    }else{
                        $tree .= "<td>".number_format($finalTotalScores['scoreYtd'],2)."</td>";
                    }
                    //Total Score Ytd
                    $tree .= "<td>".number_format($finalTotalScores['scoreYtd'],2)."</td>";

                    //action
                    $tree .= "<td>-</td>";
                }
                $tree .= "</tr>";
                $tree .= $this->generatePageTreeTableActual($datas, $depth + 1, $datas[$i]->kia_id);
            }
        }
        return $tree;
    }

    function get_all_total_final($datas, $finalTotalScore=0, $depth = 1, $parent = 0)
    {
        $month = $this->input->get("month");
        $nowYear = $this->input->get("year");

        if ($depth > 1000) return '';
        $deepChild = 1;
        $tree = '';
        $globalFinalScore = $finalTotalScore;
        for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
            if ($datas[$i]->kia_parent_id == $parent) {
                $target = explode("|", $datas[$i]->kiad_target);
                $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
                $actualYtd = (($datas[$i]->kiad_actual_ytd == null || empty($datas[$i]->kiad_actual_ytd)) ? "-" : $datas[$i]->kiad_actual_ytd);
                $tree .= "<tr data-id='" . $datas[$i]->kia_id . "' data-parent='" . $datas[$i]->kia_parent_id . "' data-level='" . $depth . "'> <td data-column='name'> L" . $depth . "&nbsp;&nbsp;-" . str_repeat('&nbsp;&nbsp;&nbsp;', $depth);
                if ($this->M_kpi_unit_temp->getDeepChild($datas[$i]->kia_id, $datas[$i]->role_id) == 0) {
                    $globalFinalScore += number_format(score(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2);
                } else {
                    $maxLevel = $this->M_kpi_unit_temp->maxLevel($datas[$i]->kia_id, $datas[$i]->role_id);
                    if ($depth == "1") {
                        $weight = $this->M_kpi_unit_temp->countWeight($datas[$i]->kia_id, $nowYear, $month, $datas[$i]->role_id);
                    } else {
                        $weight = ($datas[$i]->kia_weight == null || empty($datas[$i]->kia_weight)) ? 0 : number_format($datas[$i]->kia_weight, 2);
                    }
                    if ($maxLevel != "1" && $depth != "1") {
                        $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
                        $totalScore = 0;
                        $totalScoreYtd = 0;
                        foreach ($data_temp_id as $temp_id) {
                            $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
                            $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
                            $totalScore += number_format(($newTotal * $temp_id["weight"]) / 100, 2);
                            $totalScoreYtd += number_format(($newTotalYtd * $temp_id["weight"]) / 100, 2);
                        }
                    } elseif ($depth == "1" && $maxLevel != "1") {
                        $data_temp_id = $this->M_kpi_unit_temp->getTotalScoreNotLevelOneWhere($datas[$i]->kia_id, $month, $nowYear, $depth, $datas[$i]->role_id);
                        $totalScore = 0;
                        $totalScoreYtd = 0;
                        foreach ($data_temp_id as $temp_id) {
                            $newTotal = $this->M_kpi_unit_temp->getTotalScoreWhere($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
                            $newTotalYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($temp_id["ki_id"], $month, $nowYear, $datas[$i]->role_id, $depth);
                            $totalScore += number_format((($newTotal * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
                            $totalScoreYtd += number_format((($newTotalYtd * $temp_id["weight"]) / 100) * $temp_id["weight"] / 100, 2);
                        }
                    } else {
                        $totalScore = $this->M_kpi_unit_temp->getTotalScoreWhere($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
                        $totalScoreYtd = $this->M_kpi_unit_temp->getTotalScoreWhereYtd($datas[$i]->kia_id, $month, $nowYear, $datas[$i]->role_id, $depth);
                    }
                    $parentScore = number_format(($totalScore * $weight) / 100, 2);
                    $parentScoreYtd = number_format(($totalScoreYtd * $weight) / 100, 2);
                    $globalFinalScore += $totalScore;
                }
                $this->get_all_total_final($datas, $globalFinalScore, $depth + 1, $datas[$i]->kia_id);
            }
        }
        return $globalFinalScore;
    }

    public function get_all_total()
    {

      $year= $this->input->get("year");
      $month = $this->input->get("month");
      $role_id = $this->input->get("role_id");
      $dataMentah = $this->M_kpi_sla_master->getZerosParent_unit($year,$role_id);
    //   echo json_encode($dataMentah);exit;
      $loops=0;
      $temp=0;
      foreach ($dataMentah as $key ) {
          # code...
          $maxLevel = $this->M_kpi_sla_master->maxLevel_unit($key['kia_id']);
          $finalTotalScores=$this->FunctionName($maxLevel,$key['kia_id'],$key['role_id']);
          $temp+=$finalTotalScores['score'];
          $loops+=1;
      }
      
    //   $content =number_format(($temp/$loops),2);
      echo json_encode(array("total" => number_format($temp,2)));
    
    }

    public function getDataActual()
    {
        $dataMentah = $this->M_kpi_unit_temp->getAllItemKpiWithDetail();
        // print_r($dataMentah);exit;
        $content = $this->generatePageTreeTableActual($dataMentah);
        echo json_encode(array("content" => $content));
    }


    public function FunctionName($maxLevel,$ki_id,$role_id)
    {
        # code...
                    // $finalTotalScore=0;
                    
                    if($maxLevel==0){
                        $oke=0;$okeYtd=0;
                        $dataM = $this->M_kpi_sla_master->listActualWhere_unit($ki_id,$role_id);$z=0;
                         $oke+=number_format(score(achievment($dataM[$z]->kiad_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2);
                         $okeYtd+=number_format(score(achievment($dataM[$z]->kiad_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2) ;
                         return $finalTotalScore= array('score'=>$oke,'scoreYtd'=>$okeYtd);
                         exit;
                    }

                    $arrayTemp=array();
                    $parentArr=array();
                    for ($k=$maxLevel; $k >=1 ; $k--) { 
                        # code...

                        $items = $this->M_kpi_sla_master->getPenghuni_unit($k,$ki_id);                        
                        $parent = $this->M_kpi_sla_master->getGroupLevelParent_unit($k,$ki_id);

                        // echo json_encode($items);exit;
                       
                       
                        if($k==$maxLevel){
                            
                             foreach ($parent as $key ) {
                                # code...
                                $tempTotalYtd=0;
                                $tempTotal=0;
                                for ($p=0; $p < count($items) ; $p++) { 
                                    # code...
                                    if($items[$p]['kia_parent_id']==$key['kia_parent_id']){
                                       $dataM = $this->M_kpi_sla_master->listActualWhere_unit($items[$p]['kia_id'],$role_id);
                                    //    echo json_encode($dataM);exit;
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                        // $tree .= "<td>" . number_format(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), 2) . "%" . "</td>";
                                        // $tree .= "<td>" . number_format(score(achievment($datas[$i]->kiad_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kiad_limit, $datas[$i]->kiad_weight), 2) . "%" . "</td>";
                                           $tempTotal+=number_format(score(achievment($dataM[$z]->kiad_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2);
                                           $tempTotalYtd+=number_format(score(achievment($dataM[$z]->kiad_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2) ;
                                       }
                                    }
                                }
                                $arrayTemp[$key['kia_parent_id']] = $tempTotal;
                                $parentArr[$key['kia_parent_id']] = $tempTotalYtd;
                            }

                        }
                        else{
                            $tempFuck=0;
                            $tempFuckYtd=0;

                            foreach ($parent as $keys ) {
                                # code...
                                for ($q=0; $q < count($items) ; $q++) { 
                                    if (array_key_exists($items[$q]['kia_id'],$arrayTemp) && $items[$q]['kia_parent_id']==$keys['kia_parent_id']){
                                        $tempFuck+=$arrayTemp[$items[$q]['kia_id']]*($items[$q]['kia_weight']/100);
                                        $tempFuckYtd+=$parentArr[$items[$q]['kia_id']]*($items[$q]['kia_weight']/100);
                                    }elseif( $items[$q]['kia_parent_id']==$keys['kia_parent_id']){
                                       $dataM = $this->M_kpi_sla_master->listActualWhere_unit($items[$q]['kia_id'],$role_id);
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                           $tempFuck+=number_format(score(achievment($dataM[$z]->kiad_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2);
                                           $tempFuckYtd+=number_format(score(achievment($dataM[$z]->kiad_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kiad_limit, $dataM[$z]->kiad_weight), 2) ;
                                       }
                                    }
                                }
                               
                                $arrayTemp[$keys['kia_parent_id']] = $tempFuck;
                                $parentArr[$keys['kia_parent_id']] = $tempFuckYtd;
                                // $parentArr[$key['ki_parent_id']] = $tempTotal*$items[$q]['ki_id'];
                                $tempFuck=0;
                                $tempFuckYtd=0;
                                
                            }

                            

                        }
                        
                    }

                    return $finalTotalScore= array('score'=>$arrayTemp[$ki_id],'scoreYtd'=>$parentArr[$ki_id]);
    }
}

?>
