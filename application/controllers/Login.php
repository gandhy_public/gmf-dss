<?php

/**
 *
 */
class Login extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_login");
        $this->load->model("M_user");
        $this->load->model("M_kpi_sla_master");
        $this->load->library("session");
        $this->output->set_header('X-Content-Type-Options: nosniff');
        $this->output->set_header('X-XSS-Protection:1;mode=block');
        header_remove('x-powered-by');
        // header_remove('Set-Cookie');

    }


    // public function css()
    // {
    //   $css = $this->input->get("css");
    //   header('X-Content-Type-Options: nosniff');
    //   header('X-XSS-Protection:1;mode=block');
    //   header_remove('x-powered-by');
    //   header('Content-Type: text/css');
    //   $file = file_get_contents("./assets/{$css}");
    //   echo $file;
    // }
    //
    // public function js($any)
    // {
    //
    // }

    public function index()
    {
        // echo base_url();
        // exit();
        $role = $this->M_login->load_role();
        $this->load->view("login", array("roles" => $role));
        // phpinfo();
        // print_r(in_array('mod_rewrite', apache_get_modules()));
    }

    public function checkLdap($username, $password = "tryufdbwiEGDOYILSzCBI1563847Y2RFGDSUA!@#$%$@#q#$@$%#%#%^^&%^%LSZV")
    {
        $dn = "DC=gmf-aeroasia,DC=co,DC=id";
        $ldapconn = ldap_connect("192.168.240.57") or die ("Could not connect to LDAP server.");
        if ($ldapconn) {
            ldap_set_option(@$ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option(@$ldap, LDAP_OPT_REFERRALS, 0);
            $ldapbind = ldap_bind($ldapconn, "ldap", "aeroasia");
            @$sr = ldap_search($ldapconn, $dn, "samaccountname=$username");
            @$srmail = ldap_search($ldapconn, $dn, "mail=$username@gmf-aeroasia.co.id");
            @$info = ldap_get_entries($ldapconn, @$sr);
            @$infomail = ldap_get_entries($ldapconn, @$srmail);
            @$usermail = substr(@$infomail[0]["mail"][0], 0, strpos(@$infomail[0]["mail"][0], '@'));
            @$bind = (($password != "tryufdbwiEGDOYILSzCBI1563847Y2RFGDSUA!@#$%$@#q#$@$%#%#%^^&%^%LSZV") ? @ldap_bind($ldapconn, $info[0][dn], $password) : true);
            if ((@$info[0]["samaccountname"][0] == $username AND $bind) OR (@$usermail == $username AND $bind)) {
              // echo "<pre>";
              //   print_r($info);
              // echo "</pre>";
              // exit();
                // $search_filter = '(&(objectCategory=person)(samaccountname=*))';
                // $attributes = array();
                // $attributes[] = 'givenname';
                // $attributes[] = 'mail';
                // $attributes[] = 'samaccountname';
                // $attributes[] = 'sn';
                // $attributes[] = 'description';
                // $result = ldap_search($ldapconn, $dn, $search_filter, $attributes);
                // if (FALSE !== $result){
                //     $entries = ldap_get_entries($ldapconn, $result);
                //     for ($x=0; $x<$entries['count']; $x++){
                //         if (!empty($entries[$x]['givenname'][0]) &&
                //              !empty($entries[$x]['mail'][0]) &&
                //              !empty($entries[$x]['samaccountname'][0]) &&
                //              !empty($entries[$x]['sn'][0]) &&
                //              'Shop' !== $entries[$x]['sn'][0] &&
                //              'Account' !== $entries[$x]['sn'][0])
                //         {
                //             $ad_users[strtoupper(trim($entries[$x]['samaccountname'][0]))] = array(
                //               'email' => strtolower(trim($entries[$x]['mail'][0])),
                //               'first_name' => trim($entries[$x]['givenname'][0]),
                //               'last_name' => trim($entries[$x]['sn'][0]),
                //               'unit' => trim($entries[$x]['description'][0])
                //             );
                //         }
                //     }
                //     echo "<pre>";
                //       print_r($ad_users);
                //     echo "</pre>";
                // }
                return true;
            } else {
                return false;
            }
        } else {
            echo "LDAP Connection trouble,, please try again 2/3 time";
        }
    }

    public function tesLdap($ldap_username, $ldap_password)
    {
      $ldap_connection = ldap_connect("192.168.240.57");
      if (FALSE === $ldap_connection){
          // Uh-oh, something is wrong...
          echo "Some thing wrong";
      }

      // We have to set this option for the version of Active Directory we are using.
      ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Unable to set LDAP protocol version');
      ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0); // We need this for doing an LDAP search.

      if (TRUE === ldap_bind($ldap_connection, $ldap_username, $ldap_password)){
          $ldap_base_dn = 'DC=XXXX,DC=XXXX';
          $search_filter = '(&(objectCategory=person)(samaccountname=*))';
          $attributes = array();
          $attributes[] = 'givenname';
          $attributes[] = 'mail';
          $attributes[] = 'samaccountname';
          $attributes[] = 'sn';
          $result = ldap_search($ldap_connection, $ldap_base_dn, $search_filter, $attributes);
          if (FALSE !== $result){
              $entries = ldap_get_entries($ldap_connection, $result);
              for ($x=0; $x<$entries['count']; $x++){
                  if (!empty($entries[$x]['givenname'][0]) &&
                       !empty($entries[$x]['mail'][0]) &&
                       !empty($entries[$x]['samaccountname'][0]) &&
                       !empty($entries[$x]['sn'][0]) &&
                       'Shop' !== $entries[$x]['sn'][0] &&
                       'Account' !== $entries[$x]['sn'][0]){
                      $ad_users[strtoupper(trim($entries[$x]['samaccountname'][0]))] = array('email' => strtolower(trim($entries[$x]['mail'][0])),'first_name' => trim($entries[$x]['givenname'][0]),'last_name' => trim($entries[$x]['sn'][0]));
                  }
              }
          }
          ldap_unbind($ldap_connection); // Clean up after ourselves.
      }

      $message .= "Retrieved ". count($ad_users) ." Active Directory users\n";
      echo $message;
    }

    public function register()
    {
      $username = $this->input->post("username");
      $role_id = $this->input->post("role_id");
      $description = $this->input->post("description");
      $result = array();
      $checkLdap = $this->checkLdap($username);
      if($username == "user_tes"){
        $checkLdap = true;
      }
      if($checkLdap){
        if($this->M_user->isUnique($this->input->post('username'))){
          $save = $this->M_login->save($username, $role_id, $description);
          if($save){
            $this->output->set_status_header(200)->set_content_type('application/json');
            $result["success"] = true;
            $result["message"] = "Data has sent to admin";
          } else {
            $this->output->set_status_header(500)->set_content_type('application/json');
            $result["success"] = false;
            $result["message"] = "Failed insert database";
          }
        } else {
            $this->output->set_status_header(500)->set_content_type('application/json');
            $result["success"] = false;
            $result["code"] = "1";
            $result["message"] = "User already exist";
        }
      } else {
        $this->output->set_status_header(500)->set_content_type('application/json');
        $result["code"] = "2";
        $result["message"] = "User not found in LDAP server";
        $result["success"] = false;
      }
      echo json_encode($result);
    }

    public function masuk()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $cek = $this->M_login->cek_login($username);
        $count = count($cek);
        if($username == "anon_dss" || $username == "user_ter" || $username == "user_tlp" || $username == "user_td"){
            $checkLdap = true;
        } else {
            $checkLdap = $this->checkLdap($username, $password);
        }
        if ($checkLdap) {
            if ($count == "1") {
                $status = "1";
                $msg = "Login success, Please wait..";
                $get = $this->M_login->get_user($username);
                $getRole = $this->M_kpi_sla_master->get_unit($get->kpi_unit_id);
                $data_session = array(
                    "user_id" => $get->user_id,
                    'username' => $get->username,
                    'new_role' => (empty($getRole->kpi_unit_name) ? "Undefined" : $getRole->kpi_unit_name),
                    'new_role_id' => (empty($getRole->kpi_unit_id) ? "Undefined" : $getRole->kpi_unit_id),
                    'role' => $get->role_name,
                    'roleid' => $get->roleid,
                    'status' => "login"
                );
                $this->session->set_userdata($data_session);
            } else {
                $status = "0";
                $msg = "Username or password is wrong";
                $data_session = '';
            }
        } else {
            $status = "0";
            $msg = "Username or password is wrong";
            $data_session = '';
        }
          echo json_encode(
              array(
                  'status' => $status,
                  'msg' => $msg,
                  'sess' => $data_session
              )
          );
    }

    public function keluar()
    {
        $this->session->unset_userdata("user_id");
        $this->session->unset_userdata("nama");
        $this->session->unset_userdata("email");
        $this->session->unset_userdata("role");
        $this->session->unset_userdata("roleid");
        $this->session->unset_userdata("status");
        $this->session->unset_userdata("username");
        $this->session->unset_userdata("__ci_last_regenerate");
        echo json_encode(array("success" => true));
    }

    public function cek_menu()
    {
        $role_id = $this->input->post('role_id');
        $link = $this->input->post('link');
        $cek = $this->M_login->cek_menu($role_id, $link);
        $count = count($cek);
        echo json_encode(
            array(
                'status' => $count,
            )
        );
    }
}
