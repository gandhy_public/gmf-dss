<?php

 defined('BASEPATH') OR exit('No direct script access allowed');


  class Error extends MY_Controller{

    function __construct(){
      parent::__construct();
      $this->load->database();
    }
	
    function not_found(){
      $this->load->view("error/404");
    }

  }
