<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Role extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_role");
    }

    public function index()
    {
        $data = array(
            "content" => "role/role_view",
            "title" => "Role Managements",
            "small_tittle" => "",
            "breadcrumb" => ["Role Managements"],
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }

    public function view_role()
    {
        $id = $this->input->post("id");
        $all_menu = $this->M_role->get_menu();
        $induk = $this->M_role->get_role($id, 'induk');
        $anak = $this->M_role->get_role($id, 'anak');
        // print_r($all_menu);

        echo json_encode(
            array(
                'all' => $all_menu,
                'induk' => $induk,
                'anak' => $anak,
            )
        );
    }

    public function datatable()
    {
        $columns = array(
            0 => "role_id",
            1 => "role_name"
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']["value"];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            "draw" => $draw,
            "length" => $length,
            "start" => $start,
            "search" => $search,
            "order_column" => $order_column,
            "order_dir" => $order_dir
        );

        $send = $this->M_role->show_datatable($data);
        echo $send;
    }

    public function role_form($id = null)
    {
        $data = array(
            "content" => "role/role_form",
            "title" => "Role Form",
            "small_tittle" => "",
            "breadcrumb" => ["Role Managements", "Role Form"],
            "role_id" => ($id != null) ? $id : "",
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }

    public function save_role()
    {
        $response = array();
        if ($this->input->post("role_id") === "") {
            echo json_encode(
                array("success" => ($this->M_role->save_role()) ? true : false)
            );
        } else {
            echo json_encode(
                array("success" => ($this->M_role->update_role()) ? true : false)
            );
        }
    }

    public function edit_data_form($id)
    {
        echo json_encode($this->M_role->get_role_where($id));
    }

    public function role_to_bin($id)
    {
        echo json_encode(
            array("success" => ($this->M_role->del_role($id)) ? true : false)
        );
    }

    public function update_user_role()
    {
        $id_user = $this->input->post('id_user');
        $induk = $this->input->post('induk');
        $anak = $this->input->post('anak');
        // print_r($id_user);
        foreach ($induk as $r) {
            if ($r['check'] == "1" && isset($r['id'])) {
                // print_r($r['id']);
                $cek = $this->M_role->cek_role($id_user, $r['id']);

                if ($cek == "0") {
                    // print_r($cek);
                    $exec = $this->M_role->add_role($id_user, $r['id']);
                }
            } else if ($r['check'] == "0" && isset($r['id'])) {
                // print_r(0);
                $cek = $this->M_role->cek_role($id_user, $r['id']);
                if ($cek == "1") {
                    $exec = $this->M_role->delete_role($id_user, $r['id']);
                }
            }

        }
        foreach ($anak as $r) {
            if ($r['check'] == "1" && isset($r['id'])) {
                $cek = $this->M_role->cek_role($id_user, $r['id']);
                if ($cek == "0") {
                    $exec = $this->M_role->add_role($id_user, $r['id']);
                }
            } else if ($r['check'] == "0" && isset($r['id'])) {
                // print_r(1);
                $cek = $this->M_role->cek_role($id_user, $r['id']);
                if ($cek == "1") {
                    $exec = $this->M_role->delete_role($id_user, $r['id']);
                }
            }
        }

        echo json_encode(array('notify' => 1, 'message' => 'Role User Berhasil Diubah'));
    }

}
