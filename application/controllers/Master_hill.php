<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Master_hill extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("M_menu");
        $this->load->model("Master_Repetitive_hill");
    }

    function index()
    {
        $data = array(
            "content" => "hil/Master_Hill",
            "title" => "Master Data HIL",
            "small_tittle" => "",
            "breadcrumb" => ["HIL Target"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    public function datamitigation()
    {
        $data = $this->Master_Repetitive_hill->test();
        echo json_encode($data);
    }

    public function target_hill()
    {
        $in['target'] = $this->input->post('target');
        $in['tahun'] = $this->input->post('year');
        $success = '';
        $cek = $this->Master_Repetitive_hill->cek_hill($in);
        if ($cek == '') {
            $insert = $this->Master_Repetitive_hill->add_target_hill($in);
            $success = '1';
        } else {
            $update = $this->Master_Repetitive_hill->update_target_hill($in);
            $success = '2';
        }
        echo json_encode($success);
    }

    public function get_target()
    {
        $in['tahun'] = '2018';
        // $in['tahun'] = date("Y");

        $value = $this->Master_Repetitive_hill->cek_hill($in);
        $value = $value[0]['target_val'];
        // $value = $cek;
        echo $value;

    }

    public function datatables($param1 = "", $param2 = "")
    {
        // code...
        if ($param1 == "" OR $param2 == "") {
            // code...
            $param1 = "01-" . date('Y');
            $param2 = date('m-Y');
        }

        $start = explode('-', $param1);
        $end = explode('-', $param2);

        $draw = $_POST['draw'];
        $length = $_POST['length'];
        $starts = $_POST['start'];
        $search = $_POST['search']['value'];


        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $starts,
            'search' => $search
        );

        $jml = $this->Master_Repetitive_hill->count_all($start, $end, $data);
        $datas = $this->Master_Repetitive_hill->get_datatables($start, $end, $data);

        if ($jml > 0) {
            // code...
            $no = $data['start'] + 1;
            foreach ($datas as $key) {
                // code...
                if ($key['mitigasi_id'] != null) {
                    // code...
                    $note = true;
                } else {
                    $note = false;
                }
                $print['data'][] = array(
                    // $key['OWN'],
                    $no++,
                    $key['own'],
                    $key['actype'],
                    $key['notification'],
                    date('d-m-Y', strtotime($key['created_on'])),
                    date('d-m-Y', strtotime($key['req_start'])),
                    $note,
                    "<button type=button data-id='" . $key['notification'] . "' class='btn btn-info btn-flat ' id=add name=add><i class='fa fa-pencil' ></i></button>"
                );
            }
        } else {
            $print['data'] = array();
        }

        $print['draw'] = $data['draw'];
        $print['recordsFiltered'] = $jml;
        $print['recordsTotal'] = $jml;
        echo json_encode($print);
    }

    public function get_mitigasi()
    {
        // code...
        $id = $this->input->post('param1');
        $query = $this->Master_Repetitive_hill->m_get_mitigation($id);
        if (count($query) > 0) {
            // code...
            $data = array('why' => $query[0]['mitigasi_why'],
                'solution' => $query[0]['mitigasi_solution']);
        } else {
            $data = array('why' => '',
                'solution' => '');
        }
        echo json_encode($data);
    }

    public function store_mitigasi()
    {
        // code...
        $why = $this->input->post('why');
        $solution = $this->input->post('solution');
        $idmitigasi = $this->input->post('idmitigasi');

        $data = array('mitigasi_why' => $why,
            'mitigasi_solution' => $solution,
            'mitigasi_fk_id' => $idmitigasi,
            'mitigasi_type' => 'hill',
            'mitigasi_date' => date('Y-m-d h:i:s'));

        $query = $this->Master_Repetitive_hill->cek_mitigastion($idmitigasi, $data);

        if ($query) {
            // code...
            $data = array('info' => 'success',
                'msg' => 'berhasil simpan data');
        } else {
            $data = array('info' => 'error',
                'msg' => 'Gagal simpan data');
        }

        echo json_encode($data);
    }

    public function store_target()
    {
        // code...
        // print_r($_POST);

        $target = $this->input->post('target');
        $year = $this->input->post('year');

        $data = array('target_year' => $year,
            'target_val' => $target,
            'target_type' => 'Hill');

        $query = $this->Master_Repetitive_hill->target($data);

        if ($query) {
            // code...
            $data = array('info' => 'success',
                'msg' => 'berhasil simpan data');
        } else {
            $data = array('info' => 'error',
                'msg' => 'Gagal simpan data');
        }

        echo json_encode($data);
    }
}
