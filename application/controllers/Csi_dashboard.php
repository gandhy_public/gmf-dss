<?php
defined("BASEPATH") or exit('no direct script access allowed');


/**
 *
 */
class Csi_dashboard extends MY_Controller
{

    public function __construct()
    {
        # code...
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_menu");
        $this->load->model("M_csi_dashboard");
    }

    public function index()
    {
        $data = array(
            "content" => "csi/V_dashboard_csi",
            "title" => "Customer Satisfaction Index",
            "small_tittle" => "",
            "breadcrumb" => [""],
            "menu" => $this->M_menu->tampil(),
            'startPeriode' => date('Y', strtotime('-2 years')),
            'endPeriode' => date('Y')
        );
        $this->load->view("layouts", $data);
    }

    public function get_customer()
    {
        echo "<select class='form-control m-bot15' name='customer' placeholder='customer' id='customer' >";
        echo "<option value='all'>All</option>";

        foreach ($this->M_csi_dashboard->show_customer() as $k => $v) {
            echo "<option value='" . $v['id_customer'] . "'>" . $v['name_customer'] . "</option>";
        }

        echo "</select>";
    }

    public function show_chart_accumulation()
    {
        $y_start = $this->input->post("y_start");
        $y_end = $this->input->post("y_end");
        $s_start = $this->input->post("s_start");
        $s_end = $this->input->post("s_end");
        $cus = $this->input->post("cus");
        $val['start'] = $y_start . "-" . $s_start;
        $val['end'] = $y_end . "-" . $s_end;
        $val['range'] = 'semesterly';
        $val['customer'] = $cus;

        if ($val['customer'] == "all") {
            $query = "";
        } else {
            $query = "AND cf.id_customer = '" . $val['customer'] . "'";
        }

        $post = array();
        $post['finalscore'] = array();
        $cate = array();
        $cate['category'] = array();
        for ($year = $y_start; $year <= $y_end; $year++) {
            for ($i = '01'; $i <= '12'; $i++) {
                if ($i % '06') {
                } else {

                    switch ($i) {
                        case 6:
                            $i = "06";
                            break;
                        case 12:
                            $i = "12";
                    }
                    $date = $year . "-" . $i;

                    $data = $this->M_csi_dashboard->show_chart1_by_all($val, $query, $date);
                    if (empty($data)) {
                        $value = array(0);
                        array_push($post['finalscore'], $value);
                    } else {
                        foreach ($data as $k => $v) {
                            $value = array(floatval(number_format($v->finalscore, 2)));
                            array_push($post['finalscore'], $value);
                        }
                    }
                    switch ($i) {
                        case "06":
                            $is = "1";
                            break;
                        case "12":
                            $is = "2";
                    }

                    $tes = $is . "-" . $year;
                    $cat = array($tes);
                    array_push($cate['category'], $cat);

                }
            }
            $i++;
        }
        $year++;

        $json[] = $post;
        $json1[] = $cate;
        echo json_encode(array("data" => $json, "categ" => $json1));
    }

    public function load_barchart_total_score()
    {
        $dat = $this->input->post('date');
        $dat1 = explode("-", $dat[0]);
        switch ($dat1[0]) {
            case "1":
                $is = "06";
                break;
            case "2":
                $is = "12";
        }

        $val['date'] = $dat1[1] . "-" . $is;
        $val['range'] = 'semesterly';
        $val['cus'] = $this->input->post('cus');
        $post = array();
        $post['total_score'] = array();
        $post2 = array();
        $post2['shortness'] = array();
        if ($val['cus'] == "all") {
            $query = "";
        } else {
            $query = "and id_customer='" . $val['cus'] . "' ";
        }
        $data3 = $this->M_csi_dashboard->get_total_score_all_cus($val, $query);

        foreach ($data3 as $key => $value) {
            # code...
            $valu = array(floatval(number_format($value->total_score, 2)));
            $valu2 = array($value->shortness);
            array_push($post['total_score'], $valu);
            array_push($post2['shortness'], $valu2);
        }

        $json[] = $post;
        $json2[] = $post2;

        echo json_encode(array("data_total" => $json, "data_short" => $json2));

    }

    public function show_chart_key()
    {
        $y_start = $this->input->post("y_start");
        $y_end = $this->input->post("y_end");
        $s_start = $this->input->post("s_start");
        $s_end = $this->input->post("s_end");
        $level = $this->input->post("level");
        $val['customer'] = $this->input->post("cus");
        $val['start'] = $y_start . "-" . $s_start;
        $val['end'] = $y_end . "-" . $s_end;
        $val['range'] = 'semesterly';

        if ($val['customer'] == "all") {
            $query = "AND level_customer='" . $level . "'";
        } else {
            $query = "AND co.id_customer='" . $val['customer'] . "'";
        }

        $cate = array();
        $cate['category'] = array();

        $key = array();
        $key['com_csi'] = array();
        $key2 = array();
        $key2['bqs_csi'] = array();
        $key3 = array();
        $key3['otd_csi'] = array();
        $key4 = array();
        $key4['pca_csi'] = array();
        $key5 = array();
        $key5['ss_csi'] = array();
        $key6 = array();
        $key6['oss_csi'] = array();
        $key7 = array();
        $key7['fm_csi'] = array();

        for ($year = $y_start; $year <= $y_end; $year++) {
            for ($i = '01'; $i <= '12'; $i++) {
                if ($i % '06') {
                } else {

                    switch ($i) {
                        case 6:
                            $i = "06";
                            break;
                        case 12:
                            $i = "12";
                    }
                    $date = $year . "-" . $i;

                    $data = $this->M_csi_dashboard->show_chart2_by($val, $query, $date);
                    if (empty($data)) {

                        $value = array(0);
                        array_push($key['com_csi'], $value);
                        array_push($key2['bqs_csi'], $value);
                        array_push($key3['otd_csi'], $value);
                        array_push($key4['pca_csi'], $value);
                        array_push($key5['ss_csi'], $value);
                        array_push($key6['oss_csi'], $value);
                        array_push($key7['fm_csi'], $value);

                    } else {

                        foreach ($data as $t => $r) {
                            $valuekey = array(floatval(number_format($r->com_csi, 2)));
                            array_push($key['com_csi'], $valuekey);

                            $valuekey2 = array(floatval(number_format($r->bqs_csi, 2)));
                            array_push($key2['bqs_csi'], $valuekey2);

                            $valuekey3 = array(floatval(number_format($r->otd_csi, 2)));
                            array_push($key3['otd_csi'], $valuekey3);

                            $valuekey4 = array(floatval(number_format($r->pca_csi, 2)));
                            array_push($key4['pca_csi'], $valuekey4);

                            $valuekey5 = array(floatval(number_format($r->ss_csi, 2)));
                            array_push($key5['ss_csi'], $valuekey5);

                            $valuekey6 = array(floatval(number_format($r->oss_csi, 2)));
                            array_push($key6['oss_csi'], $valuekey6);

                            $valuekey7 = array(floatval(number_format($r->fm_csi, 2)));
                            array_push($key7['fm_csi'], $valuekey7);
                            //
                            // $valcat2 = array($r->date_type);
                            // array_push($cat2_val['date_type'], $valcat2);

                        }

                    }
                    switch ($i) {
                        case "06":
                            $is = "1";
                            break;
                        case "12":
                            $is = "2";
                    }

                    $tes = $is . "-" . $year;
                    $cat = array($tes);
                    array_push($cate['category'], $cat);

                }
            }
            $i++;
        }
        $year++;

        $jsonkey[] = $key;
        $jsonkey2[] = $key2;
        $jsonkey3[] = $key3;
        $jsonkey4[] = $key4;
        $jsonkey5[] = $key5;
        $jsonkey6[] = $key6;
        $jsonkey7[] = $key7;
        $json1[] = $cate;
        echo json_encode(array("categ" => $json1, "chart2_com" => $jsonkey,
            "chart2_bqs" => $jsonkey2, "chart2_otd" => $jsonkey3,
            "chart2_pca" => $jsonkey4, "chart2_ss" => $jsonkey5,
            "chart2_oss" => $jsonkey6, "chart2_fm" => $jsonkey7));
    }

    public function show_chart_bisnis()
    {
        $y_start = $this->input->post("y_start");
        $y_end = $this->input->post("y_end");
        $s_start = $this->input->post("s_start");
        $s_end = $this->input->post("s_end");
        $cus = $this->input->post("cus");
        $val['start'] = $y_start . "-" . $s_start;
        $val['end'] = $y_end . "-" . $s_end;
        $val['range'] = 'semesterly';
        $val['customer'] = $cus;

        $bus = array();
        $bus['avg_bus'] = array();
        $sus = array();
        $sus['avg_sus'] = array();
        $cate = array();
        $cate['category'] = array();

        if($val['customer'] == "all"){
          $query = "";
        }else{
          $query = "AND cf.id_customer='".$val['customer']."'";
        }

            for ($year = $y_start; $year <= $y_end; $year++) {
                for ($i = '01'; $i <= '12'; $i++) {
                    if ($i % '06') {
                    } else {

                        switch ($i) {
                            case 6:
                                $i = "06";
                                break;
                            case 12:
                                $i = "12";
                        }
                        $date = $year . "-" . $i;

                        $data = $this->M_csi_dashboard->show_chart3_all($val, $date, $query);
                        if (empty($data)) {
                            $value = array(0);
                            array_push($bus['avg_bus'], $value);
                            array_push($sus['avg_sus'], $value);
                        } else {
                            foreach ($data as $w => $y) {
                                $value6 = array(floatval(number_format($y->avg_bus, 2)));
                                array_push($bus['avg_bus'], $value6);

                                $value9 = array(floatval(number_format($y->avg_sus, 2)));
                                array_push($sus['avg_sus'], $value9);
                            }
                        }
                        switch ($i) {
                            case "06":
                                $is = "1";
                                break;
                            case "12":
                                $is = "2";
                        }

                        $tes = $is . "-" . $year;
                        $cat = array($tes);
                        array_push($cate['category'], $cat);

                    }
                }
                $i++;
            }
            $year++;

        $json[] = $bus;
        $json2[] = $sus;
        $json1[] = $cate;
        echo json_encode(array("data_bus" => $json, "data_sus" => $json2, "categ" => $json1));
    }

}
