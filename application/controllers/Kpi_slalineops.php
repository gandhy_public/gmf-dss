<?php
defined('BASEPATH') or exit ('no direct script access allowed');

/**
 *
 */
class Kpi_slalineops extends CI_Controller
{

  function __construct()
  {
    # code...
    parent::__construct();
    $this->load->database();
    $this->load->helper("url");
    $this->load->helper("encript");
    $this->load->helper("kpiformula_helper");
    $this->load->model("M_menu");
    $this->load->model("M_slaline");
    $this->load->model("M_kpi_sla_master");
    $this->load->model("M_slabaseops");
  
  }

  public function index()
  {
    # code...
    $data = array(
      "content" => "kpi/kpi_slalineops",
      "title" => "KPI SLA LINE OPS",
      "small_tittle" => "",
      "breadcrumb" => [""],
      "menu" => $this->M_menu->tampil()
    );
    $this->load->view("layouts", $data);
  }
  
  
  
 public function grafik_parent()
    {
    $start= $this->input->post("start");
    $start_tanggal = explode('-', $start);
    $start_tanggal = floatval($start_tanggal[0]);
    $start_tahun = explode('-', $start);
    $start_tahun = floatval($start_tahun[1]);
    $end= $this->input->post("end") ;
    $end_tanggal = explode('-', $end);
    $end_tanggal = floatval($end_tanggal[0]);

    $aircraft= $this->input->post("aircraft") ;
    $tipe= $this->input->post("tipe") ;
    
    if($aircraft=='garuda'){
    $aircraft = 'SLA GA';
    $depth = '1';
    }else if($aircraft=='citilink'){
    $aircraft = 'SLA QG';
    $depth = '1';
    }else if($aircraft=='general'){
    $aircraft = 'SLA Line Operations';
    $depth = '2';
    }
  
  $get_idparent = $this->M_slaline->get_idparent($aircraft, $start_tahun);
  
  $data['data'] = array();
  $data['datat'] = array();
  $data['datag'] = array();
  
  for($i=$start_tanggal;$i<=$end_tanggal;$i++){
      $bulan = str_pad($i,2,"0",STR_PAD_LEFT);
      // $valuet = date('F', strtotime($start_tahun.'-'.$bulan.'-21'));
      $valuet = date('M-Y', strtotime($start_tahun.'-'.$bulan.'-21'));
    
    // if($depth=='2'){
      
    //   $data_temp_id = $this->M_kpi_sla_master->getTotalScoreNotLevelOneWhere($get_idparent['ki_id'], $bulan, $start_tahun, $depth);
    //     $totalScore = 0;
    //     foreach ($data_temp_id as $temp_id) {
    //     $newTotal = $this->M_kpi_sla_master->getTotalScoreWhere($temp_id["ki_id"], $bulan, $start_tahun, $depth);
    //     $totalScore += number_format(($newTotal*$temp_id["weight"])/100, 2);
    //     }
    // }else{
      
    //   $totalScore = $this->M_kpi_sla_master->getTotalScoreWhere($get_idparent['ki_id'], $bulan, $start_tahun, $depth);
    //   $weight = $this->M_kpi_sla_master->countWeight($get_idparent['ki_id'], $start_tahun, $bulan);
    //   $parentScore = number_format(($totalScore*$weight)/100, 2);
    // }
      $kpi_item = $this->M_slabaseops->get_by_name($aircraft);
      $maxLevel = $this->M_kpi_sla_master->maxLevel($kpi_item->ki_id);
      $finalTotalScores=$this->FunctionName($maxLevel,$kpi_item->ki_id,$i,$start_tahun);  
    
      $actual =(int)number_format($finalTotalScores['score'],2);
      $target = array(floatval(100));
      array_push($data['data'], $actual);
      array_push($data['datat'], $valuet);
      array_push($data['datag'], $target);  
  }
  
  
  
  
      // $code_child = $this->M_slaline->get_code_child($aircraft, $tipe);
    // $temp_child = array();
     // foreach ($code_child as $key => $val) {
       // $reg = $val['kia_id'];
      // $temp_child[]= $reg;
     // }  
     // $child= "'".join("','" ,$temp_child)."'";
     
   
     // if($tipe=='general'){
       // $start = '01-'.date('Y');
       // $end = date('m-Y');
     
      // $code_child = $this->M_slaline->get_code_child('SLA GA', '');
    // $temp_child = array();
     // foreach ($code_child as $key => $val) {
       // $reg = $val['kia_id'];
      // $temp_child[]= $reg;
     // }  
     // $child= "'".join("','" ,$temp_child)."'";
     // $get_grafik_ga = $this->M_slaline->get_grafik_parent($child, $start, $end);
     
     
      // $code_child = $this->M_slaline->get_code_child('SLA QG', '');
    // $temp_child = array();
     // foreach ($code_child as $key => $val) {
       // $reg = $val['kia_id'];
      // $temp_child[]= $reg;
     // }  
     // $child= "'".join("','" ,$temp_child)."'";
     
     
    // $data['data'] = array();
    // $data['datat'] = array();
    // $data['datag'] = array();
    
    
    // foreach ($get_grafik_ga as $gv_key => $gv_value) { 
       // $get = $this->M_slaline->get_grafik_parent_two($child, $gv_value['kiad_date']);
       // $tanggal =  $gv_value['kiad_date'];
        // $valuet =  date('F', strtotime($tanggal));
        // $actual = floatval($gv_value['rata_actual']+$get['rata_actual'])/2;
        // $actual = array(floatval(is_null($actual)?0:number_format($actual, 2)));
        // $target = array(floatval(100));
        // array_push($data['data'], $actual);
        // array_push($data['datat'], $valuet);
        // array_push($data['datag'], $target); 
       
    // }
     
     // }else{
     
       // $get_grafik = $this->M_slaline->get_grafik_parent($child, $start, $end);
      

      // $data['data'] = array();
      // $data['datat'] = array();
      // $data['datag'] = array();
      
      
      // foreach ($get_grafik as $gv_key => $gv_value) { 
        // $tanggal =  $gv_value['kiad_date'];
        // $valuet =  date('F', strtotime($tanggal));
        // $actual = array(floatval(is_null($gv_value['rata_actual'])?0:number_format($gv_value['rata_actual'], 2)));
        // $target = array(floatval(100));
        // array_push($data['data'], $actual);
        // array_push($data['datat'], $valuet);
        // array_push($data['datag'], $target);
        
      // }
      
   // }
     
    $json[] = $data;
    echo json_encode(
        array(
            'grafik' => $json,
        )
      );
    }
  
  
 public function grafik_tot_detail()
    {
      $start= $this->input->post("start") ;
    $start_tahun = explode('-', $start);
    $start_tahun = floatval($start_tahun[1]);
      $end= $this->input->post("end") ;
      $aircraft= $this->input->post("aircraft") ;
      $tipe= $this->input->post("tipe") ;
    
      // echo json_encode($aircraft);exit;
    if($aircraft=='garuda'){
      $aircraft = 'SLA GA';
    }else{
       $aircraft = 'SLA QG';
    }
     $get_grafik = $this->M_slaline->count_child($aircraft, $start_tahun);
     $count = count($get_grafik);
     // echo $count;
     $data =  array();
    $json_builder = array();
   if($count!=0){
     foreach ($get_grafik as $key => $gv_value) {
       $data['name'] = $gv_value['ki_name'];
       $data['ki_id'] = $gv_value['ki_id'];
       $data['kia_id'] = $gv_value['kia_id'];
       $json_builder[] = $data;
     }
   }else{
     $json_builder = '';
   }
     
    echo json_encode(
      array(
        'grafik' => $json_builder,
        'count' => $count,
      )
      );
  }


/* 
Start
Edit By DIMAS ISLAMI
*/  

  function cek_tipe_formula($ki_id){
    $ceking = $this->M_slaline->cek_tipe_formula($ki_id);
    echo json_encode($ceking);
  }
  
  
  function grafik_detail(){
    $start    = $this->input->post("start") ;
    $end      = $this->input->post("end") ;
    $kia_id   = $this->input->post("kia_id") ;
    $ki_id    = $this->input->post("ki_id") ;
    $tipe_for = $this->input->post("tipe");
     
     $get_performance = $this->M_slaline->get_grafik_detail_performance($kia_id, $start, $end);
     $get_target      = $this->M_slaline->get_grafik_detail_target($ki_id, $start, $end);
     $ceking          = $this->M_slaline->cek_tipe_formula($ki_id);
     $tipe            = $ceking['ki_uom'];


    $title  = array('Upper', 'Lower', 'Actual');
    $title1 = array('Target', 'Actual');

    // Stabilize
    if($tipe_for == 0){
      $arr_act    = [];
      $arr_date   = [];
      $arr_low    = [];
      $arr_upp    = [];
      foreach($title as $key1 => $gv_val){
        if($gv_val =='Actual'){
          foreach ($get_performance as $gv_key => $gv_value) { 
            $max = $gv_value['kiad_actual'];
            if($max >= $gv_value['kiad_actual']){
              $max   = $gv_value['kiad_actual'];
            } 
             $actual    = array(floatval(is_null($gv_value['kiad_actual'])?0:$gv_value['kiad_actual']));
             $arr_act[] = $actual;            
          }
        }else{
          foreach ($get_target as $gv_key2 => $gv_value) { 
            $tanggal    = $gv_value['kid_date'];
            $valuet     = date('M-Y', strtotime($tanggal));
            $arr_date[] = $valuet;
            $pecah      = explode('|', $gv_value['kid_target']);
            $upper      = array(floatval(is_null($pecah[0])?0:$pecah[0]));
            $lower      = array(floatval(is_null($pecah[1])?0:$pecah[1]));
            if($gv_val=='Upper'){
             $arr_upp[] = $upper;
            }else{
             $arr_low[] = $lower;
            }
          }
        }      
      }
      echo json_encode(array('name' => $gv_val, 'date' => $arr_date, 'lower' => $arr_low, 'upper' => $arr_upp, 'actual' => $arr_act, 'y_tittle' => $tipe));
    }
    // Selain Stabilize
    else{
      $arr_act    = [];
      $arr_date   = [];
      $arr_target = [];
      foreach($title1 as $key1 => $gv_val){
        if($gv_val=='Actual'){
          foreach ($get_performance as $gv_key => $gv_value) { 
            $max = $gv_value['kiad_actual'];
            if($max >= $gv_value['kiad_actual']){
              $max  = $gv_value['kiad_actual'];
            } 
             $actual    = array(floatval(is_null($gv_value['kiad_actual'])?0:$gv_value['kiad_actual']));
             $arr_act[] =  $actual;
          }
        }else{       
          foreach ($get_target as $gv_key2 => $gv_value) { 
            $tanggal      =  $gv_value['kid_date'];
            $valuet       =  date('M-Y', strtotime($tanggal));
            $performance  = array(floatval(is_null($gv_value['kid_target'])?0:$gv_value['kid_target']));
            $arr_date[]   = $valuet;
            $arr_target[] = $performance;
          }
        }
      }
      echo json_encode(array('date' => $arr_date, 'target' => $arr_target, 'actual' => $arr_act, 'y_tittle' => $tipe));
    }
  }
/* 
End
Edit by dimas mbul
*/  

public function FunctionName($maxLevel,$ki_id,$mon,$year)
    {
        # code...
                    // $finalTotalScore=0;
                    $arrayTemp=array();
                    $parentArr=array();
                    for ($k=$maxLevel; $k >=1 ; $k--) { 
                        # code...

                        $items = $this->M_kpi_sla_master->getPenghuni($k,$ki_id);                        
                        $parent = $this->M_kpi_sla_master->getGroupLevelParent($k,$ki_id);
                       
                       
                        if($k==$maxLevel){
                            
                             foreach ($parent as $key ) {
                                # code...
                                $tempTotalYtd=0;
                                $tempTotal=0;
                                for ($p=0; $p < count($items) ; $p++) { 
                                    # code...
                                    if($items[$p]['ki_parent_id']==$key['ki_parent_id']){
                                       $dataM = $this->M_slabaseops->listActualWhere($items[$p]['ki_id'],$mon,$year);
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                           $tempTotal+=number_format(score(achievment($dataM[$z]->kid_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2);
                                           $tempTotalYtd+=number_format(score(achievment($dataM[$z]->kid_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2) ;
                                       }
                                    }
                                }
                                $arrayTemp[$key['ki_parent_id']] = $tempTotal;
                                $parentArr[$key['ki_parent_id']] = $tempTotalYtd;
                            }

                        }
                        else{
                            $tempFuck=0;
                            $tempFuckYtd=0;

                            foreach ($parent as $keys ) {
                                # code...
                                for ($q=0; $q < count($items) ; $q++) { 
                                    if (array_key_exists($items[$q]['ki_id'],$arrayTemp) && $items[$q]['ki_parent_id']==$keys['ki_parent_id']){
                                        $tempFuck+=$arrayTemp[$items[$q]['ki_id']]*($items[$q]['ki_weight']/100);
                                        $tempFuckYtd+=$parentArr[$items[$q]['ki_id']]*($items[$q]['ki_weight']/100);
                                    }elseif( $items[$q]['ki_parent_id']==$keys['ki_parent_id']){
                                       $dataM = $this->M_slabaseops->listActualWhere($items[$q]['ki_id'],$mon,$year);
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                           $tempFuck+=number_format(score(achievment($dataM[$z]->kid_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2);
                                           $tempFuckYtd+=number_format(score(achievment($dataM[$z]->kid_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2) ;
                                       }
                                    }
                                }
                               
                                $arrayTemp[$keys['ki_parent_id']] = $tempFuck;
                                $parentArr[$keys['ki_parent_id']] = $tempFuckYtd;
                                // $parentArr[$key['ki_parent_id']] = $tempTotal*$items[$q]['ki_id'];
                                $tempFuck=0;
                                $tempFuckYtd=0;
                                
                            }

                            

                        }
                        
                    }

                    return $finalTotalScore= array('score'=>$arrayTemp[$ki_id],'scoreYtd'=>$parentArr[$ki_id]);
    }

}
