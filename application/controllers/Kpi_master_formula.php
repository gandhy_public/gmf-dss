<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  ini_set('max_execution_time', 0);
  ini_set('memory_limit', '-1');

  class Kpi_master_formula extends MY_Controller
  {

        public function __construct()
        {
          parent::__construct();
          $this->load->helper("url");
          $this->load->helper("date");
          $this->load->helper("hierarchyitem_helper");
          $this->load->model("M_kpi_formula");
        }

        public function load_helper_formula()
        {
          $result = htmlentities(file_get_contents(APPPATH."helpers/kpiformula_helper.php"));
          return $result;
        }

        public function index()
        {
          $data = array(
              "content" => "kpi/master_formula",
              "title" => "KPI Master Formula",
              "small_tittle" => "(Coorporate)",
              "breadcrumb" => ["KPI", "Master Formula"],
              "load_file" => $this->load_helper_formula(),
              "menu" => $this->M_menu->tampil()
          );
          // echo json_encode($data["allItem"]);
          // exit();
          $this->load->view("layouts", $data);
        }

        public function file()
        {
          $code = $this->input->post("code");
          $kf_name = $this->input->post("kf_name");
          $fa_function_name = $this->input->post("fa_function_name");

          $open = fopen(APPPATH."helpers/kpiformula_helper.php","w+");
          fwrite($open, $code);
          fclose($open);
          $result = array();
          try{
            if($this->load->helper("kpiformula") != true){
              throw new Exception("Please check your PHP code !!");
            } else {
              $save = $this->M_kpi_formula->saveFormula($kf_name, $fa_function_name);
              if($save){
                $result["success"] = true;
                $result["notif"] = null;
              } else {
                $result["success"] = false;
                $result["notif"] = "Something Wrong";
              }
            }
          } catch(Exception $e){
            $result["success"] = false;
            $result["notif"] = $e->getMessage();
          }
          echo json_encode($result);
        }
  }
