<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Repetitive extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("M_menu");
        $this->load->model("M_repetitive");
        $this->load->model("M_hil");
    }

    function index()
    {
        $data = array(
            "content" => "repetitive/v_repetitive",
            "title" => "Repetitive Problem Closing Rate",
            "titleGrafik" => "General Chart",
            "small_tittle" => "",
            "breadcrumb" => ["Repetitive Problem Closing Rate"],
            "menu" => $this->M_menu->tampil(),
            'countGaruda' => count($this->M_repetitive->getDataGaruda()),
            'countCitilink' => count($citilinks = $this->M_repetitive->getDataCitilink()),
            'startPeriode' => '01-' . date('Y'),
            'endPeriode' => date('m-Y'),
        );
        $this->load->view("layouts", $data);
    }


    function master_manufacture()
    {
        $query = $this->M_hil->master_manufacture();
        echo json_encode($query);
    }

    function master_type($id)
    {
        $query = $this->M_hil->master_reg($id);
        echo json_encode($query);
    }

    public function actualGaruda()
    {
        // $allDatas = $this->M_repetitive->getAllData('GA');
        // $garudas = $this->M_repetitive->getDataGarudaPeriode();

        // $actualGaruda = count($garudas) / count($allDatas);
        $year=date('Y');
        $datas=$this->M_repetitive->getNewActual($year);
        if(count($datas)!=0){
            $sum=0;
            $loop=0;
            foreach ($datas as $key) {
                # code...
                if($key['OWN']=="GA"){
                    $sum+=$key['ratio'];
                    $loop++;
                }
            }
            $actualGaruda=$sum/$loop;
        }else{
            $actualGaruda=0;
        }

        return number_format($actualGaruda, 2);

    }

    public function actualCitilink()
    {
        // $allDatas = $this->M_repetitive->getAllData('Citilink');
        // $citilinks = $this->M_repetitive->getDataCitilinkPeriode();

        // $actualCitilink = count($citilinks) / count($allDatas);
        $year=date('Y');
        $datas=$this->M_repetitive->getNewActual($year);
        if(count($datas)!=0){
            $sum=0;
            $loop=0;
            foreach ($datas as $key) {
                # code...
                if($key['OWN']=="CITILINK"){
                    $sum+=$key['ratio'];
                    $loop++;
                }
            }
            $actualCitilink=$sum/$loop;
        }else{
            $actualCitilink=0;
        }

        return number_format($actualCitilink, 2);

    }

    function targetActual()
    {

        $value = array();
        $value_target = number_format($this->M_repetitive->getTarget(), 2);
        // $value_garuda = $this->actualGaruda();
        // $value_citilink = $this->actualCitilink();

        $year=date('Y');
        $datas=$this->M_repetitive->getNewActual($year);
        if(count($datas)!=0){
            $sum_ga=0;
            $sum_ct=0;
            $loop_ga=0;
            $loop_ct=0;
            foreach ($datas as $key) {
                # code...
                if($key['OWN']=="GA"){
                    $sum_ga+=$key['ratio'];
                    $loop_ga++;
                }elseif($key['OWN']=="CITILINK"){
                    $sum_ct+=$key['ratio'];
                    $loop_ct++;
                }
            }
            $actualGA=$sum_ga/$loop_ga;
            $actualCitilink=$sum_ct/$loop_ct;
        }else{
            $actualCitilink=0;
            $actualGA=0;
        }

        $value = array('target' => $value_target, 'garuda' => number_format($actualGA,2), 'citilink' => number_format($actualCitilink,2));
        array_push($value, $value);
        echo json_encode($value);
    }

    public function tableGaruda()
    {

        $columns = array(
            0 => 'id_reptitive',
            1 => 'notification',
            2 => 'actype',
            3 => 'functional_location',
            4 => 'description',
            5 => 'create_on',
            6 => 'req_start',
            7 => 'system_status'
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search,
            'order_column' => $order_column,
            'order_dir' => $order_dir,
        );

        $send = $this->M_repetitive->showDatatable($data, 'Garuda');

        echo $send;

    }

    public function tableCitilink()
    {
        $columns = array(
            0 => 'id_reptitive',
            1 => 'notification',
            2 => 'actype',
            3 => 'functional_location',
            4 => 'description',
            5 => 'create_on',
            6 => 'req_start',
            7 => 'system_status'
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search,
            'order_column' => $order_column,
            'order_dir' => $order_dir,
        );

        $send = $this->M_repetitive->showDatatable($data, 'Citilink');

        echo $send;

    }

    public function getDataChartTrend()
    {

        if($this->input->post('list_manufacture') == 00){
            $list_manufacture = '';
        }else{
            $list_manufacture = $this->input->post('list_manufacture');
        }
        if($this->input->post('list_aircraft_type') == 00){
            $list_aircraft_type = '';
        }else{
            $list_aircraft_type = $this->input->post('list_aircraft_type');
        }
        $input = array(
            'list_aircraft_type' => $list_aircraft_type,
            'list_manufacture' => $list_manufacture,
            'startPeriode' => date('Y-m-d', strtotime('01-' . $this->input->post('startPeriode'))),
            'endPeriode' => date('Y-m-d', strtotime('01-' . $this->input->post('endPeriode'))),
        );

        $categories = $this->M_repetitive->getNameMonth($input);

        $colors = array('#05354D', '#8EC3A7');

        //id GA = 12
        //id CITILINK = 11

        if ($input['list_manufacture'] == '12') {
            $garudas = $this->M_repetitive->getDataChart('Garuda', $input);
            $citilinks = null;
        } elseif ($input['list_manufacture'] == '11') {
            $garudas = null;
            $citilinks = $this->M_repetitive->getDataChart('Citilink', $input);
        } else {
            $garudas = $this->M_repetitive->getDataChart('Garuda', $input);
            $citilinks = $this->M_repetitive->getDataChart('Citilink', $input);
        }

        $result = array('categories' => $categories, 'data_garuda' => $garudas, 'data_citilink' => $citilinks);

        echo json_encode($result);
    }

    public function detailTrendData()
    {
        $input = array(
            'list_aircraft_type' => $this->input->post('list_aircraft_type'),
        );

        $aircraft = $this->input->post('aircraft');
        if ($this->input->post('month') == 'Feb') {
            $month = '02';
        } else {
            $month = date('m', strtotime($this->input->post('month')));
        }

        $columns = array(
            0 => 'id_reptitive',
            1 => 'notification',
            2 => 'actype',
            3 => 'functional_location',
            4 => 'description',
            5 => 'create_on',
            6 => 'req_start',
            7 => 'system_status'
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search,
            'order_column' => $order_column,
            'order_dir' => $order_dir,
        );

        $send = $this->M_repetitive->showDatatableDetail($data, $aircraft, $month, date('Y'), $input);

        echo $send;

    }

    public function changeColorGaruda()
    {
        if ($this->actualGaruda() >= $this->M_repetitive->getTarget()) {
            $color = '#F24738';
        } else {
            $color = '#8EC3A7';
        }

        return $color;
    }

    public function changeColorCitilink()
    {
        if ($this->actualCitilink() >= $this->M_repetitive->getTarget()) {
            $color = '#F24738';
        } else {
            $color = '#8EC3A7';
        }

        return $color;
    }


    public function viewMitigation()
    {
        $notification = $this->input->post('id');

        $data = $this->M_repetitive->getMitigation($notification);
        if (count($data)!=0) {
            # code...
            foreach ($data as $key) {
                $why = $key['mitigasi_why'];
                $solusi = $key['mitigasi_solution'];
            }
        }else{
            $why="";
            $solusi="";
        }

        
        echo json_encode(array('why' => $why, 'solusi' => $solusi));
    }
}
