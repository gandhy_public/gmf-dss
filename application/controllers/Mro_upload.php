<?php
  require_once(APPPATH.'libraries/PHPExcel.php');
  defined('BASEPATH') OR exit('No direct script access allowed');
 

  class Mro_upload extends MY_Controller{

    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->helper("url");
      $this->load->helper("encript");
      $this->load->model("M_mro");
      $this->load->model("M_mro_history");
      $this->load->model("M_mro_operator");
      $this->load->model("M_mro_enginefamily");
      $this->load->model("M_mro_expensecategory");
      $this->load->model("M_mro_region");
      $this->load->model("M_mro_country");

      $this->load->helper("file");
      ini_set('upload_max_filesize', '10M');
      ini_set('memory_limit', '-1'); // or you could use 1G
      ini_set('max_execution_time', 0);
      ob_implicit_flush(true);
    }

    function index(){
      phpinfo();
    }

    function market(){
      $data = array(
        "content"       => "mro/mro-market",
        "title"         => "MRO Market",
        "small_tittle"  => "",
        "breadcrumb"    => ["MRO Market"],
        "menu"          => $this->M_menu->tampil(),
      );
      $this->load->view("layouts", $data);
    }

    function upload(){
      $data = array(
        "content"       => "mro/upload-data",
        "title"         => "MRO Upload",
        "small_tittle"  => "",
        "breadcrumb"    => ["Upload Data"],
        "menu"          => $this->M_menu->tampil(),
      );
      $this->load->view("layouts", $data);
    }

    //===========DATATABLE============\\
    function list_mro(){
      $list = $this->M_mro->get_datatables_mro();
      $records = $this->M_mro->count_filtered_mro();
      $data = array();
      $no   = $_POST['start'];
      foreach ($list as $mro) {
        $no++;
        $row   = array();
        $row[] = '<center>'.$no.'</center>';
        $row[] = $mro->year;
        $row[] = $mro->nm_operator;
        $row[] = '<center>'.$mro->aircraft_type.'</center>';
        $row[] = '<center>'.$mro->nm_engine.'</center>';
        $row[] = $mro->expense_type;
        $row[] = 'USD '.number_format($mro->total_cost, 2);
        $data[] = $row;
      }
      $output = array(
          "draw"            => $_POST['draw'],
          "recordsTotal"    => $records,
          // "recordsTotal"    => $this->M_mro->count_all_mro(),
          "recordsFiltered" => $records,
          "data"            => $data,
      );
      echo json_encode($output);
    }

    function list_history_upload(){
      $list = $this->M_mro_history->get_datatables();
      $data = array();
      $no   = $_POST['start'];
      foreach ($list as $mro) {
        $no++;
        $row   = array();
        $row[] = $no;
        $row[] = $mro->date_upload;
        $row[] = $mro->name_file;

        $row[] = '<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Delete" onclick="confirm_delete('.$mro->id_mro_history_upload.','."'".$mro->date_upload."'".')"><i class="fa fa-trash"></i></a>';

        $data[] = $row;
      }
      $output = array(
          "draw"            => $_POST['draw'],
          "recordsTotal"    => $this->M_mro_history->count_all(),
          "recordsFiltered" => $this->M_mro_history->count_filtered(),
          "data"            => $data,
      );
      echo json_encode($output);
    }

    function delete_history($id){

      if ($this->M_mro->delete($id)) {
        $dataOut['sukses'] = true;
        $dataOut['message'] = "Sukses Delete File";
        $this->M_mro_history->delete($id);
      }else{
        $dataOut['sukses'] = false;
        $dataOut['message'] = "Delete File Gagal!!";
      }

      echo json_encode($dataOut);

    }

    public function uploadFile(){
        $this->load->library('PHPExcel');

        $path = 'assets/upload-mro';

        $new_name_file=date("Ymd_His").'_'.$_FILES['files']['name'];
        $inputFileName="$path/$new_name_file";
        move_uploaded_file($_FILES['files']['tmp_name'], $inputFileName);

        // $config['upload_path'] = $path;
        // $config['allowed_types'] = '*';
        // $config['remove_spaces'] = TRUE;
        // $config['encrypt_name'] = TRUE;
        // // $config['max_size']      = 50;

        // $this->load->library('upload', $config);
        // $this->upload->initialize($config);

        // // $this->load->library('upload', $config);
        // // $new_name_file=date("Ymd_His").'_'.$_FILES(['files']['name']);
        $name_file_up = $_FILES['files']['name'];
        // if ($this->upload->do_upload('files')) {
        //     $data = array('upload_data' => $this->upload->data());
        // } else {
        //     $error = array('error' => $this->upload->display_errors());
        //     print_r($error);
        //     exit();
        // }
        // // echo "Data: ".json_encode($data)."\n";
        // if (!empty($data['upload_data']['file_name'])) {
        //     $import_xls_file = $data['upload_data']['file_name'];
        // } else {
        //     $import_xls_file = 0;
        // }
        // // echo "File Name: ".$import_xls_file."\n";
        // $inputFileName = $path . $import_xls_file;
        // echo "Path File Name:".$inputFileName."\n";z

            $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
            $cacheSettings = array( ' memoryCacheSize ' => '8MB');
            PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
// echo 'Loading file ',pathinfo($inputFileName,PATHINFO_BASENAME),' using IOFactory with a defined reader type of ',$inputFileType,'<br />';
/**  Create a new Reader of the type defined in $inputFileType  **/
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
// echo '<hr />';
/**  Define how many rows we want for each "chunk"  **/
$chunkSize = 120001;
/**  Loop to read our worksheet in "chunk size" blocks  **/
$allDataInSheet = array();
// $allDataInSheet2 = array();
for ($startRow = 2; $startRow <= 50000; $startRow += $chunkSize) {
  // echo 'Loading WorkSheet using configurable filter for headings row 1 and for rows ',$startRow,' to ',($startRow+$chunkSize-1),'<br />';
  /**  Create a new Instance of our Read Filter, passing in the limits on which rows we want to read  **/
  $chunkFilter = new chunkReadFilter($startRow,$chunkSize);
  /**  Tell the Reader that we want to use the new Read Filter that we've just Instantiated  **/
  $objReader->setReadFilter($chunkFilter);
  $objReader->setReadDataOnly(true);
  /**  Load only the rows that match our filter from $inputFileName to a PHPExcel Object  **/
  $objPHPExcel = $objReader->load($inputFileName);
  $worksheet = $objPHPExcel->getActiveSheet();

  $highestRow = $worksheet->getHighestRow('A');
  // $highestColumn = $worksheet->getHighestColumn();
  //  Do some processing here
  // $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
  // var_dump($sheetData);
  // echo '<br /><br />';
  // echo "startRow: ".$startRow."\n";
  // echo "highestRow: ".$highestRow."\n";
  // echo "startRow-chunkSize: ".($startRow+$chunkSize-1);

  if ($highestRow<=1) {
    // echo "\nbreak\n";
    break;
  }else{
    $allDataInSheet[0] = $worksheet->toArray(null, true, true, true);
    // $allDataInSheet[0] = $worksheet->toArray(
    //     'A'.$startRow.':'.$highestColumn.$highestRow,
    //     null,
    //     true,
    //     true,
    //     true
    // );
    // array_push($allDataInSheet, $dataInSheet);
    // array_push($allDataInSheet, $dataInSheet[0]);
  }
  // if ($highestRow==31) {
  //   break;
  // }

  // if (count($allDataInSheet[0]) % 1000 != 1) {
  //   break;
  // }

  // $allDataSheet[] = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

  // print_r($allDataSheet);
  // if (count($allDataSheet) % 1000 != 1) {
  //   break;
  // }else{
  //   $allDataInSheet[0] = $allDataSheet;
  // }
  // echo "count: ".count($allDataSheet);

  // echo "\ncount: ".count($allDataInSheet)."\n";
  //    Free up some of the memory 
  $objPHPExcel->disconnectWorksheets(); 
  unset($objPHPExcel); 
}
  // echo "countAll: ".count($allDataInSheet);
  // echo "<pre>";
  // print_r($allDataInSheet);
  // echo "</pre>";
  // echo "<pre>";
  // print_r($allDataInSheet2);
  // echo "</pre>";
// exit();

//         try {
//             $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
//             $cacheSettings = array( ' memoryCacheSize ' => '8MB');
//             PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
//             $inputFileType = PHPExcel_IOFactory::identify($inputFileName);

//             /**  Create a new Reader of the type defined in $inputFileType  **/
//             $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//             /**  Define how many rows we want to read for each "chunk"  **/ 
//             $chunkSize = 20;
//             /**  Create a new Instance of our Read Filter  **/ 
//             $chunkFilter = new chunkReadFilter(); 
//             /**  Tell the Reader that we want to use the Read Filter that we've Instantiated  **/ 
//             $objReader->setReadFilter($chunkFilter); 

//             /**  Loop to read our worksheet in "chunk size" blocks  **/ 
//             /**  $startRow is set to 2 initially because we always read the headings in row #1  **/
//             for ($startRow = 2; $startRow <= 1000; $startRow += $chunkSize) { 
//                 *  Tell the Read Filter, the limits on which rows we want to read this iteration  * 
//                 $chunkFilter->setRows($startRow,$chunkSize); 
//                 /**  Load only the rows that match our filter from $inputFileName to a PHPExcel Object  **/ 
//                 $objPHPExcel = $objReader->load($inputFileName); 
//                 //    Do some processing here 
//                 echo $startRow;
//                 //    Free up some of the memory 
//                 $objPHPExcel->disconnectWorksheets(); 
//                 unset($objPHPExcel); 
//             }

//             // $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//             // $objReader->setReadDataOnly(true);
//             // $objReader->setReadDataOnly(true);
//             // $objPHPExcel = $objReader->load($inputFileName);
            $dataOut['sukses'] = true;
//         } catch (Exception $e) {
//             if (!empty($inputFileName)) {
//               unlink($inputFileName);
//             }
//             $dataOut['sukses'] = false;
//             $dataOut['message'] = "Error Read Data";
//             echo json_encode($dataOut);
//             die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
//                     . '": ' . $e->getMessage());
//         }
        // $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            // echo "<pre>";
            // print_r($allDataInSheet[0]);
            // echo "</pre>";
            // exit();

        $arrayCount = count($allDataInSheet[0]);
        if($arrayCount<=120001){
            
        
        $flag = 0;
        // echo "arrayCount: ".$arrayCount;

        $createArray = array('Year', 'Operator', 'Region', 'Country', 'Aircraft Role', 'Aircraft Category', 'Manufacturer', 'Aircraft Family', 'Aircraft Group', 'Aircraft Type', 'Propulsion', 'Engine Manufacturer', 'Engine Family', 'Expense Category', 'Expense Type', 'Total Cost', 'Events', 'Material Spend', 'Labor Hours', 'Labor Spend');

        $SheetDataKey = array();
        foreach ($allDataInSheet[0] as $dataInSheet) {
            foreach ($dataInSheet as $key => $value) {
              // echo 'val: ' .$value ."\n";
                if (in_array(trim($value), $createArray)) {
                    // $value = preg_replace('/\s+/', '', $value);
                    $SheetDataKey[trim($value)] = $key;
                } else {

                }
            }
        }

        $makeArray = array('Year' => 'Year', 'Operator' => 'Operator', 'Region' => 'Region', 'Country' => 'Country', 'Aircraft Role' => 'Aircraft Role', 'Aircraft Category' => 'Aircraft Category', 'Manufacturer' => 'Manufacturer', 'Aircraft Family' => 'Aircraft Family', 'Aircraft Group' => 'Aircraft Group', 'Aircraft Type' => 'Aircraft Type', 'Propulsion' => 'Propulsion', 'Engine Manufacturer' => 'Engine Manufacturer', 'Engine Family' => 'Engine Family', 'Expense Category' => 'Expense Category', 'Expense Type' => 'Expense Type', 'Total Cost' => 'Total Cost', 'Events' => 'Events', 'Material Spend' => 'Material Spend', 'Labor Hours' => 'Labor Hours', 'Labor Spend' => 'Labor Spend');


        $data = array_diff_key($makeArray, $SheetDataKey);

        if (empty($data)) {
            $flag = 1;
        }

        $fetchData = array();
        // $a = 0;
        if ($flag == 1) {
            for ($i = 2; $i <= $arrayCount; $i++) {
                $year = $SheetDataKey['Year'];
                $operator = $SheetDataKey['Operator'];
                $region = $SheetDataKey['Region'];
                $country = $SheetDataKey['Country'];
                $aircraft_role = $SheetDataKey['Aircraft Role'];
                $aircraft_category = $SheetDataKey['Aircraft Category'];
                $manufacturer = $SheetDataKey['Manufacturer'];
                $aircraft_family = $SheetDataKey['Aircraft Family'];
                $aircraft_group = $SheetDataKey['Aircraft Group'];
                $aircraft_type = $SheetDataKey['Aircraft Type'];
                $propulsion = $SheetDataKey['Propulsion'];
                $engine_manufacturer = $SheetDataKey['Engine Manufacturer'];
                $engine_family = $SheetDataKey['Engine Family'];
                $expense_category = $SheetDataKey['Expense Category'];
                $expense_type = $SheetDataKey['Expense Type'];
                $total_cost = $SheetDataKey['Total Cost'];
                $events = $SheetDataKey['Events'];
                $material_spend = $SheetDataKey['Material Spend'];
                $labor_hours = $SheetDataKey['Labor Hours'];
                $labor_spend = $SheetDataKey['Labor Spend'];
                $year = filter_var(trim($allDataInSheet[0][$i][$year]), FILTER_SANITIZE_STRING);
                $operator = filter_var(trim($allDataInSheet[0][$i][$operator]), FILTER_SANITIZE_STRING);
                $region = filter_var(trim($allDataInSheet[0][$i][$region]), FILTER_SANITIZE_EMAIL);
                $country = filter_var(trim($allDataInSheet[0][$i][$country]), FILTER_SANITIZE_STRING);
                $aircraft_role = filter_var(trim($allDataInSheet[0][$i][$aircraft_role]), FILTER_SANITIZE_STRING);
                $aircraft_category = filter_var(trim($allDataInSheet[0][$i][$aircraft_category]), FILTER_SANITIZE_STRING);
                $manufacturer = filter_var(trim($allDataInSheet[0][$i][$manufacturer]), FILTER_SANITIZE_STRING);
                $aircraft_family = filter_var(trim($allDataInSheet[0][$i][$aircraft_family]), FILTER_SANITIZE_STRING);
                $aircraft_group = filter_var(trim($allDataInSheet[0][$i][$aircraft_group]), FILTER_SANITIZE_STRING);
                $aircraft_type = filter_var(trim($allDataInSheet[0][$i][$aircraft_type]), FILTER_SANITIZE_STRING);
                $propulsion = filter_var(trim($allDataInSheet[0][$i][$propulsion]), FILTER_SANITIZE_STRING);
                $engine_manufacturer = filter_var(trim($allDataInSheet[0][$i][$engine_manufacturer]), FILTER_SANITIZE_STRING);
                $engine_family = filter_var(trim($allDataInSheet[0][$i][$engine_family]), FILTER_SANITIZE_STRING);
                $expense_category = filter_var(trim($allDataInSheet[0][$i][$expense_category]), FILTER_SANITIZE_STRING);
                $expense_type = filter_var(trim($allDataInSheet[0][$i][$expense_type]), FILTER_SANITIZE_STRING);
                $total_cost = filter_var(trim($allDataInSheet[0][$i][$total_cost]), FILTER_SANITIZE_STRING);
                $events = filter_var(trim($allDataInSheet[0][$i][$events]), FILTER_SANITIZE_STRING);
                $material_spend = filter_var(trim($allDataInSheet[0][$i][$material_spend]), FILTER_SANITIZE_STRING);
                $labor_hours = filter_var(trim($allDataInSheet[0][$i][$labor_hours]), FILTER_SANITIZE_STRING);
                $labor_spend = filter_var(trim($allDataInSheet[0][$i][$labor_spend]), FILTER_SANITIZE_STRING);

                // if ($SheetDataKey['Year']!=$yearPost) {
                //   $dataOut['message'] = "Ada tahun yang tidak sesuai";
                //   $dataOut['sukses'] = false;
                // }
                // $a = $i;
              if (strpos(strtolower($operator), "unknown") === false ||
                  strpos(strtolower($region), "unknown") === false ||
                  strpos(strtolower($country), "unknown") === false) {

                // if ($year != "") {
                  $fetchData[] = array(
                    'year' => $year,
                    'operator' => $operator,
                    'region' => $region,
                    'country' => $country,
                    'aircraft_role' => $aircraft_role,
                    'aircraft_category' => $aircraft_category,
                    'manufacturer' => $manufacturer,
                    'aircraft_family' => $aircraft_family,
                    'aircraft_group' => $aircraft_group,
                    'aircraft_type' => $aircraft_type,
                    'propulsion' => $propulsion,
                    'engine_manufacturer' => $engine_manufacturer,
                    'engine_family' => $engine_family,
                    'expense_category' => $expense_category,
                    'expense_type' => $expense_type,
                    'total_cost' => $total_cost,
                    'events' => $events,
                    'material_spend' => $material_spend,
                    'labor_hours' => $labor_hours,
                    'labor_spend' => $labor_spend);
                // }
              }

            }

            // echo "count i: ".$a;
            // echo "count fetchData: ".count($fetchData);

            // $data['data_mro'] = $fetchData;
            // $dataOut['sukses'] = false;
            if ($dataOut['sukses']) {
              $id_history = $this->insertHistoryUpload($name_file_up);
              if (!empty($id_history)) {
                $status = $this->mappingData($fetchData, $id_history);
                if ($status) {
                  $dataOut['sukses'] = true;
                  $dataOut['message'] = "Sukses";
                }else{
                  $this->M_mro_history->delete($id_history);
                  $dataOut['sukses'] = false;
                  $dataOut['message'] = "Gagal Input Data MRO";
                }
              }else{
                $dataOut['sukses'] = false;
                $dataOut['message'] = "Gagal Input Data History Upload";
              }
            }
            // $this->import->setBatchImport($fetchData);
            // $this->import->importData();
        } else {
            $dataOut['sukses'] = false;
            $dataOut['message'] = "Please import correct file";
        }

        // if (!empty($inputFileName)) {
        //   unlink($inputFileName);
        // }
        
        
        
        }else{
            $dataOut['sukses'] = false;
            $dataOut['message'] = "Max 20.000 Baris Data";
        }
        // print_r($data);
        echo json_encode($dataOut);

    // }

    }

    function insertHistoryUpload($file){
      $data['date_upload'] = date('Y-m-d h:i:sa');
      $data['name_file'] = $file;
      $id = $this->M_mro_history->save($data);

      return $id;
    }

    function mappingData($listMro, $id_history){

      if ($this->M_mro->_emptyData()){
        // echo "empty data MRO";
      }

      $arrOperator = array();
      $arrEngineFamily = array();
      $arrExpenseCategory = array();
      $arrCountry = array();
      $arrRegion = array();
      $arrCountryReg = array();

      $sizeList = count($listMro);

      foreach ($listMro as $key => $value) {
        // $arrOperator[] = strtoupper($value['operator']);
        // $arrEngineFamily[] = $value['engine_family'];
        // $arrExpenseCategory[] = strtoupper($value['expense_category']);
        // $arrCountry[] = strtolower($value['country']);
        // $arrRegion[] = strtolower($value['region']);
        // $arrCountryReg[$value['country']] = strtolower($value['region']);
        $arrOperator[] = $value['operator'];
        $arrEngineFamily[] = $value['engine_family'];
        $arrExpenseCategory[] = $value['expense_category'];
        $arrCountry[] = $value['country'];
        $arrRegion[] = $value['region'];
        $arrCountryReg[$value['country']] = $value['region'];
      }

      $resultOperator = array_map("unserialize", array_unique(array_map("serialize", $arrOperator)));
      $resultEngineFamily = array_map("unserialize", array_unique(array_map("serialize", $arrEngineFamily)));
      $resultExpenseCategory = array_map("unserialize", array_unique(array_map("serialize", $arrExpenseCategory)));
      $resultCountry = array_map("unserialize", array_unique(array_map("serialize", $arrCountry)));
      $resultRegion = array_map("unserialize", array_unique(array_map("serialize", $arrRegion)));

      // insert Master Operator
      if (!empty($resultOperator) || $resultOperator != "") {
        foreach ($resultOperator as $key => $value) {
          if (!empty($value) || $value!="") {
            $valOperator = $this->M_mro_operator->getByName($value);
            if (empty($valOperator)) {
              $dataOpr['name'] = $value;
              $this->M_mro_operator->insert($dataOpr);
            }
          }
        }
      }

      // insert Master EngineFamily
      if (!empty($resultEngineFamily) || $resultEngineFamily != "") {
        foreach (array_unique($resultEngineFamily) as $key => $value) {
          if (!empty($value)) {
            $valEF = $this->M_mro_enginefamily->getByName($value);
            if (empty($valEF)) {
              $dataEF['name'] = $value;
              $this->M_mro_enginefamily->insert($dataEF);
            }
          }
        }
      }

      // insert Master ExpenseCategory
      if (!empty($resultExpenseCategory) || $resultExpenseCategory != "") {
        foreach (array_unique($resultExpenseCategory) as $key => $value) {
          if (!empty($value)) {
            $valEC = $this->M_mro_expensecategory->getByName($value);
            if (empty($valEC)) {
              $dataEC['name'] = $value;
              $this->M_mro_expensecategory->insert($dataEC);
            }
          }
        }
      }

      // insert Master Region
      if (!empty($resultRegion) || $resultRegion != "") {
        foreach (array_unique($resultRegion) as $key => $val) {
          if (!empty($val)) {
            $valReg = $this->M_mro_region->getByName($val);
            if (empty($valReg)) {
              $dataReg['region'] = $val;
              $this->M_mro_region->insert($dataReg);
            }
          }
        }
      }

      // insert Master Country
      if (!empty($arrCountryReg) || $arrCountryReg != "") {
        foreach ($arrCountryReg as $key => $value) {
          if (!empty($value)) {
            $valCountry = $this->M_mro_country->getByName($key);
            $valReg = $this->M_mro_region->getByName($value);
            if (empty($valCountry)) {
              $dataC['id_country'] = null;
              $dataC['id_region'] = $valReg->id_region;
              $dataC['country'] = $key;
              $this->M_mro_country->insert($dataC);
            }
          }
        }
      }

      $status = $this->insertTransaction($listMro, $id_history);
      return $status;
      // print_r(array_unique($resultOperator));
      // print_r(array_unique($resultEngineFamily));
      // print_r(array_unique($resultExpenseCategory));
      // print_r(array_unique($resultCountry));
      // print_r(array_unique($resultRegion));
      // print_r($arrCountryReg);

    }

    function insertTransaction($listMro, $id_history){
      $dbOperator = $this->M_mro_operator->getAllData();
      $dbOperatorArray = array();
      foreach ($dbOperator as $key => $value) {
        $dbOperatorArray[$value['id_operator']] = $value['name'];
      }

      $dbEngineFamily = $this->M_mro_enginefamily->getAllData();
      $dbEngineFamilyArray = array();
      foreach ($dbEngineFamily as $key => $value) {
        $dbEngineFamilyArray[$value['id_engine_family']] = $value['name'];
      }

      $dbExpenseCategory = $this->M_mro_expensecategory->getAllData();
      $dbExpenseCategoryArray = array();
      foreach ($dbExpenseCategory as $key => $value) {
        $dbExpenseCategoryArray[$value['id_expense_category']] = $value['name'];
      }

      $dbRegion = $this->M_mro_region->getAllData();
      $dbRegionArray = array();
      foreach ($dbRegion as $key => $value) {
        $dbRegionArray[$value['id_region']] = $value['region'];
      }

      $dbCountry = $this->M_mro_country->getAllData();
      $dbCountryArray = array();
      foreach ($dbCountry as $key => $value) {
        $dbCountryArray[$value['id_country']] = $value['country'];
      }

      $j = 0;
      $arrInputDataMro = array();
      $i = 0;
      foreach ($listMro as $key => $value) {
        $dataTransaksi['year'] = $value['year'];
        $dataTransaksi['id_operator'] = array_search(strtoupper($value['operator']), array_map('strtoupper', $dbOperatorArray));
        $dataTransaksi['id_country'] = array_search(strtolower($value['country']), array_map('strtolower', $dbCountryArray));
        $dataTransaksi['aircraft_role'] = $value['aircraft_role'];
        $dataTransaksi['aircraft_category'] = $value['aircraft_category'];
        $dataTransaksi['manufacturer'] = $value['manufacturer'];
        $dataTransaksi['aircraft_family'] = $value['aircraft_family'];
        $dataTransaksi['aircraft_group'] = $value['aircraft_group'];
        $dataTransaksi['aircraft_type'] = $value['aircraft_type'];
        $dataTransaksi['propulsion'] = $value['propulsion'];
        $dataTransaksi['engine_manufacturer'] = $value['engine_manufacturer'];
        $dataTransaksi['id_engine_family'] = array_search(strtoupper($value['engine_family']), array_map('strtoupper', $dbEngineFamilyArray));
        $dataTransaksi['id_expense_category'] = array_search(strtoupper($value['expense_category']), array_map('strtoupper', $dbExpenseCategoryArray));
        $dataTransaksi['expense_type'] = $value['expense_type'];
        $dataTransaksi['total_cost'] = $value['total_cost'];
        $dataTransaksi['events'] = $value['events'];
        $dataTransaksi['material_spend'] = $value['material_spend'];
        $dataTransaksi['labor_hours'] = $value['labor_hours'];
        $dataTransaksi['labor_spend'] = $value['labor_spend'];
        $dataTransaksi['id_mro_history_upload'] = $id_history;

        if ($i!=0 && $i % 10000 == 0) {
          $j++;
          $i=0;
        }
        $arrInputDataMro[$j][$i] = $dataTransaksi;
        $i++;
      }

      // echo "counter: ".count($arrInputDataMro);
      // for ($i=0; $i < count($arrInputDataMro); $i++) { 
      //   print_r($arrInputDataMro[$i]);
      // }
      // echo "counter: ".count($arrInputDataMro[0]);
      return $this->M_mro->insert($arrInputDataMro);
      // foreach ($arrInputDataMro as $key => $value) {
      //   echo "<pre>";
      //   print_r($value);
      //   echo "</pre>";
      // }
      // echo "<pre>";
      // print_r($arrInputDataMro);
      // echo "</pre>";
      // return true;

    }

  }

  class chunkReadFilter implements PHPExcel_Reader_IReadFilter
  {
    private $_startRow = 0;
    private $_endRow = 0;
    /**  We expect a list of the rows that we want to read to be passed into the constructor  */
    public function __construct($startRow, $chunkSize) {
      $this->_startRow  = $startRow;
      $this->_endRow    = $startRow + $chunkSize;
    }
    public function readCell($column, $row, $worksheetName = '') {
      //  Only read the heading row, and the rows that were configured in the constructor
      if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
        return true;
      }
      return false;
    }
  }
