<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Mro_dashboard extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("M_mro");
        $this->load->model("M_region");
        $this->load->model("M_mro_enginefamily");
        $this->load->model("M_mro_operator");
        $this->load->model("M_mro_expensecategory");
        $this->load->model("M_mro_transaction");
        $this->load->helper("color");
    }

    function index()
    {
        $data = array(
            "content" => "mro/mro-market",
            "title" => "MRO Market",
            "small_tittle" => "",
            "breadcrumb" => ["MRO Market"],
            "menu" => $this->M_menu->tampil(),
        );

        $data['regions'] = $this->M_region->get_all();
        $data['engine_types'] = $this->M_mro_enginefamily->_getAllData();
        $data['operators'] = $this->M_mro_operator->getAllData();
        $data['aircraft_types'] = $this->get_all_aircraft_type();
        $data['year_from'] = date('Y');
        $data['year_to'] = date('Y') + 5;
        $this->load->view("layouts", $data);
    }

    /*
    START EDIT
    BY DIMAS ISLAMI
    */
    function get_data_by_year()
    {
        $operator_id = $this->input->post('operator_id');
        $region_id = $this->input->post('region_id');
        $engine_type_id = $this->input->post('engine_type_id');
        $year_from = (int)$this->input->post('year_from');
        $year_to = (int)$this->input->post('year_to');
        $tipe = $this->input->post('tipe');
        $aircraft_type = $this->input->post('aircraft_type');

        $params = array();
        if (isset($operator_id) && $operator_id != "all")
            $params['id_operator'] = $operator_id;
        if (isset($engine_type_id) && $engine_type_id != "all")
            $params['id_engine_family'] = $engine_type_id;
        if (isset($aircraft_type) && $aircraft_type != "all")
            $params['aircraft_type'] = $aircraft_type;

        $years = array();
        for ($i = $year_from; $i <= $year_to; $i++) {
            array_push($years, $i);
        }

        if ($tipe == 0) {
            $series = array();
            $segments = $this->M_mro_expensecategory->getAllData();
            $color = standardColor();

            for ($i = 0; $i < count($segments); $i++) {
                $total_cost = array();
                $params['id_expense_category'] = $segments[$i]['id_expense_category'];
                for ($year = $year_from; $year <= $year_to; $year++) {
                    array_push($total_cost, (int)$this->M_mro_transaction->get_total_cost($params, $region_id, $year, $year));
                }
                $data = array();
                $data['name'] = $segments[$i]['name'];
                $data['data'] = $total_cost;
                $data['data'] = $total_cost;
                $data['showInLegend'] = false;
                $data['color'] = $color[$i];
                array_push($series, $data);
            }
            $result = array('categories' => $years, 'series' => $series);
            echo json_encode($result);
        } else {
            $series = array();
            $segments = $this->M_mro_expensecategory->getAllData();
            $color = standardColor();

            for ($i = 0; $i < count($segments); $i++) {
                $total_cost = array();
                $params['id_expense_category'] = $segments[$i]['id_expense_category'];
                for ($year = $year_from; $year <= $year_to; $year++) {
                    array_push($total_cost, (int)$this->M_mro_transaction->get_total_cost($params, $region_id, $year, $year));
                }
                $data = array();
                $data['name'] = $segments[$i]['name'];
                $data['data'] = $total_cost;
                $data['color'] = $color[$i];
                array_push($series, $data);
            }
            $result = array('categories' => $years, 'series' => $series);
            echo json_encode($result);
        }

    }

    function get_data_by_region()
    {
        $operator_id = $this->input->post('operator_id');
        $engine_type_id = $this->input->post('engine_type_id');
        $year_from = (int)$this->input->post('year_from');
        $year_to = (int)$this->input->post('year_to');
        $aircraft_type = $this->input->post('aircraft_type');
        $tipe = $this->input->post('tipe');
        $segments = $this->M_mro_expensecategory->getAllData();
        $regions = $this->M_region->get_all();
        $colors = standardColor();

        $params = array();
        if (isset($operator_id) && $operator_id != 'all')
            $params['id_operator'] = $operator_id;
        if (isset($engine_type_id) && $engine_type_id != 'all')
            $params['id_engine_family'] = $engine_type_id;
        if (isset($aircraft_type) && $aircraft_type != 'all')
            $params['aircraft_type'] = $aircraft_type;


        $categories = array();
        foreach ($regions as $region) {
            array_push($categories, $region['region']);
        }

        if ($tipe == 0) {
            $series = array();
            for ($i = 0; $i < count($segments); $i++) {
                $total_cost = array();
                $params['id_expense_category'] = $segments[$i]['id_expense_category'];
                foreach ($regions as $region) {
                    array_push($total_cost, (int)$this->M_mro_transaction->get_total_cost($params, $region['id_region'], $year_from, $year_to));
                }
                $data = array();
                $data['name'] = $segments[$i]['name'];
                $data['data'] = $total_cost;
                $data['color'] = $colors[$i];
                $data['showInLegend'] = false;
                array_push($series, $data);
            }
            $result = array('categories' => $categories, 'series' => $series);
            echo json_encode($result);
        } else {
            $series = array();
            for ($i = 0; $i < count($segments); $i++) {
                $total_cost = array();
                $params['id_expense_category'] = $segments[$i]['id_expense_category'];
                foreach ($regions as $region) {
                    array_push($total_cost, (int)$this->M_mro_transaction->get_total_cost($params, $region['id_region'], $year_from, $year_to));
                }
                $data = array();
                $data['name'] = $segments[$i]['name'];
                $data['data'] = $total_cost;
                $data['color'] = $colors[$i];
                array_push($series, $data);
            }
            $result = array('categories' => $categories, 'series' => $series);
            echo json_encode($result);
        }
    }

    function get_data_by_segment()
    {
        $aircraft=$this->input->post('aircraft');
        $operator_id = $this->input->post('operator_id');
        $engine_type_id = $this->input->post('engine_type_id');
        $year_from = (int)$this->input->post('year_from');
        $year_to = (int)$this->input->post('year_to');
        $region_id = $this->input->post('region_id');
        $tipe = $this->input->post('tipe');
        $colors = standardColor();
        $segments = $this->M_mro_expensecategory->getAllData();

        $params = array();
        if (isset($aircraft) && $aircraft != 'all')
            $params['aircraft_type'] = $aircraft;
        if (isset($operator_id) && $operator_id != 'all')
            $params['id_operator'] = $operator_id;
        if (isset($engine_type_id) && $engine_type_id != 'all')
            $params['id_engine_family'] = $engine_type_id;

        if ($tipe == 0) {
            $data = array();
            $categories = array();
            foreach ($segments as $segment) {
                array_push($categories, $segment['name']);
                $params['id_expense_category'] = $segment['id_expense_category'];
                $row = (int)$this->M_mro_transaction->get_total_cost($params, $region_id, $year_from, $year_to);
                array_push($data, $row);
            }
            $series = array('name' => "Total Cost", "data" => $data, "color" => $colors[1], "categories" => $categories, 'showInLegend' => false);
            echo json_encode($series);
        } else {
            $data = array();
            $categories = array();
            foreach ($segments as $segment) {
                array_push($categories, $segment['name']);
                $params['id_expense_category'] = $segment['id_expense_category'];
                $row = (int)$this->M_mro_transaction->get_total_cost($params, $region_id, $year_from, $year_to);
                array_push($data, $row);
            }
            $series = array('name' => "Total Cost", "data" => $data, "color" => $colors[1], "categories" => $categories, 'showInLegend' => true);
            echo json_encode($series);
        }
    }

    /*
    END EDIT
    BY DIMAS ISLAMI
    */

    function get_all_aircraft_type()
    {
        $result = $this->M_mro_transaction->get_all_aircraft_type();
        $data = array();
        foreach ($result as $item) {
            array_push($data, $item['aircraft_type']);
        }
        return $data;
    }

}
