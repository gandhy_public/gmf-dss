<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Mro extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("M_mro");
        $this->load->model("M_region");
    }

    function index()
    {

    }

    function market()
    {
        $data = array(
            "content" => "mro/mro-market",
            "title" => "MRO Market",
            "small_tittle" => "",
            "breadcrumb" => ["MRO Market"],
            "menu" => $this->M_menu->tampil(),
        );

        $this->load->view("layouts", $data);
    }

    function get_all_region()
    {
        $result = $this->M_region->get_all();
        echo "<pre>";
        print_r($result);
    }

    function upload()
    {
        $data = array(
            "content" => "mro/upload-data",
            "title" => "MRO Upload",
            "small_tittle" => "",
            "breadcrumb" => ["Upload Data"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

}
