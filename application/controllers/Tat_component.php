<?php
defined("BASEPATH") or exit('no direct script access allowed');


/**
 *
 */
class Tat_component extends CI_Controller
{

    public function __construct()
    {
        # code...
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_menu");
        $this->load->model("M_tatcs");
        $this->load->helper("date");
        $this->load->helper("file");
    }

    public function index()
    {
        $data = array(
            "content" => "TAT_COMP/V_TAT_component",
            "title" => "TAT Component",
            "small_tittle" => "",
            "breadcrumb" => [""],
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }


    public function view_plan()
    {
        $plan = $this->input->post("plan");

        // $sudah = $this->M_tatcs->get_plan_sudah($plan);
        $tahun = date("Y");
        $bulan_ini = floatval(date('m'));
        // echo $bulan_ini;
        $data['data'] = array();
        $data['datat'] = array();
        $data['datag'] = array();

        // foreach ($all as $gv_key => $gv_value) {
        // $tanggal =  $gv_value['tahun'].'-'.str_pad($gv_value['bulan'],2,"0",STR_PAD_LEFT).'-22';
        // $valuet =  date('F', strtotime($tanggal));
        // $target = array(floatval(100));
        // if($gv_value['total']>100){
        // $hitung  = array(floatval(100));
        // }else{
        // $hitung = array(floatval(is_null($gv_value['total'])?0:number_format($gv_value['total'], 2)));
        // }
        // array_push($data['data'], $hitung);
        // array_push($data['datat'], $valuet);
        // array_push($data['datag'], $target);
        // }
        for ($i = 1; $i <= $bulan_ini; $i++) {
            // echo 2;
            $gv_value = $this->M_tatcs->get_plan_all($plan, $i, $tahun);
            $tanggal = $tahun . '-' . str_pad($i, 2, "0", STR_PAD_LEFT) . '-22';
            $valuet = date('M', strtotime($tanggal));
            // $valuet =  $tanggal;
            $target = array(floatval(80));
            if ($gv_value['total'] > 80) {
                $hitung = array(floatval(80));
            } else {
                $hitung = array(floatval(is_null($gv_value['total']) ? 0 : number_format($gv_value['total'], 2)));
            }
            array_push($data['data'], $hitung);
            array_push($data['datat'], $valuet);
            array_push($data['datag'], $target);
        }

        $json[] = $data;
        echo json_encode(
            array(
                'grafik' => $json,
            )
        );
    }


    public function view_detail_plan()
    {

        $plan = $this->input->post("plan");
        $all = $this->M_tatcs->get_all_detail_plan($plan);
        $work = $this->M_tatcs->distinct_work_center($plan);
        $total = $this->M_tatcs->get_all_detail_total($plan);
        $total = floatval($total['jumlah']);
        $count = count($all);
        $acount = count($work);
        $dataChart = array();
        if ($count != 0) {
            for ($i = 0; $i < $acount; $i++) {
                $dataChart['name'] = $work[$i];
                $cek = $this->M_tatcs->cek_detail_total($work[$i]);
                if (count($cek) != 0) {
                    $jumlah = floatval($cek['jumlah']);
                    $hitung = ($jumlah / $total) * 100;
                    $dataChart['y'] = floatval(is_null($hitung) ? 0 : number_format($hitung, 2));
                } else {
                    echo 2;
                    $dataChart['y'] = 0;
                }
                $json_builder[] = $dataChart;
            }
        } else {
            $dataChart['name'] = 'Data not found';
            $dataChart['y'] = 0;
            $json_builder[] = $dataChart;
            // $json_builder = 'not found';
        }
        echo json_encode(
            array(
                'grafik' => $json_builder,
            )
        );
    }


    public function view_detail_table()
    {
        $tipe = $this->input->post("tipe");
        $plan = $this->input->post("plan");
        $status = $this->input->post("param");
        $air = $this->input->post("status");
        $tahun = '2001';

        $week = $this->input->post('week');

        $dataTable = array();
        if ($air == '' || $air == 'all') {
            $all = $this->M_tatcs->get_all_detail_table($plan, $tipe, $status, '', $week);
            $count = count($all);
            if ($count != 0) {
                $status = "Ada";
                $json = $this->build_datatable($all);

            } else {
                $status = "Tidak ada data";
                $json[] = '';
            }
        } else {
            if ($air == 'ga') {
                $like = 'like';
                $and = 'or';
            } else if ($air == 'nonga') {
                $like = 'not like';
                $and = 'and';
            }

            $huruf = array('P', 'T');
            $count = count($huruf);
            $where = '(';
            for ($i = 0; $i < $count; $i++) {
                for ($x = 0; $x <= 10; $x++) {
                    $angka = str_pad($x, 2, "0", STR_PAD_LEFT);
                    $kode = $huruf[$i] . '-' . $tahun . '-0' . $angka . '-';
                    $where .= "wbs {$like} '{$kode}%'";
                    if ($angka == '10' && $huruf[$i] == 'T') {
                        $where .= '';
                    } else {
                        $where .= " {$and} ";
                    }
                }
            }
            $where .= ')';
            // echo $where;
            // exit();
            $data = $this->M_tatcs->get_all_detail_table($plan, $tipe, $status, $where, $week);
            $json = $this->build_datatable($data);
        }

        echo json_encode(
            array(
                'tabel' => $json,
                'status' => $status,
            )
        );
    }

    private function build_datatable($param)
    {
        $json = array();
        foreach ($param as $gv_key => $val) {

            $param = $val['plant'];
            if ($param == 'WSAV') {
                $param = 'TCA';
            } else if ($param == 'WSEM') {
                $param = 'TCE';
            } else if ($param == 'WSWB') {
                $param = 'TCW';
            } else if ($param == 'WSNC') {
                $param = 'TCY';
            }
            $dataTable['order_number'] = $val['order_number'];
            $dataTable['plant'] = $param;
            $dataTable['work_center'] = $val['main_work_center'];
            $dataTable['material'] = $val['material'];
            $dataTable['mat_desc'] = $val['mat_desc'];
            $dataTable['serial'] = $val['serial_number'];
            $dataTable['wbs'] = $val['wbs'];
            $dataTable['start'] = date('d-m-Y', strtotime($val['created_on']));
            $dataTable['end'] = date('d-m-Y', strtotime($val['act_finish_date']));
            $dataTable['user_status'] = $val['user_status'];
            if ($val['tat'] == '' || $val['tat'] == null) {
                $tat = 0;
            } else {
                $tat = $val['tat'];
            }
            $dataTable['tat'] = $tat;
            $dataTable['jumlah_hari'] = $val['jumlah_hari'];
            $start = new DateTime($val['created_on']);
            $end = new DateTime($val['act_finish_date']);
            $interval = $start->diff($end);
            $selisih = $interval->days;
            $presentase = ($tat / ($selisih+1)) * 100;
            $dataTable["presentase"] = (($presentase > 100) ? 100 : number_format($presentase, 2));
            $json[] = $dataTable;
        };
        return $json;
    }


    public function view_panel_proses()
    {
        $plan = $this->input->post("plan");
        $tipe = $this->input->post("tipe");
        $huruf = array('P', 'T');
        $count = count($huruf);
        // $tahun = date('Y');
        $tahun = '2001';
        $total = 0;
        $all = $this->M_tatcs->get_panel_proses($plan);
        $jumlah = $all['jumlah'];

        if ($tipe == 'all') {
            $jumlah = $all['jumlah'];
        } else {
            // for($i=0;$i<$count;$i++){
            // for($x=0;$x<=10;$x++){
            // $angka = str_pad($x,2,"0",STR_PAD_LEFT);
            // $kode = $huruf[$i].'-'.$tahun.'-0'.$angka.'-';
            // $data = $this->M_tatcs->get_panel_proses_cek($plan, $kode, $tipe);
            // $cou = count($data);
            // $total += $cou;
            // }
            // }

            if ($tipe == 'ga') {
                $like = 'like';
                $and = 'or';
            } else if ($tipe == 'nonga') {
                $like = 'not like';
                $and = 'and';
            }

            $huruf = array('P', 'T');
            $count = count($huruf);
            $where = '(';
            for ($i = 0; $i < $count; $i++) {
                for ($x = 0; $x <= 10; $x++) {
                    $angka = str_pad($x, 2, "0", STR_PAD_LEFT);
                    $kode = $huruf[$i] . '-' . $tahun . '-0' . $angka . '-';
                    $where .= "wbs {$like} '{$kode}%'";
                    if ($angka == '10' && $huruf[$i] == 'T') {
                        $where .= '';
                    } else {
                        $where .= " {$and} ";
                    }
                }
            }
            $where .= ')';
            $data = $this->M_tatcs->get_panel_proses_cek($plan, $where, $tipe);
            $jumlah = $data['jumlah'];
            if ($jumlah == '') {
                $jumlah = 0;
            }
        }


        if (isset($all)) {
            $val = $jumlah;
        } else {
            $val = 0;
        }


        echo json_encode(
            array(
                'val' => $val,
            )
        );
    }


    //===========DATATABLE============\\
    function list_data()
    {
        $list = $this->M_tatcs->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $val) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $val->material;
            $row[] = $val->description;
            $row[] = $val->tat;;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_tatcs->count_all(),
            "recordsFiltered" => $this->M_tatcs->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }


    function upload()
    {
        $data = array(
            "content" => "TAT_COMP/upload-data",
            "title" => "TAT Component Shop Upload",
            "small_tittle" => "",
            "breadcrumb" => ["Upload Data"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }


    public function uploadFile()
    {
        $this->load->library('PHPExcel');

        $path = 'assets/upload-tatcs/';

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'xlsx|xls|svg';
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        // $config['max_size']      = 50;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        // $this->load->library('upload', $config);
        // $new_name_file=date("Ymd_His").'_'.$_FILES(['files']['name']);
        if ($this->upload->do_upload('files')) {
            $data = array('upload_data' => $this->upload->data());
        } else {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
            exit();
        }
        // echo "Data: ".json_encode($data)."\n";
        if (!empty($data['upload_data']['file_name'])) {
            $import_xls_file = $data['upload_data']['file_name'];
        } else {
            $import_xls_file = 0;
        }
        //echo "File Name: ".$import_xls_file."\n";
        $inputFileName = $path . $import_xls_file;
        // echo "Path File Name:".$inputFileName."\n";
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            // $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($inputFileName);


            $sukses = true;
        } catch (Exception $e) {
            if (!empty($inputFileName)) {
                unlink($inputFileName);
            }
            $sukses = false;
            $message = "Error Read Data";
            //echo json_encode($dataOut);
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                . '": ' . $e->getMessage());
        }
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

        $arrayCount = count($allDataInSheet);
        $flag = 0;

        $createArray = array('Material', 'Description', 'TAT');

        $SheetDataKey = array();
        foreach ($allDataInSheet as $dataInSheet) {
            foreach ($dataInSheet as $key => $value) {
                // echo 'val: ' .$value ."\n";
                if (in_array(trim($value), $createArray)) {
                    // $value = preg_replace('/\s+/', '', $value);
                    $SheetDataKey[trim($value)] = $key;
                } else {

                }
            }
        }

        $makeArray = array('Material' => 'Material', 'Description' => 'Description', 'TAT' => 'TAT');


        $data = array_diff_key($makeArray, $SheetDataKey);

        if (empty($data)) {
            $flag = 1;
        }

        if ($flag == 1) {
          $fetchData = array();
            for ($i = 2; $i <= $arrayCount; $i++) {
                $material = $SheetDataKey['Material'];
                $description = $SheetDataKey['Description'];
                $tat = $SheetDataKey['TAT'];
                $material = filter_var(trim($allDataInSheet[$i][$material]), FILTER_SANITIZE_STRING);
                $description = filter_var(trim($allDataInSheet[$i][$description]), FILTER_SANITIZE_STRING);
                $tat = filter_var(trim($allDataInSheet[$i][$tat]), FILTER_SANITIZE_STRING);

                // $cek_upload = $this->M_tatcs->cek_upload($material);
                // $cek = count($cek_upload);

                // if($cek==0){
                $fetchData[] = array(
                    'material' => $material,
                    'description' => $description,
                    'tat' => $tat);
                // }

                // else{
                // $data = array(
                // 'description' => $description,
                // 'tat' => $tat
                // );
                // $where = array(
                // 'material' => $material
                // );
                // $update = $this->M_tatcs->update_upload($data, $where, 'tcs_sla_tat');
                // }
            }

            $data['data_tatcs'] = $fetchData;
            if ($sukses) {
                $this->mappingData($fetchData);
                $sukses = true;
                $message = "Sukses";
            }
            // $this->import->setBatchImport($fetchData);
            // $this->import->importData();
        } else {
            $message = "Please import correct file";
        }

        if (!empty($inputFileName)) {
            unlink($inputFileName);
        }
        // print_r($data);

        $sukses = true;


        echo json_encode(
            array(
                'sukses' => $sukses,
                'message' => $message,
            )
        );

        // echo json_encode($dataOut);

        // }

    }


    function mappingData($listData)
    {
        $count = count($listData);
        if ($count != 0) {
            foreach ($listData as $key => $value) {
                $data['material'] = $value['material'];
                $data['description'] = $value['description'];
                $data['tat'] = $value['tat'];

                $arrInputData[] = $data;
            }

            // print_r($listData); exit;

            $this->M_tatcs->truncate_table();
            $this->M_tatcs->insert($arrInputData);
        }


    }


    public function exportExcel()
    {
        $list = $this->M_tatcs->get_all_data();
        $heading = array('Material', 'Description', 'TAT');
        $this->load->library('PHPExcel');
        //Create a new Object
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setTitle('Data TAT Component Shop');
        //Loop Heading
        $rowNumberH = 1;
        $colH = 'A';
        foreach ($heading as $h) {
            $objPHPExcel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
            $colH++;
        }
        //Loop Result
        $totn = count($list);
        $maxrow = $totn + 1;
        $row = 2;
        foreach ($list as $key => $n) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $n['material']);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $n['description']);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $n['tat']);
            $row++;
        }
        //Freeze pane
        $objPHPExcel->getActiveSheet()->freezePane('A2');
        //Cell Style
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:C' . $maxrow)->applyFromArray($styleArray);
        //Save as an Excel BIFF (xls) file


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Format Upload Data TAT Component Shop.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        ob_start();
        $objWriter->save('php://output');
        exit;
    }


}
