<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Tat_airframe extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->helper("date");
        $this->load->model("M_menu");
        $this->load->model("M_tat_airframe");
    }

    function index()
    {
        // $ac = $this->M_tat_airframe->get_aircraft();
        $data = array(
            "content" => "TAT Airframe/graph",
            "title" => "TAT Airframe",
            "small_tittle" => "",
            "breadcrumb" => ["TAT Airframe"],
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }

    public function produceSumArray($datas)
    {
        $sumArray = array();

        foreach ($datas as $k => $subArray) {
            foreach ($subArray as $id => $value) {
                array_key_exists($id, $sumArray) ? $sumArray[$id] += $value : $sumArray[$id] = $value;
            }
        }

        return $sumArray;
    }

    public function load_chart_accumulation()
    {
        $line = array('GAH2');
        $base = array('GAH1', 'GAH3', 'GAH4');
        $date['start'] = date("Y") . "01-01";
        $date['end'] = date("Y-m-d");
        $fleet = implode(",", $this->input->post('param2'));

        $input['tat_type'] = $this->input->post('param3');
        $input['fleet'] = $fleet;
        $input['air_type'] = '';

        if ($input['tat_type'] == "Line") {
            $input['tat_type'] = $line;
        } else {
            $input['tat_type'] = $base;
        }

        if ($input['air_type'] == '' or null) {
            $plotTitle = 'All';
        } else {
            $plotTitle = $input['air_type'];
        }

        $dataAllOP = array(
            'garuda' => $this->M_tat_airframe->getDetailData($input, 'GA'),
            'citilink' => $this->M_tat_airframe->getDetailData($input, 'CITILINK'),
            'nonga' => $this->M_tat_airframe->getDetailData($input, 'NON GA')
        );

        $sumAllOP = $this->produceSumArray($dataAllOP);

        $actual = array();
        $target = array();

        foreach ($sumAllOP as $op) {
            array_push($target, 100);
            array_push($actual, (float)number_format((float)$op / 3, 2, '.', ''));
        }

        $color = array('#05354D', '#FD9214', '#F2C573', '#8EC3A7', '#F24738', '#92A8CD');

        $randColor = $color[array_rand($color)];

        $result = array(
            'categories' => $this->M_tat_airframe->getMonthName_acm(),
            'actual' => $actual,
            'target' => $target,
            'color' => $randColor
        );

        echo json_encode($result);
    }

    public function load_chart_GA()
    {
        $line = array('GAH2');
        $base = array('GAH1', 'GAH3', 'GAH4');
        $date['start'] = date("Y") . "01-01";
        $date['end'] = date("Y-m-d");

        $input['tat_type'] = $this->input->post('param3');
        $input['fleet'] = implode(",", $this->input->post('param2'));
        $inp = $this->input->post('param');
        $input['air_type'] = $this->input->post('param4');

        if ($input['tat_type'] == "Line") {
            $input['tat_type'] = $line;
        } else {
            $input['tat_type'] = $base;
        }

        if ($input['air_type'] == '' or null) {
            $plotTitle = 'All';
        } else {
            $plotTitle = $input['air_type'];
        }

        $data1 = array();
        foreach ($inp as $key => $v) {
            array_push($data1, $v);
        }

        $operator = implode("','", $data1);


        $allDatas = $this->M_tat_airframe->getDetailData($input, $operator);

        $target = array();

        foreach ($this->M_tat_airframe->getMonthName() as $month) {
            array_push($target, 100);
        }

        $color = array('#05354D', '#FD9214', '#F2C573', '#8EC3A7', '#F24738', '#92A8CD');

        $randColor = $color[array_rand($color)];

        $result = array(
            'categories' => $this->M_tat_airframe->getMonthName(),
            'actype' => $plotTitle,
            'data' => $allDatas,
            'target' => $target,
            'color' => $randColor
        );

        echo json_encode($result);
    }

    public function theColor()
    {
        $colors = ['#8EC3A7', '#F24738', '#92A8CD'];
        $rand = array_rand($colors, 1);
        return $colors[$rand];
    }

    public function search_tat()
    {
        $line = "'GAH2'";
        $base = "'GAH1','GAH3','GAH4'";
        $date['start'] = date("Y") . "0101";
        $date['end'] = date("Ymd");
        $input['tat_type'] = $this->input->post('param3');
        if ($input['tat_type'] == "Line") {
            $input['tat_type'] = $line;
        } else {
            $input['tat_type'] = $base;
        }
        $fleet = "'".implode("','", $this->input->post('param2'))."'";

        $input['fleet'] =  $fleet;
        $inp = $this->input->post('param');
        $input['air_type'] = $this->input->post('param4');

        if ($input['air_type'] == 'All') {
            $input['air_type'] = "";
            // $input['fleet'] = "'NARROW','WIDE'";
        }

        $data1 = array();
        foreach ($inp as $key => $v) {
            // code...
            array_push($data1, $v);

        }
        $input['param_airc'] = implode("','", $data1);
        $query = "";
        if (in_array("NON GA", $inp)) {
            $query = "AND ( ID_CUSTOMER NOT LIKE 'GIA%' ESCAPE '!'
                      OR ID_CUSTOMER NOT LIKE 'CTV%' ESCAPE '!'
                      OR ID_CUSTOMER NOT LIKE 'GL%' ESCAPE '!')";
            // $value = $this->M_tat_airframe->get_value_nonga_by($input,$date);
        } else if (in_array("GA", $inp)) {
            $query = "AND ID_CUSTOMER LIKE 'GIA%' ESCAPE '!'";
        } elseif (in_array("CITILINK", $inp)) {
            $query = "AND (ID_CUSTOMER LIKE 'CTV%' ESCAPE '!'
                      OR ID_CUSTOMER LIKE 'GL%' ESCAPE '!') ";
        }

        $value = $this->M_tat_airframe->get_value_ga_by($input, $date, $query);

        $respon = array();
        $post = array();
        $no_ga = 0;
        $no_citi = 0;
        $no_nga = 0;

        foreach ($value as $ke => $va) {
            if ($va['own'] == 'GA') {
                $post[] = array(
                    "no_ga" => $no_ga
                );
                $no_ga++;
            } else if ($va['own'] == 'CITILINK') {
                $post[] = array(
                    "no_citi" => $no_citi
                );
                $no_citi++;
            } else if ($va['own'] == 'NON GA') {
                $post[] = array(
                    "no_nga" => $no_nga
                );
                $no_nga++;
            }
        }
        $respon['no_ga'] = $no_ga;
        $respon['no_citi'] = $no_citi;
        $respon['no_nga'] = $no_nga;

        echo json_encode($respon);
    }

    public function get_table_ga_det()
    {
        $line = array('GAH2');
        $base = array('GAH1', 'GAH3', 'GAH4');
        $date['start'] = date("Y") . "0101";
        $date['end'] = date("Ymd");
        $input['tat_type'] = $this->input->post('param3');
        if ($input['tat_type'] == "Line") {
            $input['tat_type'] = $line;
        } else {
            $input['tat_type'] = $base;
        }
        $input['fleet'] = implode(",", $this->input->post('param2'));
        $input['param_airc'] = $this->input->post('param');
        $input['air_type'] = $this->input->post('param4');

        if ($input['air_type'] == 'All') {
            $input['air_type'] = "";
        }
        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search
        );

        $tatAirframes = $this->M_tat_airframe->get_table_ga_det($input, $data, $date, 'data');

        $pages = $this->M_tat_airframe->get_table_ga_det($input, $data, $date, 'page');

        if (count($tatAirframes) > 0) {
            $no = $data['start'] + 0;
            foreach ($tatAirframes as $tatAirframe) {
                $dev = $tatAirframe['ACT_TAT'] - $tatAirframe['PLANT_TAT'];
                $rem = number_format((($tatAirframe['PLANT_TAT'] + 1) / ($tatAirframe['ACT_TAT'] + 1)) * 100, 2);
                if ($rem > 100) {
                    $rem = 100;
                    $perc = "100.00 %";
                } else {
                    $perc = $rem . " %";
                }

                $print['data'][] = array(
                    $no += 1,
                    date('d-m-Y', strtotime($tatAirframe['ACT_START_DATE'])),
					$tatAirframe['revision'],
                    $tatAirframe['actype'],
                    $tatAirframe['AIRCRAFT_REG'],
                    $tatAirframe['REVISION_DESCRIPTION'],
                    $tatAirframe['PLANT_TAT'],
                    $tatAirframe['ACT_TAT'],
                    $dev,
                    $perc,
                    $rem
                );
            }
        } else {
            $print['data'] = array();
        }

        $print['draw'] = $data['draw'];
        $print['recordsFiltered'] = $pages;
        $print['recordsTotal'] = $pages;

        echo json_encode($print);
    }

    public function get_value_ga()
    {
        $line = "'GAH2'";
        $base = "'GAH1','GAH3','GAH4'";
        $date['start'] = date("Y") . "0101";
        $date['end'] = date("Ymd");
        $input['tat_type'] = $line;
        $input['fleet'] = "'WIDE'";
        $input['param_airc'] = 'GA';
        $input['air_type'] = "";
        $no = 0;
        $post = array();
        $cek = $this->M_tat_airframe->get_value_ga_by($input, $date);
        foreach ($cek as $key => $value) {
            $post[] = array(
                "no" => $no);
            $no++;
        }
        echo json_encode($no);
    }

    public function get_value_citi()
    {
        $line = "'GAH2'";
        $base = "'GAH1','GAH3','GAH4'";
        $date['start'] = date("Y") . "0101";
        $date['end'] = date("Ymd");
        $input['tat_type'] = $line;
        $input['fleet'] = "'WIDE'";
        $input['param_airc'] = 'CITILINK';
        $input['air_type'] = "";
        $no = 0;
        $post = array();
        $cek = $this->M_tat_airframe->get_value_ga_by($input, $date);
        foreach ($cek as $key => $value) {
            $post[] = array(
                "no" => $no);
            $no++;
        }
        echo json_encode($no);
    }

    public function get_value_non()
    {
        $line = "'GAH2'";
        $base = "'GAH1','GAH3','GAH4'";
        $date['start'] = date("Y") . "0101";
        $date['end'] = date("Ymd");
        $type = "CITILINK";
        $input['tat_type'] = $line;
        $input['fleet'] = "'WIDE'";
        $input['param_airc'] = "";
        $input['air_type'] = "";
        $no = 0;
        $post = array();
        $cek = $this->M_tat_airframe->get_value_nonga_by($input, $date);
        foreach ($cek as $key => $value) {
            $post[] = array(
                "no" => $no);
            $no++;
        }
        echo json_encode($no);
    }

    public function get_table_det_type()
    {
        $line = "'GAH2'";
        $base = "'GAH1','GAH3','GAH4'";
        $date['start'] = date("Y") . "0101";
        $date['end'] = date("Ymd");
        $type = $this->input->post('param');
        $input['tat_type'] = $line;
        $input['fleet'] = "'WIDE'";
        $input['air_type'] = "";
        if ($type == 'NON') {
            $input['param_airc'] = "";
            $data = $this->M_tat_airframe->get_value_nonga_by($input, $date);
        } else {

            $input['param_airc'] = $type;
            $data = $this->M_tat_airframe->get_value_ga_by($input, $date);
        }
        $respon = array();
        $no = 1;
        $post = array();
        foreach ($data as $key => $v) {
            $post[] = array(
                "no" => $no,
                "tipe" => $v['tipe'],
                "register" => $v['register'],
                "inspect" => $v['inspect'],
                "target" => $v['target'],
                "actual" => $v['target'],
                "total" => $v['selisih']
            );
            $no++;
        }
        $respon['data'] = $post;
        echo json_encode($respon);
    }

    public function get_aircraft()
    {
        $data = $this->M_tat_airframe->get_aircraft();
        $output = array('data' => $data);
        echo json_encode($output);
    }

    public function get_airtype()
    {
        $data = $this->input->post('param');
        // print_r($data);exit;
        $data1 = array();
        foreach ($data as $key => $value) {
            // code...
            array_push($data1, $value);
        }
        $param = implode("','", $data1);
        if (in_array("NON GA", $data)) {
            $value = $this->M_tat_airframe->get_airtype_nonga($param);
        } else {
            $value = $this->M_tat_airframe->get_airtype($param);
        }
        $output = array('data' => $value);
        echo json_encode($output);
    }


    public function search_table_by()
    {

        $type = $this->input->post('param1');
        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];

        $data = array(
            'draw' => $draw,
            'length' => $length,
            'start' => $start,
            'search' => $search
        );
        $line = "'GAH2'";
        $base = "'GAH1','GAH3','GAH4'";
        $date['start'] = date("Y") . "0101";
        $date['end'] = date("Ymd");
        $input['tat_type'] = $this->input->post('param3');
        if ($input['tat_type'] == "Line") {
            $input['tat_type'] = $line;
        } else {
            $input['tat_type'] = $base;
        }

        $fleet = "'" .implode("','", $this->input->post('param2'))."'";
        $input['fleet'] =  $fleet;
        $input['param_airc'] = $this->input->post('param');
        $input['air_type'] = $this->input->post('param4');
        if ($input['air_type'] == 'All') {
            $input['air_type'] = "";
        }
        if ($input['param_airc'] == 'NON') {
            $input['param_airc'] = "";
            $value = $this->M_tat_airframe->get_value_nonga_datatable($input, $date, $data);
        } else {
            $value = $this->M_tat_airframe->get_value_ga_datatable($input, $date, $data);
        }

        if (count($value['data']) > 0) {
            // code...
            foreach ($value['data'] as $k) {
                // code...
				$con_date = date('d-m-Y', strtotime($k['start_date']));

                $print['data'][] = array(
                    $k['RowNum'],
					$con_date,
					$k['revision'],
                    $k['tipe'],
                    $k['register'],
                    $k['inspect'],
                    $k['target'],
                    $k['selisih']
                );
            }
        } else {
            $print['data'] = array();
        }

        $print['draw'] = $data['draw'];
        $print['recordsTotal'] = $print['recordsFiltered'] = $value['jml'];
        echo json_encode($print);
    }

}
