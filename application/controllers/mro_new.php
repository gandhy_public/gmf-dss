<?php
require_once(APPPATH.'libraries/PHPExcel.php');
defined('BASEPATH') OR exit('No direct script access allowed');


class Mro_new extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model("M_mro_new");
        $this->load->helper("file");
        // ini_set('memory_limit', '2048M');
        // ini_set('max_execution_time', -1);
        // ob_implicit_flush(true);
        // set_time_limit(0);
    }

    function index(){
        $data = array(
            "content" => "mro-new/upload-data",
            "title" => "MRO Market",
            "small_tittle" => "",
            "breadcrumb" => ["MRO Market"],
            "menu" => $this->M_menu->tampil(),
        );

        $this->load->view("layouts", $data);
    }

    function list_mro(){
      $list = $this->M_mro_new->get_datatables_mro();
      $records = $this->M_mro_new->count_filtered_mro();
      $data = array();
      $no   = $_POST['start'];
      foreach ($list as $mro) {
        $no++;
        $row   = array();
        $row[] = '<center>'.$no.'</center>';
        $row[] = $mro->year;
        $row[] = $mro->operator;
        $row[] = '<center>'.$mro->aircraft_type.'</center>';
        $row[] = '<center>'.$mro->engine_family.'</center>';
        $row[] = $mro->expense_type;
        $row[] = 'USD '.number_format($mro->total_cost, 2);
        $data[] = $row;
      }
      $output = array(
          "draw"            => $_POST['draw'],
          "recordsTotal"    => $records,
          "recordsFiltered" => $records,
          "data"            => $data,
      );
      echo json_encode($output);
    }

    // public function uploadFile(){
    //     $this->load->library('PHPExcel');
    //     $path = 'assets/upload-mro';
    //     $new_name_file=date("Ymd_His").'_'.$_FILES['files']['name'];
    //     $inputFileName="$path/$new_name_file";
    //     move_uploaded_file($_FILES['files']['tmp_name'], $inputFileName);
    //     $name_file_up = $_FILES['files']['name'];

    //     $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
    //     $cacheSettings = array( ' memoryCacheSize ' => '50MB');
    //     PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

    //     $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    //     $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    //     $chunkSize = 200001;
    //     $allDataInSheet = array();
    //     $dataOut = array();

    //     for ($startRow = 2; $startRow <= 200001; $startRow += $chunkSize) {
    //       $chunkFilter = new chunkReadFilter($startRow,$chunkSize);
    //       $objReader->setReadFilter($chunkFilter);
    //       $objReader->setReadDataOnly(true);
    //       $objPHPExcel = $objReader->load($inputFileName);
    //       $worksheet = $objPHPExcel->getActiveSheet();
    //       $highestRow = $worksheet->getHighestRow('A');
    //       if ($highestRow<=1) {
    //         break;
    //       }else{
    //         $allDataInSheet = $worksheet->toArray(null, true, true, true);
    //       }
    //       $objPHPExcel->disconnectWorksheets(); 
    //       unset($objPHPExcel); 
    //     }

    //     $arrayCount = count($allDataInSheet);

    //     if($arrayCount<=200001){
            
    //     $flag = 0;
    //     $createArray = array('Year', 'Operator', 'Region', 'Country', 'Aircraft Role', 'Aircraft Category', 'Manufacturer', 'Aircraft Family', 'Aircraft Group', 'Aircraft Type', 'Propulsion', 'Engine Manufacturer', 'Engine Family', 'Expense Category', 'Expense Type', 'Total Cost', 'Events', 'Material Spend', 'Labor Hours', 'Labor Spend');

    //     $SheetDataKey = array();
    //     foreach ($allDataInSheet as $dataInSheet) {
    //         foreach ($dataInSheet as $key => $value) {
    //             if (in_array(trim($value), $createArray)) {
    //                 $SheetDataKey[trim($value)] = $key;
    //             } else {

    //             }
    //         }
    //     }

    //     $makeArray = array('Year' => 'Year', 'Operator' => 'Operator', 'Region' => 'Region', 'Country' => 'Country', 'Aircraft Role' => 'Aircraft Role', 'Aircraft Category' => 'Aircraft Category', 'Manufacturer' => 'Manufacturer', 'Aircraft Family' => 'Aircraft Family', 'Aircraft Group' => 'Aircraft Group', 'Aircraft Type' => 'Aircraft Type', 'Propulsion' => 'Propulsion', 'Engine Manufacturer' => 'Engine Manufacturer', 'Engine Family' => 'Engine Family', 'Expense Category' => 'Expense Category', 'Expense Type' => 'Expense Type', 'Total Cost' => 'Total Cost', 'Events' => 'Events', 'Material Spend' => 'Material Spend', 'Labor Hours' => 'Labor Hours', 'Labor Spend' => 'Labor Spend');


    //     $data = array_diff_key($makeArray, $SheetDataKey);

    //     if (empty($data)) {
    //         $flag = 1;
    //     }

    //     $fetchData = array();
    //     if ($flag == 1) {
    //         for ($i = 2; $i <= $arrayCount; $i++) {
    //             $year = $SheetDataKey['Year'];
    //             $operator = $SheetDataKey['Operator'];
    //             $region = $SheetDataKey['Region'];
    //             $country = $SheetDataKey['Country'];
    //             $aircraft_role = $SheetDataKey['Aircraft Role'];
    //             $aircraft_category = $SheetDataKey['Aircraft Category'];
    //             $manufacturer = $SheetDataKey['Manufacturer'];
    //             $aircraft_family = $SheetDataKey['Aircraft Family'];
    //             $aircraft_group = $SheetDataKey['Aircraft Group'];
    //             $aircraft_type = $SheetDataKey['Aircraft Type'];
    //             $propulsion = $SheetDataKey['Propulsion'];
    //             $engine_manufacturer = $SheetDataKey['Engine Manufacturer'];
    //             $engine_family = $SheetDataKey['Engine Family'];
    //             $expense_category = $SheetDataKey['Expense Category'];
    //             $expense_type = $SheetDataKey['Expense Type'];
    //             $total_cost = $SheetDataKey['Total Cost'];
    //             $events = $SheetDataKey['Events'];
    //             $material_spend = $SheetDataKey['Material Spend'];
    //             $labor_hours = $SheetDataKey['Labor Hours'];
    //             $labor_spend = $SheetDataKey['Labor Spend'];
    //             $year = filter_var(trim($allDataInSheet[$i][$year]), FILTER_SANITIZE_STRING);
    //             $operator = filter_var(trim($allDataInSheet[$i][$operator]), FILTER_SANITIZE_STRING);
    //             $region = filter_var(trim($allDataInSheet[$i][$region]), FILTER_SANITIZE_EMAIL);
    //             $country = filter_var(trim($allDataInSheet[$i][$country]), FILTER_SANITIZE_STRING);
    //             $aircraft_role = filter_var(trim($allDataInSheet[$i][$aircraft_role]), FILTER_SANITIZE_STRING);
    //             $aircraft_category = filter_var(trim($allDataInSheet[$i][$aircraft_category]), FILTER_SANITIZE_STRING);
    //             $manufacturer = filter_var(trim($allDataInSheet[$i][$manufacturer]), FILTER_SANITIZE_STRING);
    //             $aircraft_family = filter_var(trim($allDataInSheet[$i][$aircraft_family]), FILTER_SANITIZE_STRING);
    //             $aircraft_group = filter_var(trim($allDataInSheet[$i][$aircraft_group]), FILTER_SANITIZE_STRING);
    //             $aircraft_type = filter_var(trim($allDataInSheet[$i][$aircraft_type]), FILTER_SANITIZE_STRING);
    //             $propulsion = filter_var(trim($allDataInSheet[$i][$propulsion]), FILTER_SANITIZE_STRING);
    //             $engine_manufacturer = filter_var(trim($allDataInSheet[$i][$engine_manufacturer]), FILTER_SANITIZE_STRING);
    //             $engine_family = filter_var(trim($allDataInSheet[$i][$engine_family]), FILTER_SANITIZE_STRING);
    //             $expense_category = filter_var(trim($allDataInSheet[$i][$expense_category]), FILTER_SANITIZE_STRING);
    //             $expense_type = filter_var(trim($allDataInSheet[$i][$expense_type]), FILTER_SANITIZE_STRING);
    //             $total_cost = filter_var(trim($allDataInSheet[$i][$total_cost]), FILTER_SANITIZE_STRING);
    //             $events = filter_var(trim($allDataInSheet[$i][$events]), FILTER_SANITIZE_STRING);
    //             $material_spend = filter_var(trim($allDataInSheet[$i][$material_spend]), FILTER_SANITIZE_STRING);
    //             $labor_hours = filter_var(trim($allDataInSheet[$i][$labor_hours]), FILTER_SANITIZE_STRING);
    //             $labor_spend = filter_var(trim($allDataInSheet[$i][$labor_spend]), FILTER_SANITIZE_STRING);

    //           if (strpos(strtolower($operator), "unknown") === false || strpos(strtolower($operator), "UNKNOWN") === false ||
    //               strpos(strtolower($region), "unknown") === false ||
    //               strpos(strtolower($country), "unknown") === false) {

    //               $fetchData[] = array(
    //                 'year' => $year,
    //                 'operator' => $operator,
    //                 'region' => $region,
    //                 'country' => $country,
    //                 'aircraft_role' => $aircraft_role,
    //                 'aircraft_category' => $aircraft_category,
    //                 'manufacturer' => $manufacturer,
    //                 'aircraft_family' => $aircraft_family,
    //                 'aircraft_group' => $aircraft_group,
    //                 'aircraft_type' => $aircraft_type,
    //                 'propulsion' => $propulsion,
    //                 'engine_manufacturer' => $engine_manufacturer,
    //                 'engine_family' => $engine_family,
    //                 'expense_category' => $expense_category,
    //                 'expense_type' => $expense_type,
    //                 'total_cost' => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
    //                 'events' => floatval(preg_replace("/[^-0-9\.]/","",$events)),
    //                 'material_spend' => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
    //                 'labor_hours' => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
    //                 'labor_spend' => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
    //                 );
    //             }
    //         };
    //         $exec = $this->M_mro_new->upload_data($fetchData);
    //         if ($exec) {
    //             $dataOut['sukses'] = true;
    //             $dataOut['message'] = "Sukses";
    //         }else {
    //             $dataOut['sukses'] = false;
    //             $dataOut['message'] = "Gagal";
    //         }
    //     }
    //     echo json_encode($dataOut);
    // }

    function upload_excel(){  
        ini_set("memory_limit", -1);
        ini_set('MAX_EXECUTION_TIME', -1);
        set_time_limit(0);
        //ini_set('sqlsrv.ClientBufferMaxKBSize','524288');
        $this->load->library('Excel_reader');
        $config = array(
            'upload_path'   => './assets/upload-mro',
            'allowed_types' => "xls",
            'file_name'     => url_title($this->input->post('files'))
            );
        $this->upload->initialize($config); 
       

        if ( ! $this->upload->do_upload('files')) {
                // jika validasi file gagal, kirim parameter error ke index
                $error = array('error' => $this->upload->display_errors());
                $this->index($error);
            } else {
             // jika berhasil upload ambil data dan masukkan ke database
              $upload_data = $this->upload->data();
 
 
              //tentukan file
              $this->excel_reader->setOutputEncoding('230787');
              $file = $upload_data['full_path'];
              $this->excel_reader->read($file);
              error_reporting(E_ALL ^ E_NOTICE);

                $notif = array();
                $notif_all = array();
 
                $sheet1 = $this->excel_reader->sheets;
                $dataexcel1 = Array();
                for ($i = 2; $i <= $sheet1['numRows']; $i++) {
                    if ($sheet1['cells'][$i][1] == '')
                       break;
                    $dataexcel1[$i - 2]['year']       = $sheet1['cells'][$i][1];
                    $dataexcel1[$i - 2]['operator']   = $sheet1['cells'][$i][2];
                    $dataexcel1[$i - 2]['region']     = $sheet1['cells'][$i][3];
                    $dataexcel1[$i - 2]['country']    = $sheet1['cells'][$i][4];
                }
                $exec1 = $this->M_mro_new->upload_data($dataexcel1);
                if($exec1){
                    $notif['message'] = 'Sheet 1 Sukses Upload';
                    array_push($notif_all, $notif);
                }else{
                    $notif['message'] = 'Sheet 1 Gagal Upload';
                    array_push($notif_all, $notif);
                }

                $sheet2 = $this->excel_reader->sheets[1];
                $dataexcel2 = Array();
                for ($i = 2; $i <= $sheet2['numRows']; $i++) {
                    if ($sheet2['cells'][$i][1] == '')
                       break;
                    $dataexcel2[$i - 2]['year']       = $sheet2['cells'][$i][1];
                    $dataexcel2[$i - 2]['operator']   = $sheet2['cells'][$i][2];
                    $dataexcel2[$i - 2]['region']     = $sheet2['cells'][$i][3];
                    $dataexcel2[$i - 2]['country']    = $sheet2['cells'][$i][4];
                }
                $exec2 = $this->M_mro_new->upload_data($dataexcel2);
                if($exec2){
                    $notif['message'] = 'Sheet 2 Sukses Upload';
                    array_push($notif_all, $notif);
                }else{
                    $notif['message'] = 'Sheet 2 Gagal Upload';
                    array_push($notif_all, $notif);
                }
              
               echo json_encode($notif_all);
                      
            }
        }

        function save_upload(){
            ini_set('memory_limit', -1);
            ini_set('MAX_EXECUTION_TIME', -1);
            set_time_limit(0);
            ini_set('sqlsrv.ClientBufferMaxKBSize','524288');
            $this->load->library('PHPExcel');
            $destination_folder = 'assets/upload-mro';
            $new_name_file = date("Y-m-d H-i-s") . '_' . $_FILES['files']['name'];
            $inputFileName = "$destination_folder/$new_name_file";
            move_uploaded_file($_FILES['files']['tmp_name'], $inputFileName);
            //$object =  PHPExcel_IOFactory::load($file);

            $notif = array();
            $notif_all = array();

            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($inputFileName);

            $sheet1 = $objPHPExcel->setActiveSheetIndex(0);
            $highestRowsheet1     = $sheet1->getHighestRow();
            $sheet2 = $objPHPExcel->setActiveSheetIndex(1);
            $highestRowsheet2     = $sheet2->getHighestRow();

            //print_r($sheet1) ;die;

                for($row=2; $row<=$highestRowsheet1; $row++){
                    $year                   = $sheet1->getCellByColumnAndRow(0, $row)->getValue();
                    $operator               = $sheet1->getCellByColumnAndRow(1, $row)->getValue();
                    $region                 = $sheet1->getCellByColumnAndRow(2, $row)->getValue();
                    $country                = $sheet1->getCellByColumnAndRow(3, $row)->getValue();
                    $aircraft_role          = $sheet1->getCellByColumnAndRow(4, $row)->getValue();
                    $aircraft_category      = $sheet1->getCellByColumnAndRow(5, $row)->getValue();
                    $manufacturer           = $sheet1->getCellByColumnAndRow(6, $row)->getValue();
                    $aircraft_family        = $sheet1->getCellByColumnAndRow(7, $row)->getValue();
                    $aircraft_group         = $sheet1->getCellByColumnAndRow(8, $row)->getValue();
                    $aircraft_type          = $sheet1->getCellByColumnAndRow(9, $row)->getValue();
                    $propulsion             = $sheet1->getCellByColumnAndRow(10, $row)->getValue();
                    $engine_manufacturer    = $sheet1->getCellByColumnAndRow(11, $row)->getValue();
                    $engine_family          = $sheet1->getCellByColumnAndRow(12, $row)->getValue();
                    $expense_category       = $sheet1->getCellByColumnAndRow(13, $row)->getValue();
                    $expense_type           = $sheet1->getCellByColumnAndRow(14, $row)->getValue();
                    $total_cost             = $sheet1->getCellByColumnAndRow(15, $row)->getValue();
                    $events                 = $sheet1->getCellByColumnAndRow(16, $row)->getValue();
                    $material_spend         = $sheet1->getCellByColumnAndRow(17, $row)->getValue();
                    $labor_hours            = $sheet1->getCellByColumnAndRow(18, $row)->getValue();
                    $labor_spend            = $sheet1->getCellByColumnAndRow(19, $row)->getValue();

                    $data1[] = array(
                        'year'                  => $year,
                        'operator'              => addslashes($operator),
                        'region'                => addslashes($region),
                        'country'               => addslashes($country),
                        'aircraft_role'         => addslashes($aircraft_role),
                        'aircraft_category'     => addslashes($aircraft_category),
                        'manufacturer'          => addslashes($manufacturer),
                        'aircraft_family'       => addslashes($aircraft_family),
                        'aircraft_group'        => addslashes($aircraft_group),
                        'aircraft_type'         => addslashes($aircraft_type),
                        'propulsion'            => addslashes($propulsion),
                        'engine_manufacturer'   => addslashes($engine_manufacturer),
                        'engine_family'         => addslashes($engine_family),
                        'expense_category'      => addslashes($expense_category),
                        'expense_type'          => addslashes($expense_type),
                        'total_cost'            => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
                        'events'                => floatval(preg_replace("/[^-0-9\.]/","",$events)),
                        'material_spend'        => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
                        'labor_hours'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
                        'labor_spend'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
                         );
                    }
                    $exec1 = $this->M_mro_new->upload_data($data1);
                    if($exec1){
                        $notif['message'] = 'Sukses Upload Sheet 1';
                        array_push($notif_all, $notif);
                    }else{
                        $notif['message'] = 'Gagal Upload Sheet 1';
                        array_push($notif_all, $notif);
                    }

                for($row=2; $row<=$highestRowsheet2; $row++){
                    $year                   = $sheet2->getCellByColumnAndRow(0, $row)->getValue();
                    $operator               = $sheet2->getCellByColumnAndRow(1, $row)->getValue();
                    $region                 = $sheet2->getCellByColumnAndRow(2, $row)->getValue();
                    $country                = $sheet2->getCellByColumnAndRow(3, $row)->getValue();
                    $aircraft_role          = $sheet2->getCellByColumnAndRow(4, $row)->getValue();
                    $aircraft_category      = $sheet2->getCellByColumnAndRow(5, $row)->getValue();
                    $manufacturer           = $sheet2->getCellByColumnAndRow(6, $row)->getValue();
                    $aircraft_family        = $sheet2->getCellByColumnAndRow(7, $row)->getValue();
                    $aircraft_group         = $sheet2->getCellByColumnAndRow(8, $row)->getValue();
                    $aircraft_type          = $sheet2->getCellByColumnAndRow(9, $row)->getValue();
                    $propulsion             = $sheet2->getCellByColumnAndRow(10, $row)->getValue();
                    $engine_manufacturer    = $sheet2->getCellByColumnAndRow(11, $row)->getValue();
                    $engine_family          = $sheet2->getCellByColumnAndRow(12, $row)->getValue();
                    $expense_category       = $sheet2->getCellByColumnAndRow(13, $row)->getValue();
                    $expense_type           = $sheet2->getCellByColumnAndRow(14, $row)->getValue();
                    $total_cost             = $sheet2->getCellByColumnAndRow(15, $row)->getValue();
                    $events                 = $sheet2->getCellByColumnAndRow(16, $row)->getValue();
                    $material_spend         = $sheet2->getCellByColumnAndRow(17, $row)->getValue();
                    $labor_hours            = $sheet2->getCellByColumnAndRow(18, $row)->getValue();
                    $labor_spend            = $sheet2->getCellByColumnAndRow(19, $row)->getValue();

                    $data2[] = array(
                        'year'                  => $year,
                        'operator'              => addslashes($operator),
                        'region'                => addslashes($region),
                        'country'               => addslashes($country),
                        'aircraft_role'         => addslashes($aircraft_role),
                        'aircraft_category'     => addslashes($aircraft_category),
                        'manufacturer'          => addslashes($manufacturer),
                        'aircraft_family'       => addslashes($aircraft_family),
                        'aircraft_group'        => addslashes($aircraft_group),
                        'aircraft_type'         => addslashes($aircraft_type),
                        'propulsion'            => addslashes($propulsion),
                        'engine_manufacturer'   => addslashes($engine_manufacturer),
                        'engine_family'         => addslashes($engine_family),
                        'expense_category'      => addslashes($expense_category),
                        'expense_type'          => addslashes($expense_type),
                        'total_cost'            => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
                        'events'                => floatval(preg_replace("/[^-0-9\.]/","",$events)),
                        'material_spend'        => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
                        'labor_hours'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
                        'labor_spend'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
                         );
                    }
                    $exec2 = $this->M_mro_new->upload_data($data2);
                    if($exec2){
                        $notif['message'] = 'Sukses Upload Sheet 1';
                        array_push($notif_all, $notif);
                    }else{
                        $notif['message'] = 'Gagal Upload Sheet 1';
                        array_push($notif_all, $notif);
                    }


            // if($object->getSheet(0)){
                

            
            // }
            // if($object->getSheet(1)){
            //     $sheet          = $object->getSheet(1);
            //     $highestRow     = $sheet->getHighestRow();
            //     $highestColumn  = $sheet->getHighestColumn();

            //     for($row=2; $row<=$highestRow; $row++){
            //         $year                   = $sheet->getCellByColumnAndRow(0, $row)->getValue();
            //         $operator               = $sheet->getCellByColumnAndRow(1, $row)->getValue();
            //         $region                 = $sheet->getCellByColumnAndRow(2, $row)->getValue();
            //         $country                = $sheet->getCellByColumnAndRow(3, $row)->getValue();
            //         $aircraft_role          = $sheet->getCellByColumnAndRow(4, $row)->getValue();
            //         $aircraft_category      = $sheet->getCellByColumnAndRow(5, $row)->getValue();
            //         $manufacturer           = $sheet->getCellByColumnAndRow(6, $row)->getValue();
            //         $aircraft_family        = $sheet->getCellByColumnAndRow(7, $row)->getValue();
            //         $aircraft_group         = $sheet->getCellByColumnAndRow(8, $row)->getValue();
            //         $aircraft_type          = $sheet->getCellByColumnAndRow(9, $row)->getValue();
            //         $propulsion             = $sheet->getCellByColumnAndRow(10, $row)->getValue();
            //         $engine_manufacturer    = $sheet->getCellByColumnAndRow(11, $row)->getValue();
            //         $engine_family          = $sheet->getCellByColumnAndRow(12, $row)->getValue();
            //         $expense_category       = $sheet->getCellByColumnAndRow(13, $row)->getValue();
            //         $expense_type           = $sheet->getCellByColumnAndRow(14, $row)->getValue();
            //         $total_cost             = $sheet->getCellByColumnAndRow(15, $row)->getValue();
            //         $events                 = $sheet->getCellByColumnAndRow(16, $row)->getValue();
            //         $material_spend         = $sheet->getCellByColumnAndRow(17, $row)->getValue();
            //         $labor_hours            = $sheet->getCellByColumnAndRow(18, $row)->getValue();
            //         $labor_spend            = $sheet->getCellByColumnAndRow(19, $row)->getValue();

            //         $data2[] = array(
            //             'year'                  => $year,
            //             'operator'              => addslashes($operator),
            //             'region'                => addslashes($region),
            //             'country'               => addslashes($country),
            //             'aircraft_role'         => addslashes($aircraft_role),
            //             'aircraft_category'     => addslashes($aircraft_category),
            //             'manufacturer'          => addslashes($manufacturer),
            //             'aircraft_family'       => addslashes($aircraft_family),
            //             'aircraft_group'        => addslashes($aircraft_group),
            //             'aircraft_type'         => addslashes($aircraft_type),
            //             'propulsion'            => addslashes($propulsion),
            //             'engine_manufacturer'   => addslashes($engine_manufacturer),
            //             'engine_family'         => addslashes($engine_family),
            //             'expense_category'      => addslashes($expense_category),
            //             'expense_type'          => addslashes($expense_type),
            //             'total_cost'            => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
            //             'events'                => floatval(preg_replace("/[^-0-9\.]/","",$events)),
            //             'material_spend'        => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
            //             'labor_hours'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
            //             'labor_spend'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
            //              );
            //         }
            //         $exec2 = $this->M_mro_new->upload_data($data2);
            //         if($exec2){
            //             $notif['message'] = 'Sukses Upload Sheet 2';
            //             array_push($notif_all, $notif);
            //         }else{
            //             $notif['message'] = 'Gagal Upload Sheet 2';
            //             array_push($notif_all, $notif);
            //         }
            // }
            // if($object->getSheet(2)){
            //     $sheet          = $object->getSheet(2);
            //     $highestRow     = $sheet->getHighestRow();
            //     $highestColumn  = $sheet->getHighestColumn();

            //     for($row=2; $row<=$highestRow; $row++){
            //         $year                   = $sheet->getCellByColumnAndRow(0, $row)->getValue();
            //         $operator               = $sheet->getCellByColumnAndRow(1, $row)->getValue();
            //         $region                 = $sheet->getCellByColumnAndRow(2, $row)->getValue();
            //         $country                = $sheet->getCellByColumnAndRow(3, $row)->getValue();
            //         $aircraft_role          = $sheet->getCellByColumnAndRow(4, $row)->getValue();
            //         $aircraft_category      = $sheet->getCellByColumnAndRow(5, $row)->getValue();
            //         $manufacturer           = $sheet->getCellByColumnAndRow(6, $row)->getValue();
            //         $aircraft_family        = $sheet->getCellByColumnAndRow(7, $row)->getValue();
            //         $aircraft_group         = $sheet->getCellByColumnAndRow(8, $row)->getValue();
            //         $aircraft_type          = $sheet->getCellByColumnAndRow(9, $row)->getValue();
            //         $propulsion             = $sheet->getCellByColumnAndRow(10, $row)->getValue();
            //         $engine_manufacturer    = $sheet->getCellByColumnAndRow(11, $row)->getValue();
            //         $engine_family          = $sheet->getCellByColumnAndRow(12, $row)->getValue();
            //         $expense_category       = $sheet->getCellByColumnAndRow(13, $row)->getValue();
            //         $expense_type           = $sheet->getCellByColumnAndRow(14, $row)->getValue();
            //         $total_cost             = $sheet->getCellByColumnAndRow(15, $row)->getValue();
            //         $events                 = $sheet->getCellByColumnAndRow(16, $row)->getValue();
            //         $material_spend         = $sheet->getCellByColumnAndRow(17, $row)->getValue();
            //         $labor_hours            = $sheet->getCellByColumnAndRow(18, $row)->getValue();
            //         $labor_spend            = $sheet->getCellByColumnAndRow(19, $row)->getValue();

            //         $data3[] = array(
            //             'year'                  => $year,
            //             'operator'              => addslashes($operator),
            //             'region'                => addslashes($region),
            //             'country'               => addslashes($country),
            //             'aircraft_role'         => addslashes($aircraft_role),
            //             'aircraft_category'     => addslashes($aircraft_category),
            //             'manufacturer'          => addslashes($manufacturer),
            //             'aircraft_family'       => addslashes($aircraft_family),
            //             'aircraft_group'        => addslashes($aircraft_group),
            //             'aircraft_type'         => addslashes($aircraft_type),
            //             'propulsion'            => addslashes($propulsion),
            //             'engine_manufacturer'   => addslashes($engine_manufacturer),
            //             'engine_family'         => addslashes($engine_family),
            //             'expense_category'      => addslashes($expense_category),
            //             'expense_type'          => addslashes($expense_type),
            //             'total_cost'            => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
            //             'events'                => floatval(preg_replace("/[^-0-9\.]/","",$events)),
            //             'material_spend'        => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
            //             'labor_hours'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
            //             'labor_spend'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
            //              );
            //         }
            //         $exec3 = $this->M_mro_new->upload_data($data3);
            //         if($exec3){
            //             $notif['message'] = 'Sukses Upload Sheet 3';
            //             array_push($notif_all, $notif);
            //         }else{
            //             $notif['message'] = 'Gagal Upload Sheet 3';
            //             array_push($notif_all, $notif);
            //         }
            // }
            // if($object->getSheet(3)){
            //     $sheet          = $object->getSheet(3);
            //     $highestRow     = $sheet->getHighestRow();
            //     $highestColumn  = $sheet->getHighestColumn();

            //     for($row=2; $row<=$highestRow; $row++){
            //         $year                   = $sheet->getCellByColumnAndRow(0, $row)->getValue();
            //         $operator               = $sheet->getCellByColumnAndRow(1, $row)->getValue();
            //         $region                 = $sheet->getCellByColumnAndRow(2, $row)->getValue();
            //         $country                = $sheet->getCellByColumnAndRow(3, $row)->getValue();
            //         $aircraft_role          = $sheet->getCellByColumnAndRow(4, $row)->getValue();
            //         $aircraft_category      = $sheet->getCellByColumnAndRow(5, $row)->getValue();
            //         $manufacturer           = $sheet->getCellByColumnAndRow(6, $row)->getValue();
            //         $aircraft_family        = $sheet->getCellByColumnAndRow(7, $row)->getValue();
            //         $aircraft_group         = $sheet->getCellByColumnAndRow(8, $row)->getValue();
            //         $aircraft_type          = $sheet->getCellByColumnAndRow(9, $row)->getValue();
            //         $propulsion             = $sheet->getCellByColumnAndRow(10, $row)->getValue();
            //         $engine_manufacturer    = $sheet->getCellByColumnAndRow(11, $row)->getValue();
            //         $engine_family          = $sheet->getCellByColumnAndRow(12, $row)->getValue();
            //         $expense_category       = $sheet->getCellByColumnAndRow(13, $row)->getValue();
            //         $expense_type           = $sheet->getCellByColumnAndRow(14, $row)->getValue();
            //         $total_cost             = $sheet->getCellByColumnAndRow(15, $row)->getValue();
            //         $events                 = $sheet->getCellByColumnAndRow(16, $row)->getValue();
            //         $material_spend         = $sheet->getCellByColumnAndRow(17, $row)->getValue();
            //         $labor_hours            = $sheet->getCellByColumnAndRow(18, $row)->getValue();
            //         $labor_spend            = $sheet->getCellByColumnAndRow(19, $row)->getValue();

            //         $data4[] = array(
            //             'year'                  => $year,
            //             'operator'              => addslashes($operator),
            //             'region'                => addslashes($region),
            //             'country'               => addslashes($country),
            //             'aircraft_role'         => addslashes($aircraft_role),
            //             'aircraft_category'     => addslashes($aircraft_category),
            //             'manufacturer'          => addslashes($manufacturer),
            //             'aircraft_family'       => addslashes($aircraft_family),
            //             'aircraft_group'        => addslashes($aircraft_group),
            //             'aircraft_type'         => addslashes($aircraft_type),
            //             'propulsion'            => addslashes($propulsion),
            //             'engine_manufacturer'   => addslashes($engine_manufacturer),
            //             'engine_family'         => addslashes($engine_family),
            //             'expense_category'      => addslashes($expense_category),
            //             'expense_type'          => addslashes($expense_type),
            //             'total_cost'            => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
            //             'events'                => floatval(preg_replace("/[^-0-9\.]/","",$events)),
            //             'material_spend'        => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
            //             'labor_hours'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
            //             'labor_spend'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
            //              );
            //         }
            //         $exec4 = $this->M_mro_new->upload_data($data4);
            //         if($exec4){
            //             $notif['message'] = 'Sukses Upload Sheet 4';
            //             array_push($notif_all, $notif);
            //         }else{
            //             $notif['message'] = 'Gagal Upload Sheet 4';
            //             array_push($notif_all, $notif);
            //         }
            // }
            // if($object->getSheet(4)){
            //     $sheet          = $object->getSheet(4);
            //     $highestRow     = $sheet->getHighestRow();
            //     $highestColumn  = $sheet->getHighestColumn();

            //     for($row=2; $row<=$highestRow; $row++){
            //         $year                   = $sheet->getCellByColumnAndRow(0, $row)->getValue();
            //         $operator               = $sheet->getCellByColumnAndRow(1, $row)->getValue();
            //         $region                 = $sheet->getCellByColumnAndRow(2, $row)->getValue();
            //         $country                = $sheet->getCellByColumnAndRow(3, $row)->getValue();
            //         $aircraft_role          = $sheet->getCellByColumnAndRow(4, $row)->getValue();
            //         $aircraft_category      = $sheet->getCellByColumnAndRow(5, $row)->getValue();
            //         $manufacturer           = $sheet->getCellByColumnAndRow(6, $row)->getValue();
            //         $aircraft_family        = $sheet->getCellByColumnAndRow(7, $row)->getValue();
            //         $aircraft_group         = $sheet->getCellByColumnAndRow(8, $row)->getValue();
            //         $aircraft_type          = $sheet->getCellByColumnAndRow(9, $row)->getValue();
            //         $propulsion             = $sheet->getCellByColumnAndRow(10, $row)->getValue();
            //         $engine_manufacturer    = $sheet->getCellByColumnAndRow(11, $row)->getValue();
            //         $engine_family          = $sheet->getCellByColumnAndRow(12, $row)->getValue();
            //         $expense_category       = $sheet->getCellByColumnAndRow(13, $row)->getValue();
            //         $expense_type           = $sheet->getCellByColumnAndRow(14, $row)->getValue();
            //         $total_cost             = $sheet->getCellByColumnAndRow(15, $row)->getValue();
            //         $events                 = $sheet->getCellByColumnAndRow(16, $row)->getValue();
            //         $material_spend         = $sheet->getCellByColumnAndRow(17, $row)->getValue();
            //         $labor_hours            = $sheet->getCellByColumnAndRow(18, $row)->getValue();
            //         $labor_spend            = $sheet->getCellByColumnAndRow(19, $row)->getValue();

            //         $data5[] = array(
            //             'year'                  => $year,
            //             'operator'              => addslashes($operator),
            //             'region'                => addslashes($region),
            //             'country'               => addslashes($country),
            //             'aircraft_role'         => addslashes($aircraft_role),
            //             'aircraft_category'     => addslashes($aircraft_category),
            //             'manufacturer'          => addslashes($manufacturer),
            //             'aircraft_family'       => addslashes($aircraft_family),
            //             'aircraft_group'        => addslashes($aircraft_group),
            //             'aircraft_type'         => addslashes($aircraft_type),
            //             'propulsion'            => addslashes($propulsion),
            //             'engine_manufacturer'   => addslashes($engine_manufacturer),
            //             'engine_family'         => addslashes($engine_family),
            //             'expense_category'      => addslashes($expense_category),
            //             'expense_type'          => addslashes($expense_type),
            //             'total_cost'            => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
            //             'events'                => floatval(preg_replace("/[^-0-9\.]/","",$events)),
            //             'material_spend'        => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
            //             'labor_hours'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
            //             'labor_spend'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
            //              );
            //         }
            //         $exec5 = $this->M_mro_new->upload_data($data5);
            //         if($exec5){
            //             $notif['message'] = 'Sukses Upload Sheet 5';
            //             array_push($notif_all, $notif);
            //         }else{
            //             $notif['message'] = 'Gagal Upload Sheet 5';
            //             array_push($notif_all, $notif);
            //         }
            // }
            // if($object->getSheet(5)){
            //     $sheet          = $object->getSheet(5);
            //     $highestRow     = $sheet->getHighestRow();
            //     $highestColumn  = $sheet->getHighestColumn();

            //     for($row=2; $row<=$highestRow; $row++){
            //         $year                   = $sheet->getCellByColumnAndRow(0, $row)->getValue();
            //         $operator               = $sheet->getCellByColumnAndRow(1, $row)->getValue();
            //         $region                 = $sheet->getCellByColumnAndRow(2, $row)->getValue();
            //         $country                = $sheet->getCellByColumnAndRow(3, $row)->getValue();
            //         $aircraft_role          = $sheet->getCellByColumnAndRow(4, $row)->getValue();
            //         $aircraft_category      = $sheet->getCellByColumnAndRow(5, $row)->getValue();
            //         $manufacturer           = $sheet->getCellByColumnAndRow(6, $row)->getValue();
            //         $aircraft_family        = $sheet->getCellByColumnAndRow(7, $row)->getValue();
            //         $aircraft_group         = $sheet->getCellByColumnAndRow(8, $row)->getValue();
            //         $aircraft_type          = $sheet->getCellByColumnAndRow(9, $row)->getValue();
            //         $propulsion             = $sheet->getCellByColumnAndRow(10, $row)->getValue();
            //         $engine_manufacturer    = $sheet->getCellByColumnAndRow(11, $row)->getValue();
            //         $engine_family          = $sheet->getCellByColumnAndRow(12, $row)->getValue();
            //         $expense_category       = $sheet->getCellByColumnAndRow(13, $row)->getValue();
            //         $expense_type           = $sheet->getCellByColumnAndRow(14, $row)->getValue();
            //         $total_cost             = $sheet->getCellByColumnAndRow(15, $row)->getValue();
            //         $events                 = $sheet->getCellByColumnAndRow(16, $row)->getValue();
            //         $material_spend         = $sheet->getCellByColumnAndRow(17, $row)->getValue();
            //         $labor_hours            = $sheet->getCellByColumnAndRow(18, $row)->getValue();
            //         $labor_spend            = $sheet->getCellByColumnAndRow(19, $row)->getValue();

            //         $data6[] = array(
            //             'year'                  => $year,
            //             'operator'              => addslashes($operator),
            //             'region'                => addslashes($region),
            //             'country'               => addslashes($country),
            //             'aircraft_role'         => addslashes($aircraft_role),
            //             'aircraft_category'     => addslashes($aircraft_category),
            //             'manufacturer'          => addslashes($manufacturer),
            //             'aircraft_family'       => addslashes($aircraft_family),
            //             'aircraft_group'        => addslashes($aircraft_group),
            //             'aircraft_type'         => addslashes($aircraft_type),
            //             'propulsion'            => addslashes($propulsion),
            //             'engine_manufacturer'   => addslashes($engine_manufacturer),
            //             'engine_family'         => addslashes($engine_family),
            //             'expense_category'      => addslashes($expense_category),
            //             'expense_type'          => addslashes($expense_type),
            //             'total_cost'            => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
            //             'events'                => floatval(preg_replace("/[^-0-9\.]/","",$events)),
            //             'material_spend'        => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
            //             'labor_hours'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
            //             'labor_spend'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
            //              );
            //         }
            //         $exec6 = $this->M_mro_new->upload_data($data6);
            //         if($exec6){
            //             $notif['message'] = 'Sukses Upload Sheet 6';
            //             array_push($notif_all, $notif);
            //         }else{
            //             $notif['message'] = 'Gagal Upload Sheet 6';
            //             array_push($notif_all, $notif);
            //         }
            // }
            // if($object->getSheet(6)){
            //     $sheet          = $object->getSheet(6);
            //     $highestRow     = $sheet->getHighestRow();
            //     $highestColumn  = $sheet->getHighestColumn();

            //     for($row=2; $row<=$highestRow; $row++){
            //         $year                   = $sheet->getCellByColumnAndRow(0, $row)->getValue();
            //         $operator               = $sheet->getCellByColumnAndRow(1, $row)->getValue();
            //         $region                 = $sheet->getCellByColumnAndRow(2, $row)->getValue();
            //         $country                = $sheet->getCellByColumnAndRow(3, $row)->getValue();
            //         $aircraft_role          = $sheet->getCellByColumnAndRow(4, $row)->getValue();
            //         $aircraft_category      = $sheet->getCellByColumnAndRow(5, $row)->getValue();
            //         $manufacturer           = $sheet->getCellByColumnAndRow(6, $row)->getValue();
            //         $aircraft_family        = $sheet->getCellByColumnAndRow(7, $row)->getValue();
            //         $aircraft_group         = $sheet->getCellByColumnAndRow(8, $row)->getValue();
            //         $aircraft_type          = $sheet->getCellByColumnAndRow(9, $row)->getValue();
            //         $propulsion             = $sheet->getCellByColumnAndRow(10, $row)->getValue();
            //         $engine_manufacturer    = $sheet->getCellByColumnAndRow(11, $row)->getValue();
            //         $engine_family          = $sheet->getCellByColumnAndRow(12, $row)->getValue();
            //         $expense_category       = $sheet->getCellByColumnAndRow(13, $row)->getValue();
            //         $expense_type           = $sheet->getCellByColumnAndRow(14, $row)->getValue();
            //         $total_cost             = $sheet->getCellByColumnAndRow(15, $row)->getValue();
            //         $events                 = $sheet->getCellByColumnAndRow(16, $row)->getValue();
            //         $material_spend         = $sheet->getCellByColumnAndRow(17, $row)->getValue();
            //         $labor_hours            = $sheet->getCellByColumnAndRow(18, $row)->getValue();
            //         $labor_spend            = $sheet->getCellByColumnAndRow(19, $row)->getValue();

            //         $data7[] = array(
            //             'year'                  => $year,
            //             'operator'              => addslashes($operator),
            //             'region'                => addslashes($region),
            //             'country'               => addslashes($country),
            //             'aircraft_role'         => addslashes($aircraft_role),
            //             'aircraft_category'     => addslashes($aircraft_category),
            //             'manufacturer'          => addslashes($manufacturer),
            //             'aircraft_family'       => addslashes($aircraft_family),
            //             'aircraft_group'        => addslashes($aircraft_group),
            //             'aircraft_type'         => addslashes($aircraft_type),
            //             'propulsion'            => addslashes($propulsion),
            //             'engine_manufacturer'   => addslashes($engine_manufacturer),
            //             'engine_family'         => addslashes($engine_family),
            //             'expense_category'      => addslashes($expense_category),
            //             'expense_type'          => addslashes($expense_type),
            //             'total_cost'            => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
            //             'events'                => floatval(preg_replace("/[^-0-9\.]/","",$events)),
            //             'material_spend'        => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
            //             'labor_hours'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
            //             'labor_spend'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
            //              );
            //         }
            //         $exec7 = $this->M_mro_new->upload_data($data7);
            //         if($exec7){
            //             $notif['message'] = 'Sukses Upload Sheet 7';
            //             array_push($notif_all, $notif);
            //         }else{
            //             $notif['message'] = 'Gagal Upload Sheet 7';
            //             array_push($notif_all, $notif);
            //         }
            // }
            // if($object->getSheet(7)){
            //     $sheet          = $object->getSheet(7);
            //     $highestRow     = $sheet->getHighestRow();
            //     $highestColumn  = $sheet->getHighestColumn();

            //     for($row=2; $row<=$highestRow; $row++){
            //         $year                   = $sheet->getCellByColumnAndRow(0, $row)->getValue();
            //         $operator               = $sheet->getCellByColumnAndRow(1, $row)->getValue();
            //         $region                 = $sheet->getCellByColumnAndRow(2, $row)->getValue();
            //         $country                = $sheet->getCellByColumnAndRow(3, $row)->getValue();
            //         $aircraft_role          = $sheet->getCellByColumnAndRow(4, $row)->getValue();
            //         $aircraft_category      = $sheet->getCellByColumnAndRow(5, $row)->getValue();
            //         $manufacturer           = $sheet->getCellByColumnAndRow(6, $row)->getValue();
            //         $aircraft_family        = $sheet->getCellByColumnAndRow(7, $row)->getValue();
            //         $aircraft_group         = $sheet->getCellByColumnAndRow(8, $row)->getValue();
            //         $aircraft_type          = $sheet->getCellByColumnAndRow(9, $row)->getValue();
            //         $propulsion             = $sheet->getCellByColumnAndRow(10, $row)->getValue();
            //         $engine_manufacturer    = $sheet->getCellByColumnAndRow(11, $row)->getValue();
            //         $engine_family          = $sheet->getCellByColumnAndRow(12, $row)->getValue();
            //         $expense_category       = $sheet->getCellByColumnAndRow(13, $row)->getValue();
            //         $expense_type           = $sheet->getCellByColumnAndRow(14, $row)->getValue();
            //         $total_cost             = $sheet->getCellByColumnAndRow(15, $row)->getValue();
            //         $events                 = $sheet->getCellByColumnAndRow(16, $row)->getValue();
            //         $material_spend         = $sheet->getCellByColumnAndRow(17, $row)->getValue();
            //         $labor_hours            = $sheet->getCellByColumnAndRow(18, $row)->getValue();
            //         $labor_spend            = $sheet->getCellByColumnAndRow(19, $row)->getValue();

            //         $data8[] = array(
            //             'year'                  => $year,
            //             'operator'              => addslashes($operator),
            //             'region'                => addslashes($region),
            //             'country'               => addslashes($country),
            //             'aircraft_role'         => addslashes($aircraft_role),
            //             'aircraft_category'     => addslashes($aircraft_category),
            //             'manufacturer'          => addslashes($manufacturer),
            //             'aircraft_family'       => addslashes($aircraft_family),
            //             'aircraft_group'        => addslashes($aircraft_group),
            //             'aircraft_type'         => addslashes($aircraft_type),
            //             'propulsion'            => addslashes($propulsion),
            //             'engine_manufacturer'   => addslashes($engine_manufacturer),
            //             'engine_family'         => addslashes($engine_family),
            //             'expense_category'      => addslashes($expense_category),
            //             'expense_type'          => addslashes($expense_type),
            //             'total_cost'            => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
            //             'events'                => floatval(preg_replace("/[^-0-9\.]/","",$events)),
            //             'material_spend'        => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
            //             'labor_hours'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
            //             'labor_spend'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
            //              );
            //         }
            //         $exec8 = $this->M_mro_new->upload_data($data8);
            //         if($exec8){
            //             $notif['message'] = 'Sukses Upload Sheet 8';
            //             array_push($notif_all, $notif);
            //         }else{
            //             $notif['message'] = 'Gagal Upload Sheet 8';
            //             array_push($notif_all, $notif);
            //         }
            // }
            // if($object->getSheet(8)){
            //     $sheet          = $object->getSheet(8);
            //     $highestRow     = $sheet->getHighestRow();
            //     $highestColumn  = $sheet->getHighestColumn();

            //     for($row=2; $row<=$highestRow; $row++){
            //         $year                   = $sheet->getCellByColumnAndRow(0, $row)->getValue();
            //         $operator               = $sheet->getCellByColumnAndRow(1, $row)->getValue();
            //         $region                 = $sheet->getCellByColumnAndRow(2, $row)->getValue();
            //         $country                = $sheet->getCellByColumnAndRow(3, $row)->getValue();
            //         $aircraft_role          = $sheet->getCellByColumnAndRow(4, $row)->getValue();
            //         $aircraft_category      = $sheet->getCellByColumnAndRow(5, $row)->getValue();
            //         $manufacturer           = $sheet->getCellByColumnAndRow(6, $row)->getValue();
            //         $aircraft_family        = $sheet->getCellByColumnAndRow(7, $row)->getValue();
            //         $aircraft_group         = $sheet->getCellByColumnAndRow(8, $row)->getValue();
            //         $aircraft_type          = $sheet->getCellByColumnAndRow(9, $row)->getValue();
            //         $propulsion             = $sheet->getCellByColumnAndRow(10, $row)->getValue();
            //         $engine_manufacturer    = $sheet->getCellByColumnAndRow(11, $row)->getValue();
            //         $engine_family          = $sheet->getCellByColumnAndRow(12, $row)->getValue();
            //         $expense_category       = $sheet->getCellByColumnAndRow(13, $row)->getValue();
            //         $expense_type           = $sheet->getCellByColumnAndRow(14, $row)->getValue();
            //         $total_cost             = $sheet->getCellByColumnAndRow(15, $row)->getValue();
            //         $events                 = $sheet->getCellByColumnAndRow(16, $row)->getValue();
            //         $material_spend         = $sheet->getCellByColumnAndRow(17, $row)->getValue();
            //         $labor_hours            = $sheet->getCellByColumnAndRow(18, $row)->getValue();
            //         $labor_spend            = $sheet->getCellByColumnAndRow(19, $row)->getValue();

            //         $data9[] = array(
            //             'year'                  => $year,
            //             'operator'              => addslashes($operator),
            //             'region'                => addslashes($region),
            //             'country'               => addslashes($country),
            //             'aircraft_role'         => addslashes($aircraft_role),
            //             'aircraft_category'     => addslashes($aircraft_category),
            //             'manufacturer'          => addslashes($manufacturer),
            //             'aircraft_family'       => addslashes($aircraft_family),
            //             'aircraft_group'        => addslashes($aircraft_group),
            //             'aircraft_type'         => addslashes($aircraft_type),
            //             'propulsion'            => addslashes($propulsion),
            //             'engine_manufacturer'   => addslashes($engine_manufacturer),
            //             'engine_family'         => addslashes($engine_family),
            //             'expense_category'      => addslashes($expense_category),
            //             'expense_type'          => addslashes($expense_type),
            //             'total_cost'            => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
            //             'events'                => floatval(preg_replace("/[^-0-9\.]/","",$events)),
            //             'material_spend'        => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
            //             'labor_hours'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
            //             'labor_spend'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
            //              );
            //         }
            //         $exec9 = $this->M_mro_new->upload_data($data9);
            //         if($exec9){
            //             $notif['message'] = 'Sukses Upload Sheet 9';
            //             array_push($notif_all, $notif);
            //         }else{
            //             $notif['message'] = 'Gagal Upload Sheet 9';
            //             array_push($notif_all, $notif);
            //         }
            // }
            // if($object->getSheet(9)){
            //     $sheet          = $object->getSheet(9);
            //     $highestRow     = $sheet->getHighestRow();
            //     $highestColumn  = $sheet->getHighestColumn();

            //     for($row=2; $row<=$highestRow; $row++){
            //         $year                   = $sheet->getCellByColumnAndRow(0, $row)->getValue();
            //         $operator               = $sheet->getCellByColumnAndRow(1, $row)->getValue();
            //         $region                 = $sheet->getCellByColumnAndRow(2, $row)->getValue();
            //         $country                = $sheet->getCellByColumnAndRow(3, $row)->getValue();
            //         $aircraft_role          = $sheet->getCellByColumnAndRow(4, $row)->getValue();
            //         $aircraft_category      = $sheet->getCellByColumnAndRow(5, $row)->getValue();
            //         $manufacturer           = $sheet->getCellByColumnAndRow(6, $row)->getValue();
            //         $aircraft_family        = $sheet->getCellByColumnAndRow(7, $row)->getValue();
            //         $aircraft_group         = $sheet->getCellByColumnAndRow(8, $row)->getValue();
            //         $aircraft_type          = $sheet->getCellByColumnAndRow(9, $row)->getValue();
            //         $propulsion             = $sheet->getCellByColumnAndRow(10, $row)->getValue();
            //         $engine_manufacturer    = $sheet->getCellByColumnAndRow(11, $row)->getValue();
            //         $engine_family          = $sheet->getCellByColumnAndRow(12, $row)->getValue();
            //         $expense_category       = $sheet->getCellByColumnAndRow(13, $row)->getValue();
            //         $expense_type           = $sheet->getCellByColumnAndRow(14, $row)->getValue();
            //         $total_cost             = $sheet->getCellByColumnAndRow(15, $row)->getValue();
            //         $events                 = $sheet->getCellByColumnAndRow(16, $row)->getValue();
            //         $material_spend         = $sheet->getCellByColumnAndRow(17, $row)->getValue();
            //         $labor_hours            = $sheet->getCellByColumnAndRow(18, $row)->getValue();
            //         $labor_spend            = $sheet->getCellByColumnAndRow(19, $row)->getValue();

            //         $data10[] = array(
            //             'year'                  => $year,
            //             'operator'              => addslashes($operator),
            //             'region'                => addslashes($region),
            //             'country'               => addslashes($country),
            //             'aircraft_role'         => addslashes($aircraft_role),
            //             'aircraft_category'     => addslashes($aircraft_category),
            //             'manufacturer'          => addslashes($manufacturer),
            //             'aircraft_family'       => addslashes($aircraft_family),
            //             'aircraft_group'        => addslashes($aircraft_group),
            //             'aircraft_type'         => addslashes($aircraft_type),
            //             'propulsion'            => addslashes($propulsion),
            //             'engine_manufacturer'   => addslashes($engine_manufacturer),
            //             'engine_family'         => addslashes($engine_family),
            //             'expense_category'      => addslashes($expense_category),
            //             'expense_type'          => addslashes($expense_type),
            //             'total_cost'            => floatval(preg_replace("/[^-0-9\.]/","",$total_cost)),
            //             'events'                => floatval(preg_replace("/[^-0-9\.]/","",$events)),
            //             'material_spend'        => floatval(preg_replace("/[^-0-9\.]/","",$material_spend)),
            //             'labor_hours'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_hours)),
            //             'labor_spend'           => floatval(preg_replace("/[^-0-9\.]/","",$labor_spend))
            //              );
            //         }
            //         $exec10= $this->M_mro_new->upload_data($data10);
            //         if($exec10){
            //             $notif['message'] = 'Sukses Upload Sheet 10';
            //             array_push($notif_all, $notif);
            //         }else{
            //             $notif['message'] = 'Gagal Upload Sheet 10';
            //             array_push($notif_all, $notif);
            //         }
            // }
           echo json_encode($notif_all);
        }
    public function uploadFile(){
        $this->load->library('PHPExcel');

        $path = 'assets/upload-mro';

        $new_name_file=date("Ymd_His").'_'.$_FILES['files']['name'];
        $inputFileName="$path/$new_name_file";
        move_uploaded_file($_FILES['files']['tmp_name'], $inputFileName);

        // $config['upload_path'] = $path;
        // $config['allowed_types'] = '*';
        // $config['remove_spaces'] = TRUE;
        // $config['encrypt_name'] = TRUE;
        // // $config['max_size']      = 50;

        // $this->load->library('upload', $config);
        // $this->upload->initialize($config);

        // // $this->load->library('upload', $config);
        // // $new_name_file=date("Ymd_His").'_'.$_FILES(['files']['name']);
        $name_file_up = $_FILES['files']['name'];
        // if ($this->upload->do_upload('files')) {
        //     $data = array('upload_data' => $this->upload->data());
        // } else {
        //     $error = array('error' => $this->upload->display_errors());
        //     print_r($error);
        //     exit();
        // }
        // // echo "Data: ".json_encode($data)."\n";
        // if (!empty($data['upload_data']['file_name'])) {
        //     $import_xls_file = $data['upload_data']['file_name'];
        // } else {
        //     $import_xls_file = 0;
        // }
        // // echo "File Name: ".$import_xls_file."\n";
        // $inputFileName = $path . $import_xls_file;
        // echo "Path File Name:".$inputFileName."\n";z

            $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
            $cacheSettings = array( ' memoryCacheSize ' => '8MB');
            PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
// echo 'Loading file ',pathinfo($inputFileName,PATHINFO_BASENAME),' using IOFactory with a defined reader type of ',$inputFileType,'<br />';
/**  Create a new Reader of the type defined in $inputFileType  **/
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
// echo '<hr />';
/**  Define how many rows we want for each "chunk"  **/
$chunkSize = 100001;
/**  Loop to read our worksheet in "chunk size" blocks  **/
$allDataInSheet = array();
// $allDataInSheet2 = array();
for ($startRow = 2; $startRow <= 50000; $startRow += $chunkSize) {
  // echo 'Loading WorkSheet using configurable filter for headings row 1 and for rows ',$startRow,' to ',($startRow+$chunkSize-1),'<br />';
  /**  Create a new Instance of our Read Filter, passing in the limits on which rows we want to read  **/
  $chunkFilter = new chunkReadFilter($startRow,$chunkSize);
  /**  Tell the Reader that we want to use the new Read Filter that we've just Instantiated  **/
  $objReader->setReadFilter($chunkFilter);
  $objReader->setReadDataOnly(true);
  /**  Load only the rows that match our filter from $inputFileName to a PHPExcel Object  **/
  $objPHPExcel = $objReader->load($inputFileName);
  $worksheet = $objPHPExcel->getActiveSheet();

  $highestRow = $worksheet->getHighestRow('A');
  // $highestColumn = $worksheet->getHighestColumn();
  //  Do some processing here
  // $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
  // var_dump($sheetData);
  // echo '<br /><br />';
  // echo "startRow: ".$startRow."\n";
  // echo "highestRow: ".$highestRow."\n";
  // echo "startRow-chunkSize: ".($startRow+$chunkSize-1);

  if ($highestRow<=1) {
    // echo "\nbreak\n";
    break;
  }else{
    $allDataInSheet[0] = $worksheet->toArray(null, true, true, true);
    // $allDataInSheet[0] = $worksheet->toArray(
    //     'A'.$startRow.':'.$highestColumn.$highestRow,
    //     null,
    //     true,
    //     true,
    //     true
    // );
    // array_push($allDataInSheet, $dataInSheet);
    // array_push($allDataInSheet, $dataInSheet[0]);
  }
  // if ($highestRow==31) {
  //   break;
  // }

  // if (count($allDataInSheet[0]) % 1000 != 1) {
  //   break;
  // }

  // $allDataSheet[] = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

  // print_r($allDataSheet);
  // if (count($allDataSheet) % 1000 != 1) {
  //   break;
  // }else{
  //   $allDataInSheet[0] = $allDataSheet;
  // }
  // echo "count: ".count($allDataSheet);

  // echo "\ncount: ".count($allDataInSheet)."\n";
  //    Free up some of the memory 
  // $objPHPExcel->disconnectWorksheets(); 
  // unset($objPHPExcel); 
}
  // echo "countAll: ".count($allDataInSheet);
  // echo "<pre>";
  // print_r($allDataInSheet);
  // echo "</pre>";
  // echo "<pre>";
  // print_r($allDataInSheet2);
  // echo "</pre>";
// exit();

//         try {
//             $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
//             $cacheSettings = array( ' memoryCacheSize ' => '8MB');
//             PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
//             $inputFileType = PHPExcel_IOFactory::identify($inputFileName);

//             /**  Create a new Reader of the type defined in $inputFileType  **/
//             $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//             /**  Define how many rows we want to read for each "chunk"  **/ 
//             $chunkSize = 20;
//             /**  Create a new Instance of our Read Filter  **/ 
//             $chunkFilter = new chunkReadFilter(); 
//             /**  Tell the Reader that we want to use the Read Filter that we've Instantiated  **/ 
//             $objReader->setReadFilter($chunkFilter); 

//             /**  Loop to read our worksheet in "chunk size" blocks  **/ 
//             /**  $startRow is set to 2 initially because we always read the headings in row #1  **/
//             for ($startRow = 2; $startRow <= 1000; $startRow += $chunkSize) { 
//                 *  Tell the Read Filter, the limits on which rows we want to read this iteration  * 
//                 $chunkFilter->setRows($startRow,$chunkSize); 
//                 /**  Load only the rows that match our filter from $inputFileName to a PHPExcel Object  **/ 
//                 $objPHPExcel = $objReader->load($inputFileName); 
//                 //    Do some processing here 
//                 echo $startRow;
//                 //    Free up some of the memory 
//                 $objPHPExcel->disconnectWorksheets(); 
//                 unset($objPHPExcel); 
//             }

//             // $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//             // $objReader->setReadDataOnly(true);
//             // $objReader->setReadDataOnly(true);
//             // $objPHPExcel = $objReader->load($inputFileName);
            $dataOut['sukses'] = true;
//         } catch (Exception $e) {
//             if (!empty($inputFileName)) {
//               unlink($inputFileName);
//             }
//             $dataOut['sukses'] = false;
//             $dataOut['message'] = "Error Read Data";
//             echo json_encode($dataOut);
//             die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
//                     . '": ' . $e->getMessage());
//         }
        // $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            // echo "<pre>";
            // print_r($allDataInSheet[0]);
            // echo "</pre>";
            // exit();

        $arrayCount = count($allDataInSheet[0]);
        if($arrayCount<=100001){
            
        
        $flag = 0;
        // echo "arrayCount: ".$arrayCount;

        $createArray = array('Year', 'Operator', 'Region', 'Country', 'Aircraft Role', 'Aircraft Category', 'Manufacturer', 'Aircraft Family', 'Aircraft Group', 'Aircraft Type', 'Propulsion', 'Engine Manufacturer', 'Engine Family', 'Expense Category', 'Expense Type', 'Total Cost', 'Events', 'Material Spend', 'Labor Hours', 'Labor Spend');

        $SheetDataKey = array();
        foreach ($allDataInSheet[0] as $dataInSheet) {
            foreach ($dataInSheet as $key => $value) {
              // echo 'val: ' .$value ."\n";
                if (in_array(trim($value), $createArray)) {
                    // $value = preg_replace('/\s+/', '', $value);
                    $SheetDataKey[trim($value)] = $key;
                } else {

                }
            }
        }

        $makeArray = array('Year' => 'Year', 'Operator' => 'Operator', 'Region' => 'Region', 'Country' => 'Country', 'Aircraft Role' => 'Aircraft Role', 'Aircraft Category' => 'Aircraft Category', 'Manufacturer' => 'Manufacturer', 'Aircraft Family' => 'Aircraft Family', 'Aircraft Group' => 'Aircraft Group', 'Aircraft Type' => 'Aircraft Type', 'Propulsion' => 'Propulsion', 'Engine Manufacturer' => 'Engine Manufacturer', 'Engine Family' => 'Engine Family', 'Expense Category' => 'Expense Category', 'Expense Type' => 'Expense Type', 'Total Cost' => 'Total Cost', 'Events' => 'Events', 'Material Spend' => 'Material Spend', 'Labor Hours' => 'Labor Hours', 'Labor Spend' => 'Labor Spend');


        $data = array_diff_key($makeArray, $SheetDataKey);

        if (empty($data)) {
            $flag = 1;
        }
        
        //echo $objPHPExcel->getSheetCount();die;

        $fetchData = array();
        $notif_all = array();
        // $a = 0;
        if ($flag == 1) {
            if($objPHPExcel->getActiveSheet(0)){
                            for ($i = 2; $i <= $arrayCount; $i++) {
                $year = $SheetDataKey['Year'];
                $operator = $SheetDataKey['Operator'];
                $region = $SheetDataKey['Region'];
                $country = $SheetDataKey['Country'];
                $aircraft_role = $SheetDataKey['Aircraft Role'];
                $aircraft_category = $SheetDataKey['Aircraft Category'];
                $manufacturer = $SheetDataKey['Manufacturer'];
                $aircraft_family = $SheetDataKey['Aircraft Family'];
                $aircraft_group = $SheetDataKey['Aircraft Group'];
                $aircraft_type = $SheetDataKey['Aircraft Type'];
                $propulsion = $SheetDataKey['Propulsion'];
                $engine_manufacturer = $SheetDataKey['Engine Manufacturer'];
                $engine_family = $SheetDataKey['Engine Family'];
                $expense_category = $SheetDataKey['Expense Category'];
                $expense_type = $SheetDataKey['Expense Type'];
                $total_cost = $SheetDataKey['Total Cost'];
                $events = $SheetDataKey['Events'];
                $material_spend = $SheetDataKey['Material Spend'];
                $labor_hours = $SheetDataKey['Labor Hours'];
                $labor_spend = $SheetDataKey['Labor Spend'];
                $year = filter_var(trim($allDataInSheet[0][$i][$year]), FILTER_SANITIZE_STRING);
                $operator = filter_var(trim($allDataInSheet[0][$i][$operator]), FILTER_SANITIZE_STRING);
                $region = filter_var(trim($allDataInSheet[0][$i][$region]), FILTER_SANITIZE_EMAIL);
                $country = filter_var(trim($allDataInSheet[0][$i][$country]), FILTER_SANITIZE_STRING);
                $aircraft_role = filter_var(trim($allDataInSheet[0][$i][$aircraft_role]), FILTER_SANITIZE_STRING);
                $aircraft_category = filter_var(trim($allDataInSheet[0][$i][$aircraft_category]), FILTER_SANITIZE_STRING);
                $manufacturer = filter_var(trim($allDataInSheet[0][$i][$manufacturer]), FILTER_SANITIZE_STRING);
                $aircraft_family = filter_var(trim($allDataInSheet[0][$i][$aircraft_family]), FILTER_SANITIZE_STRING);
                $aircraft_group = filter_var(trim($allDataInSheet[0][$i][$aircraft_group]), FILTER_SANITIZE_STRING);
                $aircraft_type = filter_var(trim($allDataInSheet[0][$i][$aircraft_type]), FILTER_SANITIZE_STRING);
                $propulsion = filter_var(trim($allDataInSheet[0][$i][$propulsion]), FILTER_SANITIZE_STRING);
                $engine_manufacturer = filter_var(trim($allDataInSheet[0][$i][$engine_manufacturer]), FILTER_SANITIZE_STRING);
                $engine_family = filter_var(trim($allDataInSheet[0][$i][$engine_family]), FILTER_SANITIZE_STRING);
                $expense_category = filter_var(trim($allDataInSheet[0][$i][$expense_category]), FILTER_SANITIZE_STRING);
                $expense_type = filter_var(trim($allDataInSheet[0][$i][$expense_type]), FILTER_SANITIZE_STRING);
                $total_cost = filter_var(trim($allDataInSheet[0][$i][$total_cost]), FILTER_SANITIZE_STRING);
                $events = filter_var(trim($allDataInSheet[0][$i][$events]), FILTER_SANITIZE_STRING);
                $material_spend = filter_var(trim($allDataInSheet[0][$i][$material_spend]), FILTER_SANITIZE_STRING);
                $labor_hours = filter_var(trim($allDataInSheet[0][$i][$labor_hours]), FILTER_SANITIZE_STRING);
                $labor_spend = filter_var(trim($allDataInSheet[0][$i][$labor_spend]), FILTER_SANITIZE_STRING);

                  $fetchData1[] = array(
                    'year' => $year,
                    'operator' => $operator,
                    'region' => $region,
                    'country' => $country,
                    'aircraft_role' => $aircraft_role,
                    'aircraft_category' => $aircraft_category,
                    'manufacturer' => $manufacturer,
                    'aircraft_family' => $aircraft_family,
                    'aircraft_group' => $aircraft_group,
                    'aircraft_type' => $aircraft_type,
                    'propulsion' => $propulsion,
                    'engine_manufacturer' => $engine_manufacturer,
                    'engine_family' => $engine_family,
                    'expense_category' => $expense_category,
                    'expense_type' => $expense_type,
                    'total_cost' => $total_cost,
                    'events' => $events,
                    'material_spend' => $material_spend,
                    'labor_hours' => $labor_hours,
                    'labor_spend' => $labor_spend
                    );

            }

                if ($dataOut['sukses']) {
                 $this->M_mro_new->upload_data($fetchData1);
                    $dataOut['sukses'] = true;
                    $dataOut['message'] = "Sukses Upload Sheet1";
                    array_push($notif_all, $dataOut);

                }else{
                    $dataOut['sukses'] = false;
                    $dataOut['message'] = "Gagal Upload Sheet1";
                    array_push($notif_all, $dataOut);
                }
            }
                        if($objPHPExcel->getActiveSheet(1)){
                            for ($i = 2; $i <= $arrayCount; $i++) {
                $year = $SheetDataKey['Year'];
                $operator = $SheetDataKey['Operator'];
                $region = $SheetDataKey['Region'];
                $country = $SheetDataKey['Country'];
                $aircraft_role = $SheetDataKey['Aircraft Role'];
                $aircraft_category = $SheetDataKey['Aircraft Category'];
                $manufacturer = $SheetDataKey['Manufacturer'];
                $aircraft_family = $SheetDataKey['Aircraft Family'];
                $aircraft_group = $SheetDataKey['Aircraft Group'];
                $aircraft_type = $SheetDataKey['Aircraft Type'];
                $propulsion = $SheetDataKey['Propulsion'];
                $engine_manufacturer = $SheetDataKey['Engine Manufacturer'];
                $engine_family = $SheetDataKey['Engine Family'];
                $expense_category = $SheetDataKey['Expense Category'];
                $expense_type = $SheetDataKey['Expense Type'];
                $total_cost = $SheetDataKey['Total Cost'];
                $events = $SheetDataKey['Events'];
                $material_spend = $SheetDataKey['Material Spend'];
                $labor_hours = $SheetDataKey['Labor Hours'];
                $labor_spend = $SheetDataKey['Labor Spend'];
                $year = filter_var(trim($allDataInSheet[0][$i][$year]), FILTER_SANITIZE_STRING);
                $operator = filter_var(trim($allDataInSheet[0][$i][$operator]), FILTER_SANITIZE_STRING);
                $region = filter_var(trim($allDataInSheet[0][$i][$region]), FILTER_SANITIZE_EMAIL);
                $country = filter_var(trim($allDataInSheet[0][$i][$country]), FILTER_SANITIZE_STRING);
                $aircraft_role = filter_var(trim($allDataInSheet[0][$i][$aircraft_role]), FILTER_SANITIZE_STRING);
                $aircraft_category = filter_var(trim($allDataInSheet[0][$i][$aircraft_category]), FILTER_SANITIZE_STRING);
                $manufacturer = filter_var(trim($allDataInSheet[0][$i][$manufacturer]), FILTER_SANITIZE_STRING);
                $aircraft_family = filter_var(trim($allDataInSheet[0][$i][$aircraft_family]), FILTER_SANITIZE_STRING);
                $aircraft_group = filter_var(trim($allDataInSheet[0][$i][$aircraft_group]), FILTER_SANITIZE_STRING);
                $aircraft_type = filter_var(trim($allDataInSheet[0][$i][$aircraft_type]), FILTER_SANITIZE_STRING);
                $propulsion = filter_var(trim($allDataInSheet[0][$i][$propulsion]), FILTER_SANITIZE_STRING);
                $engine_manufacturer = filter_var(trim($allDataInSheet[0][$i][$engine_manufacturer]), FILTER_SANITIZE_STRING);
                $engine_family = filter_var(trim($allDataInSheet[0][$i][$engine_family]), FILTER_SANITIZE_STRING);
                $expense_category = filter_var(trim($allDataInSheet[0][$i][$expense_category]), FILTER_SANITIZE_STRING);
                $expense_type = filter_var(trim($allDataInSheet[0][$i][$expense_type]), FILTER_SANITIZE_STRING);
                $total_cost = filter_var(trim($allDataInSheet[0][$i][$total_cost]), FILTER_SANITIZE_STRING);
                $events = filter_var(trim($allDataInSheet[0][$i][$events]), FILTER_SANITIZE_STRING);
                $material_spend = filter_var(trim($allDataInSheet[0][$i][$material_spend]), FILTER_SANITIZE_STRING);
                $labor_hours = filter_var(trim($allDataInSheet[0][$i][$labor_hours]), FILTER_SANITIZE_STRING);
                $labor_spend = filter_var(trim($allDataInSheet[0][$i][$labor_spend]), FILTER_SANITIZE_STRING);

                  $fetchData2[] = array(
                    'year' => $year,
                    'operator' => $operator,
                    'region' => $region,
                    'country' => $country,
                    'aircraft_role' => $aircraft_role,
                    'aircraft_category' => $aircraft_category,
                    'manufacturer' => $manufacturer,
                    'aircraft_family' => $aircraft_family,
                    'aircraft_group' => $aircraft_group,
                    'aircraft_type' => $aircraft_type,
                    'propulsion' => $propulsion,
                    'engine_manufacturer' => $engine_manufacturer,
                    'engine_family' => $engine_family,
                    'expense_category' => $expense_category,
                    'expense_type' => $expense_type,
                    'total_cost' => $total_cost,
                    'events' => $events,
                    'material_spend' => $material_spend,
                    'labor_hours' => $labor_hours,
                    'labor_spend' => $labor_spend
                    );

            }

                if ($dataOut['sukses']) {
                 $this->M_mro_new->upload_data($fetchData2);
                  $dataOut['sukses'] = true;
                    $dataOut['message'] = "Sukses Upload Sheet2";
                    array_push($notif_all, $dataOut);

                }else{
                    $dataOut['sukses'] = false;
                    $dataOut['message'] = "Gagal Upload Sheet2";
                    array_push($notif_all, $dataOut);
                }
            }


        } else {
            $dataOut['sukses'] = false;
            $dataOut['message'] = "Please import correct file";
            array_push($notif_all, $dataOut);
        }        
        
        }else{
            $dataOut['sukses'] = false;
            $dataOut['message'] = "Max 100.000 Baris Data";
            array_push($notif_all, $dataOut);
        }
       echo json_encode($notif_all);
    }


}

class chunkReadFilter implements PHPExcel_Reader_IReadFilter
  {
    private $_startRow = 0;
    private $_endRow = 0;
    /**  We expect a list of the rows that we want to read to be passed into the constructor  */
    public function __construct($startRow, $chunkSize) {
      $this->_startRow  = $startRow;
      $this->_endRow    = $startRow + $chunkSize;
    }
    public function readCell($column, $row, $worksheetName = '') {
      //  Only read the heading row, and the rows that were configured in the constructor
      if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
        return true;
      }
      return false;
    }
  }
