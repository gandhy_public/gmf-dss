<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Menu extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_menu");
        $this->load->library("pagination");
    }

    public function index()
    {


        $limit_per_page = 5;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;
        $total_records = $this->M_menu->get_total();

        // echo $start_index;


        $data = array(
            "content" => "menu/menu_view",
            "title" => "Menu Managements",
            "small_tittle" => "",
            "breadcrumb" => ["Menu Managements"],
            "menu" => $this->M_menu->tampil(),
            "results" => []
        );

        if ($total_records > 0) {

            $config['base_url'] = base_url() . 'index.php/menu/index';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;

            // custom paging configuration
            $config['num_links'] = 2;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;

            $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
            $config['full_tag_close'] = '</ul>';

            $config['first_link'] = 'First Page';
            $config['first_tag_open'] = '<li class="firstlink">';
            $config['first_tag_close'] = '</li>';

            $config['last_link'] = 'Last Page';
            $config['last_tag_open'] = '<li class="lastlink">';
            $config['last_tag_close'] = '</li>';

            $config['next_link'] = 'Next Page';
            $config['next_tag_open'] = '<li class="nextlink">';
            $config['next_tag_close'] = '</li>';

            $config['prev_link'] = 'Prev Page';
            $config['prev_tag_open'] = '<li class="prevlink">';
            $config['prev_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="curlink"> <a style="background-color:grey; color:#fff;">';
            $config['cur_tag_close'] = '</a></li>';

            $config['num_tag_open'] = '<li class="numlink">';
            $config['num_tag_close'] = '</li>';

            $this->pagination->initialize($config);

            $data = array(
                "content" => "menu/menu_view",
                "title" => "Menu Managements",
                "small_tittle" => "",
                "breadcrumb" => ["Menu Managements"],
                "menu" => $this->M_menu->tampil(),
                "center" => $this->M_menu->get_center($limit_per_page, ($start_index - 1) * $this->pagination->per_page),
                "results" => $this->M_menu->get_current_page_records(),
                "links" => $this->pagination->create_links()
            );
        }
        $this->load->view("layouts", $data);

    }

    public function edit_data_form($id)
    {
        echo json_encode($this->M_menu->get_menu_item_where($id));
    }

    public function menu_form($id = null)
    {
        $menuCenter = $this->M_menu->get_menuCenter();
        $menuParent = $this->M_menu->get_menuParent();
        $data = array(
            "content" => "menu/menu_form",
            "title" => "Menu Form",
            "small_tittle" => "",
            "breadcrumb" => ["Menu Managements", "Menu Form"],
            "menu_item_id" => ($id != null) ? $id : "",
            "menu_center" => $menuCenter,
            "menu_parent" => $menuParent,
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }
    
    
    public function get_parent()
    {
        $id = $this->input->post("id");
        $menuParent = $this->M_menu->get_menuParent_where($id);
        echo json_encode(
            array("menu" =>$menuParent)
        );
    }
    
    public function get_center_edit()
    {
        $center = $this->input->post("center");
        $parent = $this->input->post("parent");
        $val = $this->M_menu->get_menuCenter_where($center, $parent);
        if($center==''){
            $id = $val['menu_item_iscenter'];
        }else{
            $id = $val['menu_item_id'];
        }
        echo json_encode(
            array("menu" => $id)
        );
    }
    
        public function get_parent_edit()
    {
        $center = $this->input->post("center");
        if($center=='center'){
            $center = '0';
        }
        $parent = $this->input->post("parent");
        $val = $this->M_menu->get_menuCenter_where($center, $parent);
        if($center==''){
            $id = $val['menu_item_iscenter'];
        }else{
            $id = $val['menu_item_id'];
        }
        $data = $this->M_menu->get_menuParent_where($id);
        
        $option = '<select class="form-control selectpicker" data-size="12" id="pilihan-prov"><option value="" selected="">--Choose--</option><option value="0">Is Parent</option>';
        
        foreach($data as $row) {
            $option .= '<option value="'.$row["menu_item_id"].'">'.$row["menu_item_name"].'</option>';
        }
        $option .= '</select>';
        echo $option;
    }

    public function save_menu()
    {
        $response = array();
        if ($this->input->post("menu_item_id") === "") {
            echo json_encode(
                array("success" => ($this->M_menu->save_menu()) ? true : false)
            );
        } else {
            echo json_encode(
                array("success" => ($this->M_menu->update_menu()) ? true : false)
            );
        }
    }

    public function menu_to_bin($id)
    {
        echo json_encode(
            array("success" => ($this->M_menu->del_menu($id)) ? true : false)
        );
    }
	
	public function change_status_menu($id, $status){
		echo json_encode(
			array("success" => ($this->M_menu->change_status($id, $status)) ? true : false)
		);
	}

}


?>
