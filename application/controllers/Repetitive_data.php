<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Repetitive_data extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("M_menu");
    }

    function index()
    {
        $data = array(
            "content" => "repetitive/v_master_data",
            "title" => "Repetitive Problem Closing Rate",
            "titleGrafik" => "General Chart",
            "small_tittle" => "",
            "breadcrumb" => ["Repetitive Problem Closing Rate"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

}