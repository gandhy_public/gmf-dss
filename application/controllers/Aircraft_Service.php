<?php
defined("BASEPATH") or exit('no direct script access allowed');


/**
 *
 */
class Aircraft_Service extends MY_Controller
{

    public $service;

    public function __construct()
    {
        # code...
        parent::__construct();
        $this->service = $this->load->database("service", true);
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_menu");
        $this->load->model("M_aircraft_serviceability");
    }

    public function index()
    {
        $sekarang = $this->M_aircraft_serviceability->get_panel("sekarang");
        $kemarin = $this->M_aircraft_serviceability->get_panel("kemarin");
        $panel1 = $this->M_aircraft_serviceability->get_panel1();
        $target = str_replace(',', '.', is_null($sekarang) ? 0 : $sekarang->target);
        $sekarang = str_replace(',', '.', is_null($sekarang) ? 0 : $sekarang->daily_service_ability);
        $kemarin = str_replace(',', '.', is_null($kemarin) ? 0 : $kemarin->daily_service_ability);
        if ($sekarang > $target) {
            $p_sekarang = '<i class="fa fa-caret-up"></i>';
            $w_sekarang = '#04ff02';
        } else if ($sekarang < $target) {
            $p_sekarang = '<i class="fa fa-caret-down"></i>';
            $w_sekarang = '#ff0505';
        } else {
            $p_sekarang = '';
            $w_sekarang = '';
        }
        $t_target = str_replace(',', '.', $panel1->TARGET);
        $B777 = str_replace(',', '.', $panel1->B777);

        if ($B777 > $t_target) {
            $w_b777 = '#04ff02';
        } else if ($B777 < $t_target) {
            $w_b777 = '#ff0505';
        } else {
            $w_b777 = '';
        }
        $B744 = str_replace(',', '.', $panel1->B744);
        if ($B744 > $t_target) {
            $w_b744 = '#04ff02';
        } else if ($B744 < $t_target) {
            $w_b744 = '#ff0505';
        } else {
            $w_b744 = '';
        }
        $A333 = str_replace(',', '.', $panel1->A333);
        if ($A333 > $t_target) {
            $w_a333 = '#04ff02';
        } else if ($A333 < $t_target) {
            $w_a333 = '#ff0505';
        } else {
            $w_a333 = '';
        }
        $A332 = str_replace(',', '.', $panel1->A332);
        if ($A332 > $t_target) {
            $w_a332 = '#04ff02';
        } else if ($A332 < $t_target) {
            $w_a332 = '#ff0505';
        } else {
            $w_a332 = '';
        }
        $B738 = str_replace(',', '.', $panel1->B738);
        if ($B738 > $t_target) {
            $w_b738 = '#04ff02';
        } else if ($B738 < $t_target) {
            $w_b738 = '#ff0505';
        } else {
            $w_b738 = '';
        }
        $B735 = str_replace(',', '.', $panel1->B735);
        if ($B735 > $t_target) {
            $w_b735 = '#04ff02';
        } else if ($B735 < $t_target) {
            $w_b735 = '#ff0505';
        } else {
            $w_b735 = '';
        }
        $CRJ1000 = str_replace(',', '.', $panel1->CRJ1000);
        if ($CRJ1000 > $t_target) {
            $w_crj1000 = '#04ff02';
        } else if ($CRJ1000 < $t_target) {
            $w_crj1000 = '#ff0505';
        } else {
            $w_crj1000 = '';
        }
        $ATR72 = str_replace(',', '.', $panel1->ATR72);
        if ($ATR72 > $t_target) {
            $w_atr72 = '#04ff02';
        } else if ($ATR72 < $t_target) {
            $w_atr72 = '#ff0505';
        } else {
            $w_atr72 = '';
        }

        $B7M8 = str_replace(',', '.', $panel1->B7M8);
        if ($B7M8 > $t_target) {
            $w_B7M8 = '#04ff02';
        } else if ($B7M8 < $t_target) {
            $w_B7M8 = '#ff0505';
        } else {
            $w_B7M8 = '';
        }

        if ($B7M8 == 0) {
            $HB7M8 = ' display:none;';
        } else {
            $HB7M8 = '';
        };

        if ($B777 == 0) {
            $HB777 = ' display:none;';
        } else {
            $HB777 = '';
        };
        
        if ($B744 == 0) {
            $HB744 = ' display:none;';
        } else {
            $HB744 = '';
        };
        if ($A333 == 0) {
            $HA333 = ' display:none;';
        } else {
            $HA333 = '';
        };
        if ($A332 == 0) {
            $HA332 = ' display:none;';
        } else {
            $HA332 = '';
        };
        if ($B738 == 0) {
            $HB738 = ' display:none;';
        } else {
            $HB738 = '';
        };
        if ($B735 == 0) {
            $HB735 = ' display:none;';
        } else {
            $HB735 = '';
        };
        if ($CRJ1000 == 0) {
            $HCRJ1000 = ' display:none;';
        } else {
            $HCRJ1000 = '';
        };
        if ($ATR72 == 0) {
            $HATR72 = ' display:none;';
        } else {
            $HATR72 = '';
        };

        $getLastDate = $this->M_aircraft_serviceability;
        $p_A333 = $getLastDate->getLastDatePulsa("A333");
        $p_A332 = $getLastDate->getLastDatePulsa("A332");
        $p_ATR72 = $getLastDate->getLastDatePulsa("ATR72");
        $p_B777 = $getLastDate->getLastDatePulsa("B777");
        $p_B738 = $getLastDate->getLastDatePulsa("B738");
        $p_CRJ = $getLastDate->getLastDatePulsa("CRJ1000");
        $p_B744 = $getLastDate->getLastDatePulsa("B744");
        $p_B735 = $getLastDate->getLastDatePulsa("B735");
        $p_B7M8 = $getLastDate->getLastDatePulsa("B7M8");

        $ytd = date('d/M/Y');

        $data = array(
            "content" => "Aircraft Serviceability/graph",
            "title" => "Aircraft Serviceability",
            "title1" => "Year to Date",
            "title2" => "Aircraft Type",
            "small_tittle" => "",
            "breadcrumb" => [""],
            "menu" => $this->M_menu->tampil(),
            "target" => round($target, 2) . " %",
            "sekarang" => round($sekarang, 2) . " %",
            "p_sekarang" => $p_sekarang,
            "w_sekarang" => $w_sekarang,
            "B777" => round($B777, 2) . " %",
            "B744" => round($B744, 2) . " %",
            "A333" => round($A333, 2) . " %",
            "A332" => round($A332, 2) . " %",
            "B738" => round($B738, 2) . " %",
            "B735" => round($B735, 2) . " %",
            "CRJ1000" => round($CRJ1000, 2) . " %",
            "ATR72" => round($ATR72, 2) . " %",
            "B7M8" => round($B7M8, 2) . " %",
            "HB777" => $HB777,
            "HB744" => $HB744,
            "HB7M8" => $HB7M8,
            "HA333" => $HA333,
            "HA332" => $HA332,
            "HB738" => $HB738,
            "HB735" => $HB735,
            "HCRJ1000" => $HCRJ1000,
            "HATR72" => $HATR72,
            "WB777" => $w_b777,
            "WB744" => $w_b744,
            "WA333" => $w_a333,
            "WA332" => $w_a332,
            "WB738" => $w_b738,
            "WB735" => $w_b735,
            "WCRJ1000" => $w_crj1000,
            "WATR72" => $w_atr72,
            "WB7M8" => $w_B7M8,
            'ytd' => $ytd,
            "P_A333" => $p_A333,
            "P_A332" => $p_A332,
            "P_ATR72" => $p_ATR72,
            "P_B777" => $p_B777,
            "P_B738" => $p_B738,
            "P_CRJ" => $p_CRJ,
            "P_B744"=> $p_B744,
            "P_B735" => $p_B735,
            "P_B7M8" => $p_B7M8
        );

        $this->load->view("layouts", $data);
    }


    public function view_grafik()
    {
        $start = $this->input->post("start");
        $end = $this->input->post("end");
        $tipe = $this->input->post("tipe");
        $grafik = $this->M_aircraft_serviceability->get_grafik(date('Y-m-d', strtotime($start)), date('Y-m-d', strtotime($end)), $tipe);
        // $target = $this->M_aircraft_serviceability->get_grafik_target($start,$end,$tipe);
        $target = $this->M_aircraft_serviceability->get_grafik_by_actype(date('Y-m-d', strtotime($start)), date('Y-m-d', strtotime($end)), $tipe);
        // $target = str_replace(',', '.', is_null($target->target) ? 0 : $target->target);
        // $target = array(floatval(number_format($target,2)));


        $data['data'] = array();
        $data['datat'] = array();

        $data['datap'] = array();
        $minimal = 100;
        foreach ($grafik as $gv_key => $gv_value) {
            // $value  = $gv_value->$tipe;

            if ($minimal > floatval($gv_value->$tipe)) {
                $minimal = floatval($gv_value->$tipe);
            }
            $value = str_replace(',', '.', is_null($gv_value->$tipe) ? 0 : $gv_value->$tipe);
            if ($value <= 1) {
                $value = array(floatval($value));
            } else {
                $value = array(floatval(number_format($value, 2)));
            }

            $valuet = date('d-m-Y', strtotime($gv_value->date));
            array_push($data['data'], $value);
            array_push($data['datat'], $valuet);
        }
        foreach ($target as $gv_key => $gv_value) {
            // $value = $gv_value->target;
            $value = str_replace(',', '.', is_null($gv_value->target) ? 0 : $gv_value->target);
            if ($value <= 1) {
                $value = array(floatval($value));
            } else {
                $value = array(floatval(number_format($value, 2)));
            }

            array_push($data['datap'], $value);
        }
        $json[] = $data;
        echo json_encode(
            array(
                'grafik' => $json,
                'target' => $target,
                'minimal' => $minimal,
            )
        );
    }


    public function view_detail_panel()
    {
        $tipe = $this->input->post("tipe");
        $tahun = date('Y');
        $grafik = $this->M_aircraft_serviceability->get_detail_panel($tipe, $tahun);
        $target = $this->M_aircraft_serviceability->get_detail_panel_by_actype($tipe, $tahun);

        $data['data'] = array();
        $data['datat'] = array();
        $data['datap'] = array();
        $minimal = 100;
        foreach ($grafik as $gv_key => $gv_value) {
            if ($minimal > floatval($gv_value->total)) {
                $minimal = floatval($gv_value->total);
            }
            $value = str_replace(',', '.', is_null($gv_value->total) ? 0 : $gv_value->total);
            $value = array(floatval(number_format($value, 2)));
            $valuet = $gv_value->bulan;
            // $valuet = date('d-m-Y', strtotime($gv_value->bulan));
            array_push($data['data'], $value);
            array_push($data['datat'], $valuet);
        }

        foreach ($target as $gv_key => $gv_value) {
            $value = str_replace(',', '.', is_null($gv_value->target) ? 0 : $gv_value->target);
            $value = array(floatval(number_format($value, 2)));
            array_push($data['datap'], $value);
        }

        $json[] = $data;

        // $target = $this->M_aircraft_serviceability->get_panel1();
        // $target = str_replace(',', '.', is_null($target->TARGET) ? 0 : $target->TARGET);
        // $target = array(floatval(number_format($target,2)));
        echo json_encode(
            array(
                'grafik' => $json,
                'target' => $target,
                'minimal' => $minimal,
            )
        );
    }

    public function view_detail_panel_grafik()
    {
        $tipe = $this->input->post("tipe");
        $bulan = $this->input->post("bulan");
        $grafik = $this->M_aircraft_serviceability->get_detail_panel_grafik($tipe, $bulan);
        // $target = $this->M_aircraft_serviceability->get_detail_panel_grafik_target($tipe, $bulan);
        $target = $this->M_aircraft_serviceability->get_detail_panel_grafik_by_actype($tipe, $bulan);

        $data['data'] = array();
        $data['datat'] = array();
        $data['datap'] = array();
        $minimal = 100;
        foreach ($grafik as $gv_key => $gv_value) {
            if ($minimal > floatval($gv_value->$tipe)) {
                $minimal = floatval($gv_value->$tipe);
            }
            $value = str_replace(',', '.', is_null($gv_value->$tipe) ? 0 : $gv_value->$tipe);
            $value = array(floatval(number_format($value, 2)));
            $valuet = $gv_value->DATE;

            $valuet = date('d-m-Y', strtotime($gv_value->DATE));
            array_push($data['data'], $value);
            array_push($data['datat'], $valuet);
        }
        foreach ($target as $gv_key => $gv_value) {
            $value = str_replace(',', '.', is_null($gv_value->target) ? 0 : $gv_value->target);
            if ($value <= 1) {

                $value = array(floatval($value));
            } else {
                $value = array(floatval(number_format($value, 2)));
            }
            array_push($data['datap'], $value);
        }

        $json[] = $data;

        echo json_encode(
            array(
                'grafik' => $json,
                'target' => $target,
                'minimal' => $minimal,
            )
        );
    }

    public function datatable()
    {
        $columns = array(
            0 => "Date",
            1 => "Mitigation",
            2 => "Remarks",
            3 => "Reg",
            4 => "problem",
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']["value"];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];
        $data = array(
            "draw" => $draw,
            "length" => $length,
            "start" => $start,
            "search" => $search,
            "order_column" => $order_column,
            "order_dir" => $order_dir,
            "tipe" => $this->input->post("tipe"),
            "date" => date('Y-m-d', strtotime($this->input->post("tanggal"))),
        );

        $send = $this->M_aircraft_serviceability->show_datatable($data);
        echo $send;
    }

    public function datatable_top_trouble(){
      $type = $this->input->post('tipe');
      $date_st = $this->input->post('date_start');
      $date_ed = $this->input->post('date_end');
      $date_start = date('Y-m-d', strtotime($date_st));
      $date_end = date('Y-m-d', strtotime($date_ed));

      if($type == "Classic"){
        $type = "B737-300";
      }

      $datatabel = $this->M_aircraft_serviceability->getTop3Remarks($date_start,$date_end,$type);
      // $no = 1;
      // $response = array();
      // $post = array();
      // foreach ($datatabel as $key => $v) {
      //   $post[] = array(
      //     "NO" => "<center>".$no."<center>",
      //     "CATEGORY" => $v['kejadian'],
      //     "ACCIDENT" => "<center>".$v['total']."<center>"
      //   );
      //   $no++;
      // }
      // $response['data'] = $post;

      echo json_encode($datatabel);
    }

    // Store File Excell Upload 

    public function storefile()
    {
        # code...
        $this->load->library('PHPExcel');
        $destination_folder = 'assets/upload';
        // Change name files
        $new_name_file = date("Ymd_His") . '_' . $_FILES['files']['name'];
        $file = "$destination_folder/$new_name_file";

        $today = date("Y-m-d H:i:s");

        // insert Data base
        move_uploaded_file($_FILES['files']['tmp_name'], $file);
        
        // exit();

        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        //get only the Cell Collection
        // $cell_collection = $objPHPExcel->getActiveSheet(0)->getCellCollection();
        $cell_collection = $objPHPExcel->setActiveSheetIndex(0)->toArray(null, true, true, true);

        // Template Perfomance
        unlink($file);

       

        $startRow=$this->input->post('start_rows',true);
        $endRow=$this->input->post('end_rows',true);

        $column=count($cell_collection[3]);
        $AllRowData=count($cell_collection);

        if($startRow <=3){
            $startRow=4;
        }
        if($endRow >=$AllRowData){
            $endRow=$AllRowData;
        }


        // Initial Loop Data
        $loops=0;
        $dataInsert=array();
        // echo $column."<br>";
        // echo json_encode($cell_collection);exit;

        if($column==31){
            for ($i=$startRow; $i <= $endRow ; $i++) { 
                 # code...
                $loops+=1;
                $month=preg_split("(-|/)",$cell_collection[$i]['D']);
                $year=$this->cekMonthFormat($month);
                $dataInsert[]=array(
                    'date'=>$year,
                    'B777'=>$cell_collection[$i]['F']*100,
                    // 'B744'=>$cell_collection[$i][''],
                    'A333'=>$cell_collection[$i]['G']*100,
                    'A332'=>$cell_collection[$i]['H']*100,
                    'B738'=>$cell_collection[$i]['I']*100,
                    // 'B735'=>$cell_collection[$i][''],
                    'CRJ1000'=>$cell_collection[$i]['K']*100,
                    'ATR72'=>$cell_collection[$i]['L']*100,
                    'daily_service_ability'=>$cell_collection[$i]['M']*100,
                    'week'=>$cell_collection[$i]['B'],
                    'target'=>$cell_collection[$i]['AE']*100,
                    'B7M8'=>$cell_collection[$i]['J']*100
                );
             }
             $tabel="graph_4";
            //  echo json_encode($dataInsert);exit;

             if($loops==0){
                 $print=array('info'=> "warning",'msg'=> "Nothing Data To Insert Into DB Please Select Your Data Correctly");
                 echo json_encode($print);exit;
             }else{
                $query=$this->M_aircraft_serviceability->saveUploadData($tabel,$dataInsert);
             }


            

        }elseif($column==110){
            // echo json_encode($cell_collection[$startRow]['D']);exit;
            
             for ($i=$startRow; $i <= $endRow ; $i++) { 
                 # code...
                $loops+=1;
                $month=preg_split("(-|/)",$cell_collection[$i]['D']);
                $year=$this->cekMonthFormat($month);
                $dataInsert[]=array(
                    'date'=>$year,
                    'ac_type'=>$cell_collection[$i]['E'],
                    'total_fleet'=>$cell_collection[$i]['F'],
                    'total_ac'=>$cell_collection[$i]['G'],
                    'On_line'=>$cell_collection[$i]['H'],
                    'Mtc'=>$cell_collection[$i]['I'],
                    'VVIP'=>$cell_collection[$i]['J'],
                    'Stby'=>$cell_collection[$i]['K'],
                    'Serv'=>$cell_collection[$i]['L'],
                    '1st_Reg'=>$cell_collection[$i]['M'],
                    'Maint_Remarks'=>$cell_collection[$i]['N'],
                    '2nd_Reg'=>$cell_collection[$i]['O'],
                    '`Maint_Remarks (1)`'=>$cell_collection[$i]['P'],
                    '3rd_Reg'=>$cell_collection[$i]['Q'],
                    '`Maint_Remarks (2)`'=>$cell_collection[$i]['R'],
                    '4th_Reg'=>$cell_collection[$i]['S'],
                    '`Maint_Remarks (3)`'=>$cell_collection[$i]['T'],
                    '5th_Reg'=>$cell_collection[$i]['U'],
                    '`Maint_Remarks (4)`'=>$cell_collection[$i]['V'],
                    '6th_Reg'=>$cell_collection[$i]['W'],
                    '`Maint_Remarks (5)`'=>$cell_collection[$i]['X'],
                    // '7th_Reg'=>'',
                    // 'Maint_Remarks (plan6)'=>'',
                    // '8th_Reg'=>$cell_collection[$i][''],
                    // 'Maint_Remarks (plan7)'=>'',
                    // '9th_Reg'=>$cell_collection[$i][''],
                    // 'Maint_Remarks (plan8)'=>'',
                    // '10th_Reg'=>$cell_collection[$i][''],
                    // 'Maint_Remarks (plan9)'=>'',
                    '`On_line (1)`'=>$cell_collection[$i]['Y'],
                    '`VVIP (1)`'=>$cell_collection[$i]['Z'],
                    'Pln_Mtc'=>$cell_collection[$i]['AA'],
                    'Unpln_Mtc'=>$cell_collection[$i]['AB'],
                    'AOG'=>$cell_collection[$i]['AC'],
                    'Extd_TAT'=>$cell_collection[$i]['AD'],
                    'Total_Mtc_without_Incident'=>$cell_collection[$i]['AE'],
                    'Incident'=>$cell_collection[$i]['AF'],
                    'Total_Mtc_with_Incident'=>$cell_collection[$i]['AG'],
                    'Prep_1st_Rev_Flt'=>$cell_collection[$i]['AH'],
                    '`Stby (1)`'=>$cell_collection[$i]['AI'],
                    'Serv_excl_Incident'=>$cell_collection[$i]['AJ'],
                    'Serv_incl_Incident'=>$cell_collection[$i]['AK'],
                    'Grouping'=>$cell_collection[$i]['AL'],
                    '`1st_Reg (1)`'=>$cell_collection[$i]['AM'],
                    '`Maint_Remarks (6)`'=>$cell_collection[$i]['AN'],
                    'Problem_Rect'=>$cell_collection[$i]['AO'],
                    '`Grouping (1)`'=>$cell_collection[$i]['AP'],
                    '`2nd_Reg (1)`'=>$cell_collection[$i]['AQ'],
                    '`Maint_Remarks (7)`'=>$cell_collection[$i]['AR'],
                    '`Problem_Rect (1)`'=>$cell_collection[$i]['AS'],
                    '`Grouping (2)`'=>$cell_collection[$i]['AT'],
                    '`3rd_Reg (1)`'=>$cell_collection[$i]['AU'],
                    '`Maint_Remarks (8)`'=>$cell_collection[$i]['AV'],
                    '`Problem_Rect (2)`'=>$cell_collection[$i]['AW'],
                    '`Grouping (3)`'=>$cell_collection[$i]['AX'],
                    '`4th_Reg (1)`'=>$cell_collection[$i]['AY'],
                    '`Maint_Remarks (9)`'=>$cell_collection[$i]['AZ'],
                    '`Problem_Rect (3)`'=>$cell_collection[$i]['BA'],
                    '`Grouping (4)`'=>$cell_collection[$i]['BB'],
                    '`5th_Reg (1)`'=>$cell_collection[$i]['BC'],
                    '`Maint_Remarks (10)`'=>$cell_collection[$i]['BD'],
                    '`Problem_Rect (4)`'=>$cell_collection[$i]['BE'],
                    '`Grouping (5)`'=>$cell_collection[$i]['BF'],
                    '`6th_Reg (1)`'=>$cell_collection[$i]['BG'],
                    '`Maint_Remarks (11)`'=>$cell_collection[$i]['BH'],
                    '`Problem_Rect (5)`'=>$cell_collection[$i]['BI'],
                    // 'Grouping (6)'=>'',
                    // '7th_Reg (1)'=>'',
                    // 'Maint_Remarks (12)'=>'',
                    // 'Problem_Rect (6)'=>'',
                    // 'Grouping (7)'=>'',
                    // '8th_Reg (1)'=>'',
                    // 'Maint_Remarks (13)'=>'',
                    // 'Problem_Rect (7)'=>'',
                    // 'Grouping (8)'=>'',
                    // '9th_Reg (1)'=>'',
                    // 'Maint_Remarks (14)'=>'',
                    // 'Problem_Rect (8)'=>'',
                    // 'Grouping (9)'=>'',
                    // '10th_Reg (1)'=>'',
                    // 'Maint_Remarks (15)'=>'',
                    // 'Problem_Rect (9)'=>'',
                    'Svblty_excl_Incident'=>$cell_collection[$i]['BJ']*100,
                    // 'F61'=>'',
                    'week'=>$cell_collection[$i]['CI'],
                    'target'=>$cell_collection[$i]['CH'],
                    'pulsa'=>$cell_collection[$i]['DF']
                );
             }

             $tabel="graph_4_detail";
             if($loops==0){
                $print=array('info'=> "warning",'msg'=> "Nothing Data To Insert Into DB Please Select Your Data Correctly");
                echo json_encode($print);exit;
             }else{
                $query=$this->M_aircraft_serviceability->saveUploadData($tabel,$dataInsert);
             }

        }else{
            $print=array('info'=> "warning",'msg'=> "Nothing Data To Insert Into DB Please Select Your Data Correctly");
            echo json_encode($print);exit;
        }

        if($query){
          $print=array('info'=> "success",'msg'=> "Upload Data has Succeses Insert into Databases");      
        }else{
          $print=array('info'=> "error",'msg'=> "Upload Data has Failed Insert into Databases");  
        }

        echo json_encode($print);
    }


    public function cekMonthFormat($month){
        if(strlen($month[1])!=3){
                         $print=array('info'=> "warning",'msg'=> "Please Check Date Format , make Sure formated Month Formated Like Jan Or Feb ");
                         echo json_encode($print);exit;
                    }
                    
                    if(strlen($month[2])==2 ){
                        $year="20".$month[2];
                    }else{
                        $year=$month[2];
                    }                
                        switch ($month[1]) {
                            case "Jan":
                                $mon="01";
                                break;
                            case "Feb":
                                $mon="02";
                                break;
                            case "Mar":
                                $mon="03";
                                break;
                            case "Apr":
                                $mon="04";
                                break;
                            case "May":
                                $mon="05";
                                break;
                            case "Jun":
                                $mon="06";
                                break;
                            case "Jul":
                                $mon="07";
                                break;
                            case "Aug":
                                $mon="08";
                                break;
                            case "Sep":
                                $mon="09";
                                break; 
                             case "Oct":
                                $mon="10";
                                break;
                             case "Nov":
                                $mon="11";
                                break;  
                             case "Dec":
                                $mon="12";
                                break;
                        }

                        $tgl=$year."-".$mon."-".$month[0];
                        return $tgl;
    }
    // End Store File Upload 

}
