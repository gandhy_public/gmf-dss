<?php
defined('BASEPATH') or exit ('No direct script access allowed');

/**
*
*/
class Engine_APU_Spare_Availability extends MY_Controller
{

  function __construct()
  {
    // code...
    parent::__construct();
    $this->load->model("M_menu");
    $this->load->model("M_engine_apu");

  }

  function index(){
    $data = array(
      "content"       => "spareavaibility/v_spare_avaibility",
      "title"         => "Engine And APU Spare Availability",
      "small_tittle"  => "",
      "breadcrumb"    => ["Engine And APU Spare Availability"],
      "menu"          => $this->M_menu->tampil(),
    );
    $this->load->view("layouts", $data);
  }

  // Fungi Untuk get data pertama kali halaman direload
  public function landing()
  {
    // code...
    $data=$this->M_engine_apu->get_data_availability();
    $output = array('grd_engine_av' => $data[0]['grd_engine_avabiliy'] == NULL ? 0 : $data[0]['grd_engine_avabiliy'],
                    'grd_engine_nav' => $data[0]['grd_engine_navability'] == NULL ? 0 : $data[0]['grd_engine_navability'],
                    'grd_apu_av' => $data[0]['grd_apu_avability'] == NULL ? 0 : $data[0]['grd_apu_avability'],
                    'grd_apu_nav' => $data[0]['grd_apu_navability']== NULL ? 0 : $data[0]['grd_apu_navability'],
                    'ct_engine_av' => $data[0]['ct_engine_avability'] == NULL ? 0 : $data[0]['ct_engine_avability'],
                    'ct_engine_nav' => $data[0]['ct_engine_navability'] == NULL ? 0 : $data[0]['ct_engine_navability'],
                    'ct_apu_av' => $data[0]['ct_apu_avability'] == NULL ? 0 : $data[0]['ct_apu_avability'],
                    'ct_apu_nav' => $data[0]['ct_apu_navability'] == NULL ? 0 : $data[0]['ct_apu_navability']);
   echo json_encode($output);
  }

  public function avability()
  {
    $param1=$this->input->post('param1');
    if($this->input->post('param2')=='Garuda'){
      $param2='GA STOCK';
    }else{
      $param2='CITILINK';
    }
    $param3=$this->input->post('param3');

        if($param3 == '' || $param3 == 'All'){
          // mengambil data option buat material number
          if($this->input->post('param1') == 'Engine') {
            $output=$this->M_engine_apu->material_number_engine();
          } else {
            $output=$this->M_engine_apu->material_number_apu();            
          }
          $arr=array();
          $arr2 = array();
          foreach ($output as $y => $val) {
            array_push($arr,$val['ex_long_mat_numb']);
          }
          $param3=implode(",",$arr);
        }
    $select=str_replace(",","','",$param3);
    if ($param1=='APU') {
      // code...
      $tipe="APU";
    }else{
      $tipe="ENGINE";
    }
      $draw = $_REQUEST['draw'];
      $length = $_REQUEST['length'];
      $starts = $_REQUEST['start'];
      $search = $_REQUEST['search']['value'];


      $data = array(
          'draw' => $draw,
          'length' => $length,
          'start' => $starts,
          'search' => $search
      );

    if ($param3=='') {
      // code...
      $jml=$this->M_engine_apu->count_detail_av($tipe,$param2,$data);
      $query=$this->M_engine_apu->detail_av($tipe,$param2,$data);
      if(count($query)!=0){
        foreach ($query as $key) {
          // code...
          $print['data'][]=array(
            $key['ex_long_mat_numb'],
            $key['material_desc'],
            $key['plant'],
            $key['storage_loc'],
            $key['desc_storage_loc'],
            $key['unrestricted']
          );
        }
      }else{
        $print['data']=array();
      }
    }else{
      $jml=$this->M_engine_apu->count_detail_av_select($tipe,$select,$param2,$data);
      $query=$this->M_engine_apu->detail_av_select($tipe,$select,$param2,$data);
      if(count($query)!=0){
        foreach ($query as $key) {
          // code...
          $print['data'][]=array(
            $key['ex_long_mat_numb'],
              $key['material_desc'],
              $key['plant'],
              $key['storage_loc'],
              $key['desc_storage_loc'],
              $key['unrestricted']
          );
        }
      }else{
        $print['data']=array();
      }
    }
    $print['draw']= $data['draw'];
     $print['recordsFiltered']=$jml;
     $print['recordsTotal']  = $jml;
    echo json_encode ($print);
  }

  public function nonavability()
  {
    // code...
    $param1=$this->input->post('param1');
    if($this->input->post('param2')=='Garuda'){
      $param2='GA STOCK';
    }else{
      $param2='CITILINK';
    }
    $param3=$this->input->post('param3');

    if($param3 == '' || $param3 == 'All'){
      // mengambil data option buat material number
      if($this->input->post('param1') == 'Engine') {
            $output=$this->M_engine_apu->material_number_engine();
          } else {
            $output=$this->M_engine_apu->material_number_apu();            
          }
      $arr=array();
      $arr2 = array();
      foreach ($output as $y => $val) {
        array_push($arr,$val['ex_long_mat_numb']);
      }
      $param3=implode(",",$arr);
    }

    $select=str_replace(",","','",$param3);

    if ($param1=='APU') {
      // code...
      $tipe="APU";
    }else{
      $tipe="ENGINE";
    }
    $draw = $_REQUEST['draw'];
      $length = $_REQUEST['length'];
      $starts = $_REQUEST['start'];
      $search = $_REQUEST['search']['value'];


      $data = array(
          'draw' => $draw,
          'length' => $length,
          'start' => $starts,
          'search' => $search
      );

    if ($param3=='') {
      // code...
      $jml=$this->M_engine_apu->count_detail_nav($tipe,$param2,$data);
      $query=$this->M_engine_apu->detail_nav($tipe,$param2,$data);
      if(count($query)!=0){
        foreach ($query as $key) {
          // code...
          $print['data'][]=array(
            $key['ex_long_mat_numb'],
            $key['material_desc'],
            $key['plant'],
            $key['storage_loc'],
            $key['desc_storage_loc'],
            $key['unrestricted']

          );
        }
      }else{
        $print['data']=array();
      }

    } else {
      $jml=$this->M_engine_apu->count_detail_nav_select($tipe,$select,$param2,$data);
      $query=$this->M_engine_apu->detail_nav_select($tipe,$select,$param2,$data);
      if(count($query)!=0){
        foreach ($query as $key) {
          // code...
          $print['data'][]=array(
            $key['ex_long_mat_numb'],
            $key['material_desc'],
            $key['plant'],
            $key['storage_loc'],
            $key['desc_storage_loc'],
            $key['unrestricted']
          );
        }
      }else{
        $print['data']=array();
      }
    }
    $print['draw']= $data['draw'];
     $print['recordsFiltered']=$jml;
     $print['recordsTotal']  = $jml;
    echo json_encode ($print);
  }
  public function searchby()
  {
    // code...
    $param=$this->input->post('param');

    if($param == '' || $param[0] == 'All'){
      // mengambil data option buat material number
      $output=$this->M_engine_apu->material_number_engine();
      $arr=array();
      $arr2 = array();
      foreach ($output as $y => $val) {
        array_push($arr,$val['ex_long_mat_numb']);
      }
      $param_e = $arr;

      $output=$this->M_engine_apu->material_number_apu();
      $arr=array();
      $arr2 = array();
      foreach ($output as $y => $val) {
        array_push($arr,$val['ex_long_mat_numb']);
      }
      $param_a = $arr;
    }

    $data1 = array();
    $data2 = array();
    foreach ($param_e as $key => $value) {
      // code...
      array_push($data1,$value);
    }
    foreach ($param_a as $key => $value) {
      // code...
      array_push($data2,$value);
    }

    $param_e=implode("','",$data1);
    $param_a=implode("','",$data2);
    $data=$this->M_engine_apu->get_data_availability($param_e, $param_a); // before get_data_searchby
    $output = array('grd_engine_av' => $data[0]['grd_engine_avabiliy'] == NULL ? 0 : $data[0]['grd_engine_avabiliy'],
                    'grd_engine_nav' => $data[0]['grd_engine_navability'] == NULL ? 0 : $data[0]['grd_engine_navability'],
                    'grd_apu_av' => $data[0]['grd_apu_avability'] == NULL ? 0 : $data[0]['grd_apu_avability'],
                    'grd_apu_nav' => $data[0]['grd_apu_navability']== NULL ? 0 : $data[0]['grd_apu_navability'],
                    'ct_engine_av' => $data[0]['ct_engine_avability'] == NULL ? 0 : $data[0]['ct_engine_avability'],
                    'ct_engine_nav' => $data[0]['ct_engine_navability'] == NULL ? 0 : $data[0]['ct_engine_navability'],
                    'ct_apu_av' => $data[0]['ct_apu_avability'] == NULL ? 0 : $data[0]['ct_apu_avability'],
                    'ct_apu_nav' => $data[0]['ct_apu_navability'] == NULL ? 0 : $data[0]['ct_apu_navability']);
   echo json_encode($output);
  }

  public function material()
  {
    $data_e = $this->M_engine_apu->material_number_engine();
    $data_a = $this->M_engine_apu->material_number_apu();
    $data = array('data' => array_merge($data_e, $data_a));
    echo json_encode ($data);
  }
}
