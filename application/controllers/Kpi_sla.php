<?php

defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');

class Kpi_sla extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("date");
        $this->load->helper("hierarchyitem_helper");
        $this->load->helper("kpiformula");
        $this->load->model("M_kpi_sla_master");
        $this->load->model("M_kpi_unit");
    }

    function generatePageTreeTableActual($datas, $depth = 1, $parent = 0)
    {
        $month = $this->input->get("month");
        $nowYear = date("Y");

        if ($depth > 1000) return '';
        $deepChild = 1;
        $tree = '';
        for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
            if ($datas[$i]->ki_parent_id == $parent) {
                $target = explode("|", $datas[$i]->kid_target);
                $actual = (($datas[$i]->kiad_actual == null || empty($datas[$i]->kiad_actual)) ? "-" : $datas[$i]->kiad_actual);
                $actualYtd = (($datas[$i]->kiad_actual_ytd == null || empty($datas[$i]->kiad_actual_ytd)) ? "-" : $datas[$i]->kiad_actual_ytd);
                $tree .= "<tr data-id='" . $datas[$i]->ki_id . "' data-parent='" . $datas[$i]->ki_parent_id . "' data-level='" . $depth . "'> <td data-column='name'> L" . $depth . "&nbsp;&nbsp;-" . str_repeat('&nbsp;&nbsp;&nbsp;', $depth);
                if ($this->M_kpi_sla_master->getDeepChild($datas[$i]->ki_id) == 0) {
                  $tree .= $datas[$i]->ki_name . "</td>";
                  $tree .= "<td>" . $datas[$i]->ki_uom . "</td>";
                  $tree .= "<td style='".(($actual >= $datas[$i]->kid_target) ? "color:green" : "color:red")."'>" . $actual . "</td>";
                  //target
                  $tree .= "<td>".$datas[$i]->kid_target."</td>";
                  //Achievement
                  $tree .= "<td>" . number_format(achievment($datas[$i]->kid_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), 2) . "%" . "</td>";
                  //Score
                  $tree .= "<td>" . number_format(score(achievment($datas[$i]->kid_target, $datas[$i]->kiad_actual, $datas[$i]->fa_function_name), $datas[$i]->kid_limit, $datas[$i]->kid_weight), 2) . "%" . "</td>";
                  //total score
                  $tree .= "<td></td>";

                  $tree .= "<td style='".(($actualYtd >= $datas[$i]->kid_target_ytd) ? "color:green" : "color:red")."'>" . $actualYtd . "</td>";
                  //target ytd
                  $tree .= "<td>".(($datas[$i]->kid_target_ytd === 'undefined' || $datas[$i]->kid_target_ytd == '') ? 0 : $datas[$i]->kid_target_ytd)."</td>";
                  //Achievement ytd
                  $tree .= "<td>" . number_format(achievment($datas[$i]->kid_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), 2) . "%" . "</td>";
                  //Score ytd
                  $tree .= "<td>" . number_format(score(achievment($datas[$i]->kid_target_ytd, $datas[$i]->kiad_actual_ytd, $datas[$i]->fa_function_name), $datas[$i]->kid_limit, $datas[$i]->kid_weight), 2) . "%" . "</td>";
                  //total score
                  $tree .= "<td></td>";

                } else {
                    $maxLevel = $this->M_kpi_sla_master->maxLevel($datas[$i]->ki_id);
                    // echo $datas[$i]->ki_id;
                    $finalTotalScores=$this->FunctionName($maxLevel,$datas[$i]->ki_id);

                    $tree .= $datas[$i]->ki_name . "</td>";
                    $tree .= "<td>" . $datas[$i]->ki_uom . "</td>";
                    $tree .= "<td>" . $actual . "</td>";
                    //target
                    $tree .= "<td>".$datas[$i]->ki_target."</td>";
                    //Achievement
                    $tree .= "<td>-</td>";
                    // //Score
                    if($depth!=1){
                        $tree .= "<td>" . number_format($finalTotalScores['score']*($datas[$i]->ki_weight/100),2) . "%</td>";
                    }else{
                        $tree .= "<td>" . number_format($finalTotalScores['score'],2) . "%</td>";
                    }
                    //Total Score
                    $tree .= "<td>" . number_format($finalTotalScores['score'],2) . "%</td>";
                    

                    $tree .= "<td>" . $datas[$i]->ki_id. "</td>";
                    //target_ytd
                    $tree .= "<td>".$datas[$i]->ki_target_ytd."</td>";
                    //Achievement ytd
                    $tree .= "<td></td>";
                    //Score Ytd
                    if($depth!=1){
                    $tree .= "<td>". number_format($finalTotalScores['scoreYtd']*($datas[$i]->ki_weight/100),2) ."</td>";
                    }else{
                        $tree .= "<td>".number_format($finalTotalScores['scoreYtd'],2)."</td>";
                    }
                    //Total Score Ytd
                    $tree .= "<td>".number_format($finalTotalScores['scoreYtd'],2)."</td>";
                }
                $tree .= "</tr>";
                $tree .= $this->generatePageTreeTableActual($datas, $depth + 1, $datas[$i]->ki_id);
            }
        }

        return $tree;
    }


    public function FunctionName($maxLevel,$ki_id)
    {
        # code...
                    // $finalTotalScore=0;
                    $arrayTemp=array();
                    $parentArr=array();
                    for ($k=$maxLevel; $k >=1 ; $k--) { 
                        # code...

                        $items = $this->M_kpi_sla_master->getPenghuni($k,$ki_id);                        
                        $parent = $this->M_kpi_sla_master->getGroupLevelParent($k,$ki_id);
                       
                       
                        if($k==$maxLevel){
                            
                             foreach ($parent as $key ) {
                                # code...
                                $tempTotalYtd=0;
                                $tempTotal=0;
                                for ($p=0; $p < count($items) ; $p++) { 
                                    # code...
                                    if($items[$p]['ki_parent_id']==$key['ki_parent_id']){
                                       $dataM = $this->M_kpi_sla_master->listActualWhere($items[$p]['ki_id']);
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                           $tempTotal+=number_format(score(achievment($dataM[$z]->kid_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2);
                                           $tempTotalYtd+=number_format(score(achievment($dataM[$z]->kid_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2) ;
                                       }
                                    }
                                }
                                $arrayTemp[$key['ki_parent_id']] = $tempTotal;
                                $parentArr[$key['ki_parent_id']] = $tempTotalYtd;
                            }

                        }
                        else{
                            $tempFuck=0;
                            $tempFuckYtd=0;

                            foreach ($parent as $keys ) {
                                # code...
                                for ($q=0; $q < count($items) ; $q++) { 
                                    if (array_key_exists($items[$q]['ki_id'],$arrayTemp) && $items[$q]['ki_parent_id']==$keys['ki_parent_id']){
                                        $tempFuck+=$arrayTemp[$items[$q]['ki_id']]*($items[$q]['ki_weight']/100);
                                        $tempFuckYtd+=$parentArr[$items[$q]['ki_id']]*($items[$q]['ki_weight']/100);
                                    }elseif( $items[$q]['ki_parent_id']==$keys['ki_parent_id']){
                                       $dataM = $this->M_kpi_sla_master->listActualWhere($items[$q]['ki_id']);
                                       for ($z=0; $z < (count($dataM)) ; $z++) { 
                                           # code...
                                           $tempFuck+=number_format(score(achievment($dataM[$z]->kid_target, $dataM[$z]->kiad_actual, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2);
                                           $tempFuckYtd+=number_format(score(achievment($dataM[$z]->kid_target_ytd, $dataM[$z]->kiad_actual_ytd, $dataM[$z]->fa_function_name), $dataM[$z]->kid_limit, $dataM[$z]->kid_weight), 2) ;
                                       }
                                    }
                                }
                               
                                $arrayTemp[$keys['ki_parent_id']] = $tempFuck;
                                $parentArr[$keys['ki_parent_id']] = $tempFuckYtd;
                                // $parentArr[$key['ki_parent_id']] = $tempTotal*$items[$q]['ki_id'];
                                $tempFuck=0;
                                $tempFuckYtd=0;
                                
                            }

                            

                        }
                        
                    }

                    return $finalTotalScore= array('score'=>$arrayTemp[$ki_id],'scoreYtd'=>$parentArr[$ki_id]);
    }

   

    public function get_all_total()
    {
      $year=date('Y');
      $month = $this->input->get("month");
      $dataMentah = $this->M_kpi_sla_master->getZerosParent($year);
      $loops=0;
      $temp=0;
      foreach ($dataMentah as $key ) {
          # code...
          $maxLevel = $this->M_kpi_sla_master->maxLevel($key['ki_id']);
          $finalTotalScores=$this->FunctionName($maxLevel,$key['ki_id']);
          $temp+=$finalTotalScores['score'];
          $loops+=1;
      }
      
      $content =number_format(($temp/$loops),2);
      echo json_encode(array("total" => $content));
    }

    public function delete_unit()
    {
      $role_id = $this->input->post("role_id");
      $delete_unit = $this->M_kpi_sla_master->delete_unit_where($role_id);
      $delete_unit_assign = $this->M_kpi_sla_master->delete_unit_assign_where($role_id);
      $delete_unit_relation = $this->M_kpi_sla_master->delete_unit_user_where($role_id);
      if($delete_unit == true && $delete_unit_assign == true && $delete_unit_relation == true){
        echo json_encode(array("success" => true));
      } else {
        echo json_encode(array("success" => false));
      }
    }

    public function delete_kpi()
    {
        $delete = $this->M_kpi_sla_master->delete_kpi();
        echo json_encode(array("success" => $delete));
    }

    public function getDataActual()
    {
        $dataMentah = $this->M_kpi_sla_master->listActual();
        $content = $this->generatePageTreeTableActual($dataMentah);
        echo json_encode(array("content" => $content));
    }

    public function buildTree(array $elements, $parentId = 0)
    {
        $kpiItem = array();

        foreach ($elements as $element) {
            if ($element->ki_parent_id == $parentId) {
                $children = $this->buildTree($elements, $element->ki_id);
                if ($children) {
                    $element->children = $children;
                }
                $kpiItem[] = $element;
            }
        }

        return $kpiItem;
    }

    public function duplicate()
    {
        $copy_year1 = $this->input->post("copy_year1");
        $copy_year2 = $this->input->post("copy_year2");

        $data = $this->M_kpi_sla_master->select_kpi_where_year($copy_year1);
        $tree = $this->buildTree($data);
        $copy_kpi = $this->M_kpi_sla_master->copy_kpi($tree);
        echo json_encode(array("success" => true));
    }

    public function task_asigment()
    {
        $deep_child = $this->M_kpi_sla_master->get_deep_child();
        echo json_encode($deep_child);
    }

    public function treeKpiParent($year)
    {
        $allItem = $this->M_kpi_sla_master->getAllItemKpi($year);
        $tree = generatePageTree($allItem);
        echo json_encode(array("success" => true, "tree" => $tree));
    }



    public function index()
    {
        $formula = $this->M_kpi_sla_master->getAllFormula();
        $allItem = $this->M_kpi_sla_master->getAllItemKpi();
        $allRole = $this->M_kpi_sla_master->role();

        $recursive = $this->buildTree($allItem);
        $data = array(
            "content" => "kpi/kpi_sla_master",
            "title" => "KPI Management",
            "formula" => $formula,
            "allItem" => $allItem,
            "allRole" => $allRole,
            "small_tittle" => "(Coorporate)",
            "breadcrumb" => ["KPI Management"],
            "menu" => $this->M_menu->tampil()
        );
        // echo json_encode($data["allItem"]);
        // exit();
        $this->load->view("layouts", $data);
    }

    public function change_lock()
    {
      $params = $this->input->post("params");
      $change = $this->M_kpi_unit->change_lock($params);
      echo json_encode(
        array(
            "success" => (($change) ? true : false)
        )
      );
    }

    public function check_lock()
    {
      $isLocked = $this->M_kpi_unit->check_lock();
      echo json_encode(array("isLocked" => $isLocked));
    }

    public function getAllRole()
    {
      echo json_encode($this->M_kpi_sla_master->role());
    }

    public function delete_assign()
    {
        $kia_id = $this->input->post("kia_id");
        $delete = $this->M_kpi_sla_master->delete_kia_where($kia_id);
        echo json_encode($delete);
    }

    public function get_exist_assigment($role_id)
    {
        $data = $this->M_kpi_sla_master->get_kpi_assigment_where($role_id);
        echo json_encode($data);
    }

    public function assign_kpi()
    {
        $insert = $this->M_kpi_sla_master->insert_assign_kpi();
        echo json_encode($insert);
    }

    public function insert_unit_kpi()
    {
        $unit_name = $this->input->post("unit_name");
        $kpi_unit_id = $this->input->post("kpi_unit_id");
        if($kpi_unit_id == ''){
            $insertUnit = $this->M_kpi_sla_master->insert_unit($_POST["user_id"], $unit_name);
        }else{
            $insertUnit = $this->M_kpi_sla_master->insert_unit_new($_POST["user_id"], $unit_name, $kpi_unit_id);
        }
        
        echo json_encode($insertUnit);
    }

    public function all_list_unit()
    {
        $allListUnit = $this->M_kpi_sla_master->list_unit();
        echo json_encode($allListUnit);
    }

    public function insert()
    {
        $insert = $this->M_kpi_sla_master->saveKpi();
        echo json_encode($insert);
    }

    public function list_with_detail()
    {
        $allKpi = $this->M_kpi_sla_master->getAllItemKpiWithDetail();
        $nowYear = $this->input->post("year");
        $html = generatePageTreeTable($allKpi, $nowYear);
        echo json_encode(array(
            "success" => true,
            "htmlBuild" => $html
        ));
    }

    public function edit_item($id)
    {
        $data = $this->M_kpi_sla_master->edit($id);
        echo json_encode($data);
    }

    public function manage_detail()
    {
        $save = $this->M_kpi_sla_master->saveDetailItem();
        $result = array();
        if ($save) {
            $result["success"] = true;
        } else {
            $result["success"] = false;
        }
        echo json_encode($result);
    }

    public function detail_item($ki_id, $year)
    {
        $data = $this->M_kpi_sla_master->getDetailWhere($ki_id, $year);
        echo json_encode($data);
    }

    public function saveGroup()
    {
      $arr = array();
      $save = $this->M_kpi_sla_master->saveGroup();
      $arr["success"] = ($save) ? true : false;
      echo json_encode($arr);
    }

    public function jsonGroup()
    {
      $data = $this->M_kpi_sla_master->loadGrup();
      echo json_encode($data);
    }

    public function listGrup()
    {
      $data = $this->M_kpi_sla_master->loadGrup();
      $string = "";
      $no = 1;
      foreach ($data as $list) {
        $string .= "<tr>";
        $string .= "<td style='text-align:center;'>".$no."</td>";
        $string .= "<td style='text-align:center;'>".$list->kpi_group_name."</td>";
        $string .= "<td style='text-align:center;'>
                      <button class='btn btn-default btn-flat' data-name='".$list->kpi_group_name."' data-id='".$list->kpi_group_id."' id='manage_grup'><i class='fa fa-pencil'></i></button>
                      <button class='btn bg-navy btn-flat' data-id='".$list->kpi_group_id."' id='edit_group'><i class='fa fa-wrench'></i></button>
                      <button class='btn btn-danger btn-flat' data-id='".$list->kpi_group_id."' id='delete_group'><i class='fa fa-trash'></i></button>
                   </td>";
        $string .= "</tr>";
        $no++;
      }
      $arr = array("success" => true, "html" => $string);
      echo json_encode($arr);
    }

    public function edit($id)
    {
      $dataEdit = $this->M_kpi_sla_master->loadGrupWhere($id);
      echo json_encode($dataEdit);
    }

    public function member_group($id, $thn)
    {
      $data = $this->M_kpi_sla_master->member_group_where($id, $thn);
      echo json_encode($data);
    }

   public function list_parent($thn)
   {
     $data = $this->M_kpi_sla_master->get_all_parent($thn);
     echo json_encode($data);
   }

    public function member_group_datatable($id)
    {
      $columns = array(
        0 => "ki_id",
        1 => "ki_name"
      );

      $draw   =  $_REQUEST['draw'];
      $length =  $_REQUEST['length'];
      $start  = $_REQUEST['start'];
      $search =  $_REQUEST['search']["value"];
      $order_column  = $columns[$_REQUEST['order'][0]['column']];
      $order_dir  = $_REQUEST['order'][0]['dir'];

      $data = array(
        "draw" => $draw,
        "length" => $length,
        "start" => $start,
        "search" => $search,
        "order_column" => $order_column,
        "order_dir" => $order_dir,
        "id" => $id
      );

      $send = $this->M_kpi_sla_master->member_group_datatable_where($data);
      echo $send;
    }

    public function save_member()
    {
      $kpi_group_id = $this->input->post("kpi_group_id");
      $ki_id = $this->input->post("ki_id");
      $update = $this->M_kpi_sla_master->set_member_where($ki_id, $kpi_group_id);
      $result = array();
      $result["success"] = $update;
      echo json_encode($result);
    }

    public function remove_member()
    {
      $id = $this->input->post("id");
      $delete = $this->M_kpi_sla_master->remove_member_where($id);
      $result["success"] = ($delete) ? true : false;
      echo json_encode($result);
    }

    public function delete_group()
    {
      $id = $this->input->post("id");
      $delete = $this->M_kpi_sla_master->delete_group_where($id);
      $result["success"] = ($delete) ? true : false;
      echo json_encode($result);
    }
}


?>
