<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Copa extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->model("M_copa_menu");
    }

    function copa_highlight() {
        $data = array(
            "content" => "Copa/copa-highlight",
            "title" => "COPA Highlight",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["COPA Highlight"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function executive_summary() {
        $data = array(
            "content" => "Copa/executive-summary",
            "title" => "Executive Summary",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["Executive Summary"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function financial_performance() {
        $data = array(
            "content" => "Copa/financial-performance",
            "title" => "Financial Performance Highlight",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["Financial Performance Highlight"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function segment() {
        $data = array(
            "content" => "Copa/segment",
            "title" => "Segment",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["Segment"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function s_department() {
        $data = array(
            "content" => "Copa/s-department",
            "title" => "S-Department",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["S-Department"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function s_pbth() {
        $data = array(
            "content" => "Copa/s-pbth",
            "title" => "S-PBTH",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["S-PBTH"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function s_ga_nga() {
        $data = array(
            "content" => "Copa/s-ga-nga",
            "title" => "S-GA-NGA",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["S-GA-NGA"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function s_customer() {
        $data = array(
            "content" => "Copa/s-customer",
            "title" => "S-Customer",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["S-Customer"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function pbth_tmb() {
        $data = array(
            "content" => "Copa/pbth-tmb",
            "title" => "PBTH-TMB",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["PBTH-TMB"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function ga_nga() {
        $data = array(
            "content" => "Copa/ga-nga",
            "title" => "GA-NGA",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["GA-NGA"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function s_segment() {
        $data = array(
            "content" => "Copa/s-segment",
            "title" => "S-Segment",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["S-Segment"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function aircraft_type() {
        $data = array(
            "content" => "Copa/aircraft-type",
            "title" => "Aircraft Type",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["Aircraft Type"],
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function menu_json() {
        $row = $this->M_copa_menu->getActiveMenu();
        echo json_encode($row);
    }

    function refresh() {
        $menu = $this->M_copa_menu->getMenuObject($this->uri->segment(3));
        $refresh = $this->M_copa_menu->getRefresh();
        $interfal = $refresh[0]->interfal;
        $satuan = $refresh[0]->satuan;
        $data = array(
            "content" => "Copa/refresh",
            "title" => "Setting Auto Refresh",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["Setting Auto Refresh"],
            "menu" => $this->M_menu->tampil(),
            "interfal" => $interfal,
            "satuan" => $satuan,
        );
        $this->load->view("layouts", $data);
    }

    function set_refresh()
    {
        $interfal = $this->input->post('interfal');
        $satuan = $this->input->post('satuan');

        $insert = $this->M_copa_menu->insertRefresh($interfal,$satuan);
        if ($insert) {
            echo "<script language='JavaScript'>
                swal({
                    title: 'Setting Success',
                    text: 'Setting Auto Refresh Succesfully!',
                    icon: 'success',
                }).then((value) => {
                    window.location.href='".base_url('index.php/Copa/refresh')."'
                });             
            </script>";
        }else{
            echo "<script language='JavaScript'>
                swal({
                    title: 'Setting Fail',
                    text: 'Setting Auto Refresh Failed!',
                    icon: 'error',
                }).then((value) => {
                    window.location.href='".base_url('index.php/Copa/refresh')."'
                });             
            </script>";
        }
    }

    function iframe() {
        $menu = $this->M_copa_menu->getMenuObject($this->uri->segment(3));
        $refresh = $this->M_copa_menu->getRefresh();
        $interfal = $refresh[0]->interfal;
        $satuan = $refresh[0]->satuan;
        if($satuan == "hari"){
            $waktu = ($interfal*24)*3600000;
        }elseif($satuan == "jam"){
            $waktu = $interfal*3600000;
        }elseif($satuan == "menit"){
            $waktu = $interfal*60000;
        }

        $data = array(
            "content" => "Copa/iframe",
            "title" => $menu->name,
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => [$menu->name],
            "iframe_url" => $menu->url,
            "menu" => $this->M_menu->tampil(),
            "waktu" => $waktu,
        );
        $this->load->view("layouts", $data);
    }

    function iframe_manager() {
        $data = array(
            "content" => "Copa/iframe",
            "title" => "Menu Manager",
            "title_box" => "",
            "small_tittle" => "",
            "breadcrumb" => ["Menu Manager"],
            "iframe_url" => base_url() . 'index.php/Copa/manager',
            "menu" => $this->M_menu->tampil(),
        );
        $this->load->view("layouts", $data);
    }

    function manager() {
        $crud = new grocery_CRUD();
        $crud->set_table('copa_menu');
        $crud->columns(array('seq', 'name', 'isactive'));
        $crud->fields('seq', 'name', 'url', 'isactive');
        // $crud->field_type('url', 'text');
        $crud->display_as('seq', 'Sequence')
                ->display_as('name', 'Name')
                ->display_as('url', 'URL')
                ->display_as('isactive', 'Is Active');
        $crud->order_by('seq');
        $crud->set_subject('Menu');
        $crud->set_crud_url_path(base_url() . 'index.php/Copa/manager');
        $crud->callback_after_insert(array($this, 'copa_menu_after_update'));
        $crud->callback_after_update(array($this, 'copa_menu_after_update'));
        $crud->callback_before_delete(array($this,'copa_menu_before_delete'));

        $output = $crud->render();

        $this->load->view("example", (array) $output);
    }

    function copa_menu_after_update($post_array, $primary_key) {
        $this->M_copa_menu->updateSeqMenu($primary_key);
        return TRUE;
    }

    function copa_menu_before_delete($primary_key) {
        $this->M_copa_menu->preDeleteSeqMenu($primary_key);
        return TRUE;
    }

}
