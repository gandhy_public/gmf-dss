<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class FinDashGw extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->findash_url = 'http://dev.gmf-aeroasia.co.id/app_dash_financial/restapi';
        $this->load->model('M_finance');
    }

/* 
--START--
Create Mas Okky
Data konsumsi untuk label dan grafik yang MTD dan YTD
*/
    function index() {
        $curl = '';
        for ($seg = 3; $seg <= 10; $seg++) {
            if ($this->uri->segment($seg) != FALSE) {
                $curl .= '/' . $this->uri->segment($seg);
            }
        }
        header('Content-Type: application/json');
        echo file_get_contents($this->findash_url . $curl);
    }

    function findata() {
        $tgl_data = $this->M_finance->exec("select distinct(tgl_data) as tgl from fin_data order by tgl desc");
        $tgl_data = $tgl_data[0]['tgl']; 
        if (empty($tgl_data)) {$tgl_data = "0000-00-00";}
        else{ $tgl_data = $tgl_data; }

        if ($this->uri->segment(4) != FALSE) {
            $where['fiscal_year'] = $this->uri->segment(4);
        }
        if ($this->uri->segment(5) != FALSE) {
            $where['fiscal_period'] = $this->uri->segment(5);
        }
        if ($this->uri->segment(3) == 'base') {
            $tipes = $this->M_finance->getTipe()->result();
            foreach ($tipes as $tipe) {
                $where['tipe'] = $tipe->id;
                $indikators = $this->M_finance->getIndikator()->result();
                foreach ($indikators as $indikator) {
                    $where['indikator'] = $indikator->id;
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['mtd'] = $this->M_finance->getFinDataBaseMTD($where['tipe'], $where['indikator'], $where['fiscal_year'], array_key_exists('fiscal_period', $where) ? $where['fiscal_period'] : NULL,$tgl_data)->result_array();
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['ytd'] = $this->M_finance->getFinDataBaseYTD($where['tipe'], $where['indikator'], $where['fiscal_year'], array_key_exists('fiscal_period', $where) ? $where['fiscal_period'] : NULL, $tgl_data)->result_array();
                }
            }
        } else if ($this->uri->segment(3) == 'line') {
            $tipes = $this->M_finance->getTipe()->result();
            foreach ($tipes as $tipe) {
                $where['tipe'] = $tipe->id;
                $indikators = $this->M_finance->getIndikator()->result();
                foreach ($indikators as $indikator) {
                    $where['indikator'] = $indikator->id;
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['mtd'] = $this->M_finance->getFinDataLineMTD($where['tipe'], $where['indikator'], $where['fiscal_year'], array_key_exists('fiscal_period', $where) ? $where['fiscal_period'] : NULL,$tgl_data)->result_array();
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['ytd'] = $this->M_finance->getFinDataLineYTD($where['tipe'], $where['indikator'], $where['fiscal_year'], array_key_exists('fiscal_period', $where) ? $where['fiscal_period'] : NULL,$tgl_data)->result_array();
                }
            }
        } else if ($this->uri->segment(3) == 'comp') {
            $tipes = $this->M_finance->getTipe()->result();
            foreach ($tipes as $tipe) {
                $where['tipe'] = $tipe->id;
                $indikators = $this->M_finance->getIndikator()->result();
                foreach ($indikators as $indikator) {
                    $where['indikator'] = $indikator->id;
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['mtd'] = $this->M_finance->getFinDataCompMTD($where['tipe'], $where['indikator'], $where['fiscal_year'], array_key_exists('fiscal_period', $where) ? $where['fiscal_period'] : NULL,$tgl_data)->result_array();
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['ytd'] = $this->M_finance->getFinDataCompYTD($where['tipe'], $where['indikator'], $where['fiscal_year'], array_key_exists('fiscal_period', $where) ? $where['fiscal_period'] : NULL,$tgl_data)->result_array();
                }
            }
        } else if ($this->uri->segment(3) == 'engine') {
            $tipes = $this->M_finance->getTipe()->result();
            foreach ($tipes as $tipe) {
                $where['tipe'] = $tipe->id;
                $indikators = $this->M_finance->getIndikator()->result();
                foreach ($indikators as $indikator) {
                    $where['indikator'] = $indikator->id;
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['mtd'] = $this->M_finance->getFinDataEngineMTD($where['tipe'], $where['indikator'], $where['fiscal_year'], array_key_exists('fiscal_period', $where) ? $where['fiscal_period'] : NULL,$tgl_data)->result_array();
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['ytd'] = $this->M_finance->getFinDataEngineYTD($where['tipe'], $where['indikator'], $where['fiscal_year'], array_key_exists('fiscal_period', $where) ? $where['fiscal_period'] : NULL,$tgl_data)->result_array();
                }
            }
        } else if ($this->uri->segment(3) == 'corp') {
            $tipes = $this->M_finance->getTipe()->result();
            foreach ($tipes as $tipe) {
                $where['tipe'] = $tipe->id;
                $indikators = $this->M_finance->getIndikator()->result();
                foreach ($indikators as $indikator) {
                    $where['indikator'] = $indikator->id;
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['mtd'] = $this->M_finance->getFinDataCorpMTD($where['tipe'], $where['indikator'], $where['fiscal_year'], array_key_exists('fiscal_period', $where) ? $where['fiscal_period'] : NULL,$tgl_data)->result_array();
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['ytd'] = $this->M_finance->getFinDataCorpYTD($where['tipe'], $where['indikator'], $where['fiscal_year'], array_key_exists('fiscal_period', $where) ? $where['fiscal_period'] : NULL,$tgl_data)->result_array();
                }
            }
        } else {
            $where['dinas'] = $this->uri->segment(3);
            $tipes = $this->M_finance->getTipe()->result();
            $where['tgl_data'] = $tgl_data;
            foreach ($tipes as $tipe) {
                $where['tipe'] = $tipe->id;
                $indikators = $this->M_finance->getIndikator()->result();
                foreach ($indikators as $indikator) {
                    $where['indikator'] = $indikator->id;
                    $where['tgl_data'] = $tgl_data;
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['mtd'] = $this->M_finance->getFinData('fiscal_year, fiscal_period, SUM(value) as value', $where, array('fiscal_year', 'fiscal_period'))->result_array();
                    $result[strtolower(preg_replace('/\s+/', '_', $tipe->name))][strtolower(preg_replace('/\s+/', '_', $indikator->name))]['ytd'] = $this->M_finance->getFinData('
                        fiscal_year, 
                        fiscal_period, 
                        (
                            SELECT SUM( value ) FROM fin_data
                            WHERE 
                            dinas = '.$where['dinas'].' AND
                            fiscal_year = '.$where['fiscal_year'].' AND
                            fiscal_period <= '.$where['fiscal_period'].' AND
                            tipe = '.$where['tipe'].' AND
                            indikator = '.$where['indikator'].' AND
                            tgl_data = '."'".$tgl_data."'".'
                        ) as value', $where)->result_array();
                }
            }
        }
        header('Content-Type: application/json');
        echo json_encode($result);
        //print("<pre>".print_r($result,true)."</pre>");
    }
    
/* 
--END--
*/

/* 
--START--
Create Dimas
Data konsumsi untuk tabel yang MTD, YTD dan YTE
*/
    function findata_table($param1, $param2, $param3, $param4){
        $tgl_data = $this->M_finance->exec("select distinct(tgl_data) as tgl from fin_data order by tgl desc");
        $tgl_data = $tgl_data[0]['tgl'];
        if (empty($tgl_data)) {$tgl_data = "0000-00-00";}
        else{ $tgl_data = $tgl_data; }
        if($param1 == '1' && $param2 == 'MTD'){
            $query_mtd['base']      = $this->M_finance->data_revenue_base_mtd($param3, $param4,$tgl_data);
            $query_mtd['line']      = $this->M_finance->data_revenue_line_mtd($param3, $param4,$tgl_data);
            $query_mtd['comp']      = $this->M_finance->data_revenue_comp_mtd($param3, $param4,$tgl_data);
            $query_mtd['engine']    = $this->M_finance->data_revenue_engine_mtd($param3, $param4,$tgl_data);
            $query_mtd['other']     = $this->M_finance->data_revenue_other_mtd($param3, $param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_mtd['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_mtd['mtd']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_mtd['mtd']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_mtd['mtd']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_mtd['mtd']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                    $budget_corp = $budget;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                    $rofo_corp  = $rofo;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofo           = $rofo_corp-$sum_rofo;
                $required       = ($budget_corp-$sum_budget)-($rofo_corp-$sum_rofo);
                $gap            = $required/$rofo;

                $query_mtd['mtd']['data']['other']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['other']['required'] = round($required/1000000, 2);
                $query_mtd['mtd']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_mtd['mtd']);
        }else if($param1 == '2' && $param2 == 'MTD'){
            $query_mtd['base']      = $this->M_finance->data_ebitda_base_mtd($param3, $param4,$tgl_data);
            $query_mtd['line']      = $this->M_finance->data_ebitda_line_mtd($param3, $param4,$tgl_data);
            $query_mtd['comp']      = $this->M_finance->data_ebitda_comp_mtd($param3, $param4,$tgl_data);
            $query_mtd['engine']    = $this->M_finance->data_ebitda_engine_mtd($param3, $param4,$tgl_data);
            $query_mtd['other']     = $this->M_finance->data_ebitda_other_mtd($param3, $param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_mtd['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_mtd['mtd']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_mtd['mtd']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_mtd['mtd']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_mtd['mtd']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                    $budget_corp = $budget;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                    $rofo_corp  = $rofo;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofo           = $rofo_corp-$sum_rofo;
                $required       = ($budget_corp-$sum_budget)-($rofo_corp-$sum_rofo);
                $gap            = $required/$rofo;

                $query_mtd['mtd']['data']['other']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['other']['required'] = round($required/1000000, 2);
                $query_mtd['mtd']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_mtd['mtd']);
        }else if($param1 == '3' && $param2 == 'MTD'){
            $query_mtd['base']      = $this->M_finance->data_op_base_mtd($param3, $param4,$tgl_data);
            $query_mtd['line']      = $this->M_finance->data_op_line_mtd($param3, $param4,$tgl_data);
            $query_mtd['comp']      = $this->M_finance->data_op_comp_mtd($param3, $param4,$tgl_data);
            $query_mtd['engine']    = $this->M_finance->data_op_engine_mtd($param3, $param4,$tgl_data);
            $query_mtd['other']     = $this->M_finance->data_op_other_mtd($param3, $param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_mtd['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_mtd['mtd']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_mtd['mtd']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_mtd['mtd']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_mtd['mtd']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofos          = $rofo-$sum_rofo;
                $required       = ($budget-$sum_budget)-($rofo-$sum_rofo);
                $gap            = $required/$rofos;

                $query_mtd['mtd']['data']['other']['rofo']     = round($rofos/1000000, 2); 
                $query_mtd['mtd']['data']['other']['required'] = round($required/1000000, 2);
                $query_mtd['mtd']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_mtd['mtd']);
        }else if($param1 == '4' && $param2 == 'MTD'){
            $query_mtd['base']      = $this->M_finance->data_expense_base_mtd($param3, $param4,$tgl_data);
            $query_mtd['line']      = $this->M_finance->data_expense_line_mtd($param3, $param4,$tgl_data);
            $query_mtd['comp']      = $this->M_finance->data_expense_comp_mtd($param3, $param4,$tgl_data);
            $query_mtd['engine']    = $this->M_finance->data_expense_engine_mtd($param3, $param4,$tgl_data);
            $query_mtd['other']     = $this->M_finance->data_expense_other_mtd($param3, $param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_mtd['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_mtd['mtd']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_mtd['mtd']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_mtd['mtd']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_mtd['mtd']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_mtd['mtd']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_mtd['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                    $budget_corp = $budget;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                    $rofo_corp  = $rofo;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofo           = $rofo_corp-$sum_rofo;
                $required       = ($budget_corp-$sum_budget)-($rofo_corp-$sum_rofo);
                $gap            = $required/$rofo;

                $query_mtd['mtd']['data']['other']['rofo']     = round($rofo/1000000, 2); 
                $query_mtd['mtd']['data']['other']['required'] = round($required/1000000, 2);
                $query_mtd['mtd']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_mtd['mtd']);
        }else if($param1 == '1' && $param2 == 'YTD'){
            $query_ytd['base']      = $this->M_finance->data_revenue_base_ytd($param3, $param4,$tgl_data);
            $query_ytd['line']      = $this->M_finance->data_revenue_line_ytd($param3, $param4,$tgl_data);
            $query_ytd['comp']      = $this->M_finance->data_revenue_comp_ytd($param3, $param4,$tgl_data);
            $query_ytd['engine']    = $this->M_finance->data_revenue_engine_ytd($param3, $param4,$tgl_data);
            $query_ytd['other']     = $this->M_finance->data_revenue_other_ytd($param3, $param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_ytd['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_ytd['ytd']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_ytd['ytd']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_ytd['ytd']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_ytd['ytd']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                    $budget_corp = $budget;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                    $rofo_corp  = $rofo;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofo           = $rofo_corp-$sum_rofo;
                $required       = ($budget_corp-$sum_budget)-($rofo_corp-$sum_rofo);
                $gap            = $required/$rofo;


                $query_ytd['ytd']['data']['other']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['other']['required'] = round($required/1000000, 2);
                $query_ytd['ytd']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_ytd['ytd']);
        }else if($param1 == '2' && $param2 == 'YTD'){
            $query_ytd['base']      = $this->M_finance->data_ebitda_base_ytd($param3, $param4,$tgl_data);
            $query_ytd['line']      = $this->M_finance->data_ebitda_line_ytd($param3, $param4,$tgl_data);
            $query_ytd['comp']      = $this->M_finance->data_ebitda_comp_ytd($param3, $param4,$tgl_data);
            $query_ytd['engine']    = $this->M_finance->data_ebitda_engine_ytd($param3, $param4,$tgl_data);
            $query_ytd['other']     = $this->M_finance->data_ebitda_other_ytd($param3, $param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_ytd['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_ytd['ytd']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_ytd['ytd']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['mtd']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_ytd['ytd']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_ytd['ytd']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                    $budget_corp = $budget;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                    $rofo_corp  = $rofo;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofo           = $rofo_corp-$sum_rofo;
                $required       = ($budget_corp-$sum_budget)-($rofo_corp-$sum_rofo);
                $gap            = $required/$rofo;

                $query_ytd['ytd']['data']['other']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['other']['required'] = round($required/1000000, 2);
                $query_ytd['ytd']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_ytd['ytd']);
        }else if($param1 == '3' && $param2 == 'YTD'){
            $query_ytd['base']      = $this->M_finance->data_op_base_ytd($param3, $param4,$tgl_data);
            $query_ytd['line']      = $this->M_finance->data_op_line_ytd($param3, $param4,$tgl_data);
            $query_ytd['comp']      = $this->M_finance->data_op_comp_ytd($param3, $param4,$tgl_data);
            $query_ytd['engine']    = $this->M_finance->data_op_engine_ytd($param3, $param4,$tgl_data);
            $query_ytd['other']     = $this->M_finance->data_op_other_ytd($param3, $param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_ytd['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_ytd['ytd']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_ytd['ytd']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_ytd['ytd']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_ytd['ytd']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                    $budget_corp = $budget;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                    $rofo_corp  = $rofo;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofo           = $rofo_corp-$sum_rofo;
                $required       = ($budget_corp-$sum_budget)-($rofo_corp-$sum_rofo);
                $gap            = $required/$rofo;

                $query_ytd['ytd']['data']['other']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['other']['required'] = round($required/1000000, 2);
                $query_ytd['ytd']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_ytd['ytd']);
        }else if($param1 == '4' && $param2 == 'YTD'){
            $query_ytd['base']      = $this->M_finance->data_expense_base_ytd($param3, $param4,$tgl_data);
            $query_ytd['line']      = $this->M_finance->data_expense_line_ytd($param3, $param4,$tgl_data);
            $query_ytd['comp']      = $this->M_finance->data_expense_comp_ytd($param3, $param4,$tgl_data);
            $query_ytd['engine']    = $this->M_finance->data_expense_engine_ytd($param3, $param4,$tgl_data);
            $query_ytd['other']     = $this->M_finance->data_expense_other_ytd($param3, $param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_ytd['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_ytd['ytd']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_ytd['ytd']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_ytd['ytd']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_ytd['ytd']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_ytd['ytd']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_ytd['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                    $budget_corp = $budget;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                    $rofo_corp  = $rofo;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofo           = $rofo_corp-$sum_rofo;
                $required       = ($budget_corp-$sum_budget)-($rofo_corp-$sum_rofo);
                $gap            = $required/$rofo;

                $query_ytd['ytd']['data']['other']['rofo']     = round($rofo/1000000, 2); 
                $query_ytd['ytd']['data']['other']['required'] = round($required/1000000, 2);
                $query_ytd['ytd']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_ytd['ytd']);
        }else if($param1 == '1' && $param2 == 'YTE'){
            $query_yte['base']      = $this->M_finance->data_revenue_base_yte($param4,$tgl_data);
            $query_yte['line']      = $this->M_finance->data_revenue_line_yte($param4,$tgl_data);
            $query_yte['comp']      = $this->M_finance->data_revenue_comp_yte($param4,$tgl_data);
            $query_yte['engine']    = $this->M_finance->data_revenue_engine_yte($param4,$tgl_data);
            $query_yte['other']     = $this->M_finance->data_revenue_other_yte($param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_yte['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_yte['yte']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_yte['yte']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_yte['yte']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_yte['yte']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                    $budget_corp = $budget;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                    $rofo_corp  = $rofo;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofo           = $rofo_corp-$sum_rofo;
                $required       = ($budget_corp-$sum_budget)-($rofo_corp-$sum_rofo);
                $gap            = $required/$rofo;

                $query_yte['yte']['data']['other']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['other']['required'] = round($required/1000000, 2);
                $query_yte['yte']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_yte['yte']);
        }else if($param1 == '2' && $param2 == 'YTE'){
            $query_yte['base']      = $this->M_finance->data_ebitda_base_yte($param4,$tgl_data);
            $query_yte['line']      = $this->M_finance->data_ebitda_line_yte($param4,$tgl_data);
            $query_yte['comp']      = $this->M_finance->data_ebitda_comp_yte($param4,$tgl_data);
            $query_yte['engine']    = $this->M_finance->data_ebitda_engine_yte($param4,$tgl_data);
            $query_yte['other']     = $this->M_finance->data_ebitda_other_yte($param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_yte['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_yte['yte']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_yte['yte']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_yte['yte']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_yte['yte']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                    $budget_corp = $budget;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                    $rofo_corp  = $rofo;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofo           = $rofo_corp-$sum_rofo;
                $required       = ($budget_corp-$sum_budget)-($rofo_corp-$sum_rofo);
                $gap            = $required/$rofo;

                $query_yte['yte']['data']['other']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['other']['required'] = round($required/1000000, 2);
                $query_yte['yte']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_yte['yte']);
        }else if($param1 == '3' && $param2 == 'YTE'){
            $query_yte['base']      = $this->M_finance->data_op_base_yte($param4,$tgl_data);
            $query_yte['line']      = $this->M_finance->data_op_line_yte($param4,$tgl_data);
            $query_yte['comp']      = $this->M_finance->data_op_comp_yte($param4,$tgl_data);
            $query_yte['engine']    = $this->M_finance->data_op_engine_yte($param4,$tgl_data);
            $query_yte['other']     = $this->M_finance->data_op_other_yte($param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_yte['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_yte['yte']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_yte['yte']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_yte['yte']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_yte['yte']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                    $budget_corp = $budget;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                    $rofo_corp  = $rofo;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofo           = $rofo_corp-$sum_rofo;
                $required       = ($budget_corp-$sum_budget)-($rofo_corp-$sum_rofo);
                $gap            = $required/$rofo;

                $query_yte['yte']['data']['other']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['other']['required'] = round($required/1000000, 2);
                $query_yte['yte']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_yte['yte']);
        }else if($param1 == '4' && $param2 == 'YTE'){
            $query_yte['base']      = $this->M_finance->data_expense_base_yte($param4,$tgl_data);
            $query_yte['line']      = $this->M_finance->data_expense_line_yte($param4,$tgl_data);
            $query_yte['comp']      = $this->M_finance->data_expense_comp_yte($param4,$tgl_data);
            $query_yte['engine']    = $this->M_finance->data_expense_engine_yte($param4,$tgl_data);
            $query_yte['other']     = $this->M_finance->data_expense_other_yte($param4,$tgl_data);
            $arr1                   = [];
            $arr2                   = [];
            $arr3                   = [];
            $arr4                   = [];
            $arr5                   = [];
            $arr6                   = [];
            $arr7                   = [];
            $arr8                   = [];

            foreach ($query_yte['base'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr1, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr2, $rofo);
                }
                $query_yte['yte']['data']['base']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['base']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['base']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['line'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr3, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr4, $rofo);
                }
                $query_yte['yte']['data']['line']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['line']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['line']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['comp'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr5, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr6, $rofo);
                }
                $query_yte['yte']['data']['comp']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['comp']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['comp']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['engine'] as $key) {
                if($key->tipe=='Budget'){
                    $budget = $key->total;
                    array_push($arr7, $budget);
                }
                if($key->tipe=='Rofo'){
                    $rofo   = $key->total;
                    array_push($arr8, $rofo);
                }
                $query_yte['yte']['data']['engine']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['engine']['required'] = round(($budget-$rofo)/1000000, 2);
                $query_yte['yte']['data']['engine']['gap']      = round((($budget-$rofo)/$rofo)*100, 2);
            }
            foreach ($query_yte['other'] as $key) {
                if($key->tipe=='Budget'){
                    $budget      = $key->total;
                    $budget_corp = $budget;
                }
                if($key->tipe=='Rofo'){
                    $rofo       = $key->total;
                    $rofo_corp  = $rofo;
                }
                $merge_rofo     = array_merge($arr2, $arr4, $arr6, $arr8);
                $sum_rofo       = array_sum($merge_rofo);
                $merge_budget   = array_merge($arr1, $arr3, $arr5, $arr7);
                $sum_budget     = array_sum($merge_budget);
                $rofo           = $rofo_corp-$sum_rofo;
                $required       = ($budget_corp-$sum_budget)-($rofo_corp-$sum_rofo);
                $gap            = $required/$rofo;

                $query_yte['yte']['data']['other']['rofo']     = round($rofo/1000000, 2); 
                $query_yte['yte']['data']['other']['required'] = round($required/1000000, 2);
                $query_yte['yte']['data']['other']['gap']      = round($gap*100, 2);
            }
            echo json_encode($query_yte['yte']);
        }
        
    }

/* 
--END--
*/

}
