<?php

 defined('BASEPATH') OR exit('No direct script access allowed');


  class Ctd extends MY_Controller{

    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model("M_menu");
    }
	
    function index(){
      $data = array(
        "content"       => "ctd/ctd_grafik",
        "title"         => "Detail Delay",
        "small_tittle"  => "",
        "breadcrumb"    => ["Contribution of Technical Delay"],
        "menu"          => $this->M_menu->tampil(),
      );
      $this->load->view("layouts", $data);
    }

  }
