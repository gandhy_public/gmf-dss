<?php

  defined('BASEPATH') OR exit('No direct script access allowed');
  ini_set('max_execution_time', 0);
  ini_set('memory_limit', '-1');

  class Hilldashboard extends CI_Controller
  {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("M_menu");
        $this->load->model("M_hill_dash");
    }

    function index()
    {
        $data = array(
            "content" => "hil/dashboardhill",
            "title" => "Hil Status",
            "small_tittle" => "",
            "breadcrumb" => ["Hil Status"],
            "menu" => $this->M_menu->tampil()
        );
        $this->load->view("layouts", $data);
    }

    function master_manufacture()
    {
        $query = $this->M_hill_dash->master_manufacture();
        echo json_encode($query);
    }

    function master_type($id)
    {
        $query = $this->M_hill_dash->master_reg($id);
        echo json_encode($query);
    }

    function count_garuda_open()
    {
        $query = $this->M_hill_dash->garuda_open();
        if($query[0]['TOTALOPEN']=='' || $query[0]['TOTALOPEN']==null){
            $count = 0;
        }else{
            $count = $query[0]['TOTALOPEN'];
        }
        // foreach ($query as $key) {
        //     $count += $key['count_data'];
        // }
        echo json_encode($count);
    }

    function garuda_open()
    {
        $query = $this->M_hill_dash->garuda_open_table();
        if (count($query) == 0) {
            $status = 0;
            echo json_encode(array('status' => $status, 'data' => $query));
        } else {
            $status = 1;
            echo json_encode(array('status' => $status, 'data' => $query));
        }
    }

    function garuda_detail_open($id)
    {
        $query = $this->M_hill_dash->garuda_detail_open($id);
        echo json_encode($query);
    }

    function count_citilink_open()
    {
        $query = $this->M_hill_dash->citilink_open();
        if($query[0]['TOTALOPEN']=='' || $query[0]['TOTALOPEN']==null){
            $count = 0;
        }else{
            $count = $query[0]['TOTALOPEN'];
        }
        echo json_encode($count);
    }

    function citilink_open()
    {
        $query = $this->M_hill_dash->citilink_open_table();
        if (count($query) == 0) {
            $status = 0;
            echo json_encode(array('status' => $status, 'data' => $query));
        } else {
            $status = 1;
            echo json_encode(array('status' => $status, 'data' => $query));
        }
    }

    function citilink_detail_open($id)
    {
        $query = $this->M_hill_dash->citilink_detail_open($id);
        echo json_encode($query);
    }

    function actual_target_manufacture()
    {
        $query_garuda = $this->M_hill_dash->garuda_open_table();
        $query_citilink = $this->M_hill_dash->citilink_open_table();
        $query_ga = $this->M_hill_dash->count_reg_per_type_garuda();

        if(count($query_garuda)!=0){
            $value_garuda=0;
            foreach($query_garuda as $key){
                
                $index = array_search($key['actype_id'], array_column($query_ga, 'actype_id'));
                if($index!=false){
                    $temp=$key['total'] / $query_ga[$index]['total_reg'];
                }else{
                    $temp=0;
                }
                $value_garuda+=$temp;
            }            
        }else{
            $value_garuda=0;
        }

        //  $val_garuda =  count($query_garuda);
         $val_garuda = round(($value_garuda / count($query_garuda)), 2);


        $query_ct= $this->M_hill_dash->count_reg_per_type_citilink();

        if(count($query_citilink)!=0){
            $value_citilink=0;
            foreach($query_citilink as $key){
                
                $index = array_search($key['actype_id'], array_column($query_ct, 'actype_id'));
                if($index!=false){
                    $temp=$key['total'] / $query_ct[$index]['total_reg'];
                }else{
                    $temp=0;
                }
                $value_citilink+=$temp;
            }            
        }else{
            $value_citilink=0;
        }

        $val_citilink = round(($value_citilink / count($query_citilink)), 2);
        // $val_citilink = count($query_citilink);
   
        //TARGET HIL
        $target_hil = $this->M_hill_dash->target_hil();
        foreach ($target_hil as $key) {
            $value_target = round(($key->target_val), 2);
        }
        
        echo json_encode(array('target' => $value_target, 'garuda' => $val_garuda, 'citilink' => $val_citilink));
        // exit;

        // //AKUMULASI HIL GARUDA
        // $array_tampung_garuda = array();
        // foreach ($query_garuda as $key) {
        //     $query = $this->M_hill_dash->count_reg_per_type_garuda($key['actype_id']);
        //     foreach ($query as $row) {
        //         if ($key['actype_id'] == $row['actype_id']) {
        //             $total = $key['count_data'] / $row['total_reg'];
        //             $array_tampung_garuda[] = $total;

        //         }
        //     }
        // }

        // $sum_garuda = array_sum($array_tampung_garuda);
        // $value_garuda = round(($sum_garuda / count($query_garuda)), 2);

        // //AKUMULASI HIL CITILINK
        // $array_tampung_citilink = array();
        // foreach ($query_citilink as $key) {
        //     $query = $this->M_hill_dash->count_reg_per_type_citilink($key['actype_id']);
        //     foreach ($query as $row) {
        //         if ($key['actype_id'] == $row['actype_id']) {
        //             $total = $key['count_data'] / $row['total_reg'];
        //             $array_tampung_citilink[] = $total;
        //         }
        //     }
        // }
        // $sum_citilink = array_sum($array_tampung_citilink);
        // $value_citilink = round(($sum_citilink / count($query_citilink)), 2);

        

    }

    function data_tren_hil($id_manufacture, $id_type, $week1, $week2)
    {

        $week_1c = explode("-", $week1);
        $week_2c = explode("-", $week2);
         $time_start = $week_1c[2].'-'.$week_1c[1].'-'.$week_1c[0];
        $time_end = $week_2c[2].'-'.$week_2c[1].'-'.$week_2c[0];

        $week_1c = mktime(0, 0, 0, $week_1c[1], $week_1c[0], $week_1c[2]);
        $week_1c = (int)date('W', $week_1c);
        $week_2c = mktime(0, 0, 0, $week_2c[1], $week_2c[0], $week_2c[2]);
        $week_2c = (int)date('W', $week_2c);
        $week_plus_one_c = $week_1c + 1;
        $week_range = range($week_1c, $week_2c);

        // echo $time_start."<br>";
        // echo $time_end."<br>";
      

        // $week_1g = explode("-", $week1);
        // $week_1g = mktime(0, 0, 0, $week_1g[1], $week_1g[0], $week_1g[2]);
        // $week_1g = (int)date('W', $week_1g);
        // $week_2g = explode("-", $week2);
        // $week_2g = mktime(0, 0, 0, $week_2g[1], $week_2g[0], $week_2g[2]);
        // $week_2g = (int)date('W', $week_2g);
        // $week_plus_one_g = $week_1g + 1;
        // $week_range_g = range($week_1g, $week_2g);

        $trend_ga =$this->M_hill_dash->dataTrend('GA',$time_start,$time_end,$id_type);
        $trend_ct =$this->M_hill_dash->dataTrend_ct('CITILINK',$time_start,$time_end,$id_type);
        
        // echo json_encode($trend_ga);exit;
       
        $ga_arr=array();
        $ct_arr=array();
        $temp_ga=0;
        $temp_ct=0;
        foreach($week_range as $k){
            // echo json_encode($index);exit;
            for ($i=1; $i <=$k; $i++) { 
                $index = array_search($i, array_column($trend_ga, 'weeks'));
                if($index!==false){
                    $temp_ga+=$trend_ga[$index]['JML'];
                }
            }
            array_push($ga_arr,$temp_ga);
            $temp_ga=0;
            // echo json_encode($week_range);exit;
            // echo $k;exit;
            for ($i=1; $i <=$k; $i++) { 
                $index = array_search($i, array_column($trend_ct, 'weeks'));
                if($index!==false){
                    $temp_ct+=$trend_ct[$index]['JML'];
                }
            }
            array_push($ct_arr,$temp_ct);
            $temp_ct=0;
            // echo json_encode($trend_ct);exit;
        }
        echo json_encode(array('categories' => $week_range, 'data_garuda' => $ga_arr, 'data_citilink' => $ct_arr));
        exit;



        if ($id_manufacture == 00) {
            $array_garuda = array();
            $groups_garuda = array();
            $total_hil_garuda = 0;
            foreach ($week_range_g as $row) {
                $query_garuda = $this->M_hill_dash->data_tren_hil('12', $id_type, $week_1g, $week_plus_one_g, $week_2g);
                $week_1g++;
                $week_plus_one_g++;
                $total_hil_garuda = 0;
                foreach ($query_garuda as $key) {
                    $total_hil_garuda += $key['hil_open'] + $key['hil_complete'];
                }
                $array_garuda[] = $total_hil_garuda;
            }

            $array_citilink = array();
            $groups_citilink = array();
            $total_hil_citilink = 0;
            foreach ($week_range_c as $row) {
                $query_citilink = $this->M_hill_dash->data_tren_hil('11', $id_type, $week_1c, $week_plus_one_c, $week_2c);
                $week_1c++;
                $week_plus_one_c++;
                $total_hil_citilink = 0;
                foreach ($query_citilink as $key) {
                    $total_hil_citilink += $key['hil_open'] + $key['hil_complete'];
                }
                $array_citilink[] = $total_hil_citilink;
            }
            echo json_encode(array('categories' => $week_range_c, 'data_garuda' => $array_garuda, 'data_citilink' => $array_citilink));

        } else if ($id_manufacture == 11) {
            $array_citilink = array();
            $groups_citilink = array();
            $total_hil_citilink = 0;
            foreach ($week_range_c as $row) {
                $query_citilink = $this->M_hill_dash->data_tren_hil($id_manufacture, $id_type, $week_1c, $week_plus_one_c, $week_2c);

                $week_1c++;
                $week_plus_one_c++;
                $total_hil_citilink = 0;
                foreach ($query_citilink as $key) {
                    $total_hil_citilink += $key['hil_open'] + $key['hil_complete'];
                }

                $array_citilink[] = $total_hil_citilink;
            }
            echo json_encode(array('categories' => $week_range_c, 'data_citilink' => $array_citilink));

        } else if ($id_manufacture == 12) {
            $array_garuda = array();
            $groups_garuda = array();
            $total_hil_garuda = 0;
            foreach ($week_range_g as $row) {
                $query_garuda = $this->M_hill_dash->data_tren_hil($id_manufacture, $id_type, $week_1g, $week_plus_one_g, $week_2g);
                $week_1g++;
                $week_plus_one_g++;
                $total_hil_garuda = 0;
                foreach ($query_garuda as $key) {
                    $total_hil_garuda += $key['hil_open'] + $key['hil_complete'];
                }
                $array_garuda[] = $total_hil_garuda;
            }
            echo json_encode(array('categories' => $week_range_g, 'data_garuda' => $array_garuda));
        }
    }

    function mitigasi_hil($id)
    {
        $data = $this->M_hill_dash->mitigasi_hil($id);
        echo json_encode($data);
    }

    function detail_grafik($id, $id_type, $week_start, $week_plus_one, $week_end)
    {
        $data = $this->M_hill_dash->detail_grafiks($id, $id_type, $week_start);
        echo json_encode($data);
    }


  }