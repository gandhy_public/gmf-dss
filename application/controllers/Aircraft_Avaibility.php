<?php
defined("BASEPATH") or exit('no direct script access allowed');


/**
 *
 */
class Aircraft_Avaibility extends MY_Controller
{

    public $service;

    public function __construct()
    {
        # code...
        parent::__construct();
        $this->service = $this->load->database("service", true);
        $this->load->database();
        $this->load->helper("url");
        $this->load->helper("encript");
        $this->load->model("M_menu");
        $this->load->model("M_aircraft_avaibility");
    }

    public function index()
    {
        $sekarang = $this->M_aircraft_avaibility->get_panel("sekarang");
        $panel1 = $this->M_aircraft_avaibility->get_panel1("A320");
        $panel2 = $this->M_aircraft_avaibility->get_panel1("Classic");

        //$getLastDate = $this->M_aircraft_avaibility->getLastDate();

        $ytd = date('d-m-Y');

        // $target =   str_replace(',', '.', is_null($sekarang) ? 0 : $sekarang->target);
        $target = 100;
        $sekarang = round(str_replace(',', '.', is_null($sekarang) ? 0 : $sekarang->Availability), 2);
        if ($sekarang >= $target) {
            $p_sekarang = '<i class="fa fa-caret-up"></i>';
            $w_sekarang = '#04ff02';
        } else if ($sekarang < $target) {
            $p_sekarang = '<i class="fa fa-caret-down"></i>';
            $w_sekarang = '#ff0505';
        } else {
            $p_sekarang = '';
            $w_sekarang = '';
        }
        $t_target = str_replace(',', '.', $panel1->TARGET);
        $A320 = round(str_replace(',', '.', $panel1->Availability), 2);
        if ($A320 >= $target) {
            $w_A320 = '#04ff02';
        } else if ($A320 < $target) {
            $w_A320 = '#ff0505';
        } else {
            $w_A320 = '';
        }
        $Classic = round(str_replace(',', '.', $panel2->Availability), 2);
        if ($Classic >= $target) {
            $w_Classic = '#04ff02';
        } else if ($Classic < $target) {
            $w_Classic = '#ff0505';
        } else {
            $w_Classic = '';
        }

        if ($A320 == 0) {
            $HA320 = ' style="display:none;"';
        } else {
            $HA320 = '';
        };
        if ($Classic == 0) {
            $HClassic = ' style="display:none;"';
        } else {
            $HClassic = '';
        };

        $data = array(
            "content" => "Aircraft Avaibility/index",
            "title" => "Aircraft Avaibility",
            "title1" => "Year to Date",
            "title2" => "Aircraft Type",
            "small_tittle" => "",
            "breadcrumb" => [""],
            "menu" => $this->M_menu->tampil(),
            "target" => round($target, 2) . "%",
            "sekarang" => round($sekarang, 2) ."%",
            "p_sekarang" => $p_sekarang,
            "w_sekarang" => $w_sekarang,
            "A320" => round($A320, 2) . "%",
            "Classic" => round($Classic, 2) . "%",
            "HA320" => $HA320,
            "HClassic" => $HClassic,
            "WA320" => $w_A320,
            "WClassic" => $w_Classic,
            'ytd' => $ytd,
            // 'pulsaA320' => $getLastDate->pulsa_A320,
            // 'pulsaClassic' => $getLastDate->pulsa_classic,
        );
        $this->load->view("layouts", $data);
    }


    public function view_grafik()
    {
        $start  = $this->input->post("start");
        $end    = $this->input->post("end");
        $tipe   = $this->input->post("tipe");
        $grafik = $this->M_aircraft_avaibility->get_grafik(date('Y-m-d', strtotime($start)), date('Y-m-d', strtotime($end)), $tipe);
        $target = $this->M_aircraft_avaibility->get_grafik_target(date('Y-m-d', strtotime($start)), date('Y-m-d', strtotime($end)), $tipe);

        $data['data'] = array();
        $data['datap'] = array();
        $data['datat'] = array();
        $minimal = 100;

        foreach ($grafik as $gv_key => $gv_value) {
            if ($minimal > floatval($gv_value->ActServ)) {
                $minimal = floatval($gv_value->ActServ);
            }
            $actsrv = str_replace(',', '.', is_null($gv_value->ActServ) ? 0 : $gv_value->ActServ);
            $actsrv = array(floatval($actsrv));
            $plan   = str_replace(',', '.', is_null($gv_value->plan) ? 0 : $gv_value->plan);
            $plan   = array(floatval(number_format($plan, 2)));
            $valuet = date('d-m-Y', strtotime($gv_value->date));
            array_push($data['data'], $actsrv);
            array_push($data['datap'], $plan);
            array_push($data['datat'], $valuet);
        }
        $json[] = $data;

        echo json_encode(
            array(
                'grafik' => $json,
                'target' => '90',
                'minimal' => $minimal,
            )
        );
    }

    public function storeupload(){
        $this->load->library('PHPExcel');
        $destination_folder = 'assets/upload';
        // Change name files
        $new_name_file = date("Ymd_His") . '_' . $_FILES['files']['name'];
        $file = "$destination_folder/$new_name_file";

        $today = date("Y-m-d H:i:s");

        // insert Data base
        move_uploaded_file($_FILES['files']['tmp_name'], $file);
        
        // exit();

        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        //get only the Cell Collection
        // $cell_collection = $objPHPExcel->getActiveSheet(0)->getCellCollection();
        $cell_collection = $objPHPExcel->setActiveSheetIndex(0)->toArray(null, true, true, true);

        // Template Perfomance
        unlink($file);

        // Loop For get Start
        

       
        $on_off=$this->input->post('dataselected',true);
        $lastdata=count($cell_collection);
        if($on_off==1){
            $start =2;
            $end = $lastdata;
        }else{
            $start =$this->input->post('start_rows',true);
            $end = $this->input->post('end_rows',true);

            if($start=="" or $end==""){
                 $print=array('info'=> "warning",'msg'=> "Please Fill Start Rows and End Rows");
                 echo json_encode($print);exit;
            }
            if($end < $start){
                 $print=array('info'=> "warning",'msg'=> "End Rows Must Be Greather then Start Rows");
                 echo json_encode($print);exit;
            }

            if( $end > $lastdata ){
                $end=$lastdata;
            }
            if($start <=1){
                $start=2;
            }
        }
       

        $loops=$start;
        $dataInsert=array();



        // echo $cell_collection[1]['A'];exit;


        if(count($cell_collection[1])==12){

            for ($i=$start ; $i <= $end ; $i++) { 
            //     # code...
            // }
            // foreach ($cell_collection as $key) {
            // $loops+=1;  

            // if(($key['A']!=null && $key['B']!=null && $key['C']!=null && $key['D']!=null && $key['E']!=null && $key['F']!=null && $key['G']!=null && $key['H']!=null && $key['I']!=null && $key['J']!=null && $key['K']!=null && $key['L']!=null) && $loops!=1 ){
                if($cell_collection[$i]['B']!=null && $loops!=1){
                # code...
                    $month=explode('-',$cell_collection[$i]['B']);   
                   if(strlen($month[1])!=3){
                         $print=array('info'=> "warning",'msg'=> "Please Check Date Format , make Sure formated Month Formated Like Jan Or Feb ");
                         echo json_encode($print);exit;
                    }
                    
                    if(strlen($month[2])==2 ){
                        $year="20".$month[2];
                    }else{
                        $year=$month[2];
                    }                
                        switch ($month[1]) {
                            case "Jan":
                                $mon="01";
                                break;
                            case "Feb":
                                $mon="02";
                                break;
                            case "Mar":
                                $mon="03";
                                break;
                            case "Apr":
                                $mon="04";
                                break;
                            case "May":
                                $mon="05";
                                break;
                            case "Jun":
                                $mon="06";
                                break;
                            case "Jul":
                                $mon="07";
                                break;
                            case "Aug":
                                $mon="08";
                                break;
                            case "Sep":
                                $mon="09";
                                break; 
                             case "Oct":
                                $mon="10";
                                break;
                             case "Nov":
                                $mon="11";
                                break;  
                             case "Dec":
                                $mon="12";
                                break;
                        }
                        
                    $tgl=$year."-".$mon."-".$month[0];
                    $dataInsert[]=array(
                        'Date'=> $tgl,
                        'NoOfAllAC' => $cell_collection[$i]['C'],
                        'Avaibility' => $cell_collection[$i]['D'],
                        'Avaibility_' => $cell_collection[$i]['E'],
                        'StandbyA320' => $cell_collection[$i]['F'],
                        'StandbyClassic' => $cell_collection[$i]['G']
                    );
                    // echo $loops;
                }
            }

            // Insert Into DB
            $tbl='graph_n';
            $query=$this->M_aircraft_avaibility->insert_graph_n_bacth($dataInsert,$tbl);



        // Template Air Bus
        }elseif(count($cell_collection[1]==17)){
            
             for ($i=$start ; $i <= $end ; $i++) { 
            // foreach ($cell_collection as $key) {
                # code...
                // $loops+=1;  

            // if(($key['A']!=null && $key['B']!=null && $key['C']!=null && $key['D']!=null && $key['E']!=null && $key['F']!=null && $key['G']!=null && $key['H']!=null && $key['I']!=null && $key['J']!=null && $key['K']!=null && $key['L']!=null) && $loops!=1 ){
                if($cell_collection[$i]['B']!=null && $loops!=1){
                # code...
                    $month=explode('-',$cell_collection[$i]['B']);

                    // if(strlen($month[2])!=4 or strlen($month[1])!=3){
                    if(strlen($month[1])!=3){
                         $print=array('info'=> "warning",'msg'=> "Please Check Date Format , make Sure formated Month Formated Like Jan Or Feb ");
                         echo json_encode($print);exit;
                    }
                    
                    if(strlen($month[2])==2 ){
                        $year="20".$month[2];
                    }else{
                        $year=$month[2];
                    }
                    
                        switch ($month[1]) {
                            case "Jan":
                                $mon="01";
                                break;
                            case "Feb":
                                $mon="02";
                                break;
                            case "Mar":
                                $mon="03";
                                break;
                            case "Apr":
                                $mon="04";
                                break;
                            case "May":
                                $mon="05";
                                break;
                            case "Jun":
                                $mon="06";
                                break;
                            case "Jul":
                                $mon="07";
                                break;
                            case "Aug":
                                $mon="08";
                                break;
                            case "Sep":
                                $mon="09";
                                break; 
                             case "Oct":
                                $mon="10";
                                break;
                             case "Nov":
                                $mon="11";
                                break;  
                             case "Dec":
                                $mon="12";
                                break;
                        }
                        
                    $tgl=$year."-".$mon."-".$month[0];
                    $dataInsert[]=array(
                        'Date'=>$tgl,
                        'ac_type'=>'A320',
                        'NoAC'=>$cell_collection[$i]['C'],
                        'Maint'=>$cell_collection[$i]['E'],
                        'UnschMaint'=>$cell_collection[$i]['F'],
                        'AOG'=>$cell_collection[$i]['G'],
                        'Accident'=>$cell_collection[$i]['H'],
                        'ActServ'=>$cell_collection[$i]['J'],
                        'Plan'=>$cell_collection[$i]['K'],
                        'Availability'=>$cell_collection[$i]['M'],
                        'Availability_'=>$cell_collection[$i]['N'],
                        'Stby'=>$cell_collection[$i]['O'],
                        'Reg'=>$cell_collection[$i]['P'],
                        'Remarks'=>$cell_collection[$i]['Q']

                    );
                    // echo $loops;
                }
            }

            // Insert Into DB
            $tbl='graph_n_detail';
            $query=$this->M_aircraft_avaibility->insert_graph_n_bacth($dataInsert,$tbl);
        
        // When Template Missed
        }else{
            $print=array('info'=> "warning",'msg'=> "Template Not Valid . Please Insert Tempalte Correctly");
            
        }

       

        
        if($query){
          $print=array('info'=> "success",'msg'=> "Upload Data has Succeses Insert into Databases");      
        }else{
          $print=array('info'=> "error",'msg'=> "Upload Data has Failed Insert into Databases");  
        }

        echo json_encode($print);
    }


    public function view_detail_panel()
    {
        $tipe = $this->input->post("tipe");
        $tahun = date('Y');
        $grafik = $this->M_aircraft_avaibility->get_detail_panel($tipe, $tahun);
        $data['data'] = array();
        $data['datat'] = array();

        foreach ($grafik as $gv_key => $gv_value) {
            $value = str_replace(',', '.', is_null($gv_value->total) ? 0 : $gv_value->total);
            $value = array(floatval(number_format($value, 2)));
            $valuet = $gv_value->bulan;
            array_push($data['data'], $value);
            array_push($data['datat'], $valuet);
        }
        $json[] = $data;

        $target = $this->M_aircraft_avaibility->get_panel1($tipe);
        $target = str_replace(',', '.', is_null($target->TARGET) ? 0 : $target->TARGET);
        $target = array(floatval(number_format($target, 2)));
        echo json_encode(
            array(
                'grafik' => $json,
                'target' => $target,
            )
        );
    }

    public function view_detail_panel_grafik()
    {
        $tipe = $this->input->post("tipe");
        $bulan = $this->input->post("bulan");
        $grafik = $this->M_aircraft_avaibility->get_detail_panel_grafik($tipe, $bulan);

        $data['data'] = array();
        $data['datap'] = array();
        $data['datat'] = array();

        $minimal = 100;
        foreach ($grafik as $gv_key => $gv_value) {
            if ($minimal > floatval($gv_value->ActServ)) {
                $minimal = floatval($gv_value->ActServ);
            }
            $actsrv = str_replace(',', '.', is_null($gv_value->ActServ) ? 0 : $gv_value->ActServ);
            $actsrv = array(floatval(number_format($actsrv, 2)));
            $plan = str_replace(',', '.', is_null($gv_value->plan) ? 0 : $gv_value->plan);
            $plan = array(floatval(number_format($plan, 2)));
            $valuet = date('d-m-Y', strtotime($gv_value->DATE));
            array_push($data['data'], $actsrv);
            array_push($data['datap'], $plan);
            array_push($data['datat'], $valuet);
        }
        $json[] = $data;

        // $target = str_replace(',', '.', is_null($target->target) ? 0 : $target->target);
        // $target = array(floatval(number_format($target,2)));
        echo json_encode(
            array(
                'grafik' => $json,
                'target' => '90',
                'minimal' => $minimal,
            )
        );
    }

    public function datatable()
    {
        $columns = array(
            0 => "Date",
            1 => "ActServ",
            2 => "Plan",
            3 => "Availability",
            4 => "Reg",
            5 => "Remarks",
        );

        $draw = $_REQUEST['draw'];
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']["value"];
        $order_column = $columns[$_REQUEST['order'][0]['column']];
        $order_dir = $_REQUEST['order'][0]['dir'];
        $data = array(
            "draw" => $draw,
            "length" => $length,
            "start" => $start,
            "search" => $search,
            "order_column" => $order_column,
            "order_dir" => $order_dir,
            "tipe" => $this->input->post("tipe"),
            "date" => $this->input->post("tanggal"),
        );

        $send = $this->M_aircraft_avaibility->show_datatable($data);
        echo $send;
    }

    public function datatable_top_trouble(){
      $type = $this->input->post('tipe');
      $date_st = $this->input->post('date_start');
      $date_ed = $this->input->post('date_end');
      $date_start = date('Y-m-d', strtotime($date_st));
      $date_end = date('Y-m-d', strtotime($date_ed));

      if($type == "Classic"){
        $type = "B737-300";
      }

      $datatabel = $this->M_aircraft_avaibility->getTop3Remarks($type,$date_start,$date_end);
      // $no = 1;
      // $response = array();
      // $post = array();
      // foreach ($datatabel as $key => $v) {
      //   $post[] = array(
      //     "NO" => $no,
      //     "CATEGORY" => $v['kejadian'],
      //     "ACCIDENT" => $v['total']
      //   );
      //   $no++;
      // }
      // $response['data'] = $post;

      echo json_encode($datatabel);
    }

}
