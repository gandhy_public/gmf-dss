<?php

  class MY_Controller extends CI_Controller
  {
      public function __construct()
      {
        parent::__construct();
        header('X-Content-Type-Options: nosniff');
        header('X-XSS-Protection:1;mode=block');
        header_remove('x-powered-by');
        header_remove('Server');
        // header_remove('Set-Cookie');
        $this->load->database();
        $this->load->model(array("M_menu"));
        $this->load->helper(array("url"));
        if($this->session->has_userdata("user_id") == false){
          redirect('index.php/login', 'refresh');
        }
        else {
          $id = $this->session->user_id;
          $currentLink = uri_string();
          $this->M_menu->getPermission($id, $currentLink);
          if($this->M_menu->checkMenu($currentLink) == true && $this->M_menu->getPermission($id, $currentLink) == false){
            show_404();
          }
        }
      }
  }
