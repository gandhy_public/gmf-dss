<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_sla_component_avability extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    $this->SQL_server = $this->load->database('dev_gmf', true);
  }

  public function aircraft_ess($year = '2018', $type = 'CITILINK')
  {
    // code...
    $this->SQL_server->select('DISTINCT(actype)');
    $this->SQL_server->from('scv_trans trans');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or part.part_number = trans.material", 'left');
    $this->SQL_server->join('scv_mtype  mtype', "mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or mtype.part_number = trans.material", 'left');
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.func_location', 'left');
    $this->SQL_server->join('m_actype', 'm_acreg.actype_id=m_actype.actype_id', 'left');
    if ($type == 'GA' || $type == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $type);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where_in('part.ess', array(1, 2, 3));
    $this->SQL_server->where_not_in('part.skema', array('POOL STA', 'POOL TURKISH', 'POOL SRT'));
    $this->SQL_server->where('YEAR(CONVERT(date, trans.posting_date, 104))=', $year);
    return $this->SQL_server->get()->result_array();
  }

  public function count_ess($type, $year)
  {
    // code...
    $this->SQL_server->select('sum(CAST(trans.quantity_value AS int)) as jml');
    $this->SQL_server->from('scv_trans trans');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or part.part_number = trans.material");
    $this->SQL_server->join('scv_mtype  mtype', "mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or mtype.part_number = trans.material");
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.func_location');
    if ($type == 'GA' || $type == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $type);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where_in('part.ess', array(1, 2, 3));
    $this->SQL_server->where_not_in('part.skema', array('POOL STA', 'POOL TURKISH', 'POOL SRT'));
    $this->SQL_server->where('YEAR(CONVERT(date, trans.posting_date, 104))=', $year);
    return $this->SQL_server->get()->row_array();
  }

  public function count_robbing($type, $year)
  {
    // code...
    $this->SQL_server->select('*');
    $this->SQL_server->from('robbing_trans trans');
    $this->SQL_server->join('m_acreg', 'trans.receive_func_loc=m_acreg.acreg', 'left');
    $this->SQL_server->join('scv_mtype  mtype', 'mtype.part_number=trans.material', 'left');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' )");
    if ($type == 'GA' || $type == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $type);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where("mtype.mtype != 'EXP'");
    $this->SQL_server->where_not_in('part.skema', array('TMB', 'EXPENDABLE', ''));
    $this->SQL_server->where('part.skema is NOT NULL', null, false);
    $this->SQL_server->where("trans.description NOT Like '%x-change%' AND trans.description NOT Like '%cross change%'
    AND trans.description NOT Like '%swap%' AND trans.description NOT Like '%cancel%'
    AND trans.description NOT Like '%xchange%' AND trans.description NOT Like '%crosschange%'");
    $this->SQL_server->where('YEAR(trans.created_on)', $year);
    return $this->SQL_server->get()->num_rows();
  }


  public function get_type_pesawat($data)
  {
    // code...
    if ($data['maskapai'] == 'GA' || $data['maskapai'] == 'CITILINK') {
      // code...
      $query = $this->SQL_server->query("select DISTINCT(m_actype.actype),m_actype.actype_id from m_actype
      left join m_acreg on m_acreg.actype_id=m_actype.actype_id
      WHERE m_acreg.own='" . $data['maskapai'] . "'");
    } else {
      $query = $this->SQL_server->query("select DISTINCT(m_actype.actype),m_actype.actype_id from m_actype
      left join m_acreg on m_acreg.actype_id=m_actype.actype_id
      WHERE m_acreg.own NOT IN ('GA','CITILINK')");
    }
    return $query->result_array();
  }

  public function get_all()
  {
    $data = $this->SQL_server->select("*")->from("view_scv_new");
    return $data->get()->result_array();
  }

  public function get_all_robbing_graph()
  {
    $year = date("Y");
    $sql = "SELECT
              CASE
                WHEN a.own <> 'GA' AND a.own <> 'CITILINK' THEN 'NON_GA'
                ELSE a.own
              END AS new_own,
              CONVERT(DATE, a.created_on, 121) as new_created_on,
              *
            FROM
            	view_robbing a
            LEFT JOIN scv_mtype b ON a.part_number = b.part_number
            OR a.material = b.part_number
            LEFT JOIN scv_part_number c ON a.part_number = c.part_number
            OR a.material = c.part_number
            WHERE
            	b.mtype <> 'EXP'
            AND c.skema NOT IN ('TMB', 'EXPENDABLE')
            AND c.skema IS NOT NULL
            AND a.description NOT LIKE '%x-change%'
            AND a.description NOT LIKE '%cross change%'
            AND a.description NOT LIKE '%swap%'
            AND a.description NOT LIKE '%cancel%'
            AND a.description NOT LIKE '%xchange%'
            AND a.description NOT LIKE '%crosschange%'
            AND YEAR(CONVERT(DATE, a.created_on, 121)) = '{$year}'";
    $data = $this->SQL_server->query($sql);
    return $data->result_array();
  }

  public function get_robbing_where($customer = null, $type = null, $year = null)
  {
    $sql_plus = " ";
    if($customer != null){
      if($customer == "GA" || $customer == "CITILINK"){
        $sql_plus .= " AND a.own = '{$customer}' ";
      } else {
        $sql_plus .= " AND a.own NOT IN ('CITILINK', 'GA') ";
      }
    }

    if($type != null){
      $sql_plus .= " AND a.actype = '{$type}' ";
    }

    if($year = null){
      $sql_plus .= " AND YEAR(CONVERT(DATE, a.created_on, 121)) = '{$year}' ";
    }
    $sql = "SELECT
            	*
            FROM
            	view_robbing a
            LEFT JOIN scv_mtype b ON a.part_number = b.part_number
            OR a.material = b.part_number
            LEFT JOIN scv_part_number c ON a.part_number = c.part_number
            OR a.material = c.part_number
            WHERE
            	b.mtype <> 'EXP'
            AND c.skema NOT IN ('TMB', 'EXPENDABLE')
            AND c.skema IS NOT NULL
            AND a.description NOT LIKE '%x-change%'
            AND a.description NOT LIKE '%cross change%'
            AND a.description NOT LIKE '%swap%'
            AND a.description NOT LIKE '%cancel%'
            AND a.description NOT LIKE '%xchange%'
            AND a.description NOT LIKE '%crosschange%' {$sql_plus} ";
    $data = $this->SQL_server->query($sql);
    return $data->result_array();
  }

  public function get_all_trans_to_graph($year)
  {
    $sql = "SELECT
            	CONVERT(DATE, view_scv_trans.posting_date, 121) as posting_date,
              CASE
          			WHEN m_acreg.own <> 'GA' AND m_acreg.own <> 'CITILINK' THEN 'NON_GA'
          			ELSE m_acreg.own
          		END as own,
            	m_actype.actype,
            	ABS(SUM(CAST(view_scv_trans.quantity_value AS INT))) as quantity,
            	COUNT_BIG(*) as total_rows
            FROM
            	[dbo].[view_scv_trans]
            	JOIN dbo.scv_part_number ON
            		view_scv_trans.part_number = scv_part_number.part_number
            		OR
            		view_scv_trans.material = scv_part_number.part_number
            	JOIN dbo.m_acreg ON
            		view_scv_trans.func_location = m_acreg.acreg
            	JOIN dbo.m_actype ON
            		m_acreg.actype_id = m_actype.actype_id
            	JOIN dbo.scv_mtype ON
            		view_scv_trans.part_number = scv_mtype.part_number
            		OR
            		view_scv_trans.material = scv_mtype.part_number
            WHERE
            	(view_scv_trans.posting_date is not null or view_scv_trans.posting_date not like '%0000')
            	AND YEAR(CONVERT(DATE, view_scv_trans.posting_date, 121)) = '{$year}'
            	AND scv_part_number.ess in ('1','2','3')
            	AND scv_part_number.skema NOT IN ('POOL STA', 'POOL TURKISH', 'POOL SRT')
            GROUP BY
            	m_acreg.own,
            	m_actype.actype,
            	view_scv_trans.posting_date
            ORDER BY view_scv_trans.posting_date ASC";
      $data = $this->SQL_server->query($sql);
      return $data->result_array();
  }

  public function get_all_target()
  {
    $this->SQL_server->select("*");
    $this->SQL_server->from("tbl_target_sla");
    return $this->SQL_server->get()->result_array();
  }

  public function ess_by_aicrafttype($aircraft, $type, $year)
  {
    // code...
    $this->SQL_server->select('sum(CAST(trans.quantity_value AS int)) as jml');
    $this->SQL_server->from('scv_trans trans');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or part.part_number = trans.material");
    $this->SQL_server->join('scv_mtype  mtype', "mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or mtype.part_number = trans.material");
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.func_location');
    $this->SQL_server->join('m_actype', 'm_actype.actype_id=m_acreg.actype_id');
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where('m_actype.actype', $type);
    $this->SQL_server->where_in('part.ess', array(1, 2, 3));
    $this->SQL_server->where_not_in('part.skema', array('POOL STA', 'POOL TURKISH', 'POOL SRT'));
    $this->SQL_server->where('YEAR(CONVERT(date, trans.posting_date, 104))=', $year);
    return $this->SQL_server->get()->row_array();
  }

  public function robbing_by_aicrafttype($aircraft, $type, $year)
  {
    // code...
    $this->SQL_server->select('*');
    $this->SQL_server->from('robbing_trans trans');
    $this->SQL_server->join('m_acreg', 'trans.receive_func_loc=m_acreg.acreg', 'left');
    $this->SQL_server->join('m_actype', 'm_actype.actype_id=m_acreg.actype_id', 'left');
    $this->SQL_server->join('scv_mtype', "scv_mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or scv_mtype.part_number = trans.material", 'left');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' )");
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where('m_actype.actype', $type);
    $this->SQL_server->where("scv_mtype.mtype != 'EXP'");
    $this->SQL_server->where_not_in('part.skema', array('TMB', 'EXPENDABLE'));
    $this->SQL_server->where('part.skema is NOT NULL', null, false);
    $this->SQL_server->where("trans.description NOT Like '%x-change%' AND trans.description NOT Like '%cross change%'
    AND trans.description NOT Like '%swap%' AND trans.description NOT Like '%cancel%'
    AND trans.description NOT Like '%xchange%' AND trans.description NOT Like '%crosschange%'");
    $this->SQL_server->where('YEAR(trans.created_on)', $year);
    return $this->SQL_server->get()->num_rows();
  }

  public function skema_by_aicrafttype($aircraft, $type, $year)
  {
    // code...
    $this->SQL_server->select('DISTINCT(part.skema) as skema');
    $this->SQL_server->from('scv_trans trans');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or part.part_number = trans.material", 'left');
    $this->SQL_server->join('scv_mtype  mtype', "mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or mtype.part_number = trans.material", 'left');
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.func_location', 'left');
    $this->SQL_server->join('m_actype', 'm_actype.actype_id=m_acreg.actype_id', 'left');
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where_in('part.ess', array(1, 2, 3));
    $this->SQL_server->where_not_in('part.skema', array('POOL STA', 'POOL TURKISH', 'POOL SRT'));
    $this->SQL_server->where('m_actype.actype', $type);
    $this->SQL_server->where('YEAR(CONVERT(date, trans.posting_date, 104))=', $year);
    return $this->SQL_server->get()->result_array();
  }

  public function count_ess_by_skema($aircraft, $type, $year, $skema)
  {
    // code...
    $this->SQL_server->select('sum(CAST(trans.quantity_value AS int)) as jml');
    $this->SQL_server->from('scv_trans trans');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or part.part_number = trans.material");
    $this->SQL_server->join('scv_mtype  mtype', "mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or mtype.part_number = trans.material");
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.func_location');
    $this->SQL_server->join('m_actype', 'm_acreg.actype_id=m_actype.actype_id');
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where_in('part.ess', array(1, 2, 3));
    $this->SQL_server->where_not_in('part.skema', array('POOL STA', 'POOL TURKISH', 'POOL SRT'));
    $this->SQL_server->where('m_actype.actype', $type);
    $this->SQL_server->where('part.skema', $skema);
    $this->SQL_server->where('YEAR(CONVERT(date, trans.posting_date, 104))=', $year);
    return $this->SQL_server->get()->row_array();

  }

  public function count_robbing_by_skema($aircraft, $type, $year, $skema)
  {
    // code...
    $this->SQL_server->select('*');
    $this->SQL_server->from('robbing_trans trans');
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.receive_func_loc', 'left');
    $this->SQL_server->join('m_actype', 'm_acreg.actype_id=m_actype.actype_id', 'left');
    $this->SQL_server->join('scv_mtype', "scv_mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or scv_mtype.part_number = trans.material", 'left');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' )");
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where("scv_mtype.mtype != 'EXP'");
    $this->SQL_server->where_not_in('part.skema', array('TMB'));
    $this->SQL_server->where('part.skema is NOT NULL', null, false);
    $this->SQL_server->where("trans.description NOT Like '%x-change%' AND trans.description NOT Like '%cross change%'
    AND trans.description NOT Like '%swap%' AND trans.description NOT Like '%cancel%'
    AND trans.description NOT Like '%xchange%' AND trans.description NOT Like '%crosschange%'");
    $this->SQL_server->where('m_actype.actype', $type);
    $this->SQL_server->where('part.skema', $skema);
    $this->SQL_server->where('YEAR(trans.created_on)', $year);
    return $this->SQL_server->get()->num_rows();
  }

  public function get_ess_by_month_and_aircraft($bln, $year, $aircraft)
  {
    // code...
    $this->SQL_server->select('sum(CAST(trans.quantity_value AS int)) as jml');
    $this->SQL_server->from('scv_trans trans');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or part.part_number = trans.material");
    $this->SQL_server->join('scv_mtype  mtype', "mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or mtype.part_number = trans.material");
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.func_location');
    $this->SQL_server->join('m_actype', 'm_acreg.actype_id=m_actype.actype_id');
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where_in('part.ess', array(1, 2, 3));
    $this->SQL_server->where_not_in('part.skema', array('POOL STA', 'POOL TURKISH', 'POOL SRT'));
    $this->SQL_server->where('YEAR(CONVERT(date, trans.posting_date, 104))=', $year);
    $this->SQL_server->where('MONTH (CONVERT(date, trans.posting_date, 104))=', $bln);
    return $this->SQL_server->get()->row_array();
  }

  public function get_robbing_by_month_and_aircraft($bln, $year, $aircraft)
  {
    // code...
    $this->SQL_server->select('*');
    $this->SQL_server->from('robbing_trans trans');
    $this->SQL_server->join('m_acreg', 'trans.receive_func_loc=m_acreg.acreg', 'left');
    $this->SQL_server->join('m_actype', 'm_actype.actype_id=m_acreg.actype_id', 'left');
    $this->SQL_server->join('scv_mtype', "scv_mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or scv_mtype.part_number = trans.material", 'left');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' )");
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where("scv_mtype.mtype != 'EXP'");
    $this->SQL_server->where_not_in('part.skema', array('TMB', 'EXPENDABLE'));
    $this->SQL_server->where('part.skema is NOT NULL', null, false);
    $this->SQL_server->where("trans.description NOT Like '%x-change%' AND trans.description NOT Like '%cross change%'
    AND trans.description NOT Like '%swap%' AND trans.description NOT Like '%cancel%'
    AND trans.description NOT Like '%xchange%' AND trans.description NOT Like '%crosschange%'");
    $this->SQL_server->where('YEAR(CONVERT(DATE, created_on, 120))=', $year);
    $this->SQL_server->where('MONTH(CONVERT(DATE, created_on, 120))=', $bln);
    return $this->SQL_server->get()->num_rows();
  }

  public function get_aircraft_type($aircraft, $year, $bln)
  {
    // code...
    $this->SQL_server->select('m_actype.actype');
    $this->SQL_server->from('scv_trans trans');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or part.part_number = trans.material", 'left');
    $this->SQL_server->join('scv_mtype  mtype', "mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or mtype.part_number = trans.material", 'left');
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.func_location', 'left');
    $this->SQL_server->join('m_actype', 'm_actype.actype_id=m_acreg.actype_id', 'left');
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where_in('part.ess', array(1, 2, 3));
    $this->SQL_server->where_not_in('part.skema', array('POOL STA', 'POOL TURKISH', 'POOL SRT'));
    $this->SQL_server->where('YEAR(CONVERT(date, trans.posting_date, 104))=', $year);
    $this->SQL_server->where('MONTH (CONVERT(date, trans.posting_date, 104))=', $bln);
    $this->SQL_server->group_by('actype');
    return $this->SQL_server->get()->result_array();
  }

  // public function skema_in_reg_by_aircraft_and_date($aircraft, $type, $reg, $year, $bln)
  public function skema_in_reg_by_aircraft_and_date($aircraft, $type, $year, $bln)
  {
    // code...
    $this->SQL_server->select('DISTINCT(part.skema) as skema');
    $this->SQL_server->from('scv_trans trans');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or part.part_number = trans.material", 'left');
    $this->SQL_server->join('scv_mtype  mtype', "mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or mtype.part_number = trans.material", 'left');
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.func_location', 'left');
    $this->SQL_server->join('m_actype', 'm_actype.actype_id=m_acreg.actype_id', 'left');
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where_in('part.ess', array(1, 2, 3));
    $this->SQL_server->where_not_in('part.skema', array('POOL STA', 'POOL TURKISH', 'POOL SRT'));
    $this->SQL_server->where('m_actype.actype', $type);
    // $this->SQL_server->where('trans.func_location', $reg);
    $this->SQL_server->where('YEAR(CONVERT(date, trans.posting_date, 104))=', $year);
    $this->SQL_server->where('MONTH (CONVERT(date, trans.posting_date, 104))=', $bln);
    return $this->SQL_server->get()->result_array();
  }

  // public function get_ess_by_month_and_aircraft_reg($aircraft, $type, $reg, $year, $bln, $skema)
  public function get_ess_by_month_and_aircraft_reg($aircraft, $type, $year, $bln, $skema)
  {
    // code...
    $this->SQL_server->select('sum(CAST(trans.quantity_value AS int)) as jml');
    $this->SQL_server->from('scv_trans trans');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or part.part_number = trans.material");
    $this->SQL_server->join('scv_mtype  mtype', "mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or mtype.part_number = trans.material");
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.func_location');
    $this->SQL_server->join('m_actype', 'm_acreg.actype_id=m_actype.actype_id');
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where_in('part.ess', array(1, 2, 3));
    $this->SQL_server->where_not_in('part.skema', array('POOL STA', 'POOL TURKISH', 'POOL SRT'));
    $this->SQL_server->where('part.skema', $skema);
    $this->SQL_server->where('m_actype.actype', $type);
    // $this->SQL_server->where('trans.func_location', $reg);
    $this->SQL_server->where('YEAR(CONVERT(date, trans.posting_date, 104))=', $year);
    $this->SQL_server->where('MONTH (CONVERT(date, trans.posting_date, 104))=', $bln);
    return $this->SQL_server->get()->row_array();
  }

  public function get_rob_by_month_and_aircraft_reg($aircraft, $type, $year, $bln, $key)
  {
    // code...
    $this->SQL_server->select('*');
    $this->SQL_server->from('robbing_trans trans');
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.receive_func_loc', 'left');
    $this->SQL_server->join('m_actype', 'm_acreg.actype_id=m_actype.actype_id', 'left');
    $this->SQL_server->join('scv_mtype', "scv_mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or scv_mtype.part_number = trans.material", 'left');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' )");
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where("scv_mtype.mtype != 'EXP'");
    $this->SQL_server->where_not_in('part.skema', array('TMB', 'EXPENDABLE'));
    $this->SQL_server->where('part.skema is NOT NULL', null, false);
    $this->SQL_server->where("trans.description NOT Like '%x-change%' AND trans.description NOT Like '%cross change%'
    AND trans.description NOT Like '%swap%' AND trans.description NOT Like '%cancel%'
    AND trans.description NOT Like '%xchange%' AND trans.description NOT Like '%crosschange%'");
    $this->SQL_server->where('m_actype.actype', $type);
    $this->SQL_server->where('part.skema', $key);
    // $this->SQL_server->where('trans.receive_func_loc', $reg);
    $this->SQL_server->where('YEAR(trans.created_on)', $year);
    $this->SQL_server->where('MONTH(trans.created_on)', $bln);
    return $this->SQL_server->get()->num_rows();
  }

  public function get_export_mtype_xls()
  {
    // code...
    $query = $this->SQL_server->select('*')
      ->from('scv_mtype')
      ->get();
    return $query->result();

  }

  public function get_export_datapn_xls()
  {
    // code...
    $query = $this->SQL_server->select('*')
      ->from('scv_part_number')
      ->get();
    return $query->result();
  }

  public function getNameMonth($start, $end)
  {
    $monthFrom = $start;
    $monthTo = $end;

    $start = (new DateTime($monthFrom))->modify('first day of this month');
    $end = (new DateTime($monthTo))->modify('first day of next month');
    $interval = DateInterval::createFromDateString('1 month');
    $period = new DatePeriod($start, $interval, $end);
    $monthNames = array();
    foreach ($period as $dt) {
      $monthName = $dt->format('Y-m');
      array_push($monthNames, $monthName);
    }
    return $monthNames;

  }


  public function save_pn($value)
  {
    // code...
    $this->SQL_server->trans_begin();
    $this->SQL_server->truncate('scv_part_number');
    $this->SQL_server->insert_batch('scv_part_number', $value);
    if ($this->SQL_server->trans_status() === false) {
      $this->db->trans_rollback();
      return false;
    } else {
      $this->SQL_server->trans_commit();
      return true;
    }
  }

  public function save_mtype($value)
  {
    // code...
    $this->SQL_server->trans_begin();
    $this->SQL_server->truncate('scv_mtype');
    $this->SQL_server->insert_batch('scv_mtype', $value);

    if ($this->SQL_server->trans_status() === false) {
      $this->db->trans_rollback();
      return false;
    } else {
      $this->SQL_server->trans_commit();
      return true;
    }
  }

  // get_datatables
  //===========DATATABLE============\\
  var $table = 'scv_mtype';
  var $column_order = array('part_number', 'mtype');
  var $column_search = array('part_number', 'mtype');
  var $order = array('part_number' => 'ASC');

  private function query()
  {

    $this->SQL_server->select("part_number,mtype");
    $this->SQL_server->from('scv_mtype');
    $i = 0;

    foreach ($this->column_search as $item) {
      if (!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if ($i === 0) // first loop
        {
          $this->SQL_server->group_start();
          $this->SQL_server->like($item, $_POST['search']['value']);
        } else {
          $this->SQL_server->or_like($item, $_POST['search']['value']);
        }

        if (count($this->column_search) - 1 == $i) //last loop
        $this->SQL_server->group_end(); //close bracket
      }
      $i++;
    }

    if (isset($_POST['order'])) {
      $this->SQL_server->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if (isset($this->order)) {
      $order = $this->order;
      $this->SQL_server->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables()
  {

    $this->query();
    if ($_POST['length'] != -1)
      $this->SQL_server->limit($_POST['length'], $_POST['start']);
    $query = $this->SQL_server->get();
    return $query->result();
  }

  function count_all()
  {

    $this->query();
    $query = $this->SQL_server->get();
    return $query->num_rows();
  }


  function count_filtered()
  {

    $this->query();
    $query = $this->SQL_server->get();
    return $query->num_rows();
  }


  //===========DATATABLE PN============\\
  var $table2 = 'scv_part_number';
  var $column_order2 = array('pnreg', 'ess', 'part_number', 'alternate', 'description', 'cap', 'aircraft', 'skema', 'tot_float_spare', 'remarks');
  var $column_search2 = array('pnreg', 'ess', 'part_number', 'alternate', 'description', 'cap', 'aircraft', 'skema', 'tot_float_spare', 'remarks');
  var $order2 = array('part_number' => 'ASC');

  private function querypn()
  {

    $this->SQL_server->select("pnreg,ess,part_number,alternate,description,cap,aircraft,skema,tot_float_spare,remarks");
    $this->SQL_server->from('scv_part_number');
    $i = 0;

    foreach ($this->column_search2 as $item) {
      if (!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if ($i === 0) // first loop
        {
          $this->SQL_server->group_start();
          $this->SQL_server->like($item, $_POST['search']['value']);
        } else {
          $this->SQL_server->or_like($item, $_POST['search']['value']);
        }

        if (count($this->column_search2) - 1 == $i) //last loop
        $this->SQL_server->group_end(); //close bracket
      }
      $i++;
    }

    if (isset($_POST['order'])) {
      $this->SQL_server->order_by($this->column_order2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if (isset($this->order)) {
      $order = $this->order;
      $this->SQL_server->order_by(key($order2), $order2[key($order)]);
    }
  }

  function get_datatables_pn()
  {

    $this->querypn();
    if ($_POST['length'] != -1)
      $this->SQL_server->limit($_POST['length'], $_POST['start']);
    $query = $this->SQL_server->get();
    return $query->result();
  }

  function count_all_pn()
  {

    $this->querypn();
    $query = $this->SQL_server->get();
    return $query->num_rows();
  }


  function count_filtered_pn()
  {

    $this->querypn();
    $query = $this->SQL_server->get();
    return $query->num_rows();
  }

  public function get_aircraft_where_own()
  {
    $sql = " SELECT
        b.actype,
        a.own
      FROM
        m_acreg a
      LEFT JOIN m_actype b ON a.actype_id = b.actype_id
      WHERE
        (b.actype IS NOT NULL or b.actype <> '' or b.actype <> '0')
      AND (a.own IS NOT NULL or a.own <> '' or a.own <> '0')
      GROUP BY
        b.actype,
        a.own";
    $data = $this->SQL_server->query($sql);
    return $data->result_array();
  }

  public function get_aircraft()
  {
    // code...
    $this->SQL_server->select('*');
    $this->SQL_server->from('m_actype');
    return $this->SQL_server->get()->result_array();
  }

  public function get_avability_target($aircraft, $year_target)
  {
    // code...
    $this->SQL_server->select('*');
    $this->SQL_server->from('tbl_target_sla');
    $this->SQL_server->where('type_aircraft', $aircraft);
    $this->SQL_server->where('year_aircraft', $year_target);
    return $this->SQL_server->get()->num_rows();
  }

  public function store_target_sla($aircraft, $val_target, $year_target)
  {
    // code...
    $data = array(
      'type_aircraft' => $aircraft,
      'target_aircraft' => $val_target,
      'year_aircraft' => $year_target
    );

    $query = $this->SQL_server->insert('tbl_target_sla', $data);
    return $query;
  }
  public function value_target_aircraft($year)
  {
    // code...
    $this->SQL_server->select('type_aircraft,target_aircraft');
    $this->SQL_server->from('tbl_target_sla');
    $this->SQL_server->where('year_aircraft', $year);
    return $this->SQL_server->get()->result_array();
  }

  public function load_datatable_target($data)
  {
    // code...
    $this->SQL_server->select('*');
    $this->SQL_server->from('tbl_target_sla');
    if ($data['search'] != '') {
      // code...
      $this->SQL_server->where("(type_aircraft like '%" . $data['search'] . "%' OR
      target_aircraft like '%" . $data['search'] . "%' OR
      year_aircraft LIKE  '%" . $data['search'] . "%')");
    }
    $this->SQL_server->where('year_aircraft', $data['year']);
    return $this->SQL_server->get()->result_array();
  }
  public function count_load_datatable_target($data)
  {
    // code...
    $this->SQL_server->select('*');
    $this->SQL_server->from('tbl_target_sla');
    if ($data['search'] != '') {
      // code...
      $this->SQL_server->where("(type_aircraft like '%" . $data['search'] . "%' OR
      target_aircraft like '%" . $data['search'] . "%' OR
      year_aircraft LIKE  '%" . $data['search'] . "%')");
    }
    $this->SQL_server->where('year_aircraft', $data['year']);
    return $this->SQL_server->get()->num_rows();
  }

  public function get_edit_data($id)
  {
    // code...
    $this->SQL_server->select('*');
    $this->SQL_server->from('tbl_target_sla');
    $this->SQL_server->where('id_target_sla', $id);
    return $this->SQL_server->get()->result();
  }

  public function update_value_target($id, $target)
  {
    // code...
    $data = array(
      'target_aircraft' => $target
    );

    $this->SQL_server->where('id_target_sla', $id);
    $query = $this->SQL_server->update('tbl_target_sla', $data);
    return $query;
  }

  public function delete_value_target($id)
  {
    // code...
    $this->SQL_server->where('id_target_sla', $id);
    $query = $this->SQL_server->delete('tbl_target_sla');
    return $query;
  }


  // ==================================================
  //
  //===========DATATABLE PN============\\
  var $table3 = 'scv_part_number';
  var $column_order3 = array('created_on','material','trans.description','donor_func_loc','receive_func_loc','created_on');
  var $column_search3 = array('created_on','material','trans.description','donor_func_loc','receive_func_loc','created_on');
  var $order3 = array('created_on' => 'ASC');

  private function queryrob_month($aircraft, $type, $bln, $year, $key)
  {

    $this->SQL_server->select('material,trans.description,donor_func_loc,receive_func_loc,created_on');
    $this->SQL_server->from('robbing_trans trans');
    $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.receive_func_loc', 'left');
    $this->SQL_server->join('m_actype', 'm_acreg.actype_id=m_actype.actype_id', 'left');
    $this->SQL_server->join('scv_mtype', "scv_mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or scv_mtype.part_number = trans.material", 'left');
    $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' )");
    if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
      $this->SQL_server->where('m_acreg.own', $aircraft);
    } else {
      $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
    }
    $this->SQL_server->where("scv_mtype.mtype != 'EXP'");
    $this->SQL_server->where_not_in('part.skema', array('TMB', 'EXPENDABLE'));
    $this->SQL_server->where('part.skema is NOT NULL', null, false);
    $this->SQL_server->where("trans.description NOT Like '%x-change%' AND trans.description NOT Like '%cross change%'
    AND trans.description NOT Like '%swap%' AND trans.description NOT Like '%cancel%'
    AND trans.description NOT Like '%xchange%' AND trans.description NOT Like '%crosschange%'");
    $this->SQL_server->where('m_actype.actype', $type);
    if($key=="PBTH"){
      $this->SQL_server->like('part.skema', $key);
    }else{
      $this->SQL_server->where('part.skema', $key);
    }
    $this->SQL_server->where('YEAR(trans.created_on)', $year);
    if($bln!=""){
      $this->SQL_server->where('MONTH(trans.created_on)', $bln);
    }
    $i = 0;

    foreach ($this->column_search3 as $item) {
      if (!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if ($i === 0) // first loop
        {
          $this->SQL_server->group_start();
          $this->SQL_server->like($item, $_POST['search']['value']);
        } else {
          $this->SQL_server->or_like($item, $_POST['search']['value']);
        }

        if (count($this->column_search3) - 1 == $i) //last loop
        $this->SQL_server->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order']))
    {
      $this->SQL_server->order_by($this->column_order2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->SQL_server->order_by(key($order3), $order3[key($order)]);
    }
  }

  function get_datatables_rob_month($aircraft, $type, $bln, $year, $key)
  {

    $this->queryrob_month($aircraft, $type, $bln, $year, $key);
    if (10 != -1)
      $this->SQL_server->limit($_REQUEST['length'], $_REQUEST['start']);
    $query = $this->SQL_server->get();
    return $query->result_array();
  }

  function count_all_rob_month($aircraft, $type, $bln, $year, $key)
  {

    $this->queryrob_month($aircraft, $type, $bln, $year, $key);
    $query = $this->SQL_server->get();
    return $query->num_rows();
  }
  // public function datatables_rob_by_month_and_aircraft_reg($aircraft, $type, $bln, $year, $key)
  // {
  //   // code...
  //   $this->SQL_server->select('*');
  //   $this->SQL_server->from('robbing_trans trans');
  //   $this->SQL_server->join('m_acreg', 'm_acreg.acreg=trans.receive_func_loc', 'left');
  //   $this->SQL_server->join('m_actype', 'm_acreg.actype_id=m_actype.actype_id', 'left');
  //   $this->SQL_server->join('scv_mtype', "scv_mtype.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' ) or scv_mtype.part_number = trans.material", 'left');
  //   $this->SQL_server->join('scv_part_number part', "part.part_number=REPLACE( LEFT ( trans.material, CHARINDEX( ':', trans.material ) ), ':', '' )");
  //   if ($aircraft == 'GA' || $aircraft == 'CITILINK') {
  //     $this->SQL_server->where('m_acreg.own', $aircraft);
  //   } else {
  //     $this->SQL_server->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
  //   }
  //   $this->SQL_server->where("scv_mtype.mtype != 'EXP'");
  //   $this->SQL_server->where_not_in('part.skema', array('TMB', 'EXPENDABLE'));
  //   $this->SQL_server->where('part.skema is NOT NULL', null, false);
  //   $this->SQL_server->where("trans.description NOT Like '%x-change%' AND trans.description NOT Like '%cross change%'
  //   AND trans.description NOT Like '%swap%' AND trans.description NOT Like '%cancel%'
  //   AND trans.description NOT Like '%xchange%' AND trans.description NOT Like '%crosschange%'");
  //   $this->SQL_server->where('m_actype.actype', $type);
  //   $this->SQL_server->where('part.skema', $key);
  //   $this->SQL_server->where('YEAR(trans.created_on)', $year);
  //   $this->SQL_server->where('MONTH(trans.created_on)', $bln);
  //   return $this->SQL_server->get()->result_array();
  // }


}
