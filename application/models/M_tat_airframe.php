<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class M_TAT_AIRFRAME extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('default', true);
        $this->db2 = $this->load->database('dev_gmf', true);
    }

    public function acRegs($input, $operator)
    {
        $this->db2->select('m_acreg.acreg');
        $this->db2->from('m_acreg');
        $this->db2->join('m_actype', 'm_acreg.actype_id = m_actype.actype_id');
        if ($input['air_type'] != '' or $input['air_type'] != null) {
            $this->db2->like('m_actype.actype', $input['air_type']);
        }
        if ($operator != 'NON GA') {
            $this->db2->where('m_acreg.own', $operator);
        } else {
            $this->db2->where_not_in('m_acreg.own', array('GA', 'CITILINK'));
        }
        $this->db2->where('m_actype.fleet_type', $input['fleet']);
        $this->db2->group_by('m_acreg.acreg');
        $acRegs = $this->db2->get()->result_array();

        return $acRegs;
    }


    public function getDetailData($input, $operator)
    {
        $startPeriode = date('Y') . '-01-01';
        $endPeriode = date('Y-m') . '-01';
        $acRegs = array();
        $acRegsMulty = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegs($input, $operator)));
        foreach ($acRegsMulty as $data) {
            array_push($acRegs, $data);
        }



        $start = (new DateTime($startPeriode))->modify('first day of this month');
        $end = (new DateTime($endPeriode))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $performanceGroupRow = array();



        foreach ($period as $now) {
           $this->db2->select('plant_tat, act_tat, aircraft_type, aircraft_reg')
              ->from('tat_airframe')
              ->join("m_acreg", "tat_airframe.aircraft_reg=m_acreg.acreg", "left")
              ->join("m_actype", "m_acreg.actype_id=m_actype.actype_id", "left")
              // ->where_in('aircraft_reg', $acRegs)
              ->where_in('plant', $input['tat_type'])
              ->not_like('act_finish_date', '000000')
              ->where("MONTH( CASE WHEN ISDATE( act_start_date ) = 1 THEN CAST ( act_start_date AS DATE ) END ) =", $now->format('m'))
              ->where("YEAR( CASE WHEN ISDATE( act_start_date ) = 1 THEN CAST ( act_start_date AS DATE ) END ) =", $now->format('Y'));
              if($operator == "GA"){
                $this->db2->like("ID_CUSTOMER", "GIA", "AFTER");
              } elseif ($operator == "CITILINK") {
                $where = "(ID_CUSTOMER LIKE 'CTV%' ESCAPE '!' OR ID_CUSTOMER LIKE 'GL%' ESCAPE '!')";
                $this->db2->where($where, null, false);
              } elseif ($operator == "NON GA") {
                $this->db2->not_like("ID_CUSTOMER", "GIA", "AFTER");
                $this->db2->not_like("ID_CUSTOMER", "CTV", "AFTER");
                $this->db2->not_like("ID_CUSTOMER", "GL", "AFTER");
              }
              $this->db2->where_in("fleet_type", explode(",", $input["fleet"]));
              $dataDetails = $this->db2->get()->result();

            $allPerformance = array();
            foreach ($dataDetails as $dataDetail) {
                $plantTat = (int)$dataDetail->plant_tat + 1;
                $actTat = (int)$dataDetail->act_tat + 1;

                $performance = $plantTat / $actTat;
                if ($performance > 1) {
                    $performance = 1;
                }

                array_push($allPerformance, $performance);
            }

            array_push($performanceGroupRow, $allPerformance);
        }
        $value = array();
        foreach ($performanceGroupRow as $performanceEachRow) {

            if (count($performanceEachRow) == 0) {
                $result = 0;
            } else {
                $resultcounting = number_format(array_sum($performanceEachRow) / count($performanceEachRow), 2) * 100;
                $result = (float)number_format((float)$resultcounting, 2, '.', '');
            }

            array_push($value, $result);
        }

        return $value;
    }

    public function getMonthName()
    {
        $year = date('Y');
        $monthFrom = $year . '-01';
        $monthTo = date('Y-m');
        $con_year = substr($year, 2);

        $start = (new DateTime($monthFrom))->modify('first day of this month');
        $end = (new DateTime($monthTo))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $monthNames = array();
        foreach ($period as $dt) {
            $monthName = $dt->format('M');

            array_push($monthNames, $monthName);
        }
        return $monthNames;
    }

    public function getMonthName_acm()
    {
        $year = date('Y');
        $monthFrom = $year . '-01';
        $monthTo = date('Y-m');
        $con_year = substr($year, 2);

        $start = (new DateTime($monthFrom))->modify('first day of this month');
        $end = (new DateTime($monthTo))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $monthNames = array();
        foreach ($period as $dt) {
            $monthName = $dt->format('M') . '-' . $con_year;

            array_push($monthNames, $monthName);
        }
        return $monthNames;
    }

    function get_chart_accumulation($input, $date, $query, $mon)
    {

        $sql = "SELECT * FROM ( SELECT  DATEPART(YEAR, ta.ACT_START_DATE) as ac_year,
	        DATEPART(MONTH, ta.ACT_START_DATE) as AC_MONTH,
	        Sum(isnull(cast(ta.PLANT_TAT as float),0)) as planttat,
	        Sum(isnull(cast(ta.ACT_TAT as float),0)) as acttat
	        FROM    TAT_AIRFRAME ta
                join dbo.m_acreg ma on ta.AIRCRAFT_REG=ma.acreg
                join dbo.m_actype mt on ma.actype_id=mt.actype_id
	        WHERE   ta.ACT_START_DATE >= '" . $date['start'] . "'
	        AND     ta.ACT_START_DATE <= '" . $date['end'] . "'
	        and 	ta.ACT_FINISH_DATE is not null
          and mt.actype like '%" . $input['air_type'] . "'
          and mt.fleet_type in (" . $input['fleet'] . ")
          and ta.PLANT in (" . $input['tat_type'] . ")
          " . $query . "
	        GROUP BY DATEPART(YEAR, ta.ACT_START_DATE), DATEPART(MONTH, ta.ACT_START_DATE) ) chart
          WHERE AC_MONTH='$mon'";
        $sql = $this->db2->query($sql);
        return $sql->result();
    }

    function get_chart_ga($query, $date, $input)
    {
        $sql = $this->db2->query("SELECT mt.actype as actype
          from dbo.TAT_AIRFRAME ta join dbo.m_acreg ma
          on ta.AIRCRAFT_REG=ma.acreg join dbo.m_actype mt
          on ma.actype_id=mt.actype_id
          where ta.BASIC_START_DATE BETWEEN '" . $date['start'] . "' AND '" . $date['end'] . "'
          and mt.actype like '%" . $input['air_type'] . "'
          and mt.fleet_type in (" . $input['fleet'] . ")
          and ta.PLANT in (" . $input['tat_type'] . ")
          " . $query . "
          group by mt.actype");
        return $sql->result_array();
    }

    public function get_table_ga_det($input, $data, $date, $statusDataTable)
    {
        $operator = $input['param_airc'];
        $this->db2->select('ACT_START_DATE, m_actype.actype, AIRCRAFT_REG, REVISION_DESCRIPTION, PLANT_TAT, ACT_TAT, REVISION AS revision, ');
        $this->db2->from('TAT_AIRFRAME');
        $this->db2->join('m_acreg', 'TAT_AIRFRAME.AIRCRAFT_REG = m_acreg.acreg');
        $this->db2->join('m_actype', ' m_acreg.actype_id = m_actype.actype_id');
        $this->db2->where_in('TAT_AIRFRAME.PLANT', $input['tat_type']);
        if($operator == "GA"){
          $this->db2->like("ID_CUSTOMER", "GIA", "AFTER");
        } elseif ($operator == "CITILINK") {
          $this->db2->like("ID_CUSTOMER", "CTV", "AFTER");
          $this->db2->or_like("ID_CUSTOMER", "GL", "AFTER");
        } elseif ($operator == "NON GA") {
          $this->db2->not_like("ID_CUSTOMER", "GIA", "AFTER");
          $this->db2->not_like("ID_CUSTOMER", "CTV", "AFTER");
          $this->db2->not_like("ID_CUSTOMER", "GL", "AFTER");
        }
        $this->db2->where("MONTH( CASE WHEN ISDATE( TAT_AIRFRAME.ACT_START_DATE ) = 1 THEN CAST ( TAT_AIRFRAME.ACT_START_DATE AS DATE ) END ) >=", date('m', strtotime($date['start'])));
        $this->db2->where("YEAR( CASE WHEN ISDATE( TAT_AIRFRAME.ACT_START_DATE ) = 1 THEN CAST ( TAT_AIRFRAME.ACT_START_DATE AS DATE ) END ) >=", date('Y', strtotime($date['start'])));
        $this->db2->where("MONTH( CASE WHEN ISDATE( TAT_AIRFRAME.ACT_START_DATE ) = 1 THEN CAST ( TAT_AIRFRAME.ACT_START_DATE AS DATE ) END ) <=", date('m', strtotime($date['end'])));
        $this->db2->where("YEAR( CASE WHEN ISDATE( TAT_AIRFRAME.ACT_START_DATE ) = 1 THEN CAST ( TAT_AIRFRAME.ACT_START_DATE AS DATE ) END ) <=", date('Y', strtotime($date['end'])));
        $this->db2->not_like('TAT_AIRFRAME.ACT_FINISH_DATE', '000000');
        $this->db2->where_in('m_actype.fleet_type', explode(",",$input['fleet']));
        $this->db2->order_by('ID_TAT_AIRFR', 'ASC');

        if ($data['search'] != '') {
            $this->db2->where("(ACT_START_DATE like '%" . $data['search'] . "%' OR
                          m_actype.actype like '%" . $data['search'] . "%' OR
                          AIRCRAFT_REG like '%" . $data['search'] . "%' OR
                          REVISION_DESCRIPTION like '%" . $data['search'] . "%' OR
                          PLANT_TAT like '%" . $data['search'] . "%' OR
                          ACT_TAT like '%" . $data['search'] . "%')");
        }

        if ($statusDataTable == 'data') {
            $this->db2->limit($data["length"], $data['start']);
            return $this->db2->get()->result_array();
        } else {
            return $this->db2->get()->num_rows();
        }

    }

    function get_value_ga($type)
    {
        $sql = $this->db2->query("SELECT mt.actype as tipe,ta.AIRCRAFT_REG as register,ta.REVISION_DESCRIPTION as inspect,ta.PLANT_TAT as target,ta.ACT_TAT as actual,DATEDIFF(DAY,ta.ACT_START_DATE, GETDATE()) as selisih from dbo.TAT_AIRFRAME ta join dbo.m_acreg ma on ta.AIRCRAFT_REG=ma.acreg join dbo.m_actype mt on ma.actype_id=mt.actype_id where ACT_FINISH_DATE is null and ACT_START_DATE is not null and ma.own = '" . $type . "'");
        return $sql->result_array();
    }

    function get_value_non()
    {
        $sql = $this->db2->query("SELECT mt.actype as tipe,ta.AIRCRAFT_REG as register,ta.REVISION_DESCRIPTION as inspect,ta.PLANT_TAT as target,ta.ACT_TAT as actual, DATEDIFF(DAY,ta.ACT_START_DATE, GETDATE()) as selisih from dbo.TAT_AIRFRAME ta join dbo.m_acreg ma on ta.AIRCRAFT_REG=ma.acreg join dbo.m_actype mt on ma.actype_id=mt.actype_id where ACT_FINISH_DATE is null and ACT_START_DATE is not null and ma.own not in ('GA','CITILINK')");
        return $sql->result_array();
    }

    function get_aircraft()
    {
        $sql = $this->db2->query("SELECT DISTINCT case
                when own = 'GA' then 'GA'
                when own = 'CITILINK' then 'CITILINK'
                else 'NON GA'
                end as own
                from m_acreg group by OWN");
        return $sql->result_array();
    }

    function get_airtype($param)
    {

        $sql = $this->db2->query("SELECT own,actype from m_acreg ma join m_actype mt on ma.actype_id=mt.actype_id where own in ('" . $param . "') group by own,mt.actype");
        return $sql->result_array();
    }

    function get_airtype_nonga($param)
    {

        $sql = $this->db2->query("SELECT own,actype from m_acreg ma join m_actype mt on ma.actype_id=mt.actype_id where own not in ('GA','CITILINK') or own in ('" . $param . "') group by own,mt.actype");
        return $sql->result_array();
    }

    function get_value_ga_by($input, $date, $query)
    {
        // and ma.own in ('".$input['param_airc']."')
        $sql = $this->db2->query("SELECT mt.actype as tipe,ta.AIRCRAFT_REG as register,
        ta.REVISION_DESCRIPTION as inspect,ta.PLANT_TAT as target,ta.ACT_TAT as actual,
        DATEDIFF(DAY,ta.ACT_START_DATE, GETDATE()) as selisih,case
              when ma.own = 'GA' then 'GA'
              when ma.own = 'CITILINK' then 'CITILINK'
              else 'NON GA'
              end as own
          from dbo.TAT_AIRFRAME ta join dbo.m_acreg ma on ta.AIRCRAFT_REG=ma.acreg
          join dbo.m_actype mt on ma.actype_id=mt.actype_id
          where
          ACT_START_DATE BETWEEN '" . $date['start'] . "' AND '" . $date['end'] . "'
          and mt.actype like '%" . $input['air_type'] . "'
          and ta.PLANT in (" . $input['tat_type'] . ")
          and mt.fleet_type in (" . $input['fleet'] . ")
          " . $query . " AND LAST_UPDATE = 'REL' ");

        return $sql->result_array();
    }

    function get_value_nonga_by($input, $date)
    {
        $sql = $this->db2->query("SELECT mt.actype as tipe,
        ta.AIRCRAFT_REG as register,
        ta.REVISION_DESCRIPTION as inspect,
        ta.PLANT_TAT as target,
        ta.ACT_TAT as actual,
        DATEDIFF(DAY,ta.ACT_START_DATE, GETDATE()) as selisih,case
              when ma.own = 'GA' then 'GA'
              when ma.own = 'CITILINK' then 'CITILINK'
              else 'NON GA'
              end as own
          from dbo.TAT_AIRFRAME ta join dbo.m_acreg ma on ta.AIRCRAFT_REG=ma.acreg
          join dbo.m_actype mt on ma.actype_id=mt.actype_id
          where ACT_FINISH_DATE LIKE '000000%'
          and ACT_START_DATE BETWEEN '" . $date['start'] . "' AND '" . $date['end'] . "'
          and mt.actype like '%" . $input['air_type'] . "'
          and mt.fleet_type in (" . $input['fleet'] . ")
          and ta.PLANT in (" . $input['tat_type'] . ")
          and (ma.own not in ('GA','CITILINK')
          or ma.own in ('" . $input['param_airc'] . "'))");

        return $sql->result_array();
    }

    function get_value_ga_datatable($input, $date, $data)
    {
      $own = $input['param_airc'];
      $q_own = " ";
      if($own == "GA"){
          $q_own = " AND tc.TAT_C_CODE LIKE 'GIA%' ESCAPE '!' ";
      } elseif ($own == "CITILINK") {
          $q_own = " AND tc.TAT_C_CODE LIKE 'CTV%' ESCAPE '!' OR tc.TAT_C_CODE LIKE 'GL%' ESCAPE '!' ";
      }
      $query = "SELECT
                	*
                FROM
                	(
                		SELECT
                			ROW_NUMBER () OVER (ORDER BY mt.actype) AS RowNum,
                			mt.actype AS tipe,
                			ta.AIRCRAFT_REG AS register,
                			ta.REVISION_DESCRIPTION AS inspect,
                			ta.PLANT_TAT AS target,
                			ta.ACT_TAT AS actual,
							ta.REVISION AS revision,
							ta.ACT_START_DATE AS start_date,
                			DATEDIFF(
                				DAY,
                				ta.ACT_START_DATE,
                				GETDATE()
                			) AS selisih,
                			CASE
                			WHEN tc.TAT_C_CODE LIKE 'GIA%' THEN
                				'GARUDA'
                			WHEN tc.TAT_C_CODE LIKE 'CTV%'
                			OR tc.TAT_C_CODE LIKE 'GL%' THEN
                				'CITILINK'
                			ELSE 
                				'NON GA'
                			END AS own
                		FROM
                			dbo.TAT_AIRFRAME ta
                		JOIN dbo.TAT_CUSTOMER tc ON ta.ID_CUSTOMER = tc.TAT_C_CODE
                		LEFT JOIN dbo.m_acreg ma ON ta.AIRCRAFT_REG = ma.acreg
                		LEFT JOIN dbo.m_actype mt ON ma.actype_id = mt.actype_id
                		WHERE
                			LAST_UPDATE = 'REL'
                		AND ACT_START_DATE BETWEEN '".$date['start']."'
                		AND '".$date['end']."'
                		AND mt.actype LIKE '%".$input['air_type']."'
                		AND mt.fleet_type IN (".$input['fleet'].")
                		AND ta.PLANT IN (".$input['tat_type'].")
                		".$q_own."
                	) N
                WHERE ";

        if ($data['search'] != '') {
            $query .= " N.tipe like '%" . $data['search'] . "%' OR
                          N.register like '%" . $data['search'] . "%' OR
                          N.inspect LIKE  '%" . $data['search'] . "%' OR
                          N.target LIKE  '%" . $data['search'] . "%' OR
                          N.own LIKE  '%" . $data['search'] . "%' OR
                          N.actual LIKE '%" . $data['search'] . "%' AND";
        }
        $query .= " N.RowNum >'" . $data['start'] . "'
                       AND N.RowNum <= '" . ($data['length'] + $data['start']) . "'";

        $query_count =  "SELECT
                                	*
                                FROM
                                	(
                                		SELECT
                                			ROW_NUMBER () OVER (ORDER BY mt.actype) AS RowNum,
                                			mt.actype AS tipe,
                                			ta.AIRCRAFT_REG AS register,
                                			ta.REVISION_DESCRIPTION AS inspect,
                                			ta.PLANT_TAT AS target,
                                			ta.ACT_TAT AS actual,
											ta.REVISION AS revision,
											ta.ACT_START_DATE AS start_date,
                                			DATEDIFF(
                                				DAY,
                                				ta.ACT_START_DATE,
                                				GETDATE()
                                			) AS selisih,
                                			CASE
                                			WHEN tc.TAT_C_CODE LIKE 'GIA%' THEN
                                				'GARUDA'
                                			WHEN tc.TAT_C_CODE LIKE 'CTV%'
                                			OR tc.TAT_C_CODE LIKE 'GL%' THEN
                                				'CITILINK'
                                			ELSE
                                				'NON GA'
                                			END AS own
                                		FROM
                                			dbo.TAT_AIRFRAME ta
                                		JOIN dbo.TAT_CUSTOMER tc ON ta.ID_CUSTOMER = tc.TAT_C_CODE
                                		LEFT JOIN dbo.m_acreg ma ON ta.AIRCRAFT_REG = ma.acreg
                                		LEFT JOIN dbo.m_actype mt ON ma.actype_id = mt.actype_id
                                		WHERE
                                			LAST_UPDATE = 'REL'
                                		AND ACT_START_DATE BETWEEN '".$date['start']."'
                                		AND '".$date['end']."'
                                		AND mt.actype LIKE '%".$input['air_type']."'
                                		AND mt.fleet_type IN (".$input['fleet'].")
                                		AND ta.PLANT IN (".$input['tat_type'].")
                                		".$q_own."
                                	) N ";
        if ($data['search'] != '') {
            $query_count .= "WHERE N.tipe like '%" . $data['search'] . "%' OR
                                  N.register like '%" . $data['search'] . "%' OR
                                  N.inspect LIKE  '%" . $data['search'] . "%' OR
                                  N.target LIKE  '%" . $data['search'] . "%' OR
                                  N.own LIKE  '%" . $data['search'] . "%' OR
                                  N.actual LIKE '%" . $data['search'] . "%' ";
        }

        $hasil = $this->db2->query($query);
        $hasil2 = $this->db2->query($query_count);

        $output = array('data' => $hasil->result_array(),
            'jml' => $hasil2->num_rows());
        return $output;

    }

    function get_value_nonga_datatable($input, $date, $data)
    {

      $query = "SELECT
                	*
                FROM
                	(
                		SELECT
                			ROW_NUMBER () OVER (ORDER BY mt.actype) AS RowNum,
                			mt.actype AS tipe,
                			ta.AIRCRAFT_REG AS register,
                			ta.REVISION_DESCRIPTION AS inspect,
                			ta.PLANT_TAT AS target,
                			ta.ACT_TAT AS actual,
							ta.REVISION AS revision,
							ta.ACT_START_DATE AS start_date,
                			DATEDIFF(
                				DAY,
                				ta.ACT_START_DATE,
                				GETDATE()
                			) AS selisih,
                      CASE
                      WHEN ma.own = 'GA' THEN
                  			'GARUDA'
                  		WHEN ma.own ='CITILINK' THEN
                  			'CITILINK'
                  		ELSE
                  			'NON GA'
                			END
                			AS own
                		FROM
                			dbo.TAT_AIRFRAME ta
                		LEFT JOIN dbo.m_acreg ma ON ta.AIRCRAFT_REG = ma.acreg
                		LEFT JOIN dbo.m_actype mt ON ma.actype_id = mt.actype_id
                		WHERE
                			LAST_UPDATE = 'REL'
                		AND ACT_START_DATE BETWEEN '".$date['start']."' AND '".$date['end']."'
                		AND mt.actype LIKE '%".$input['air_type']."'
                		AND mt.fleet_type IN (".$input['fleet'].")
                		AND ta.PLANT IN (".$input['tat_type'].")
                    AND ID_CUSTOMER NOT LIKE 'GIA%' ESCAPE '!'
                    AND ID_CUSTOMER NOT LIKE 'CTV%' ESCAPE '!'
                    AND ID_CUSTOMER NOT LIKE 'GL%' ESCAPE '!'
                ) N WHERE ";

        if ($data['search'] != '') {
            $query .= " N.tipe like '%" . $data['search'] . "%' OR
                          N.register like '%" . $data['search'] . "%' OR
                          N.inspect LIKE  '%" . $data['search'] . "%' OR
                          N.target LIKE  '%" . $data['search'] . "%' OR
                          N.own LIKE  '%" . $data['search'] . "%' OR
                          N.actual LIKE '%" . $data['search'] . "%' AND";
        }
        $query .= " N.RowNum >'" . $data['start'] . "'
                       AND N.RowNum <= '" . ($data['length'] + $data['start']) . "'";

        $query_count = "SELECT
                        	*
                        FROM
                        	(
                        		SELECT
                        			ROW_NUMBER () OVER (ORDER BY mt.actype) AS RowNum,
                        			mt.actype AS tipe,
                        			ta.AIRCRAFT_REG AS register,
                        			ta.REVISION_DESCRIPTION AS inspect,
                        			ta.PLANT_TAT AS target,
                        			ta.ACT_TAT AS actual,
									ta.REVISION AS revision,
									ta.ACT_START_DATE AS start_date,
                        			DATEDIFF(
                        				DAY,
                        				ta.ACT_START_DATE,
                        				GETDATE()
                        			) AS selisih,
                        			ID_CUSTOMER
                        		FROM
                        			dbo.TAT_AIRFRAME ta
                        		LEFT JOIN dbo.m_acreg ma ON ta.AIRCRAFT_REG = ma.acreg
                        		LEFT JOIN dbo.m_actype mt ON ma.actype_id = mt.actype_id
                        		WHERE
                        			LAST_UPDATE = 'REL'
                        		AND ACT_START_DATE BETWEEN '".$date['start']."' AND '".$date['end']."'
                        		AND mt.actype LIKE '%".$input['air_type']."'
                        		AND mt.fleet_type IN (".$input['fleet'].")
                        		AND ta.PLANT IN (".$input['tat_type'].")
                            AND ID_CUSTOMER NOT LIKE 'GIA%' ESCAPE '!'
                            AND ID_CUSTOMER NOT LIKE 'CTV%' ESCAPE '!'
                            AND ID_CUSTOMER NOT LIKE 'GL%' ESCAPE '!'
                        ) N ";
        if ($data['search'] != '') {
            $query_count .= "WHERE N.tipe like '%" . $data['search'] . "%' OR
                                  N.register like '%" . $data['search'] . "%' OR
                                  N.inspect LIKE  '%" . $data['search'] . "%' OR
                                  N.target LIKE  '%" . $data['search'] . "%' OR
                                  N.own LIKE  '%" . $data['search'] . "%' OR
                                  N.actual LIKE '%" . $data['search'] . "%'";
        }

        $hasil = $this->db2->query($query);
        $hasil2 = $this->db2->query($query_count);

        $output = array('data' => $hasil->result_array(),
            'jml' => $hasil2->num_rows());
        return $output;

    }

    function get_total_chart_2($dataType, $query, $date, $mon)
    {
        $sql = $this->db2->query("SELECT * from (
              SELECT DATEPART(YEAR, ACT_START_DATE) as ac_year,
                     DATEPART(MONTH, ACT_START_DATE) as AC_MONTH,
                    Sum(isnull(cast(PLANT_TAT as float),0)) as planttat,
                    Sum(isnull(cast(ACT_TAT as float),0)) as acttat
              from dbo.TAT_AIRFRAME ta
              join dbo.m_acreg ma on ta.AIRCRAFT_REG=ma.acreg
              join dbo.m_actype mt on ma.actype_id=mt.actype_id
              where ACT_FINISH_DATE is not null
              AND ACT_START_DATE >= '" . $date['start'] . "'
              AND ACT_START_DATE <= '" . $date['end'] . "'
              and mt.actype ='" . $dataType['name'] . "'
              " . $query . "
              GROUP BY DATEPART(YEAR, ACT_START_DATE), DATEPART(MONTH, ACT_START_DATE)
              ) T where AC_MONTH = '" . $mon . "'");
        return $sql->row_array();
    }

    function get_total_chart_nonga($dataType, $date, $mon)
    {
        $sql = $this->db2->query("SELECT * from (
                SELECT DATEPART(YEAR, ACT_START_DATE) as ac_year,
                       DATEPART(MONTH, ACT_START_DATE) as AC_MONTH,
                      Sum(isnull(cast(PLANT_TAT as float),0)) as planttat,
                      Sum(isnull(cast(ACT_TAT as float),0)) as acttat
                from dbo.TAT_AIRFRAME ta
                join dbo.m_acreg ma on ta.AIRCRAFT_REG=ma.acreg
                join dbo.m_actype mt on ma.actype_id=mt.actype_id
                where ACT_FINISH_DATE is not null
                AND ACT_START_DATE >= '" . $date['start'] . "'
                AND ACT_START_DATE <= '" . $date['end'] . "'
                and mt.actype ='" . $dataType['name'] . "' and ma.own not in ('GA','CITILINK')
                GROUP BY DATEPART(YEAR, ACT_START_DATE), DATEPART(MONTH, ACT_START_DATE)
                ) T where AC_MONTH = '" . $mon . "'");
        return $sql->result();
    }

    function cek_month_chart($jenis, $date)
    {
        $sql = $this->db2->query("SELECT DATEPART(YEAR, ACT_START_DATE) as ac_year,
             DATEPART(MONTH, ACT_START_DATE) as AC_MONTH
      from dbo.TAT_AIRFRAME ta
      join dbo.m_acreg ma on ta.AIRCRAFT_REG=ma.acreg
      join dbo.m_actype mt on ma.actype_id=mt.actype_id
      where ACT_FINISH_DATE is not null
      AND ACT_START_DATE >= '" . $date['start'] . "'
      AND ACT_START_DATE <= '" . $date['end'] . "'
      AND ma.own='" . $jenis . "'
      GROUP BY DATEPART(YEAR, ACT_START_DATE), DATEPART(MONTH, ACT_START_DATE)");
        return $sql->result_array();
    }

    function cek_month_chart_nonga($jenis, $date)
    {
        $sql = $this->db2->query("SELECT DATEPART(YEAR, ACT_START_DATE) as ac_year,
             DATEPART(MONTH, ACT_START_DATE) as AC_MONTH
      from dbo.TAT_AIRFRAME ta
      join dbo.m_acreg ma on ta.AIRCRAFT_REG=ma.acreg
      join dbo.m_actype mt on ma.actype_id=mt.actype_id
      where ACT_FINISH_DATE is not null
      AND ACT_START_DATE >= '" . $date['start'] . "'
      AND ACT_START_DATE <= '" . $date['end'] . "'
      AND ma.own not in ('GA','CITILINK')
      GROUP BY DATEPART(YEAR, ACT_START_DATE), DATEPART(MONTH, ACT_START_DATE)");
        return $sql->result_array();
    }
}
