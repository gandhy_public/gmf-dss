<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class M_mro_expensecategory extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->dev_gmf=$this->load->database('dev_gmf',true);
    }

    public function _getAllData()
    {
        $data = $this->db->get('mro_expense_category');
        return $data;
    }

    public function _getCountData()
    {
        $this->db->from('mro_expense_category');
        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }

    public function getAllData()
    {
        $data = $this->db->query('select * from mro_expense_category order by name ASC');

        return $data->result_array();
    }

    public function getexpenseall()
    {
        $data = $this->dev_gmf->query('select DISTINCT(expense_category) from mro ORDER BY expense_category ASC');

        return $data->result_array();
    }

    public function getByName($name)
    {
        $this->db->where('name', $name);
        $query = $this->db->get('mro_expense_category');

        return $query->row();
    }

    public function insert($data)
    {
        $this->db->insert('mro_expense_category', $data);

        $id = $this->db->insert_id();
        return (isset($id)) ? $id : FALSE;
    }

}
