<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class M_kpi_sla_master extends CI_Model
  {
    public function __construct()
    {
      parent::__construct();
      $this->dev_gmf = $this->load->database("dev_gmf", true);
      $this->load->database();
    }

    public function delete_unit_where($role_id)
    {
      $sql = "delete kpi_unit where kpi_unit_id = '".$role_id."'";
      $exec = $this->dev_gmf->query($sql);
      return ($exec) ? true : false;
    }

    public function delete_unit_assign_where($role_id)
    {
      $sql = "delete kpi_item_assignment where role_id = '".$role_id."'";
      $exec = $this->dev_gmf->query($sql);
      return ($exec) ? true : false;
    }

    public function delete_unit_user_where($role_id)
    {
      $sql = "update user set kpi_unit_id = NULL where kpi_unit_id = '".$role_id."'";
      $exec = $this->db->query($sql);
      return ($exec) ? true : false;
    }

    public function getTotalScoreNotLevelOneWhere($id, $month, $nowYear, $depth)
    {
      $total = 0;
      $sql = "select ki_id, ki_weight from kpi_item where ki_parent_id = '".$id."'
              AND DATEPART(
                YEAR,
                CONVERT (DATE, ki_year_date, 20)
              ) = '".$nowYear."'";
      $exec = $this->dev_gmf->query($sql);
      $arr = [];
      foreach ($exec->result() as $data) {
        $arr[] = array("ki_id" => $data->ki_id, "weight" => $data->ki_weight);
      }
      // $newId = implode(",", $arr);
      // exit();
      return $arr;
    }

    public function getTotalScoreWhere($id, $month, $nowYear, $depth)
    {
      $sql = "SELECT
                a.ki_id,
                SUM (e.kiad_actual) / COUNT (a.ki_id) AS kiad_actual,
                a.ki_name,
                a.ki_uom,
                a.ki_year_date,
                a.ki_parent_id,
                a.ki_weight,
                b.kf_name,
                b.fa_function_name,
                c.kid_id,
                c.kid_date,
                c.kid_target,
                c.kid_weight,
                c.kid_limit,
                c.kid_islock,
                c.kid_author
              FROM
                kpi_item a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_detail c ON a.ki_id = c.ki_id
              AND DATEPART(
                YEAR,
                CONVERT (DATE, c.kid_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
                MONTH,
                CONVERT (DATE, c.kid_date, 20)
              ) = '".$month."'
              LEFT JOIN kpi_item_assignment d ON a.ki_id = d.ki_id
              LEFT JOIN kpi_item_assigment_detail e ON d.kia_id = e.kia_id
              AND DATEPART(
                YEAR,
                CONVERT (DATE, e.kiad_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
                MONTH,
                CONVERT (DATE, e.kiad_date, 20)
              ) = '".$month."'
              where a.ki_parent_id = '".$id."'
              GROUP BY
                a.ki_id, 	a.ki_name,
                a.ki_uom,
                a.ki_year_date,
                a.ki_parent_id,
                a.ki_weight,
                b.kf_name,
                b.fa_function_name,
                c.kid_id,
                c.kid_date,
                c.kid_target,
                c.kid_weight,
                c.kid_limit,
                c.kid_islock,
                c.kid_author";
        // echo "<pre>".$sql."</pre>";
        $exec = $this->dev_gmf->query($sql);
        $result = $exec->result();
        $total = 0;
        foreach ($result as $data) {

          $weight = "";
          if($depth == "0"){
            $weight = $this->countWeight($data->ki_id, $nowYear, $month);
          } else {
            if(empty($data->kid_weight)){
              $weight  = $data->ki_weight;
            } else{
              $weight = $data->kid_weight;
            }
          }
          $ach = number_format(achievment( $data->kid_target, $data->kiad_actual, $data->fa_function_name), 2);
          $total += score($ach, $data->kid_limit, $weight);
          $total += $this->getTotalScoreWhere($data->ki_id, $month, $nowYear, $depth+1);
        }
        // exit();
        return number_format($total, 2);
    }

    public function getTotalScoreYtdWhere($id, $month, $nowYear, $depth)
    {
      $sql = "SELECT
                a.ki_id,
                SUM (e.kiad_actual_ytd) / COUNT (a.ki_id) AS kiad_actual_ytd,
                a.ki_name,
                a.ki_uom,
                a.ki_year_date,
                a.ki_parent_id,
                a.ki_weight,
                b.kf_name,
                b.fa_function_name,
                c.kid_id,
                c.kid_date,
                c.kid_target_ytd,
                c.kid_weight,
                c.kid_limit,
                c.kid_islock,
                c.kid_author
              FROM
                kpi_item a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_detail c ON a.ki_id = c.ki_id
              AND DATEPART(
                YEAR,
                CONVERT (DATE, c.kid_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
                MONTH,
                CONVERT (DATE, c.kid_date, 20)
              ) = '".$month."'
              LEFT JOIN kpi_item_assignment d ON a.ki_id = d.ki_id
              LEFT JOIN kpi_item_assigment_detail e ON d.kia_id = e.kia_id
              AND DATEPART(
                YEAR,
                CONVERT (DATE, e.kiad_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
                MONTH,
                CONVERT (DATE, e.kiad_date, 20)
              ) = '".$month."'
              where a.ki_parent_id = '".$id."'
              GROUP BY
                a.ki_id, 	a.ki_name,
                a.ki_uom,
                a.ki_year_date,
                a.ki_parent_id,
                a.ki_weight,
                b.kf_name,
                b.fa_function_name,
                c.kid_id,
                c.kid_date,
                c.kid_target_ytd,
                c.kid_weight,
                c.kid_limit,
                c.kid_islock,
                c.kid_author";
        // echo "<pre>".$sql."</pre>";
        $exec = $this->dev_gmf->query($sql);
        $result = $exec->result();
        $total = 0;
        foreach ($result as $data) {
          // echo json_encode($result);exit;
          $weight = "";
          if($depth == "0"){
            $weight = $this->countWeight($data->ki_id, $nowYear, $month);
          } else {
            if(empty($data->kid_weight)){
              $weight  = $data->ki_weight;
            } else{
              $weight = $data->kid_weight;
            }
          }
          $ach = number_format(achievment( $data->kid_target_ytd, $data->kiad_actual_ytd, $data->fa_function_name), 2);
          $total += score($ach, $data->kid_limit, $weight);
          $total += $this->getTotalScoreWhere($data->ki_id, $month, $nowYear, $depth+1);
        }
        // exit();
        return number_format($total, 2);
    }

    public function maxLevel($id)
    {
      $sql = "WITH rCTE AS(
                  SELECT *, 0 AS Level FROM kpi_item WHERE ki_id = '".$id."'
                  UNION ALL
                  SELECT t.*, r.Level + 1 AS Level
                  FROM kpi_item t
                  INNER JOIN rCTE r
                      ON t.ki_parent_id = r.ki_id
              )
              SELECT max(level) as ujung FROM rCTE OPTION(MAXRECURSION 0)";
      $exec = $this->dev_gmf->query($sql);
      return $exec->row()->ujung;
    }

     public function maxLevel_unit($id)
    {
      $sql = "WITH rCTE AS(
                  SELECT *, 0 AS Level FROM kpi_item_assignment WHERE kia_id = '".$id."'
                  UNION ALL
                  SELECT t.*, r.Level + 1 AS Level
                  FROM kpi_item_assignment t
                  INNER JOIN rCTE r
                      ON t.kia_parent_id = r.kia_id
              )
              SELECT max(level) as ujung FROM rCTE OPTION(MAXRECURSION 0)";
      $exec = $this->dev_gmf->query($sql);
      return $exec->row()->ujung;
    }

    public function getLevelWithID($id){
      $sql="WITH rCTE AS(
                  SELECT *, 0 AS Level FROM kpi_item WHERE ki_id = '".$id."'
                  UNION ALL
                  SELECT t.*, r.Level + 1 AS Level
                  FROM kpi_item t
                  INNER JOIN rCTE r
                      ON t.ki_parent_id = r.ki_id
              )
              SELECT level as ujung,ki_id,ki_name,ki_parent_id FROM rCTE OPTION(MAXRECURSION 0)";
      $sikat=$this->dev_gmf->query($sql);
      return $sikat->result_array();    
      
      
    }

    public function countWeight($id, $year, $month)
    {
      $sql = "SELECT
                SUM (weight) AS total
              FROM
                (
                  SELECT
                    CASE
                  WHEN b.kid_weight IS NULL THEN
                    a.ki_weight
                  ELSE
                    b.kid_weight
                  END AS weight
                  FROM
                    kpi_item a
                  LEFT JOIN kpi_item_detail b ON a.ki_id = b.ki_id
                  AND DATEPART(
                    MONTH,
                    CONVERT (DATE, kid_date, 20)
                  ) = '".$month."'
                  AND DATEPART(
                    YEAR,
                    CONVERT (DATE, kid_date, 20)
                  ) = '".$year."'
                  WHERE
                    ki_parent_id = '".$id."'
                ) as A";
        $exec = $this->dev_gmf->query($sql);
        return $exec->row()->total;
    }

    public function listActual()
    {
      $nowYear = date("Y");
      $month = $this->input->get("month");
      $sql = "SELECT
                a.ki_id,
              	SUM (e.kiad_actual) / COUNT (a.ki_id) AS kiad_actual,
              	SUM (e.kiad_actual_ytd) / COUNT (a.ki_id) AS kiad_actual_ytd,
              	a.ki_name,
              	a.ki_uom,
              	a.ki_year_date,
              	a.ki_parent_id,
              	a.ki_weight,
              	a.ki_target,
              	a.ki_target_ytd,
              	b.kf_name,
              	b.fa_function_name,
                c.kid_id,
               c.kid_date,
               c.kid_target,
               c.kid_target_ytd,
               c.kid_weight,
               c.kid_limit,
               c.kid_islock,
               c.kid_author
              FROM
              	kpi_item a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_detail c ON a.ki_id = c.ki_id
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, c.kid_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
              	MONTH,
              	CONVERT (DATE, c.kid_date, 20)
              ) = '".$month."'
              LEFT JOIN kpi_item_assignment d ON a.ki_id = d.ki_id
              LEFT JOIN kpi_item_assigment_detail e ON d.kia_id = e.kia_id
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, e.kiad_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
              	MONTH,
              	CONVERT (DATE, e.kiad_date, 20)
               ) = '".$month."'
              WHERE DATEPART(
              	YEAR,
              	CONVERT (DATE, a.ki_year_date, 20)
              ) = '".$nowYear."'
              GROUP BY
              	a.ki_id, 	a.ki_name,
              	a.ki_uom,
              	a.ki_year_date,
              	a.ki_parent_id,
              	a.ki_weight,
                a.ki_target,
              	a.ki_target_ytd,
              	b.kf_name,
              	b.fa_function_name,
                c.kid_id,
                 c.kid_date,
                 c.kid_target,
                 c.kid_target_ytd,
                 c.kid_weight,
                 c.kid_limit,
                 c.kid_islock,
                 c.kid_author
              ";
      // echo $sql; exit();
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }
    public function listActualWhere($id)
    {
      $nowYear = date("Y");
      $month = $this->input->get("month");
      $sql = "SELECT
                a.ki_id,
              	SUM (e.kiad_actual) / COUNT (a.ki_id) AS kiad_actual,
              	SUM (e.kiad_actual_ytd) / COUNT (a.ki_id) AS kiad_actual_ytd,
              	a.ki_name,
              	a.ki_uom,
              	a.ki_year_date,
              	a.ki_parent_id,
              	a.ki_weight,
              	a.ki_target,
              	a.ki_target_ytd,
              	b.kf_name,
              	b.fa_function_name,
                c.kid_id,
               c.kid_date,
               c.kid_target,
               c.kid_target_ytd,
               c.kid_weight,
               c.kid_limit,
               c.kid_islock,
               c.kid_author
              FROM
              	kpi_item a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_detail c ON a.ki_id = c.ki_id
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, c.kid_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
              	MONTH,
              	CONVERT (DATE, c.kid_date, 20)
              ) = '".$month."'
              LEFT JOIN kpi_item_assignment d ON a.ki_id = d.ki_id
              LEFT JOIN kpi_item_assigment_detail e ON d.kia_id = e.kia_id
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, e.kiad_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
              	MONTH,
              	CONVERT (DATE, e.kiad_date, 20)
               ) = '".$month."'
              WHERE
              a.ki_id='".$id."'
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, a.ki_year_date, 20)
              ) = '".$nowYear."'
              GROUP BY
              	a.ki_id, 	a.ki_name,
              	a.ki_uom,
              	a.ki_year_date,
              	a.ki_parent_id,
              	a.ki_weight,
                a.ki_target,
              	a.ki_target_ytd,
              	b.kf_name,
              	b.fa_function_name,
                c.kid_id,
                 c.kid_date,
                 c.kid_target,
                 c.kid_target_ytd,
                 c.kid_weight,
                 c.kid_limit,
                 c.kid_islock,
                 c.kid_author
              ";
      // echo $sql; exit();
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }


    public function listActualWhere_unit($id,$role_id)
    {
      $nowYear = date("Y");
      $month = $this->input->get("month");
      $sql = "SELECT
              	a.*, b.kf_name,
              	b.fa_function_name,
              	c.kiad_id,
              	c.kiad_date,
              	c.kiad_target,
              	c.kiad_target_ytd,
              	c.kiad_limit,
              	c.kiad_islock,
              	c.kiad_author,
                c.kiad_actual,
                c.kiad_actual_ytd,
                c.kiad_weight
              FROM
              	kpi_item_assignment a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_assigment_detail c ON a.kia_id = c.kia_id
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, c.kiad_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
              	MONTH,
              	CONVERT (DATE, c.kiad_date, 20)
              ) = '".$month."'
              where a.role_id = '".$role_id."'
              and a.kia_id='".$id."'
               and DATEPART( year, CONVERT ( DATE, a.kia_year_date, 20 ) ) = '".$nowYear."'";


      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function listActualWhere_unitz($id,$role_id,$month,$year)
    {
      $nowYear = $year;
      
      $sql = "SELECT
              	a.*, b.kf_name,
              	b.fa_function_name,
              	c.kiad_id,
              	c.kiad_date,
              	c.kiad_target,
              	c.kiad_target_ytd,
              	c.kiad_limit,
              	c.kiad_islock,
              	c.kiad_author,
                c.kiad_actual,
                c.kiad_actual_ytd,
                c.kiad_weight
              FROM
              	kpi_item_assignment a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_assigment_detail c ON a.kia_id = c.kia_id
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, c.kiad_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
              	MONTH,
              	CONVERT (DATE, c.kiad_date, 20)
              ) = '".$month."'
              where a.role_id = '".$role_id."'
              and a.kia_id='".$id."'
               and DATEPART( year, CONVERT ( DATE, a.kia_year_date, 20 ) ) = '".$nowYear."'";


      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function getDeepChild($id)
    {
      $sql = "select count(ki_parent_id) as total from kpi_item where ki_parent_id = '".$id."'";
      $exec = $this->dev_gmf->query($sql);
      $arr = $exec->row();
      return $arr->total;
    }

    public function the_year($kia_id)
    {
      $sql = "SELECT DATEPART(
        YEAR,
        CONVERT (DATE, kia_year_date, 20)
      ) as year from kpi_item_assignment where kia_id = '".$kia_id."'";
      $exec = $this->dev_gmf->query($sql)->row();
      return $exec->year;
    }

    public function delete_kia_where($kia_id)
    {
      $return = [];
      $year = $this->the_year($kia_id);
      $sql1 = "delete from kpi_item_assigment_detail where
               kia_id = '".$kia_id."'
               and
               DATEPART(
                 YEAR,
                 CONVERT (DATE, kiad_date, 20)
               ) = '".$year."'";
      $exec1 = $this->dev_gmf->query($sql1);
      if($exec1){
        $sql = "delete from kpi_item_assignment where kia_id = '".$kia_id."'";
        $exec = $this->dev_gmf->query($sql);
        $return["success"] = ($exec) ? true : false;
      }
      return $return;
    }

    public function get_kpi_assigment_where($role_id)
    {
      $sql = "SELECT
              	a.kia_id, a.kia_name+' - '+CONVERT(VARCHAR, YEAR(a.kia_year_date)) as kia_name, a.ki_id
              FROM
              	kpi_item_assignment a join kpi_item b on a.ki_id = b.ki_id
              WHERE
              	role_id = '".$role_id."'
              ORDER BY a.kia_name ASC";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function insert_assign_kpi()
    {
      $kpi = $this->input->post("kpi");
      $role_id = $this->input->post("role_id");
      $boolean = false;
      foreach ($kpi as $ki_id) {
        $sql = "INSERT INTO kpi_item_assignment (
                  kf_id,
                  kia_author,
                  kia_name,
                  kia_uom,
                  kia_level,
                  kia_year_date,
                  kia_parent_id,
                  role_id,
                  ki_id,
                  kia_weight
                ) SELECT
                  kf_id,
                  ki_author,
                  ki_name,
                  ki_uom,
                  'deep_child' AS a,
                  ki_year_date,
                  '' AS b,
                  '".$role_id."' AS c,
                  '".$ki_id."' AS d,
                  ki_weight
                FROM
                  kpi_item
                WHERE
                  ki_id = '".$ki_id."'";
          $exec1 = $this->dev_gmf->query($sql);
          if($exec1){
            $insert_id = $this->dev_gmf->insert_id();;
            $sql2 = "INSERT INTO kpi_item_assigment_detail (
                    	kia_id,
                    	kiad_date,
                    	kiad_target,
                    	kiad_target_ytd,
                    	kiad_weight,
                    	kiad_limit,
                    	kiad_islock,
                    	kiad_author
                    ) SELECT
                    	'".$insert_id."' AS kia_id,
                    	kid_date,
                    	kid_target,
                    	kid_target_ytd,
                    	kid_weight,
                    	kid_limit,
                    	kid_islock,
                    	kid_author
                    FROM
                    	kpi_item_detail
                    where ki_id = '".$ki_id."'";
            $exec2 = $this->dev_gmf->query($sql2);
            $boolean = ($exec2) ? true : false;
          }
      }
      return array("success" => $boolean);
    }

    public function get_deep_child()
    {
      $year = $this->input->get("year");
      $sql = "SELECT
              	ki_id,
              	ki_name+' - '+CONVERT(VARCHAR, YEAR(ki_year_date)) as ki_name
              FROM
              	kpi_item
              WHERE
              	ki_level = 'deep_child'
                and
                YEAR(ki_year_date) = '".$year."'
              ORDER BY
              	ki_name ASC";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function insert_unit($user_id, $unit_name)
    {
      $kpi_unit_id = $this->input->post("kpi_unit_id");
      if(empty($kpi_unit_id) || $kpi_unit_id == ""){
          $sql = "insert into kpi_unit (kpi_unit_name) values ('".$unit_name."');";
      } else {
        $sql = "update kpi_unit set kpi_unit_name = '".$unit_name."' where kpi_unit_id = '".$kpi_unit_id."'";
      }
      $result = array();
      if($this->dev_gmf->query($sql)){
        $query = $this->dev_gmf->query("SELECT IDENT_CURRENT('kpi_unit') as last_id");
        $res = $query->result();
        $last_id = $res[0]->last_id;
        $update = "'".implode("','", $user_id)."'";
        $sql = "update user set kpi_unit_id = '".$last_id."' where user_id in (".$update.")";
        if($this->db->query($sql)){
          $result["success"] = true;
        } else {
          $result["success"] = false;
        }
      } else{
        $result["success"] = false;
      }
      return $result;
    }

    public function insert_unit_new($user_id, $unit_name, $kpi_unit_id)
    {
      if(empty($kpi_unit_id) || $kpi_unit_id == ""){
          $sql = "insert into kpi_unit (kpi_unit_name) values ('".$unit_name."');";
      } else {
        $sql = "update kpi_unit set kpi_unit_name = '".$unit_name."' where kpi_unit_id = '".$kpi_unit_id."'";
      }

      $result = array();
      if($this->dev_gmf->query($sql)){
        $query = $this->dev_gmf->query("SELECT IDENT_CURRENT('kpi_unit') as last_id");
        $res = $query->result();
        $last_id = $res[0]->last_id;
        $update = "'".implode("','", $user_id)."'";
        $sql = "update user set kpi_unit_id = '".$kpi_unit_id."' where user_id in (".$update.")";
        if($this->db->query($sql)){
          $result["success"] = true;
        } else {
          $result["success"] = false;
        }
      } else{
        $result["success"] = false;
      }
      return $result;
    }

    public function list_unit()
    {
      $result = array();
      $sql = "select * from kpi_unit";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function role()
    {
      $result = array();
      $sql = "SELECT role_id, role_name from role where role_isactive = '1' and role_iskpi = '0'";
      $exec = $this->db->query($sql);
      return $exec->result();
    }

    public function getAllFormula()
    {
      $result = array();
      $sql = "SELECT kf_id, kf_name FROM kpi_formula";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function getAllItemKpi($ki_year = null)
    {
      $result = array();
      $sql_plus = "";
      if($ki_year != null){
        $sql_plus = " WHERE
                        DATEPART(
                          YEAR,
                          ki_year_date
                        ) = '".$ki_year."' ";
      }
      $sql = "SELECT a.*, b.kf_name FROM kpi_item a join kpi_formula b on a.kf_id = b.kf_id ".$sql_plus;
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function getAllItemKpiWithDetail()
    {
      $nowYear = $this->input->post("year");
      $nowMonth = date("m");
      $result = array();
      $sql = "SELECT
              	a.*, b.kf_name, b.fa_function_name, c.kid_id, c.kid_date, c.kid_target, c.kid_weight, c.kid_limit, c.kid_islock, c.kid_author, c.kid_target_ytd
              FROM
              	kpi_item a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_detail c on a.ki_id = c.ki_id
              and DATEPART(
                  year,
                  CONVERT (
                    DATE,
                    c.kid_date,
                    20
                  )
                ) = '".$nowYear."'
              and DATEPART(
                  month,
                  CONVERT (
                    DATE,
                    c.kid_date,
                    20
                  )
                ) = '".$nowMonth."'
                WHERE DATEPART(
                	YEAR,
                	CONVERT (DATE, a.ki_year_date, 20)
                ) = '".$nowYear."'";

      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function deleteHierarchyWhereId($id, $parent = 0)
    {
      if($parent == "0"){
        $del1 = $this->dev_gmf->query("delete kpi_item where ki_id = '".$id."'");
        $del2 = $this->dev_gmf->query("delete kpi_item_detail where ki_id = '".$id."'");
      }
      $sql = "SELECT
              	a.*, b.kf_name, b.fa_function_name, c.kid_id, c.kid_date, c.kid_target, c.kid_weight, c.kid_limit, c.kid_islock, c.kid_author
              FROM
              	kpi_item a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_detail c on a.ki_id = c.ki_id where a.ki_parent_id = '".$id."'";
      $exec = $this->dev_gmf->query($sql);
      $datas = $exec->result();
      foreach ($datas as $data) {
        $del3 = $this->dev_gmf->query("delete kpi_item where ki_id = '".$data->ki_id."'");
        $del4 = $this->dev_gmf->query("delete kpi_item_detail where ki_id = '".$data->ki_id."'");
        $this->deleteHierarchyWhereId($data->ki_id, $parent = $data->ki_id);
      }
    }

    public function delete_kpi()
    {
      $ki_id = $this->input->post("ki_id");
      $del = $this->deleteHierarchyWhereId($ki_id);
      return true;
    }

    public function deleteDetailKpiWhere($ki_id, $year)
    {
      $sql = "delete from kpi_item_detail where ki_id = '".$ki_id."'
              and DATEPART(
                  year,
                  CONVERT (DATE, kid_date, 20)
                ) = '".$year."'";
      $exec = $this->dev_gmf->query($sql);
      return ($exec) ? true : false;
    }

    public function updateDeepChild($ki_id, $year)
    {
      $sql = "update kpi_item set ki_level = '' where ki_id = '".$ki_id."'
              and DATEPART( year, CONVERT (DATE, ki_year_date, 20)) = '".$year."'";
      $exec = $this->dev_gmf->query($sql);
    }

    public function isDeep($ki_id)
    {
      $sql = "select count(ki_id) as total from kpi_item where ki_parent_id = '".$ki_id."'";
      $exec = $this->dev_gmf->query($sql);
      if($exec->row()->total < 1){
        return true;
      } else{
        return false;
      }
    }

    public function setDeepChild($ki_id)
    {
      $sql = "update kpi_item set ki_level = 'deep_child' where ki_id = '".$ki_id."'";
      $exec = $this->dev_gmf->query($sql);
    }

    public function saveKpi()
    {
      $ki_id = $this->input->post("ki_id");
      $ki_name = $this->input->post("ki_name");
      $ki_uom = $this->input->post("ki_uom");
      $ki_parent_id = $this->input->post("ki_parent_id");
      $kf_id = $this->input->post("kf_id");
      $ki_year = $this->input->post("ki_year");
      $ki_author = $this->input->post("ki_author");
      $ki_weight = $this->input->post("ki_weight");
      $ki_target = $this->input->post("ki_target");
      $ki_target_ytd = $this->input->post("ki_target_ytd");
      $kpi_group_id = $this->input->post("kpi_group_id");
      $deep_child = 'deep_child';
      if($ki_parent_id != ""){
        $this->deleteDetailKpiWhere($ki_parent_id, $ki_year);
        $this->updateDeepChild($ki_parent_id, $ki_year);
        $deep_child = 'deep_child';
      }

      if($ki_id == "" || empty($ki_id)){
        $sql = "insert into kpi_item
                (kf_id, ki_author, ki_name, ki_uom, ki_level, ki_year_date, ki_parent_id, ki_weight, ki_target, ki_target_ytd, kpi_group_id)
                values
                ('".$kf_id."', '".$ki_author."', '".$ki_name."', '".$ki_uom."', '".$deep_child."', '".$ki_year."', '".$ki_parent_id."', '".$ki_weight."', '".$ki_target."', '".$ki_target_ytd."', '".$kpi_group_id."')";
        $sql2 = "";
     } else {
      $sql = "update kpi_item set ki_name = '".$ki_name."', ki_parent_id = '".$ki_parent_id."', ki_weight = '".$ki_weight."', ki_target = '".$ki_target."', ki_target_ytd = '".$ki_target_ytd."', kpi_group_id = '".$kpi_group_id."' where ki_id = '".$ki_id."'";
      $sql2 = "update kpi_item_assignment set kia_name = '".$ki_name."' where ki_id = '".$ki_id."'";
     }
     $dataEdit = $this->edit($ki_id);
     $old_parent = (!empty($dataEdit->ki_parent_id) ? $dataEdit->ki_parent_id : null);
     if ($sql2 != "") {
        $exec1 = $this->dev_gmf->query($sql2);
     }
       $exec = $this->dev_gmf->query($sql);
     if($old_parent != null){
       if($this->isDeep($old_parent) == true){
         $this->setDeepChild($old_parent);
       }
     }
     $allItem = $this->getAllItemKpi($ki_year);
     $result = array(
       "success" => ($exec) ? true : false,
       "isparent" => ($ki_parent_id == "") ? true : false,
       "newData" => generatePageTree($allItem),
       "error" => []
     );
     return $result;
    }

    public function edit($id)
    {
      $sql = "select * from kpi_item where ki_id = '".$id."'";
      $exec = $this->dev_gmf->query($sql);
      return $exec->row();
    }

    function copy_kpi($array, $parentId = 0)
    {
        $copy_year2 = $this->input->post("copy_year2");
        foreach ($array as $key => $value) {
          $post_data = array(
            "kf_id" => $value->kf_id,
            "ki_author" => $value->ki_author,
            "ki_level" => $value->ki_level,
            "ki_name" => $value->ki_name,
            "ki_parent_id" => $parentId,
            "ki_target" => $value->ki_target,
            "ki_uom" => $value->ki_uom,
            "ki_weight" => $value->ki_weight,
            "kpi_group_id"=>$value->kpi_group_id,
            "ki_target_ytd"=>$value->ki_target_ytd,
            "ki_year_date" => $copy_year2."-01-01"
          );
           $this->dev_gmf->insert('kpi_item', $post_data);
            if(isset($value->children)){
              $this->copy_kpi($value->children, $this->dev_gmf->insert_id());
            }
          }
      }

    public function select_kpi_where_year($year){
      $sql = "select
                ki_id,
                kf_id,
                ki_author,
                ki_name,
                ki_uom,
                ki_level,
                ki_year_date,
                ki_parent_id,
                ki_weight,
                ki_target,
                kpi_group_id,
                ki_target_ytd
              from kpi_item
              where DATEPART(
                YEAR,
                CONVERT (DATE, ki_year_date, 20)
              ) = '".$year."'";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function saveDetailItem()
    {
      $year = $this->input->post("year_input");
      $ki_id = $this->input->post("ki_id");
      $author = $this->input->post("author_detail");
      $target = $this->input->post("target");
      $target_ytd = $this->input->post("target_ytd");
      $upper = $this->input->post("upper");
      $lower = $this->input->post("lower");
      $upper_ytd = $this->input->post("upper_ytd");
      $lower_ytd = $this->input->post("lower_ytd");
      $weight = $this->input->post("weight");
      $limit = $this->input->post("limit");
      $kid_id = $this->input->post("kid_id");
      $ki_uom = $this->input->post("ki_uom");

      $result = array();
      $no = 1;
      foreach ($limit as $i => $data) {
        if(empty($kid_id)){
          $target_final = ($upper[$i] != null && $lower[$i] != null) ? $upper[$i]."|".$lower[$i] : $target[$i];
          $target_final_ytd = ($upper_ytd[$i] != null && $lower_ytd[$i] != null) ? $upper_ytd[$i]."|".$lower_ytd[$i] : $target_ytd[$i];
          $sql = "insert into kpi_item_detail(ki_id, kid_date, kid_target, kid_weight, kid_limit, kid_author, kid_target_ytd)
                  values('".$ki_id."', '".$year."-".$no."-01', '".$target_final."', '".$weight[$i]."', '".$limit[$i]."', '".$author."', '".$target_final_ytd."')";
          $exec = $this->dev_gmf->query($sql);
          $no++;
        } else {
          $target_final = ($upper[$i] != null && $lower[$i] != null) ? $upper[$i]."|".$lower[$i] : $target[$i];
          $target_final_ytd = ($upper_ytd[$i] != null && $lower_ytd[$i] != null) ? $upper_ytd[$i]."|".$lower_ytd[$i] : $target_ytd[$i];
          $sql = "update kpi_item_detail set
                  kid_target = '".$target_final."',
                  kid_target_ytd = '".$target_final_ytd."',
                  kid_weight = '".$weight[$i]."',
                  kid_limit = '".$limit[$i]."'
                  where kid_id = '".$kid_id[$i]."'";
          $exec = $this->dev_gmf->query($sql);
        }
      }
      return true;
    }

    public function getDetailWhere($ki_id, $year)
    {
      $sql = "SELECT
              	*,
                DATEPART(
                  	month,
                  	CONVERT (DATE, kid_date, 20)
                  ) as month
              FROM
              	kpi_item_detail
              WHERE
              	ki_id = '".$ki_id."'
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, kid_date, 20)
              ) = '".$year."'";
     $exec = $this->dev_gmf->query($sql);
     return $exec->result();
    }

    public function saveGroup()
    {
      $kpi_group_name = $this->input->post("kpi_group_name");
      $kpi_group_id = $this->input->post("kpi_group_id");
      if($kpi_group_id != ""){
        $sql = "update kpi_grup set kpi_group_name = '".$kpi_group_name."' where kpi_group_id = '".$kpi_group_id."'";
      } else {
        $sql = "insert into kpi_grup (kpi_group_name) values ('".$kpi_group_name."')";
      }
      $exec = $this->dev_gmf->query($sql);
      return ($exec) ? true : false;
    }

    public function loadGrup()
    {
      $sql = "select * from kpi_grup";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function loadGrupWhere($id)
    {
      $sql = "select * from kpi_grup where kpi_group_id = '".$id."'";
      $exec = $this->dev_gmf->query($sql);
      return $exec->row();
    }

    public function member_group_datatable_where($data)
    {
      $total = $this->dev_gmf->count_all_results("kpi_item");
      $this->dev_gmf->where("kpi_group_id", $data["id"]);
      $otuput = array();
      $output["draw"] = $data["draw"];
      $output["recordsTotal"] = $output["recordsFiltered"] = $total;
      $output["data"] = array();
      if($data["search"] != ""){
        $this->dev_gmf->or_like("ki_name", $data["search"]);
      }
      $this->dev_gmf->limit($data["length"], $data["start"]);
      $this->dev_gmf->order_by($data["order_column"], $data["order_dir"]);
      $query = $this->dev_gmf->get("kpi_item");
      if($data["search"] != ""){
        $this->dev_gmf->or_like("ki_name", $data["search"]);
        $jum = $this->dev_gmf->get("kpi_item");
        $output["recordsTotal"] = $output["recordsFiltered"] = $jum->num_rows();
      }
      $nomor_urut = $data["start"]+1;
      foreach ($query->result_array() as $kpi) {
        $output["data"][] = array(
          $nomor_urut,
          $kpi["ki_id"],
          $kpi["ki_name"]
        );
      $nomor_urut++;
      }
      echo json_encode($output, JSON_PRETTY_PRINT);
    }

    public function member_group_where($id, $thn)
    {
      $sql = "select ki_id, (ki_name+' - '+CONVERT(VARCHAR, YEAR(ki_year_date))) as ki_name from kpi_item where kpi_group_id = '".$id."' AND YEAR(ki_year_date) = '$thn' ORDER BY ki_year_date DESC";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function get_all_parent($thn)
    {
      $sql = "SELECT
              	ki_id,
              	(
              		ki_name + ' - ' + CONVERT (VARCHAR, YEAR(ki_year_date))
              	) AS ki_name
              FROM
              	kpi_item
              WHERE
              	(
              		ki_parent_id = '0'
              		OR ki_parent_id IS NULL
              	)
              AND (kpi_group_id IS NULL or kpi_group_id = '0')
              AND YEAR(ki_year_date) = '$thn'
              ORDER BY
              	ki_year_date DESC";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function set_member_where($ki_id, $kpi_group_id)
    {
      $sql = "update kpi_item set kpi_group_id = '".$ki_id."' where ki_id = '".$kpi_group_id."'";
      $exec = $this->dev_gmf->query($sql);
      return ($exec) ? true : false;
    }

    public function remove_member_where($id)
    {
      $sql = "update kpi_item set kpi_group_id = null where ki_id = '".$id."'";
      $exec = $this->dev_gmf->query($sql);
      return ($exec) ? true : false;
    }

    public function delete_group_where($id)
    {
      $sql1 = "update kpi_item set kpi_group_id = null where kpi_group_id = '".$id."'";
      $exec = $this->dev_gmf->query($sql1);
      if($exec){
        $sql2 = "delete kpi_grup where kpi_group_id = '".$id."'";
        $exec2 = $this->dev_gmf->query($sql2);
        return ($exec2) ? true : false;
      }
      return false;
    }

    public function get_unit($kpi_unit_id)
    {
      $sql = "select * from kpi_unit where kpi_unit_id = '".$kpi_unit_id."'";
      $exec = $this->dev_gmf->query($sql);
      $count = count($exec);
      return (($count > 0) ? $exec->row() : null);
    }


    public function getScoreBapak($id, $month, $nowYear, $depth){
     
      $sql = "SELECT
                a.ki_id,
                SUM (e.kiad_actual) / COUNT (a.ki_id) AS kiad_actual,
                a.ki_name,
                a.ki_uom,
                a.ki_year_date,
                a.ki_parent_id,
                a.ki_weight,
                b.kf_name,
                b.fa_function_name,
                c.kid_id,
                c.kid_date,
                c.kid_target,
                c.kid_weight,
                c.kid_limit,
                c.kid_islock,
                c.kid_author
              FROM
                kpi_item a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_detail c ON a.ki_id = c.ki_id
              AND DATEPART(
                YEAR,
                CONVERT (DATE, c.kid_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
                MONTH,
                CONVERT (DATE, c.kid_date, 20)
              ) = '".$month."'
              LEFT JOIN kpi_item_assignment d ON a.ki_id = d.ki_id
              LEFT JOIN kpi_item_assigment_detail e ON d.kia_id = e.kia_id
              AND DATEPART(
                YEAR,
                CONVERT (DATE, e.kiad_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
                MONTH,
                CONVERT (DATE, e.kiad_date, 20)
              ) = '".$month."'
              where a.ki_parent_id = '".$id."'
              GROUP BY
                a.ki_id, 	a.ki_name,
                a.ki_uom,
                a.ki_year_date,
                a.ki_parent_id,
                a.ki_weight,
                b.kf_name,
                b.fa_function_name,
                c.kid_id,
                c.kid_date,
                c.kid_target,
                c.kid_weight,
                c.kid_limit,
                c.kid_islock,
                c.kid_author";
        // echo "<pre>".$sql."</pre>";
        $exec = $this->dev_gmf->query($sql);
        $result = $exec->result();
        $total = 0;
        foreach ($result as $data) {

          $weight = "";
          if($depth == "0"){
            $weight = $this->countWeight($data->ki_id, $nowYear, $month);
          } else {
            if(empty($data->kid_weight)){
              $weight  = $data->ki_weight;
            } else{
              $weight = $data->kid_weight;
            }
          }
          $ach = number_format(achievment( $data->kid_target, $data->kiad_actual, $data->fa_function_name), 2);
          $total += score($ach, $data->kid_limit, $weight);
          // $total += $this->getTotalScoreWhere($data->ki_id, $month, $nowYear, $depth+1);
        }
        // exit();
        return number_format($total, 2);
    }


    public function getPenghuni($level,$id)
    {
      # code...
      $sql="WITH rCTE AS(
                  SELECT *, 0 AS Level FROM kpi_item WHERE ki_id = '".$id."'
                  UNION ALL
                  SELECT t.*, r.Level + 1 AS Level
                  FROM kpi_item t
                  INNER JOIN rCTE r
                      ON t.ki_parent_id = r.ki_id
              )
              SELECT level as ujung,ki_id,ki_name,ki_parent_id,ki_weight FROM rCTE WHERE level ='".$level."'";
      $sikat=$this->dev_gmf->query($sql);
      return $sikat->result_array();  
    }

    public function getPenghuni_unit($level,$id)
    {
      # code...
      $sql="WITH rCTE AS(
                  SELECT *, 0 AS Level FROM kpi_item_assignment WHERE kia_id = '".$id."'
                  UNION ALL
                  SELECT t.*, r.Level + 1 AS Level
                  FROM kpi_item_assignment t
                  INNER JOIN rCTE r
                      ON t.kia_parent_id = r.kia_id
              )
              SELECT level as ujung,kia_id,kia_name,kia_parent_id,kia_weight FROM rCTE WHERE level ='".$level."'";
      $sikat=$this->dev_gmf->query($sql);
      return $sikat->result_array();  
    }


    public function getGroupLevelParent($level,$id)
    {
      # code...
      $sql="WITH rCTE AS(
                  SELECT *, 0 AS Level FROM kpi_item WHERE ki_id = '".$id."'
                  UNION ALL
                  SELECT t.*, r.Level + 1 AS Level
                  FROM kpi_item t
                  INNER JOIN rCTE r
                      ON t.ki_parent_id = r.ki_id
              )
               SELECT level as ujung,ki_parent_id,count(ki_parent_id) as jml FROM rCTE WHERE level ='".$level."' GROUP BY level,ki_parent_id";
      $sikat=$this->dev_gmf->query($sql);
      return $sikat->result_array();  
    }

    public function getGroupLevelParent_unit($level,$id)
    {
      # code...
      $sql="WITH rCTE AS(
                  SELECT *, 0 AS Level FROM kpi_item_assignment WHERE kia_id = '".$id."'
                  UNION ALL
                  SELECT t.*, r.Level + 1 AS Level
                  FROM kpi_item_assignment t
                  INNER JOIN rCTE r
                      ON t.kia_parent_id = r.kia_id
              )
               SELECT level as ujung,kia_parent_id,count(kia_parent_id) as jml FROM rCTE WHERE level ='".$level."' GROUP BY level,kia_parent_id";
      $sikat=$this->dev_gmf->query($sql);
      return $sikat->result_array();  
    }


    public function getZerosParent($year)
    {
      # code...
      $sql="SELECT
              *
            FROM
              [dbo].[kpi_item]
            WHERE
              ki_parent_id = 0
            AND
              DATEPART(
                YEAR,
                CONVERT (DATE, ki_year_date, 20)
              ) = '".$year."'";
      $sikat=$this->dev_gmf->query($sql);
      return $sikat->result_array();  
    }

     public function getZerosParent_unit($year,$roles)
    {
      # code...
      $sql="SELECT
              *
            FROM
              [dbo].[kpi_item_assignment]
            WHERE
              kia_parent_id = 0
              AND role_id= '".$roles."'
            AND
              DATEPART(
                YEAR,
                CONVERT (DATE, kia_year_date, 20)
              ) = '".$year."'";
      $sikat=$this->dev_gmf->query($sql);
      return $sikat->result_array();  
    }
    

  }
