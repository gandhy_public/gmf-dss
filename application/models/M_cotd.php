<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_cotd extends CI_Model{

  var $table = 'tbl_all_target';
  var $column_order = array('target_id','target_val','target_year', 'target_type', 'target_aircraft', null);
  var $column_search = array('target_id','target_val','target_year');
  var $order = array('target_val' => 'asc');

  private function _get_datatables_query(){

    $this->db->select('tbl_all_target.target_id,
                       tbl_all_target.target_val,
                       tbl_all_target.target_year,
                       tbl_all_target.target_type,
                       tbl_all_target.target_aircraft'
                       );
    $this->db->from($this->table);
    $i = 0;

    foreach ($this->column_search as $item)
    {
      if(!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order']))
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function _emptyData($year, $type, $aircraft){
      $this->db->from($this->table);
      $this->db->where('target_year', $year);
      $this->db->where('target_type', $type);
      $this->db->where('target_aircraft', $aircraft);
      $query = $this->db->get();

      return $query->result();
    }

  function get_alldata()
  {
    $this->_get_datatables_query();
    $data = $this->db->get();

    return $data->result();
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('target_id',$id);
    $query = $this->db->get();

    return $query->row();
  }

  public function get_val_garuda($aircraft){
    $this->db->from($this->table);
    $this->db->where('target_type', 'COTD');
    $this->db->where('target_aircraft',$aircraft);
    $query = $this->db->get();

    return $query->row();
  }

  public function get_val_citylink($aircraft){
    $this->db->from($this->table);
    $this->db->where('target_type', 'COTD');
    $this->db->where('target_aircraft',$aircraft);
    $query = $this->db->get();

    return $query->row();
  }

  public function save($data)
  {
    $this->db->insert($this->table, $data);

    $id = $this->db->insert_id();
    return (isset($id)) ? $id : FALSE;
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->where('target_id', $id);
    $this->db->delete($this->table);
  }

  function get_alldata_mitigasi()
  {
    $this->db->from("tbl_mitigasi");
    $this->db->where('mitigasi_type',"cotd");
    $data = $this->db->get();

    return $data->result();
  }

  public function get_mitigasi_by_id($id)
  {
    $this->db->from("tbl_mitigasi");
    $this->db->where('mitigasi_id',$id);
    $query = $this->db->get();

    return $query->row();
  }

  public function save_mitigasi($data)
  {
    $this->db->insert("tbl_mitigasi", $data);

    $id = $this->db->insert_id();
    return (isset($id)) ? $id : FALSE;
  }

  public function update_mitigasi($where, $data)
  {
    $this->db->update("tbl_mitigasi", $data, $where);
    return $this->db->affected_rows();
  }

}
