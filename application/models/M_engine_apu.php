
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_engine_apu extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    $this->SQL_server=$this->load->database('dev_gmf', true);
  }


  public function get_data_availability($data1, $data2)
  {
    // code...
    $query=$this->SQL_server->query("SELECT * FROM
        (SELECT SUM(unrestricted) AS grd_engine_avabiliy FROM (
          SELECT
            DISTINCT
            ex_long_mat_numb,
            material_desc,
            tbl_engine_apu.plant,
            tbl_engine_apu.storage_loc,
            tbl_engine_apu_dtl.category,
            unrestricted
          FROM
            [dbo].[tbl_engine_apu]
            LEFT JOIN tbl_engine_apu_dtl ON tbl_engine_apu.ex_long_mat_numb = tbl_engine_apu_dtl.mat_number
            AND tbl_engine_apu.plant = tbl_engine_apu_dtl.plant
            AND tbl_engine_apu.storage_loc = tbl_engine_apu_dtl.storage_loc
          WHERE
            tbl_engine_apu_dtl.category = 'GA STOCK'
            AND tbl_engine_apu.storage_loc = '1000'
            AND tbl_engine_apu.unrestricted != '0'
            -- AND (
            -- tbl_engine_apu.material_desc LIKE '%ENGINE%'
            -- AND tbl_engine_apu.material_desc NOT LIKE '%APU%')
            AND tbl_engine_apu.ex_long_mat_numb in ('$data1')
          )A)A ,

        (SELECT SUM(unrestricted) AS grd_engine_navability FROM (
          SELECT
            DISTINCT
            ex_long_mat_numb,
            material_desc,
            tbl_engine_apu.plant,
            tbl_engine_apu.storage_loc,
            tbl_engine_apu_dtl.category,
            unrestricted
          FROM
            [dbo].[tbl_engine_apu]
            LEFT JOIN tbl_engine_apu_dtl ON tbl_engine_apu.ex_long_mat_numb = tbl_engine_apu_dtl.mat_number
            AND tbl_engine_apu.plant = tbl_engine_apu_dtl.plant
            AND tbl_engine_apu.storage_loc = tbl_engine_apu_dtl.storage_loc
          WHERE
            tbl_engine_apu_dtl.category = 'GA STOCK'
            AND tbl_engine_apu.storage_loc != '1000'
            AND tbl_engine_apu.unrestricted != '0'
            -- AND (
            -- tbl_engine_apu.material_desc LIKE '%ENGINE%'
            -- AND tbl_engine_apu.material_desc NOT LIKE '%APU%')
            AND tbl_engine_apu.ex_long_mat_numb in ('$data1')
          )A)B,

        (SELECT SUM(unrestricted) AS grd_apu_avability FROM (
          SELECT
            DISTINCT
            ex_long_mat_numb,
            material_desc,
            tbl_engine_apu.plant,
            tbl_engine_apu.storage_loc,
            tbl_engine_apu_dtl.category,
            unrestricted
          FROM
            [dbo].[tbl_engine_apu]
            LEFT JOIN tbl_engine_apu_dtl ON tbl_engine_apu.ex_long_mat_numb = tbl_engine_apu_dtl.mat_number
            AND tbl_engine_apu.plant = tbl_engine_apu_dtl.plant
            AND tbl_engine_apu.storage_loc = tbl_engine_apu_dtl.storage_loc
          WHERE
            tbl_engine_apu_dtl.category = 'GA STOCK'
            AND tbl_engine_apu.storage_loc = '1000'
            AND tbl_engine_apu.unrestricted != '0'
            -- AND tbl_engine_apu.material_desc LIKE '%APU%'
            AND tbl_engine_apu.ex_long_mat_numb in ('$data2')
          )A)C,

        (SELECT SUM(unrestricted) AS grd_apu_navability FROM (
          SELECT
            DISTINCT
            ex_long_mat_numb,
            material_desc,
            tbl_engine_apu.plant,
            tbl_engine_apu.storage_loc,
            tbl_engine_apu_dtl.category,
            unrestricted
          FROM
            [dbo].[tbl_engine_apu]
            LEFT JOIN tbl_engine_apu_dtl ON tbl_engine_apu.ex_long_mat_numb = tbl_engine_apu_dtl.mat_number
            AND tbl_engine_apu.plant = tbl_engine_apu_dtl.plant
            AND tbl_engine_apu.storage_loc = tbl_engine_apu_dtl.storage_loc
          WHERE
            tbl_engine_apu_dtl.category = 'GA STOCK'
            AND tbl_engine_apu.storage_loc != '1000'
            AND tbl_engine_apu.unrestricted != '0'
            -- AND tbl_engine_apu.material_desc LIKE '%APU%'
            AND tbl_engine_apu.ex_long_mat_numb in ('$data2')
          )A)D,

        (SELECT SUM(unrestricted) AS ct_engine_avability FROM (
          SELECT
            DISTINCT
            ex_long_mat_numb,
            material_desc,
            tbl_engine_apu.plant,
            tbl_engine_apu.storage_loc,
            tbl_engine_apu_dtl.category,
            unrestricted
          FROM
            [dbo].[tbl_engine_apu]
            LEFT JOIN tbl_engine_apu_dtl ON tbl_engine_apu.ex_long_mat_numb = tbl_engine_apu_dtl.mat_number
            AND tbl_engine_apu.plant = tbl_engine_apu_dtl.plant
            AND tbl_engine_apu.storage_loc = tbl_engine_apu_dtl.storage_loc
          WHERE
            tbl_engine_apu_dtl.category = 'CITILINK'
            AND tbl_engine_apu.storage_loc = '1000'
            AND tbl_engine_apu.unrestricted != '0'
            -- AND (tbl_engine_apu.material_desc LIKE '%ENGINE%'
            -- AND tbl_engine_apu.material_desc NOT LIKE '%APU%')
            AND tbl_engine_apu.ex_long_mat_numb in ('$data1')
          )A)E ,

        (SELECT SUM(unrestricted) AS ct_engine_navability FROM (
          SELECT
            DISTINCT
            ex_long_mat_numb,
            material_desc,
            tbl_engine_apu.plant,
            tbl_engine_apu.storage_loc,
            tbl_engine_apu_dtl.category,
            unrestricted
          FROM
            [dbo].[tbl_engine_apu]
            LEFT JOIN tbl_engine_apu_dtl ON tbl_engine_apu.ex_long_mat_numb = tbl_engine_apu_dtl.mat_number
            AND tbl_engine_apu.plant = tbl_engine_apu_dtl.plant
            AND tbl_engine_apu.storage_loc = tbl_engine_apu_dtl.storage_loc
          WHERE
            tbl_engine_apu_dtl.category = 'CITILINK'
            AND tbl_engine_apu.storage_loc != '1000'
            AND tbl_engine_apu.unrestricted != '0'
            -- AND (tbl_engine_apu.material_desc LIKE '%ENGINE%'
            -- AND tbl_engine_apu.material_desc NOT LIKE '%APU%')
            AND tbl_engine_apu.ex_long_mat_numb in ('$data1')
          )A)F,

        (SELECT SUM(unrestricted) AS ct_apu_avability FROM (
          SELECT
            DISTINCT
            ex_long_mat_numb,
            material_desc,
            tbl_engine_apu.plant,
            tbl_engine_apu.storage_loc,
            tbl_engine_apu_dtl.category,
            unrestricted
          FROM
            [dbo].[tbl_engine_apu]
            LEFT JOIN tbl_engine_apu_dtl ON tbl_engine_apu.ex_long_mat_numb = tbl_engine_apu_dtl.mat_number
            AND tbl_engine_apu.plant = tbl_engine_apu_dtl.plant
            AND tbl_engine_apu.storage_loc = tbl_engine_apu_dtl.storage_loc
          WHERE
            tbl_engine_apu_dtl.category = 'CITILINK'
            AND tbl_engine_apu.storage_loc = '1000'
            AND tbl_engine_apu.unrestricted != '0'
            -- AND tbl_engine_apu.material_desc LIKE '%APU%'
            AND tbl_engine_apu.ex_long_mat_numb in ('$data2')
          )A)G,

        (SELECT SUM(unrestricted) AS ct_apu_navability FROM (
          SELECT
            DISTINCT
            ex_long_mat_numb,
            material_desc,
            tbl_engine_apu.plant,
            tbl_engine_apu.storage_loc,
            tbl_engine_apu_dtl.category,
            unrestricted
          FROM
            [dbo].[tbl_engine_apu]
            LEFT JOIN tbl_engine_apu_dtl ON tbl_engine_apu.ex_long_mat_numb = tbl_engine_apu_dtl.mat_number
            AND tbl_engine_apu.plant = tbl_engine_apu_dtl.plant
            AND tbl_engine_apu.storage_loc = tbl_engine_apu_dtl.storage_loc
          WHERE
            tbl_engine_apu_dtl.category = 'CITILINK'
            AND tbl_engine_apu.storage_loc != '1000'
            AND tbl_engine_apu.unrestricted != '0'
            -- AND tbl_engine_apu.material_desc LIKE '%APU%'
            AND tbl_engine_apu.ex_long_mat_numb in ('$data2')
          )A)H
        ");
    return $query->result_array();
  }

  public function get_data_searchby($data)
  {
    // code...
    $query=$this->SQL_server->query("SELECT * FROM (SELECT
                                    	SUM(unrestricted) AS grd_engine_avabiliy
                                      FROM
                                        [dbo].[tbl_engine_apu] ea
                                        LEFT JOIN [dbo].[tbl_engine_apu_dtl] ead ON ea.ex_long_mat_numb = ead.mat_number
                                      AND ea.plant = ead.plant
                                      AND ea.storage_loc = ead.storage_loc
                                    where
                                    ea.storage_loc = '1000'
                                    AND
                                    ea.material_desc like '%ENGINE%'
                                    AND
                                    ea.ex_long_mat_numb in ('$data') AND ead.category='GA STOCK')A ,

                                    (SELECT
                                    	SUM(unrestricted) AS grd_engine_navability
                                      FROM
                                        [dbo].[tbl_engine_apu] ea
                                        LEFT JOIN [dbo].[tbl_engine_apu_dtl] ead ON ea.ex_long_mat_numb = ead.mat_number
                                      AND ea.plant = ead.plant
                                      AND ea.storage_loc = ead.storage_loc
                                    where
                                    ea.storage_loc != '1000'
                                    AND
                                    ea.material_desc like '%ENGINE%'
                                    AND
                                    ea.ex_long_mat_numb in ('$data')AND ead.category='GA STOCK')B,

                                    (SELECT
                                    	SUM(unrestricted) AS grd_apu_avability
                                      FROM
                                        [dbo].[tbl_engine_apu] ea
                                        LEFT JOIN [dbo].[tbl_engine_apu_dtl] ead ON ea.ex_long_mat_numb = ead.mat_number
                                      AND ea.plant = ead.plant
                                      AND ea.storage_loc = ead.storage_loc
                                    where
                                    ea.storage_loc = '1000'
                                    AND
                                    ea.material_desc like '%APU%'
                                    AND
                                    ea.ex_long_mat_numb in ('$data') AND ead.category='GA STOCK')C,

                                    (SELECT
                                    	SUM(unrestricted) AS grd_apu_navability
                                      FROM
                                        [dbo].[tbl_engine_apu] ea
                                        LEFT JOIN [dbo].[tbl_engine_apu_dtl] ead ON ea.ex_long_mat_numb = ead.mat_number
                                      AND ea.plant = ead.plant
                                      AND ea.storage_loc = ead.storage_loc
                                    where
                                    ea.storage_loc != '1000'
                                    AND
                                    ea.material_desc like '%APU%'
                                    AND
                                    ea.ex_long_mat_numb in ('$data') AND ead.category='GA STOCK')D,

                                    (SELECT
                                    	SUM(unrestricted) AS ct_engine_avability
                                      FROM
                                        [dbo].[tbl_engine_apu] ea
                                        LEFT JOIN [dbo].[tbl_engine_apu_dtl] ead ON ea.ex_long_mat_numb = ead.mat_number
                                      AND ea.plant = ead.plant
                                      AND ea.storage_loc = ead.storage_loc
                                    where
                                    ea.storage_loc = '1000'
                                    AND
                                    ea.material_desc like '%ENGINE%'
                                    AND
                                    ea.ex_long_mat_numb in ('$data') AND ead.category='CITILINK')E ,

                                    (SELECT
                                    	SUM(unrestricted) AS ct_engine_navability
                                      FROM
                                        [dbo].[tbl_engine_apu] ea
                                        LEFT JOIN [dbo].[tbl_engine_apu_dtl] ead ON ea.ex_long_mat_numb = ead.mat_number
                                      AND ea.plant = ead.plant
                                      AND ea.storage_loc = ead.storage_loc
                                    where
                                    ea.storage_loc != '1000'
                                    AND
                                    ea.material_desc like '%ENGINE%'
                                    AND
                                    ea.ex_long_mat_numb in ('$data') AND ead.category='CITILINK')F,

                                    (SELECT
                                    	SUM(unrestricted) AS ct_apu_avability
                                      FROM
                                        [dbo].[tbl_engine_apu] ea
                                        LEFT JOIN [dbo].[tbl_engine_apu_dtl] ead ON ea.ex_long_mat_numb = ead.mat_number
                                      AND ea.plant = ead.plant
                                      AND ea.storage_loc = ead.storage_loc
                                    where
                                    ea.storage_loc = '1000'
                                    AND
                                    ea.material_desc like '%APU%'
                                    AND
                                    ea.ex_long_mat_numb in ('$data') AND ead.category='CITILINK')G,

                                    (SELECT
                                    	SUM(unrestricted) AS ct_apu_navability
                                      FROM
                                        [dbo].[tbl_engine_apu] ea
                                        LEFT JOIN [dbo].[tbl_engine_apu_dtl] ead ON ea.ex_long_mat_numb = ead.mat_number
                                      AND ea.plant = ead.plant
                                      AND ea.storage_loc = ead.storage_loc
                                    where
                                    ea.storage_loc != '1000'
                                    AND
                                    ea.material_desc like '%APU%'
                                    AND
                                    ea.ex_long_mat_numb in ('$data') AND ead.category='CITILINK')H");
    return $query->result_array();
  }

  public function detail_material($param1,$param2,$data)
  {
    // code...
    $query=$this->SQL_server->select('ex_long_mat_numb,material_desc,plant,storage_loc,desc_storage_loc,unrestricted')
                            ->from('tbl_engine_apu')
                            ->where('storage_lsoc !=', $param2,FALSE)
                            ->where('unrestricted !=',0,FALSE)
                            ->where('material_desc',$param1)
                            ->where_in('ex_long_mat_numb',array('WE3800770-3:99193'))
                            ->like($data)
                            ->get();
  return $query->result_array();
  }

  public function material_number_engine()
  {
    // code...
    $query=$this->SQL_server->distinct('ex_long_mat_numb')
                            ->select('ex_long_mat_numb')
                            ->from('tbl_engine_apu')
                            ->like('ex_long_mat_numb','CFM56-3')
                            ->or_like('ex_long_mat_numb','CFM56-5')
                            ->or_like('ex_long_mat_numb','CFM56-7')
                            ->or_like('ex_long_mat_numb','TRENT')                            
                            ->or_like('ex_long_mat_numb','PW127M')
                            ->or_like('ex_long_mat_numb','GE90')
                            ->or_like('ex_long_mat_numb','CF6-80')
                            ->or_like('ex_long_mat_numb','CF34_85C5A1')
                            ->order_by('ex_long_mat_numb','ASC')
                            ->get();
    return $query->result_array()                            ;
  }

    public function material_number_apu()
  {
    // code...
    $query=$this->SQL_server->distinct('ex_long_mat_numb')
                            ->select('ex_long_mat_numb')
                            ->from('tbl_engine_apu')
                            ->like('ex_long_mat_numb','3800454-3')
                            ->or_like('ex_long_mat_numb','3800708-1')
                            ->or_like('ex_long_mat_numb','3800550-1')
                            ->or_like('ex_long_mat_numb','3800702-1')
                            ->or_like('ex_long_mat_numb','3800454-5')
                            ->or_like('ex_long_mat_numb','3800454-6')
                            ->or_like('ex_long_mat_numb','3800454-2')
                            ->or_like('ex_long_mat_numb','3800454-4')
                            ->or_like('ex_long_mat_numb','3800454-1')
                            ->or_like('ex_long_mat_numb','WE3800770-3')
                            ->order_by('ex_long_mat_numb','ASC')
                            ->get();
    return $query->result_array()                            ;
  }

  public function detail_nav($tipe,$customer,$data)
  {
    // code...
    $this->SQL_server->select('DISTINCT (ea.ex_long_mat_numb),ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');
    $this->SQL_server->from('tbl_engine_apu ea');
    $this->SQL_server->join('tbl_engine_apu_dtl ead','ea.ex_long_mat_numb = ead.mat_number AND ea.plant = ead.plant AND ea.storage_loc = ead.storage_loc','left');
    $this->SQL_server->where('ead.category ',$customer);
    $this->SQL_server->where('ea.storage_loc !=','1000');
    // if($tipe == 'ENGINE') {
    //   $this->SQL_server->not_like('ea.material_desc', 'APU');
    // }
    // $this->SQL_server->like('ea.material_desc', $tipe);
    $this->SQL_server->where('ea.unrestricted !=',0);
    $this->SQL_server->group_by('ea.ex_long_mat_numb,ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');

     if ($data['search']!='') {
          // code...
          $this->SQL_server->where("(ea.ex_long_mat_numb like '%".$data['search']."%' OR
                    ea.material_desc like '%".$data['search']."%' OR
                    ea.material_type LIKE  '%".$data['search']."%' OR
                    ea.plant LIKE  '%".$data['search']."%' OR
                    ea.storage_loc LIKE  '%".$data['search']."%' OR
                    ea.desc_storage_loc LIKE  '%".$data['search']."%' OR
                    ea.unrestricted LIKE '%".$data['search']."%')");
        }
    return $this->SQL_server->get()->result_array();
  }

  public function detail_av($tipe,$customer,$data)
  {
    $this->SQL_server->select('DISTINCT (ea.ex_long_mat_numb),ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');
    $this->SQL_server->from('tbl_engine_apu ea');
    $this->SQL_server->join('tbl_engine_apu_dtl ead','ea.ex_long_mat_numb = ead.mat_number AND ea.plant = ead.plant AND ea.storage_loc = ead.storage_loc','left');
    $this->SQL_server->where('ead.category',$customer);
    $this->SQL_server->where('ea.storage_loc','1000');
    $this->SQL_server->where('ead.category ',$customer);
    // if($tipe == 'ENGINE') {
    //   $this->SQL_server->not_like('ea.material_desc', 'APU');
    // }
    // $this->SQL_server->like('ea.material_desc', $tipe);
    $this->SQL_server->where('ea.unrestricted !=',0);
    // $this->SQL_server->group_by('ea.ex_long_mat_numb,ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');

     if ($data['search']!='') {
          // code...
          $this->SQL_server->where("(ea.ex_long_mat_numb like '%".$data['search']."%' OR
                    ea.material_desc like '%".$data['search']."%' OR
                    ea.material_type LIKE  '%".$data['search']."%' OR
                    ea.plant LIKE  '%".$data['search']."%' OR
                    ea.storage_loc LIKE  '%".$data['search']."%' OR
                    ea.desc_storage_loc LIKE  '%".$data['search']."%' OR
                    ea.unrestricted LIKE '%".$data['search']."%')");
        }
    $this->SQL_server->where('category',$customer);
    return $this->SQL_server->get()->result_array();
  }

  public function count_detail_av($tipe,$customer,$data)
  {
    // code...

    // $this->SQL_server->select('ea.ex_long_mat_numb,ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');
    // $this->SQL_server->from('tbl_engine_apu ea');
    // $this->SQL_server->join('tbl_engine_apu_dtl ead','ea.ex_long_mat_numb = ead.mat_number AND ea.plant = ead.plant AND ea.storage_loc = ead.storage_loc','left');
    // $this->SQL_server->where('ead.category',$customer);
    // $this->SQL_server->where('ea.storage_loc','1000');
    // $this->SQL_server->where('ea.material_desc',$tipe);
    // $this->SQL_server->where('ea.unrestricted !=',0);
    //  if ($data['search']!='') {
    //       // code...
    //       $this->SQL_server->where("(ea.ex_long_mat_numb like '%".$data['search']."%' OR
    //                 ea.material_desc like '%".$data['search']."%' OR
    //                 ea.material_type LIKE  '%".$data['search']."%' OR
    //                 ea.plant LIKE  '%".$data['search']."%' OR
    //                 ea.storage_loc LIKE  '%".$data['search']."%' OR
    //                 ea.desc_storage_loc LIKE  '%".$data['search']."%' OR
    //                 ea.unrestricted LIKE '%".$data['search']."%')");
    //     }

    $this->SQL_server->select('DISTINCT (ea.ex_long_mat_numb),ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');
    $this->SQL_server->from('tbl_engine_apu ea');
    $this->SQL_server->join('tbl_engine_apu_dtl ead','ea.ex_long_mat_numb = ead.mat_number AND ea.plant = ead.plant AND ea.storage_loc = ead.storage_loc','left');
    $this->SQL_server->where('ead.category',$customer);
    $this->SQL_server->where('ea.storage_loc','1000');
    $this->SQL_server->where('ead.category ',$customer);
    // if($tipe == 'ENGINE') {
    //   $this->SQL_server->not_like('ea.material_desc', 'APU');
    // }
    // $this->SQL_server->like('ea.material_desc', $tipe);
    $this->SQL_server->where('ea.unrestricted !=',0);
    // $this->SQL_server->group_by('ea.ex_long_mat_numb,ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');

     if ($data['search']!='') {
          // code...
          $this->SQL_server->where("(ea.ex_long_mat_numb like '%".$data['search']."%' OR
                    ea.material_desc like '%".$data['search']."%' OR
                    ea.material_type LIKE  '%".$data['search']."%' OR
                    ea.plant LIKE  '%".$data['search']."%' OR
                    ea.storage_loc LIKE  '%".$data['search']."%' OR
                    ea.desc_storage_loc LIKE  '%".$data['search']."%' OR
                    ea.unrestricted LIKE '%".$data['search']."%')");
        }
    $this->SQL_server->where('category',$customer);
    return $this->SQL_server->get()->num_rows();
  }

    // mendapatkan detail data availability
    public function detail_av_select($tipe,$select,$customer,$data)
    {
      $this->SQL_server->select('DISTINCT (ea.ex_long_mat_numb),ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');
      $this->SQL_server->from('tbl_engine_apu ea');
      $this->SQL_server->join('tbl_engine_apu_dtl ead','ea.ex_long_mat_numb = ead.mat_number AND ea.plant = ead.plant AND ea.storage_loc = ead.storage_loc','left');
      $this->SQL_server->where('ead.category',$customer);
      $this->SQL_server->where('ea.storage_loc','1000');
      // if($tipe == 'ENGINE') {
      //   $this->SQL_server->not_like('ea.material_desc', 'APU');
      // }
      // $this->SQL_server->like('ea.material_desc', $tipe);
      $this->SQL_server->where('ea.unrestricted !=',0);
      // $this->SQL_server->group_by('ea.ex_long_mat_numb,ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');

      if ($data['search']!='') {
            // code...
            $this->SQL_server->where("(ea.ex_long_mat_numb like '%".$data['search']."%' OR
                      ea.material_desc like '%".$data['search']."%' OR
                      ea.material_type LIKE  '%".$data['search']."%' OR
                      ea.plant LIKE  '%".$data['search']."%' OR
                      ea.storage_loc LIKE  '%".$data['search']."%' OR
                      ea.desc_storage_loc LIKE  '%".$data['search']."%' OR
                      ea.unrestricted LIKE '%".$data['search']."%')");
          }
      $this->SQL_server->where("ex_long_mat_numb in ('".$select."')");
      return $this->SQL_server->get()->result_array();
    }

    //mendapatkan jumlah banyaknya data availability
    public function count_detail_av_select($tipe,$select,$customer,$data)
    {
      // code...
      $this->SQL_server->select('DISTINCT(ea.ex_long_mat_numb),ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');
      $this->SQL_server->from('tbl_engine_apu ea');
      $this->SQL_server->join('tbl_engine_apu_dtl ead','ea.ex_long_mat_numb = ead.mat_number AND ea.plant = ead.plant AND ea.storage_loc = ead.storage_loc','left');
      $this->SQL_server->where('ead.category',$customer);
      $this->SQL_server->where('ea.storage_loc','1000');
      // if($tipe == 'ENGINE') {
      //   $this->SQL_server->not_like('ea.material_desc', 'APU');
      // }
      // $this->SQL_server->like('ea.material_desc', $tipe);
      $this->SQL_server->where('ea.unrestricted !=',0);
      // $this->SQL_server->group_by('ea.ex_long_mat_numb,ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');

      if ($data['search']!='') {
            // code...
            $this->SQL_server->where("(ea.ex_long_mat_numb like '%".$data['search']."%' OR
                      ea.material_desc like '%".$data['search']."%' OR
                      ea.material_type LIKE  '%".$data['search']."%' OR
                      ea.plant LIKE  '%".$data['search']."%' OR
                      ea.storage_loc LIKE  '%".$data['search']."%' OR
                      ea.desc_storage_loc LIKE  '%".$data['search']."%' OR
                      ea.unrestricted LIKE '%".$data['search']."%')");
          }
      $this->SQL_server->where("ex_long_mat_numb in ('".$select."')");
      // $this->SQL_server->select('ea.ex_long_mat_numb,ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');
      // $this->SQL_server->from('tbl_engine_apu ea');
      // $this->SQL_server->join('tbl_engine_apu_dtl ead','ea.ex_long_mat_numb = ead.mat_number AND ea.plant = ead.plant AND ea.storage_loc = ead.storage_loc','left');
      // $this->SQL_server->where('ea.storage_loc','1000');
      // $this->SQL_server->where('ea.material_desc',$tipe);
      // $this->SQL_server->where('ea.unrestricted !=',0);
      // $this->SQL_server->where('ead.category',$customer);
      // $this->SQL_server->where("ex_long_mat_numb in ('".$select."')");
      // if ($data['search']!='') {
      //       // code...
      //       $this->SQL_server->where("(ea.ex_long_mat_numb like '%".$data['search']."%' OR
      //                 ea.material_desc like '%".$data['search']."%' OR
      //                 ea.material_type LIKE  '%".$data['search']."%' OR
      //                 ea.plant LIKE  '%".$data['search']."%' OR
      //                 ea.storage_loc LIKE  '%".$data['search']."%' OR
      //                 ea.desc_storage_loc LIKE  '%".$data['search']."%' OR
      //                 ea.unrestricted LIKE '%".$data['search']."%')");
      //     }
      return $this->SQL_server->get()->num_rows();
    }

  // mendapatkan nilai detail datatable not availability
    public function detail_nav_select($tipe,$select,$customer,$data)
    {
      // code...
     $this->SQL_server->select('DISTINCT (ea.ex_long_mat_numb),ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');
      $this->SQL_server->from('tbl_engine_apu ea');
      $this->SQL_server->join('tbl_engine_apu_dtl ead','ea.ex_long_mat_numb = ead.mat_number AND ea.plant = ead.plant AND ea.storage_loc = ead.storage_loc','left');
      $this->SQL_server->where('ead.category',$customer);
      $this->SQL_server->where('ea.storage_loc !=','1000');
      // if($tipe == 'ENGINE') {
      //   $this->SQL_server->not_like('ea.material_desc', 'APU');
      // }
      // $this->SQL_server->like('ea.material_desc', $tipe);
      $this->SQL_server->where('ea.unrestricted !=',0);
      // $this->SQL_server->group_by('ea.ex_long_mat_numb,ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');

       if ($data['search']!='') {
            // code...
            $this->SQL_server->where("(ea.ex_long_mat_numb like '%".$data['search']."%' OR
                      ea.material_desc like '%".$data['search']."%' OR
                      ea.material_type LIKE  '%".$data['search']."%' OR
                      ea.plant LIKE  '%".$data['search']."%' OR
                      ea.storage_loc LIKE  '%".$data['search']."%' OR
                      ea.desc_storage_loc LIKE  '%".$data['search']."%' OR
                      ea.unrestricted LIKE '%".$data['search']."%')");
          }
      $this->SQL_server->where("ex_long_mat_numb in ('".$select."')");
      return $this->SQL_server->get()->result_array();
    }

  //mendapatkan nilai jumlah datatables not availability
  public function count_detail_nav_select($tipe,$select,$customer,$data)
  {
    // code...
   $this->SQL_server->select('DISTINCT (ea.ex_long_mat_numb),ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');
    $this->SQL_server->from('tbl_engine_apu ea');
    $this->SQL_server->join('tbl_engine_apu_dtl ead','ea.ex_long_mat_numb = ead.mat_number AND ea.plant = ead.plant AND ea.storage_loc = ead.storage_loc','left');
    $this->SQL_server->where('ead.category',$customer);
    $this->SQL_server->where('ea.storage_loc !=','1000');
    // if($tipe == 'ENGINE') {
    //   $this->SQL_server->not_like('ea.material_desc', 'APU');
    // }
    // $this->SQL_server->like('ea.material_desc', $tipe);
    $this->SQL_server->where('ea.unrestricted !=',0);
    // $this->SQL_server->group_by('ea.ex_long_mat_numb,ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');

     if ($data['search']!='') {
          // code...
          $this->SQL_server->where("(ea.ex_long_mat_numb like '%".$data['search']."%' OR
                    ea.material_desc like '%".$data['search']."%' OR
                    ea.material_type LIKE  '%".$data['search']."%' OR
                    ea.plant LIKE  '%".$data['search']."%' OR
                    ea.storage_loc LIKE  '%".$data['search']."%' OR
                    ea.desc_storage_loc LIKE  '%".$data['search']."%' OR
                    ea.unrestricted LIKE '%".$data['search']."%')");
        }
    $this->SQL_server->where("ex_long_mat_numb in ('".$select."')");

      // code...
      // $this->SQL_server->select('ea.ex_long_mat_numb,ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');
      // $this->SQL_server->from('tbl_engine_apu ea');
      // $this->SQL_server->join('tbl_engine_apu_dtl ead','ea.ex_long_mat_numb = ead.mat_number AND ea.plant = ead.plant AND ea.storage_loc = ead.storage_loc','left');
      // $this->SQL_server->where('ead.category',$customer);
      // $this->SQL_server->where('ea.storage_loc !=','1000');
      // $this->SQL_server->where('ea.material_desc',$tipe);
      // $this->SQL_server->where('ea.unrestricted !=',0);
      //  if ($data['search']!='') {
      //       // code...
      //       $this->SQL_server->where("(ea.ex_long_mat_numb like '%".$data['search']."%' OR
      //                 ea.material_desc like '%".$data['search']."%' OR
      //                 ea.material_type LIKE  '%".$data['search']."%' OR
      //                 ea.plant LIKE  '%".$data['search']."%' OR
      //                 ea.storage_loc LIKE  '%".$data['search']."%' OR
      //                 ea.desc_storage_loc LIKE  '%".$data['search']."%' OR
      //                 ea.unrestricted LIKE '%".$data['search']."%')");
      //     }
      // $this->SQL_server->where("ex_long_mat_numb in ('".$select."')");

    return $this->SQL_server->get()->num_rows();

  }

  public function count_detail_nav($tipe,$customer,$data)
  {
    // code...
    $this->SQL_server->select('DISTINCT (ea.ex_long_mat_numb),ea.material_desc,ea.material_type,ea.plant,ea.storage_loc,ea.desc_storage_loc,ea.unrestricted');
    $this->SQL_server->from('tbl_engine_apu ea');
    $this->SQL_server->join('tbl_engine_apu_dtl ead','ea.ex_long_mat_numb = ead.mat_number AND ea.plant = ead.plant AND ea.storage_loc = ead.storage_loc','left');
    $this->SQL_server->where('ead.category',$customer);
    $this->SQL_server->where('ea.storage_loc !=','1000');
    // $this->SQL_server->where('ea.material_desc',$tipe);
    $this->SQL_server->where('ea.unrestricted !=',0);
     if ($data['search']!='') {
          // code...
          $this->SQL_server->where("(ea.ex_long_mat_numb like '%".$data['search']."%' OR
                    ea.material_desc like '%".$data['search']."%' OR
                    ea.material_type LIKE  '%".$data['search']."%' OR
                    ea.plant LIKE  '%".$data['search']."%' OR
                    ea.storage_loc LIKE  '%".$data['search']."%' OR
                    ea.desc_storage_loc LIKE  '%".$data['search']."%' OR
                    ea.unrestricted LIKE '%".$data['search']."%')");
        }
    return $this->SQL_server->get()->num_rows();
  }
}
