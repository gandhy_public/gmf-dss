
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class M_user extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_all_role() {
        $this->db->where("role_isactive", "1");
        $q1 = $this->db->get("role");
        $dataRole = array();
        foreach ($q1->result_array() as $data) {
            $dataRole[] = $data;
        }
        return $dataRole;
    }

    public function show_datatable($data) {
        $tipe = $data["tipe"];

        $total = $this->db->count_all_results("user");
        $output = array();
        $output["draw"] = $data["draw"];
        $output["recordsTotal"] = $output["recordsFiltered"] = $total;
        $output["data"] = array();

        if ($data["search"] != "") {
            if ($tipe == "") {
                $tipe = "";
            } else {
                $this->db->where("user_role.role_id", $tipe);
            }
            $this->db->select('user.username, user.user_id, user_isactive, role.role_id, role.role_name');
            $this->db->from('user');
            $this->db->join('user_role', 'user_role.user_id = user.user_id');
            $this->db->join('role', 'role.role_id = user_role.role_id');
            $this->db->where("user_isactive", "1");
            $this->db->or_like("username", $data["search"]);
            $jum = $this->db->get();
            $output["recordsTotal"] = $output["recordsFiltered"] = $jum->num_rows();
        }

        if ($data["search"] != "") {
            $this->db->or_like("username", $data["search"]);
        }
        if ($data["tipe"] == "") {
            $tipe = "";
        } else {
            $this->db->where("user_role.role_id", $tipe);
        }

        $this->db->select('user.username,  user.user_id, user_isactive, role.role_id, role.role_name');
        $this->db->from('user');
        $this->db->join('user_role', 'user_role.user_id = user.user_id');
        $this->db->join('role', 'role.role_id = user_role.role_id');
        $this->db->where("user_isactive", "1");
        $this->db->limit($data["length"], $data["start"]);

        $query = $this->db->get();
        $nomor_urut = $data["start"] + 1;
        foreach ($query->result_array() as $role) {
            $output["data"][] = array(
                $nomor_urut,
                SaveTextPass("GMF", $role["user_id"]),
                $role["username"],
                $role["role_name"],
            );
            $nomor_urut++;
        }
        echo json_encode($output, JSON_PRETTY_PRINT);
    }

    public function count_approval()
    {
      $sql = "select count(user_id) as total from user where user_approved = '0'";
      return $this->db->query($sql)->row();
    }

    public function approved($user_id)
    {
      $sql = "update user set user_approved = '1' where user_id = '".$user_id."' ";
      $exec = $this->db->query($sql);
      return (($exec) ? true : false);
    }

    public function show_user_approval()
    {
      $sql = "SELECT
              	a.*, c.role_name
              FROM
              	user a
              LEFT JOIN user_role b ON a.user_id = b.user_id
              LEFT JOIN role c ON b.role_id = c.role_id
              WHERE
              	user_approved = '0'";
      return $this->db->query($sql)->result();
    }

    public function input_user($data, $table) {
        $username = $data["username"];
        $data["user_approved"] = '1';
        $this->db->insert($table, $data);
        $query = $this->db->query("select user_id from user where username='" . $username . "' ");
        $row = $query->row();
        $id = $row->user_id;

        return $id;
    }

    public function isUnique($username) {
        $sql = "select count(username) as total from user where user_isactive = '1' and username = '" . $username . "'";
        $exec = $this->db->query($sql);
        $result = $exec->row()->total;
        if ($result > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function input_role($data, $table) {
        $this->db->insert($table, $data);
    }

    public function edit_user($data, $table, $id) {
        $this->db->where('user_id', $id);
        $this->db->update($table, $data);
    }

    public function edit_role($data, $table, $id) {
        $this->db->where('user_id', $id);
        $this->db->update($table, $data);
    }

    public function del_user($id) {
        $id = GetSaveTextPass("GMF", $id);
        $query = "update user set user_isactive = '0' where user_id = '" . $id . "'";
        return ($this->db->query($query)) ? true : false;
    }

    public function get_user($id) {
        //$id = GetSaveTextPass("GMF", $id);
        $query = $this->db->query('
        SELECT
        user .username,
        user .user_id,
        user_isactive,
        role.role_id,
        role.role_name
        FROM user left join user_role on `user`.user_id = user_role.user_id
        left join role on user_role.role_id = role.role_id
        WHERE user_isactive = "1"
        AND user.user_id = "'.$id.'"
        ');

        return $query->row();
    }

    public function get_all_user($id = null) {
        if ($id == null) {
            $sql = "select * from user where user_isactive = '1' and kpi_unit_id is null";
            //$sql = "select * from user where user_isactive = '1'";
        } else {
            $sql = "select * from user where user_isactive = '1' and kpi_unit_id = '" . $id . "'";
        }
        $exec = $this->db->query($sql);
        return $exec->result();
    }

    public function delete_unit_where($id) {
        $sql = "update user set kpi_unit_id = null where user_id = '" . $id . "'";
        $exec = $this->db->query($sql);
        return ($exec) ? true : false;
    }

    //DIMAS
    var $column_order = array('user.user_id','user.username','role.role_name',null);
    var $order = array('user.user_id' => 'ASC');

    private function query_new($tipe){
        $this->db->select('user.username, user.user_id, user.user_isactive, role.role_id, role.role_name');
        $this->db->from('user');
        $this->db->join('user_role', 'user_role.user_id = user.user_id', 'left');
        $this->db->join('role', 'role.role_id = user_role.role_id', 'left');
        $this->db->where("user.user_isactive", "1");
        $this->db->where("user.user_approved", "1");
        if($tipe == ''){
            '';
        }else{
            $this->db->where("user_role.role_id", $tipe);
        }
        if ($_POST['search']['value']) {
            $this->db->like("LOWER(user.username)", strtolower($_POST["search"]["value"]));
            $this->db->or_like("LOWER(role.role_name)", strtolower($_POST["search"]["value"]));
        }
        if(isset($_POST['order'])){
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }else if(isset($this->order)){
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
    }

    function get_datatables_new($tipe){
        $this->query_new($tipe);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_all_new($tipe){
        $this->query_new($tipe);
        return $this->db->count_all_results();
    }

    function count_filtered_new($tipe){
        $this->query_new($tipe);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function delete_user_new($where, $data) {
        $this->db->update('user', $data, $where);
        return $this->db->affected_rows();
    }
}

?>
