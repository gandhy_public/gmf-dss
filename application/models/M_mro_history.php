<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_mro_history extends CI_Model{

  var $table = 'mro_history_upload';
  var $column_order = array('id_mro_history_upload','date_upload','name_file', null);
  var $column_search = array('date_upload','name_file');
  var $order = array('date_upload' => 'desc');

  private function _get_datatables_query(){

    $this->db->select('mro_history_upload.id_mro_history_upload,
                       mro_history_upload.date_upload,
                       mro_history_upload.name_file'
                       );
    $this->db->from($this->table);
    $i = 0;

    foreach ($this->column_search as $item)
    {
      if(!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order']))
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_alldata()
  {
    $this->_get_datatables_query();
    $data = $this->db->get();

    return $data->result();
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_mro_history_upload',$id);
    $query = $this->db->get();

    return $query->row();
  }

  public function save($data)
  {
    $this->db->insert($this->table, $data);

    $id = $this->db->insert_id();
    return (isset($id)) ? $id : FALSE;
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->where('id_mro_history_upload', $id);
    $this->db->delete($this->table);
  }

}
