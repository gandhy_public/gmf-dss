 <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_repetitive_mitigation extends CI_Model
{

    var $table = 'repetitive_d5_final';
    var $column_order = array('id_repetitive_d5', 'functional_location', 'description', 'm_acreg.own', 'm_actype.actype', 'repetitive_d5_final.created_on', 'repetitive_d5_final.req_start', null);
    var $column_search = array('notification','functional_location', 'description', 'm_acreg.own', 'm_actype.actype', 'repetitive_d5_final.created_on', 'repetitive_d5_final.req_start');
    var $order = array('id_repetitive_d5' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->db_dev = $this->load->database('dev_gmf', TRUE);
    }

    private function _get_datatables_query()
    {

        $this->db_dev->select('
                       repetitive_d5_final.id_repetitive_d5,
                       repetitive_d5_final.order,
                       repetitive_d5_final.notification,
                       repetitive_d5_final.system_status,
                       repetitive_d5_final.description,
                       repetitive_d5_final.date_all,
                       repetitive_d5_final.functional_location,
                       repetitive_d5_final.notif_date,
                       repetitive_d5_final.fault_code,
                       repetitive_d5_final.req_start,
                       repetitive_d5_final.created_on,
                       tbl_mitigasi.mitigasi_id,
                       tbl_mitigasi.mitigasi_why,
                       tbl_mitigasi.mitigasi_solution,
                       tbl_mitigasi.mitigasi_date,
                       tbl_mitigasi.mitigasi_aircraft,
                       tbl_mitigasi.mitigasi_type,
                       tbl_mitigasi.mitigasi_fk_id,
                       m_acreg.acreg_id,
                       m_acreg.actype_id,
                       m_acreg.own,
                       m_acreg.acreg,
                       m_actype.actype'
        );
        $this->db_dev->join('tbl_mitigasi', 'repetitive_d5_final.notification = tbl_mitigasi.mitigasi_fk_id', 'left');
        $this->db_dev->join('m_acreg', 'repetitive_d5_final.functional_location = m_acreg.acreg', 'left');
        $this->db_dev->join('m_actype', 'm_actype.actype_id = m_acreg.actype_id', 'left');
        $this->db_dev->where("(repetitive_d5_final.system_status in ('open','monitor','reminder') )");
        $this->db_dev->where("(m_acreg.own = 'GA' OR m_acreg.own = 'CITILINK')");
        $this->db_dev->order_by('m_acreg.own', 'DESC');
        $this->db_dev->order_by('repetitive_d5_final.notification', 'DESC');

        $this->db_dev->from($this->table);

        // if ($this->s_date!="" && $this->e_date!="") {
        //   $start_date = explode("-", $this->s_date, 2);
        //   $end_date = explode("-", $this->e_date, 2);
        //   $this->db_dev->where('MONTH(CONVERT(date, repetitive_d5_final.created_on, 104)) >=', $start_date[1]);
        //   $this->db_dev->where('MONTH(CONVERT(date, repetitive_d5_final.created_on, 104)) <=', $end_date[1]);
        //   $this->db_dev->where('YEAR(CONVERT(date, db_dss.dbo.repetitive_d5_final.created_on, 104)) >=', $start_date[0]);
        //   $this->db_dev->where('YEAR(CONVERT(date, db_dss.dbo.repetitive_d5_final.created_on, 104)) <=', $end_date[0]);
        // }
        $i = 0;

        foreach ($this->column_search as $item) {
            if (!empty($_POST['search']['value'])) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db_dev->group_start();
                    $this->db_dev->like($item, $_POST['search']['value']);
                } else {
                    $this->db_dev->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db_dev->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db_dev->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db_dev->order_by(key($order), $order[key($order)]);
        }
    }

    function get_alldata()
    {
        $this->_get_datatables_query();
        $data = $this->db_dev->get();

        return $data->result();
    }

    function get_datatables($p_start = "", $p_end = "")
    {
        if (!empty($p_start) || $p_start != "" || !empty($p_end) || $p_end != "") {
            $awal = explode("-", $p_start);
            $awal_bulan = $awal[0];
            $awal_tahun = $awal[1];
            $akhir = explode("-", $p_end);
            $akhir_bulan = $akhir[0];
            $akhir_tahun = $akhir[1];
            $de = cal_days_in_month(CAL_GREGORIAN, $akhir_bulan, $akhir_tahun);
            $p_start = $awal_tahun . $awal_bulan . '01';
            $p_end = $akhir_tahun . $akhir_bulan . $de;
            $this->_get_datatables_query();
            $this->db_dev->where('CAST(repetitive_d5_final.created_on AS DATE) >=', $p_start);
            $this->db_dev->where('CAST(repetitive_d5_final.created_on AS DATE) <=', $p_end);
        } else {
            $this->_get_datatables_query();
        }
        if ($_POST['length'] != -1)
            $this->db_dev->limit($_POST['length'], $_POST['start']);
        $query = $this->db_dev->get();
        return $query->result();
    }

    function count_filtered($p_start = "", $p_end = "")
    {
        if (!empty($p_start) || $p_start != "" || !empty($p_end) || $p_end != "") {
            $awal = explode("-", $p_start);
            $awal_bulan = $awal[0];
            $awal_tahun = $awal[1];
            $akhir = explode("-", $p_end);
            $akhir_bulan = $akhir[0];
            $akhir_tahun = $akhir[1];
            $de = cal_days_in_month(CAL_GREGORIAN, $akhir_bulan, $akhir_tahun);
            $p_start = $awal_tahun . $awal_bulan . '01';
            $p_end = $akhir_tahun . $akhir_bulan . $de;
            $this->_get_datatables_query();
            $this->db_dev->where('CAST(repetitive_d5_final.created_on AS DATE) >=', $p_start);
            $this->db_dev->where('CAST(repetitive_d5_final.created_on AS DATE) <=', $p_end);
        } else {
            $this->_get_datatables_query();
        }
        $query = $this->db_dev->get();
        return $query->num_rows();
    }

    public function count_all($p_start = "", $p_end = "")
    {
        if (!empty($p_start) || $p_start != "" || !empty($p_end) || $p_end != "") {
            $awal = explode("-", $p_start);
            $awal_bulan = $awal[0];
            $awal_tahun = $awal[1];
            $akhir = explode("-", $p_end);
            $akhir_bulan = $akhir[0];
            $akhir_tahun = $akhir[1];
            $de = cal_days_in_month(CAL_GREGORIAN, $akhir_bulan, $akhir_tahun);
            $p_start = $awal_tahun . $awal_bulan . '01';
            $p_end = $akhir_tahun . $akhir_bulan . $de;
            $this->_get_datatables_query();
            $this->db_dev->where('CAST(repetitive_d5_final.created_on AS DATE) >=', $p_start);
            $this->db_dev->where('CAST(repetitive_d5_final.created_on AS DATE) <=', $p_end);
        } else {
            $this->_get_datatables_query();
        }
        $query = $this->db_dev->get();
        return $query->num_rows();
    }

    public function get_by_id($id)
    {
        $this->db_dev->select('repetitive_d5_final.id_repetitive_d5,
                       repetitive_d5_final.order,
                       repetitive_d5_final.notification,
                       repetitive_d5_final.system_status,
                       repetitive_d5_final.date_all,
                       repetitive_d5_final.functional_location,
                       repetitive_d5_final.notif_date,
                       repetitive_d5_final.fault_code,
                       repetitive_d5_final.req_start,
                       repetitive_d5_final.created_on,
                       m_acreg.acreg_id,
                       m_acreg.actype_id,
                       m_acreg.own,
                       m_acreg.acreg,
                       m_actype.actype'
        );
        $this->db_dev->join('m_acreg', 'repetitive_d5_final.functional_location = m_acreg.acreg', 'left');
        $this->db_dev->join('m_actype', 'm_actype.actype_id = m_acreg.actype_id', 'left');
        $this->db_dev->where('repetitive_d5_final.notification', $id);
        $query = $this->db_dev->get($this->table);

        return $query->row();
    }

    public function save($data)
    {
        $this->db_dev->insert($this->table, $data);

        $id = $this->db_dev->insert_id();
        return (isset($id)) ? $id : FALSE;
    }

    public function update($where, $data)
    {
        $this->db_dev->update($this->table, $data, $where);
        return $this->db_dev->affected_rows();
    }

    public function delete($id)
    {
        $this->db_dev->where('id_repetitive_d5', $id);
        $this->db_dev->delete($this->table);
    }

    public function get_mitigasi_by_id($id)
    {
        $this->db_dev->from("tbl_mitigasi");
        $this->db_dev->where('mitigasi_fk_id', $id);
        $query = $this->db_dev->get();

        return $query->row();
    }

    public function update_mitigasi($data)
    {
        $mitigation = $this->db_dev->select('mitigasi_fk_id')
            ->from('tbl_mitigasi')
            ->where('mitigasi_fk_id', $data['mitigasi_fk_id'])
            ->where('mitigasi_type', 'repetitive')
            ->get()
            ->row();

        if ($mitigation == null) {

            $this->db_dev->insert("tbl_mitigasi", $data);

            return 'insert';
        } else {
            $this->db_dev->set('mitigasi_why', 'mitigasi_solution', 'mitigasi_date', 'mitigasi_aircraft', 'mitigasi_type', false);
            $this->db_dev->where('mitigasi_fk_id', $data["mitigasi_fk_id"]);
            $this->db_dev->update('tbl_mitigasi', $data);

            return 'update';
        }
    }

}