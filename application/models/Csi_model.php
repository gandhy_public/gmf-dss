<?php
defined('BASEPATH') or exit ('no direct script access allowed');

/**
 *
 */
class Csi_model extends CI_Model
{

  function __construct(){
      parent::__construct();
    }

    public function update_customer($name,$id,$today)
    {
      # code...
      $sql=$this->db->where('id_customer',$id)
                    ->update('csi_customer',array('name_customer' => $name, 'update_date'=> $today));
      return $sql;
    }
    public function add_customer($customer,$today)
    {
      # code...
      $data=array(
        'name_customer'=>$customer,
        'create_date'=> $today,
        'update_date'=> $today
      );
      $sql=$this->db->insert('csi_customer',$data);
      return $sql;
    }
    public function get_customer($data)
    {
      # code...

      if ($data['search'] != '') {
        $this->db->or_like('name_customer', $data['search']);
      }
      $this->db->limit($data['length'], $data['start']);
      $this->db->order_by($data['order_column'], $data['order_dir']);
      $query = $this->db->get('csi_customer');

      if ($data['search'] != '') {
          $this->db->or_like('name_customer', $data['search']);
          $num = $this->db->get('csi_customer');
          $print['recordsTotal'] = $print['recordsFiltered'] = $num->num_rows();
      }

      return $query->result_array();
    }

    public function cek_date($value,$periode)
    {
      # code...
      $sql=$this->db->select()
                    ->from('csi_history_action')
                    ->where('date_type',$value)
                    ->where('type',$periode)
                    ->count_all_results();
      return $sql;
    }

    public function cek_return_files_upload($id_user,$name_files,$periode,$for_date,$today)
    {
      # code...
      // Query Pertama
      $data = array(
          'id_user' => $id_user,
          'filename' => $name_files,
          'type' => $periode,
          'date_type'=> $for_date,
          'create_date'=>$today
      );
      $this->db->insert('csi_history_action', $data);
      $insert_id = $this->db->insert_id();
      return $insert_id;
    }


    public function cek_availabe_customer($name)
    {
      # code...
      $sql=$this->db->select('id_customer')
                    ->from('csi_customer')
                    ->where('name_customer',$name)
                    ->get();
      if ($sql==TRUE) {
        # code...
        return $sql->row();
      }
    }

    public function insert_data_org_item($sql_5_1,$sql_5,$sql_4_1,$sql_4_2,$sql_6_1,$sql_6_2)
    {
      # code...
      $this->db->trans_begin();
      $this->db->insert_batch('csi_data_org',$sql_5_1);
      $this->db->insert_batch('csi_data_org',$sql_5);
      $this->db->insert_batch('csi_data_org',$sql_4_1);
      $this->db->insert_batch('csi_data_org',$sql_4_2);
      $this->db->insert_batch('csi_data_org',$sql_6_1);
      $this->db->insert_batch('csi_data_org',$sql_6_2);

      if ($this->db->trans_status() === FALSE)
      {
              $this->db->trans_rollback();
              return FALSE;
      }
      else
      {
              $this->db->trans_commit();
              return TRUE;
      }

    }

    public function insert_final_score($data)
    {
      # code...
      $this->db->insert('csi_detail_finalscore', $data);
      $insert_id = $this->db->insert_id();

      return  $insert_id;
    }

    public function show_in_datatable($data)
    {
      # code...
      if ($data['search'] != '') {
        $this->db->or_like('date_type', $data['search']);
      }
      $this->db->where('type','semesterly');
      $this->db->limit($data['length'], $data['start']);
      $this->db->order_by('create_date', 'DESC');
      $query = $this->db->get('csi_history_action');
      return $query->result_array();
    }
    public function count_in_datatable($data)
    {
      # code...
      if ($data['search'] != '') {
        $this->db->or_like('date_type', $data['search']);
      }
      $this->db->where('type','semesterly');
      $query = $this->db->get('csi_history_action');
      return $query->num_rows();
    }

    public function insert_data_org_mountly($sql_5)
    {
      # code...
      $this->db->trans_begin();
      $this->db->insert_batch('csi_data_org',$sql_5);

      if ($this->db->trans_status() === FALSE)
      {
              $this->db->trans_rollback();
              return FALSE;
      }
      else
      {
              $this->db->trans_commit();
              return TRUE;
      }

    }

    public function del_customer($val)
    {
      # code...
      $this->db->trans_begin();
      $this->db->delete('csi_customer', array('id_customer' => $val));
      if ($this->db->trans_status() === FALSE)
      {
              $this->db->trans_rollback();
              return FALSE;
      }
      else
      {
              $this->db->trans_commit();
              return TRUE;
      }
    }

    public function rollback_func($val)
    {
      # code...
      $this->db->trans_begin();
      $this->db->delete('csi_detail_finalscore', array('id_history_action' => $val));
      $this->db->delete('csi_data_org', array('id_history_usr_action' => $val));
      $this->db->delete('csi_history_action', array('id_history' => $val));
      if ($this->db->trans_status() === FALSE)
      {
              $this->db->trans_rollback();
              return FALSE;
      }
      else
      {
              $this->db->trans_commit();
              return TRUE;
      }
    }

    public function get_org($id_act,$id_cus)
    {
      # code...
       $sql=$this->db->select()
                 ->from('csi_data_org')
                 ->where('id_history_usr_action',$id_act)
                 ->where('id_customer',$id_cus)
                 ->order_by('id_type_org_csi','ASC')
                 ->get();
      return $sql->result_array();
    }

    public function get_mountly_semester()
    {
      # code...
      $sql=$this->db->select();

    }

    public function get_name_customer($id_act_history)
    {
      // code...
      $sql=$this->db->query("SELECT DISTINCT
                            (CDO.id_customer),CC.name_customer
                            FROM
                            csi_data_org CDO
                            LEFT JOIN csi_customer CC ON CDO.id_customer= CC.id_customer
                            where CDO.id_history_usr_action=$id_act_history");
      return $sql->result_array();
    }
}
