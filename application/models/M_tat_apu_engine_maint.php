<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_tat_apu_engine_maint extends CI_Model{

  var $table = 'M_REVISION';
  // var $progress = false;
  // var $column_order = array('MONTH(M_REVISION.REVBD)','ESN','M_REVISIONDETAIL.WORKSCOPE', 'M_SALESORDER.NAME1', 'CUSTOMER_TYPE', 'PROD_TYPE', 'AIR_REG', 'TARGET_TAT', null, 'ACHIEVEMENT');
  // var $column_search = array('MONTH(M_REVISION.REVBD)','ESN','M_REVISIONDETAIL.WORKSCOPE', 'M_SALESORDER.NAME1', 'CUSTOMER_TYPE', 'PROD_TYPE', 'AIR_REG', 'TARGET_TAT');
  var $column_order = array('MONTH(A.MONTH)','ESN','CUSTOMER','CUST_TYPE','PRODUCT_TYPE','ENGINE_APU','MAINT_TYPE','TARGET_TAT',null,'PERFORMANCE');
  var $column_search = array('MONTH(A.MONTH)','ESN','CUSTOMER','CUST_TYPE','PRODUCT_TYPE','ENGINE_APU','MAINT_TYPE','TARGET_TAT');
  var $order = array('MONTH' => 'asc');

  public function __construct()
  {
    parent::__construct();
      $this->load->database();
      $this->db_dev = $this->load->database('prod_gmf_crm', TRUE);
      $this->db_dev2 = $this->load->database('dev_gmf_crm', TRUE);
      // $this->db_dev = $this->load->database('local_gmf_crm', TRUE);
      // $this->db_dev = $this->load->database('local_gmf_sqlsrv', TRUE);
  }

  private function _get_datatables_query(){
    $this->db_dev->select('*');
    $this->db_dev->from('V_CRM_TO_DSS');

    // $this->db_dev->distinct();
    // $this->db_dev->select("
    //
    // M_REVISION.REVNR as REVISION,
    //                       M_REVISION.EQUNR as ESN,
    //                       M_PMORDER.ATWRT as PROD_TYPE,
    //                       M_SALESORDER.NAME1 as CUSTOMER,
    //
    //                       (
    //                         CASE
    //                         WHEN M_SALESORDER.NAME1 LIKE '%GARUDA INDONESIA%' THEN 'GA'
    //                         WHEN M_SALESORDER.NAME1 LIKE '%CITILINK%' THEN 'GA'
    //                       ELSE
    //                       'NGA'
    //                       END
    //                       ) AS CUSTOMER_TYPE,
    //
    //                       M_REVISIONDETAIL.INDUCT_DATE as START_DATE_INDUCTION,
    //                       M_REVISION.REVBD as START_DATE_REVISION,
    //
    //                       MONTH(M_REVISION.REVBD) as MONTH_REVISION,
    //                       CONVERT(DATE, M_REVISION.REVED, 112) as END_DATE,
    //                       M_REVISIONDETAIL.WORKSCOPE as MAINT_TYPE,
    //                       M_REVISION.TPLNR as AIR_REG,
    //                       M_REVISIONDETAIL.AGREED_TAT as TARGET_TAT,
    //
    //                       (
    //                         CASE M_REVISIONDETAIL.INDUCT_DATE
    //                       WHEN NULL THEN DATEDIFF(DAY, CONVERT(DATE, M_REVISION.REVBD, 112), CONVERT(DATE, M_REVISION.REVED, 112)) + 1
    //                       ELSE DATEDIFF(DAY, CONVERT(date, M_REVISIONDETAIL.INDUCT_DATE, 112), CONVERT(DATE, M_REVISION.REVED, 112)) + 1
    //                       END
    //                       ) as ACTUAL_TAT,
    //                       convert(DECIMAL(10,2), ((convert(float, M_REVISIONDETAIL.AGREED_TAT) / convert(float,
    //                       (
    //                         CASE M_REVISIONDETAIL.INDUCT_DATE
    //                       WHEN NULL THEN DATEDIFF(DAY, CONVERT(DATE, M_REVISION.REVBD, 112), CONVERT(DATE, M_REVISION.REVED, 112)) + 1
    //                       ELSE DATEDIFF(DAY, CONVERT(date, M_REVISIONDETAIL.INDUCT_DATE, 112), CONVERT(DATE, M_REVISION.REVED, 112)) + 1
    //                       END)))*100)) AS ACHIEVEMENT,
    //                       (CASE M_REVISIONDETAIL.INDUCT_DATE
    //                       WHEN NULL THEN DATEDIFF(DAY,CONVERT ( DATE, M_REVISION.REVBD, 112 ),getdate()) + 1
    //                       ELSE
    //                         DATEDIFF(DAY,CONVERT ( DATE, M_REVISIONDETAIL.INDUCT_DATE, 112 ), getdate()) + 1
    //                       END) AS DIFF_DATE"
    //
    //                       );
    // $this->db_dev->from($this->table);
    // $this->db_dev->join('M_PMORDER', 'M_REVISION.REVNR = M_PMORDER.REVNR AND M_REVISION.EQUNR = M_PMORDER.EQUNR', 'left');
    // $this->db_dev->join('M_PMORDER', 'M_REVISION.EQUNR = M_PMORDER.EQUNR', 'left');
    // $this->db_dev->join('M_REVISIONDETAIL', 'M_REVISION.REVNR = M_REVISIONDETAIL.REVNR');
    // $this->db_dev->join('(SELECT
    //                   M_PMORDER.REVNR,
    //                   M_SALESORDER.VBELN,
    //                   M_SALESORDER.KUNNR,
    //                   M_SALESORDER.NAME1
    //                 FROM
    //                   M_PMORDER
    //                 LEFT OUTER JOIN M_SALESORDER ON M_SALESORDER.VBELN = M_PMORDER.KDAUF
    //                 WHERE
    //                   M_SALESORDER.NAME1 IS NOT NULL
    //                 GROUP BY
    //                   M_PMORDER.REVNR,
    //                   M_SALESORDER.VBELN,
    //                   M_SALESORDER.KUNNR,
    //                   M_SALESORDER.NAME1) M_SALESORDER',
    //   'M_REVISION.REVNR = M_SALESORDER.REVNR');
    $i = 0;

    foreach ($this->column_search as $item)
    {
      if(!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db_dev->group_start();
          $this->db_dev->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db_dev->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db_dev->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order']))
    {
      $this->db_dev->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db_dev->order_by(key($order), $order[key($order)]);
    }
  }


  function get_alldata()
  {
    $this->_get_datatables_query();
    $data = $this->db_dev->get();

    return $data->result();
  }

  public function getDataMonth()
  {
    $year = date('Y');
    // $this->db_dev2->select('DISTINCT(MONTH(M_REVISION.REVBD)) as bulan');
    // $this->db_dev2->from($this->table);
    $this->_get_datatables_query();
    $this->db_dev->where('YEAR', $year);
    $query = $this->db_dev->get();
    return $query->result();
  }

  public function getDataChart($product_type)
  {
    $year = date('Y');
    $sql = "select bulan, AVG(PERFORMANCE) as nilai from (
            	SELECT
            	 YEAR  AS  tahun ,
            	 MONTH  AS  bulan ,
            	CASE
            		WHEN PERFORMANCE > 1 THEN 1
            		ELSE PERFORMANCE
               END AS PERFORMANCE
            -- 		AVG (PERFORMANCE) AS nilai
            	FROM
            		 V_CRM_TO_DSS
            	WHERE
            		 YEAR  = '".$year."'
            	AND  ACT_FINISH  IS NOT NULL
            	AND  PRODUCT_TYPE  LIKE '%".$product_type."%' ESCAPE '!'
            ) a
             	 GROUP BY
            		 bulan ,
             		 tahun ";
    $exec = $this->db_dev->query($sql);
    return $exec->result();
    // $this->db_dev->select('MONTH AS bulan,AVG(PERFORMANCE) as nilai');
    // $this->db_dev->from('V_CRM_TO_DSS');
    // $this->db_dev->where('YEAR',$year);
    // $this->db_dev->where('ACT_FINISH IS NOT NULL');
    // $this->db_dev->like('PRODUCT_TYPE',$product_type);
    // $this->db_dev->group_by('MONTH , YEAR');
    // $query = $this->db_dev->get();
    // return $query->result();
    // $year = date('Y');
    // $this->db_dev2->select("*"); //MONTH(M_REVISION.REVBD) AS bulan, AVG(convert(DECIMAL(10,2),
                            // ((convert(float, M_REVISIONDETAIL.AGREED_TAT) / convert(float, (CASE M_REVISIONDETAIL.INDUCT_DATE WHEN NULL THEN DATEDIFF(DAY, CONVERT(DATE, M_REVISION.REVBD, 112),
                            // CONVERT(DATE, M_REVISION.REVED, 112)) + 1 ELSE DATEDIFF(DAY, CONVERT(date, M_REVISIONDETAIL.INDUCT_DATE, 112), CONVERT(DATE, M_REVISION.REVED, 112)) + 1 END)))*100))) as nilai
    // $this->db_dev2->from($this->table);
    // $this->db_dev2->join('M_PMORDER', 'M_REVISION.REVNR = M_PMORDER.REVNR AND M_REVISION.EQUNR = M_PMORDER.EQUNR', 'left');
    // $this->db_dev2->join('M_PMORDER', 'M_REVISION.EQUNR = M_PMORDER.EQUNR', 'left');
    // $this->db_dev2->join('M_REVISIONDETAIL', 'M_REVISION.REVNR = M_REVISIONDETAIL.REVNR');
    // $this->db_dev2->join('(SELECT
    //                   M_PMORDER.REVNR,
    //                   M_SALESORDER.VBELN,
    //                   M_SALESORDER.KUNNR,
    //                   M_SALESORDER.NAME1
    //                 FROM
    //                   M_PMORDER
    //                 LEFT OUTER JOIN M_SALESORDER ON M_SALESORDER.VBELN = M_PMORDER.KDAUF
    //                 WHERE
    //                   M_SALESORDER.NAME1 IS NOT NULL
    //                 GROUP BY
    //                   M_PMORDER.REVNR,
    //                   M_SALESORDER.VBELN,
    //                   M_SALESORDER.KUNNR,
    //                   M_SALESORDER.NAME1) M_SALESORDER',
    //   'M_REVISION.REVNR = M_SALESORDER.REVNR');
    // $this->db_dev2->where('YEAR(M_REVISION.REVBD)', $year);
    // $this->db_dev2->like('M_PMORDER.ATWRT', $product_type, 'BOTH');
    // $this->db_dev2->where("CONVERT(DATE, M_REVISION.REVED, 112) IS NOT NULL"); // by me
    // $this->db_dev2->where("(status like '%TECO%' OR status like '%CLSD%')");
    // $this->db_dev2->like('status', "TECO", 'BOTH');
    // $this->db_dev2->or_like('status', "CLSD", 'BOTH');
    // $this->db_dev2->where('M_PMORDER.ATWRT', $product_type);
    // $this->db_dev2->where('status', "TECO");
    // $this->db_dev2->or_where('status', "CLSD");
    // $this->db_dev2->group_by('MONTH(M_REVISION.REVBD)');
    // $query = $this->db_dev2->get();
    // return $query->result();
  }


  function get_datatables($month,$where, $prod_type) //$where
  {
    $year = date('Y');
    $this->_get_datatables_query();
    if ($month!=-1) {
      $this->db_dev->where('MONTH', $month);
    }
    // if ($where = 'CONVERT(DATE, M_REVISION.REVED, 112) IS NOT NULL') {

    // }
    $this->db_dev->where($where);
    $this->db_dev->where('YEAR', $year);
    if ($prod_type!="") {
      $this->db_dev->like('PRODUCT_TYPE', $prod_type, 'BOTH');
    }
    if($_POST['length'] != -1)
      $this->db_dev->limit($_POST['length'], $_POST['start']);
      $query = $this->db_dev->get();
    return $query->result();
  }

  function count_filtered($month,$where, $prod_type) //$where
  {
    $year = date('Y');
    $this->_get_datatables_query();
    if ($month!=-1) {
      $this->db_dev->where('MONTH', $month);
    }
    $this->db_dev->where($where);
    $this->db_dev->where('YEAR', $year);
    $this->db_dev->like('PRODUCT_TYPE', $prod_type, 'BOTH');
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  public function count_all($month,$where, $prod_type) // $where,
  {
    $year = date('Y');
    $this->_get_datatables_query();
    if ($month!=-1) {
      $this->db_dev->where('MONTH', $month);
    }
    $this->db_dev->where($where);
    $this->db_dev->where('YEAR', $year);
    $this->db_dev->like('PRODUCT_TYPE', $prod_type, 'BOTH');
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  public function count_event_closed($prod_type)
  {
    $year = date('Y');
    $where_like = "ACT_FINISH IS NULL"; // by me
    // $where_like = "(status LIKE '%TECO%' OR status LIKE '%CLSD%')";
    $this->_get_datatables_query();
    $this->db_dev->where($where_like);
    $this->db_dev->where('YEAR', $year);
    $this->db_dev->like('PRODUCT_TYPE', $prod_type, 'BOTH');
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  // public function get_by_id($id)
  // {
  //   $this->db_dev->from($this->table);
  //   $this->db_dev->where('MONTH(MONTH)',$id);
  //   $query = $this->db_dev->get();
  //
  //   return $query->row();
  // }


    public function _emptyData($year, $type, $aircraft)
    {
      // $this->db_dev2->from($this->table);
      // $this->db_dev2->where('CUSTOMER', $year);
      // // $this->db_dev2->where('M_PMORDER.ATWRT', $type);
      // $this->db_dev2->where('MAINT_TYPE', $aircraft);
      // $query = $this->db_dev2->get();
      //
      // return $query->result();
    }


      public function getDataChart_($product_type)
      {
        $year = date('Y');
        $this->db_dev->select('MONTH AS bulan,AVG(PERFORMANCE) as nilai');
        $this->db_dev->from('V_CRM_TO_DSS');
        $this->db_dev->where('YEAR',$year);
        $this->db_dev->like('PRODUCT_TYPE',$product_type);
        $this->db_dev->group_by('MONTH','YEAR');
        $query = $this->db_dev->get();
        return $query->result();
        // SELECT MONTH, AVG(PERFORMANCE) FROM V_CRM_TO_DSS WHERE YEAR = '2018' AND PRODUCT_TYPE LIKE '%CFM%' GROUP BY MONTH, YEAR

        // $this->db_dev2->select('*');
        // sum(((convert(float, M_REVISIONDETAIL.AGREED_TAT) / convert(float, (CASE M_REVISIONDETAIL.INDUCT_DATE
        //   WHEN NULL THEN DATEDIFF(DAY, CONVERT(DATE, M_REVISION.REVBD, 112), CONVERT(DATE, M_REVISION.REVED, 112)) + 1
        //   ELSE DATEDIFF(DAY, CONVERT(date, M_REVISIONDETAIL.INDUCT_DATE, 112), CONVERT(DATE, M_REVISION.REVED, 112)) + 1
        //   END)))))/count(((convert(float, M_REVISIONDETAIL.AGREED_TAT) / convert(float, (CASE M_REVISIONDETAIL.INDUCT_DATE
        //   WHEN NULL THEN DATEDIFF(DAY, CONVERT(DATE, M_REVISION.REVBD, 112), CONVERT(DATE, M_REVISION.REVED, 112)) + 1
        //   ELSE DATEDIFF(DAY, CONVERT(date, M_REVISIONDETAIL.INDUCT_DATE, 112), CONVERT(DATE, M_REVISION.REVED, 112)) + 1
        //   END))))) as nilai
        // $this->db_dev2->from($this->table);
        // $this->db_dev2->join('M_PMORDER', 'M_REVISION.REVNR = M_PMORDER.REVNR AND M_REVISION.EQUNR = M_PMORDER.EQUNR', 'left');
        // $this->db_dev2->join('M_PMORDER', 'M_REVISION.EQUNR = M_PMORDER.EQUNR', 'left');
        // $this->db_dev2->join('M_REVISIONDETAIL', 'M_REVISION.REVNR = M_REVISIONDETAIL.REVNR');
        // $this->db_dev2->join('(SELECT
        //                   M_PMORDER.REVNR,
        //                   M_SALESORDER.VBELN,
        //                   M_SALESORDER.KUNNR,
        //                   M_SALESORDER.NAME1
        //                 FROM
        //                   M_PMORDER
        //                 LEFT OUTER JOIN M_SALESORDER ON M_SALESORDER.VBELN = M_PMORDER.KDAUF
        //                 WHERE
        //                   M_SALESORDER.NAME1 IS NOT NULL
        //                 GROUP BY
        //                   M_PMORDER.REVNR,
        //                   M_SALESORDER.VBELN,
        //                   M_SALESORDER.KUNNR,
        //                   M_SALESORDER.NAME1) M_SALESORDER',
        //   'M_REVISION.REVNR = M_SALESORDER.REVNR');
        // $this->db_dev2->where('YEAR(M_REVISION.REVBD)', $year);
        // $this->db_dev2->like('M_PMORDER.ATWRT', $product_type, 'BOTH');
        // $this->db_dev2->where("CONVERT(DATE, M_REVISION.REVED, 112) IS NOT NULL"); // by me
        // $this->db_dev2->where("(status like '%TECO%' OR status like '%CLSD%')");
        // $this->db_dev2->like('status', "TECO", 'BOTH');
        // $this->db_dev2->or_like('status', "CLSD", 'BOTH');
        // $this->db_dev2->where('M_PMORDER.ATWRT', $product_type);
        // $this->db_dev2->where('status', "TECO");
        // $this->db_dev2->or_where('status', "CLSD");
        // $this->db_dev2->group_by('MONTH(M_REVISION.REVBD)');
        // $query = $this->db_dev2->get();
        // return $query->result();
      }

      function get_datatables_($month, $where)
      {
        $year = date('Y');
        $this->_get_datatables_query();
        if ($month!=-1) {
          $this->db_dev->where('MONTH(MONTH)', $month);
        }
        $this->db_dev->where($where);
        $this->db_dev->where('YEAR(YEAR)', $year);
        // $this->db_dev2->like('M_PMORDER.ATWRT', $prod_type, 'BOTH');
        if($_POST['length'] != -1)
          $this->db_dev->limit($_POST['length'], $_POST['start']);
          $query = $this->db_dev->get();
        return $query->result();
      }



        public function count_all_($month, $where)
        {
          $year = date('Y');
          $this->_get_datatables_query();
          if ($month!=-1) {
            $this->db_dev->where('MONTH(MONTH)', $month);
          }
          $this->db_dev->where($where);
          $this->db_dev->where('YEAR(MONTH)', $year);
          // $this->db_dev->like('M_PMORDER.ATWRT', $prod_type, 'BOTH');
          $query = $this->db_dev->get();
          return $query->num_rows();
        }

        function count_filtered_($month) //, $where
        {
          $year = date('Y');
          $this->_get_datatables_query();
          if ($month!=-1) {
            $this->db_dev->where('MONTH(MONTH)', $month);
          }
          // $this->db_dev->where($where);
          $this->db_dev->where('YEAR(MONTH)', $year);
          // $this->db_dev->like('M_PMORDER.ATWRT', $prod_type, 'BOTH');
          $query = $this->db_dev->get();
          return $query->num_rows();
        }


}
