<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_slaline extends CI_Model{

    
    public function __construct(){
      parent::__construct();
      $this->load->database();
      $this->dev_gmf = $this->load->database('dev_gmf', TRUE);
	  
	  
    }
	
	
	public function get_code_child($aircraft, $tipe)
       {
		   if($tipe=='general'){
				$query = $this->dev_gmf->query("
				SELECT ki_name, ki_id from kpi_item where ki_parent_id = (SELECT ki_id from kpi_item where ki_name = 'SLA GA' and ki_parent_id = (SELECT ki_id from kpi_item where ki_name='SLA Line Operations'))
				UNION
				SELECT ki_name, ki_id from kpi_item where ki_parent_id = (SELECT ki_id from kpi_item where ki_name = 'SLA QG' and ki_parent_id = (SELECT ki_id from kpi_item where ki_name='SLA Line Operations'))
				");
		   }else{
				$query = $this->dev_gmf->query("
				SELECT ki_name, ki_id from kpi_item where ki_parent_id = (SELECT ki_id from kpi_item where ki_name='{$aircraft}' and ki_parent_id = (SELECT ki_id from kpi_item where ki_name='SLA Line Operations'))
				");
		   }	
		  $query = $query->result_array();
		  
		  $temp_child = array();
		   foreach ($query as $key => $val) {
			   $reg = $val['ki_id'];
				$temp_child[]= $reg;
		   };
			$child= "'".join("','" ,$temp_child)."'";	
		   
		   $query1 = $this->dev_gmf->query("
					select kia_id from kpi_item_assignment where ki_id in ({$child})
				");
		  $query1 = $query1->result_array();
           
           return $query1;
       }
	   
	   
	
	public function get_grafik_parent($child, $start, $end)
       {
		   
		   $awal = explode("-",$start);
		   $awal_bulan = $awal[0];
		   $awal_tahun = $awal[1];
		   $akhir = explode("-",$end);
		   $akhir_bulan = $akhir[0];
		   $akhir_tahun = $akhir[1];
           $query = $this->dev_gmf->query("
					SELECT kiad_date, avg(kiad_actual) as rata_actual from kpi_item_assigment_detail where kia_id in({$child}) and MONTH(kiad_date) >= '{$awal_bulan}' and YEAR(kiad_date) >= '{$awal_tahun}' and  MONTH(kiad_date) <= '{$akhir_bulan}' and YEAR(kiad_date) <= '{$akhir_tahun}'
					GROUP BY kiad_date
				");
		  $query = $query->result_array();
		  // echo $this->dev_gmf->last_query();
           return $query;
       }
	   
	public function get_grafik_parent_two($child, $tanggal)
       {
		   
		   $awal = explode("-",$tanggal);
		   $awal_bulan = $awal[1];
		   $awal_tahun = $awal[0];
           $query = $this->dev_gmf->query("
					SELECT kiad_date, avg(kiad_actual) as rata_actual from kpi_item_assigment_detail where kia_id in({$child}) and MONTH(kiad_date) = '{$awal_bulan}' and YEAR(kiad_date) = '{$awal_tahun}' 
					GROUP BY kiad_date
				");
		  $query = $query->row_array();
		  // echo $this->dev_gmf->last_query();
           return $query;
       }
	   
	   
	public function count_child($aircraft, $tahun)
       {
		   
		  $query = $this->dev_gmf->query("
				SELECT kpi_item.ki_name, kpi_item.ki_id, kpi_item_assignment.kia_id from kpi_item
				LEFT JOIN kpi_item_assignment on kpi_item.ki_id = kpi_item_assignment.ki_id
				where ki_parent_id = (SELECT ki_id from kpi_item where ki_name='{$aircraft}' and ki_parent_id = (SELECT ki_id from kpi_item where ki_name='SLA Line Operations' and YEAR(ki_year_date)='{$tahun}'))
				");
		  
		  $query = $query->result_array();
		  // echo $this->dev_gmf->last_query();
           return $query;
       }
	   
	public function get_grafik_detail_performance($kia_id, $start, $end)
       {
		   
		   $awal = explode("-",$start);
		   $awal_bulan = $awal[0];
		   $awal_tahun = $awal[1];
		   $akhir = explode("-",$end);
		   $akhir_bulan = $akhir[0];
		   $akhir_tahun = $akhir[1];
		  $query = $this->dev_gmf->query("
				select * from kpi_item_assigment_detail where kia_id='{$kia_id}' and MONTH(kiad_date) >= '{$awal_bulan}' and YEAR(kiad_date) >= '{$awal_tahun}' and  MONTH(kiad_date) <= '{$akhir_bulan}' and YEAR(kiad_date) <= '{$akhir_tahun}'
				");
		  
		  $query = $query->result_array();
           return $query;
       }
	public function get_grafik_detail_target($ki_id, $start, $end)
       {
		   
		   $awal = explode("-",$start);
		   $awal_bulan = $awal[0];
		   $awal_tahun = $awal[1];
		   $akhir = explode("-",$end);
		   $akhir_bulan = $akhir[0];
		   $akhir_tahun = $akhir[1];
		  $query = $this->dev_gmf->query("
				select * from kpi_item_detail where ki_id='{$ki_id}' and MONTH(kid_date) >= '{$awal_bulan}' and YEAR(kid_date) >= '{$awal_tahun}' and  MONTH(kid_date) <= '{$akhir_bulan}' and YEAR(kid_date) <= '{$akhir_tahun}'
				");
		  // echo $this->dev_gmf->last_query();
		  $query = $query->result_array();
           return $query;
       }
	   
	   
	public function get_idparent($aircraft, $tahun)
       {
		   $tambahan = '';
		   if($aircraft!='SLA Line Operations'){
			   $tambahan = " and ki_parent_id = (SELECT ki_id from kpi_item where ki_name='SLA Line Operations' and YEAR(ki_year_date)='{$tahun}')";
		   }
		  $query = $this->dev_gmf->query("
				select * from kpi_item where ki_name = '{$aircraft}' {$tambahan}
				");
		  
		  $query = $query->row_array();
		  // echo $this->dev_gmf->last_query();
           return $query;
       }
	   
	public function cek_tipe_formula($ki_id)
       {
		  $query = $this->dev_gmf->query("
				select ki_id, ki_uom, kpi_formula.* from kpi_item
				join kpi_formula on kpi_item.kf_id = kpi_formula.kf_id 
				where ki_id = '{$ki_id}'
				");
		  
		  $query = $query->row_array();
           return $query;
       }
	
}	