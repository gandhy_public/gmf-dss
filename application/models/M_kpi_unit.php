<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class M_kpi_unit extends CI_Model
  {
    public function __construct()
    {
      parent::__construct();
      $this->dev_gmf = $this->load->database("dev_gmf", true);
      $this->load->database("default");
      $this->load->database();
    }

    public function the_year($kia_id)
    {
      $sql = "SELECT DATEPART(
        YEAR,
        CONVERT (DATE, kia_year_date, 20)
      ) as year from kpi_item_assignment where kia_id = '".$kia_id."'";
      $exec = $this->dev_gmf->query($sql)->row();
      return $exec->year;
    }

    public function member_group_where($id,$role)
    {
      $sql = "select kia_id, (kia_name+' - '+CONVERT(VARCHAR, YEAR(kia_year_date))) as kia_name
      from kpi_item_assignment where kpi_group_id = '".$id."' and role_id ='".$role."' ORDER BY kia_year_date DESC";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function get_all_parent($role)
    {
      $sql = "SELECT
              	kia_id,
              	(
              		kia_name + ' - ' + CONVERT (
              			VARCHAR,
              			YEAR (kia_year_date)
              		)
              	) AS kia_name
              FROM
              	kpi_item_assignment
              WHERE
              	(
              		kia_parent_id = '0'
              		OR kia_parent_id IS NULL
              	)
              AND (
              	kpi_group_id IS NULL
              	OR kpi_group_id = '0'
              )
              AND role_id = '".$role."'
              ORDER BY
              	kia_year_date DESC";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function loadGrup()
    {
      $sql = "select * from kpi_grup";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function set_member_where($ki_id, $kpi_group_id,$role)
    {
      $sql = "update kpi_item_assignment set kpi_group_id = '".$ki_id."' where kia_id = '".$kpi_group_id."' and role_id = '".$role."'";
      $exec = $this->dev_gmf->query($sql);
      return ($exec) ? true : false;
    }

    public function delete_kia_where($kia_id)
    {
      $return = [];
      $year = $this->the_year($kia_id);
      $sql1 = "delete from kpi_item_assigment_detail where
               kia_id = '".$kia_id."'
               and
               DATEPART(
                 YEAR,
                 CONVERT (DATE, kiad_date, 20)
               ) = '".$year."'";
      $exec1 = $this->dev_gmf->query($sql1);
      if($exec1){
        $sql = "delete from kpi_item_assignment where kia_id = '".$kia_id."'";
        $exec = $this->dev_gmf->query($sql);
        $return["success"] = ($exec) ? true : false;
      }
      return $return;
    }

    public function get_kpi_assigment_where($role_id)
    {
      $sql = "SELECT
              	kia_id, kia_name, ki_id
              FROM
              	kpi_item_assignment
              WHERE
              	role_id = '".$role_id."'
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, kia_year_date, 20)
              ) = '2018'";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function insert_assign_kpi()
    {
      $kpi = $this->input->post("kpi");
      $role_id = $this->input->post("role_id");
      $boolean = false;
      foreach ($kpi as $ki_id) {
        $sql = "INSERT INTO kpi_item_assignment (
                  kf_id,
                  kia_author,
                  kia_name,
                  kia_uom,
                  kia_level,
                  kia_year_date,
                  kia_parent_id,
                  role_id,
                  ki_id
                ) SELECT
                  kf_id,
                  ki_author,
                  ki_name,
                  ki_uom,
                  '' AS a,
                  ki_year_date,
                  '' AS b,
                  '".$role_id."' AS c,
                  '".$ki_id."' AS d
                FROM
                  kpi_item
                WHERE
                  ki_id = '".$ki_id."'";
          $exec1 = $this->dev_gmf->query($sql);
          if($exec1){
            $insert_id = $this->dev_gmf->insert_id();;
            $sql2 = "INSERT INTO kpi_item_assigment_detail (
                    	kia_id,
                    	kiad_date,
                    	kiad_target,
                    	kiad_weight,
                    	kiad_limit,
                    	kiad_islock,
                    	kiad_author
                    ) SELECT
                    	'".$insert_id."' AS kia_id,
                    	kid_date,
                    	kid_target,
                    	kid_weight,
                    	kid_limit,
                    	kid_islock,
                    	kid_author
                    FROM
                    	kpi_item_detail
                    where ki_id = '".$ki_id."'";
            $exec2 = $this->dev_gmf->query($sql2);
            $boolean = ($exec2) ? true : false;
          }
      }
      return array("success" => $boolean);
    }

    public function show_all_role()
    {
      $sql = "select * from kpi_unit";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function get_deep_child($role)
    {
      $queryPlus = "";
      if ($role != "Undefined") {
        $queryPlus = " and role_id='".$role."'";
      }
      $sql = "select kia_id, kia_name from kpi_item_assignment where kia_level = 'deep_child' ".$queryPlus." order by kia_parent_id asc";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function insert_unit()
    {
      $role_id = $this->input->post("role_id");
      $sql = "update role set role_iskpi = '1' where role_id = '".$role_id."'";
      $result = array();
      if($this->db->query($sql)){
        $result["success"] = true;
      } else{
        $result["success"] = false;
      }
      return $result;
    }

    public function list_unit()
    {
      $result = array();
      $sql = "SELECT role_id, role_name from role where role_isactive = '1' and role_iskpi = '1'";
      $exec = $this->db->query($sql);
      return $exec->result();
    }

    public function role()
    {
      $result = array();
      $sql = "SELECT role_id, role_name from role where role_isactive = '1' and role_iskpi = '0'";
      $exec = $this->db->query($sql);
      return $exec->result();
    }

    public function getAllFormula()
    {
      $result = array();
      $sql = "SELECT kf_id, kf_name FROM kpi_formula";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function getAllItemKpi($role,$year)
    {
      $result = array();
      $sql = "SELECT a.*, b.kf_name FROM kpi_item_assignment a
      join kpi_formula b on a.kf_id = b.kf_id
      where role_id='".$role."' and DATEPART( YEAR,
      CONVERT (DATE, kia_year_date, 20)) ='".$year."' ";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function getAllItemKpiWithDetail()
    {
      $nowYear = $this->input->post("year");
      $role = $this->input->post("role");
      $nowMonth = date("m");
      $result = array();
      $sql = "SELECT
          a.*,
          b.kf_name,
          b.fa_function_name,
          c.kiad_id,
          c.kiad_date,
          c.kiad_target,
          c.kiad_target_ytd,
          c.kiad_weight,
          c.kiad_limit,
          c.kiad_islock,
          c.kiad_author
          FROM kpi_item_assignment a JOIN kpi_formula b ON a.kf_id = b.kf_id LEFT JOIN kpi_item_assigment_detail c
          on a.kia_id = c.kia_id and DATEPART( year, CONVERT ( DATE, c.kiad_date, 20 ) ) = '".$nowYear."'
          and DATEPART( month, CONVERT ( DATE, c.kiad_date, 20 ) ) = '".$nowMonth."' where role_id='".$role."'
           and DATEPART( year, CONVERT ( DATE, a.kia_year_date, 20 ) ) = '".$nowYear."'";
      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function deleteDetailKpiWhere($ki_id, $year)
    {
      $sql = "delete from kpi_item_assigment_detail where kia_id = '".$ki_id."'
              and DATEPART(
                  year,
                  CONVERT (DATE, kiad_date, 20)
                ) = '".$year."'";
      $exec = $this->dev_gmf->query($sql);
      return ($exec) ? true : false;
    }

    public function updateDeepChild($ki_id, $year)
    {
      $sql = "update kpi_item_assignment set kia_level = '' where kia_id = '".$ki_id."'
              and DATEPART( year, CONVERT (DATE, kia_year_date, 20)) = '".$year."'";
      $exec = $this->dev_gmf->query($sql);
    }

    public function change_lock($params)
    {
      $sql = "update kpi_lock set kpi_islocked = '".$params."'";
      $exec = $this->dev_gmf->query($sql);
      return (($exec) ? true : false);
    }

    public function saveKpi()
    {
      $ki_name = $this->input->post("ki_name");
      $ki_uom = $this->input->post("ki_uom");
      $ki_parent_id = $this->input->post("ki_parent_id");
      $kf_id = $this->input->post("kf_id");
      $ki_year = $this->input->post("ki_year");
      $ki_author = $this->input->post("ki_author");
      $ki_weight = $this->input->post("ki_weight");
      $ki_role = $this->input->post("ki_role");
      $ki_id = $this->input->post("ki_id");
      $ki_target = $this->input->post("ki_target");
      $ki_target_ytd = $this->input->post("ki_target_ytd");
      $kpi_group_id = $this->input->post("kpi_group_id");

      $deep_child = 'deep_child';
      if($ki_parent_id != ""){
        $this->deleteDetailKpiWhere($ki_parent_id, $ki_year);
        $this->updateDeepChild($ki_parent_id, $ki_year);
        $deep_child = 'deep_child';
      }
      //hardcode
      if($ki_id == "" || empty($ki_id)){
      $sql = "insert into kpi_item_assignment
              (kf_id, kia_author, kia_name, kia_uom, kia_level, kia_year_date, kia_parent_id,role_id,kia_weight,kia_target, kia_target_ytd, kpi_group_id)
              values
              ('".$kf_id."', '".$ki_author."', '".$ki_name."', '".$ki_uom."', '".$deep_child."', '".$ki_year."', '".$ki_parent_id."', '".$ki_role."','".$ki_weight."','".$ki_target."', '".$ki_target_ytd."', '".$kpi_group_id."')";
      }else{
        $sql = "Update kpi_item_assignment set kia_name = '".$ki_name."', kia_parent_id = '".$ki_parent_id."', kia_weight = '".$ki_weight."',kia_target = '".$ki_target."', kia_target_ytd = '".$ki_target_ytd."', kpi_group_id = '".$kpi_group_id."' where kia_id = '".$ki_id."'";
        $dataEdit = $this->edit_k($ki_id);
        $old_parent = $dataEdit->kia_parent_id;

        if($this->isDeep($old_parent) == true){
          $this->setDeepChild($old_parent);
        }

      }

      // if($ki_parent_id != ""){
      //   $get_weight = $this->dev_gmf->query("select kia_weight from kpi_item_assignment where kia_id='".$ki_parent_id."'");
      //   $row = $get_weight->row_array();
      //   $weight = $row['kia_weight'] + $ki_weight;
      //   $exec2 = $this->dev_gmf->query("update kpi_item_assignment set kia_weight='".$weight."' where kia_id='".$ki_parent_id."'");
      // }

     $exec = $this->dev_gmf->query($sql);
     // $allItem = $this->getAllItemKpi($ki_role);

     $result = array(
       "success" => ($exec) ? true : false,
       "isparent" => ($ki_parent_id == "") ? true : false,
       // "newData" => generatePageTreeUnit($allItem),
       "error" => []
     );
     return $result;
    }

    public function edit_k($id)
    {
      $sql = "select * from kpi_item_assignment where kia_id = '".$id."'";
      $exec = $this->dev_gmf->query($sql);
      return $exec->row();
    }

    public function isDeep($ki_id)
    {
      $sql = "select count(kia_id) as total from kpi_item_assignment where kia_parent_id = '".$ki_id."'";
      $exec = $this->dev_gmf->query($sql);
      if($exec->row()->total < 1){
        return true;
      } else{
        return false;
      }
    }

    public function setDeepChild($ki_id)
    {
      $sql = "update kpi_item_assignment set kia_level = 'deep_child' where kia_id = '".$ki_id."'";
      $exec = $this->dev_gmf->query($sql);
    }


    public function saveDetailItem()
    {
      $year = $this->input->post("year_input");
      $ki_id = $this->input->post("ki_id");
      $author = $this->input->post("author_detail");
      $target = $this->input->post("target");
      $upper = $this->input->post("upper");
      $lower = $this->input->post("lower");
      $target_ytd = $this->input->post("target_ytd");
      $upper_ytd = $this->input->post("upper_ytd");
      $lower_ytd = $this->input->post("lower_ytd");
      $limit = $this->input->post("limit");
      $kid_id = $this->input->post("kid_id");
      $ki_uom = $this->input->post("ki_uom");
      $weight = $this->input->post("weight");

      $result = array();
      $no = 1;
      foreach ($limit as $i => $data) {//$weight
        if(empty($kid_id)){
          // echo " kosong";
          // exit;
          $target_final = ($upper[$i] != null && $lower[$i] != null) ? $upper[$i]."|".$lower[$i] : $target[$i];
          $target_final_ytd = ($upper_ytd[$i] != null && $lower_ytd[$i] != null) ? $upper_ytd[$i]."|".$lower_ytd[$i] : $target_ytd[$i];

          $sql = "insert into kpi_item_assigment_detail (kia_id, kiad_date, kiad_target, kiad_weight, kiad_limit, kiad_author, kiad_target_ytd)
                  values('".$ki_id."', '".$year."-".$no."-01', '".$target_final."', '".$weight[$i]."', '".$limit[$i]."', '".$author."', '".$target_final_ytd."')";
          $exec = $this->dev_gmf->query($sql);
          $no++;
        } else {
          // echo " ada";
          // exit;
          $target_final = ($upper[$i] != null && $lower[$i] != null) ? $upper[$i]."|".$lower[$i] : $target[$i];
          $target_final_ytd = ($upper_ytd[$i] != null && $lower_ytd[$i] != null) ? $upper_ytd[$i]."|".$lower_ytd[$i] : $target_ytd[$i];

          $sql = "update kpi_item_assigment_detail set
                  kiad_target = '".$target_final."',
                  kiad_target_ytd = '".$target_final_ytd."',
                  kiad_weight = '".$weight[$i]."',
                  kiad_limit = '".$limit[$i]."'
                  where kiad_id = '".$kid_id[$i]."'";
          $exec = $this->dev_gmf->query($sql);
        }
      }
      return true;
    }

    public function getDetailWhere($ki_id, $year)
    {
      $sql = "SELECT
              	*,
                DATEPART(
                  	month,
                  	CONVERT (DATE, kiad_date, 20)
                  ) as month
              FROM
              	kpi_item_assigment_detail
              WHERE
              	kia_id = '".$ki_id."'
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, kiad_date, 20)
              ) = '".$year."'";
     $exec = $this->dev_gmf->query($sql);
     return $exec->result();
    }

    public function edit($id)
    {
      $sql = "select * from kpi_item_assignment where kia_id = '".$id."'";
      $exec = $this->dev_gmf->query($sql);
      return $exec->row();
    }


        public function select_kpi_where_year($year,$role_id){
          $sql = "select
                    kia_id,
                    kf_id,
                    kia_author,
                    kia_name,
                    kia_uom,
                    kia_level,
                    kia_year_date,
                    kia_parent_id,
                    kia_weight,
                    kia_target,
                    role_id
                  from kpi_item_assignment
                  where DATEPART(
                    YEAR,
                    CONVERT (DATE, kia_year_date, 20)
                  ) = '".$year."' and role_id = '".$role_id."'";
          $exec = $this->dev_gmf->query($sql);
          return $exec->result();
        }

        function copy_kpi($array, $parentId = 0)
        {
            $copy_year2 = $this->input->post("copy_year2");
            foreach ($array as $key => $value) {
              $post_data = array(
                "kf_id" => $value->kf_id,
                "kia_author" => $value->kia_author,
                "kia_level" => $value->kia_level,
                "kia_name" => $value->kia_name,
                "kia_parent_id" => $parentId,
                "kia_target" => $value->kia_target,
                "kia_uom" => $value->kia_uom,
                "kia_weight" => $value->kia_weight,
                "kia_year_date" => $copy_year2."-01-01",
                "role_id" => $value->role_id
              );
               $this->dev_gmf->insert('kpi_item_assignment', $post_data);
                if(isset($value->children)){
                  $this->copy_kpi($value->children, $this->dev_gmf->insert_id());
                }
              }
          }

    public function check_lock()
    {
      $result;
      $sql = "select kpi_islocked from kpi_lock";
      $exec = $this->dev_gmf->query($sql);
      $isLocked = (empty($exec->row()->kpi_islocked) ? null : $exec->row()->kpi_islocked);
      if($isLocked == 1){
        $result = true;
      } else {
        $result = false;
      }
      return $result;
    }

    public function get_ytd($fa_function_name, $act, $kia_name_act, $month, $year)
    {
      $date1 = $year."-01-01";
      $month_before = ($month == 01) ? $month : $month-1;
      $date2 = $year."-".$month_before."-01";
      $sql = "SELECT SUM(a.kiad_actual) as kiad_actual_ytd FROM kpi_item_assigment_detail a join kpi_item_assignment b
              on a.kia_id = b.kia_id
              WHERE
              kiad_date BETWEEN '".$date1."' AND '".$date2."'
              AND
              b.kia_name = '".$kia_name_act."'";
      $exec = $this->dev_gmf->query($sql);
      $act_ytd_before = $exec->row()->kiad_actual_ytd;
      if($fa_function_name == "SumMax" || $fa_function_name == "SumMin"){
        return $act_ytd_before+$act;
      } else if ($fa_function_name == "Progressifmax" || $fa_function_name == "ProgressiveMin") {
        return $act;
      } else{
        return ($act_ytd_before+$act)/$month;
      }
    }

    // TAMBAHAN DIMAS
    public function remove_group ($where, $data){
      $this->dev_gmf->update('kpi_item_assignment', $data, $where);
      return $this->dev_gmf->affected_rows();
    }

    public function cek($id){
      $this->dev_gmf->from('kpi_item_assignment');
      $this->dev_gmf->where('kia_id', $id);
      $exec = $this->dev_gmf->get();
      return $exec->result();
    }

    public function edit_from_assign($id,$month,$year){
      $this->dev_gmf->select('kpi_item_assignment.kia_ida, kpi_item_assignment.kia_name, kpi_item_assignment.kia_uom, kpi_item_assignment.kia_parent_id, kpi_item_assignment.kf_id, kpi_item_assignment.kpi_group_id, kpi_item_assignment.kia_parent_id, kpi_item_assigment_detail.kiad_target as kia_target, kpi_item_assigment_detail.kiad_target_ytd as kia_target_ytd');
      $this->dev_gmf->from('kpi_item_assignment');
      $this->dev_gmf->join('kpi_item_assigment_detail', 'kpi_item_assigment_detail.kia_id=kpi_item_assignment.kia_id');
      $this->dev_gmf->where('kpi_item_assignment.kia_id',$id);
      $this->dev_gmf->where('DATEPART(MM, kpi_item_assigment_detail.kiad_date)=',$month);
      $this->dev_gmf->where('DATEPART(YYYY, kpi_item_assigment_detail.kiad_date)=',$year);
      $exec = $this->dev_gmf->get();
      return $exec->row();
    }

    public function show_value_month($kia_id){
      $this->dev_gmf->from('kpi_item_assignment');
      $this->dev_gmf->where('kia_id', $kia_id);
      $exec = $this->dev_gmf->get();
      return $exec->result();
    }


  }
