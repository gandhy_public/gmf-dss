<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class M_aircraft_serviceability extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function get_panel($param)
    {

        if ($param == "sekarang") {
            $date = " DATE <= CURDATE()";
        } else if ($param == "kemarin") {
            $date = " DATE <= SUBDATE(CURDATE(), 1)";
        }
        $tahun = date('Y');
        $tahun = $tahun . '-01-01';

        $query = $this->service->query("SELECT AVG(daily_service_ability) as daily_service_ability, AVG(target) as target
			FROM graph_4
			WHERE DATE >= '" . $tahun . "'
			AND " . $date . "
			");

        $query = $query->row();


        return $query;
    }


    public function get_panel1()
    {
        $tahun = date('Y');
        $date = $tahun . '-01-01';
        $query = $this->service->query("SELECT
			  AVG(B777) as B777,
			  AVG(B744) as B744,
			  AVG(A333) AS A333,
			  AVG(A332) AS A332,
			  AVG(B738) AS B738,
			  AVG(B735) AS B735,
			  AVG(CRJ1000) AS CRJ1000,
			  AVG(ATR72) AS ATR72,
              AVG(B7M8) AS B7M8,
			  AVG(target) as TARGET
			FROM graph_4
			WHERE date >= '" . $date . "'
				and date <= CURDATE()");

        $query = $query->row();


        return $query;
    }


    public function get_grafik($start, $end, $tipe)
    {
        // $start = "2016-01-01";
        // $end = "2016-01-08";

        $query = $this->service->query("SELECT " . $tipe . ", date, target FROM graph_4
			WHERE date >= '" . $start . "' and date <= '" . $end . "'");
        $query = $query->result();

        return $query;
    }

    public function get_grafik_by_actype($start, $end, $tipe)
    {
        $query = $this->service->query("select AC_TYPE, target from graph_4_detail where AC_TYPE='{$tipe}' and date >= '{$start}' and date <= '{$end}'");
        $query = $query->result();

        return $query;
    }

    public function get_grafik_target($start, $end, $tipe)
    {

        $query = $this->service->query("SELECT AVG(target) as target FROM graph_4
			WHERE date >= '" . $start . "' and date <= '" . $end . "'");
        $query = $query->row();

        return $query;
    }


    public function get_detail_panel($tipe, $tahun)
    {
        $bulan = (float)date('m', strtotime($this->getLastDate()->date));
        $bulan1 = $bulan;
        $value = "";
        if ($bulan1 > 1) {
            for ($x = 1; $x <= $bulan1; $x++) {
                $bulan = str_pad($x, 2, "0", STR_PAD_LEFT);
                $value .= " UNION SELECT AVG({$tipe}) AS total, '{$bulan}-{$tahun}' AS bulan,AVG(target) AS TARGET FROM graph_4
					WHERE EXTRACT(YEAR_MONTH FROM DATE) = {$tahun}{$bulan}";
            }
        }

        $query = $this->service->query("SELECT AVG({$tipe}) AS total, '01-{$tahun}' AS bulan, AVG(target) AS TARGET FROM graph_4
				WHERE EXTRACT(YEAR_MONTH FROM DATE) = {$tahun}01
					{$value}
				");
        $query = $query->result();

        return $query;
    }

    public function get_detail_panel_by_actype($tipe, $tahun)
    {
        $bulan = (float)date('m', strtotime($this->getLastDate()->date));
        $bulan1 = $bulan;
        $value = "";
        if ($bulan1 > 1) {
            for ($x = 1; $x <= $bulan1; $x++) {
                $bulan = str_pad($x, 2, "0", STR_PAD_LEFT);
                $value .= " UNION SELECT '{$bulan}-{$tahun}' AS bulan,AVG(target) AS target FROM graph_4_detail
					WHERE  AC_TYPE='{$tipe}' and EXTRACT(YEAR_MONTH FROM DATE) = {$tahun}{$bulan}";
            }
        }

        $query = $this->service->query("SELECT '01-{$tahun}' AS bulan, AVG(target) AS target FROM graph_4_detail
				WHERE  AC_TYPE='{$tipe}' and  EXTRACT(YEAR_MONTH FROM DATE) = {$tahun}01
					{$value}
				");
        $query = $query->result();

        return $query;
    }


    public function get_detail_panel_grafik($tipe, $bulan)
    {
        $bt = explode("-", $bulan);
        $bulan = $bt[0];
        $tahun = $bt[1];
        $bt = $tahun . "-" . $bulan;
        $query = $this->service->query("SELECT $tipe, DATE, target FROM graph_4
			WHERE DATE >= '{$bt}-01' AND DATE <= LAST_DAY('{$bt}-01')");
        $query = $query->result();

        return $query;
    }

    public function get_detail_panel_grafik_by_actype($tipe, $bulan)
    {
        $bt = explode("-", $bulan);
        $bulan = $bt[0];
        $tahun = $bt[1];
        $bt = $tahun . "-" . $bulan;
        $query = $this->service->query("SELECT target FROM graph_4_detail
			WHERE  AC_TYPE='{$tipe}' and   DATE >= '{$bt}-01' AND DATE <= LAST_DAY('{$bt}-01')");
        $query = $query->result();

        return $query;
    }

    public function get_detail_panel_grafik_target($tipe, $bulan)
    {
        $bt = explode("-", $bulan);
        $bulan = $bt[0];
        $tahun = $bt[1];
        $bt = $tahun . "-" . $bulan;
        $query = $this->service->query("SELECT AVG(target) as target FROM graph_4
			WHERE DATE >= '{$bt}-01' AND DATE <= LAST_DAY('{$bt}-01')");
        $query = $query->row();

        return $query;
    }

    public function show_datatable($data)
    {
        $output = array();
        $output["draw"] = $data["draw"];

        $output["data"] = array();
        if ($data["search"] != "") {
            $this->service->or_like("DATE", $data["search"]);
        }
        $this->service->select('DATE, ac_type, Grouping AS grup1,`1st_Reg (1)` AS reg1,`Maint_Remarks (6)` AS remarks1,Problem_Rect AS problem1,
			`Grouping (1)` AS grup2,`2nd_Reg (1)` AS reg2,`Maint_Remarks (7)` AS remarks2,`Problem_Rect (1)` AS problem2,
			`Grouping (2)` AS grup3,`3rd_Reg (1)` AS reg3,`Maint_Remarks (8)` AS remarks3,`Problem_Rect (2)` AS problem3,
			`Grouping (3)` AS grup4,`4th_Reg (1)` AS reg4,`Maint_Remarks (9)` AS remarks4,`Problem_Rect (3)` AS problem4,
			`Grouping (4)` AS grup5,`5th_Reg (1)` AS reg5,`Maint_Remarks (10)` AS remarks5,`Problem_Rect (4)` AS problem5,
			`Grouping (5)` AS grup6,`6th_Reg (1)` AS reg6,`Maint_Remarks (11)` AS remarks6,`Problem_Rect (5)` AS problem6,
			`Grouping (6)` AS grup7,`7th_Reg (1)` AS reg7,`Maint_Remarks (12)` AS remarks7,`Problem_Rect (6)` AS problem7,
			`Grouping (7)` AS grup8,`8th_Reg (1)` AS reg8,`Maint_Remarks (13)` AS remarks8,`Problem_Rect (7)` AS problem8,
			`Grouping (8)` AS grup9,`9th_Reg (1)` AS reg9,`Maint_Remarks (14)` AS remarks9,`Problem_Rect (8)` AS problem9,
			`Grouping (9)` AS grup10,`10th_Reg (1)` AS reg10,`Maint_Remarks (15)` AS remarks10,`Problem_Rect (9)` AS problem10');
        $this->service->from('graph_4_detail');
        $this->service->where("DATE", $data["date"]);
        $this->service->where("ac_type", $data["tipe"]);
        $this->service->limit($data["length"], $data["start"]);
        // $this->service->order_by($data["order_column"], $data["order_dir"]);
        $query = $this->service->get();

        $output["recordsTotal"] = $output["recordsFiltered"] = $query->num_rows();
        if ($data["search"] != "") {
            $this->service->select('DATE, ac_type, Grouping AS grup1,`1st_Reg (1)` AS reg1,`Maint_Remarks (6)` AS remarks1,Problem_Rect AS problem1,
					`Grouping (1)` AS grup2,`2nd_Reg (1)` AS reg2,`Maint_Remarks (7)` AS remarks2,`Problem_Rect (1)` AS problem2,
					`Grouping (2)` AS grup3,`3rd_Reg (1)` AS reg3,`Maint_Remarks (8)` AS remarks3,`Problem_Rect (2)` AS problem3,
					`Grouping (3)` AS grup4,`4th_Reg (1)` AS reg4,`Maint_Remarks (9)` AS remarks4,`Problem_Rect (3)` AS problem4,
					`Grouping (4)` AS grup5,`5th_Reg (1)` AS reg5,`Maint_Remarks (10)` AS remarks5,`Problem_Rect (4)` AS problem5,
					`Grouping (5)` AS grup6,`6th_Reg (1)` AS reg6,`Maint_Remarks (11)` AS remarks6,`Problem_Rect (5)` AS problem6,
					`Grouping (6)` AS grup7,`7th_Reg (1)` AS reg7,`Maint_Remarks (12)` AS remarks7,`Problem_Rect (6)` AS problem7,
                    `Grouping (7)` AS grup8,`8th_Reg (1)` AS reg8,`Maint_Remarks (13)` AS remarks8,`Problem_Rect (7)` AS problem8,
                    `Grouping (8)` AS grup9,`9th_Reg (1)` AS reg9,`Maint_Remarks (14)` AS remarks9,`Problem_Rect (8)` AS problem9,
                    `Grouping (9)` AS grup10,`10th_Reg (1)` AS reg10,`Maint_Remarks (15)` AS remarks10,`Problem_Rect (9)` AS problem10');
            $this->service->from('graph_4_details');
            $this->service->where("DATE", $data["date"]);
            $this->service->where("ac_type", $data["tipe"]);
            $this->service->limit($data["length"], $data["start"]);
            // $this->service->order_by($data["order_column"], $data["order_dir"]);

            $jum = $this->service->get();
            $output["recordsTotal"] = $output["recordsFiltered"] = $jum->num_rows();
        }

        $nomor_urut = $data["start"] + 1;
        foreach ($query->result_array() as $arr) {
          // $output["data"][] = array(
          //     date('d-m-Y', strtotime($arr["DATE"])),
          //     $arr["grup1"],
          //     $arr["remarks1"],
          //     $arr["reg1"],
          //     $arr["problem1"]
          // );
            if ($arr["grup1"] != "" && $arr["remarks1"] != "") {
                $output["data"][] = array(
                    date('d-m-Y', strtotime($arr["DATE"])),
                    $arr["grup1"],
                    $arr["remarks1"],
                    $arr["reg1"],
                    $arr["problem1"]
                );
                $nomor_urut++;
            }
            if ($arr["grup2"] != "" && $arr["remarks2"] != "") {
                $output["data"][] = array(
                    date('d-m-Y', strtotime($arr["DATE"])),
                    $arr["grup2"],
                    $arr["remarks2"],
                    $arr["reg2"],
                    $arr["problem2"]
                );
                $nomor_urut++;
            }
            if ($arr["grup3"] != "" && $arr["remarks3"] != "") {
                $output["data"][] = array(
                    date('d-m-Y', strtotime($arr["DATE"])),
                    $arr["grup3"],
                    $arr["remarks3"],
                    $arr["reg3"],
                    $arr["problem3"]
                );
                $nomor_urut++;
            }
            if ($arr["grup4"] != "" && $arr["remarks4"] != "") {
                $output["data"][] = array(
                    date('d-m-Y', strtotime($arr["DATE"])),
                    $arr["grup4"],
                    $arr["remarks4"],
                    $arr["reg4"],
                    $arr["problem4"]
                );
                $nomor_urut++;
            }
            if ($arr["grup5"] != "" && $arr["remarks5"] != "") {
                $output["data"][] = array(
                    date('d-m-Y', strtotime($arr["DATE"])),
                    $arr["grup5"],
                    $arr["remarks5"],
                    $arr["reg5"],
                    $arr["problem5"]
                );
                $nomor_urut++;
            }
            if ($arr["grup6"] != "" && $arr["remarks6"] != "") {
                $output["data"][] = array(
                    date('d-m-Y', strtotime($arr["DATE"])),
                    $arr["grup6"],
                    $arr["remarks6"],
                    $arr["reg6"],
                    $arr["problem6"]
                );
                $nomor_urut++;
            }
            if ($arr["grup7"] != "" && $arr["remarks7"] != "") {
                $output["data"][] = array(
                    date('d-m-Y', strtotime($arr["DATE"])),
                    $arr["grup7"],
                    $arr["remarks7"],
                    $arr["reg7"],
                    $arr["problem7"]
                );
                $nomor_urut++;
            }
            if ($arr["grup8"] != "" && $arr["remarks8"] != "") {
                $output["data"][] = array(
                    date('d-m-Y', strtotime($arr["DATE"])),
                    $arr["grup8"],
                    $arr["remarks8"],
                    $arr["reg8"],
                    $arr["problem8"]
                );
                $nomor_urut++;
            }
            if ($arr["grup9"] != "" && $arr["remarks9"] != "") {
                $output["data"][] = array(
                    date('d-m-Y', strtotime($arr["DATE"])),
                    $arr["grup9"],
                    $arr["remarks9"],
                    $arr["reg9"],
                    $arr["problem9"]
                );
                $nomor_urut++;
            }
            if ($arr["grup10"] != "" && $arr["remarks10"] != "") {
                $output["data"][] = array(
                    date('d-m-Y', strtotime($arr["DATE"])),
                    $arr["grup10"],
                    $arr["remarks10"],
                    $arr["reg10"],
                    $arr["problem10"]
                );
                $nomor_urut++;
            }

            // $nomor_urut++;
        }
        $output["recordsTotal"] = $output["recordsFiltered"] = $nomor_urut-1;
        echo json_encode($output);
    }

    public function getLastDate()
    {
        $query = $this->service->select('*')
            ->from('graph_4')
            ->order_by('date', 'desc')
            ->limit(1)
            ->get();

        return $query->row();
    }

    public function getLastDatePulsa($ac_type)
    {
      $date = date("Y-m-d");
      $sql = "SELECT pulsa from graph_4_detail where ac_type = '".$ac_type."' and date = '".$date."' limit 1";
      $exec = $this->service->query($sql);
      return (empty($exec->row()->pulsa) ? "-" : $exec->row()->pulsa);
    }

    public function getTop3Remarks($date_start,$date_end,$ac_type){
      $sql = $this->service->query("SELECT * from (
select satu+dua+tiga+empat+lima+enam+tuju+dlpn+semb+spluh as total, 'Flap' as kejadian from(
SELECT
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Flap%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as satu,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (7)` like '%Flap%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dua,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (8)` like '%FLap%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tiga,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (9)` like '%Flap%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as empat,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (10)` like '%Flap%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as lima,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (11)` like '%Flap%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as enam,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (12)` like '%Flap%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tuju,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (13)` like '%Flap%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dlpn,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (14)` like '%Flap%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as semb,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (15)` like '%Flap%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as spluh
FROM (SELECT DISTINCT(SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Flap%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') FROM graph_4_detail) a)
B
union
select satu+dua+tiga+empat+lima+enam+tuju+dlpn+semb+spluh as total, 'Engine' as kejadian from(
SELECT
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Engine%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as satu,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (7)` like '%Engine%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dua,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (8)` like '%Engine%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tiga,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (9)` like '%Engine%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as empat,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (10)` like '%Engine%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as lima,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (11)` like '%Engine%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as enam,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (12)` like '%Engine%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tuju,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (13)` like '%Engine%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dlpn,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (14)` like '%Engine%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as semb,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (15)` like '%Engine%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as spluh
FROM (SELECT DISTINCT(SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Engine%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') FROM graph_4_detail) a)
B
union
select satu+dua+tiga+empat+lima+enam+tuju+dlpn+semb+spluh as total, 'ACM' as kejadian from(
SELECT
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%ACM%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as satu,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (7)` like '%ACM%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dua,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (8)` like '%ACM%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tiga,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (9)` like '%ACM%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as empat,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (10)` like '%ACM%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as lima,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (11)` like '%ACM%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as enam,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (12)` like '%ACM%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tuju,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (13)` like '%ACM%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dlpn,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (14)` like '%ACM%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as semb,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (15)` like '%ACM%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as spluh
FROM (SELECT DISTINCT(SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%ACM%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') FROM graph_4_detail) a)
B
union
select satu+dua+tiga+empat+lima+enam+tuju+dlpn+semb+spluh as total, 'Flight Control' as kejadian from(
SELECT
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Flight Control%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as satu,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (7)` like '%Flight Control%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dua,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (8)` like '%Flight Control%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tiga,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (9)` like '%Flight Control%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as empat,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (10)` like '%Flight Control%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as lima,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (11)` like '%Flight Control%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as enam,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (12)` like '%Flight Control%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tuju,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (13)` like '%Flight Control%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dlpn,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (14)` like '%Flight Control%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as semb,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (15)` like '%Flight Control%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as spluh
FROM (SELECT DISTINCT(SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Flight Control%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') FROM graph_4_detail) a)
B
union
select satu+dua+tiga+empat+lima+enam+tuju+dlpn+semb+spluh as total, 'Shortage Material' as kejadian from(
SELECT
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Shortage Material%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as satu,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (7)` like '%Shortage Material%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dua,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (8)` like '%Shortage Material%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tiga,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (9)` like '%Shortage Material%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as empat,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (10)` like '%Shortage Material%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as lima,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (11)` like '%Shortage Material%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as enam,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (12)` like '%Shortage Material%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tuju,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (13)` like '%Shortage Material%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dlpn,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (14)` like '%Shortage Material%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as semb,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (15)` like '%Shortage Material%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as spluh
FROM (SELECT DISTINCT(SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Shortage Material%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') FROM graph_4_detail) a)
B
union
select satu+dua+tiga+empat+lima+enam+tuju+dlpn+semb+spluh as total, 'Windshield' as kejadian from(
SELECT
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Windshield%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as satu,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (7)` like '%Windshield%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dua,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (8)` like '%Windshield%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tiga,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (9)` like '%Windshield%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as empat,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (10)` like '%Windshield%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as lima,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (11)` like '%Windshield%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as enam,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (12)` like '%Windshield%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tuju,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (13)` like '%Windshield%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dlpn,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (14)` like '%Windshield%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as semb,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (15)` like '%Windshield%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as spluh
FROM (SELECT DISTINCT(SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Windshield%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') FROM graph_4_detail) a)
B
union
select satu+dua+tiga+empat+lima+enam+tuju+dlpn+semb+spluh as total, 'Slot Run Up Bay' as kejadian from(
SELECT
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Slot Run Up Bay%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as satu,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (7)` like '%Slot Run Up Bay%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dua,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (8)` like '%Slot Run Up Bay%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tiga,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (9)` like '%Slot Run Up Bay%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as empat,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (10)` like '%Slot Run Up Bay%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as lima,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (11)` like '%Slot Run Up Bay%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as enam,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (12)` like '%Slot Run Up Bay%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tuju,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (13)` like '%Slot Run Up Bay%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dlpn,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (14)` like '%Slot Run Up Bay%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as semb,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (15)` like '%Slot Run Up Bay%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as spluh
FROM (SELECT DISTINCT(SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Slot Run Up Bay%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') FROM graph_4_detail) a)
B
union
select satu+dua+tiga+empat+lima+enam+tuju+dlpn+semb+spluh as total, 'MCDU' as kejadian from(
SELECT
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%MCDU%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as satu,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (7)` like '%MCDU%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dua,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (8)` like '%MCDU%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tiga,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (9)` like '%MCDU%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as empat,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (10)` like '%MCDU%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as lima,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (11)` like '%MCDU%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as enam,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (12)` like '%MCDU%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tuju,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (13)` like '%MCDU%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dlpn,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (14)` like '%MCDU%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as semb,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (15)` like '%MCDU%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as spluh
FROM (SELECT DISTINCT(SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%MCDU%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') FROM graph_4_detail) a)
B
union
select satu+dua+tiga+empat+lima+enam+tuju+dlpn+semb+spluh as total, 'Brake' as kejadian from(
SELECT
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Brake%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as satu,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (7)` like '%Brake%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dua,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (8)` like '%Brake%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tiga,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (9)` like '%Brake%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as empat,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (10)` like '%Brake%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as lima,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (11)` like '%Brake%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as enam,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (12)` like '%Brake%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tuju,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (13)` like '%Brake%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dlpn,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (14)` like '%Brake%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as semb,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (15)` like '%Brake%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as spluh
FROM (SELECT DISTINCT(SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Brake%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') FROM graph_4_detail) a)
B
union
select satu+dua+tiga+empat+lima+enam+tuju+dlpn+semb+spluh as total, 'Hangar Slot' as kejadian from(
SELECT
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Hangar Slot%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as satu,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (7)` like '%Hangar Slot%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dua,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (8)` like '%Hangar Slot%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tiga,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (9)` like '%Hangar Slot%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as empat,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (10)` like '%Hangar Slot%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as lima,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (11)` like '%Hangar Slot%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as enam,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (12)` like '%Hangar Slot%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as tuju,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (13)` like '%Hangar Slot%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as dlpn,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (14)` like '%Hangar Slot%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as semb,
    (SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (15)` like '%Hangar Slot%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') as spluh
FROM (SELECT DISTINCT(SELECT COUNT(*) FROM graph_4_detail WHERE `Maint_Remarks (6)` like '%Hangar Slot%' and date between '$date_start' and '$date_end' and ac_type ='$ac_type') FROM graph_4_detail) a)
B
) N
order by total desc limit 3");

return $sql->result_array();
    }



    public function saveUploadData($tabel,$datas){
         
        $this->service->trans_begin();
        $this->service->insert_batch($tabel,$datas);

        if($this->service->trans_status()===FALSE){
            $this->service->trans_rollback();
            return FALSE;
        }else{
            $this->service->trans_commit();
            return TRUE;
        }

    
    }


}
