<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_tat_apu_engine extends CI_Model{

  var $table = 'tm_final';
  var $column_order = array('start_date','esn','customer', 'customer_type', 'prod_type', null);
  var $column_search = array('start_date','esn','customer');
  var $order = array('start_date' => 'asc');

  public function __construct()
  {
    parent::__construct();
      $this->load->database();
      $this->db_dev = $this->load->database('dev_gmf', TRUE);
      // $this->db_dev = $this->load->database('local_gmf_sqlsrv', TRUE);
  }

  private function _get_datatables_query(){

    $this->db_dev->select('tm_final.id_tm_final,
                       tm_final.start_date,
                       tm_final.esn,
                       tm_final.customer,
                       tm_final.prod_type,
                       tm_final.maint_type,
                       tm_final.target_tat,
                       tm_final.status,
                       tm_final.acreg,
                       tm_final.actual,
                       tm_final.close_date,
                       tm_final.customer_type,
                       tm_final.achievment'
                       );
    $this->db_dev->from($this->table);
    $i = 0;

    foreach ($this->column_search as $item)
    {
      if(!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db_dev->group_start();
          $this->db_dev->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db_dev->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db_dev->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order']))
    {
      $this->db_dev->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db_dev->order_by(key($order), $order[key($order)]);
    }
  }

  public function _emptyData($year, $type, $aircraft)
  {
    $this->db_dev->from($this->table);
    $this->db_dev->where('CUSTOMER', $year);
    $this->db_dev->where('PROD_TYPE', $type);
    $this->db_dev->where('MAINT_TYPE', $aircraft);
    $query = $this->db_dev->get();

    return $query->result();
  }

  function get_alldata()
  {
    $this->_get_datatables_query();
    $data = $this->db_dev->get();

    return $data->result();
  }

  public function getDataMonth()
  {
    $year = date('Y');
    $this->db_dev->select('DISTINCT(MONTH(start_date)) as bulan');
    $this->db_dev->from($this->table);
    $this->db_dev->where('YEAR(start_date)', $year);
    $query = $this->db_dev->get();
    return $query->result();
  }

  public function getDataChart($product_type)
  {
    $year = date('Y');
    $this->db_dev->select('sum(isnull(cast(achievment as float),0))/count(achievment) as nilai'
                      );
    $this->db_dev->from($this->table);
    $this->db_dev->where('YEAR(start_date)', $year);
    $this->db_dev->like('prod_type', $product_type, 'BOTH');
    $this->db_dev->where("(status like '%TECO%' OR status like '%CLSD%')");
    // $this->db_dev->like('status', "TECO", 'BOTH');
    // $this->db_dev->or_like('status', "CLSD", 'BOTH');
    // $this->db_dev->where('prod_type', $product_type);
    // $this->db_dev->where('status', "TECO");
    // $this->db_dev->or_where('status', "CLSD");
    $this->db_dev->group_by('MONTH(start_date)');
    $query = $this->db_dev->get();
    return $query->result();
  }

  function get_datatables($month, $where, $prod_type)
  {
    $year = date('Y');
    $this->_get_datatables_query();
    if ($month!=-1) {
      $this->db_dev->where('MONTH(start_date)', $month);
    }
    $this->db_dev->where($where);
    $this->db_dev->where('YEAR(start_date)', $year);
    $this->db_dev->like('prod_type', $prod_type, 'BOTH');
    if($_POST['length'] != -1)
      $this->db_dev->limit($_POST['length'], $_POST['start']);
      $query = $this->db_dev->get();
    return $query->result();
  }

  function count_filtered($month, $where, $prod_type)
  {
    $year = date('Y');
    $this->_get_datatables_query();
    if ($month!=-1) {
      $this->db_dev->where('MONTH(start_date)', $month);
    }
    $this->db_dev->where($where);
    $this->db_dev->where('YEAR(start_date)', $year);
    $this->db_dev->like('prod_type', $prod_type, 'BOTH');
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  public function count_all($month, $where, $prod_type)
  {
    $year = date('Y');
    $this->_get_datatables_query();
    if ($month!=-1) {
      $this->db_dev->where('MONTH(start_date)', $month);
    }
    $this->db_dev->where($where);
    $this->db_dev->where('YEAR(start_date)', $year);
    $this->db_dev->like('prod_type', $prod_type, 'BOTH');
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  public function count_event_closed($prod_type)
  {
    $year = date('Y');
    $where_like = "(status LIKE '%TECO%' OR status LIKE '%CLSD%')";
    $this->_get_datatables_query();
    $this->db_dev->where($where_like);
    $this->db_dev->where('YEAR(start_date)', $year);
    $this->db_dev->like('prod_type', $prod_type, 'BOTH');
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  public function get_by_id($id)
  {
    $this->db_dev->from($this->table);
    $this->db_dev->where('START_DATE',$id);
    $query = $this->db_dev->get();

    return $query->row();
  }

}
