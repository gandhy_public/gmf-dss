<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class M_aircraft_avaibility extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function get_panel($param)
    {

        if ($param == "sekarang") {
            $date = " DATE <= CURDATE()";
        } else if ($param == "kemarin") {
            $date = " DATE <= SUBDATE(CURDATE(), 1)";
        }
        $tahun = date('Y');
        $tahun = $tahun . '-01-01';

        $query = $this->service->query("SELECT AVG(Availability) as Availability, '90' as TARGET
			  FROM graph_n_detail
			  WHERE DATE >= '" . $tahun . "'
			  AND " . $date . "
      ");

        $query = $query->row();


        return $query;
    }


    public function get_panel1($tipe)
    {
        if ($tipe == "A320") {
            $where = "'A320'";
        } else if ($tipe == "Classic") {
            $where = "'B737-300'";
        }
        $tahun = date('Y');
        $date = $tahun . '-01-01';
        $query = $this->service->query("SELECT
        AVG(Availability) AS Availability,
		'90' AS TARGET
      FROM graph_n_detail
      WHERE DATE >= '" . $date . "'
        AND DATE <= CURDATE()
        AND ac_type IN ({$where})");

        $query = $query->row();


        return $query;
    }


    public function get_grafik($start, $end, $tipe)
    {

        if ($tipe == "A320") {
            $where = "A320";
        } else if ($tipe == "Classic") {
            $where = "B737-300";
        }

        $query = $this->service->query("SELECT ActServ, plan, Availability as " . $tipe . ", date, '90' as TARGET FROM graph_n_detail
      WHERE date >= '" . $start . "' and date <= '" . $end . "' and ac_type ='" . $where . "'");
        $query = $query->result();

        return $query;
    }

    public function get_grafik_target($start, $end, $tipe)
    {
        $query = $this->service->query("SELECT AVG(target) as target FROM graph_4
		  WHERE date >= '" . $start . "' and date <= '" . $end . "'");
        $query = $query->result();

        return $query;
    }


    public function get_detail_panel($tipe, $tahun)
    {
        if ($tipe == "A320") {
            $where = "'A320'";
        } else if ($tipe == "Classic") {
            $where = "'B737-300'";
        }

        $tipe_ = $tipe;
        $tipe = "Standby" . $tipe;
        $bulan = (float)date('m', strtotime(date('d-m-Y')));

        $bulan1 = $bulan;
        // echo $bulan;
        $value = "";
        if ($bulan1 > 1) {
            for ($x = 1; $x <= $bulan1; $x++) {
                $bulan = str_pad($x, 2, "0", STR_PAD_LEFT);
                $value .= " UNION SELECT
						  AVG(Availability) AS total,
						  '{$bulan}-{$tahun}' AS bulan,
						  '90'      AS TARGET
						FROM graph_n_detail
						WHERE EXTRACT(YEAR_MONTH FROM DATE) = {$tahun}{$bulan} AND ac_type IN ({$where})";
            }
        }

        // $query = $this->service->query("SELECT AVG({$tipe}) AS total, '01-{$tahun}' AS bulan, '90' AS TARGET FROM graph_n
        // WHERE EXTRACT(YEAR_MONTH FROM DATE) = {$tahun}01
        $query = $this->service->query("SELECT
					  AVG(Availability) AS total,
					  '01-{$tahun}' AS bulan,
					  '90'      AS TARGET
					FROM graph_n_detail
					WHERE EXTRACT(YEAR_MONTH FROM DATE) = {$tahun}01 AND ac_type IN ({$where})
					{$value}
        ");
        $query = $query->result();

        return $query;
    }


    public function get_detail_panel_grafik($tipe, $bulan)
    {
        $bt = explode("-", $bulan);
        $bulan = $bt[0];
        $tahun = $bt[1];
        $bt = $tahun . "-" . $bulan;

        if ($tipe == "A320") {
            $where = "'A320'";
        } else if ($tipe == "Classic") {
            $where = "'B737-300'";
        }

        $query = $this->service->query("SELECT ActServ, plan, availability as {$tipe}, DATE, '90' as TARGET FROM graph_n_detail
      WHERE DATE >= '{$bt}-01' AND DATE <= LAST_DAY('{$bt}-01') AND ac_type in({$where})");
        $query = $query->result();

        return $query;
    }

    public function show_datatable($data)
    {
        $date = date('Y-m-d', strtotime($data['date']));
        $output = array();
        $output["draw"] = $data["draw"];

        $output["data"] = array();
        if ($data["search"] != "") {
            $this->service->or_like("DATE", $data["search"]);
        }
        $this->service->select('DATE, Availability, Remarks, Reg, ActServ, Plan');
        $this->service->from('graph_n_detail');
        $this->service->where("DATE", $date);
        $this->service->where("ac_type", $data["tipe"]);
        //$this->service->limit($data["length"], $data["start"]);
        //$this->service->order_by($data["order_column"], $data["order_dir"]);
        $query = $this->service->get();

        $output["recordsTotal"] = $output["recordsFiltered"] = $query->num_rows();
        if ($data["search"] != "") {
            $this->service->select('DATE, Availability, Remarks, Reg, ActServ, Plan');
            $this->service->from('graph_n_detail');
            $this->service->where("DATE", $date);
            $this->service->where("ac_type", $data["tipe"]);
            // $this->service->limit($data["length"], $data["start"]);
            // $this->service->order_by($data["order_column"], $data["order_dir"]);

            $jum = $this->service->get();
            $output["recordsTotal"] = $output["recordsFiltered"] = $jum->num_rows();
        }

        $nomor_urut = $data["start"] + 1;
        foreach ($query->result_array() as $role) {
            $output["data"][] = array(
                date('d-m-Y', strtotime($role["DATE"])),
                $role["ActServ"],
                $role["Plan"],
                $role["Availability"] . " %",
                $role["Reg"],
                $role["Remarks"],
            );
            $nomor_urut++;
        }
        echo json_encode($output, JSON_PRETTY_PRINT);
    }

    public function getLastDate()
    {
        $query = $this->service->select('A320_testp AS pulsa_A320,classic_testp AS pulsa_classic,date')
            ->from('graph_n')
            ->order_by('date', 'desc')
            ->limit(1)
            ->get();

        return $query->row();
    }

    public function getTop3Remarks($actype,$date_start,$date_finish){
      $sql = $this->service->query("SELECT * FROM (select count(*) as total, 'Flap' as kejadian from graph_n_detail where Remarks like '%fLap%' and ac_type ='$actype' and Date between '$date_start' and '$date_finish'
              union
              select count(*) as total, 'Engine' as kejadian from graph_n_detail where Remarks like '%Engine%' and ac_type ='$actype' and Date between '$date_start' and '$date_finish'
              union
              select count(*) as total, 'Windshield' as kejadian from graph_n_detail where Remarks like '%Windshield%' and ac_type ='$actype' and Date between '$date_start' and '$date_finish'
              union
              select count(*) as total, 'Shortage Material' as kejadian from graph_n_detail where Remarks like '%Shortage Material%' and ac_type ='$actype' and Date between '$date_start' and '$date_finish'
              union
              select count(*) as total, 'ACM' as kejadian from graph_n_detail where Remarks like '%ACM%' and ac_type ='$actype' and Date between '$date_start' and '$date_finish'
              union
              select count(*) as total, 'Flight Control' as kejadian from graph_n_detail where Remarks like '%Flight Control%' and ac_type ='$actype' and Date between '$date_start' and '$date_finish'
              union
              select count(*) as total, 'Slot Run Up Bay' as kejadian from graph_n_detail where Remarks like '%Slot Run Up Bay%' and ac_type ='$actype' and Date between '$date_start' and '$date_finish'
              union
              select count(*) as total, 'MCDU' as kejadian from graph_n_detail where Remarks like '%MCDU%' and ac_type ='$actype' and Date between '$date_start' and '$date_finish'
              union
              select count(*) as total, 'Brake' as kejadian from graph_n_detail where Remarks like '%Brake%' and ac_type ='$actype' and Date between '$date_start' and '$date_finish'
              union
              select count(*) as total, 'Hangar Slot' as kejadian from graph_n_detail where Remarks like '%Hangar Slot%' and ac_type ='$actype' and Date between '$date_start' and '$date_finish'
            ) A order by total desc limit 3");
        return $sql->result_array();
    }


    public function insert_graph_n_bacth($datas,$tbl){
        $this->service->trans_begin();
        $this->service->insert_batch($tbl,$datas);

        if($this->service->trans_status()===FALSE){
            $this->service->trans_rollback();
            return FALSE;
        }else{
            $this->service->trans_commit();
            return TRUE;
        }

    }

   

}
