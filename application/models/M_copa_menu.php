<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_copa_menu extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }

    public function getActiveMenu() {
        $query = $this->db->query("select * from copa_menu where isactive=1 order by seq,id");
        return $query->result();
    }

    public function getMenuObject($id) {
        $query = $this->db->query("select * from copa_menu where id=?", $id);
        return $query->row(0);
    }

    public function getMenuSeqCount($seq) {
        $query = $this->db->query("select * from copa_menu where seq=?", $seq);
        return $query->num_rows();
    }

    public function updateSeqMenu($id) {
        $menu = $this->getMenuObject($id);
        if ($this->getMenuSeqCount($menu->seq) > 1) {
            $query = $this->db->query("update copa_menu set seq=seq+1 where seq>=? and id!=?", array($menu->seq, $menu->id));
        }
        return TRUE;
    }

    public function insertRefresh($interfal,$satuan)
    {
        $delete = $this->db->empty_table('refresh');
        if ($delete) {
            $data = array(
                'interfal' => $interfal,
                'satuan' => $satuan
            );
            $insert = $this->db->insert('refresh', $data);

            return $insert;
        }else{
            return $delete;
        }
    }

    public function getRefresh()
    {
        $data = $this->db->query('select * from refresh');
        return $data->result();
    }

    public function preDeleteSeqMenu($id) {
        $menu = $this->getMenuObject($id);
        $this->db->query("update copa_menu set seq=seq-1 where seq>=? and id!=?", array($menu->seq, $menu->id));
        return TRUE;
    }

}
