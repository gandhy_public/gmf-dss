<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kpi_unit_dashboard extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    $this->dev_gmf=$this->load->database('dev_gmf', true);
  }

  function all_data_by_role($month, $year, $role, $id, $type = null) {
      $sqlPlus = "";
      if($type != null){
        if($type == "grup"){
          $sqlPlus = " and kpi_group_id = '".$id."' ";
        } else if ($type == "single") {
          $sqlPlus = " and a.kia_id = '".$id."' ";
        } else if ($type == "name") {
          $sqlPlus = " and a.kia_name = '".$id."' ";
        } else {
          $sqlPlus = "";
        }
      }
      $sql = "SELECT
                a.*,
                b.kf_name,
                b.fa_function_name,
                c.kiad_id,
                c.kiad_date,
                c.kiad_target,
                c.kiad_target_ytd,
                c.kiad_limit,
                c.kiad_islock,
                c.kiad_author,
                c.kiad_actual,
                c.kiad_actual_ytd,
                c.kiad_weight
              FROM
                kpi_item_assignment a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_assigment_detail c ON a.kia_id = c.kia_id
              AND DATEPART(
                YEAR,
                CONVERT (DATE, c.kiad_date, 20)
              ) = '".$year."'
              AND DATEPART(
                MONTH,
                CONVERT (DATE, c.kiad_date, 20)
              ) = '".$month."'
              WHERE DATEPART(
                YEAR,
                CONVERT (DATE, a.kia_year_date, 20)
              ) = '".$year."'
              ".$sqlPlus."
              AND a.role_id = '$role'";

      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

  function get_target_parent($role){
    $sql   = "SELECT
                kia_target
              FROM
                kpi_item_assignment
              WHERE
                kia_parent_id = '0'
              AND role_id = $role";
    $query = $this->dev_gmf->query($sql);
    return $query->result();
  }

}
