<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_finance extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db_dss = $this->load->database('dev_gmf', TRUE);
    }

/* 
--START--
Create Mas Okky
Data konsumsi untuk label dan grafik yang MTD dan YTD
*/
    public function exec($sql)
    {
        $data = $this->db_dss->query($sql);
        return $data->result_array();
    }

    public function getDinas() {
        return $this->db_dss->get("fin_dinas");
    }

    public function getDinasByName($name) {
        $query = $this->db_dss->query("select * from fin_dinas where name like ?", $name);
        return $query->row(0);
    }

    public function getDinasById($id) {
        $query = $this->db_dss->query("select * from fin_dinas where id=?", $id);
        return $query->row(0);
    }

    public function getTipe() {
        return $this->db_dss->get("fin_tipe");
    }

    public function getTipeByName($name) {
        $query = $this->db_dss->query("select * from fin_tipe where name like ?", $name);
        return $query->row(0);
    }

    public function getTipeById($id) {
        $query = $this->db_dss->query("select * from fin_tipe where id=?", $id);
        return $query->row(0);
    }

    public function getIndikator() {
        return $this->db_dss->get("fin_indikator");
    }

    public function getIndikatorByName($name) {
        $query = $this->db_dss->query("select * from fin_indikator where name like ?", $name);
        return $query->row(0);
    }

    public function getIndikatorById($id) {
        $query = $this->db_dss->query("select * from fin_indikator where id=?", $id);
        return $query->row(0);
    }

//    public function getFinDataByYear($year) {
//        $query = $this->db_dss->query("select * from fin_data where year=?", $year);
//        return $query->result();
//    }
//
//    public function getFinDataByPeriod($period, $year) {
//        $query = $this->db_dss->query("select * from fin_data where year=? and period=?", array($year, $period));
//        return $query->result();
//    }

    public function getFinData($select = NULL, $param = NULL, $group_by = NULL, $limit = NULL, $offset = NULL) {
        if ($select) {
            $this->db_dss->select($select);
        }
        if ($param) {
            $this->db_dss->where($param);
        }
        if ($group_by) {
            $this->db_dss->group_by($group_by);
        }
        return $this->db_dss->get('fin_data', $limit, $offset);
    }

    public function getFinDataBaseMTD($tipe, $indikator, $fiscal_year, $fiscal_period = NULL, $tgl_data) {
        if ($fiscal_period) {
            return $this->db_dss->query('SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM "fin_data" WHERE ("dinas" = 1 OR "dinas" = 2 OR "dinas" = 9) AND "fiscal_year" = ? AND "fiscal_period" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data"='.$tgl_data.' GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $fiscal_period,
                        $tipe,
                        $indikator
            ));
        } else {
            return $this->db_dss->query('SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM "fin_data" WHERE ("dinas" = 1 OR "dinas" = 2 OR "dinas" = 9) AND "fiscal_year" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.' GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $tipe,
                        $indikator
            ));
        }
    }

    public function getFinDataBaseYTD($tipe, $indikator, $fiscal_year, $fiscal_period = NULL,$tgl_data) {
        if ($fiscal_period) {
            return $this->db_dss->query('WITH a AS (SELECT "fiscal_year", "fiscal_period", (SELECT SUM(value) FROM "fin_data" "fin_data2"WHERE "fin_data2"."dinas" = "fin_data"."dinas"AND "fin_data2"."fiscal_year" = "fin_data"."fiscal_year"AND "fin_data"."fiscal_period" <= "fin_data"."fiscal_period" AND "fin_data2"."tipe" = "fin_data"."tipe"AND "fin_data2"."indikator" = "fin_data"."indikator") as value FROM "fin_data"WHERE ("dinas" = 1 OR "dinas" = 2 OR "dinas" = 9 ) AND "fiscal_year" = ? AND "fiscal_period" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.') SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM a GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $fiscal_period,
                        $tipe,
                        $indikator
            ));
        } else {
            return $this->db_dss->query('WITH a AS (SELECT "fiscal_year", "fiscal_period", (SELECT SUM(value) FROM "fin_data" "fin_data2"WHERE "fin_data2"."dinas" = "fin_data"."dinas"AND "fin_data2"."fiscal_year" = "fin_data"."fiscal_year"AND "fin_data2"."fiscal_period" <= "fin_data"."fiscal_period"AND "fin_data2"."tipe" = "fin_data"."tipe"AND "fin_data2"."indikator" = "fin_data"."indikator") as value FROM "fin_data"WHERE ("dinas" = 1 OR "dinas" = 2 OR "dinas" = 9 ) AND "fiscal_year" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.') SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM a GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $tipe,
                        $indikator
            ));
        }
    }

    public function getFinDataLineMTD($tipe, $indikator, $fiscal_year, $fiscal_period = NULL,$tgl_data) {
        if ($fiscal_period) {
            return $this->db_dss->query('SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM "fin_data" WHERE ("dinas" = 6 OR "dinas" = 7 OR "dinas" = 8 OR "dinas" = 11) AND "fiscal_year" = ? AND "fiscal_period" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.' GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $fiscal_period,
                        $tipe,
                        $indikator
            ));
        } else {
            return $this->db_dss->query('SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM "fin_data" WHERE ("dinas" = 6 OR "dinas" = 7 OR "dinas" = 8 OR "dinas" = 11) AND "fiscal_year" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.' GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $tipe,
                        $indikator
            ));
        }
    }

    public function getFinDataLineYTD($tipe, $indikator, $fiscal_year, $fiscal_period = NULL,$tgl_data) {
        if ($fiscal_period) {
            return $this->db_dss->query('WITH a AS (SELECT "fiscal_year", "fiscal_period", (SELECT SUM(value) FROM "fin_data" "fin_data2"WHERE "fin_data2"."dinas" = "fin_data"."dinas"AND "fin_data2"."fiscal_year" = "fin_data"."fiscal_year"AND "fin_data2"."fiscal_period" <= "fin_data"."fiscal_period"AND "fin_data2"."tipe" = "fin_data"."tipe"AND "fin_data2"."indikator" = "fin_data"."indikator") as value FROM "fin_data"WHERE ("dinas" = 6 OR "dinas" = 7 OR "dinas" = 8 OR "dinas" = 11) AND "fiscal_year" = ? AND "fiscal_period" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.') SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM a GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $fiscal_period,
                        $tipe,
                        $indikator
            ));
        } else {
            return $this->db_dss->query('WITH a AS (SELECT "fiscal_year", "fiscal_period", (SELECT SUM(value) FROM "fin_data" "fin_data2"WHERE "fin_data2"."dinas" = "fin_data"."dinas"AND "fin_data2"."fiscal_year" = "fin_data"."fiscal_year"AND "fin_data2"."fiscal_period" <= "fin_data"."fiscal_period"AND "fin_data2"."tipe" = "fin_data"."tipe"AND "fin_data2"."indikator" = "fin_data"."indikator") as value FROM "fin_data"WHERE ("dinas" = 6 OR "dinas" = 7 OR "dinas" = 8 OR "dinas" = 11) AND "fiscal_year" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.') SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM a GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $tipe,
                        $indikator
            ));
        }
    }

    public function getFinDataCompMTD($tipe, $indikator, $fiscal_year, $fiscal_period = NULL,$tgl_data) {
        if ($fiscal_period) {
            return $this->db_dss->query('SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM "fin_data" WHERE ("dinas" = 3 OR "dinas" = 4 OR "dinas" = 10) AND "fiscal_year" = ? AND "fiscal_period" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.' GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $fiscal_period,
                        $tipe,
                        $indikator
            ));
        } else {
            return $this->db_dss->query('SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM "fin_data" WHERE ("dinas" = 3 OR "dinas" = 4 OR "dinas" = 10) AND "fiscal_year" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.' GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $tipe,
                        $indikator
            ));
        }
    }

    public function getFinDataCompYTD($tipe, $indikator, $fiscal_year, $fiscal_period = NULL,$tgl_data) {
        if ($fiscal_period) {
            return $this->db_dss->query('WITH a AS (SELECT "fiscal_year", "fiscal_period", (SELECT SUM(value) FROM "fin_data" "fin_data2"WHERE "fin_data2"."dinas" = "fin_data"."dinas"AND "fin_data2"."fiscal_year" = "fin_data"."fiscal_year"AND "fin_data2"."fiscal_period" <= "fin_data"."fiscal_period"AND "fin_data2"."tipe" = "fin_data"."tipe"AND "fin_data2"."indikator" = "fin_data"."indikator") as value FROM "fin_data"WHERE ("dinas" = 3 OR "dinas" = 4 OR "dinas" = 10) AND "fiscal_year" = ? AND "fiscal_period" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.') SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM a GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $fiscal_period,
                        $tipe,
                        $indikator
            ));
        } else {
            return $this->db_dss->query('WITH a AS (SELECT "fiscal_year", "fiscal_period", (SELECT SUM(value) FROM "fin_data" "fin_data2"WHERE "fin_data2"."dinas" = "fin_data"."dinas"AND "fin_data2"."fiscal_year" = "fin_data"."fiscal_year"AND "fin_data2"."fiscal_period" <= "fin_data"."fiscal_period"AND "fin_data2"."tipe" = "fin_data"."tipe"AND "fin_data2"."indikator" = "fin_data"."indikator") as value FROM "fin_data"WHERE ("dinas" = 3 OR "dinas" = 4 OR "dinas" = 10) AND "fiscal_year" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.') SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM a GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $tipe,
                        $indikator
            ));
        }
    }

    public function getFinDataEngineMTD($tipe, $indikator, $fiscal_year, $fiscal_period = NULL,$tgl_data) {
        if ($fiscal_period) {
            return $this->db_dss->query('SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM "fin_data" WHERE ("dinas" = 5) AND "fiscal_year" = ? AND "fiscal_period" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.' GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $fiscal_period,
                        $tipe,
                        $indikator
            ));
        } else {
            return $this->db_dss->query('SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM "fin_data" WHERE ("dinas" = 5) AND "fiscal_year" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.' GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $tipe,
                        $indikator
            ));
        }
    }

    public function getFinDataEngineYTD($tipe, $indikator, $fiscal_year, $fiscal_period = NULL,$tgl_data) {
        if ($fiscal_period) {
            return $this->db_dss->query('WITH a AS (SELECT "fiscal_year", "fiscal_period", (SELECT SUM(value) FROM "fin_data" "fin_data2"WHERE "fin_data2"."dinas" = "fin_data"."dinas"AND "fin_data2"."fiscal_year" = "fin_data"."fiscal_year"AND "fin_data2"."fiscal_period" <= "fin_data"."fiscal_period"AND "fin_data2"."tipe" = "fin_data"."tipe"AND "fin_data2"."indikator" = "fin_data"."indikator") as value FROM "fin_data"WHERE ("dinas" = 5) AND "fiscal_year" = ? AND "fiscal_period" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.') SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM a GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $fiscal_period,
                        $tipe,
                        $indikator
            ));
        } else {
            return $this->db_dss->query('WITH a AS (SELECT "fiscal_year", "fiscal_period", (SELECT SUM(value) FROM "fin_data" "fin_data2"WHERE "fin_data2"."dinas" = "fin_data"."dinas"AND "fin_data2"."fiscal_year" = "fin_data"."fiscal_year"AND "fin_data2"."fiscal_period" <= "fin_data"."fiscal_period"AND "fin_data2"."tipe" = "fin_data"."tipe"AND "fin_data2"."indikator" = "fin_data"."indikator") as value FROM "fin_data"WHERE ("dinas" = 5) AND "fiscal_year" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.') SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM a GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $tipe,
                        $indikator
            ));
        }
    }

    public function getFinDataCorpMTD($tipe, $indikator, $fiscal_year, $fiscal_period = NULL,$tgl_data) {
        if ($fiscal_period) {
            return $this->db_dss->query('SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM "fin_data" WHERE ("dinas" = 0) AND "fiscal_year" = ? AND "fiscal_period" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.' GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $fiscal_period,
                        $tipe,
                        $indikator
            ));
        } else {
            return $this->db_dss->query('SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM "fin_data" WHERE ("dinas" = 0) AND "fiscal_year" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.' GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $tipe,
                        $indikator
            ));
        }
    }

    public function getFinDataCorpYTD($tipe, $indikator, $fiscal_year, $fiscal_period = NULL,$tgl_data) {
        if ($fiscal_period) {
            return $this->db_dss->query('WITH a AS (SELECT "fiscal_year", "fiscal_period", (SELECT SUM(value) FROM "fin_data" "fin_data2"WHERE "fin_data2"."dinas" = "fin_data"."dinas"AND "fin_data2"."fiscal_year" = "fin_data"."fiscal_year"AND "fin_data2"."fiscal_period" <= "fin_data"."fiscal_period"AND "fin_data2"."tipe" = "fin_data"."tipe"AND "fin_data2"."indikator" = "fin_data"."indikator") as value FROM "fin_data"WHERE ("dinas" = 0) AND "fiscal_year" = ? AND "fiscal_period" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.') SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM a GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $fiscal_period,
                        $tipe,
                        $indikator
            ));
        } else {
            return $this->db_dss->query('WITH a AS (SELECT "fiscal_year", "fiscal_period", (SELECT SUM(value) FROM "fin_data" "fin_data2"WHERE "fin_data2"."dinas" = "fin_data"."dinas"AND "fin_data2"."fiscal_year" = "fin_data"."fiscal_year"AND "fin_data2"."fiscal_period" <= "fin_data"."fiscal_period"AND "fin_data2"."tipe" = "fin_data"."tipe"AND "fin_data2"."indikator" = "fin_data"."indikator") as value FROM "fin_data"WHERE ("dinas" = 0) AND "fiscal_year" = ? AND "tipe" = ? AND "indikator" = ? AND "tgl_data" = '.$tgl_data.') SELECT "fiscal_year", "fiscal_period", SUM(value) as value,  REPLACE(CONVERT(VARCHAR(255), (CAST(SUM(value) AS money)), 1), \'.00\', \'\') AS valuef FROM a GROUP BY "fiscal_year", "fiscal_period"', array(
                        $fiscal_year,
                        $tipe,
                        $indikator
            ));
        }
    }

/* 
--END--
*/


/* 
--START--
Create Dimas
Data konsumsi untuk tabel yang MTD, YTD dan YTE
*/


// MTD
    function data_revenue_base_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_line_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_comp_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_engine_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_other_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_base_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_line_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_comp_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_engine_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_other_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_base_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_line_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_comp_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_engine_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_other_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

        function data_expense_base_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_line_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_comp_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_engine_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_other_mtd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            fin_data.fiscal_period AS bulan,
                                            fin_data.fiscal_year AS tahun,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period = $param2
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name,
                                            fin_data.fiscal_period,
                                            fin_data.fiscal_year
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

// YTD
    function data_revenue_base_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_line_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_comp_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_engine_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_other_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_base_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_line_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_comp_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                       
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_engine_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_other_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_base_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_line_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_comp_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_engine_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_other_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_base_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_line_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_comp_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12                                       
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_engine_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_other_ytd($param2, $param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period >= $param2
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

// YTE
    function data_revenue_base_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_line_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_comp_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_engine_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_revenue_other_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 1
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_base_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_line_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_comp_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_engine_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_ebitda_other_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                       
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 2
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_base_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_line_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_comp_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                    
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_engine_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_op_other_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 3
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

        function data_expense_base_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (1, 2, 9)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                    
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_line_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (6, 7, 8, 11)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                    
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_comp_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas IN (3, 4, 10)
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'                                        
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_engine_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 5
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    function data_expense_other_yte($param3,$tgl_data){
        $query = $this->db_dss->query("SELECT
                                            fin_indikator.name AS indikator,
                                            fin_tipe.name AS tipe,
                                            SUM(fin_data.value) AS total
                                        FROM
                                            fin_data,
                                            fin_indikator,
                                            fin_tipe,
                                            fin_dinas
                                        WHERE
                                            fin_data.dinas = 0
                                        AND fin_data.fiscal_period >= 1
                                        AND fin_data.fiscal_period <= 12
                                        AND fin_data.fiscal_year = '$param3'
                                        AND fin_data.indikator = fin_indikator.id
                                        AND fin_data.indikator = 4
                                        AND fin_data.dinas = fin_dinas.id
                                        AND fin_data.tipe = fin_tipe.id
                                        AND fin_data.tgl_data = '$tgl_data'
                                        GROUP BY
                                            fin_indikator.name,
                                            fin_tipe.name
                                        ORDER BY
                                            fin_indikator.name,
                                            fin_tipe.name");
        return $query->result();
    }

    var $table  = 'fin_data';
    var $column_order = array('fin_indikator.name as indikator', 'fin_tipe.name as tipe', 'fin_dinas.name as dinas', 'fin_data.fiscal_period as period', 'fin_data.year as year', 'fin_data.value');
    var $column_search = array('fin_indikator.name as indikator', 'fin_tipe.name as tipe', 'fin_dinas.name as dinas', 'fin_data.fiscal_period as period', 'fin_data.year as year', 'fin_data.value'); 
    var $order = array('fin_data.id' => 'DESC');  

    private function query(){

        $this->db_dss->distinct();
        $this->db_dss->select('name_file, tgl_data, upload_by');
        //$this->db_dss->select("fin_indikator.name as indikator, fin_tipe.name as tipe, fin_dinas.name as dinas, fin_data.fiscal_period as period, fin_data.fiscal_year as year, fin_data.value");
        $this->db_dss->from('fin_data');
        //$this->db_dss->join('fin_indikator', 'fin_indikator.id=fin_data.indikator');
        //$this->db_dss->join('fin_tipe', 'fin_tipe.id=fin_data.tipe');
        //$this->db_dss->join('fin_dinas', 'fin_dinas.id=fin_data.dinas');
        $i = 0;
    
      foreach ($this->column_search as $item) 
      {
        if(!empty($_POST['search']['value'])) // if datatable send POST for search
        {
          
          if($i===0) // first loop
          {
            $this->db_dss->group_start(); 
            $this->db_dss->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db_dss->or_like($item, $_POST['search']['value']);
          }

          if(count($this->column_search) - 1 == $i) //last loop
            $this->db_dss->group_end(); //close bracket
        }
        $i++;
      }
      
      if(isset($_POST['order'])) 
      {
        $this->db_dss->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      } 
      else if(isset($this->order))
      {
        $order = $this->order;
        //$this->db_dss->order_by(key($order), $order[key($order)]);
      }
    }
        

    function get_datatables_finance(){

        $this->query();
        if($_POST['length'] != -1)
        //$this->db_dss->limit($_POST['length'], $_POST['start']);
        $this->db_dss->order_by('tgl_data','DESC');
        $query = $this->db_dss->get();
        return /*$this->db_dss->last_query();*/$query->result();
    }

    function count_filtered_finance(){

        $this->query();
        $query = $this->db_dss->get();
        return $query->num_rows();
    }

    function count_all_finance(){

        $this->query();
        $query = $this->db_dss->get();
        return $query->num_rows();
    }

    public function fetch($table, $field = [], $where = [], $page = 0, $limit = 10)
    {
        try
        {
            if(!empty($field)){
                $field_select = implode(',', $field);
            }else{
                $field_select = '*';
            }

            $query = $this->db_dss->select($field_select)
                            ->from($table)
                            ->where($where)
                            ->limit($limit, $page)
                            ->get();

            $data = $query->result_array();

            $total_row = $this->db_dss->select('*')
                            ->from($table)
                            ->where($where)
                            ->count_all_results();

            $total_page = ceil($total_row / $limit);

            return [                
                'limit'         => $limit,
                'total_row'     => $total_row,
                'total_data'    => count($data),
                'total_page'    => $total_page,
                'current_page'  => $page,
                'data'          => $data
            ];
        }
        catch (Exception $e)
        {
            return [];
        }       
    }

    public function delete($table,$where)
    {
        try
        {
            $delete = $this->db_dss->delete($table, $where);
            if(!$delete) throw new Exception("Failed Delete", 1);
            
            return [
                'codestatus'    => 'S',
                'message'       => 'Berhasil',
                'resultdata'    => [],
            ];
        }
        catch (Exception $e)
        {
            return [
                'codestatus'    => 'E',
                'message'       => $e->getMessage(),
                'resultdata'    => [],
            ];
        }
    }

/* 
--END--
*/

}
