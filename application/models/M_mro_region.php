
<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  /**
   *
   */
  class M_mro_region extends CI_Model
  {

    public function __construct()
    {
      parent::__construct();
    }

    public function _getAllData(){
      $data = $this->db->get('mro_region');

      return $data;
    }

    public function _getCountData(){
      $this->db->from('mro_region');
      $query = $this->db->get();
      $rowcount = $query->num_rows();

      return $rowcount;
    }

    public function getAllData()
    {
      $data = $this->db->get('mro_region');

      return $data->result_array();
    }

    public function getByName($name){
      $this->db->where('region', $name);
      $query = $this->db->get('mro_region');

      return $query->row();
    }

    public function insert($data){
      $this->db->insert('mro_region', $data);

      $id = $this->db->insert_id();
      return (isset($id)) ? $id : FALSE;
    }

  }