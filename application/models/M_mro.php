
<?php
ini_set('memory_limit', "256M");
  defined('BASEPATH') OR exit('No direct script access allowed');

  /**
   *
   */
  class M_mro extends CI_Model
  {

    public function __construct()
    {
      parent::__construct();
    }

    public function _emptyData(){
      $this->db->from('mro_transaction');
      $query = $this->db->get();
      $rowcount = $query->num_rows();

      if ($query->num_rows() > 0){
        return false;
      }else{
        return true;
      }
      
    }

    public function _getCountData(){
      $this->db->from('mro_transaction');
      $query = $this->db->get();
      $rowcount = $query->num_rows();

      return $rowcount;
    }

    // public function insert($data){

    //   $status = $this->db->insert_batch('mro_transaction', $data);

    //   return $status;
    // }

    public function insert($data){

      try{
        $this->db->trans_begin();

        for ($i=0; $i < count($data); $i++) { 
          $this->db->insert_batch('mro_transaction', $data[$i]);
        }
        // throw new Exception("Server Error");

      }catch(Exception $e){
        $this->db->trans_rollback();
        $status = false;
      }

      if ($this->db->trans_status() === FALSE){
              $this->db->trans_rollback();
              $status = false;
      }else{
              $this->db->trans_commit();
              $status = true;
      }

      return $status;
    }


    //===========DATATABLE============\\
    var $table  = 'mro_transaction';
    var $column_order = array('id_mro','mro_transaction.year','mro_operator.name','mro_transaction.aircraft_type','mro_engine_family.name','mro_transaction.expense_type','mro_transaction.total_cost');
    var $column_search = array('mro_transaction.year', 'mro_operator.name', 'mro_transaction.aircraft_type', 'mro_engine_family.name',  'mro_transaction.expense_type', 'mro_transaction.total_cost'); 
    var $order = array('id_mro' => 'ASC');  

    private function query(){

        $this->db->select("mro_transaction.year, mro_operator.name as nm_operator, mro_transaction.aircraft_type, mro_engine_family.name as nm_engine,  mro_transaction.expense_type, mro_transaction.total_cost");
        $this->db->from('mro_transaction');
        $this->db->join('mro_operator', 'mro_operator.id_operator=mro_transaction.id_operator');
        $this->db->join('mro_engine_family', 'mro_engine_family.id_engine_family=mro_transaction.id_engine_family');
        $this->db->order_by('mro_transaction.year', 'DESC');
        $i = 0;
    
      foreach ($this->column_search as $item) 
      {
        if(!empty($_POST['search']['value'])) // if datatable send POST for search
        {
          
          if($i===0) // first loop
          {
            $this->db->group_start(); 
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }

          if(count($this->column_search) - 1 == $i) //last loop
            $this->db->group_end(); //close bracket
        }
        $i++;
      }
      
      if(isset($_POST['order'])) 
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      } 
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
        

    function get_datatables_mro(){

        $this->query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_mro(){

        $this->query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all_mro(){

        $this->query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function delete($id)
    {
      $this->db->where('id_mro_history_upload', $id);
      $delete = $this->db->delete($this->table);

      return $delete;
    }


  }