
<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class M_menu extends CI_Model
  {
     public function __construct()
      {
        parent::__construct();
      }

      public function checkMenu($menu_item_link)
      {
        $sql = "select count(menu_item_link) as result from menu_item where menu_item_link = '".$menu_item_link."' and menu_item_isactive = '1'";
        $exec = $this->db->query($sql);
        if($exec->row()->result > 0){
          return true;
        } else{
          return false;
        }
      }

      public function getPermission($id, $currentLink)
      {
        $sql = "SELECT
                  count(menu_item_id) as result
                FROM
                  (
                    SELECT DISTINCT
                      mi.menu_item_id,
                      mi.menu_item_name,
                      mi.menu_item_link,
                      u.user_id
                    FROM
                      USER u
                    JOIN user_role ur ON (u.user_id = ur.user_id)
                    JOIN role ro ON (ur.role_id = ro.role_id)
                    JOIN menu_role mr ON (ro.role_id = mr.role_id)
                    JOIN menu_item mi ON (
                      mr.menu_item_id = mi.menu_item_id
                    )
                    WHERE
                      u.user_id = '$id'
                    AND mi.menu_item_link = '$currentLink'
                  ) a";
        $exec = $this->db->query($sql);
        if($exec->row()->result > 0){
          return true;
        } else{
          return false;
        }
      }

      public function tampil()
      {
        // $this->db->where("menu_item_isparent", "0");
        // $this->db->where("menu_item_isactive", "1");
        $query = $this->db->query("select menu_item.*, menu_role.role_id FROM `menu_item`
      JOIN menu_role ON menu_role.menu_item_id=menu_item.menu_item_id
      WHERE  `menu_item_isactive` = '1' order by menu_item.menu_item_id");
        $data = array();
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                // $this->db->where("menu_item_isparent", $row->menu_item_id);
                // $this->db->where("menu_item_isactive", "1");

        if($row->role_id!=''){
          // $query2 = $this->db->query("SELECT * FROM `menu_item` where  menu_item_iscenter is null and `menu_item_isactive` = '1'
          // and `menu_item_isparent` = '".$row->menu_item_id."'  order by menu_item.menu_item_id");
        // }else{
           $query2 = $this->db->query("SELECT * FROM `menu_item`
          JOIN menu_role ON menu_role.menu_item_id=menu_item.menu_item_id
          WHERE `menu_item_isparent` = '".$row->menu_item_id."' AND `menu_item_isactive` = '1'  order by menu_item.menu_item_id");
        }




                $data2 = array();
                foreach ($query2->result() as $row2) {
                  $data2[] = $row2;
                }
        $total = $query2->num_rows();
                $data[] = array(
                  'menu_item_id' => $row->menu_item_id,
                  'menu_item_name' => $row->menu_item_name,
                  'menu_item_link' => $row->menu_item_link,
                  'menu_item_icon' => $row->menu_item_icon,
                  'menu_item_isparent' => $row->menu_item_isparent,
                  'menu_item_isactive' => $row->menu_item_isactive,
                  'menu_item_iscenter' => $row->menu_item_iscenter,
                  'total' => $total,
                  'role_id' => $row->role_id,
                  'child' => $data2
                );
            }
        }


        $query1 = $this->db->query("
      SELECT menu_item.*, menu_role.role_id  FROM `menu_item`
      JOIN menu_role ON menu_role.menu_item_id=menu_item.menu_item_id
      WHERE `menu_item_iscenter` = '0' AND `menu_item_isactive` = '1' order by menu_item.menu_item_id");
        $data1 = array();
        if ($query1->num_rows() > 0)
        {
            foreach ($query1->result() as $row)
            {
        $cek = $this->db->query("SELECT * FROM `menu_item`
          WHERE `menu_item_iscenter` = '".$row->menu_item_id."' AND `menu_item_isactive` = '1'  order by menu_item.menu_item_id");
        $total = $cek->num_rows();
                $data1[] = array(
                  'menu_item_id' => $row->menu_item_id,
                  'menu_item_name' => $row->menu_item_name,
                  'menu_item_link' => $row->menu_item_link,
                  'menu_item_icon' => $row->menu_item_icon,
                  'menu_item_isparent' => $row->menu_item_isparent,
                  'menu_item_isactive' => $row->menu_item_isactive,
                  'menu_item_iscenter' => $row->menu_item_iscenter,
                  'total' => $total,
                );
            }
        }


        return  array("data" => $data , "data1" => $data1 );
      }

      public function del_menu($id)
      {
        // $id = GetSaveTextPass("GMF", $id);
        $query = "delete from menu_item where menu_item_id = '".$id."'";
        // echo $query; exit;
        if($this->db->query($query)){
          $query2 = "delete from menu_item where menu_item_isparent = '".$id."'";
          return ($this->db->query($query2)) ? true : false;
        } else {
          return false;
        }
      }

      public function get_menu_item_where($id)
      {
        $this->db->where("menu_item_id", GetSaveTextPass("GMF", $id));
        $this->db->where("menu_item_isactive", "1");
        $query = $this->db->get("menu_item");

        // if($query->num_rows() > 0){
        //   foreach($query->result() as $menu) {
        //     $data = $menu;
        //   }
        // }
        return $query->row();

      }

      public function save_menu()
      {
        $menu_item_name = $this->input->post("menu_item_name");
        $menu_item_link = $this->input->post("menu_item_link");
        $menu_item_icon = $this->input->post("menu_item_icon");
        $menu_item_iscenter = $this->input->post("menu_item_iscenter");
        $menu_item_isparent = $this->input->post("menu_item_isparent");
    if($menu_item_iscenter=='center'){
      $menu_item_iscenter = '0';
      $menu_item_isparent = '0';
      $sql = "
          INSERT INTO menu_item
          (menu_item_name, menu_item_link, menu_item_icon, menu_item_isparent, menu_item_isactive, menu_item_iscenter)
          VALUES('".$menu_item_name."', '".$menu_item_link."', '".$menu_item_icon."', ".$menu_item_isparent.", '1', ".$menu_item_iscenter.")
           ";
    }else if($menu_item_isparent!='0'){

      $sql = "
          INSERT INTO menu_item
          (menu_item_name, menu_item_link, menu_item_icon, menu_item_isparent, menu_item_isactive)
          VALUES('".$menu_item_name."', '".$menu_item_link."', '".$menu_item_icon."', ".$menu_item_isparent.", '1')
           ";
    }else{
      $sql = "
          INSERT INTO menu_item
          (menu_item_name, menu_item_link, menu_item_icon, menu_item_isparent, menu_item_isactive, menu_item_iscenter)
          VALUES('".$menu_item_name."', '".$menu_item_link."', '".$menu_item_icon."', ".$menu_item_isparent.", '1', ".$menu_item_iscenter.")
           ";

    }
         return ($this->db->query($sql)) ? true : false;
      }

      public function update_menu()
      {
        $menu_item_id = GetSaveTextPass("GMF", $this->input->post("menu_item_id"));
        $menu_item_name = $this->input->post("menu_item_name");
        $menu_item_link = $this->input->post("menu_item_link");
        $menu_item_icon = $this->input->post("menu_item_icon");
        $menu_item_iscenter = $this->input->post("menu_item_iscenter");
        $menu_item_isparent = $this->input->post("menu_item_isparent");
    if($menu_item_isparent!='0'){
      $center = '';
    }else{
      $center =  " menu_item_iscenter = '".$menu_item_iscenter."', ";
    }
      $sql = "update menu_item set
                menu_item_name = '".$menu_item_name."',
                menu_item_link = '".$menu_item_link."',
                menu_item_icon = '".$menu_item_icon."',
        {$center}
                menu_item_isparent = '".$menu_item_isparent."'

                where menu_item_id = '".$menu_item_id."'";


        return ($this->db->query($sql)) ? true : false;
      }

      public function get_menuCenter()
      {
        $this->db->where("menu_item_iscenter", "0");
        $this->db->where("menu_item_isactive", "1");
        $query = $this->db->get("menu_item");
        $data = array();
        if($query->num_rows() > 0){
          foreach ($query->result() as $menuParent) {
            $data[] = $menuParent;
          }
        }
        return $data;
      }

      public function get_menuParent()
      {
        $this->db->where("menu_item_isparent", "0");
        $this->db->where("menu_item_isactive", "1");
        $query = $this->db->get("menu_item");
        $data = array();
        if($query->num_rows() > 0){
          foreach ($query->result() as $menuParent) {
            $data[] = $menuParent;
          }
        }
        return $data;
      }

      public function get_menuCenter_where($center, $parent)
      {
      if($center==''){
        $id = $parent;
      }else{
        $id = $center;
      }
        $query = $this->db->query("SELECT *
      FROM menu_item
      WHERE menu_item_id = {$id} and menu_item_isactive='1'");
      // echo $this->db->last_query();
        return $query->row_array();
      }

      public function get_menuParent_where($id)
      {
     if($id=='center' || $id==''){
       $id = 11111;
     }
        $query = $this->db->query("SELECT *
      FROM menu_item
      WHERE menu_item_iscenter = {$id} and menu_item_isactive='1'");
        return $query->result_array();
      }

      public function get_center($limit, $start)
       {
           $this->db->where("menu_item_iscenter", "0");
           $this->db->where("menu_item_isactive !=", "2");
           $this->db->limit($limit, $start);
           $query = $this->db->get("menu_item");
           if ($query->num_rows() > 0)
           {
             $data = array();
              return $query->result_array();
           }

           return false;
       }
      public function get_current_page_records()
       {
           $this->db->where("menu_item_isparent", "0");
           $this->db->where("menu_item_isactive !=", "2");
           $query = $this->db->get("menu_item");
           if ($query->num_rows() > 0)
           {
             $data = array();
               foreach ($query->result() as $row)
               {
                   $this->db->where("menu_item_isparent", $row->menu_item_id);
                   $this->db->where("menu_item_isactive !=", "2");
                   $query2 = $this->db->get("menu_item");
                   $data2 = array();
                   foreach ($query2->result() as $row2) {
                     $data2[] = $row2;
                   }
                   $data[] = array(
                     'menu_item_id' => $row->menu_item_id,
                     'menu_item_name' => $row->menu_item_name,
                     'menu_item_link' => $row->menu_item_link,
                     'menu_item_icon' => $row->menu_item_icon,
                     'menu_item_isparent' => $row->menu_item_isparent,
                     'menu_item_isactive' => $row->menu_item_isactive,
                     'menu_item_iscenter' => $row->menu_item_iscenter,
                     'child' => $data2
                   );
               }
         // print_r($data);
              // echo "<pre>";
              //  print_r($this->db);
              // echo "</pre>";
               // exit();
               return $data;
           }

           return false;
       }

     public function get_total()
       {
       $this->db->where("menu_item_iscenter", "0");
           // $this->db->where("menu_item_isparent", "0");
           $this->db->where("menu_item_isactive", "1");
           $this->db->from("menu_item");
           return $this->db->count_all_results();
       }

	public function change_status($id, $status){
		$id = GetSaveTextPass("GMF", $id);
        $query = "update menu_item set menu_item_isactive = '".$status."' where menu_item_id = '".$id."'";
        return ($this->db->query($query)) ? true : false;
	}


  }
