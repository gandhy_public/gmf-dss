<?php

class M_hil extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->db_dss = $this->load->database('dev_gmf', TRUE);
    }

    function master_manufacture()
    {
        $query = $this->db_dss->query("SELECT mn_id, mn_name FROM m_manufacture");
        return $query->result();
    }

    function master_reg($id)
    {
        $query = $this->db_dss->query("SELECT
                                      m_manufacture.mn_id,
                                      m_manufacture.mn_name,
                                      m_actype.actype_id,
                                      m_actype.actype
                                    FROM
                                      m_actype
                                    INNER JOIN m_acreg ON m_acreg.actype_id = m_actype.actype_id
                                    INNER JOIN m_manufacture ON m_manufacture.mn_name = m_acreg.own
                                    WHERE
                                      m_manufacture.mn_id = '$id'
                                    GROUP BY
                                      m_manufacture.mn_id,
                                      m_manufacture.mn_name,
                                      m_actype.actype_id,
                                      m_actype.actype");
        return $query->result();
    }

    function target_hil()
    {
        $date = date('Y-m-d');
        $explode = explode('-', $date);
        $explode = $explode[0];
        $query = $this->db->query("SELECT
                                    *
                                  FROM
                                    tbl_all_target
                                  WHERE
                                    target_type = 'Hill'
                                  AND target_year = '$explode'");
        return $query->result();
    }

    function count_reg_per_type_garuda($id)
    {
        $query = $this->db_dss->query("SELECT
                                        m_actype.actype_id,
                                        COUNT (m_acreg.acreg) AS total_reg
                                      FROM
                                        m_acreg
                                      INNER JOIN m_manufacture ON m_manufacture.mn_name = m_acreg.own
                                      LEFT JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                                      WHERE
                                        m_actype.actype_id = '$id'
                                      AND m_manufacture.mn_name = 'GA'
                                      GROUP BY
                                      m_actype.actype_id");
        return $query->result_array();
    }

    function count_reg_per_type_citilink($id)
    {
        $query = $this->db_dss->query("SELECT
                                        m_actype.actype_id,
                                        COUNT (m_acreg.acreg) AS total_reg
                                      FROM
                                        m_acreg
                                      INNER JOIN m_manufacture ON m_manufacture.mn_name = m_acreg.own
                                      LEFT JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                                      WHERE
                                        m_actype.actype_id = '$id'
                                      AND m_manufacture.mn_name = 'CITILINK'
                                      GROUP BY
                                      m_actype.actype_id");
        return $query->result_array();
    }

    function garuda_open()
    {
        $firstday = date('Y' . '-01-01');
        $date = date('Y-m-d');

        $query = $this->db_dss->query("SELECT m_manufacture.mn_id, m_manufacture.mn_name, m_actype.actype_id, m_actype.actype, COUNT(sap_hil_d3.notification) AS count_data
        FROM
        m_acreg
        LEFT JOIN sap_hil_d3 ON sap_hil_d3.functional_location=m_acreg.acreg
        LEFT JOIN m_actype ON m_actype.actype_id=m_acreg.actype_id
        LEFT JOIN m_manufacture ON m_manufacture.mn_name=m_acreg.own
        WHERE (sap_hil_d3.system_status LIKE '%NOPR%' OR  sap_hil_d3.system_status LIKE '%OSNO%')
        AND m_manufacture.mn_name='GA'
        and sap_hil_d3.created_on BETWEEN '$firstday' AND '$date'
        GROUP BY
        m_manufacture.mn_id,
        m_manufacture.mn_name,
        m_actype.actype_id,
        m_actype.actype");
        return $query->result_array();
    }

    function garuda_detail_open($id)
    {
        $firstday = date('Y' . '-01-01');
        $date = date('Y-m-d');

        $query = $this->db_dss->query("SELECT m_manufacture.mn_id, m_manufacture.mn_name, sap_hil_d3.notification, sap_hil_d3.functional_location AS nm_reg, ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) AS start_date, ( CASE WHEN ISDATE( sap_hil_d3.req_start ) = 1 THEN CAST ( sap_hil_d3.req_start AS DATE ) END ) AS due_date, sap_hil_d3.description, CASE WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR%' THEN 'OPEN'
    WHEN sap_hil_d3.system_status LIKE 'NOPR ORAS' THEN 'OPEN'
    WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORAS' THEN 'OPEN'
    WHEN sap_hil_d3.system_status LIKE 'OSNO' THEN 'OPEN'
    WHEN sap_hil_d3.system_status LIKE 'NOPR' THEN 'OPEN'
    WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORCR' THEN 'OPEN'
  END as system_status
        FROM
        m_acreg
        LEFT JOIN sap_hil_d3 ON sap_hil_d3.functional_location=m_acreg.acreg
        LEFT JOIN m_actype ON m_actype.actype_id=m_acreg.actype_id
        LEFT JOIN m_manufacture ON m_manufacture.mn_name=m_acreg.own
        WHERE m_actype.actype_id='$id'
        AND (sap_hil_d3.system_status LIKE '%NOPR%' OR  sap_hil_d3.system_status LIKE '%OSNO%')
        AND m_manufacture.mn_name='GA'
        and ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) BETWEEN '$firstday' AND '$date'");
        return $query->result_array();
    }

    function citilink_open()
    {
        $firstday = date('Y' . '-01-01');
        $date = date('Y-m-d');

        $query = $this->db_dss->query("SELECT m_manufacture.mn_id, m_manufacture.mn_name, m_actype.actype_id, m_actype.actype, COUNT(sap_hil_d3.notification) AS count_data
        FROM
        m_acreg
        LEFT JOIN sap_hil_d3 ON sap_hil_d3.functional_location=m_acreg.acreg
        LEFT JOIN m_actype ON m_actype.actype_id=m_acreg.actype_id
        LEFT JOIN m_manufacture ON m_manufacture.mn_name=m_acreg.own
        WHERE (sap_hil_d3.system_status LIKE '%NOPR%' OR  sap_hil_d3.system_status LIKE '%OSNO%')
        AND m_manufacture.mn_name='CITILINK'
        and sap_hil_d3.created_on BETWEEN '$firstday' AND '$date'
        GROUP BY
        m_manufacture.mn_id,
        m_manufacture.mn_name,
        m_actype.actype_id,
        m_actype.actype");
        return $query->result_array();
    }

    function citilink_detail_open($id)
    {
        $firstday = date('Y' . '-01-01');
        $date = date('Y-m-d');

        $query = $this->db_dss->query("SELECT m_manufacture.mn_id, m_manufacture.mn_name, sap_hil_d3.notification, sap_hil_d3.functional_location AS nm_reg, ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) AS start_date, ( CASE WHEN ISDATE( sap_hil_d3.req_start ) = 1 THEN CAST ( sap_hil_d3.req_start AS DATE ) END ) AS due_date, sap_hil_d3.description, CASE
    WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR%' THEN 'OPEN'
    WHEN sap_hil_d3.system_status LIKE 'NOPR ORAS' THEN 'OPEN'
    WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORAS' THEN 'OPEN'
    WHEN sap_hil_d3.system_status LIKE 'OSNO' THEN 'OPEN'
    WHEN sap_hil_d3.system_status LIKE 'NOPR' THEN 'OPEN'
    WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORCR' THEN 'OPEN'
  END as system_status
        FROM
        m_acreg
        LEFT JOIN sap_hil_d3 ON sap_hil_d3.functional_location=m_acreg.acreg
        LEFT JOIN m_actype ON m_actype.actype_id=m_acreg.actype_id
        LEFT JOIN m_manufacture ON m_manufacture.mn_name=m_acreg.own
        WHERE m_actype.actype_id='$id'
        AND (sap_hil_d3.system_status LIKE '%NOPR%' OR  sap_hil_d3.system_status LIKE '%OSNO%')
        AND m_manufacture.mn_name='CITILINK'
        and ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) BETWEEN '$firstday' AND '$date'");
        return $query->result_array();
    }

    function data_tren_hil($id_manufacture, $id_type, $week_start, $week_plus_one, $week_end)
    {
        if ($id_manufacture == "00") {
            $id_manufacture = "AND m_manufacture.mn_id in ('11','12')";
        } else if ($id_manufacture == "11") {
            $id_manufacture = "AND m_manufacture.mn_id = '11'";
        } else if ($id_manufacture == "12") {
            $id_manufacture = "AND m_manufacture.mn_id = '12'";
        }
        if ($id_type == "00") {
            $id_type = "";
        } else {
            $id_type = "AND m_actype.actype_id ='$id_type'";
        }
        $query = $this->db_dss->query("SELECT
	m_manufacture.mn_id AS id_manufacture,
	m_actype.actype_id AS id_type,
	COUNT ( sap_hil_d3.notification ) AS hil_open,
	'' AS hil_complete,
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) ) AS week_create,
	'' AS week_comp
FROM
	m_manufacture
	INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
	INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
	INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg
WHERE
	(
	(
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) ) <= '$week_start'
	AND ( system_status LIKE '%NOPR%' OR system_status LIKE '%OSNO%' )
	)
	) $id_manufacture
	$id_type
	and sap_hil_d3.created_on like '2018%'
GROUP BY
	m_manufacture.mn_id,
	m_actype.actype_id,
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) ) UNION ALL
SELECT
	m_manufacture.mn_id AS id_manufacture,
	m_actype.actype_id AS id_type,
	'' AS hil_open,
	COUNT ( sap_hil_d3.notification ) AS hil_complete,
	'' AS week_create,
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.completion ) = 1 THEN CAST ( sap_hil_d3.completion AS DATE ) END ) ) AS week_comp
FROM
	m_manufacture
	INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
	INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
	INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg
WHERE
	(
	( DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.completion ) = 1 THEN CAST ( sap_hil_d3.completion AS DATE ) END ) ) BETWEEN '$week_plus_one' AND '$week_end' AND ( system_status LIKE '%NOCO%' ) )
	) $id_manufacture
      $id_type
      and sap_hil_d3.completion like '2018%'
GROUP BY
	m_manufacture.mn_id,
	m_actype.actype_id,
	DATEPART(
	week,
	( CASE WHEN ISDATE( sap_hil_d3.completion ) = 1 THEN CAST ( sap_hil_d3.completion AS DATE ) END )
	)");
        return $query->result_array();

    }

    function data_tren_hil_garuda($id_manufacture, $id_type, $week_start, $week_plus_one, $week_end)
    {
        $year = date('Y') . '%';
        if ($id_manufacture == "00") {
            $id_manufacture = "AND m_manufacture.mn_id = '12'";
        } else {
            $id_manufacture = "AND m_manufacture.mn_id = '12'";
        }
        if ($id_type == "00") {
            $id_type = "";
        } else {
            $id_type = "AND m_actype.actype_id ='$id_type'";
        }

        $query = $this->db_dss->query("SELECT
                                      m_manufacture.mn_id AS id_manufacture,
                                      m_actype.actype_id AS id_type,
                                      COUNT (sap_hil_d3.notification) AS hil_open,
                                      '' AS hil_complete,
                                      DATEPART(
                                        week,
                                        CONVERT (
                                          DATE,
                                          sap_hil_d3.created_on,
                                          104
                                        )
                                      ) AS week_create,
                                      '' AS week_comp
                                    FROM
                                      m_manufacture
                                    INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
                                    INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                                    INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg
                                    WHERE
                                      (
                                        (
                                          DATEPART(
                                            week,
                                            CONVERT (
                                              DATE,
                                              sap_hil_d3.created_on,
                                              104
                                            )
                                          ) <= '$week_start'
                                          AND (
                                            system_status LIKE '%NOPR%'
                                            OR system_status LIKE '%OSNO%'
                                          )
                                        )
                                      )
                                      $id_manufacture
                                      $id_type
                                      and sap_hil_d3.created_on like '$year'
                                    GROUP BY
                                      m_manufacture.mn_id,
                                      m_actype.actype_id,
                                      DATEPART(
                                        week,
                                        CONVERT (
                                          DATE,
                                          sap_hil_d3.created_on,
                                          104
                                        )
                                      )
                                    UNION ALL
                                      SELECT
                                        m_manufacture.mn_id AS id_manufacture,
                                        m_actype.actype_id AS id_type,
                                        '' AS hil_open,
                                        COUNT (sap_hil_d3.notification) AS hil_complete,
                                        '' AS week_create,
                                        DATEPART(
                                          week,
                                          CONVERT (
                                            DATE,
                                            sap_hil_d3.completion,
                                            104
                                          )
                                        ) AS week_comp
                                      FROM
                                        m_manufacture
                                      INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
                                      INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                                      INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg
                                      WHERE
                                        (
                                          (
                                            DATEPART(
                                              week,
                                              CONVERT (
                                                DATE,
                                                sap_hil_d3.completion,
                                                104
                                              )
                                            ) BETWEEN '$week_plus_one'
                                            AND '$week_end'
                                            AND (system_status LIKE '%NOCO%')
                                          )
                                        )
                                      $id_manufacture
                                      $id_type
                                      and sap_hil_d3.completion like '$year'
                                      GROUP BY
                                        m_manufacture.mn_id,
                                        m_actype.actype_id,
                                        DATEPART(
                                          week,
                                          CONVERT (
                                            DATE,
                                            sap_hil_d3.completion,
                                            104
                                          )
                                        )");
        return $query->result_array();
    }

    function data_tren_hil_citilink($id_manufacture, $id_type, $week_start, $week_plus_one, $week_end)
    {
        $year = date('Y') . '%';
        if ($id_manufacture == "00") {
            $id_manufacture = "AND m_manufacture.mn_id = '11'";
        } else {
            $id_manufacture = "AND m_manufacture.mn_id = '11'";
        }
        if ($id_type == "00") {
            $id_type = "";
        } else {
            $id_type = "AND m_actype.actype_id ='$id_type'";
        }

        $query = $this->db_dss->query("SELECT
	m_manufacture.mn_id AS id_manufacture,
	m_actype.actype_id AS id_type,
	COUNT ( sap_hil_d3.notification ) AS hil_open,
	'' AS hil_complete,
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) ) AS week_create,
	'' AS week_comp 
FROM
	m_manufacture
	INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
	INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
	INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg 
WHERE
	(
	(
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) ) <= '$week_end' 
	) 
	) $id_manufacture 
	AND sap_hil_d3.created_on LIKE '$year'
GROUP BY
	m_manufacture.mn_id,
	m_actype.actype_id,
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) )
	
	
	UNION ALL
	
SELECT
	m_manufacture.mn_id AS id_manufacture,
	m_actype.actype_id AS id_type,
	'' AS hil_open,
	COUNT ( sap_hil_d3.notification ) AS hil_complete,
	'' AS week_create,
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.completion ) = 1 THEN CAST ( sap_hil_d3.completion AS DATE ) END ) ) AS week_comp 
FROM
	m_manufacture
	INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
	INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
	INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg 
WHERE
	(
	( DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.completion ) = 1 THEN CAST ( sap_hil_d3.completion AS DATE ) END ) ) <= '$week_end' 
	AND ( system_status LIKE '%NOCO%' ) ) 
	) AND m_manufacture.mn_id = '$id_manufacture' 
	AND sap_hil_d3.created_on LIKE '$year'
GROUP BY
	m_manufacture.mn_id,
	m_actype.actype_id,
	DATEPART(
	week,
	( CASE WHEN ISDATE( sap_hil_d3.completion ) = 1 THEN CAST ( sap_hil_d3.completion AS DATE ) END ) 
	)");
        return $query->result_array();
    }

    function mitigasi_hil($id)
    {
        $query = $this->db_dss->query("SELECT
                                      sap_hil_d3.notification,
                                      tbl_mitigasi.mitigasi_why,
                                      tbl_mitigasi.mitigasi_solution
                                    FROM
                                    tbl_mitigasi
                                    LEFT JOIN  sap_hil_d3 ON sap_hil_d3.notification=tbl_mitigasi.mitigasi_fk_id
                                    WHERE sap_hil_d3.notification='$id'  AND tbl_mitigasi.mitigasi_type='hill'
                                    ");
        return $query->result_array();
    }

    function detail_grafik($id, $id_type, $week_start, $week_plus_one, $week_end)
    {
        $year = date('Y') . '%';
        if ($id_type == "00") {
            $id_type = "";
        } else {
            $id_type = "AND m_actype.actype_id ='$id_type'";
        }
        $query = $this->db_dss->query("SELECT
                                m_manufacture.mn_id AS id_manufacture,
                                m_actype.actype_id AS id_type,
                                sap_hil_d3.notification,
                                sap_hil_d3.functional_location AS nm_reg,
                                (( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) ) AS start_date,
                                (( CASE WHEN ISDATE( sap_hil_d3.req_start ) = 1 THEN CAST ( sap_hil_d3.req_start AS DATE ) END ) ) AS due_date,
                                sap_hil_d3.description,
                                CASE
                              WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR%' THEN
                                'OPEN'
                              WHEN sap_hil_d3.system_status LIKE 'NOPR ORAS' THEN
                                'OPEN'
                              WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORAS' THEN
                                'OPEN'
                              WHEN sap_hil_d3.system_status LIKE 'OSNO' THEN
                                'OPEN'
                              WHEN sap_hil_d3.system_status LIKE 'NOPR' THEN
                                'OPEN'
                              WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORCR' THEN
                                'OPEN'
                              END AS system_status
                              FROM
                                m_manufacture
                              INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
                              INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                              INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg
                              WHERE
                                (
                                  (
                                    DATEPART(
                                      week,
                                      (( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) )
                                    ) <= '$week_start'
                                    AND (
                                      system_status LIKE '%NOPR%'
                                      OR system_status LIKE '%OSNO%'
                                    )
                                  )
                                )
                              AND m_manufacture.mn_id = $id
                              $id_type
                              AND sap_hil_d3.created_on LIKE '$year'
                              UNION ALL
                                SELECT
                                  m_manufacture.mn_id AS id_manufacture,
                                  m_actype.actype_id AS id_type,
                                  sap_hil_d3.notification,
                                  sap_hil_d3.functional_location AS nm_reg,
                                  sap_hil_d3.created_on AS start_date,
                                  sap_hil_d3.req_start AS due_date,
                                  sap_hil_d3.description,
                                  CASE
                                WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR%' THEN
                                  'OPEN'
                                WHEN sap_hil_d3.system_status LIKE 'NOPR ORAS' THEN
                                  'OPEN'
                                WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORAS' THEN
                                  'OPEN'
                                WHEN sap_hil_d3.system_status LIKE 'OSNO' THEN
                                  'OPEN'
                                WHEN sap_hil_d3.system_status LIKE 'NOPR' THEN
                                  'OPEN'
                                WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORCR' THEN
                                  'OPEN'
                                END AS system_status
                                FROM
                                  m_manufacture
                                INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
                                INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                                INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg
                                WHERE
                                  (
                                    (
                                      DATEPART(
                                        week,
                                        (( CASE WHEN ISDATE( sap_hil_d3.completion ) = 1 THEN CAST ( sap_hil_d3.completion AS DATE ) END ) )
                                      ) BETWEEN '$week_plus_one'
                                      AND '$week_end'
                                      AND (system_status LIKE '%NOCO%')
                                    )
                                  )
                                AND m_manufacture.mn_id = $id
                                $id_type
                                AND sap_hil_d3.created_on LIKE '$year'");
        return $query->result_array();
    }
}

