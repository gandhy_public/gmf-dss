<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_tatcs extends CI_Model
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->dev_gmf = $this->load->database('dev_gmf', TRUE);
    }

    public function get_plan_sudah($plan, $bulan, $tahun)
    {
        if ($plan == 'TCA') {
            $plan = 'WSAV';
        } else if ($plan == 'TCE') {
            $plan = 'WSEM';
        } else if ($plan == 'TCW') {
            $plan = 'WSWB';
        } else if ($plan == 'TCY') {
            $plan = 'WSNC';
        }
        $tahun_ini = date('Y');
        $tahun_ini = $tahun_ini . '-01-01';
        // $tahun_ini = '2001-01-01';
        $tanggal_ini = date('Y-m-d');
        // $tanggal_ini = '2001-03-04';
        // $query = $this->dev_gmf->query("
        // select plant, count(plant) as total, MONTH(created_on) as bulan, YEAR(created_on) as tahun from tcs_trans
        // where user_status NOT IN('SRAM', 'SROA', 'SRMC', 'SRSC', 'CRTD', 'SROP', 'SREW', 'SROS' ) and plant = '{$plan}' and act_finish_date IS NOT NULL and  CONVERT(date, created_on)>='{$tahun_ini}' and CONVERT(date, created_on)<='{$tanggal_ini}' GROUP BY plant, MONTH(created_on), YEAR(created_on)");
        $query = $this->dev_gmf->query("
				select plant, count(plant) as total,MONTH(created_on) as bulan, YEAR(created_on) as tahun 
                from tcs_trans
    				left JOIN tcs_sla_tat 
                        on tcs_sla_tat.material=tcs_trans.material
    				    or REPLACE(LEFT (tcs_trans.material, CHARINDEX(':', tcs_trans.material)), ':', '') = tcs_sla_tat.material
				where user_status NOT IN('SRAM', 'SROA', 'SRMC', 'SRSC', 'CRTD', 'SROP', 'SREW', 'SROS' ) 
                    and wbs is not null 
                    and plant = '{$plan}' 
                    and MONTH(created_on)='{$bulan}' 
                    and YEAR(created_on)='{$tahun}' 
                    and DATEDIFF(day, CONVERT(date, created_on), CONVERT(date, act_finish_date))<= ISNULL(tat, 0)
				GROUP BY plant, MONTH(created_on), YEAR(created_on)");
        $query = $query->row_array();
        // echo $this->dev_gmf->last_query();
        return $query;
    }

    public function get_plan_all($plan, $bulan, $tahun)
    {
        if ($plan == 'TCA') {
            $plan = 'WSAV';
        } else if ($plan == 'TCE') {
            $plan = 'WSEM';
        } else if ($plan == 'TCW') {
            $plan = 'WSWB';
        } else if ($plan == 'TCY') {
            $plan = 'WSNC';
        }
        $tahun_ini = date('Y');
        $tahun_ini = $tahun_ini . '-01-01';
        // $tahun_ini = '2001-01-01';
        $tanggal_ini = date('Y-m-d');
        // $tanggal_ini = '2001-03-04';
        // $query = $this->dev_gmf->query("
        // select plant, MONTH(created_on) as bulan, YEAR(created_on) as tahun, AVG(CONVERT(FLOAT, tat)/(DATEDIFF(day, CONVERT(date, created_on), CONVERT(date, act_finish_date))+1)*100) as total from tcs_trans left JOIN tcs_sla_tat on tcs_sla_tat.material=tcs_trans.material
        // where user_status NOT IN('SRAM', 'SROA', 'SRMC', 'SRSC', 'CRTD', 'SROP', 'SREW', 'SROS' ) and plant = '{$plan}' and act_finish_date IS NOT NULL and wbs is not null  and  CONVERT(date, created_on)>='{$tahun_ini}' and CONVERT(date, created_on)<='{$tanggal_ini}'
        // GROUP BY plant, MONTH(created_on), YEAR(created_on)");
        // $query = $query->result_array();
        $query = $this->dev_gmf->query("
					SELECT avg(CASE
							WHEN total>=80 THEN 80
							WHEN total<80 THEN total
						END ) as total
							from (
					select plant, created_on, 
                        CONVERT(FLOAT, tat) / (DATEDIFF(day, CONVERT(date, created_on), CONVERT(date, act_finish_date))+1)*100  as total 
                    from tcs_trans
    					left JOIN tcs_sla_tat on tcs_sla_tat.material=tcs_trans.material
    					or REPLACE(LEFT (tcs_trans.material, CHARINDEX(':', tcs_trans.material)), ':', '') = tcs_sla_tat.material
					where user_status NOT IN('SRAM', 'SROA', 'SRMC', 'SRSC', 'CRTD', 'SROP', 'SREW', 'SROS' ) 
                        and plant = '{$plan}' 
                        and act_finish_date IS NOT NULL 
                        and act_finish_date != '00000000'
                        -- ADDED YUDI
                        and user_status = 'FSB'
                        and wbs is not null  
                        and MONTH(created_on)='{$bulan}' 
                        and YEAR(created_on)='{$tahun}'
					) a");
        $query = $query->row_array();
        // echo  $this->dev_gmf->last_query();

        return $query;
    }


    public function get_all_detail_plan($plan)
    {
        if ($plan == 'TCA') {
            $plan = 'WSAV';
        } else if ($plan == 'TCE') {
            $plan = 'WSEM';
        } else if ($plan == 'TCW') {
            $plan = 'WSWB';
        } else if ($plan == 'TCY') {
            $plan = 'WSNC';
        }
        $tahun_ini = date('Y');
        $tahun_ini = $tahun_ini . '-01-01';
        // $tahun_ini = '2001-01-01';
        $tanggal_ini = date('Y-m-d');
        // $tanggal_ini = '2001-03-04';
        $query = $this->dev_gmf->query("
				select main_work_center, count(main_work_center) as jumlah 
                from tcs_trans
				where user_status NOT IN('SRAM', 'SROA', 'SRMC', 'SRSC', 'CRTD', 'SROP', 'SREW', 'SROS' ) 
                    and plant = '{$plan}' 
                    and act_finish_date IS NOT NULL  
                    and act_finish_date != '00000000' 
                    and user_status = 'FSB'
                    and wbs is not null  
                    and CONVERT(date, created_on)>='{$tahun_ini}' 
                    and CONVERT(date, created_on)<='{$tanggal_ini}' 
                GROUP BY main_work_center");
        $query = $query->result_array();

        return $query;
    }

    public function get_all_detail_total($plan)
    {
        if ($plan == 'TCA') {
            $plan = 'WSAV';
        } else if ($plan == 'TCE') {
            $plan = 'WSEM';
        } else if ($plan == 'TCW') {
            $plan = 'WSWB';
        } else if ($plan == 'TCY') {
            $plan = 'WSNC';
        }
        $tahun_ini = date('Y');
        $tahun_ini = $tahun_ini . '-01-01';
        // $tahun_ini = '2001-01-01';
        $tanggal_ini = date('Y-m-d');
        // $tanggal_ini = '2001-03-04';
        $query = $this->dev_gmf->query("
				select count(plant) as jumlah 
                from tcs_trans
				where user_status NOT IN('SRAM', 'SROA', 'SRMC', 'SRSC', 'CRTD', 'SROP', 'SREW', 'SROS' ) 
                    and plant = '{$plan}' 
                    and act_finish_date IS NOT NULL  
                    and act_finish_date != '00000000' 
                    and wbs is not null  
                    and CONVERT(date, created_on)>='{$tahun_ini}' 
                    and CONVERT(date, created_on)<='{$tanggal_ini}'");
        $query = $query->row_array();

        return $query;
    }

    public function cek_detail_total($main_work_center)
    {
        $tahun_ini = date('Y');
        $tahun_ini = $tahun_ini . '-01-01';
        // $tahun_ini = '2001-01-01';
        $tanggal_ini = date('Y-m-d');
        // $tanggal_ini = '2001-03-04';
        $query = $this->dev_gmf->query("
				select count(main_work_center) as jumlah 
                from tcs_trans
				where user_status NOT IN('SRAM', 'SROA', 'SRMC', 'SRSC', 'CRTD', 'SROP', 'SREW', 'SROS' ) 
                    and main_work_center = '{$main_work_center}' 
                    and act_finish_date IS NOT NULL  
                    and act_finish_date != '00000000'
                    and user_status = 'FSB'
                    and wbs is not null  
                    and CONVERT(date, created_on)>='{$tahun_ini}' 
                    and CONVERT(date, created_on)<='{$tanggal_ini}'");
        $query = $query->row_array();
        return $query;
    }

    public function distinct_work_center($plan)
    {
      if ($plan == 'TCA') {
          $plan = 'WSAV';
      } else if ($plan == 'TCE') {
          $plan = 'WSEM';
      } else if ($plan == 'TCW') {
          $plan = 'WSWB';
      } else if ($plan == 'TCY') {
          $plan = 'WSNC';
      }
      $tahun_ini = date('Y');
      $tahun_ini = $tahun_ini . '-01-01';
      $tanggal_ini = date('Y-m-d');
      $sql = "  select distinct(main_work_center) as main_work_center 
                from tcs_trans
              where user_status NOT IN('SRAM', 'SROA', 'SRMC', 'SRSC', 'CRTD', 'SROP', 'SREW', 'SROS' ) 
              and plant = '{$plan}'
              and act_finish_date IS NOT NULL  
              and act_finish_date != '00000000'
              and user_status = 'FSB'
              and wbs is not null
              and CONVERT(date, created_on)>='{$tahun_ini}' 
              and CONVERT(date, created_on)<='{$tanggal_ini}'";
      $exec = $this->dev_gmf->query($sql);
      $arr = array();
      foreach ($exec->result() as $data) {
        $arr[] = $data->main_work_center;
      }
      return $arr;
    }

    public function get_all_detail_table($param, $tipe, $status, $kode, $week)
    {

        if ($tipe == 'title') {

            if ($param == 'TCA') {
                $param = 'WSAV';
            } else if ($param == 'TCE') {
                $param = 'WSEM';
            } else if ($param == 'TCW') {
                $param = 'WSWB';
            } else if ($param == 'TCY') {
                $param = 'WSNC';
            }
            $where = " and plant='{$param}'";
        } else {
            $where = " and main_work_center='{$param}'";
        }
        if ($status == 'sudah') {

            $where1 = " and (act_finish_date IS NOT NULL AND act_finish_date != '00000000' and user_status = 'FSB') ";
            $where2 = ", DATEDIFF(day, CONVERT(date, created_on), CONVERT(date, act_finish_date)) as jumlah_hari";
            // $where2  = " ";
        } else {
            $where1 = " and (act_finish_date IS NULL or act_finish_date = '00000000' ) ";
            $hariini = date('Y-m-d');
            $where2 = ", DATEDIFF(day, CONVERT(date, created_on), '{$hariini}') as jumlah_hari";
        }

        if ($kode == '') {
            $where3 = ' and wbs is not null ';
        } else {
            $where3 = " and wbs is not null  and {$kode}";
        }

        if ($week != '' or $week != null) {
            $whereWeek = " and DATEPART(week, CONVERT (DATE, created_on, 104)) = '{$week}'";
        } else {
            $whereWeek = "";
        }
        $query = $this->dev_gmf->query("
				SELECT *, tcs_trans.material {$where2} 
                from tcs_trans
					left join tcs_sla_tat on tcs_sla_tat.material = tcs_trans.material
					or REPLACE(LEFT (tcs_trans.material, CHARINDEX(':', tcs_trans.material)), ':', '') = tcs_sla_tat.material
				where user_status NOT IN('SRAM', 'SROA', 'SRMC', 'SRSC', 'CRTD', 'SROP', 'SREW', 'SROS' ) {$whereWeek} {$where} {$where1} {$where3}");
        $query = $query->result_array();

        // echo $this->dev_gmf->last_query();

        return $query;
    }


    public function get_panel_proses($param)
    {

        if ($param == 'TCA') {
            $param = 'WSAV';
        } else if ($param == 'TCE') {
            $param = 'WSEM';
        } else if ($param == 'TCW') {
            $param = 'WSWB';
        } else if ($param == 'TCY') {
            $param = 'WSNC';
        }
        $where = " and plant='{$param}'";

        $query = $this->dev_gmf->query("
				select plant, count(plant) as jumlah 
                from tcs_trans
                    left join tcs_sla_tat on tcs_sla_tat.material = tcs_trans.material
                    or REPLACE(LEFT (tcs_trans.material, CHARINDEX(':', tcs_trans.material)), ':', '') = tcs_sla_tat.material
				where user_status NOT IN('SRAM', 'SROA', 'SRMC', 'SRSC', 'CRTD', 'SROP', 'SREW', 'SROS' ) 
                    and (act_finish_date IS NULL OR act_finish_date = '00000000') 
                    and wbs IS NOT NULL {$where} 
                GROUP BY plant ");
        $query = $query->row_array();

        return $query;
    }


    public function get_panel_proses_cek($param, $kode, $tipe)
    {

        if ($param == 'TCA') {
            $param = 'WSAV';
        } else if ($param == 'TCE') {
            $param = 'WSEM';
        } else if ($param == 'TCW') {
            $param = 'WSWB';
        } else if ($param == 'TCY') {
            $param = 'WSNC';
        }
        $where = " and plant='{$param}'";

        if ($tipe == 'all') {
            $where1 = " ";
        } else {
            $where1 = " and {$kode}";
        }
        $query = $this->dev_gmf->query("
				select plant, count(plant) as jumlah 
                from tcs_trans
				where user_status NOT IN('SRAM', 'SROA', 'SRMC', 'SRSC', 'CRTD', 'SROP', 'SREW', 'SROS' ) 
                    and (act_finish_date IS NULL OR act_finish_date = '00000000') 
                    and wbs IS NOT NULL {$where} {$where1} 
                GROUP BY plant ");
        $query = $query->row_array();

        return $query;
    }

    public function get_all_data()
    {
        $query = $this->dev_gmf->query("
				select * from tcs_sla_tat ");
        $query = $query->result_array();

        return $query;
    }

    public function truncate_table()
    {
        $this->dev_gmf->truncate('tcs_sla_tat');
    }

    public function insert($data)
    {
        $this->dev_gmf->trans_begin();

        $this->dev_gmf->insert_batch('tcs_sla_tat', $data);

        if ($this->dev_gmf->trans_status() === FALSE) {
            $this->dev_gmf->trans_rollback();
        } else {
            $this->dev_gmf->trans_commit();
        }
    }


    public function cek_upload($param)
    {
        $query = $this->dev_gmf->query("
				select material from  tcs_sla_tat where material='{$param}'");
        $query = $query->result_array();

        return $query;
    }

    function update_upload($data, $where, $table)
    {
        $this->dev_gmf->where($where);
        $this->dev_gmf->update($table, $data);
    }

    //===========DATATABLE============\\
    var $table = 'tcs_sla_tat';
    var $column_order = array('material', 'description', 'tat');
    var $column_search = array('material', 'description', 'tat');
    var $order = array('material' => 'ASC');

    private function query()
    {

        $this->dev_gmf->select("material, description, tat");
        $this->dev_gmf->from('tcs_sla_tat');
        $i = 0;

        foreach ($this->column_search as $item) {
            if (!empty($_POST['search']['value'])) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->dev_gmf->group_start();
                    $this->dev_gmf->like($item, $_POST['search']['value']);
                } else {
                    $this->dev_gmf->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->dev_gmf->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->dev_gmf->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->dev_gmf->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {

        $this->query();
        if ($_POST['length'] != -1)
            $this->dev_gmf->limit($_POST['length'], $_POST['start']);
        $query = $this->dev_gmf->get();
        return $query->result();
    }

    function count_all()
    {

        $this->query();
        $query = $this->dev_gmf->get();
        return $query->num_rows();
    }


    function count_filtered()
    {

        $this->query();
        $query = $this->dev_gmf->get();
        return $query->num_rows();
    }


}
