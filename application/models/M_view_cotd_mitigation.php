<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_view_cotd_mitigation extends CI_Model{

  var $table = 'view_cotd_mitigation';
  var $column_order = array(null,'date_','aircraft','ac_type', null);
  var $column_search = array('aircraft','ac_type','actual');
  var $order = array('date_' => 'desc');

  public function __construct()
  {
    parent::__construct();
      $this->load->database();
      // $this->db_dev = $this->load->database('dev_gmf_crm', TRUE);
      $this->db_dev = $this->load->database('dev_gmf', TRUE);
  }

  private function _get_datatables_query(){

    $this->db_dev->select('view_cotd_mitigation.ac_type,
                       view_cotd_mitigation.aircraft,
                       view_cotd_mitigation.date_,
                       view_cotd_mitigation.bulan,
                       view_cotd_mitigation.tahun,
                       view_cotd_mitigation.actual'
                       );
    $this->db_dev->from($this->table);
    $i = 0;

    foreach ($this->column_search as $item)
    {
      if(!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db_dev->group_start();
          $this->db_dev->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db_dev->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db_dev->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order']))
    {
      $this->db_dev->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db_dev->order_by(key($order), $order[key($order)]);
    }
  }

  function get_alldata()
  {
    $this->_get_datatables_query();
    $data = $this->db_dev->get();

    return $data->result();
  }

  function get_datatables($p_start="", $p_end="")
  {
    if (!empty($p_start) || $p_start!= "" || !empty($p_end) || $p_end!= "") {
      $awal = explode("-",$p_start);
      $awal_bulan = $awal[0];
      $awal_tahun = $awal[1];
      $akhir = explode("-",$p_end);
      $akhir_bulan = $akhir[0];
      $akhir_tahun = $akhir[1];
      $de = cal_days_in_month(CAL_GREGORIAN, $akhir_bulan, $akhir_tahun);
      $p_start = $awal_tahun.'-'.$awal_bulan.'-01';
      $p_end = $akhir_tahun.'-'.$akhir_bulan.'-'.$de;
      $this->date_start = "'".$p_start."'";
      $this->date_end = "'".$p_end."'";
      $this->_get_datatables_query();
      $this->db_dev->where('CAST(date_ AS DATE) >=', $p_start);
      $this->db_dev->where('CAST(date_ AS DATE) <=', $p_end);
    }else{
      $this->_get_datatables_query();
    }
    if($_POST['length'] != -1)
      $this->db_dev->limit($_POST['length'], $_POST['start']);
      $query = $this->db_dev->get();
    return $query->result();
  }

  function count_filtered($aircraft="", $target="", $p_start="", $p_end="")
  {
    if (!empty($p_start) || $p_start!= "" || !empty($p_end) || $p_end!= "") {
      $awal = explode("-",$p_start);
      $awal_bulan = $awal[0];
      $awal_tahun = $awal[1];
      $akhir = explode("-",$p_end);
      $akhir_bulan = $akhir[0];
      $akhir_tahun = $akhir[1];
      $de = cal_days_in_month(CAL_GREGORIAN, $akhir_bulan, $akhir_tahun);
      $p_start = $awal_tahun.'-'.$awal_bulan.'-01';
      $p_end = $akhir_tahun.'-'.$akhir_bulan.'-'.$de;
      $this->date_start = "'".$p_start."'";
      $this->date_end = "'".$p_end."'";
      $this->_get_datatables_query();
      $this->db_dev->where('CAST(date_ AS DATE) >=', $p_start);
      $this->db_dev->where('CAST(date_ AS DATE) <=', $p_end);
    }else{
      $this->_get_datatables_query();
    }
    if (!empty($aircraft) || $aircraft!= "") {
      $this->db_dev->where('aircraft', $aircraft);
    }
    if (!empty($target) || $target!= "") {
      $this->db_dev->where('actual >=', $target);
    }
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  public function count_all($aircraft="", $target="", $p_start="", $p_end="")
  {
    if (!empty($p_start) || $p_start!= "" || !empty($p_end) || $p_end!= "") {
      $awal = explode("-",$p_start);
      $awal_bulan = $awal[0];
      $awal_tahun = $awal[1];
      $akhir = explode("-",$p_end);
      $akhir_bulan = $akhir[0];
      $akhir_tahun = $akhir[1];
      $de = cal_days_in_month(CAL_GREGORIAN, $akhir_bulan, $akhir_tahun);
      $p_start = $awal_tahun.'-'.$awal_bulan.'-01';
      $p_end = $akhir_tahun.'-'.$akhir_bulan.'-'.$de;
      $this->date_start = "'".$p_start."'";
      $this->date_end = "'".$p_end."'";
      $this->_get_datatables_query();
      $this->db_dev->where('CAST(date_ AS DATE) >=', $p_start);
      $this->db_dev->where('CAST(date_ AS DATE) <=', $p_end);
    }else{
      $this->_get_datatables_query();
    }
    if (!empty($aircraft) || $aircraft!= "") {
      $this->db_dev->where('aircraft', $aircraft);
    }
    if (!empty($target) || $target!= "") {
      $this->db_dev->where('actual >=', $target);
    }
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  public function get_mitigasi($month, $year, $aircraft)
  {
    $this->_get_datatables_query();
    $this->db_dev->where('MONTH(date_)',$month);
    $this->db_dev->where('YEAR(date_)',$year);
    $this->db_dev->like('ac_type',$aircraft, 'BOTH');
    $query = $this->db_dev->get();

    return $query->row();
  }

  public function get_by_id($id)
  {
    $this->db_dev->from($this->table);
    $this->db_dev->where('id_master_mitigation',$id);
    $query = $this->db_dev->get();

    return $query->row();
  }

  public function save($data)
  {
    $this->db_dev->insert($this->table, $data);

    $id = $this->db_dev->insert_id();
    return (isset($id)) ? $id : FALSE;
  }

  public function update($where, $data)
  {
    $this->db_dev->update($this->table, $data, $where);
    return $this->db_dev->affected_rows();
  }

  public function delete($id)
  {
    $this->db_dev->where('id_master_mitigation', $id);
    $this->db_dev->delete($this->table);
  }

}
