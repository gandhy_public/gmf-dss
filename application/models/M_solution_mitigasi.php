<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_solution_mitigasi extends CI_Model{

  var $table = 'tbl_mitigasi';
  var $column_order = array('mitigasi_id','mitigasi_why','mitigasi_solution', 'mitigasi_date', 'mitigasi_aircraft', null);
  var $column_search = array('mitigasi_id','mitigasi_why','mitigasi_solution');
  var $order = array('mitigasi_why' => 'asc');

  public function __construct()
  {
    parent::__construct();
      $this->load->database();
      $this->db_dev = $this->load->database('dev_gmf', TRUE);
  }

  private function _get_datatables_query(){

    $this->db_dev->select('tbl_mitigasi.mitigasi_id,
                       tbl_mitigasi.mitigasi_why,
                       tbl_mitigasi.mitigasi_solution,
                       tbl_mitigasi.mitigasi_date,
                       tbl_mitigasi.mitigasi_aircraft,
                       tbl_mitigasi.mitigasi_type,
                       tbl_mitigasi.mitigasi_fk_id'
                       );
    $this->db_dev->from($this->table);
    $i = 0;

    foreach ($this->column_search as $item)
    {
      if(!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db_dev->group_start();
          $this->db_dev->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db_dev->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db_dev->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order']))
    {
      $this->db_dev->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db_dev->order_by(key($order), $order[key($order)]);
    }
  }

  function get_alldata()
  {
    $this->_get_datatables_query();
    $data = $this->db_dev->get();

    return $data->result();
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db_dev->limit($_POST['length'], $_POST['start']);
      $query = $this->db_dev->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db_dev->from($this->table);
    return $this->db_dev->count_all_results();
  }

  public function get_mitigasi($month, $year, $aircraft, $type)
  {
    $this->db_dev->from($this->table);
    $this->db_dev->where('MONTH(mitigasi_date)',$month);
    $this->db_dev->where('YEAR(mitigasi_date)',$year);
    $this->db_dev->like('mitigasi_aircraft',$aircraft, 'BOTH');
    $this->db_dev->where('mitigasi_type',$type);
    $query = $this->db_dev->get();

    return $query->row();
  }

  public function get_by_id($id)
  {
    $this->db_dev->from($this->table);
    $this->db_dev->where('mitigasi_id',$id);
    $query = $this->db_dev->get();

    return $query->row();
  }

  public function save($data)
  {
    $this->db_dev->insert($this->table, $data);

    $id = $this->db_dev->insert_id();
    return (isset($id)) ? $id : FALSE;
  }

  public function update($where, $data)
  {
    $this->db_dev->update($this->table, $data, $where);
    return $this->db_dev->affected_rows();
  }

  public function delete($id)
  {
    $this->db_dev->where('mitigasi_id', $id);
    return $this->db_dev->delete($this->table);
  }

}
