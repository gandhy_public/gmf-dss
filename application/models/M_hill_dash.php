<?php

class M_hill_dash extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->db_dss = $this->load->database('prod_shil', TRUE);
        $this->db_dss_prod = $this->load->database('dev_gmf', TRUE);
    }

    function master_manufacture()
    {
        $query = $this->db_dss_prod->query("SELECT mn_id, mn_name FROM m_manufacture");
        return $query->result();
    }

    function master_reg($id)
    {
        $query = $this->db_dss_prod->query("SELECT
                                      m_manufacture.mn_id,
                                      m_manufacture.mn_name,
                                      m_actype.actype_id,
                                      m_actype.actype
                                    FROM
                                      m_actype
                                    INNER JOIN m_acreg ON m_acreg.actype_id = m_actype.actype_id
                                    INNER JOIN m_manufacture ON m_manufacture.mn_name = m_acreg.own
                                    WHERE
                                      m_manufacture.mn_id = '$id'
                                    GROUP BY
                                      m_manufacture.mn_id,
                                      m_manufacture.mn_name,
                                      m_actype.actype_id,
                                      m_actype.actype");
        return $query->result();
    }

    function target_hil()
    {
        $date = date('Y-m-d');
        $explode = explode('-', $date);
        $explode = $explode[0];
        $query = $this->db->query("SELECT
                                    *
                                  FROM
                                    tbl_all_target
                                  WHERE
                                    target_type = 'Hill'
                                  AND target_year = '$explode'");
        return $query->result();
    }

    function count_reg_per_type_garuda()
    {
        $query = $this->db_dss_prod->query("SELECT
                                        m_actype.actype_id,
                                        COUNT (m_acreg.acreg) AS total_reg
                                      FROM
                                        m_acreg
                                      INNER JOIN m_manufacture ON m_manufacture.mn_name = m_acreg.own
                                      LEFT JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                                      WHERE
                                        m_manufacture.mn_name = 'GA'
                                        and m_actype.actype_id!=''
                                        GROUP BY
                                      m_actype.actype_id");
        return $query->result_array();
    }

    function count_reg_per_type_citilink()
    {
        $query = $this->db_dss_prod->query("SELECT
                                        m_actype.actype_id,
                                        COUNT (m_acreg.acreg) AS total_reg
                                      FROM
                                        m_acreg
                                      INNER JOIN m_manufacture ON m_manufacture.mn_name = m_acreg.own
                                      LEFT JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                                      WHERE
                                        m_manufacture.mn_name = 'CITILINK'
                                        and m_actype.actype_id!=''
                                        GROUP BY
                                      m_actype.actype_id");
        return $query->result_array();
    }

    function garuda_open()
    {
        $firstday = date('Y' . '-01-01');
        $date = date('Y-m-d');
        $year=date('Y');

        $query = $this->db_dss->query(
        "SELECT SUM(total) AS TOTALOPEN FROM (

SELECT     TOP 100 PERCENT 
db_shil.dbo.tblStatus.StatusDesc, 
db_dss.dbo.m_acreg.acreg,
db_dss.dbo.m_acreg.own,
-- dbo.tbl_master_actype.ACType,
-- dbo.tblHIL_swift.itemID as notification,
-- dbo.tblHIL_swift.Description,
dbo.tblHIL_swift.DateOccur as start_date,
-- dbo.tblHIL_swift.DueDate as due_date,
-- dbo.tbl_master_acreg.acreg AS acreg,
--  dbo.tbl_acreg_owner.Own as OWN,
 COUNT(db_shil.dbo.tblHIL_swift.ItemID) AS total

FROM         db_shil.dbo.tblStatus 
             RIGHT OUTER JOIN db_shil.dbo.tblHIL_swift ON db_shil.dbo.tblStatus.StatusNo = db_shil.dbo.tblHIL_swift.Status 
--              LEFT OUTER JOIN  db_shil.dbo.tbl_master_actype 
-- 						 INNER JOIN		    db_shil.dbo.tbl_master_acreg ON db_shil.dbo.tbl_master_actype.ACTypeID = db_shil.dbo.tbl_master_acreg.actypeID ON db_shil.dbo.tblHIL_swift.acreg = db_shil.dbo.tbl_master_acreg.acreg
--   						LEFT JOIN  db_shil.dbo.tbl_acreg_owner On db_shil.dbo.tbl_master_actype.ACType=db_shil.dbo.tbl_acreg_owner.actype
						 JOIN db_dss.dbo.m_acreg ON db_shil.dbo.tblHIL_swift.acreg = db_dss.dbo.m_acreg.acreg

WHERE (SUBSTRING(TECHLOG, 1, 1)!='C' and (db_shil.dbo.tblHIL_swift.DueDate  > CONVERT(DATETIME, GETDATE(), 102))
OR 
(
(db_shil.dbo.tblHIL_swift.DueDate <= CONVERT(DATETIME, GETDATE(), 102)  or (db_shil.dbo.tblHIL_swift.DueDate is null )) 
AND
(db_shil.dbo.tblHIL_swift.status = 3)
)
OR
(
(db_shil.dbo.tblHIL_swift.status = 7)
)
)AND (db_shil.dbo.tblHIL_swift.Subject NOT LIKE '%ASDCS%')
 AND (db_shil.dbo.tblHIL_swift.Description NOT LIKE '%ASDCS%')
-- AND CONVERT(DATE,dbo.tblHIL_swift.DateOccur,102)='2018-01-01'
-- AND YEAR(CONVERT(datetime,dbo.tblHIL_swift.DateOccur,102))='2018'

 GROUP BY db_shil.dbo.tblStatus.StatusDesc, db_dss.dbo.m_acreg.acreg, db_dss.dbo.m_acreg.own
,dbo.tblHIL_swift.DateOccur
--  , dbo.tbl_master_actype.ACType,dbo.tbl_acreg_owner.own
-- ,dbo.tbl_master_acreg.acreg 
-- 	WITH rollup
-- 
) A
WHERE 
A.StatusDesc = 'Open'
-- AND A.ACType IS NOT NULL
AND A.own='GA'
AND YEAR(CONVERT(DATE,A.start_date,102))='$year'");
        return $query->result_array();
    }

    function garuda_open_table()
    {
        $firstday = date('Y' . '-01-01');
        $date = date('Y-m-d');
        $year=date('Y');

        $query = $this->db_dss->query(
        "SELECT actype_id,StatusDesc,own,actype,SUM(total) as total FROM (

SELECT     TOP 100 PERCENT 
db_shil.dbo.tblStatus.StatusDesc, 
db_dss.dbo.m_acreg.acreg,
db_dss.dbo.m_acreg.own,
db_dss.dbo.m_actype.actype,
db_dss.dbo.m_actype.actype_id,
-- dbo.tblHIL_swift.itemID as notification,
-- dbo.tblHIL_swift.Description,
dbo.tblHIL_swift.DateOccur as start_date,
-- dbo.tblHIL_swift.DueDate as due_date,
-- dbo.tbl_master_acreg.acreg AS acreg,
--  dbo.tbl_acreg_owner.Own as OWN,
 COUNT(db_shil.dbo.tblHIL_swift.ItemID) AS total

FROM         db_shil.dbo.tblStatus 
             RIGHT OUTER JOIN db_shil.dbo.tblHIL_swift ON db_shil.dbo.tblStatus.StatusNo = db_shil.dbo.tblHIL_swift.Status 
--              LEFT OUTER JOIN  db_shil.dbo.tbl_master_actype 
-- 						 INNER JOIN		    db_shil.dbo.tbl_master_acreg ON db_shil.dbo.tbl_master_actype.ACTypeID = db_shil.dbo.tbl_master_acreg.actypeID ON db_shil.dbo.tblHIL_swift.acreg = db_shil.dbo.tbl_master_acreg.acreg
--   						LEFT JOIN  db_shil.dbo.tbl_acreg_owner On db_shil.dbo.tbl_master_actype.ACType=db_shil.dbo.tbl_acreg_owner.actype
						 JOIN db_dss.dbo.m_acreg ON db_shil.dbo.tblHIL_swift.acreg = db_dss.dbo.m_acreg.acreg
							LEFT JOIN db_dss.dbo.m_actype ON db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id

WHERE (SUBSTRING(TECHLOG, 1, 1)!='C' and (db_shil.dbo.tblHIL_swift.DueDate  > CONVERT(DATETIME,GETDATE(), 102))
OR 
(
(db_shil.dbo.tblHIL_swift.DueDate <= CONVERT(DATETIME, GETDATE(), 102)  or (db_shil.dbo.tblHIL_swift.DueDate is null )) 
AND
(db_shil.dbo.tblHIL_swift.status = 3)
)
OR
(
(db_shil.dbo.tblHIL_swift.status = 7)
)
)AND (db_shil.dbo.tblHIL_swift.Subject NOT LIKE '%ASDCS%')
 AND (db_shil.dbo.tblHIL_swift.Description NOT LIKE '%ASDCS%')

 GROUP BY db_shil.dbo.tblStatus.StatusDesc, db_dss.dbo.m_acreg.acreg, db_dss.dbo.m_acreg.own
 , db_dss.dbo.m_actype.actype,db_dss.dbo.m_actype.actype_id,dbo.tblHIL_swift.DateOccur
-- ,dbo.tbl_acreg_owner.own
-- ,dbo.tbl_master_acreg.acreg 
-- 	WITH rollup
-- 
) A
WHERE 
A.StatusDesc = 'Open'
AND A.ACType IS NOT NULL
AND A.OWN='GA'
AND YEAR(CONVERT(DATE,A.start_date,102))='$year'
GROUP BY A.actype_id,A.StatusDesc,A.own,A.actype
");
        return $query->result_array();
    }

    function garuda_detail_open($id)
    {
        $firstday = date('Y' . '-01-01');
        $date = date('Y-m-d');

        $query = $this->db_dss->query(
            "SELECT A.*  FROM (

SELECT     TOP 100 PERCENT 
db_shil.dbo.tblStatus.StatusDesc, 
db_dss.dbo.m_acreg.acreg,
db_dss.dbo.m_acreg.own,
db_dss.dbo.m_actype.actype,
db_dss.dbo.m_actype.actype_id,
dbo.tblHIL_swift.itemID as notification,
dbo.tblHIL_swift.Description,
CAST(dbo.tblHIL_swift.DateOccur AS DATE) as start_date,
CAST(dbo.tblHIL_swift.DueDate AS DATE) as due_date
FROM         db_shil.dbo.tblStatus 
             RIGHT OUTER JOIN db_shil.dbo.tblHIL_swift ON db_shil.dbo.tblStatus.StatusNo = db_shil.dbo.tblHIL_swift.Status 
					 JOIN db_dss.dbo.m_acreg ON db_shil.dbo.tblHIL_swift.acreg = db_dss.dbo.m_acreg.acreg
							LEFT JOIN db_dss.dbo.m_actype ON db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id

WHERE (SUBSTRING(TECHLOG, 1, 1)!='C' and (db_shil.dbo.tblHIL_swift.DueDate  > CONVERT(DATETIME,GETDATE(), 102))
OR 
(
(db_shil.dbo.tblHIL_swift.DueDate <= CONVERT(DATETIME, GETDATE(), 102)  or (db_shil.dbo.tblHIL_swift.DueDate is null )) 
AND
(db_shil.dbo.tblHIL_swift.status = 3)
)
OR
(
(db_shil.dbo.tblHIL_swift.status = 7)
)
)AND (db_shil.dbo.tblHIL_swift.Subject NOT LIKE '%ASDCS%')
 AND (db_shil.dbo.tblHIL_swift.Description NOT LIKE '%ASDCS%')

 GROUP BY db_shil.dbo.tblStatus.StatusDesc
,dbo.tblHIL_swift.itemID
,dbo.tblHIL_swift.Description
,dbo.tblHIL_swift.DateOccur
, dbo.tblHIL_swift.DueDate
,db_dss.dbo.m_acreg.acreg
, db_dss.dbo.m_acreg.own
, db_dss.dbo.m_actype.actype
,db_dss.dbo.m_actype.actype_id
-- ,dbo.tbl_acreg_owner.own
-- ,dbo.tbl_master_acreg.acreg 
-- 	WITH rollup
-- 
) A
WHERE 
A.StatusDesc = 'Open'
-- AND A.ACType IS NOT NULL
AND A.OWN='GA'
AND A.actype_id='$id'
");
        return $query->result_array();
    }

    function citilink_open()
    {
        $firstday = date('Y' . '-01-01');
        $date = date('Y-m-d');
        $year=date('Y');

        $query = $this->db_dss->query(
        "SELECT SUM(total) AS TOTALOPEN FROM (

SELECT     TOP 100 PERCENT 
db_shil.dbo.tblStatus.StatusDesc, 
db_dss.dbo.m_acreg.acreg,
db_dss.dbo.m_acreg.own,
-- dbo.tbl_master_actype.ACType,
-- dbo.tblHIL_swift.itemID as notification,
-- dbo.tblHIL_swift.Description,
dbo.tblHIL_swift.DateOccur as start_date,
-- dbo.tblHIL_swift.DueDate as due_date,
-- dbo.tbl_master_acreg.acreg AS acreg,
--  dbo.tbl_acreg_owner.Own as OWN,
 COUNT(db_shil.dbo.tblHIL_swift.ItemID) AS total

FROM         db_shil.dbo.tblStatus 
             RIGHT OUTER JOIN db_shil.dbo.tblHIL_swift ON db_shil.dbo.tblStatus.StatusNo = db_shil.dbo.tblHIL_swift.Status 
--              LEFT OUTER JOIN  db_shil.dbo.tbl_master_actype 
-- 						 INNER JOIN		    db_shil.dbo.tbl_master_acreg ON db_shil.dbo.tbl_master_actype.ACTypeID = db_shil.dbo.tbl_master_acreg.actypeID ON db_shil.dbo.tblHIL_swift.acreg = db_shil.dbo.tbl_master_acreg.acreg
--   						LEFT JOIN  db_shil.dbo.tbl_acreg_owner On db_shil.dbo.tbl_master_actype.ACType=db_shil.dbo.tbl_acreg_owner.actype
						 JOIN db_dss.dbo.m_acreg ON db_shil.dbo.tblHIL_swift.acreg = db_dss.dbo.m_acreg.acreg

WHERE (SUBSTRING(TECHLOG, 1, 1)!='C' and (db_shil.dbo.tblHIL_swift.DueDate  > CONVERT(DATETIME, GETDATE(), 102))
OR 
(
(db_shil.dbo.tblHIL_swift.DueDate <= CONVERT(DATETIME, GETDATE(), 102)  or (db_shil.dbo.tblHIL_swift.DueDate is null )) 
AND
(db_shil.dbo.tblHIL_swift.status = 3)
)
OR
(
(db_shil.dbo.tblHIL_swift.status = 7)
)
)AND (db_shil.dbo.tblHIL_swift.Subject NOT LIKE '%ASDCS%')
 AND (db_shil.dbo.tblHIL_swift.Description NOT LIKE '%ASDCS%')
-- AND CONVERT(DATE,dbo.tblHIL_swift.DateOccur,102)='2018-01-01'
-- AND YEAR(CONVERT(datetime,dbo.tblHIL_swift.DateOccur,102))='2018'

 GROUP BY db_shil.dbo.tblStatus.StatusDesc, db_dss.dbo.m_acreg.acreg, db_dss.dbo.m_acreg.own
,dbo.tblHIL_swift.DateOccur
--  , dbo.tbl_master_actype.ACType,dbo.tbl_acreg_owner.own
-- ,dbo.tbl_master_acreg.acreg 
-- 	WITH rollup
-- 
) A
WHERE 
A.StatusDesc = 'Open'
-- AND A.ACType IS NOT NULL
AND A.own='CITILINK'
AND YEAR(CONVERT(DATE,A.start_date,102))='$year'
");
        return $query->result_array();
    }

    function citilink_open_table()
    {
        $firstday = date('Y' . '-01-01');
        $date = date('Y-m-d');
        $year=date('Y');

        $query = $this->db_dss->query(
        "SELECT actype_id,StatusDesc,own,actype,SUM(total) as total FROM (

SELECT     TOP 100 PERCENT 
db_shil.dbo.tblStatus.StatusDesc, 
db_dss.dbo.m_acreg.acreg,
db_dss.dbo.m_acreg.own,
db_dss.dbo.m_actype.actype,
db_dss.dbo.m_actype.actype_id,
-- dbo.tblHIL_swift.itemID as notification,
-- dbo.tblHIL_swift.Description,
dbo.tblHIL_swift.DateOccur as start_date,
-- dbo.tblHIL_swift.DueDate as due_date,
-- dbo.tbl_master_acreg.acreg AS acreg,
--  dbo.tbl_acreg_owner.Own as OWN,
 COUNT(db_shil.dbo.tblHIL_swift.ItemID) AS total

FROM         db_shil.dbo.tblStatus 
             RIGHT OUTER JOIN db_shil.dbo.tblHIL_swift ON db_shil.dbo.tblStatus.StatusNo = db_shil.dbo.tblHIL_swift.Status 
--              LEFT OUTER JOIN  db_shil.dbo.tbl_master_actype 
-- 						 INNER JOIN		    db_shil.dbo.tbl_master_acreg ON db_shil.dbo.tbl_master_actype.ACTypeID = db_shil.dbo.tbl_master_acreg.actypeID ON db_shil.dbo.tblHIL_swift.acreg = db_shil.dbo.tbl_master_acreg.acreg
--   						LEFT JOIN  db_shil.dbo.tbl_acreg_owner On db_shil.dbo.tbl_master_actype.ACType=db_shil.dbo.tbl_acreg_owner.actype
						 JOIN db_dss.dbo.m_acreg ON db_shil.dbo.tblHIL_swift.acreg = db_dss.dbo.m_acreg.acreg
							LEFT JOIN db_dss.dbo.m_actype ON db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id

WHERE (SUBSTRING(TECHLOG, 1, 1)!='C' and (db_shil.dbo.tblHIL_swift.DueDate  > CONVERT(DATETIME,GETDATE(), 102))
OR 
(
(db_shil.dbo.tblHIL_swift.DueDate <= CONVERT(DATETIME, GETDATE(), 102)  or (db_shil.dbo.tblHIL_swift.DueDate is null )) 
AND
(db_shil.dbo.tblHIL_swift.status = 3)
)
OR
(
(db_shil.dbo.tblHIL_swift.status = 7)
)
)AND (db_shil.dbo.tblHIL_swift.Subject NOT LIKE '%ASDCS%')
 AND (db_shil.dbo.tblHIL_swift.Description NOT LIKE '%ASDCS%')

 GROUP BY db_shil.dbo.tblStatus.StatusDesc, db_dss.dbo.m_acreg.acreg, db_dss.dbo.m_acreg.own
 , db_dss.dbo.m_actype.actype,db_dss.dbo.m_actype.actype_id,dbo.tblHIL_swift.DateOccur
-- ,dbo.tbl_acreg_owner.own
-- ,dbo.tbl_master_acreg.acreg 
-- 	WITH rollup
-- 
) A
WHERE 
A.StatusDesc = 'Open'
AND A.ACType IS NOT NULL
AND A.OWN='CITILINK'
AND YEAR(CONVERT(DATE,A.start_date,102))='$year'
GROUP BY A.actype_id,A.StatusDesc,A.own,A.actype");
        return $query->result_array();
    }

    function citilink_detail_open($id)
    {
        $firstday = date('Y' . '-01-01');
        $date = date('Y-m-d');

        $query = $this->db_dss->query(
            "SELECT A.*  FROM (

SELECT     TOP 100 PERCENT 
db_shil.dbo.tblStatus.StatusDesc, 
db_dss.dbo.m_acreg.acreg,
db_dss.dbo.m_acreg.own,
db_dss.dbo.m_actype.actype,
db_dss.dbo.m_actype.actype_id,
dbo.tblHIL_swift.itemID as notification,
dbo.tblHIL_swift.Description,
CAST(dbo.tblHIL_swift.DateOccur AS DATE) as start_date,
CAST(dbo.tblHIL_swift.DueDate AS DATE) as due_date
FROM         db_shil.dbo.tblStatus 
             RIGHT OUTER JOIN db_shil.dbo.tblHIL_swift ON db_shil.dbo.tblStatus.StatusNo = db_shil.dbo.tblHIL_swift.Status 
					 JOIN db_dss.dbo.m_acreg ON db_shil.dbo.tblHIL_swift.acreg = db_dss.dbo.m_acreg.acreg
							LEFT JOIN db_dss.dbo.m_actype ON db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id

WHERE (SUBSTRING(TECHLOG, 1, 1)!='C' and (db_shil.dbo.tblHIL_swift.DueDate  > CONVERT(DATETIME,GETDATE(), 102))
OR 
(
(db_shil.dbo.tblHIL_swift.DueDate <= CONVERT(DATETIME, GETDATE(), 102)  or (db_shil.dbo.tblHIL_swift.DueDate is null )) 
AND
(db_shil.dbo.tblHIL_swift.status = 3)
)
OR
(
(db_shil.dbo.tblHIL_swift.status = 7)
)
)AND (db_shil.dbo.tblHIL_swift.Subject NOT LIKE '%ASDCS%')
 AND (db_shil.dbo.tblHIL_swift.Description NOT LIKE '%ASDCS%')

 GROUP BY db_shil.dbo.tblStatus.StatusDesc
,dbo.tblHIL_swift.itemID
,dbo.tblHIL_swift.Description
,dbo.tblHIL_swift.DateOccur
, dbo.tblHIL_swift.DueDate
,db_dss.dbo.m_acreg.acreg
, db_dss.dbo.m_acreg.own
, db_dss.dbo.m_actype.actype
,db_dss.dbo.m_actype.actype_id
-- ,dbo.tbl_acreg_owner.own
-- ,dbo.tbl_master_acreg.acreg 
-- 	WITH rollup
-- 
) A
WHERE 
A.StatusDesc = 'Open'
-- AND A.ACType IS NOT NULL
AND A.OWN='CITILINK'
AND A.actype_id='$id'");
        return $query->result_array();
    }

    function data_tren_hil($id_manufacture, $id_type, $week_start, $week_plus_one, $week_end)
    {
        if ($id_manufacture == "00") {
            $id_manufacture = "AND m_manufacture.mn_id in ('11','12')";
        } else if ($id_manufacture == "11") {
            $id_manufacture = "AND m_manufacture.mn_id = '11'";
        } else if ($id_manufacture == "12") {
            $id_manufacture = "AND m_manufacture.mn_id = '12'";
        }
        if ($id_type == "00") {
            $id_type = "";
        } else {
            $id_type = "AND m_actype.actype_id ='$id_type'";
        }
        $query = $this->db_dss->query(
          "SELECT DATEPART(WEEK,CONVERT (varchar,A.start_date,102)) as weeks,SUM(A.total) FROM (
              SELECT TOP 100 PERCENT 
              db_shil.dbo.tblStatus.StatusDesc, 
              db_dss.dbo.m_acreg.acreg,
              db_dss.dbo.m_acreg.own,
              db_dss.dbo.m_actype.actype,
              dbo.tblHIL_swift.DateOccur as start_date,
              dbo.tblHIL_swift.DueDate as due_date,

              COUNT(db_shil.dbo.tblHIL_swift.ItemID) AS total
              FROM         db_shil.dbo.tblStatus 
                          RIGHT OUTER JOIN db_shil.dbo.tblHIL_swift ON db_shil.dbo.tblStatus.StatusNo = db_shil.dbo.tblHIL_swift.Status 
                          JOIN db_dss.dbo.m_acreg ON db_shil.dbo.tblHIL_swift.acreg = db_dss.dbo.m_acreg.acreg
                            LEFT JOIN db_dss.dbo.m_actype ON db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id

              WHERE (SUBSTRING(TECHLOG, 1, 1)!='C' and (db_shil.dbo.tblHIL_swift.DueDate  > CONVERT(DATETIME,GETDATE(), 102))
              OR ((db_shil.dbo.tblHIL_swift.DueDate <= CONVERT(DATETIME, GETDATE(), 102)  or (db_shil.dbo.tblHIL_swift.DueDate is null )) 
                  AND(db_shil.dbo.tblHIL_swift.status = 3)
                  )
              OR
              (
              (db_shil.dbo.tblHIL_swift.status = 7)
              )
              )	AND (db_shil.dbo.tblHIL_swift.Subject NOT LIKE '%ASDCS%')
                AND (db_shil.dbo.tblHIL_swift.Description NOT LIKE '%ASDCS%')
              --   AND dbo.tblHIL_swift.DateOccur like '2018%'  
              -- 	AND db_shil.dbo.tblStatus.StatusDesc='Open'
                AND DATEPART(WEEK,CONVERT (varchar,dbo.tblHIL_swift.DateOccur,102))=DATEPART(WEEK,CONVERT (varchar,dbo.tblHIL_swift.DueDate,102))

              GROUP BY 
              db_shil.dbo.tblStatus.StatusDesc, 
              db_dss.dbo.m_acreg.acreg, 
              db_dss.dbo.m_acreg.own,
              db_dss.dbo.m_actype.actype,
              dbo.tblHIL_swift.DateOccur ,
              dbo.tblHIL_swift.DueDate
              -- ,dbo.tbl_acreg_owner.own
              -- ,dbo.tbl_master_acreg.acreg 
              -- 	WITH rollup
              -- 
              UNION ALL
              SELECT TOP 100 PERCENT 
              db_shil.dbo.tblStatus.StatusDesc, 
              db_dss.dbo.m_acreg.acreg,
              db_dss.dbo.m_acreg.own,
              db_dss.dbo.m_actype.actype,
              dbo.tblHIL_swift.DateOccur as start_date,
              dbo.tblHIL_swift.DueDate as due_date,

              COUNT(db_shil.dbo.tblHIL_swift.ItemID) AS total
              FROM         db_shil.dbo.tblStatus 
                          RIGHT OUTER JOIN db_shil.dbo.tblHIL_swift ON db_shil.dbo.tblStatus.StatusNo = db_shil.dbo.tblHIL_swift.Status 
                          JOIN db_dss.dbo.m_acreg ON db_shil.dbo.tblHIL_swift.acreg = db_dss.dbo.m_acreg.acreg
                            LEFT JOIN db_dss.dbo.m_actype ON db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id

              WHERE (SUBSTRING(TECHLOG, 1, 1)!='C' and (db_shil.dbo.tblHIL_swift.DueDate  > CONVERT(DATETIME,GETDATE(), 102))
              OR ((db_shil.dbo.tblHIL_swift.DueDate <= CONVERT(DATETIME, GETDATE(), 102)  or (db_shil.dbo.tblHIL_swift.DueDate is null )) 
                  AND(db_shil.dbo.tblHIL_swift.status = 3)
                  )
              OR
              (
              (db_shil.dbo.tblHIL_swift.status = 7)
              )
              )	AND (db_shil.dbo.tblHIL_swift.Subject NOT LIKE '%ASDCS%')
                AND (db_shil.dbo.tblHIL_swift.Description NOT LIKE '%ASDCS%')
              --   AND dbo.tblHIL_swift.DateOccur like '2018%'  
                AND DATEPART(WEEK,CONVERT (varchar,dbo.tblHIL_swift.DateOccur,102))!=DATEPART(WEEK,CONVERT (varchar,dbo.tblHIL_swift.DueDate,102))

              GROUP BY 
              db_shil.dbo.tblStatus.StatusDesc, 
              db_dss.dbo.m_acreg.acreg, 
              db_dss.dbo.m_acreg.own,
              db_dss.dbo.m_actype.actype,
              dbo.tblHIL_swift.DateOccur ,
              dbo.tblHIL_swift.DueDate
              -- ,dbo.tbl_acreg_owner.own
              -- ,dbo.tbl_master_acreg.acreg 
              -- 	WITH rollup
              -- 
              )A
              WHERE CONVERT (DATE,A.start_date,102)>='2018-01-01'
              AND CONVERT (DATE,A.start_date,102)<='2018-12-28'
              AND A.own='GA'
              AND A.StatusDesc='OPEN'
              GROUP BY DATEPART(WEEK,CONVERT (varchar,A.start_date,102))");
        return $query->result_array();

    }

    function data_tren_hil_garuda($id_manufacture, $id_type, $week_start, $week_plus_one, $week_end)
    {
        $year = date('Y') . '%';
        if ($id_manufacture == "00") {
            $id_manufacture = "AND m_manufacture.mn_id = '12'";
        } else {
            $id_manufacture = "AND m_manufacture.mn_id = '12'";
        }
        if ($id_type == "00") {
            $id_type = "";
        } else {
            $id_type = "AND m_actype.actype_id ='$id_type'";
        }

        $query = $this->db_dss->query("SELECT
                                      m_manufacture.mn_id AS id_manufacture,
                                      m_actype.actype_id AS id_type,
                                      COUNT (sap_hil_d3.notification) AS hil_open,
                                      '' AS hil_complete,
                                      DATEPART(
                                        week,
                                        CONVERT (
                                          DATE,
                                          sap_hil_d3.created_on,
                                          104
                                        )
                                      ) AS week_create,
                                      '' AS week_comp
                                    FROM
                                      m_manufacture
                                    INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
                                    INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                                    INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg
                                    WHERE
                                      (
                                        (
                                          DATEPART(
                                            week,
                                            CONVERT (
                                              DATE,
                                              sap_hil_d3.created_on,
                                              104
                                            )
                                          ) <= '$week_start'
                                          AND (
                                            system_status LIKE '%NOPR%'
                                            OR system_status LIKE '%OSNO%'
                                          )
                                        )
                                      )
                                      $id_manufacture
                                      $id_type
                                      and sap_hil_d3.created_on like '$year'
                                    GROUP BY
                                      m_manufacture.mn_id,
                                      m_actype.actype_id,
                                      DATEPART(
                                        week,
                                        CONVERT (
                                          DATE,
                                          sap_hil_d3.created_on,
                                          104
                                        )
                                      )
                                    UNION ALL
                                      SELECT
                                        m_manufacture.mn_id AS id_manufacture,
                                        m_actype.actype_id AS id_type,
                                        '' AS hil_open,
                                        COUNT (sap_hil_d3.notification) AS hil_complete,
                                        '' AS week_create,
                                        DATEPART(
                                          week,
                                          CONVERT (
                                            DATE,
                                            sap_hil_d3.completion,
                                            104
                                          )
                                        ) AS week_comp
                                      FROM
                                        m_manufacture
                                      INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
                                      INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                                      INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg
                                      WHERE
                                        (
                                          (
                                            DATEPART(
                                              week,
                                              CONVERT (
                                                DATE,
                                                sap_hil_d3.completion,
                                                104
                                              )
                                            ) BETWEEN '$week_plus_one'
                                            AND '$week_end'
                                            AND (system_status LIKE '%NOCO%')
                                          )
                                        )
                                      $id_manufacture
                                      $id_type
                                      and sap_hil_d3.completion like '$year'
                                      GROUP BY
                                        m_manufacture.mn_id,
                                        m_actype.actype_id,
                                        DATEPART(
                                          week,
                                          CONVERT (
                                            DATE,
                                            sap_hil_d3.completion,
                                            104
                                          )
                                        )");
        return $query->result_array();
    }

    function data_tren_hil_citilink($id_manufacture, $id_type, $week_start, $week_plus_one, $week_end)
    {
        $year = date('Y') . '%';
        if ($id_manufacture == "00") {
            $id_manufacture = "AND m_manufacture.mn_id = '11'";
        } else {
            $id_manufacture = "AND m_manufacture.mn_id = '11'";
        }
        if ($id_type == "00") {
            $id_type = "";
        } else {
            $id_type = "AND m_actype.actype_id ='$id_type'";
        }

        $query = $this->db_dss->query("SELECT
	m_manufacture.mn_id AS id_manufacture,
	m_actype.actype_id AS id_type,
	COUNT ( sap_hil_d3.notification ) AS hil_open,
	'' AS hil_complete,
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) ) AS week_create,
	'' AS week_comp 
FROM
	m_manufacture
	INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
	INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
	INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg 
WHERE
	(
	(
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) ) <= '$week_end' 
	) 
	) $id_manufacture 
	AND sap_hil_d3.created_on LIKE '$year'
GROUP BY
	m_manufacture.mn_id,
	m_actype.actype_id,
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) )
	
	
	UNION ALL
	
SELECT
	m_manufacture.mn_id AS id_manufacture,
	m_actype.actype_id AS id_type,
	'' AS hil_open,
	COUNT ( sap_hil_d3.notification ) AS hil_complete,
	'' AS week_create,
	DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.completion ) = 1 THEN CAST ( sap_hil_d3.completion AS DATE ) END ) ) AS week_comp 
FROM
	m_manufacture
	INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
	INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
	INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg 
WHERE
	(
	( DATEPART( week, ( CASE WHEN ISDATE( sap_hil_d3.completion ) = 1 THEN CAST ( sap_hil_d3.completion AS DATE ) END ) ) <= '$week_end' 
	AND ( system_status LIKE '%NOCO%' ) ) 
	) AND m_manufacture.mn_id = '$id_manufacture' 
	AND sap_hil_d3.created_on LIKE '$year'
GROUP BY
	m_manufacture.mn_id,
	m_actype.actype_id,
	DATEPART(
	week,
	( CASE WHEN ISDATE( sap_hil_d3.completion ) = 1 THEN CAST ( sap_hil_d3.completion AS DATE ) END ) 
	)");
        return $query->result_array();
    }

    function mitigasi_hil($id)
    {
        $query = $this->db_dss->query("SELECT
                                      sap_hil_d3.notification,
                                      tbl_mitigasi.mitigasi_why,
                                      tbl_mitigasi.mitigasi_solution
                                    FROM
                                    tbl_mitigasi
                                    LEFT JOIN  sap_hil_d3 ON sap_hil_d3.notification=tbl_mitigasi.mitigasi_fk_id
                                    WHERE sap_hil_d3.notification='$id'  AND tbl_mitigasi.mitigasi_type='hill'
                                    ");
        return $query->result_array();
    }

    function detail_grafik($id, $id_type, $week_start, $week_plus_one, $week_end)
    {
        $year = date('Y') . '%';
        if ($id_type == "00") {
            $id_type = "";
        } else {
            $id_type = "AND m_actype.actype_id ='$id_type'";
        }
        $query = $this->db_dss->query("SELECT
                                m_manufacture.mn_id AS id_manufacture,
                                m_actype.actype_id AS id_type,
                                sap_hil_d3.notification,
                                sap_hil_d3.functional_location AS nm_reg,
                                (( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) ) AS start_date,
                                (( CASE WHEN ISDATE( sap_hil_d3.req_start ) = 1 THEN CAST ( sap_hil_d3.req_start AS DATE ) END ) ) AS due_date,
                                sap_hil_d3.description,
                                CASE
                              WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR%' THEN
                                'OPEN'
                              WHEN sap_hil_d3.system_status LIKE 'NOPR ORAS' THEN
                                'OPEN'
                              WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORAS' THEN
                                'OPEN'
                              WHEN sap_hil_d3.system_status LIKE 'OSNO' THEN
                                'OPEN'
                              WHEN sap_hil_d3.system_status LIKE 'NOPR' THEN
                                'OPEN'
                              WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORCR' THEN
                                'OPEN'
                              END AS system_status
                              FROM
                                m_manufacture
                              INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
                              INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                              INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg
                              WHERE
                                (
                                  (
                                    DATEPART(
                                      week,
                                      (( CASE WHEN ISDATE( sap_hil_d3.created_on ) = 1 THEN CAST ( sap_hil_d3.created_on AS DATE ) END ) )
                                    ) <= '$week_start'
                                    AND (
                                      system_status LIKE '%NOPR%'
                                      OR system_status LIKE '%OSNO%'
                                    )
                                  )
                                )
                              AND m_manufacture.mn_id = $id
                              $id_type
                              AND sap_hil_d3.created_on LIKE '$year'
                              UNION ALL
                                SELECT
                                  m_manufacture.mn_id AS id_manufacture,
                                  m_actype.actype_id AS id_type,
                                  sap_hil_d3.notification,
                                  sap_hil_d3.functional_location AS nm_reg,
                                  sap_hil_d3.created_on AS start_date,
                                  sap_hil_d3.req_start AS due_date,
                                  sap_hil_d3.description,
                                  CASE
                                WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR%' THEN
                                  'OPEN'
                                WHEN sap_hil_d3.system_status LIKE 'NOPR ORAS' THEN
                                  'OPEN'
                                WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORAS' THEN
                                  'OPEN'
                                WHEN sap_hil_d3.system_status LIKE 'OSNO' THEN
                                  'OPEN'
                                WHEN sap_hil_d3.system_status LIKE 'NOPR' THEN
                                  'OPEN'
                                WHEN sap_hil_d3.system_status LIKE 'MEBS NOPR ORCR' THEN
                                  'OPEN'
                                END AS system_status
                                FROM
                                  m_manufacture
                                INNER JOIN m_acreg ON m_acreg.own = m_manufacture.mn_name
                                INNER JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
                                INNER JOIN sap_hil_d3 ON sap_hil_d3.functional_location = m_acreg.acreg
                                WHERE
                                  (
                                    (
                                      DATEPART(
                                        week,
                                        (( CASE WHEN ISDATE( sap_hil_d3.completion ) = 1 THEN CAST ( sap_hil_d3.completion AS DATE ) END ) )
                                      ) BETWEEN '$week_plus_one'
                                      AND '$week_end'
                                      AND (system_status LIKE '%NOCO%')
                                    )
                                  )
                                AND m_manufacture.mn_id = $id
                                $id_type
                                AND sap_hil_d3.created_on LIKE '$year'");
        return $query->result_array();
    }

    function dataTrend($customer,$start_date,$end_date,$aircraft){
      $years=explode('-',$start_date);
      $year=$years[0];
      if($aircraft!=00){
        $reg="AND db_dss.dbo.m_acreg.actype_id='$aircraft'";
      }else{
        $reg="";
      }
      $query=$this->db_dss->query(
        "SELECT COUNT(A.StatusDesc) as JML, DATEPART(WEEK,CONVERT(DATE,A.DateOccur,102)) As weeks FROM ( 
          SELECT 
          db_shil.dbo.tblHIL_swift.ItemID,
          db_shil.dbo.tblStatus.StatusDesc, 
          db_dss.dbo.m_acreg.acreg,
          db_dss.dbo.m_acreg.own,
          tblHIL_swift.DateOccur,
          tblHIL_swift.DueDate
          FROM [dbo].[tblHIL_swift]
          JOIN db_shil.dbo.tblStatus ON db_shil.dbo.tblStatus.StatusNo = db_shil.dbo.tblHIL_swift.Status 
          JOIN db_dss.dbo.m_acreg ON db_shil.dbo.tblHIL_swift.acreg = db_dss.dbo.m_acreg.acreg
          WHERE 
          (SUBSTRING(TECHLOG, 1, 1)!='C' and (db_shil.dbo.tblHIL_swift.DueDate  > CONVERT(DATETIME,GETDATE(), 102))
          OR ((db_shil.dbo.tblHIL_swift.DueDate <= CONVERT(DATETIME, GETDATE(), 102)  or (db_shil.dbo.tblHIL_swift.DueDate is null )) AND(db_shil.dbo.tblHIL_swift.status = 3)     )
            OR((db_shil.dbo.tblHIL_swift.status = 7))
          )	
          AND (db_shil.dbo.tblHIL_swift.Subject NOT LIKE '%ASDCS%')
          AND (db_shil.dbo.tblHIL_swift.Description NOT LIKE '%ASDCS%') 
          AND  tblHIL_swift.DateOccur like '$year%'
          $reg
          AND db_dss.dbo.m_acreg.own='$customer'
          AND db_shil.dbo.tblStatus.StatusDesc='OPEN'
          ) A
          GROUP BY DATEPART(WEEK,CONVERT(DATE,A.DateOccur,102))"
      );
      return $query->result_array();
    }

    function dataTrend_ct($customer,$start_date,$end_date,$aircraft){
      $years=explode('-',$start_date);
      $year=$years[0];
       if($aircraft!=00){
        $reg="AND db_dss.dbo.m_acreg.actype_id='$aircraft'";
      }else{
        $reg="";
      }
      $sql="SELECT COUNT(A.StatusDesc) as JML, DATEPART(WEEK,CONVERT(DATE,A.DateOccur,102)) As weeks FROM ( 
          SELECT 
          db_shil.dbo.tblHIL_swift.ItemID,
          db_shil.dbo.tblStatus.StatusDesc, 
          db_dss.dbo.m_acreg.acreg,
          db_dss.dbo.m_actype.actype,
          db_dss.dbo.m_actype.actype_id,
          db_dss.dbo.m_acreg.own,
          tblHIL_swift.DateOccur,
          tblHIL_swift.DueDate
          FROM [dbo].[tblHIL_swift]
          JOIN db_shil.dbo.tblStatus ON db_shil.dbo.tblStatus.StatusNo = db_shil.dbo.tblHIL_swift.Status 
          JOIN db_dss.dbo.m_acreg ON db_shil.dbo.tblHIL_swift.acreg = db_dss.dbo.m_acreg.acreg
					LEFT JOIN db_dss.dbo.m_actype ON db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id
          WHERE 
          (SUBSTRING(TECHLOG, 1, 1)!='C' and (db_shil.dbo.tblHIL_swift.DueDate  > CONVERT(DATETIME,GETDATE(), 102))
          OR ((db_shil.dbo.tblHIL_swift.DueDate <= CONVERT(DATETIME, GETDATE(), 102)  or (db_shil.dbo.tblHIL_swift.DueDate is null )) AND(db_shil.dbo.tblHIL_swift.status = 3)     )
            OR((db_shil.dbo.tblHIL_swift.status = 7))
          )	
          AND (db_shil.dbo.tblHIL_swift.Subject NOT LIKE '%ASDCS%')
          AND (db_shil.dbo.tblHIL_swift.Description NOT LIKE '%ASDCS%') 
          AND  tblHIL_swift.DateOccur like '2018%'
          $reg
          AND db_shil.dbo.tblStatus.StatusDesc='OPEN'
          AND db_dss.dbo.m_acreg.own='CITILINK'
          ) A
          GROUP BY DATEPART(WEEK,CONVERT(DATE,A.DateOccur,102))";
      $query=$this->db_dss->query($sql);
      return $query->result_array();
    }

    function detail_grafiks($id, $id_type, $week)
    {
        $year = date('Y') . '%';
        if ($id_type == "00") {
            $reg = "";
        } else {
            $reg = "AND m_actype.actype_id ='$id_type'";
        }

        if ($id == "11") {
          $customer = "AND db_dss.dbo.m_acreg.own='CITILINK'";
        } else if($id == "12"){
          $customer = "AND db_dss.dbo.m_acreg.own='GA'";
        }else{
          $customer = "";
        }
        $query = $this->db_dss->query("SELECT 
                  db_shil.dbo.tblHIL_swift.ItemID as notification,
                  db_shil.dbo.tblStatus.StatusDesc as system_status ,
                  db_dss.dbo.m_acreg.acreg as nm_reg, 
                  dbo.tblHIL_swift.Description as description,
                  db_dss.dbo.m_actype.actype,
                  db_dss.dbo.m_actype.actype_id,
                  db_dss.dbo.m_acreg.own,
                  CAST(dbo.tblHIL_swift.DateOccur AS DATE) as start_date,
                  CAST(dbo.tblHIL_swift.DueDate AS DATE) as due_date
                  FROM [dbo].[tblHIL_swift]
                  JOIN db_shil.dbo.tblStatus ON db_shil.dbo.tblHIL_swift.Status = db_shil.dbo.tblStatus.StatusNo 
                  JOIN db_dss.dbo.m_acreg ON db_shil.dbo.tblHIL_swift.acreg = db_dss.dbo.m_acreg.acreg
                  LEFT JOIN db_dss.dbo.m_actype ON db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id
                  WHERE 
                  (SUBSTRING(TECHLOG, 1, 1)!='C' and (db_shil.dbo.tblHIL_swift.DueDate  > CONVERT(DATETIME,GETDATE(), 102))
                  OR ((db_shil.dbo.tblHIL_swift.DueDate <= CONVERT(DATETIME, GETDATE(), 102)  or (db_shil.dbo.tblHIL_swift.DueDate is null )) AND(db_shil.dbo.tblHIL_swift.status = 3)     )
                    OR((db_shil.dbo.tblHIL_swift.status = 7))
                  )	
                  AND (db_shil.dbo.tblHIL_swift.Subject NOT LIKE '%ASDCS%')
                  AND (db_shil.dbo.tblHIL_swift.Description NOT LIKE '%ASDCS%') 
                  AND  tblHIL_swift.DateOccur like '2018%'
                  AND db_shil.dbo.tblStatus.StatusDesc='OPEN'
                  $reg
                  AND db_shil.dbo.tblHIL_swift.Status ='1'
                  $customer
                  AND DATEPART(WEEK,CONVERT(DATE,tblHIL_swift.DateOccur,102)) <=' $week'");
        return $query->result_array();
    }


}

