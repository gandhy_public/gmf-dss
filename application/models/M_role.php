<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  /**
   *
   */
  class M_role extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    public function show_datatable($data)
    {
      $total = $this->db->count_all_results("role");
      $otuput = array();
      $output["draw"] = $data["draw"];
      $output["recordsTotal"] = $output["recordsFiltered"] = $total;
      $output["data"] = array();
      if($data["search"] != ""){
        $this->db->or_like("role_name", $data["search"]);
      }
      $this->db->where("role_isactive", "1");
      $this->db->limit($data["length"], $data["start"]);
      $this->db->order_by($data["order_column"], $data["order_dir"]);
      $query = $this->db->get("role");
      if($data["search"] != ""){
        $this->db->where("role_isactive", "1");
        $this->db->or_like("role_name", $data["search"]);
        $jum = $this->db->get("role");
        $output["recordsTotal"] = $output["recordsFiltered"] = $jum->num_rows();
      }

      $nomor_urut = $data["start"]+1;
      foreach ($query->result_array() as $role) {
        $output["data"][] = array(
          $nomor_urut,
          SaveTextPass("GMF", $role["role_id"]),
          $role["role_name"],
        );
      $nomor_urut++;
      }
      echo json_encode($output, JSON_PRETTY_PRINT);
    }

    public function save_role()
    {
      $role_name = $this->input->post("role_name");
      $sql = "insert into role(role_name) values ('".$role_name."')";
      return ($this->db->query($sql)) ? true : false;
    }

    public function get_role_where($id)
    {
      $this->db->where("role_id", GetSaveTextPass("GMF", $id));
      $this->db->where("role_isactive", "1");
      $query = $this->db->get("role");
      return $query->row();
    }

    public function update_role()
    {
      $id = GetSaveTextPass("GMF", $this->input->post("role_id"));
      $role_name = $this->input->post("role_name");
      $sql = "update role set role_name = '".$role_name."' where role_id = '".$id."'";
      return ($this->db->query($sql)) ? true : false;
    }

    public function del_role($id)
    {
      $id = GetSaveTextPass("GMF", $id);
      $query = "update role set role_isactive = '0' where role_id = '".$id."'";
      return ($this->db->query($query)) ? true : false;
    }
	
	
       public function get_menu()
       {

		// $id = GetSaveTextPass("GMF", $id);

           $query = $this->db->query("select * from menu_item where menu_item_isparent = '0' and menu_item_isactive='1'");


           if ($query->num_rows() > 0)
           {
             $data = array();
               foreach ($query->result() as $row)
               {

               
                    $query2 = $this->db->query("select * from menu_item where menu_item_isparent != '0' and menu_item_isactive='1' and menu_item_isparent = '". $row->menu_item_id."'");
                  // $query2 = $this->db->get("menu_item");
                   $data2 = array();
                   foreach ($query2->result() as $row2) {
                     $data2[] = $row2;
                   }
                   $data[] = array(
                     'menu_item_id' => $row->menu_item_id,
                     'menu_item_name' => $row->menu_item_name,
                     'menu_item_link' => $row->menu_item_link,
                     'menu_item_isparent' => $row->menu_item_isparent,
                     'child' => $data2
                   );
               }
         // print_r($data);
               return $data;
           }

           return false;
       }
	   
	    public function get_role($id, $status)
       {

			$id = GetSaveTextPass("GMF", $id);
			if($status=="induk"){
				$parent = " AND menu_item.menu_item_isparent = '0' ";
			}else{
				$parent = " AND menu_item.menu_item_isparent != '0' ";
			}

           $query = $this->db->query("SELECT
			  menu_item.menu_item_id,
			  menu_item_name,
			  menu_item_isparent,
			  menu_item_link
			FROM menu_item,
			  menu_role
			WHERE menu_item.menu_item_id = menu_role.menu_item_id
				AND menu_role.role_id = ".$id."  AND menu_item.menu_item_isactive ='1'
					 ".$parent." ORDER BY menu_item_id");


           if ($query->num_rows() > 0)
           {
             $data = array();
               foreach ($query->result() as $row)
               {

                   $data[] = array(
                     'menu_item_id' => $row->menu_item_id,
                     'menu_item_name' => $row->menu_item_name,
                     'menu_item_link' => $row->menu_item_link,
                     'menu_item_isparent' => $row->menu_item_isparent,
                   );
               }
               return $data;
           }

           return false;
       }
	   
	     public function cek_role($id_user,$id_menu)
	{
		
			$id_user = GetSaveTextPass("GMF", $id_user);
		$sql = "SELECT menu_item_id FROM menu_role WHERE role_id ={$id_user} AND menu_item_id = {$id_menu}";
		$num = $this->db->query($sql)->num_rows();
		return $num;
	}
	     public function add_role($id_user,$id_menu)
	{
		$id_user = GetSaveTextPass("GMF", $id_user);
		$sql = "INSERT INTO menu_role VALUES ({$id_user}, {$id_menu})";
		$this->db->query($sql);
	}
	   
	     public function delete_role($id_user,$id_menu)
	{
		$id_user = GetSaveTextPass("GMF", $id_user);
		$sql = "delete from menu_role WHERE role_id={$id_user} AND menu_item_id={$id_menu}";
		$this->db->query($sql);
	}
	   
	
  }
