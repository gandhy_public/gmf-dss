<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class M_mro_transaction extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->dev_gmf=$this->load->database('dev_gmf',true);;
    }

    public function get_all_aircraft_type()
    {
        $query = $this->db->query("select distinct(aircraft_type) from mro_transaction order by aircraft_type asc");
        return $query->result_array();
    }

    public function get_total_cost($params, $region_id, $year_from, $year_to)
    {
        $this->db->select('IFNULL(SUM(total_cost), 0) as total');
        $this->db->from('mro_transaction');
        $this->db->where($params);
        if (isset($region_id) && $region_id != 'all')
            $this->db->where_in('id_country', $this->get_country_id_by_region($region_id));
        $this->db->where('year >=', $year_from);
        $this->db->where('year <=', $year_to);
        $query = $this->db->get();
        $row = $query->row();
        return $row->total;
    }

    public function get_total_cost_v2($params, $region_id, $year_from, $year_to)
    {
        $this->dev_gmf->select('ISNULL(SUM(total_cost), 0) as total');
        $this->dev_gmf->from('mro');
        $this->dev_gmf->where($params);
        if (isset($region_id) && $region_id != 'all')
            // $this->db->where_in('id_country', $this->get_country_id_by_region($region_id));
            $this->dev_gmf->where('region', $region_id);
        $this->dev_gmf->where('year >=', $year_from);
        $this->dev_gmf->where('year <=', $year_to);
        $query = $this->dev_gmf->get();
        $row = $query->row();
        return $row->total;
    }


    function get_country_id_by_region($region_id)
    {
        $query = $this->db->query('select id_country from mro_country where id_region = ' . $region_id);
        $result = $query->result_array();
        $countries = array();
        foreach ($result as $res) {
            array_push($countries, $res['id_country']);
        }
        if (count($countries) > 0)
            return $countries;
        else
            return -1;
    }
}
