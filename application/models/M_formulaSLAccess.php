<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class M_formulaSLAccess extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('dev_gmf', TRUE);
    }

    public function get_panel()
    {
        $dt = $this->db->query("SELECT
            DISTINCT material
        FROM
            [dbo].[scc_trans]");

        $dt = array_column($dt->result_array(), 'material');
        // print_r($dt); exit();

        for ($i=0; $i < count($dt); $i++) { 
            $dt1 = $this->db->query("
                SELECT A.*, DATEDIFF(DAY, A.old_date, A.new_date) AS interval FROM (
                    SELECT top 1
                        material, 
                        entered_at, 
                        ess,
                        (SELECT TOP 1 ( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) FROM scc_trans WHERE material = '".$dt[$i]."' ORDER BY posting_date DESC) AS new_date,
                        (SELECT TOP 1 ( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) FROM scc_trans WHERE material = '".$dt[$i]."' ORDER BY posting_date) AS old_date
                    FROM
                        scc_trans
                        JOIN scv_part_number ON REPLACE(LEFT ( scc_trans.material, CHARINDEX(':', scc_trans.material) ), ':', '') = scv_part_number.part_number
                    WHERE material = '".$dt[$i]."') A"
            );
            if(!empty($dt1->result_array())) {
                $dt2[] = $dt1->result_array();
            };
        }

        for ($i=0; $i < count($dt2); $i++) { 
            if ($dt2[$i][0]['interval'] < 21 && $dt2[$i][0]['ess'] == 1) {
                print_r($dt2[$i][0]);
                print_r('SL 2 24 Jam');
            } else if ($dt2[$i][0]['interval'] >= 21 && $dt2[$i][0]['ess'] == 1) {
                print_r($dt2[$i][0]);
                print_r('SL 1 3 Jam');
            } else if ($dt2[$i][0]['ess'] == 2) {
                print_r($dt2[$i][0]);
                print_r('SL 3 72 Jam');
            } else {
                print_r($dt2[$i][0]);
                print_r('SL 4 72 Jam');                
            }
        }

    }

}
