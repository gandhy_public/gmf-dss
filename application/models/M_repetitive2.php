<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: ucup
 * Date: 4/26/18
 * Time: 5:25 PM
 */
class M_repetitive2 extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->dev_gmf = $this->load->database('dev_gmf', TRUE);
        // $this->rep=$this->load->database('rep_rob',true);
        $this->dev_gmf_prod = $this->load->database('prod_reprob',true);//$this->load->database('prod_reprob', TRUE)
    }

    public function periode()
    {
        $periode = array(
            'start' => date('Y') . '-01' . '-01',
            'endMonth' => date('m'),
            'endYear' => date('Y')
        );

        return $periode;
    }

    public function getRasio($tipe){
      $query = $this->dev_gmf_prod->query("
      SELECT C.A AS Data_Open, C.B AS TOTAL
      	FROM (
      		SELECT ( SELECT
      			COUNT(r.status) AS JML
      		FROM
      			db_reprob.dbo.TBL_REP_INDEX r JOIN db_dss.dbo.m_acreg d
      				ON r.acreg = d.acreg
      			JOIN db_dss.dbo.m_actype t
      				ON d.actype_id = t.actype_id
      		WHERE
      			LEFT ( r.firstOccureDate, 4 ) = '".date('Y')."'
      			AND status IN ('open','monitor','reminder')
      			AND d.own = '".$tipe."'
      		) A ,

      		(SELECT
      			COUNT(r.status) AS JML
      		FROM
      			db_reprob.dbo.TBL_REP_INDEX r JOIN db_dss.dbo.m_acreg d
      				ON r.acreg = d.acreg
      			JOIN db_dss.dbo.m_actype t
      				ON d.actype_id = t.actype_id
      		WHERE
      			LEFT ( r.firstOccureDate, 4 ) = '".date('Y')."'
      			AND d.own = '".$tipe."'
      		) B
      	) C");
        return $query->row_array();
    }

    public function acRegGA($data, $tipe)
    {
        if ($data == '') {
            $acRegs = $this->dev_gmf->select('acreg')
                ->from('m_acreg')
                ->where('own', $tipe)
                ->get()
                ->result_array();
        } else {
            $acType = $this->dev_gmf->select('actype_id')
                ->from('m_actype')
                ->where('actype_id', $data)
                ->get()->row();

            $acRegs = $this->dev_gmf->select('acreg')
                ->from('m_acreg')
                ->where('own', $tipe)
                ->where('actype_id', $acType->actype_id)
                ->get()
                ->result_array();
        }

        return $acRegs;
    }

    public function acRegCitilink($data)
    {
        if ($data == '') {
            $acRegs = $this->dev_gmf->select('acreg')
                ->from('m_acreg')
                ->where('own', 'CITILINK')
                ->get()
                ->result_array();
        } else {
            $acType = $this->dev_gmf->select('actype_id')
                ->from('m_actype')
                ->where('actype_id', $data)
                ->get()->row();
            $acRegs = $this->dev_gmf->select('acreg')
                ->from('m_acreg')
                ->where('own', 'CITILINK')
                ->where('actype_id', $acType)
                ->get()
                ->result_array();
        }

        return $acRegs;
    }

    public function getAllData($status)
    {
        if ($status == 'GA') {
            //array1Dimensional
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'GA')));
            foreach ($it as $v) {
                array_push($finalData, $v);
            }

        } else {
            //array1Dimensional
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'CITILINK'))); //acRegCitilink('')
            foreach ($it as $v) {
                array_push($finalData, $v);
            }
        }

        // $allDatas = $this->dev_gmf->select("*")
        //     ->from('sap_repetitive_d2s')
        //     ->where_in('functional_location', $finalData)
        //     ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
        //     ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
        //     ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
        //     ->get()->result_array();

        $allDatas = $this->dev_gmf_prod->select("*")
            ->from('TBL_REP_INDEX')
            ->where_in('acreg', $finalData)
            ->where("CONVERT(date, firstOccureDate, 104) >=", $this->periode()['start'])
            ->where("MONTH(CONVERT(date, firstOccureDate, 104)) <=", $this->periode()['endMonth'])
            ->where("YEAR(CONVERT(date, firstOccureDate, 104)) <=", $this->periode()['endYear'])
            ->get()->result_array();


        return $allDatas;
    }

    //RevisiVersion2
    public function getDataGaruda()
    {
        //array1Dimensional
        $finalData = array();
        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'GA')));
        foreach ($it as $v) {
            array_push($finalData, $v);
        }

        $garudas = $this->dev_gmf_prod->select('*')
            ->from('TBL_REP_INDEX')
            ->where_in('acreg', $finalData)
            ->where("LEFT ( firstOccureDate, 4 ) = ", date('Y'))///date('m')
            ->where("status in ('open','monitor','reminder')")
            ->get()->result_array();

        return $garudas;

    }

    public function getDataGarudaPeriode()
    {
        //array1Dimensional
        $finalData = array();
        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'GA')));
        foreach ($it as $v) {
            array_push($finalData, $v);
        }

        // $garudas = $this->dev_gmf->select('notification')
        //     ->from('repetitive_d5_final')
        //     ->where_in('functional_location', $finalData)
        //     ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
        //     ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
        //     ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
        //     ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE)
        //     ->distinct()
        //     ->get()->result_array();


        $garudas = $this->dev_gmf_prod->select('notif')
            ->from('TBL_REP_INDEX')
            ->where_in('acreg', $finalData)
            ->where("CONVERT(date, firstOccureDate, 104) >=", $this->periode()['start'])
            ->where("MONTH(CONVERT(date, firstOccureDate, 104)) <=", $this->periode()['endMonth'])
            ->where("YEAR(CONVERT(date, firstOccureDate, 104)) <=", $this->periode()['endYear'])
            ->where("(status in ('open','reminder','monitor'))", NULL, FALSE)
            ->distinct()
            ->get()->result_array();

        return $garudas;

    }

    public function getDataCitilink()
    {

        //array1Dimensional
        $finalData = array();
        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegCitilink('')));
        foreach ($it as $v) {
            array_push($finalData, $v);
        }

        $citilinks = $this->dev_gmf_prod->select('*')
            ->from('TBL_REP_INDEX')
            ->where_in('acreg', $finalData)
            ->where("LEFT ( firstOccureDate, 4 ) = ", date('Y'))//date('m')
            ->where("status in ('open','monitor','reminder')")
            ->get()->result_array();

        return $citilinks;
    }

    public function getDataCitilinkPeriode()
    {

        //array1Dimensional
        $finalData = array();
        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegCitilink('')));
        foreach ($it as $v) {
            array_push($finalData, $v);
        }

        // $citilinks = $this->dev_gmf->select('*')
        //     ->from('repetitive_d5_final')
        //     ->where_in('functional_location', $finalData)
        //     ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
        //     ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
        //     ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
        //     ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE)
        //     ->distinct()
        //     ->get()->result_array();

        $citilinks = $this->dev_gmf_prod->select('notif')
            ->from('TBL_REP_INDEX')
            ->where_in('acreg', $finalData)
            ->where("CONVERT(date, firstOccureDate, 104) >=", $this->periode()['start'])
            ->where("MONTH(CONVERT(date, firstOccureDate, 104)) <=", $this->periode()['endMonth'])
            ->where("YEAR(CONVERT(date, firstOccureDate, 104)) <=", $this->periode()['endYear'])
            ->where("(status in ('open','reminder','monitor'))", NULL, FALSE)
            ->distinct()
            ->get()->result_array();

        return $citilinks;
    }



    public function getTarget()
    {
        $getTarget = $this->db->select('target_val')
            ->from('tbl_all_target')
            ->where('target_type', 'Repetitive')
            ->where('target_year', date('Y'))
            ->get()->row();

        return $getTarget->target_val;
    }

    public function showDatatable($data, $status)
    {
        if ($status == 'Garuda') {
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'GA')));
            foreach ($it as $v) {
                array_push($finalData, $v);
            }
        } elseif ($status == 'Citilink') {
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'CITILINK')));
            foreach ($it as $v) {
                array_push($finalData, $v);
            }
        }

            $this->dev_gmf_prod->select('*');
            $this->dev_gmf_prod->from('TBL_REP_INDEX');
            $this->dev_gmf_prod->where_in('TBL_REP_INDEX.acreg', $finalData);
            $this->dev_gmf_prod->where("LEFT ( firstOccureDate, 4 ) =", date('Y'));//date('m')
            // $this->dev_gmf_prod->join('db_dss.dbo.m_acreg', 'TBL_REP_INDEX.acreg = db_dss.dbo.m_acreg.acreg', 'left');
            // $this->dev_gmf_prod->join('db_dss.dbo.m_actype', 'db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id', 'left');
            $this->dev_gmf_prod->where("status in ('open','monitor','reminder')");
        // $this->dev_gmf->select('*');
        // $this->dev_gmf->from('repetitive_d5_final');
        // $this->dev_gmf->where_in('functional_location', $finalData);
//        $this->dev_gmf->where("CONVERT(date, created_on, 104) >=", $this->periode()['start']);
//        $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth']);
//        $this->dev_gmf->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear']);
        // $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'));
        // $this->dev_gmf->join('m_acreg', 'repetitive_d5_final.functional_location = m_acreg.acreg', 'left');
        // $this->dev_gmf->join('m_actype', 'm_actype.actype_id = m_acreg.actype_id', 'left');
        // $this->dev_gmf->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE);

        $query1 = $this->dev_gmf_prod->get();
        $total = $query1->num_rows();

        $output = array();
        $output["draw"] = $data["draw"];
        $output["recordsTotal"] = $output["recordsFiltered"] = $total;
        $output["data"] = array();
        if ($data["search"] != "") {
            $this->dev_gmf_prod->like('actype', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("LEFT ( firstOccureDate, 4 ) =", date('Y'))//date('m')
                // ->where("status in ('open','monitor','reminder')");

//                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
//                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
//                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
            //     ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
            //     ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('acreg', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("LEFT ( firstOccureDate, 4 ) =", date('Y'))//date('m')
                // ->where("status in ('open','monitor','reminder')");
            //     ->where_in('functional_location', $finalData)
//                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
//                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
//                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
            //     ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
            //     ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('problem', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("LEFT ( firstOccureDate, 4 ) =", date('Y'))//date('m')
                // ->where("status in ('open','monitor','reminder')");
            //     ->where_in('functional_location', $finalData)
//                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
//                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
//                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
            //     ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
            //     ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('status', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("LEFT ( firstOccureDate, 4 ) =", date('Y'))//date('m')
                // ->where("status in ('open','monitor','reminder')");
            //     ->where_in('functional_location', $finalData)
//                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
//                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
//                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
                // ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
                // ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE);
        }

        $this->dev_gmf_prod->select('*');
        $this->dev_gmf_prod->from('TBL_REP_INDEX');
        $this->dev_gmf_prod->where_in('TBL_REP_INDEX.acreg', $finalData);
        // $this->dev_gmf_prod->where_in('acreg', $finalData);
        $this->dev_gmf_prod->where("LEFT ( firstOccureDate, 4 ) =", date('Y'));//date('m')
        // $this->dev_gmf_prod->join('db_dss.dbo.m_acreg', 'TBL_REP_INDEX.acreg = db_dss.dbo.m_acreg.acreg', 'left');
        // $this->dev_gmf_prod->join('db_dss.dbo.m_actype', 'db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id', 'left');
        $this->dev_gmf_prod->where("status in ('open','monitor','reminder')");
//        $this->dev_gmf->where("CONVERT(date, created_on, 104) >=", $this->periode()['start']);
//        $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth']);
//        $this->dev_gmf->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear']);
        // $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'));
        // $this->dev_gmf->join('m_acreg', 'repetitive_d5_final.functional_location = m_acreg.acreg', 'left');
        // $this->dev_gmf->join('m_actype', 'm_actype.actype_id = m_acreg.actype_id', 'left');
        // $this->dev_gmf->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE);
        $this->dev_gmf_prod->limit($data["length"], $data["start"]);
        $this->dev_gmf_prod->order_by($data["order_column"], $data["order_dir"]);

        if ($data["search"] != "") {
            $this->dev_gmf_prod->like('actype', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("LEFT ( firstOccureDate, 4 ) =", date('Y'))//date('m')
                // ->where("status in ('open','monitor','reminder')");
            //     ->where_in('functional_location', $finalData)
//                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
//                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
//                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
            //     ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
            //     ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('acreg', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("LEFT ( firstOccureDate, 4 ) =", date('Y'))//date('m')
                // ->where("status in ('open','monitor','reminder')");
            //     ->where_in('functional_location', $finalData)
//                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
//                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
//                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
            //     ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
            //     ->where("(system_status LIKE '%OPEN" . "%' OR system_status LIKE '%NOPR')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('problem', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("LEFT ( firstOccureDate, 4 ) =", date('Y'))//date('m')
                // ->where("status in ('open','monitor','reminder')");
            //     ->where_in('functional_location', $finalData)
//                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
//                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
//                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
            //     ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
            //     ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('status', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("LEFT ( firstOccureDate, 4 ) =", date('Y'))//date('m')
                // ->where("status in ('open','monitor','reminder')");
            //     ->where_in('functional_location', $finalData)
//                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
//                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
//                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
                // ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
                // ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE);


            $query = $this->dev_gmf_prod->get();
            $output["recordsTotal"] = $output["recordsFiltered"] = $query->num_rows();
        }else{
          $query = $this->dev_gmf_prod->get();
        }

        $tblHaveMitigations = $this->dev_gmf->select('mitigasi_fk_id')
            ->from('tbl_mitigasi')
            ->where('mitigasi_type', 'repetitive')
            ->get()->result_array();
        $convertArrays = new RecursiveIteratorIterator(new RecursiveArrayIterator($tblHaveMitigations));
        $tblHaveMitigation = array();
        foreach ($convertArrays as $convertArray) {
            array_push($tblHaveMitigation, $convertArray);
        }
        $nomor_urut = $data['start'] + 1;

        if($query != "" || $query != null){
        foreach ($query->result_array() as $data) {
            if (in_array((int)$data['notif'], $tblHaveMitigation)) {
                $haveMitigation = 1;
            } else {
                $haveMitigation = 0;
            }

            if (strpos($data['status'], 'reminder') !== false or strpos($data['status'], 'open') !== false or strpos($data['status'], 'monitor') !== false) {
                $data['status'] = 'OPEN';
            }

            $output['data'][] = array(
                $nomor_urut,
                $data['notif'],
                $data['actype'],
                $data['acreg'],
                $data['problem'],
                date('d-m-Y', strtotime($data['firstOccureDate'])),
                date('d-m-Y', strtotime($data['targetAction'])),
                $data['status'],
                $haveMitigation
            );
            $nomor_urut++;
        }
      }else{
        $output['data'][] = array(
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        );
      }
        echo json_encode($output, JSON_PRETTY_PRINT);
    }

    public function getDataChart($status, $input)
    {
        if ($input['startPeriode'] == '' and $input['endPeriode'] == '') {
            $year = date('Y');
            $monthFrom = $year . '-01';
            $monthTo = date('Y-m');
            $listAircraftType = $input['list_aircraft_type'];
        } else {
            $monthFrom = $input['startPeriode'];
            $monthTo = $input['endPeriode'];
            $listAircraftType = $input['list_aircraft_type'];
        }

        $start = (new DateTime($monthFrom))->modify('first day of this month');
        $end = (new DateTime($monthTo))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        $data = array();

        foreach ($period as $dt) {

            //array1Dimensional
            if ($status == 'Garuda') {
                $final = array();
                $its = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA($listAircraftType, 'GA')));
                foreach ($its as $v) {
                    array_push($final, $v);
                }
            } elseif ($status == 'Citilink') {
                $final = array();
                $its = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA($listAircraftType, 'CITILINK')));
                foreach ($its as $v) {
                    array_push($final, $v);
                }
            }

            $query = $this->dev_gmf_prod->select('*')
            ->from('TBL_REP_INDEX')
            ->where_in('acreg', $final)
            ->where("MONTH(CONVERT(date, firstOccureDate, 104)) >=", date('m', strtotime($dt->format('Y-m') . '-01')))
            ->where("YEAR(CONVERT(date, firstOccureDate, 104)) >=", date('Y', strtotime($dt->format('Y-m') . '-01')))
            ->where("MONTH(CONVERT(date, firstOccureDate, 104)) <=", date('m', strtotime($dt->format('Y-m') . '-01')))
            ->where("YEAR(CONVERT(date, firstOccureDate, 104)) <=", date('Y', strtotime($dt->format('Y-m') . '-01')))
            ->where("(status in ('open','reminder','monitor'))", NULL, FALSE)
                // ->from('repetitive_d5_finals')
                // ->where_in('functional_location', $final)
                // ->where("MONTH(CONVERT(date, created_on, 104)) >=", date('m', strtotime($dt->format('Y-m') . '-01')))
                // ->where("YEAR(CONVERT(date, created_on, 104)) >=", date('Y', strtotime($dt->format('Y-m') . '-01')))
                // ->where("MONTH(CONVERT(date, created_on, 104)) <=", date('m', strtotime($dt->format('Y-m') . '-01')))
                // ->where("YEAR(CONVERT(date, created_on, 104)) <=", date('Y', strtotime($dt->format('Y-m') . '-01')))
                // ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE)
                ->get()->result_array();
            // print_r($this->dev_gmf->last_query());
            // exit();
            array_push($data, count($query));
        }

        return $data;
    }

    public function showDatatableDetail($data, $aircraft, $month, $year, $input)
    {

        if ($aircraft == 'Garuda') {
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA($input['list_aircraft_type'], 'GA')));
            foreach ($it as $v) {
                array_push($finalData, $v);
            }
        } elseif ($aircraft == 'Citilink') {
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA($input['list_aircraft_type'], 'CITILINK')));
            foreach ($it as $v) {
                array_push($finalData, $v);
            }
        }


        $this->dev_gmf_prod->select('*');
        $this->dev_gmf_prod->from('TBL_REP_INDEX');
        $this->dev_gmf_prod->where_in('TBL_REP_INDEX.acreg', $finalData);
        $this->dev_gmf_prod->where("MONTH(CONVERT(date, firstOccureDate, 104)) =", $month);
        $this->dev_gmf_prod->where("YEAR(CONVERT(date, firstOccureDate, 104)) =", $year);
        $this->dev_gmf_prod->join('db_dss.dbo.m_acreg', 'TBL_REP_INDEX.acreg = db_dss.dbo.m_acreg.acreg', 'left');
        $this->dev_gmf_prod->join('db_dss.dbo.m_actype', 'db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id', 'left');
        $this->dev_gmf_prod->where("(status in ('open','reminder','monitor'))", NULL, FALSE);
        $this->dev_gmf_prod->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        // $this->dev_gmf->select('*');
        // $this->dev_gmf->from('repetitive_d5_final');
        // $this->dev_gmf->where_in('functional_location', $finalData);
        // $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) =", $month);
        // $this->dev_gmf->where("YEAR(CONVERT(date, created_on, 104)) =", $year);
        // $this->dev_gmf->join('m_acreg', 'repetitive_d5_final.functional_location = m_acreg.acreg', 'left');
        // $this->dev_gmf->join('m_actype', 'm_actype.actype_id = m_acreg.actype_id', 'left');
        // $this->dev_gmf->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE);
        // $this->dev_gmf->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        $query1 = $this->dev_gmf_prod->get();
        $total = $query1->num_rows();

        $output = array();
        $output["draw"] = $data["draw"];
        $output["recordsTotal"] = $output["recordsFiltered"] = $total;
        $output["data"] = array();
        if ($data["search"] != "") {
            $this->dev_gmf_prod->like('actype', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("MONTH(CONVERT(date, firstOccureDate, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, firstOccureDate, 104)) =", $year)
                // ->where("(status in ('open','reminder','monitor'))", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
                // ->where_in('functional_location', $finalData)
                // ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
                // ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('acreg', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("MONTH(CONVERT(date, firstOccureDate, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, firstOccureDate, 104)) =", $year)
                // ->where("(status in ('open','reminder','monitor'))", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
                // ->where_in('functional_location', $finalData)
                // ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
                // ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('problem', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("MONTH(CONVERT(date, firstOccureDate, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, firstOccureDate, 104)) =", $year)
                // ->where("(status in ('open','reminder','monitor'))", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
                // ->where_in('functional_location', $finalData)
                // ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
                // ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('status', $data["search"]);
                // ->where_in('TBL_REP_INDEX.acreg', $finalData)
                // ->where("MONTH(CONVERT(date, firstOccureDate, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, firstOccureDate, 104)) =", $year)
                // ->where("(status in ('open','reminder','monitor'))", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
                // ->where_in('functional_location', $finalData)
                // ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
                // ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        }

        $this->dev_gmf_prod->select('*');
        $this->dev_gmf_prod->from('TBL_REP_INDEX');
        $this->dev_gmf_prod->where_in('TBL_REP_INDEX.acreg', $finalData);
        $this->dev_gmf_prod->where("MONTH(CONVERT(date, firstOccureDate, 104)) =", $month);
        $this->dev_gmf_prod->where("YEAR(CONVERT(date, firstOccureDate, 104)) =", $year);
        $this->dev_gmf_prod->join('db_dss.dbo.m_acreg', 'TBL_REP_INDEX.acreg = db_dss.dbo.m_acreg.acreg', 'left');
        $this->dev_gmf_prod->join('db_dss.dbo.m_actype', 'db_dss.dbo.m_actype.actype_id = db_dss.dbo.m_acreg.actype_id', 'left');
        $this->dev_gmf_prod->where("(status in ('open','reminder','monitor'))", NULL, FALSE);
        $this->dev_gmf_prod->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        // $this->dev_gmf->select('*');
        // $this->dev_gmf->from('repetitive_d5_final');
        // $this->dev_gmf->where_in('functional_location', $finalData);
        // $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) =", $month);
        // $this->dev_gmf->where("YEAR(CONVERT(date, created_on, 104)) =", $year);
        // $this->dev_gmf->join('m_acreg', 'repetitive_d5_final.functional_location = m_acreg.acreg', 'left');
        // $this->dev_gmf->join('m_actype', 'm_actype.actype_id = m_acreg.actype_id', 'left');
        // $this->dev_gmf->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE);
        // $this->dev_gmf->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        $this->dev_gmf_prod->limit($data["length"], $data["start"]);
        $this->dev_gmf_prod->order_by($data["order_column"], $data["order_dir"]);
        if ($data["search"] != "") {
            $this->dev_gmf_prod->like('actype', $data["search"]);
                // $this->dev_gmf_prod->where_in('TBL_REP_INDEX.acreg', $finalData);
                // $this->dev_gmf_prod->where("MONTH(CONVERT(date, firstOccureDate, 104)) =", $month);
                // $this->dev_gmf_prod->where("YEAR(CONVERT(date, firstOccureDate, 104)) =", $year);
                // $this->dev_gmf_prod->where("(status in ('open','reminder','monitor'))", NULL, FALSE);
                // $this->dev_gmf_prod->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
                // ->where_in('functional_location', $finalData)
                // ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
                // ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('acreg', $data["search"]);
                // $this->dev_gmf_prod->where_in('TBL_REP_INDEX.acreg', $finalData);
                // $this->dev_gmf_prod->where("MONTH(CONVERT(date, firstOccureDate, 104)) =", $month);
                // $this->dev_gmf_prod->where("YEAR(CONVERT(date, firstOccureDate, 104)) =", $year);
                // $this->dev_gmf_prod->where("(status in ('open','reminder','monitor'))", NULL, FALSE);
                // $this->dev_gmf_prod->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
                // ->where_in('functional_location', $finalData)
                // ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
                // ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('problem', $data["search"]);
                // $this->dev_gmf_prod->where_in('TBL_REP_INDEX.acreg', $finalData);
                // $this->dev_gmf_prod->where("MONTH(CONVERT(date, firstOccureDate, 104)) =", $month);
                // $this->dev_gmf_prod->where("YEAR(CONVERT(date, firstOccureDate, 104)) =", $year);
                // $this->dev_gmf_prod->where("(status in ('open','reminder','monitor'))", NULL, FALSE);
                // $this->dev_gmf_prod->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
                // ->where_in('functional_location', $finalData)
                // ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
                // ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
            $this->dev_gmf_prod->or_like('status', $data["search"]);
              // ->where_in('TBL_REP_INDEX.acreg', $finalData)
              // ->where("MONTH(CONVERT(date, firstOccureDate, 104)) =", $month)
              // ->where("YEAR(CONVERT(date, firstOccureDate, 104)) =", $year)
              // ->where("(status in ('open','reminder','monitor'))", NULL, FALSE);
                // $this->dev_gmf_prod->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
                // ->where_in('functional_location', $finalData)
                // ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
                // ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
                // ->where("(system_status LIKE '%OSNO" . "%' OR system_status LIKE '%NOPR%')", NULL, FALSE)
                // ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
            $query = $this->dev_gmf_prod->get();
            $output["recordsTotal"] = $output["recordsFiltered"] = $query->num_rows();
        } else {
            $query = $this->dev_gmf_prod->get();
        }
        $tblHaveMitigations = $this->dev_gmf->select('mitigasi_fk_id')
            ->from('tbl_mitigasi')
            ->where('mitigasi_type', 'repetitive')
            ->get()->result_array();
        $convertArrays = new RecursiveIteratorIterator(new RecursiveArrayIterator($tblHaveMitigations));
        $tblHaveMitigation = array();
        foreach ($convertArrays as $convertArray) {
            array_push($tblHaveMitigation, $convertArray);
        }
        $nomor_urut = $data['start'] + 1;

        foreach ($query->result_array() as $data) {
            if (in_array((int)$data['notif'], $tblHaveMitigation)) {
                $haveMitigation = 1;
            } else {
                $haveMitigation = 0;
            }

            if (strpos($data['status'], 'open') !== false or strpos($data['status'], 'reminder') !== false or strpos($data['status'], 'monitor') !== false) {
                $data['status'] = 'OPEN';
            }

            $output['data'][] = array(
                $nomor_urut,
                $data['notif'],
                $data['actype'],
                $data['acreg'],
                $data['problem'],
                date('d-m-Y', strtotime($data['firstOccureDate'])),
                date('d-m-Y', strtotime($data['lastOccureDate'])),
                $data['status'],
                $haveMitigation
            );
            $nomor_urut++;
        }

        echo json_encode($output, JSON_PRETTY_PRINT);
    }

    public function getMitigation($id)
    {
        $this->dev_gmf->select('*');
        $this->dev_gmf->from('tbl_mitigasi');
        $this->dev_gmf->where('mitigasi_fk_id', $id);
        $this->dev_gmf->where('mitigasi_type', 'repetitive');
        $query = $this->dev_gmf->get();
        return $query->result_array();
    }

    public function getNameMonth($input)
    {
        if ($input['startPeriode'] == '' and $input['endPeriode'] == '') {
            $year = date('Y');
            $monthFrom = $year . '-01';
            $monthTo = date('Y-m');
        } else {
            $monthFrom = $input['startPeriode'];
            $monthTo = $input['endPeriode'];
        }

        $start = (new DateTime($monthFrom))->modify('first day of this month');
        $end = (new DateTime($monthTo))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $monthNames = array();
        foreach ($period as $dt) {
            $monthName = $dt->format('M');

            array_push($monthNames, $monthName);
        }
        return $monthNames;
    }
}
