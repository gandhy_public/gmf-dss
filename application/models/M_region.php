<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  /**
   *
   */
  class M_region extends CI_Model
  {
    private $table_name;
    function __construct()
    {
      parent::__construct();
      $this->load->database();
      $this->table_name =  "mro_region";
      $this->dev_gmf=$this->load->database('dev_gmf',true);
    }

    public function get_all(){
        $query = $this->db->get($this->table_name);
        return $query->result_array();
    }

    public function get_region_all(){
       $result=$this->dev_gmf->query("SELECT distinct(region) FROM [dbo].[mro] ORDER BY region ASC");
      //  $this->dev_gmf->from('mro');

       return $result->result_array();
    }

    public function get_engine_all(){
       $result=$this->dev_gmf->query("SELECT DISTINCT(engine_family) FROM [dbo].[mro] ORDER BY engine_family ASC");
      //  $this->dev_gmf->from('mro');

       return $result->result_array();
    }

    public function get_operator_all(){
       $result=$this->dev_gmf->query("SELECT DISTINCT(operator) FROM [dbo].[mro] ORDER BY operator ASC");
       return $result->result_array();
    }

    public function get_aircraft_all(){
       $result=$this->dev_gmf->query("SELECT distinct(aircraft_type) as aircraft FROM [dbo].[mro] ORDER BY aircraft_type ASC");
       return $result->result_array();
    }
  }
