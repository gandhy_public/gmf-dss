<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_cabin_performance extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->db_chi = $this->load->database('db_chi', TRUE);
    }

//================================================COMBOBOX================================================\\

    function combo_aircraft_type()
    {
        $query = $this->db_chi->query("SELECT m_aircraft_type.id,  m_aircraft_type.name_aircraft FROM m_aircraft_type");
        return $query->result();
    }

    function combo_aircraft_reg($id_aircraft_type)
    {
        if (strlen($id_aircraft_type) == 0) {
            $id_aircraft_type = "00";
        } else if (strlen($id_aircraft_type) == 1) {
            $id_aircraft_type = $id_aircraft_type;
        } else {
            $id_aircraft_type = preg_replace('/-/', ',', $id_aircraft_type);
        }

        $query = $this->db_chi->query("SELECT
                                        m_aircraft_type.id,
                                        m_aircraft_reg.id,
                                        m_aircraft_reg.name_ac_reg
                                      FROM
                                        m_aircraft_reg
                                      LEFT JOIN m_aircraft_type ON m_aircraft_reg.id_aircraft_type_fk=m_aircraft_type.id
                                      WHERE
                                        m_aircraft_type.id IN ($id_aircraft_type)
                                      ORDER BY
                                        m_aircraft_reg.name_ac_reg ASC");
        return $query->result();
    }

//================================================AKUMULASI================================================\\

    function akumulasi_target($thn)
    {
        $this->load->database();
        $query = $this->db->query("SELECT target_val FROM tbl_all_target WHERE target_type='Cabin' AND target_year='$thn'");
        return $query->result_array();
    }

    function akumulasi_interior_performance($tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                    m_aircraft_manufacture.id,
                                    m_aircraft_manufacture.name_manf,
                                    SUM (
                                      trans_interior_new.total_item
                                    ) AS total_item,
                                    SUM (
                                      trans_interior_new.total_defect
                                    ) AS total_defect,
                                    SUM (
                                      trans_interior_new.total_dirty
                                    ) AS total_dirty
                                  FROM
                                    in_items
                                  LEFT JOIN in_items_sub ON in_items_sub.ini_id = in_items.ini_id
                                  LEFT JOIN trans_interior_new ON trans_interior_new.inis_id = in_items_sub.inis_id
                                  LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_interior_new.acr_id
                                  LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                  LEFT JOIN m_aircraft_manufacture ON m_aircraft_manufacture.id = m_aircraft_reg.id_aircraft_manufacture_fk
                                  WHERE
                                    trans_interior_new.modified_date BETWEEN '$tgl1'
                                  AND '$tgl2'
                                  GROUP BY
                                    m_aircraft_manufacture.id,
                                    m_aircraft_manufacture.name_manf");
        return $query->result_array();
    }

    function akumulasi_exterior_performance($tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                      m_aircraft_manufacture.id,
                                      m_aircraft_manufacture.name_manf,
                                      COUNT (trans_exterior.exis_id) AS jumlah_trans_exterior,
                                      SUM (
                                        CASE trans_exterior.te_value
                                        WHEN '4' THEN
                                          100
                                        WHEN '3' THEN
                                          98
                                        WHEN '2' THEN
                                          85
                                        WHEN '1' THEN
                                          75
                                        END
                                      ) AS result_value
                                    FROM
                                      ex_items
                                    LEFT JOIN ex_targets ON ex_targets.exi_id = ex_items.exi_id
                                    LEFT JOIN ex_items_sub ON ex_items_sub.exi_id = ex_items.exi_id
                                    LEFT JOIN trans_exterior ON trans_exterior.exis_id = ex_items_sub.exis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_exterior.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    LEFT JOIN m_aircraft_manufacture ON m_aircraft_manufacture.id = m_aircraft_reg.id_aircraft_manufacture_fk
                                    WHERE
                                      trans_exterior.te_date BETWEEN '$tgl1'
                                    AND '$tgl2'
                                    GROUP BY
                                      m_aircraft_manufacture.id,
                                      m_aircraft_manufacture.name_manf");
        return $query->result_array();
    }

    function akumulasi_functionality_performance($tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                    m_aircraft_manufacture.id,
                                    m_aircraft_manufacture.name_manf,
                                    fun_items.fi_id AS id_item,
                                    fun_items.fi_name AS nama_item,
                                    trans_functionality.defects AS total_defects_item_sub,
                                    trans_functionality.items AS total_item_sub
                                  FROM
                                    fun_items
                                  LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                  LEFT JOIN trans_functionality ON trans_functionality.fis_id = fun_items_sub.fis_id
                                  LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_functionality.acr_id
                                  LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                  LEFT JOIN m_aircraft_manufacture ON m_aircraft_manufacture.id = m_aircraft_reg.id_aircraft_manufacture_fk
                                  WHERE
                                    trans_functionality.modified_date BETWEEN '$tgl1'
                                  AND '$tgl2'
                                  ORDER BY
                                    fun_items.fi_name");
        return $query->result_array();
    }


//================================================INTERIOR================================================\\

    function data_interior_target($thn)
    {
        $query = $this->db_chi->query("SELECT
                                      AVG (in_targets. VALUE) AS target
                                    FROM
                                      in_targets
                                    WHERE
                                      in_targets. YEAR = '$thn'");
        return $query->result_array();

    }

    function data_interior_performance($id_aircraft_type, $id_aircraft_reg, $tgl1, $tgl2)
    {

        if ($id_aircraft_type == 00) {
            $id_aircraft_type = "";
        } else {
            $id_aircraft_type = preg_replace('/-/', ',', $id_aircraft_type);
            $id_aircraft_type = "AND m_aircraft_type.id IN ($id_aircraft_type)";
        }
        if ($id_aircraft_reg == 00) {
            $id_aircraft_reg = "";
        } else {
            $id_aircraft_reg = preg_replace('/-/', ',', $id_aircraft_reg);
            $id_aircraft_reg = "AND m_aircraft_reg.id IN ($id_aircraft_reg)";
        }


        $query = $this->db_chi->query("SELECT
                                      m_aircraft_type.id AS id_aircraft,
                                      m_aircraft_type.name_aircraft AS tipe_aircraft,
                                      SUM (trans_interior_new.total_item) AS total_item,
                                      SUM (trans_interior_new.total_defect) AS total_defect,
                                      SUM(trans_interior_new.total_dirty) AS total_dirty
                                    FROM
                                      in_items
                                    LEFT JOIN in_items_sub ON in_items_sub.ini_id = in_items.ini_id
                                    LEFT JOIN trans_interior_new ON trans_interior_new.inis_id = in_items_sub.inis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_interior_new.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                      trans_interior_new.modified_date BETWEEN ' $tgl1'
                                    AND '$tgl2'
                                    $id_aircraft_type
                                    $id_aircraft_reg
                                    GROUP BY
                                      m_aircraft_type.id,
                                      m_aircraft_type.name_aircraft
                                    ORDER BY
                                      m_aircraft_type.name_aircraft ASC");
        return $query->result_array();
    }


    function reg_interior($id_aircraft_type, $id_aircraft_reg)
    {

        if ($id_aircraft_reg == "00") {
            $id_aircraft_reg = "";
        } else if (strlen($id_aircraft_reg) == 1) {
            $id_aircraft_reg = "AND m_aircraft_reg.id='$id_aircraft_reg'";
        } else {
            $id_aircraft_reg = preg_replace('/-/', ',', $id_aircraft_reg);
            $id_aircraft_reg = "AND m_aircraft_reg.id IN ($id_aircraft_reg)";
        }


        $query = $this->db_chi->query("SELECT
                                      m_aircraft_type.id AS id_aircraft_type,
                                      m_aircraft_reg.id,
                                      m_aircraft_reg.name_ac_reg
                                    FROM
                                        in_items
                                    LEFT JOIN in_targets ON in_items.ini_id = in_items.ini_id
                                    LEFT JOIN in_items_sub ON in_items_sub.ini_id = in_items.ini_id
                                    LEFT JOIN trans_interior_new ON trans_interior_new.inis_id = in_items_sub.inis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_interior_new.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                    m_aircraft_type.name_aircraft = '$id_aircraft_type'
                                    $id_aircraft_reg
                                    GROUP BY
                                      m_aircraft_type.id,
                                      m_aircraft_reg.id,
                                      m_aircraft_reg.name_ac_reg
                                     ORDER BY
                                     m_aircraft_reg.name_ac_reg ASC");
        return $query->result_array();
    }

    function aspek_interior_target($thn)
    {
        $query = $this->db_chi->query("SELECT
                                      in_items.ini_id AS id_item,
                                      in_items.ini_name AS nama_item,
                                      in_targets.
                                    VALUE
                                      AS target
                                    FROM
                                      in_items
                                    LEFT JOIN in_targets ON in_targets.ini_id = in_items.ini_id
                                    LEFT JOIN in_items_sub ON in_items_sub.inis_id = in_items.ini_id
                                    WHERE
                                      in_targets. YEAR = '$thn'
                                    GROUP BY
                                      in_items.ini_id,
                                      in_items.ini_name,
                                      in_targets.
                                    VALUE");
        return $query->result_array();
    }

    function master_interior()
    {
        $query = $this->db_chi->query("SELECT
                                      in_items.ini_id AS id_item,
                                      in_items.ini_name AS nama_item
                                    FROM
                                      in_items
                                    LEFT JOIN in_targets ON in_targets.ini_id = in_items.ini_id
                                    LEFT JOIN in_items_sub ON in_items_sub.inis_id = in_items.ini_id
                                    GROUP BY
                                      in_items.ini_id,
                                      in_items.ini_name");
        return $query->result_array();
    }

    function aspek_interior_performance($id_aircraft_reg, $tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                      m_aircraft_reg.id AS id_reg,
                                      in_items.ini_id AS id_item,
                                      m_aircraft_type.name_aircraft,
                                      m_aircraft_reg.name_ac_reg,
                                      in_items_sub.inis_name AS nama_item_sub,
                                      trans_interior_new.total_defect AS total_defect,
                                      trans_interior_new.total_dirty AS total_dirty,
                                      trans_interior_new.total_item AS total_item
                                    FROM
                                      in_items
                                    LEFT JOIN in_items_sub ON in_items_sub.ini_id = in_items.ini_id
                                    LEFT JOIN trans_interior_new ON trans_interior_new.inis_id = in_items_sub.inis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_interior_new.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                      m_aircraft_reg.id = '$id_aircraft_reg'
                                    AND trans_interior_new.modified_date BETWEEN '$tgl1'
                                    AND '$tgl2'");
        return $query->result_array();
    }

    function jumlah_interior($id_aircraft_reg, $tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                        COUNT(in_items.ini_id) AS jumlah_item
                                      FROM
                                        in_items
                                      LEFT JOIN in_items_sub ON in_items_sub.ini_id = in_items.ini_id
                                      LEFT JOIN trans_interior_new ON trans_interior_new.inis_id = in_items_sub.inis_id
                                      LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_interior_new.acr_id
                                      LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                      WHERE
                                        m_aircraft_reg.id = '$id_aircraft_reg'
                                      AND trans_interior_new.modified_date BETWEEN '$tgl1'
                                      AND '$tgl2'");
        return $query->result_array();
    }

    function detail_sub_interior($id_reg, $id_item, $tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                      m_aircraft_type.name_aircraft,
                                      m_aircraft_reg.name_ac_reg,
                                      in_items_sub.inis_name AS nama_item_sub,
                                      trans_interior_new.remark,
                                      trans_interior_new.total_defect AS total_defect,
                                      trans_interior_new.total_dirty AS total_dirty,
                                      trans_interior_new.total_item AS total_item
                                    FROM
                                      in_items
                                    LEFT JOIN in_items_sub ON in_items_sub.ini_id = in_items.ini_id
                                    LEFT JOIN trans_interior_new ON trans_interior_new.inis_id = in_items_sub.inis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_interior_new.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                      m_aircraft_reg.id = '$id_reg'
                                    AND in_items.ini_id = '$id_item'
                                    AND trans_interior_new.modified_date BETWEEN '$tgl1'
                                    AND '$tgl2'");
        return $query->result_array();
    }

    function mitigasi_interior($id_reg, $item, $type_mitigasi, $tgl1, $tgl2)
    {
        $this->load->database();
        $query = $this->db->query("SELECT cabin_mitigation_why, cabin_mitigation_solution
                                  FROM cabin_mitigation
                                  WHERE
                                  acreg='$id_reg'
                                  AND item='$item'
                                  AND type_mitigasi='$type_mitigasi'
                                  AND cabin_mitigation_date
                                  BETWEEN '$tgl1' AND '$tgl2'");
        return $query->result_array();
    }

    function reg()
    {
        $query = $this->db_chi->query("SELECT
                                  m_aircraft_reg.id AS id_reg
                                FROM
                                  in_items
                                LEFT JOIN in_targets ON in_items.ini_id = in_items.ini_id
                                LEFT JOIN in_items_sub ON in_items_sub.ini_id = in_items.ini_id
                                LEFT JOIN trans_interior_new ON trans_interior_new.inis_id = in_items_sub.inis_id
                                LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_interior_new.acr_id
                                LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                GROUP BY
                                m_aircraft_reg.id
                                ORDER BY
                                m_aircraft_reg.id ASC");
        return $query->result_array();
    }


//================================================EXTERIOR================================================\\

    function data_exterior_target($thn)
    {
        $query = $this->db_chi->query("SELECT
                                        AVG (ex_targets. VALUE) AS target
                                      FROM
                                        ex_targets
                                      WHERE
                                        ex_targets.year= '$thn'");
        return $query->result_array();
    }

    function data_exterior_performance($id_aircraft_type, $id_aircraft_reg, $tgl1, $tgl2)
    {
        if ($id_aircraft_type == 00) {
            $id_aircraft_type = "";
        } else {
            $id_aircraft_type = preg_replace('/-/', ',', $id_aircraft_type);
            $id_aircraft_type = "AND m_aircraft_type.id IN ($id_aircraft_type)";
        }
        if ($id_aircraft_reg == 00) {
            $id_aircraft_reg = "";
        } else {
            $id_aircraft_reg = preg_replace('/-/', ',', $id_aircraft_reg);
            $id_aircraft_reg = "AND m_aircraft_reg.id IN ($id_aircraft_reg)";
        }
        $query = $this->db_chi->query("SELECT
                                      m_aircraft_type.id AS id_aircraft,
                                      m_aircraft_type.name_aircraft AS tipe_aircraft,
                                      COUNT (trans_exterior.exis_id) AS jumlah_trans_exterior,
                                      SUM (
                                        CASE trans_exterior.te_value
                                        WHEN '4' THEN
                                          100
                                        WHEN '3' THEN
                                          98
                                        WHEN '2' THEN
                                          85
                                        WHEN '1' THEN
                                          75
                                        END
                                      ) AS result_value
                                    FROM
                                      ex_items
                                    LEFT JOIN ex_targets ON ex_targets.exi_id = ex_items.exi_id
                                    LEFT JOIN ex_items_sub ON ex_items_sub.exi_id = ex_items.exi_id
                                    LEFT JOIN trans_exterior ON trans_exterior.exis_id = ex_items_sub.exis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_exterior.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                      trans_exterior.te_date BETWEEN '$tgl1'
                                    AND '$tgl2'
                                    $id_aircraft_type
                                    $id_aircraft_reg
                                    GROUP BY
                                      m_aircraft_type.id,
                                      m_aircraft_type.name_aircraft
                                    ORDER BY
                                      m_aircraft_type.name_aircraft ASC");
        return $query->result_array();
    }


    function reg_exterior($id_aircraft_type, $id_aircraft_reg)
    {

        if ($id_aircraft_reg == "00") {
            $id_aircraft_reg = "";
        } else if (strlen($id_aircraft_reg) == 1) {
            $id_aircraft_reg = "AND m_aircraft_reg.id='$id_aircraft_reg'";
        } else {
            $id_aircraft_reg = preg_replace('/-/', ',', $id_aircraft_reg);
            $id_aircraft_reg = "AND m_aircraft_reg.id IN ($id_aircraft_reg)";
        }

        $query = $this->db_chi->query("SELECT
                                      m_aircraft_type.id AS id_aircraft_type,
                                      m_aircraft_reg.id,
                                      m_aircraft_reg.name_ac_reg
                                    FROM
                                      m_aircraft_reg
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                      m_aircraft_type.name_aircraft = '$id_aircraft_type'
                                      $id_aircraft_reg
                                    GROUP BY
                                      m_aircraft_type.id,
                                      m_aircraft_reg.id,
                                      m_aircraft_reg.name_ac_reg
                                    ORDER BY
                                      m_aircraft_reg.name_ac_reg ASC");
        return $query->result_array();
    }

    function aspek_exterior_target($thn)
    {
        $query = $this->db_chi->query("SELECT
                                      ex_items.exi_id AS id_item,
                                      ex_items.exi_name AS nama_item,
                                      ex_targets.VALUE AS target
                                    FROM
                                      ex_items
                                    LEFT JOIN ex_targets ON ex_targets.exi_id = ex_items.exi_id
                                    LEFT JOIN ex_items_sub ON ex_items_sub.exi_id = ex_items.exi_id
                                    WHERE
                                      ex_targets. YEAR = '$thn'
                                    GROUP BY
                                      ex_items.exi_id,
                                      ex_items.exi_name,
                                      ex_targets.VALUE");
        return $query->result_array();
    }

    function master_exterior()
    {
        $query = $this->db_chi->query("SELECT
                                      ex_items.exi_id AS id_item,
                                      ex_items.exi_name AS nama_item
                                    FROM
                                      ex_items
                                    LEFT JOIN ex_targets ON ex_targets.exi_id = ex_items.exi_id
                                    LEFT JOIN ex_items_sub ON ex_items_sub.exi_id = ex_items.exi_id
                                    GROUP BY
                                      ex_items.exi_id,
                                      ex_items.exi_name");
        return $query->result_array();
    }

    function aspek_exterior_performance($id_aircraft_reg, $tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                      ex_items.exi_id as id_item,
                                      m_aircraft_type.id AS id_aircraft,
                                      m_aircraft_type.name_aircraft AS tipe_aircraft,
                                      COUNT (trans_exterior.exis_id) AS jumlah_trans_exterior,
                                      SUM (
                                        CASE trans_exterior.te_value
                                        WHEN '4' THEN
                                          100
                                        WHEN '3' THEN
                                          98
                                        WHEN '2' THEN
                                          85
                                        WHEN '1' THEN
                                          75
                                        END
                                      ) AS result_value
                                    FROM
                                      ex_items
                                    LEFT JOIN ex_targets ON ex_targets.exi_id = ex_items.exi_id
                                    LEFT JOIN ex_items_sub ON ex_items_sub.exi_id = ex_items.exi_id
                                    LEFT JOIN trans_exterior ON trans_exterior.exis_id = ex_items_sub.exis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_exterior.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                      trans_exterior.te_date BETWEEN '$tgl1'
                                    AND '$tgl2'
                                    AND m_aircraft_reg.id='$id_aircraft_reg'
                                    GROUP BY
                                       ex_items.exi_id,
                                      m_aircraft_type.id,
                                      m_aircraft_type.name_aircraft
                                    ORDER BY
                                      m_aircraft_type.name_aircraft ASC");
        return $query->result_array();
    }

    function detail_sub_exterior($id_reg, $id_item, $tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                      ex_items_sub.exis_name AS nm_sub_item,
                                      trans_exterior.te_value AS value
                                    FROM
                                      ex_items_sub
                                    LEFT JOIN ex_items ON ex_items.exi_id = ex_items_sub.exi_id
                                    LEFT JOIN trans_exterior ON trans_exterior.exis_id = ex_items_sub.exis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_exterior.acr_id
                                    WHERE
                                    trans_exterior.te_date BETWEEN '$tgl1'
                                    AND '$tgl2'
                                    AND m_aircraft_reg.id='$id_reg'
                                    AND ex_items.exi_id ='$id_item'
                                    GROUP BY
                                      ex_items_sub.exis_name,
                                      trans_exterior.te_value");
        return $query->result_array();
    }

    function mitigasi_exterior($id_reg, $item, $type_mitigasi, $tgl1, $tgl2)
    {
        $this->load->database();
        $query = $this->db->query("SELECT cabin_mitigation_why, cabin_mitigation_solution
                                  FROM cabin_mitigation
                                  WHERE
                                  acreg='$id_reg'
                                  AND item='$item'
                                  AND type_mitigasi='$type_mitigasi'
                                  AND cabin_mitigation_date
                                  BETWEEN '$tgl1' AND '$tgl2'");
        return $query->result_array();
    }


//================================================FUNCTIONALITY================================================\\

    function data_functional_target_by_reg($thn)
    {
        $query = $this->db_chi->query("SELECT
                                      AVG (fun_targets. VALUE) AS target
                                    FROM
                                      fun_targets
                                    WHERE
                                      fun_targets.year = '$thn'");
        return $query->result_array();

    }

    function data_functional_performance_by_reg($id_aircraft_type, $id_aircraft_reg, $tgl1, $tgl2)
    {

        if ($id_aircraft_type == 00) {
            $id_aircraft_type = "";
        } else {
            $id_aircraft_type = preg_replace('/-/', ',', $id_aircraft_type);
            $id_aircraft_type = "AND m_aircraft_type.id IN ($id_aircraft_type)";
        }
        if ($id_aircraft_reg == 00) {
            $id_aircraft_reg = "";
        } else {
            $id_aircraft_reg = preg_replace('/-/', ',', $id_aircraft_reg);
            $id_aircraft_reg = "AND m_aircraft_reg.id IN ($id_aircraft_reg)";
        }

        $query = $this->db_chi->query("SELECT
                                        m_aircraft_type.id AS id_aircraft,
                                        m_aircraft_type.name_aircraft AS tipe_aircraft,
                                        SUM (trans_functionality.items) AS total_item,
                                        SUM (trans_functionality.defects) AS total_defect
                                      FROM
                                        fun_items
                                      LEFT JOIN fun_targets ON fun_targets.fi_id = fun_items.fi_id
                                      LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                      LEFT JOIN trans_functionality ON trans_functionality.fis_id = fun_items_sub.fis_id
                                      LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_functionality.acr_id
                                      LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                      WHERE
                                        trans_functionality.modified_date BETWEEN '$tgl1' AND '$tgl2'
                                        $id_aircraft_type
                                        $id_aircraft_reg
                                      GROUP BY
                                        m_aircraft_type.id,
                                        m_aircraft_type.name_aircraft
                                      ORDER BY
                                      m_aircraft_type.name_aircraft ASC");
        return $query->result_array();
    }

    function data_functional_target_by_item($thn)
    {
        $query = $this->db_chi->query("SELECT
                                      fun_items.fi_id AS id_item,
                                      fun_items.fi_name AS nama_item,
                                      fun_targets.
                                    VALUE
                                      AS target
                                    FROM
                                      fun_items
                                    LEFT JOIN fun_targets ON fun_targets.fi_id = fun_items.fi_id
                                    LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                    WHERE
                                      fun_targets. YEAR = '$thn'
                                    GROUP BY
                                      fun_items.fi_id,
                                      fun_items.fi_name,
                                      fun_targets.
                                    VALUE");
        return $query->result_array();

    }

    function data_functional_performance_by_item($tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                      fun_items.fi_id AS id_item,
                                      fun_items.fi_name AS nama_item,
                                      trans_functionality.defects AS total_defects_item_sub,
                                      trans_functionality.items AS total_item_sub
                                    FROM
                                      fun_items
                                    LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                    LEFT JOIN trans_functionality ON trans_functionality.fis_id = fun_items_sub.fis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_functionality.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                      trans_functionality.modified_date BETWEEN '$tgl1'
                                    AND '$tgl2'
                                    ORDER BY
                                      fun_items.fi_name");
        return $query->result_array();
    }

    function count_performance_by_item($tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                    fun_items.fi_id AS id_item,
                                    fun_items.fi_name AS nama_item,
                                    COUNT(fun_items.fi_id) AS count
                                  FROM
                                    fun_items
                                  LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                  LEFT JOIN trans_functionality ON trans_functionality.fis_id = fun_items_sub.fis_id
                                  LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_functionality.acr_id
                                  LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                  WHERE
                                    trans_functionality.modified_date BETWEEN '$tgl1'
                                  AND ' $tgl2'
                                  GROUP BY
                                  fun_items.fi_id,
                                  fun_items.fi_name");
        return $query->result_array();
    }

    function reg_functional($id_aircraft_type, $id_aircraft_reg)
    {

        if ($id_aircraft_reg == "00") {
            $id_aircraft_reg = "";
        } else if (strlen($id_aircraft_reg) == 1) {
            $id_aircraft_reg = "AND m_aircraft_reg.id='$id_aircraft_reg'";
        } else {
            $id_aircraft_reg = preg_replace('/-/', ',', $id_aircraft_reg);
            $id_aircraft_reg = "AND m_aircraft_reg.id IN ($id_aircraft_reg)";
        }

        $query = $this->db_chi->query("SELECT
                                      m_aircraft_type.id AS id_aircraft_type,
                                      m_aircraft_reg.id,
                                      m_aircraft_reg.name_ac_reg
                                    FROM
                                      m_aircraft_reg
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                      m_aircraft_type.name_aircraft = '$id_aircraft_type'
                                      $id_aircraft_reg
                                    GROUP BY
                                      m_aircraft_type.id,
                                      m_aircraft_reg.id,
                                      m_aircraft_reg.name_ac_reg");
        return $query->result_array();
    }

    function aspek_functional_target($thn)
    {
        $query = $this->db_chi->query("SELECT
                                        fun_items.fi_id AS id_item,
                                        fun_items.fi_name AS nama_item,
                                        fun_targets.
                                      VALUE
                                        AS target
                                      FROM
                                        fun_items
                                      LEFT JOIN fun_targets ON fun_targets.fi_id = fun_items.fi_id
                                      LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                      WHERE
                                        fun_targets. YEAR = '$thn'
                                      GROUP BY
                                        fun_items.fi_id,
                                        fun_items.fi_name,
                                        fun_targets.
                                      VALUE");
        return $query->result_array();
    }

    function master_functional()
    {
        $query = $this->db_chi->query("SELECT
                                        fun_items.fi_id AS id_item,
                                        fun_items.fi_name AS nama_item
                                      FROM
                                        fun_items
                                      LEFT JOIN fun_targets ON fun_targets.fi_id = fun_items.fi_id
                                      LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                      GROUP BY
                                        fun_items.fi_id,
                                        fun_items.fi_name
                                      ");
        return $query->result_array();
    }

    function aspek_functional_performance($id_aircraft_type, $tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                      fun_items.fi_id AS id_item,
                                      fun_items.fi_name AS nama_item,
                                      SUM(trans_functionality.defects) AS total_defect,
                                      SUM(trans_functionality.items) AS total_item
                                    FROM
                                      fun_items
                                    LEFT JOIN fun_items_sub ON  fun_items_sub.fis_id = fun_items.fi_id
                                    LEFT JOIN trans_functionality ON trans_functionality.fis_id = fun_items_sub.fis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_functionality.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                      m_aircraft_type.name_aircraft = '$id_aircraft_type'
                                    AND trans_functionality.modified_date BETWEEN '$tgl1'
                                    AND '$tgl2'
                                    GROUP BY
                                      fun_items.fi_id,
                                      fun_items.fi_name");
        return $query->result_array();
    }

    function detail_sub_functional_reg($id_type, $id_item, $tgl1, $tgl2)
    {
        $query = $this->db_chi->query("SELECT
                                        m_aircraft_type.name_aircraft,
                                        m_aircraft_reg.name_ac_reg,
                                        fun_items_sub.fis_name as nama_item_sub,
                                        trans_functionality.remark as remark_item_sub,
                                        trans_functionality.defects as total_defects_item_sub,
                                        trans_functionality.items as total_item_sub
                                      FROM
                                        fun_items
                                      LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                      LEFT JOIN trans_functionality ON trans_functionality.fis_id = fun_items_sub.fis_id
                                      LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_functionality.acr_id
                                      LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                      WHERE
                                      m_aircraft_type.name_aircraft = '$id_type'
                                      AND fun_items.fi_id ='$id_item'
                                      AND trans_functionality.modified_date BETWEEN '$tgl1'
                                      AND '$tgl2'");
        return $query->result_array();
    }

    function detail_sub_functional_by_item($id_item, $tgl1, $tgl2)
    {
        $item = str_replace('%20', ' ', $id_item);
        $query = $this->db_chi->query("SELECT
                                      fun_items_sub.fis_name AS nama_item_sub,
                                      trans_functionality.remark AS remark_item_sub,
                                      trans_functionality.defects AS total_defects_item_sub,
                                      trans_functionality.items AS total_item_sub
                                    FROM
                                      fun_items
                                    LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                    LEFT JOIN trans_functionality ON trans_functionality.fis_id = fun_items_sub.fis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_functionality.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                    fun_items.fi_name = '$item'
                                    AND trans_functionality.modified_date BETWEEN '$tgl1'
                                    AND '$tgl2'");
        return $query->result_array();
    }

    function mitigasi_functional_by_item($id_item, $type_mitigasi, $tgl1, $tgl2)
    {
        $this->load->database();
        $item = str_replace('%20', ' ', $id_item);
        $query = $this->db->query("SELECT cabin_mitigation_why, cabin_mitigation_solution
                                  FROM cabin_mitigation
                                  WHERE
                                  nm_item='$item'
                                  AND type_mitigasi='$type_mitigasi'
                                  AND cabin_mitigation_date
                                  BETWEEN '$tgl1' AND '$tgl2'");
        return $query->result_array();
    }

    function mitigasi_functional_by_reg($id_type, $id_item, $tgl1, $tgl2)
    {
        $this->load->database();
        $query = $this->db->query("SELECT cabin_mitigation_why, cabin_mitigation_solution
                                  FROM cabin_mitigation
                                  WHERE
                                  actype='$id_type'
                                  AND item='$id_item'
                                  AND cabin_mitigation_date
                                  BETWEEN '$tgl1' AND '$tgl2'
                                  ");
        return $query->result_array();
    }


    //================================================MASTER TARGET================================================\\

    function update_target_cabin($where, $data)
    {
        $this->load->database();
        $this->db->update('tbl_all_target', $data, $where);
        return $this->db->affected_rows();
    }

    function show_target_cabin($thn)
    {
        $this->load->database();
        $query = $this->db->query("SELECT *
                                  FROM tbl_all_target
                                  WHERE
                                  target_type='Cabin'
                                  AND target_year='$thn'");
        return $query->result();
    }

    //================================================MITIGASI INTERIOR================================================\\

    // function master_interior_reg($id_type){
    //     $query = $this->db_chi->query("SELECT m_aircraft_type.name_aircraft, m_aircraft_reg.name_ac_reg
    //                                   FROM m_aircraft_type LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id_aircraft_type_fk=m_aircraft_type.id
    //                                   WHERE m_aircraft_type.id IN ($id_type)");
    //   return $query->result_array();
    // }

    function mitigasi_interior_target($thn)
    {
        $query = $this->db_chi->query("SELECT
                                      in_items.ini_id AS id_item,
                                      in_items.ini_name AS nama_item,
                                      in_targets.
                                    VALUE
                                      AS target
                                    FROM
                                      in_items
                                    LEFT JOIN in_targets ON in_targets.ini_id = in_items.ini_id
                                    LEFT JOIN in_items_sub ON in_items_sub.inis_id = in_items.ini_id
                                    WHERE
                                      in_targets. YEAR = '$thn'
                                    GROUP BY
                                      in_items.ini_id,
                                      in_items.ini_name,
                                      in_targets.
                                    VALUE");
        return $query->result_array();
    }

    function mitigasi_interior_perform($bln1, $bln2, $thn)
    {
        /*$query = $this->db_chi->query("SELECT
                                        m_aircraft_manufacture.id AS id_manufacture,
                                        m_aircraft_manufacture.name_manf,
                                        m_aircraft_type.id AS id_type,
                                        m_aircraft_type.name_aircraft,
                                        m_aircraft_reg.id AS id_reg,
                                        m_aircraft_reg.name_ac_reg,
                                        in_items.ini_id AS id_item,
                                        in_items.ini_name AS nama_item,
                                        -- trans_interior_new.modified_date,
                                        SUM(trans_interior_new.total_defect) AS total_defect,
                                        SUM(trans_interior_new.total_dirty) AS total_dirty,
                                        SUM(trans_interior_new.total_item) AS total_item
                                      FROM
                                        in_items
                                      LEFT JOIN in_items_sub ON in_items_sub.ini_id = in_items.ini_id
                                      LEFT JOIN trans_interior_new ON trans_interior_new.inis_id = in_items_sub.inis_id
                                      LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_interior_new.acr_id
                                      LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                      LEFT JOIN m_aircraft_manufacture ON m_aircraft_manufacture.id = m_aircraft_reg.id_aircraft_manufacture_fk
                                      WHERE
                                        trans_interior_new.modified_date BETWEEN '$tgl1'
                                      AND '$tgl2'
                                      GROUP BY
                                        m_aircraft_manufacture.id,
                                        m_aircraft_manufacture.name_manf,
                                        m_aircraft_type.id,
                                        m_aircraft_type.name_aircraft,
                                        m_aircraft_reg.id,
                                        m_aircraft_reg.name_ac_reg,
                                        in_items.ini_id,
                                        in_items.ini_name
                                        -- trans_interior_new.modified_date
                                      ORDER BY
                                        m_aircraft_manufacture.name_manf,
                                        m_aircraft_type.name_aircraft,
                                        m_aircraft_reg.name_ac_reg,
                                        in_items.ini_name"); */
        $query = $this->db_chi->query("SELECT
                                        m_aircraft_manufacture.id AS id_manufacture,
                                        m_aircraft_manufacture.name_manf,
                                        m_aircraft_type.id AS id_type,
                                        m_aircraft_type.name_aircraft,
                                        m_aircraft_reg.id AS id_reg,
                                        m_aircraft_reg.name_ac_reg,
                                        in_items.ini_id AS id_item,
                                        in_items.ini_name AS nama_item,
                                        trans_interior_new.total_defect,
                                        trans_interior_new.total_dirty,
                                        trans_interior_new.total_item,
                                        trans_interior_new.modified_date
                                      FROM
                                        in_items
                                      LEFT JOIN in_items_sub ON in_items_sub.ini_id = in_items.ini_id
                                      LEFT JOIN trans_interior_new ON trans_interior_new.inis_id = in_items_sub.inis_id
                                      LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_interior_new.acr_id
                                      LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                      LEFT JOIN m_aircraft_manufacture ON m_aircraft_manufacture.id = m_aircraft_reg.id_aircraft_manufacture_fk
                                      WHERE
                                        YEAR (
                                          trans_interior_new.modified_date
                                        ) >= '$thn'
                                      AND MONTH (
                                        trans_interior_new.modified_date
                                      ) >= '$bln1'
                                      AND YEAR (
                                        trans_interior_new.modified_date
                                      ) <= '$thn'
                                      AND MONTH (
                                        trans_interior_new.modified_date
                                      ) <= '$bln2'
                                      ORDER BY
                                        m_aircraft_manufacture.name_manf,
                                        m_aircraft_type.name_aircraft,
                                        m_aircraft_reg.name_ac_reg,
                                        in_items.ini_name");

        return $query->result_array();
    }

    function save_mitigation_in($data)
    {
        $this->load->database();
        $this->db->insert('cabin_mitigation', $data);
        return $this->db->insert_id();
    }

    function update_mitigation_in($where, $data)
    {
        $this->load->database();
        $this->db->update('cabin_mitigation', $data, $where);
        return $this->db->affected_rows();
    }

    function show_mitigation_in($tgl, $actype, $id_item)
    {
        $this->load->database();

        $query = $this->db->query("SELECT *
                                  FROM cabin_mitigation
                                  WHERE
                                  cabin_mitigation_date='$tgl'
                                  AND actype='$actype'
                                  AND item='$id_item'");
        // print_r($this->db->last_query());
        // exit();
        return $query->result();
    }

    function data_mitigation($bln1, $bln2, $thn)
    {
        $this->load->database();
        $query = $this->db->query("SELECT
                                  *
                                FROM
                                  cabin_mitigation
                                WHERE
                                  YEAR (
                                    cabin_mitigation_date
                                  ) >= '$thn'
                                AND MONTH (
                                  cabin_mitigation_date
                                ) >= '$bln1'
                                AND YEAR (
                                  cabin_mitigation_date
                                ) <= '$thn'
                                AND MONTH (
                                  cabin_mitigation_date
                                ) <= '$bln2'");
        return $query->result();
    }

    //================================================MITIGASI EXTERIOR================================================\\

    function mitigasi_exterior_target($thn)
    {
        $query = $this->db_chi->query("SELECT
                                      ex_items.exi_id AS id_item,
                                      ex_items.exi_name AS nama_item,
                                      ex_targets.
                                    VALUE
                                      AS target
                                    FROM
                                      ex_items
                                    LEFT JOIN ex_targets ON ex_targets.exi_id = ex_items.exi_id
                                    LEFT JOIN ex_items_sub ON ex_items_sub.exi_id = ex_items.exi_id
                                    WHERE
                                      ex_targets. YEAR = '$thn'
                                    GROUP BY
                                      ex_items.exi_id,
                                      ex_items.exi_name,
                                      ex_targets.
                                    VALUE");
        return $query->result_array();

    }

    function mitigasi_exterior_perform($bln1, $bln2, $thn)
    {
        $query = $this->db_chi->query("SELECT
                                      m_aircraft_manufacture.id AS id_manufacture,
                                      m_aircraft_manufacture.name_manf,
                                      m_aircraft_type.id AS id_type,
                                      m_aircraft_type.name_aircraft,
                                      m_aircraft_reg.id AS id_reg,
                                      m_aircraft_reg.name_ac_reg,
                                      ex_items.exi_id AS id_item,
                                      ex_items.exi_name AS nama_item,
                                      trans_exterior.te_date,
                                      COUNT (trans_exterior.exis_id) AS jumlah_trans_exterior,
                                      SUM (
                                        CASE trans_exterior.te_value
                                        WHEN '4' THEN
                                          100
                                        WHEN '3' THEN
                                          98
                                        WHEN '2' THEN
                                          85
                                        WHEN '1' THEN
                                          75
                                        END
                                      ) AS result_value
                                    FROM
                                      ex_items
                                    LEFT JOIN ex_items_sub ON ex_items_sub.exi_id = ex_items.exi_id
                                    LEFT JOIN trans_exterior ON trans_exterior.exis_id = ex_items_sub.exis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_exterior.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    LEFT JOIN m_aircraft_manufacture ON m_aircraft_manufacture.id = m_aircraft_reg.id_aircraft_manufacture_fk
                                    WHERE
                                      YEAR (
                                        trans_exterior.te_date
                                      ) >= '$thn'
                                    AND MONTH (
                                      trans_exterior.te_date
                                    ) >= '$bln1'
                                    AND YEAR (
                                      trans_exterior.te_date
                                    ) <= '$thn'
                                    AND MONTH (
                                      trans_exterior.te_date
                                    ) <= '$bln2'
                                    GROUP BY
                                      m_aircraft_manufacture.id,
                                      m_aircraft_manufacture.name_manf,
                                      m_aircraft_type.id,
                                      m_aircraft_type.name_aircraft,
                                      m_aircraft_reg.id,
                                      m_aircraft_reg.name_ac_reg,
                                      ex_items.exi_id,
                                      ex_items.exi_name,
                                      trans_exterior.te_date
                                    ORDER BY
                                        m_aircraft_manufacture.name_manf,
                                        m_aircraft_type.name_aircraft,
                                        m_aircraft_reg.name_ac_reg,
                                        ex_items.exi_name");
        return $query->result_array();

    }

    function save_mitigation_ext($data)
    {
        $this->load->database();
        $this->db->insert('cabin_mitigation', $data);
        return $this->db->insert_id();
    }

    function update_mitigation_ext($where, $data)
    {
        $this->load->database();
        $this->db->update('cabin_mitigation', $data, $where);
        return $this->db->affected_rows();
    }

    function show_mitigation_ext($tgl, $actype, $id_item)
    {
        $this->load->database();
        $query = $this->db->query("SELECT *
                                  FROM cabin_mitigation
                                  WHERE
                                  cabin_mitigation_date='$tgl'
                                  AND actype='$actype'
                                  AND item='$id_item'");
        return $query->result();
    }

    //=============================================MITIGASI FUNCTIONALITY================================================\\

    function mitigasi_functionality_target($thn)
    {
        $query = $this->db_chi->query("SELECT
                                        fun_items.fi_id AS id_item,
                                        fun_items.fi_name AS nama_item,
                                        fun_targets.VALUE
                                        AS target
                                      FROM
                                        fun_items
                                      LEFT JOIN fun_targets ON fun_targets.fi_id = fun_items.fi_id
                                      LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                      WHERE
                                        fun_targets. YEAR = '$thn'
                                      GROUP BY
                                        fun_items.fi_id,
                                        fun_items.fi_name,
                                        fun_targets.VALUE");
        return $query->result_array();

    }

    /*function mitigasi_functionality_perform($tgl1, $tgl2){
      $query = $this->db_chi->query("SELECT
                                      m_aircraft_manufacture.id AS id_manufacture,
                                      m_aircraft_manufacture.name_manf,
                                      m_aircraft_type.id AS id_type,
                                      m_aircraft_type.name_aircraft,
                                      m_aircraft_reg.id AS id_reg,
                                      m_aircraft_reg.name_ac_reg,
                                      fun_items.fi_id AS id_item,
                                      fun_items.fi_name AS nama_item,
                                      trans_functionality.defects AS total_defect,
                                      trans_functionality.items AS total_item,
                                      trans_functionality.modified_date
                                    FROM
                                      fun_items
                                    LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                    LEFT JOIN trans_functionality ON trans_functionality.fis_id = fun_items_sub.fis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_functionality.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    LEFT JOIN m_aircraft_manufacture ON m_aircraft_manufacture.id = m_aircraft_reg.id_aircraft_manufacture_fk
                                    WHERE
                                      trans_functionality.modified_date BETWEEN '$tgl1'
                                    AND '$tgl2'
                                    ORDER BY
                                      fun_items.fi_name");
      return $query->result_array();
    }*/

    function mitigasi_functionality_perform($bln1, $bln2, $thn)
    {
        $query = $this->db_chi->query("SELECT
                                      fun_items.fi_id AS id_item,
                                      fun_items.fi_name AS nama_item,
                                      fun_items.fi_name AS nama_item,
                                      trans_functionality.modified_date,
                                      trans_functionality.defects AS total_defects_item_sub,
                                      trans_functionality.items AS total_item_sub
                                    FROM
                                      fun_items
                                    LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                    LEFT JOIN trans_functionality ON trans_functionality.fis_id = fun_items_sub.fis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_functionality.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                      YEAR (
                                        trans_functionality.modified_date
                                      ) >= '$thn'
                                    AND MONTH (
                                      trans_functionality.modified_date
                                    ) >= '$bln1'
                                    AND YEAR (
                                      trans_functionality.modified_date
                                    ) <= '$thn'
                                    AND MONTH (
                                      trans_functionality.modified_date
                                    ) <= '$bln2'
                                    ORDER BY
                                      fun_items.fi_name");
        return $query->result_array();
    }

    function count_performance_mitigasi_functionality_by_item($bln1, $bln2, $thn)
    {
        $query = $this->db_chi->query("SELECT
                                      fun_items.fi_id AS id_item,
                                      fun_items.fi_name AS nama_item,
                                      COUNT (fun_items.fi_id) AS count
                                    FROM
                                      fun_items
                                    LEFT JOIN fun_items_sub ON fun_items_sub.fis_id = fun_items.fi_id
                                    LEFT JOIN trans_functionality ON trans_functionality.fis_id = fun_items_sub.fis_id
                                    LEFT JOIN m_aircraft_reg ON m_aircraft_reg.id = trans_functionality.acr_id
                                    LEFT JOIN m_aircraft_type ON m_aircraft_type.id = m_aircraft_reg.id_aircraft_type_fk
                                    WHERE
                                      YEAR (
                                        trans_functionality.modified_date
                                      ) >= '$thn'
                                    AND MONTH (
                                      trans_functionality.modified_date
                                    ) >= '$bln1'
                                    AND YEAR (
                                      trans_functionality.modified_date
                                    ) <= '$thn'
                                    AND MONTH (
                                      trans_functionality.modified_date
                                    ) <= '$bln2'
                                    GROUP BY
                                      fun_items.fi_id,
                                      fun_items.fi_name");
        return $query->result_array();
    }


    function save_mitigation_funct($data)
    {
        $this->load->database();
        $this->db->insert('cabin_mitigation', $data);
        return $this->db->insert_id();
    }

    function update_mitigation_funct($where, $data)
    {
        $this->load->database();
        $this->db->update('cabin_mitigation', $data, $where);
        return $this->db->affected_rows();
    }

    /*function show_mitigation_funct($tgl, $id_reg, $id_item){
      $this->load->database();
      $query = $this->db->query("SELECT *
                                  FROM cabin_mitigation
                                  WHERE
                                  cabin_mitigation_date='$tgl'
                                  AND acreg='$id_reg'
                                  AND item='$id_item'");
      return $query->result();
    }*/

    function show_mitigation_funct($tgl, $id_item)
    {
        $this->load->database();
        $query = $this->db->query("SELECT *
                                  FROM cabin_mitigation
                                  WHERE
                                  cabin_mitigation_date='$tgl'
                                  AND item='$id_item'");
        return $query->result();
    }

}
