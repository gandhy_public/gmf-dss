<?php

  class M_kpi_unit_temp extends CI_Model
  {

    public function __construct()
    {
      parent::__construct();
      $this->dev_gmf = $this->load->database("dev_gmf", true);
    }

    public function update_actual_where($id)
    {
      $kiad_actual = $this->input->post("kiad_actual");
      $kiad_actual_ytd = $this->input->post("kiad_actual_ytd");
      $sql = "update kpi_item_assigment_detail set kiad_actual = '".$kiad_actual."', kiad_actual_ytd = '".$kiad_actual_ytd."' where kiad_id = '".$id."'";
      $exec = $this->dev_gmf->query($sql);
      if($exec){
        return true;
      } else{
        return false;
      }
    }

    public function getAllItemKpiWithDetail()
    {
      $month = $this->input->get("month");
      $role_id = $this->input->get("role_id");
      $nowYear = $this->input->get("year");

      $sql = "SELECT
              	a.*, b.kf_name,
              	b.fa_function_name,
              	c.kiad_id,
              	c.kiad_date,
              	c.kiad_target,
              	c.kiad_target_ytd,
              	c.kiad_limit,
              	c.kiad_islock,
              	c.kiad_author,
                c.kiad_actual,
                c.kiad_actual_ytd,
                c.kiad_weight
              FROM
              	kpi_item_assignment a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_assigment_detail c ON a.kia_id = c.kia_id
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, c.kiad_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
              	MONTH,
              	CONVERT (DATE, c.kiad_date, 20)
              ) = '".$month."'
              where a.role_id = '".$role_id."'
               and DATEPART( year, CONVERT ( DATE, a.kia_year_date, 20 ) ) = '".$nowYear."'";


      $exec = $this->dev_gmf->query($sql);
      return $exec->result();
    }

    public function getDeepChild($id,$role_id)
    {
      $sql = "select count(kia_parent_id) as total from kpi_item_assignment where kia_parent_id = '".$id."' and role_id='".$role_id."'";
      $exec = $this->dev_gmf->query($sql);
      $arr = $exec->row();
      return $arr->total;
    }

    public function getTotalScoreWhere($id, $month, $nowYear, $role_id,$depth)
    {
      // echo "SELECT
      //         	a.*, b.kf_name,
      //         	b.fa_function_name,
      //         	c.kiad_id,
      //         	c.kiad_date,
      //         	c.kiad_target,
      //         	c.kiad_limit,
      //         	c.kiad_islock,
      //         	c.kiad_author,
      //         	c.kiad_actual,
      //           c.kiad_weight
      //         FROM
      //         	kpi_item_assignment a
      //         JOIN kpi_formula b ON a.kf_id = b.kf_id
      //         LEFT JOIN kpi_item_assigment_detail c ON a.kia_id = c.kia_id
      //         AND DATEPART(
      //         	YEAR,
      //         	CONVERT (DATE, c.kiad_date, 20)
      //         ) = '".$nowYear."'
      //         AND DATEPART(
      //         	MONTH,
      //         	CONVERT (DATE, c.kiad_date, 20)
      //         ) = '".$month."'
      //         where a.role_id = '".$role_id."' and a.kia_id = '".$id."'";
      $sql = "SELECT
              	a.*, b.kf_name,
              	b.fa_function_name,
              	c.kiad_id,
              	c.kiad_date,
              	c.kiad_target,
              	c.kiad_limit,
              	c.kiad_islock,
              	c.kiad_author,
              	c.kiad_actual,
                c.kiad_weight
              FROM
              	kpi_item_assignment a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_assigment_detail c ON a.kia_id = c.kia_id
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, c.kiad_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
              	MONTH,
              	CONVERT (DATE, c.kiad_date, 20)
              ) = '".$month."'
              where a.role_id = '".$role_id."' and a.kia_parent_id = '".$id."'";
        $exec = $this->dev_gmf->query($sql);
        $result = $exec->result();
        $total = 0;
        foreach ($result as $data) {
          // echo "tes";
                    $weight = "";
                    if($depth == "0"){
                      $weight = $this->countWeight($data->kia_id, $nowYear, $month,$role_id);
                    } else {
                      if(empty($data->kiad_weight)){
                        $weight  = $data->kia_weight;
                      } else{
                        $weight = $data->kiad_weight;
                      }
                    }
          $ach = number_format(achievment($data->kiad_target, $data->kiad_actual, $data->fa_function_name), 2);
          $total += score($ach, $data->kiad_limit, $weight);
          $total += $this->getTotalScoreWhere($data->kia_id, $month, $nowYear, $role_id,$depth+1);
        }
        return number_format($total, 2);
    }
    public function getTotalScoreWhereYtd($id, $month, $nowYear, $role_id,$depth)
    {
      // echo "SELECT
      //         	a.*, b.kf_name,
      //         	b.fa_function_name,
      //         	c.kiad_id,
      //         	c.kiad_date,
      //         	c.kiad_target,
      //         	c.kiad_limit,
      //         	c.kiad_islock,
      //         	c.kiad_author,
      //         	c.kiad_actual,
      //           c.kiad_weight
      //         FROM
      //         	kpi_item_assignment a
      //         JOIN kpi_formula b ON a.kf_id = b.kf_id
      //         LEFT JOIN kpi_item_assigment_detail c ON a.kia_id = c.kia_id
      //         AND DATEPART(
      //         	YEAR,
      //         	CONVERT (DATE, c.kiad_date, 20)
      //         ) = '".$nowYear."'
      //         AND DATEPART(
      //         	MONTH,
      //         	CONVERT (DATE, c.kiad_date, 20)
      //         ) = '".$month."'
      //         where a.role_id = '".$role_id."' and a.kia_id = '".$id."'";
      $sql = "SELECT
              	a.*, b.kf_name,
              	b.fa_function_name,
              	c.kiad_id,
              	c.kiad_date,
              	c.kiad_target,
              	c.kiad_target_ytd,
              	c.kiad_limit,
              	c.kiad_islock,
              	c.kiad_author,
              	c.kiad_actual,
              	c.kiad_actual_ytd,
                c.kiad_weight
              FROM
              	kpi_item_assignment a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_assigment_detail c ON a.kia_id = c.kia_id
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, c.kiad_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
              	MONTH,
              	CONVERT (DATE, c.kiad_date, 20)
              ) = '".$month."'
              where a.role_id = '".$role_id."' and a.kia_parent_id = '".$id."'";
        $exec = $this->dev_gmf->query($sql);
        $result = $exec->result();
        $total = 0;
        foreach ($result as $data) {
          // echo "tes";
                    $weight = "";
                    if($depth == "0"){
                      $weight = $this->countWeight($data->kia_id, $nowYear, $month,$role_id);
                    } else {
                      if(empty($data->kiad_weight)){
                        $weight  = $data->kia_weight;
                      } else{
                        $weight = $data->kiad_weight;
                      }
                    }
          $ach = number_format(achievment($data->kiad_target_ytd, $data->kiad_actual_ytd, $data->fa_function_name), 2);
          $total += score($ach, $data->kiad_limit, $weight);
          $total += $this->getTotalScoreWhere($data->kia_id, $month, $nowYear, $role_id,$depth+1);
        }
        return number_format($total, 2);
    }
    public function maxLevel($id,$role_id)
    {
      $sql = "WITH rCTE AS(
                  SELECT *, 0 AS Level FROM kpi_item_assignment WHERE kia_id = '".$id."'
                  and role_id ='".$role_id."'
                  UNION ALL
                  SELECT t.*, r.Level + 1 AS Level
                  FROM kpi_item_assignment t
                  INNER JOIN rCTE r
                      ON t.kia_parent_id = r.kia_id
              )
              SELECT max(level) as ujung FROM rCTE OPTION(MAXRECURSION 0)";
      $exec = $this->dev_gmf->query($sql);
      return $exec->row()->ujung;
    }

    public function countWeight($id, $year, $month,$role_id)
    {
      $sql = "SELECT
                SUM (weight) AS total
              FROM
                (
                  SELECT
                    CASE
                  WHEN b.kiad_weight IS NULL THEN
                    a.kia_weight
                  ELSE
                    b.kiad_weight
                  END AS weight
                  FROM
                    kpi_item_assignment a
                  LEFT JOIN kpi_item_assigment_detail b ON a.kia_id = b.kia_id
                  AND DATEPART(
                    MONTH,
                    CONVERT (DATE, kiad_date, 20)
                  ) = '".$month."'
                  AND DATEPART(
                    YEAR,
                    CONVERT (DATE, kiad_date, 20)
                  ) = '".$year."'
                  WHERE
                    kia_parent_id = '".$id."'
                    and role_id = '".$role_id."'
                ) as A";
        $exec = $this->dev_gmf->query($sql);
        return $exec->row()->total;
    }

    public function getTotalScoreNotLevelOneWhere($id, $month, $nowYear, $depth,$role_id)
    {
      // echo "select kia_id, kia_weight from kpi_item_assignment where kia_parent_id = '".$id."'
      // and role_id='".$role_id."'
      //         AND DATEPART(
      //           YEAR,
      //           CONVERT (DATE, kia_year_date, 20)
      //         ) = '".$nowYear."'";
      $total = 0;
      $sql = "select kia_id, kia_weight from kpi_item_assignment where kia_parent_id = '".$id."'
      and role_id='".$role_id."'
              AND DATEPART(
                YEAR,
                CONVERT (DATE, kia_year_date, 20)
              ) = '".$nowYear."'";
      $exec = $this->dev_gmf->query($sql);
      $arr = [];
      foreach ($exec->result() as $data) {
        $arr[] = array("ki_id" => $data->kia_id, "weight" => $data->kia_weight);
      }
      // $newId = implode(",", $arr);
      // exit();
      return $arr;
    }

  }
