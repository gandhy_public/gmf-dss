<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Master_Repetitive_hill extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->SQL_server = $this->load->database('dev_gmf', true);
    }

    public function cek_target($in)
    {
        $sql = $this->db->query("SELECT target_val FROM tbl_all_target WHERE target_year='" . $in['tahun'] . "' AND target_type ='Repetitive'");
        $row = $sql->row_array();
        return $row['target_val'];
    }

    public function add_target_rpcr($in)
    {
        $sql = $this->db->query("INSERT INTO tbl_all_target (target_val,target_year,target_type) VALUES ('" . $in['target'] . "','" . $in['tahun'] . "','Repetitive') ");
        return true;
    }

    public function update_target_rpcr($in)
    {
        $sql = $this->db->query("UPDATE tbl_all_target SET target_val='" . $in['target'] . "' WHERE target_year='" . $in['tahun'] . "' AND target_type ='Repetitive'");
        return true;
    }

    public function cek_hill($in)
    {
        $sql = $this->db->query("SELECT target_val FROM tbl_all_target WHERE target_year='" . $in['tahun'] . "' AND target_type ='Hill'");
        $row = $sql->result_array();
        return $row;
    }

    public function m_get_mitigation($value)
    {
        // code...
        $query = $this->SQL_server->select('*')
            ->from('tbl_mitigasi')
            ->where('mitigasi_fk_id', $value)
            ->get();
        return $query->result_array();
    }

    public function cek_mitigastion($value, $data)
    {
        // code...
        $query = $this->SQL_server->select('mitigasi_id')
            ->from('tbl_mitigasi')
            ->where('mitigasi_fk_id', $value)
            ->get();

        if ($query->num_rows() == 0) {
            // code...
            $query2 = $this->SQL_server->insert('tbl_mitigasi', $data);
        } else {
            $query2 = $this->SQL_server->where('mitigasi_fk_id', $value)
                ->update('tbl_mitigasi', $data);
        }

        return $query2;
    }

    public function target($data)
    {
        // code...
        $query = $this->db->select('*')
            ->from('tbl_all_target')
            ->where('target_year', $data['target_year'])
            ->where('target_type', $data['target_type'])
            ->get();
        // return $query->num_rows();

        if ($query->num_rows() == 0) {
            // code...
            $query2 = $this->db->insert('tbl_all_target', $data);
        } else {
            $query2 = $this->db->where('target_year', $data['target_year'])
                ->where('target_type', $data['target_type'])
                ->update('tbl_all_target', $data);
        }

        return $query2;
    }


    //===========DATATABLE PN============\\
    var $table2 = 'sap_hil_d3';
    var $column_order2 = array('own', 'created_on', 'notification', 'req_start', 'actype');
    var $column_search2 = array('own', 'created_on', 'notification', 'req_start', 'actype');
    var $order2 = array('created_on' => 'ASC');

    private function querypn($start, $end, $data)
    {
        // print_r($start[1]);
        // print_r($start[0]);
        // die;

        $this->SQL_server->select('own,actype,notification, created_on,req_start,mitigasi_id');
        $this->SQL_server->from('sap_hil_d3 hill');
        $this->SQL_server->join('m_acreg aircraft', 'hill.functional_location = aircraft.acreg', 'left');
        $this->SQL_server->join('m_actype airtype', 'aircraft.actype_id = airtype.actype_id', 'left');
        $this->SQL_server->join('tbl_mitigasi mitigasi', 'mitigasi.mitigasi_fk_id = hill.notification', 'left');
        $this->SQL_server->where_in('aircraft.own', array('GA', 'CITILINK'));
        $this->SQL_server->where("(system_status LIKE '%OSNO%' OR system_status LIKE '%NOPR%')");
        $this->SQL_server->where('DATEPART(YEAR,( CASE WHEN ISDATE( created_on ) = 1 THEN CAST ( created_on AS DATE ) END )) >=', $start[1]);
        $this->SQL_server->where('DATEPART(YEAR,( CASE WHEN ISDATE( created_on ) = 1 THEN CAST ( created_on AS DATE ) END )) <=', $end[1]);
        $this->SQL_server->where('DATEPART(MONTH,( CASE WHEN ISDATE( created_on ) = 1 THEN CAST ( created_on AS DATE ) END )) >=', $start[0]);
        $this->SQL_server->where('DATEPART(MONTH,( CASE WHEN ISDATE( created_on ) = 1 THEN CAST ( created_on AS DATE ) END )) <=', $end[0]);

        $i = 0;

        foreach ($this->column_search2 as $item) {
            if (!empty($_POST['search']['value'])) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->SQL_server->group_start();
                    $this->SQL_server->like($item, $_POST['search']['value']);
                } else {
                    $this->SQL_server->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search2) - 1 == $i) //last loop
                    $this->SQL_server->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->SQL_server->order_by($this->column_order2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->SQL_server->order_by(key($order2), $order2[key($order)]);
        }
    }

    function get_datatables($start, $end, $data)
    {

        $this->querypn($start, $end, $data);
        if ($data['length'] != -1)
            $this->SQL_server->limit($data['length'], $data['start']);
        $query = $this->SQL_server->get();
        return $query->result_array();
    }

    function count_all($start, $end, $data)
    {

        $this->querypn($start, $end, $data);
        $query = $this->SQL_server->get();
        return $query->num_rows();
    }


    function count_filtered_pn($start, $end, $data)
    {

        $this->querypn($start, $end, $data);
        $query = $this->SQL_server->get();
        return $query->num_rows();
    }


}
