<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class M_csi_dashboard_new extends CI_Model
  {
     public function __construct()
      {
        parent::__construct();
      }

      public function show_customer(){
      	$sql = $this->db->query("SELECT id_customer, name_customer FROM csi_customer ORDER BY name_customer ASC");
      	return $sql->result_array();
      }

      public function show_chart1_by_all($val,$query,$date){
        $sql = $this->db->query("SELECT * FROM (SELECT avg(finalscore) as finalscore,date_type
                                  FROM csi_history_action ca join csi_detail_finalscore cf
                                  ON ca.id_history=cf.id_history_action
                                  WHERE ca.date_type BETWEEN '".$val['start']."' AND '".$val['end']."'
                                  AND ca.type='".$val['range']."' ".$query."
                                  GROUP BY ca.id_history
                                  ORDER BY date_type ASC )
                                  A where date_type ='$date'");
        return $sql->result();
      }

      public function get_total_score_all_cus($val,$query){
        $sql = $this->db->query("SELECT AVG(total_score) as total_score,id_type_org_csi,shortness
                                  from csi_data_org cdo join csi_history_action cha
                                  on cdo.id_history_usr_action = cha.id_history join csi_org_type co
                                  on cdo.id_type_org_csi = co.id_org_type
                                  where date_type='".$val['date']."'
                                  and type='".$val['range']."'
                                  ".$query."
                                  GROUP BY id_type_org_csi,shortness
                                  order by id_type_org_csi asc ");
        return $sql->result();
      }

      public function show_chart2_by($val,$query,$date){// customer
        $sql = $this->db->query("SELECT * FROM (SELECT avg(com_csi) as com_csi,
                                  avg(qa_csi) as qa_csi,
                                  avg(bqs_csi) as bqs_csi,
                                  avg(otd_csi) as otd_csi,
                                  avg(pca_csi) as pca_csi,
                                  avg(ss_csi) as ss_csi,
                                  avg(oss_csi) as oss_csi,
                                  avg(fm_csi) as fm_csi,date_type FROM csi_history_action ch
                                  JOIN csi_data_org co ON ch.id_history=co.id_history_usr_action
                                  WHERE ch.date_type BETWEEN '".$val['start']."' AND '".$val['end']."'
                                  ".$query."
                                  AND ch.type='".$val['range']."'
                                  GROUP BY id_history_usr_action
                                  ORDER BY date_type ASC)
                                  RT
                                  WHERE date_type='$date'");
        return $sql->result();
      }

      public function show_chart3_all($val,$date,$quer){
        $sql = $this->db->query("SELECT * FROM (SELECT AVG(avg_bus) as avg_bus,
                                  AVG(avg_sus) AS avg_sus ,
                                  date_type FROM csi_detail_finalscore cf
                                  JOIN csi_history_action ch on cf.id_history_action=ch.id_history
                                  WHERE ch.date_type BETWEEN '".$val['start']."' AND '".$val['end']."'
                                  ".$quer."
                                  AND type='".$val['range']."'
                                  GROUP BY ch.id_history
                                  ORDER BY date_type ASC)
                                  RT
                                  where date_type='$date'");
        return $sql->result();
      }


//////////////////////////////////////////////////////////////////////////////

      // public function show_chart1_month(){
      //   $date_end = date("Y-m");
      //   $y = date("Y");
      //   $date_start = $y."-01";
      //
      //   $sql = $this->db->query("SELECT
      //     	cdf.id_history_action,
      //     	date_type,SUBSTR(date_type,1,4) AS TAHUN,
      //     (CASE
      //     			WHEN SUBSTR(date_type,6,2) = '06' THEN (select avg(finalscore)*0.5 from csi_detail_finalscore where id_history_action in (SELECT id_history FROM csi_history_action where type='monthly' and date_type BETWEEN CONCAT(TAHUN,'-01') and CONCAT(TAHUN,'-05')))+(select avg(finalscore)*0.5  from csi_detail_finalscore where id_history_action in (SELECT id_history FROM csi_history_action where type='semesterly' and date_type =CONCAT(TAHUN,'-06')))
      //     			WHEN SUBSTR(date_type,6,2) = '12' THEN (select avg(finalscore)*0.5 from csi_detail_finalscore where id_history_action in (SELECT id_history FROM csi_history_action where type='monthly' and date_type BETWEEN CONCAT(TAHUN,'-07') and CONCAT(TAHUN,'-11')))+(select avg(finalscore)*0.5 from csi_detail_finalscore where id_history_action in (SELECT id_history FROM csi_history_action where type='semesterly' and date_type =CONCAT(TAHUN,'-12')))
      //     			ELSE AVG(finalscore)
      //     	END) as finals
      //     FROM
      //     	csi_detail_finalscore cdf
      //     JOIN csi_history_action cha ON cdf.id_history_action = cha.id_history
      //     WHERE
      //     	cha.type = 'monthly'
      //     AND date_type BETWEEN '".$date_start."'
      //     AND '".$date_end."'
      //     GROUP BY
      //     	cdf.id_history_action
      //     ORDER BY
      //     	cha.date_type ASC");
      //   return $sql->result();
      // }


      // Semesterly grafik

      // public function show_chart2_by($val,$level){//
      //   $sql = $this->db->query("SELECT avg(com_csi) as com_csi,
      //    avg(qa_csi) as qa_csi,
      //     avg(bqs_csi) as bqs_csi,
      //     avg(otd_csi) as otd_csi,
      //     avg(pca_csi) as pca_csi,
      //     avg(ss_csi) as ss_csi,
      //     avg(oss_csi) as oss_csi,
      //     avg(fm_csi) as fm_csi,
      //     date_type FROM csi_history_action ch
      //     JOIN csi_data_org co ON ch.id_history=co.id_history_usr_action
      //     WHERE ch.date_type BETWEEN '".$val['start']."' AND '".$val['end']."'
      //     AND level_customer='".$level."'
      //     AND ch.type='".$val['range']."'
      //     GROUP BY id_history_usr_action ORDER BY date_type ASC");
      //   return $sql->result();
      // }

      // public function show_chart2_by_all($val){//  AND ch.type='".$val['range']."'
      //   $sql = $this->db->query("SELECT avg(com_csi) as com_csi, a
      //   vg(qa_csi) as qa_csi,
      //   avg(bqs_csi) as bqs_csi,
      //   avg(otd_csi) as otd_csi,
      //   avg(pca_csi) as pca_csi,
      //   avg(ss_csi) as ss_csi,
      //   avg(oss_csi) as oss_csi,
      //   avg(fm_csi) as fm_csi,
      //   date_type FROM csi_history_action ch JOIN csi_data_org co
      //   ON ch.id_history=co.id_history_usr_action
      //   WHERE ch.date_type BETWEEN '".$val['start']."' AND '".$val['end']."'
      //   AND level_customer='1'
      //   GROUP BY id_history_usr_action ORDER BY date_type ASC");
      //   return $sql->result();
      // }


      // public function show_chart2_by_all_gen($val){
      //   $sql = $this->db->query("SELECT avg(com_csi) as com_csi,
      //   avg(qa_csi) as qa_csi,
      //   avg(bqs_csi) as bqs_csi,
      //   avg(otd_csi) as otd_csi,
      //   avg(pca_csi) as pca_csi,
      //   avg(ss_csi) as ss_csi,
      //   avg(oss_csi) as oss_csi,
      //   avg(fm_csi) as fm_csi,
      //   date_type FROM csi_history_action ch
      //   JOIN csi_data_org co ON ch.id_history=co.id_history_usr_action
      //   WHERE ch.date_type BETWEEN '".$val['start']."' AND '".$val['end']."'
      //   AND ch.type='".$val['range']."' AND level_customer='3'
      //   GROUP BY id_history_usr_action ORDER BY date_type ASC");
      //   return $sql->result();
      // }

      // public function get_total_score_all_cus($val){
      //   $sql = $this->db->query("SELECT AVG(total_score) as total_score,id_type_org_csi,shortness from csi_data_org cdo join csi_history_action cha on cdo.id_history_usr_action=cha.id_history join csi_org_type co on cdo.id_type_org_csi=co.id_org_type where id_customer='".$val['cus']."' AND date_type='".$val['date']."' and type='".$val['range']."' GROUP BY id_type_org_csi,shortness order by id_type_org_csi asc ");
      //   return $sql->result();
      // }

  }
