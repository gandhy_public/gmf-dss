<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_master_mitigation extends CI_Model{

  var $mitig_type;
  var $s_date;
  var $e_date;
  var $table = 'tbl_master_mitigation';
  var $column_order = array('id_master_mitigation','date','aircraft', 'aircraft_type', 'target', 'actual', null);
  var $column_search = array('aircraft','aircraft_type');
  var $order = array('date' => 'asc');

  private function _get_datatables_query(){

    $this->db->select('tbl_master_mitigation.id_master_mitigation,
                       tbl_master_mitigation.date,
                       tbl_master_mitigation.aircraft,
                       tbl_master_mitigation.aircraft_type,
                       tbl_master_mitigation.target,
                       tbl_master_mitigation.actual,
                       tbl_master_mitigation.mitigation_type'
                       );
    $this->db->from($this->table);
    if ($this->mitig_type!="") {
      $this->db->where('mitigation_type', $this->mitig_type);
    }if ($this->s_date!="" && $this->e_date!="") {
      $this->db->where('date >=', $this->s_date."-01");
      $this->db->where('date <=', $this->e_date."-30");
    }
    $i = 0;

    foreach ($this->column_search as $item)
    {
      if(!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order']))
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_alldata()
  {
    $this->_get_datatables_query();
    $data = $this->db->get();

    return $data->result();
  }

  function get_datatables($mitigation_type="", $s_month="", $e_month="")
  {
    $this->mitig_type = $mitigation_type;
    $this->s_date = $s_month;
    $this->e_date = $e_month;

    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
    return $query->result();
  }

  function count_filtered($mitigation_type="", $s_month="", $e_month="")
  {
    $this->mitig_type = $mitigation_type;
    $this->s_date = $s_month;
    $this->e_date = $e_month;
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all($mitigation_type="", $s_month="", $e_month="")
  {
    $this->mitig_type = $mitigation_type;
    $this->s_date = $s_month;
    $this->e_date = $e_month;
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $this->db->count_all_results();
  }

  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_master_mitigation',$id);
    $query = $this->db->get();

    return $query->row();
  }

  public function save($data)
  {
    $this->db->insert($this->table, $data);

    $id = $this->db->insert_id();
    return (isset($id)) ? $id : FALSE;
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->where('id_master_mitigation', $id);
    $this->db->delete($this->table);
  }

}
