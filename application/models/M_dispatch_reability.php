<?php
ini_set('max_execution_time', 0);
defined('BASEPATH') or exit ('no direct script access allowed');

/**
 *
 */
class M_dispatch_reability extends CI_Model
{


  public function __construct()
  {
    parent::__construct();
  }

  public function getAllAcType($manufacture)
  {
    $this->dev_gmf = $this->load->database("dev_gmf", true);
    $result = array();
    $actypePost = $this->input->post("acType");
    $query = "";
    if(!empty($actypePost)){
      $acTypePare = "'".implode("','", $actypePost)."'";
      $query .= ' actype in ('.$acTypePare.') ';
    }
    if($manufacture == "GA"){
      $query .= " and actype not in('B737-300', 'B737-400', 'B737-500', 'F-28', 'B747-200') ";
    }
    $q1 = "SELECT
          	DISTINCT(a.actype_id), a.actype, b.own
          FROM
          	m_actype a
          JOIN m_acreg b ON a.actype_id = b.actype_id
          where b.own = '".$manufacture."' ".$query;

    $exec = $this->dev_gmf->query($q1);
    foreach ($exec->result() as $data) {
      $result[] = $data;
    }
    return $result;
  }

  public function getAllAcTypeTwo()
  {
    $this->dev_gmf = $this->load->database("dev_gmf", true);
    $result = array();
    $actypePost = $this->input->post("acType");
    $query = "";
    if(!empty($actypePost)){
      if($actypePost == "GA" || $actypePost == "CITILINK"){
        $query .=  "WHERE b.own = '".$actypePost."'" ;
      } else{
        $query .= " WHERE a.actype = '".$actypePost."' ";
      }
    }
    $q1 = "SELECT
            DISTINCT(a.actype_id), a.actype as dr_actype
          FROM
            m_actype a
          JOIN m_acreg b ON a.actype_id = b.actype_id
          ".$query;
    $exec = $this->dev_gmf->query($q1);
    foreach ($exec->result() as $data) {
      $result[] = $data;
    }
    return $result;
  }

  public function getDataYear($years, $months)
  {
    $this->dev_gmf = $this->load->database("dev_gmf", true);
    $actype = $this->input->post("acType");
    $acreg = $this->input->post("acReg");
    $query = "";
    if(!empty($actype)){
      if($actype == "GA" || $actype == "CITILINK"){
        $sql = "SELECT
              	DISTINCT(a.actype_id), a.actype, b.own
              FROM
              	m_actype a
              JOIN m_acreg b ON a.actype_id = b.actype_id
              where b.own = '".$actype."'";
         $exec = $this->dev_gmf->query($sql);
         $arr = [];
         foreach ($exec->result() as $data) {
           $arr[] = $data->actype;
         }
         $acregPare = "'".implode("','", $arr)."'";
         $query .= ' AND dr_actype in ('.$acregPare.') ';
      } else {
        $query .= ' AND dr_actype = "'.$actype.'"';
      }
    }
    if (!empty($acreg) && count($acreg) > 0 && $acreg[0] != "all" && $acreg != "all" && $acreg != "") {
      $acregPare = "'".implode("','", $acreg)."'";
      $query .= " AND dr_acreg in (".$acregPare.") ";
    }

    $result = array();
      foreach ($months as $month) {
        $q2 = "SELECT
                	(rev - (delay+cancel))/rev * 100 as dr_result
                FROM
                	(
                		SELECT
                			sum(dr_total_rev) AS rev,
                			sum(dr_total_delay) AS delay,
                			sum(dr_total_cancel) AS cancel
                		FROM
                			dr_final
                		WHERE
                			MONTH (dr_date) = '".$month."'
                		AND YEAR (dr_date) = '".$years."'
                		".$query."
                		AND dr_range_type = 'monthly'
                	) as a";
        // $q2 = "SELECT SUM(dr_result)/count(dr_id) as dr_result from dr_final
        //          where MONTH(dr_date) = '".$month."'
        //          AND YEAR(dr_date) = '".$years."'
        //          ".$query."
        //          AND dr_range_type='monthly'";
        // echo "<pre>";
        // echo $q2;
        // echo "</pre>";
        $exQ2 = $this->db->query($q2);
        $arr2 = $exQ2->row();
        $result[] = (empty($arr2->dr_result) ? 0 : floatval(number_format($arr2->dr_result, 2)));
      }
      // exit();
      return $result;
  }

  public function nowWeek()
  {
    $q1 = "SELECT week('".date("Y-m-d")."') as week from DUAL";
    $exec = $this->db->query($q1);
    return $exec->row();
  }

  public function toWeek($week)
  {
    $result = array();
    $limitWeek = (date("l", mktime(0, 0, 0, 1, 1, date("Y"))) == "Sunday") ? 1 : 0;
    $lastMonth = (date("l", mktime(0, 0, 0, 1, 1, date("Y"))) == "Sunday") ? 13 : 12;
    if($week < $limitWeek){
      return array(
        "week" => intval($lastMonth)+intval($week),
        "year" => intval(date("Y")) - 1
      );
    }
    return array(
      "week" => $week,
      "year" => date("Y")
    );
  }


  public function getStartAndEndDate($week, $year)
  {
    $week_start = new DateTime();
    $week_start->setISODate($year,$week);
    $date1 = $week_start->format('Y-m-d');
    $date2 = date("Y-m-d", strtotime("+6 days", strtotime($date1)));
    return array($date1, $date2);
  }

  public function getDataWeek($actype, $weeks, $weekNow)
  {
    $acreg = $this->input->post("acReg");
    $date1 = $this->input->post("date1");
    $query = "";
    if (!empty($acreg) && count($acreg) > 0 && $acreg[0] != "all" && $acreg != "all" && $acreg != "") {
      $acregPare = "'".implode("','", $acreg)."'";
      $query .= " AND dr_acreg in (".$acregPare.") ";
    }
    $result = array();
    $xAxist = array();
    $i = $date1;
    foreach ($weeks as $week) {
      $reqDate = $this->getStartAndEndDate($week, date("Y"));
      $week1 = $reqDate[0];
      $week2 = $reqDate[1];
      $q2 = "SELECT
            	dr_actype,
            	CASE
            		WHEN (((rev -(delay + cancel)) / rev) * 100) IS NULL
            		THEN 0
            		WHEN (((rev -(delay + cancel)) / rev) * 100) > 0
            		THEN (((rev -(delay + cancel)) / rev) * 100)
            	END
            	AS dr_result
            FROM
            	(
            		SELECT
            			a.dr_actype,
            			SUM(a.dr_total_rev) AS rev,
            			SUM(a.dr_total_delay) AS delay,
            			SUM(a.dr_total_cancel) AS cancel
            		FROM
            			dr_final a
                where
                dr_date BETWEEN '".$week1."' and '".$week2."'
            		AND a.dr_range_type = 'weekly'
                AND a.dr_actype = '".$actype."'
                ".$query."
            	) A";
            // echo $q2; exit;

       $exQ2 = $this->db->query($q2);
       $arr2 = $exQ2->row();
       $result[] = ((empty($arr2->dr_result) || $arr2->dr_result === null) ? 0 : floatval(number_format($arr2->dr_result, 2))) ;
       $xAxist[] = "W ".$this->toWeek(intval($i))["week"].".".$this->toWeek(intval($i))["year"];
       $i++;
      }
      // exit();
      return array("result" => $result, "xAxist" => $xAxist);
  }
  public function dispatchTargetWeekly($year, $weeks, $weekNow)
  {
    $date1 = $this->input->post("date1");
    $result = array();
    $acType = $this->input->post("acType");
    $newActype = $this->convertTypeTarget($acType);
    $i = $date1;
    foreach ($weeks as $week) {
      $q = "SELECT dr_target_val from dr_target
            where dr_target_type = '".$newActype."'";
      $exQ = $this->db->query($q);
      $arr = $exQ->row();
      $result[] = (empty($arr->dr_target_val) ? null : floatval(number_format($arr->dr_target_val, 2))) ;
      // $q1 = "select SUM(b.dr_target_val)/COUNT(a.Date) as target
      //       from (
      //           select last_day('".$this->toWeek(intval($i))["year"]."-12-01') - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date
      //           from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
      //           cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
      //           cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
      //       ) a
      //       left join dr_target b on MONTH(a.Date) = MONTH(b.dr_target_date) AND YEAR(a.Date) = YEAR(b.dr_target_date)
      //       where a.Date between '".$this->toWeek(intval($i))["year"]."-01-01' and last_day('".$this->toWeek(intval($i))["year"]."-12-01') and WEEK(a.Date) = ".$this->toWeek(intval($i))["week"];
      // $exQ = $this->db->query($q1);
      // $arr = $exQ->row();
      // $result[] = (empty($arr->target) ? null : floatval(number_format($arr->target, 2)));
      // $i++;
    }
    return $result;
  }

  public function convertTypeTarget($target)
  {
    if($target == "GA"){
      return "garuda";
    } elseif ($target == "CITILINK") {
      return "citilink";
    } else{
      return $target;
    }
  }

  public function dispatchTarget($years, $months)
  {
    $result = array();
    $acType = $this->input->post("acType");
    $newActype = $this->convertTypeTarget($acType);
    foreach ($months as $month) {
      $q = "SELECT dr_target_val from dr_target
            where dr_target_type = '".$newActype."'";
      $exQ = $this->db->query($q);
      $arr = $exQ->row();
      $result[] = (empty($arr->dr_target_val) ? null : floatval(number_format($arr->dr_target_val, 2))) ;
    }
    return $result;
  }

  public function getActypeWhere($manufacture)
  {
    $this->dev_gmf = $this->load->database("dev_gmf", true);
    $sql = "SELECT
            	DISTINCT(b.actype),
            	own
            FROM
            	m_acreg a
            JOIN m_actype b ON a.actype_id = b.actype_id
            WHERE
            	own = '".$manufacture."'";
    $exec = $this->dev_gmf->query($sql);
    $result = [];
    foreach ($exec->result() as $data) {
      $result[] = $data->actype;
    }
    return $result;
  }

  public function getAcRegWhere($acType)
  {
      $this->dev_gmf = $this->load->database("dev_gmf", true);
      $result = array();
      $q1 = "SELECT DISTINCT
              (acreg),
              b.actype,
              own
            FROM
              m_acreg a
            JOIN m_actype b ON a.actype_id = b.actype_id
            WHERE
              b.actype = '".$acType."'";
      $exec = $this->dev_gmf->query($q1);
      foreach ($exec->result() as $data) {
        $result[] = $data;
      }
      return $result;
  }

  public function getDetailWhere($month, $year, $currentType, $currentReg)
  {
    $result = array();
    $nQuery = "";
    $nowType = "";
    if(!empty($currentType) && $currentType != "" && count($currentType) > 0){
      if($currentType == "GA" || $currentType == "CITILINK"){
        $nowType = $this->getActypeWhere($currentType);
        $currentTypePare = "'".implode("','", $nowType)."'";
        $nQuery .= " AND a.dr_actype IN (".$currentTypePare.") ";
      } else{
        $currentType_toarray = array_map("trim", explode(",", $currentType));
        $currentTypePare = "'".implode("','", $currentType_toarray)."'";
        $nQuery .= " AND a.dr_actype IN (".$currentTypePare.") ";
      }
    }

    if(!empty($currentReg) && $currentReg != "" && count($currentReg) > 0 && $currentReg != "all"){
      $currentReg_toarray = array_map("trim", explode(",", $currentReg));
      $currentRegPare = "'".implode("','", $currentReg_toarray)."'";
      $nQuery .= " AND a.dr_acreg IN (".$currentRegPare.") ";
    }

    $convertType = $this->convertTypeTarget($currentType);

    $q1 = "SELECT
              dr_actype,
              CASE
            		WHEN (((rev -(delay + cancel)) / rev) * 100) IS NULL
            		THEN 0
            		WHEN (((rev -(delay + cancel)) / rev) * 100) > 0
            		THEN (((rev -(delay + cancel)) / rev) * 100)
            	END
            	AS dr_result,
              dr_target_val
            from (
            SELECT
              a.dr_actype,
            	SUM(a.dr_total_rev) AS rev,
            	SUM(a.dr_total_delay) AS delay,
            	SUM(a.dr_total_cancel) AS cancel,
            	b.dr_target_val
            FROM
            	dr_final a
            -- LEFT JOIN dr_target b ON '".$convertType."' = b.dr_target_type
            LEFT JOIN dr_target b ON a.dr_actype = b.dr_target_type
            WHERE
            	YEAR (a.dr_date) = '".$year."'
            AND MONTH (a.dr_date) = '".$month."'
            ".$nQuery."
            AND a.dr_range_type = 'monthly'
            GROUP BY dr_actype
            ) A";
      // echo $q1; exit();
      $exQ = $this->db->query($q1);
      $arr = $exQ->result();
      return $arr;
   }

   public function getDetailAgain($month, $year, $actype, $acreg)
   {
     $nQuery = "";
     if(!empty($acreg) && $acreg != "" && count($acreg) > 0 && $acreg != "all"){
        $acreg_arr = explode(",", $acreg);
        $acreg_str = "'".implode("','", $acreg_arr)."'";
        $nQuery .= " AND a.dr_acreg IN (".$acreg_str.") ";
     }
     $nowType = $this->convertTypeTarget($actype);
     $q1 = "SELECT
            a.dr_id,
          	a.dr_acreg,
            a.dr_total_rev,
            a.dr_total_delay,
            a.dr_total_cancel,
          	SUM(a.dr_result) / count(a.dr_acreg) AS dr_result,
          	b.dr_target_val
          FROM
          	dr_final a
          LEFT JOIN dr_target b ON '".$nowType."' = b.dr_target_type
          WHERE
          	YEAR (a.dr_date) = '".$year."'
          AND MONTH (a.dr_date) = '".$month."'
          AND a.dr_actype = '".$actype."'
          ".$nQuery."
          AND a.dr_range_type='monthly'
          GROUP BY
            a.dr_id,
          	a.dr_acreg,
            a.dr_total_rev,
            a.dr_total_delay,
            a.dr_total_cancel,
          	b.dr_target_val
          ORDER BY dr_result ASC";
      $exQ = $this->db->query($q1);
      $arr = $exQ->result();
      return $arr;
   }

   public function getDetailAgainDua($date, $actype, $currentReg)
   {
     $week = substr($date, 2, -5);
     $year = substr($date, -4, 4);
     $nQuery = "";
     if(!empty($currentReg) && $currentReg != "" && count($currentReg) > 0 && $currentReg != "all"){
        $acreg_arr = explode(",", $currentReg);
        $acreg_str = "'".implode("','", $acreg_arr)."'";
        $nQuery .= " AND a.dr_acreg IN (".$acreg_str.") ";
     }
     $nowType = $this->convertTypeTarget($actype);
     $q1 = "SELECT
           a.dr_id,
           a.dr_acreg,
           a.dr_date,
           a.dr_total_rev,
           a.dr_total_delay,
           a.dr_total_cancel,
           SUM(a.dr_result) / count(a.dr_acreg) AS dr_result,
           b.dr_target_val
          FROM
           dr_final a
          LEFT JOIN dr_target b ON '".$nowType."' = b.dr_target_type
          WHERE
           YEAR (a.dr_date) = '".$year."'
          AND WEEK (a.dr_date) = '".$week."'
          AND a.dr_actype = '".$actype."'
          ".$nQuery."
          AND a.dr_range_type='weekly'
          GROUP BY
           a.dr_id,
           a.dr_acreg,
           a.dr_date,
           a.dr_total_rev,
           a.dr_total_delay,
           a.dr_total_cancel,
           b.dr_target_val
          ORDER BY dr_result ASC";
      $exQ = $this->db->query($q1);
      $arr = $exQ->result();
      return $arr;
   }

   public function getMitigationWhere($id)
   {
     $q1 = "SELECT
          	mitigasi_id,
          	mitigasi_why,
          	mitigasi_solution
          FROM
          	tbl_mitigasi
          WHERE
          	mitigasi_fk_id = '".$id."'";
    $exQ = $this->db->query($q1);
    $arr = $exQ->row();
    return $arr;
   }


}


 ?>
