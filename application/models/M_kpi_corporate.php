<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kpi_corporate extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    $this->SQL_server=$this->load->database('dev_gmf', true);
  }

  public function get_grup()
  {
    $sql = "select * from kpi_grup";
    $exec = $this->SQL_server->query($sql);
    return $exec->result();
  }

  public function get($year,$month, $id, $type = null)
  {
    $sqlPlus = "";
    if($type != null){
      if($type == "grup"){
        $sqlPlus = " and a.kpi_group_id = '".$id."' ";
      } else if ($type == "single") {
        $sqlPlus = " and a.ki_id = '".$id."' ";
      } else if ($type == "name") {
        $sqlPlus = " and a.ki_name = '".$id."' ";
      } else {
        $sqlPlus = "";
      }
    } else {
      $sqlPlus = " ";
    }
    $sql="SELECT
                a.ki_id,
                SUM (e.kiad_actual) / COUNT (a.ki_id) AS kiad_actual,
                SUM (e.kiad_actual_ytd) / COUNT (a.ki_id) AS kiad_actual_ytd,
                a.ki_name,
                a.ki_uom,
                a.ki_year_date,
                a.ki_parent_id,
                a.ki_target,
                a.ki_target_ytd,
                a.ki_weight,
                b.kf_name,
                c.kid_limit,
                c.kid_weight,
                b.fa_function_name,
                c.kid_target,
                c.kid_target_ytd,
                e.kiad_id,
                e.kiad_date,
                e.kiad_target,
                e.kiad_weight,
                e.kiad_limit,
                e.kiad_islock,
                e.kiad_author
              FROM
                kpi_item a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_detail c ON a.ki_id = c.ki_id
              AND DATEPART(
                YEAR,
                CONVERT (DATE, c.kid_date, 20)
              ) = '".$year."'
              AND DATEPART(
                MONTH,
                CONVERT (DATE, c.kid_date, 20)
              ) = '".$month."'
              LEFT JOIN kpi_item_assignment d ON a.ki_id = d.ki_id
              LEFT JOIN kpi_item_assigment_detail e ON d.kia_id = e.kia_id
              AND DATEPART(
                YEAR,
                CONVERT (DATE, e.kiad_date, 20)
              ) = '".$year."'
              AND DATEPART(
                MONTH,
                CONVERT (DATE, e.kiad_date, 20)
              ) = '".$month."'
              WHERE DATEPART(
                YEAR,
                CONVERT (DATE, a.ki_year_date, 20)
              ) = '".$year."'
              ".$sqlPlus."
              GROUP BY
                a.ki_id,
                a.ki_name,
                a.ki_uom,
                a.ki_year_date,
                a.ki_parent_id,
                a.ki_target,
                a.ki_target_ytd,
                a.ki_weight,
                b.kf_name,
                c.kid_limit,
                c.kid_weight,
                b.fa_function_name,
                c.kid_target,
                c.kid_target_ytd,
                e.kiad_id,
                e.kiad_date,
                e.kiad_target,
                e.kiad_weight,
                e.kiad_limit,
                e.kiad_islock,
                e.kiad_author";
     $exec = $this->SQL_server->query($sql);
      return $exec->result();
  }

  public function get_target_parent($year)
  {
    // code...
    $this->SQL_server->select('ki_target');
    $this->SQL_server->from('kpi_item');
    $this->SQL_server->where('ki_parent_id',0);
    $this->SQL_server->where('DATEPART(
                YEAR,
                CONVERT (DATE, ki_year_date, 20)
              ) = ',$year);
    return $this->SQL_server->get()->result();
  }

  public function getItemKpibyName($name){
    $this->SQL_server->select('*');
    $this->SQL_server->from('kpi_item');
    $this->SQL_server->where('ki_name',$name);

    return $this->SQL_server->get()->row();
  }

 

  public function findKpiByGroup($id,$year){
    $this->SQL_server->select('*');
    $this->SQL_server->from('kpi_item');
    $this->SQL_server->where('kpi_group_id',$id);
     $this->SQL_server->where('DATEPART(
                YEAR,
                CONVERT (DATE, ki_year_date, 20)
              ) = ',$year);

    return $this->SQL_server->get()->result();
  }

  public function findKpiUnitByGroup($role,$id,$year){
    $this->SQL_server->select('*');
    $this->SQL_server->from('kpi_item_assignment');
    $this->SQL_server->where('kpi_group_id',$id);
    $this->SQL_server->where('role_id',$role);
     $this->SQL_server->where('DATEPART(
                YEAR,
                CONVERT (DATE, kia_year_date, 20)
              ) = ',$year);

    return $this->SQL_server->get()->result();
  }

   public function getItemKpiUnitbyName($name,$year,$role){
    $this->SQL_server->select('*');
    $this->SQL_server->from('kpi_item_assignment');
    $this->SQL_server->where('kia_name',$name);
    $this->SQL_server->where('kia_year_date',$year);
    $this->SQL_server->where('role_id',$role);

    return $this->SQL_server->get()->row();
  }

  public function cek($id,$year){
    $this->SQL_server->select('*');
    $this->SQL_server->from('kpi_item');
    $this->SQL_server->where('ki_name',$id);
     $this->SQL_server->where('DATEPART(
                YEAR,
                CONVERT (DATE, ki_year_date, 20)
              ) = ',$year);

    return $this->SQL_server->get()->result();
  }

  public function cek_unit($id,$year,$role){
    $this->SQL_server->select('*');
    $this->SQL_server->from('kpi_item_assignment');
    $this->SQL_server->where('kia_name',$id);
    $this->SQL_server->where('role_id',$role);
     $this->SQL_server->where('DATEPART(
                YEAR,
                CONVERT (DATE, kia_year_date, 20)
              ) = ',$year);

    return $this->SQL_server->get()->result();
  }

  public function kpiOnGroup($year){
    $this->SQL_server->select('*');
    $this->SQL_server->from('kpi_item');
    $this->SQL_server->where('kpi_group_id!=',null);
    $this->SQL_server->where('ki_parent_id',0);
    // $this->SQL_server->where('kpi_group_id is not ',empty);
     $this->SQL_server->where('DATEPART(
                YEAR,
                CONVERT (DATE, ki_year_date, 20)
              ) = ',$year);

    return $this->SQL_server->get()->result();
  }

}
