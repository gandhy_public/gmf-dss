<?php
defined('BASEPATH') or exit ('no direct script access allowed');

/**
 *
 */
class M_m_dispatch_reability extends CI_Model
{
      var $table  = 'dr_target';
      var $column_order = array('dr_target_type','dr_type_val', 'dr_target_date',null);
      var $column_search = array('dr_target_type','dr_type_val', 'dr_target_date'); 
      var $order = array('id' => 'ASC'); 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->dev_gmf = $this->load->database('dev_gmf', TRUE);
    }

    public function getDataTargetAll()
    {
        $this->db->select('*');
        $this->db->from('dr_target');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function acTypes()
    {
        $acTypes = $this->dev_gmf->select('actype')
            ->from('m_actype')
            ->get()
            ->result_array();

        return $acTypes;
    }

    public function dataTarget()
    {
        $year = date('Y');
        $this->db->select('*');
        $this->db->from('dr_target');
        $this->db->where("DATE_FORMAT(dr_target_date,'%Y')", $year);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getByMonthTarget($month)
    {
        $targets = $this->dataTarget();
        $targetByMonth = '-';
        foreach ($targets as $target) {
            if (substr($target['dr_target_date'], 5, 2) == $month) {
                $targetByMonth = $target['dr_target_val'];
            }
        }
        return $targetByMonth;
    }

    function get_mitigations()
    {
        $this->db->select('*');
        $this->db->from('dr_final');
        $query = $this->db->get();

        return $query->result();
    }

    function showDatatableTarget($data)
    {
        $this->db->select('*');
        $this->db->from('dr_target');
        $query1 = $this->db->get();
        $total = $query1->num_rows();

        $output = array();
        $output["draw"] = $data["draw"];
        $output["recordsTotal"] = $output["recordsFiltered"] = $total;
        $output["data"] = array();
        if ($data["search"] != "") {
            $this->db->like('dr_target_type', $data["search"]);
            $this->db->or_like('dr_target_val', $data["search"]);
        }

        $this->db->select('*');
        $this->db->from('dr_target');
        $this->db->limit($data["length"], $data["start"]);
        $this->db->order_by($data["order_column"], $data["order_dir"]);
        if ($data["search"] != "") {
            $this->db->like('dr_target_type', $data["search"]);
            $this->db->or_like('dr_target_val', $data["search"]);
            $query = $this->db->get();
            $output["recordsTotal"] = $output["recordsFiltered"] = $query->num_rows();
        } else {
            $query = $this->db->get();
        }

        $nomor_urut = $data['start'] + 1;
        foreach ($query->result_array() as $data) {
            $output['data'][] = array(
                $nomor_urut,
                $data['dr_target_type'],
                $data['dr_target_val'],
                $data["id"],
            );
            $nomor_urut++;
        }

        echo json_encode($output, JSON_PRETTY_PRINT);

    }

    function showDatatableMitigation($data)
    {
        $this->db->select('*');
        $this->db->from('dr_final AS df');
        $this->db->join('dr_target AS dt', 'YEAR(dt.dr_target_date) = YEAR(df.dr_date) AND dt.dr_target_type = df.dr_actype');
        $this->db->where('df.dr_result < dt.dr_target_val');
        if ($data['month_from'] != null and $data['month_to'] != null) {
            $this->db->where('dr_date >=', date('Y-m-d', strtotime($data['month_from'] . '-01')));
            $this->db->where('dr_date <=', date('Y-m-d', strtotime($data['month_to'] . '-01')));
        }
        $query1 = $this->db->get();
        $total = $query1->num_rows();

        $output = array();
        $output["draw"] = $data["draw"];
        $output["recordsTotal"] = $output["recordsFiltered"] = $total;
        $output["data"] = array();
        if ($data["search"] != "") {
            $this->db->like('df.dr_date', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));
            $this->db->or_like('df.dr_range_type', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));;
            $this->db->or_like('df.dr_actype', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));;
            $this->db->or_like('df.dr_acreg', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));;
            $this->db->or_like('dt.dr_target_val', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));;
            $this->db->or_like('df.dr_result', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));;
        }

        $this->db->select('*');
        $this->db->from('dr_final AS df');
        $this->db->join('dr_target AS dt', 'YEAR(dt.dr_target_date) = YEAR(df.dr_date) AND dt.dr_target_type = df.dr_actype');
        $this->db->where('df.dr_result < dt.dr_target_val');
        if ($data['month_from'] != null and $data['month_to'] != null) {
            $this->db->where('dr_date >=', date('Y-m-d', strtotime($data['month_from'] . '-01')));
            $this->db->where('dr_date <=', date('Y-m-d', strtotime($data['month_to'] . '-01')));
        }
        $this->db->limit($data["length"], $data["start"]);
        $this->db->order_by($data["order_column"], $data["order_dir"]);
        if ($data["search"] != "") {
            $this->db->like('df.dr_date', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));
            $this->db->or_like('df.dr_range_type', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));;
            $this->db->or_like('df.dr_actype', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));;
            $this->db->or_like('df.dr_acreg', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));;
            $this->db->or_like('dt.dr_target_val', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));;
            $this->db->or_like('df.dr_result', $data["search"])
                ->where('df.dr_result < dt.dr_target_val')
                ->where('df.dr_date >=', date('Y-m-d', strtotime($data['month_from'])))
                ->where('df.dr_date <=', date('Y-m-d', strtotime($data['month_to'])));;
            $query = $this->db->get();
            $output["recordsTotal"] = $output["recordsFiltered"] = $query->num_rows();
        } else {
            $query = $this->db->get();
        }
        $tblHaveMitigations = $this->db->select('mitigasi_fk_id')
            ->from('tbl_mitigasi')
            ->where('mitigasi_type', 'dispatch')
            ->get()->result_array();
        $convertArrays = new RecursiveIteratorIterator(new RecursiveArrayIterator($tblHaveMitigations));
        $tblHaveMitigation = array();
        foreach ($convertArrays as $convertArray) {
            array_push($tblHaveMitigation, $convertArray);
        }
        $nomor_urut = $data['start'] + 1;
        foreach ($query->result_array() as $mitigation) {
            if (in_array($mitigation['dr_id'], $tblHaveMitigation)) {
                $haveComment = 1;
            } else {
                $haveComment = 0;
            }
            if($mitigation['dr_range_type'] == 'weekly'){
                $con_date = date("W", strtotime($mitigation['dr_date']));
            }else{
                $con_date = date("m-Y", strtotime($mitigation['dr_date']));
            }
            $output['data'][] = array(
                $nomor_urut,
                $mitigation['dr_id'],
                $con_date,
                $mitigation['dr_range_type'],
                $mitigation['dr_actype'],
                $mitigation['dr_acreg'],
                number_format($mitigation['dr_target_val'], 2),
                number_format($mitigation['dr_result'], 2),
                $haveComment
            );
            $nomor_urut++;
        }
        echo json_encode($output, JSON_PRETTY_PRINT);
    }

    function getMitigationById($dr_id)
    {
        $mitigation = $this->db->query("SELECT * FROM tbl_mitigasi WHERE mitigasi_fk_id='$dr_id'");
        if ($mitigation->num_rows() > 0) {
            foreach ($mitigation->result() as $data) {
                $hasil = array(
                    'dr_id' => $data->mitigasi_fk_id,
                    'mitigasi_why' => $data->mitigasi_why,
                    'mitigasi_solution' => $data->mitigasi_solution,
                );
            }
        } else {
            $hasil = array('dr_id' => $dr_id);
        }
        return $hasil;
    }

    function update_comment_mitigation($dr_id, $mitigasi_why, $mitigasi_solution)
    {
        $hasil = $this->db->query("UPDATE tbl_mitigasi SET mitigasi_why='$mitigasi_why', mitigasi_solution='$mitigasi_solution' WHERE mitigasi_fk_id='$dr_id'");
        return $hasil;
    }

    function inputTarget($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function getTargetById($id)
    {
        $target = $this->db->select('*')
            ->from('dr_target')
            ->where('id', $id)
            ->get()
            ->row();

        return $target;
    }

    public function updateTarget($input)
    {
        $this->db->where('id', $input['id']);
        $this->db->update('dr_target', $input);
    }

    public function deleteTarget($id)
    {
        $this->db->where('id', $id);
        return ($this->db->delete('dr_target')) ? true : false;

    }

    function inputCommentMitigation($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function updateCommentMitigation($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    //TAMBAHAN DIMAS
    private function query(){
            $this->db->from($this->table);
            $i = 0;
            foreach ($this->column_search as $item){
                if($_POST['search']['value']){   
                    $this->db->like("LOWER(dr_target_type)", strtolower($_POST["search"]["value"]));
                    $this->db->or_like("LOWER(dr_target_val)", strtolower($_POST["search"]["value"])); 
                    $this->db->or_like("LOWER(dr_target_date)", strtolower($_POST["search"]["value"]));
                }
            $i++;
            }
            if(isset($_POST['order'])){
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }else if(isset($this->order)){
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
    }

    function get_datatables_target(){
        $this->query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_target(){
        $this->query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all_target(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function inputTarget_new($data){
      $this->db->insert($this->table, $data);
      return $this->db->insert_id();
    }

    function getTargetById_new($id){
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->row();
    }

    function updateTarget_new($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    function deleteTarget_new($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    

}
