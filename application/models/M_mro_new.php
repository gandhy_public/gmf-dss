
<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  /**
   *
   */
  class M_mro_new extends CI_Model
  {

    public function __construct()
    {
      parent::__construct();
      $this->load->database();
      $this->db = $this->load->database('dev_gmf_prod', TRUE);
    }

     var $table  = 'mro_new';
    var $column_order = array('id_mro','year','operator');
    var $column_search = array('year', 'operator'); 
    var $order = array('id_mro' => 'ASC');  

    private function query(){

        $this->db->from('mro_new');
        $i = 0;
    
      foreach ($this->column_search as $item) 
      {
        if(!empty($_POST['search']['value'])) // if datatable send POST for search
        {
          
          if($i===0) // first loop
          {
            $this->db->group_start(); 
            $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $_POST['search']['value']);
          }

          if(count($this->column_search) - 1 == $i) //last loop
            $this->db->group_end(); //close bracket
        }
        $i++;
      }
      
      if(isset($_POST['order'])) 
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      } 
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }
        

    function get_datatables_mro(){

        $this->query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_mro(){

        $this->query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all_mro(){

        $this->query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function upload_data($data){
      $this->db->trans_begin();
      $this->db->insert_batch($this->table,$data);
      if ($this->db->trans_status() === FALSE){
        $this->db->trans_rollback();
        return FALSE;
      }else{
        $this->db->trans_commit();
        return TRUE;
      }
    }


}