<?php
/**
 * Created by PhpStorm.
 * User: ucup
 * Date: 5/7/18
 * Time: 1:47 PM
 */

// ini_set('max_execution_time', 0);

class M_Sla_CA extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->dev_gmf = $this->load->database('dev_gmf', TRUE);
    }

    public function get_all_sla()
    {
      $sql = "SELECT
              	scc_trans.id,
              	scc_trans.func_location,
              	m_acreg.own,
              	m_actype.actype,
              	scc_group_sla.grouping,
              	scc_trans.status,
              	scc_trans.performance,
              	CONVERT(DATE, scc_trans.posting_date, 112) AS posting_date
              FROM
              	[dbo].[scc_trans]
              JOIN scv_part_number ON REPLACE( LEFT ( scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = scv_part_number.part_number OR scc_trans.material = scv_part_number.part_number
              JOIN m_slaca_pn ON REPLACE( LEFT ( scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '' ) = m_slaca_pn.part_number OR scc_trans.material = m_slaca_pn.part_number
              JOIN scc_group_sla ON REPLACE( LEFT (scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = scc_group_sla.material OR scc_trans.material = scc_group_sla.material
              JOIN m_acreg ON scc_trans.func_location = m_acreg.acreg
              JOIN m_actype ON m_acreg.actype_id = m_actype.actype_id
              WHERE	scv_part_number.skema = 'PBTH ACCESS'
              AND scv_part_number.aircraft = 'B737NG'
              AND YEAR(CONVERT(DATE, scc_trans.posting_date, 112))= '".date("Y")."'
              AND m_acreg.own = 'GA'
              AND m_actype.actype_id = '588'
              order by grouping ASC";
        $data = $this->dev_gmf->query($sql);
        return $data->result_array();
    }

    public function acRegs()
    {
        $acRegs = $this->dev_gmf->select('acreg')
            ->from('m_acreg')
            ->where('actype_id', 588)
            ->where('own', "GA")
            ->get()
            ->result_array();

        return $acRegs;
    }

    public function funcLocation()
    {
        $acRegs = array();
        $sl1 = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegs()));
        foreach ($sl1 as $data) {
            array_push($acRegs, $data);
        }

        $sccTrans = $this->dev_gmf->select('func_location')
            ->from('scc_trans')
            ->where_in('func_location', $acRegs)
            ->get()
            ->result_array();

        $result = array();
        $acregs = new RecursiveIteratorIterator(new RecursiveArrayIterator($sccTrans));
        foreach ($acregs as $data) {
            array_push($result, $data);
        }

        return array_unique($result);
    }

    public function sccTrans($grouping, $acReg)
    {
        $cek_acreg = substr($acReg,0,5);
            $this->dev_gmf->select('distinct(scc_trans.material)');
            $this->dev_gmf->from('scc_trans');
            $this->dev_gmf->join("scc_group_sla", "REPLACE(LEFT (scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = scc_group_sla.material or scc_trans.material = scc_group_sla.material", "left");
            if($cek_acreg == 'PK-GE' || $cek_acreg == 'PK-GF' || $cek_acreg == 'PK-GM' || $cek_acreg == 'PK-GN'){
              $this->dev_gmf->join("m_slaca_pn", "REPLACE(LEFT (scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = m_slaca_pn.part_number or scc_trans.material = m_slaca_pn.part_number", "left");
            }
            $this->dev_gmf->like('grouping', $grouping);
            $this->dev_gmf->where('func_location', $acReg);
            $sccTrans = $this->dev_gmf->get();

        return $sccTrans->result();
    }

    //SubModul Dashboard
    public function getDataGrafik($input, $grouping, $acReg)
    {
        if ($input['startPeriode'] == '' and $input['endPeriode'] == '') {
            $year = date('Y');
            $monthFrom = $year . '-01';
            $monthTo = date('Y-m');
        } else {
            $monthFrom = $input['startPeriode'];
            $monthTo = $input['endPeriode'];
        }

        $start = (new DateTime($monthFrom))->modify('first day of this month');
        $end = (new DateTime($monthTo))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $result = array();

        foreach ($period as $key => $dt) {
            if ($input['status'] == 'General') {
                $finalDataGeneral = array();
                if ($grouping == 'SL1') {
                    $sl1 = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->sccTrans('SL 1 3 Jam', $acReg)));
                    foreach ($sl1 as $data) {
                        array_push($finalDataGeneral, $data);
                    }

                }
                if ($grouping == 'SL2') {
                    $sl2 = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->sccTrans('SL 2 24 Jam', $acReg)));
                    foreach ($sl2 as $data) {
                        array_push($finalDataGeneral, $data);
                    }

                }
                if ($grouping == 'SL3') {
                    $sl3 = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->sccTrans('SL 3 72 Jam', $acReg)));
                    foreach ($sl3 as $data) {
                        array_push($finalDataGeneral, $data);
                    }

                }
                if ($grouping == 'SL4') {
                    $sl4 = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->sccTrans('SL 4 72 Jam', $acReg)));
                    foreach ($sl4 as $data) {
                        array_push($finalDataGeneral, $data);
                    }
                }

                $value = array();
                $valueOnTime = array();

                if ($finalDataGeneral == null) {
                    array_push($value, 0);
                } else {
                    $queryGeneral = $this->dev_gmf->select('*')
                        ->from('scc_trans')
                        ->where_in('material', $finalDataGeneral)
                        ->where('func_location', $acReg)
                        ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) =", $dt->format('m'))
                        ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) =", $dt->format('Y'))
                        ->get()->result_array();

                    array_push($value, count($queryGeneral));

                    $queryGeneral2 = $this->dev_gmf->select('*')
                        ->from('scc_trans')
                        ->where_in('material', $finalDataGeneral)
                        ->where('func_location', $acReg)
                        ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) =", $dt->format('m'))
                        ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) =", $dt->format('Y'))
                        ->where('status', 'ON TIME')
                        ->get()->result_array();

                    array_push($valueOnTime, count($queryGeneral2));
                }

                foreach ($value as $key => $val) {
                    if ($val == 0) {
                        array_push($result, 0);
                    } else {
                        array_push($result, number_format($valueOnTime[$key] / $val, 2) * 100);
                    }
                }

            } elseif ($input['status'] == 'Detail') {
                $finalDataDetail = array();
                if ($grouping == 'SL1') {
                    $sl1 = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->sccTrans('SL 1 3 Jam', $acReg)));
                    foreach ($sl1 as $data) {
                        array_push($finalDataDetail, $data);
                    }
                }
                if ($grouping == 'SL2') {
                    $sl2 = $this->sccTrans('SL 2 24 Jam', $acReg);
                    foreach ($sl2 as $data) {
                        array_push($finalDataDetail, $data->material);
                    }
                }
                if ($grouping == 'SL3') {
                    $sl3 = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->sccTrans('SL 3 72 Jam', $acReg)));
                    foreach ($sl3 as $data) {
                        array_push($finalDataDetail, $data);
                    }
                }
                if ($grouping == 'SL4') {
                    $sl4 = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->sccTrans('SL 4 72 Jam', $acReg)));
                    foreach ($sl4 as $data) {
                        array_push($finalDataDetail, $data);
                    }
                }

                $value = array();
                $valueOnTime = array();

                if ($finalDataDetail == null) {
                    array_push($value, 0);
                } else {
                    $query = $this->dev_gmf->select('*')
                        ->from('scc_trans')
                        ->join("scv_part_number", "REPLACE( LEFT ( scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = scv_part_number.part_number OR scc_trans.material = scv_part_number.part_number")
                        ->join("m_slaca_pn", "REPLACE( LEFT ( scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '' ) = m_slaca_pn.part_number OR scc_trans.material = m_slaca_pn.part_number")
                        ->join("scc_group_sla", "REPLACE( LEFT (scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = scc_group_sla.material OR scc_trans.material = scc_group_sla.material")
                        ->join("m_acreg", "scc_trans.func_location = m_acreg.acreg")
                        ->join("m_actype", "m_acreg.actype_id = m_actype.actype_id")
                        ->where_in('scc_trans.material', $finalDataDetail)
                        ->where('func_location', $acReg)
                        ->where("skema", "PBTH ACCESS")
                        ->where("aircraft", "B737NG")
                        ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) =", $dt->format('m'))
                        ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) =", $dt->format('Y'))
                        ->get()->result_array();

                    array_push($value, count($query));

                    $query2 = $this->dev_gmf->select('*')
                        ->from('scc_trans')
                        ->join("scv_part_number", "REPLACE( LEFT ( scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = scv_part_number.part_number OR scc_trans.material = scv_part_number.part_number")
                        ->join("m_slaca_pn", "REPLACE( LEFT ( scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '' ) = m_slaca_pn.part_number OR scc_trans.material = m_slaca_pn.part_number")
                        ->join("scc_group_sla", "REPLACE( LEFT (scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = scc_group_sla.material OR scc_trans.material = scc_group_sla.material")
                        ->join("m_acreg", "scc_trans.func_location = m_acreg.acreg")
                        ->join("m_actype", "m_acreg.actype_id = m_actype.actype_id")
                        ->where_in('scc_trans.material', $finalDataDetail)
                        ->where('func_location', $acReg)
                        ->where("skema", "PBTH ACCESS")
                        ->where("aircraft", "B737NG")
                        ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) =", $dt->format('m'))
                        ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) =", $dt->format('Y'))
                        ->where('status', 'ON TIME')
                        ->get()->result_array();

                    // $query2 = $this->dev_gmf->select('*')
                    //     ->from('scc_trans')
                    //     ->where_in('material', $finalDataDetail)
                    //     ->where('func_location', $acReg)
                    //     ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) =", $dt->format('m'))
                    //     ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) =", $dt->format('Y'))
                    //
                    //     ->get()->result_array();

                    array_push($valueOnTime, count($query2));
                }

                foreach ($value as $key => $val) {
                    if ($val == 0) {
                        array_push($result, 0);
                    } else {
                        array_push($result, number_format($valueOnTime[$key] / $val, 2) * 100);
                    }
                }
            }
        }

        return $result;
    }


    public function getNameMonth($input)
    {
        if ($input['startPeriode'] == '' and $input['endPeriode'] == '') {
            $year = date('Y');
            $monthFrom = $year . '-01';
            $monthTo = date('Y-m');
        } else {
            $monthFrom = $input['startPeriode'];
            $monthTo = $input['endPeriode'];
        }

        $pecah = explode('-', $input['endPeriode']);

        // echo $monthFrom;
        // echo '<br>';
        // echo $monthTo;
        // die;

        $start = (new DateTime($monthFrom))->modify('first day of this month');
        $end = (new DateTime($monthTo))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $monthNames = array();
        foreach ($period as $dt) {
            $monthName = $dt->format('M') . '-' . $pecah[0];

            array_push($monthNames, $monthName);
        }

        //print_r($monthNames);die;
        return $monthNames;
    }

    public function showSlTable($data, $input)
    {
        if ($input['startPeriode'] == '' and $input['endPeriode'] == '') {
            $year = date('Y');
            $monthFrom = strtotime($year . '-01-01');
            $monthTo = strtotime(date('Y-m-d'));
        } else {
            $monthFrom = strtotime($input['startPeriode'] . '-01');
            $monthTo = strtotime($input['endPeriode'] . '-01');
        }

        if ($input['slType'] == 'all' or $input['slType'] == null) {
            $groups = array('SL 1 3 Jam', 'SL 2 24 Jam', 'SL 3 72 Jam', 'SL 4 72 Jam');
        } else {
            $groups = array($input['slType']);
        }

        $this->dev_gmf->select('*');
        $this->dev_gmf->from('scc_trans');
        $this->dev_gmf->where('func_location', $input['acreg']);
        $this->dev_gmf->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom));
        $this->dev_gmf->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom));
        $this->dev_gmf->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo));
        $this->dev_gmf->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));
        if ($input['week'] != '' or $input['week'] != null) {
            $this->dev_gmf->where("posting_date != ", "00000000");
            $this->dev_gmf->where("DATEPART(week, CONVERT (DATE, posting_date, 104)) =", $input['week']);
        }
        $this->dev_gmf->join("scc_group_sla", "REPLACE(LEFT (scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = scc_group_sla.material or scc_trans.material = scc_group_sla.material", "left");
        $this->dev_gmf->join("scv_part_number", "REPLACE(LEFT (scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = scv_part_number.part_number or scc_trans.material = scv_part_number.part_number", "LEFT");
        $this->dev_gmf->join("m_slaca_pn", "REPLACE( LEFT ( scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '' ) = m_slaca_pn.part_number OR scc_trans.material = m_slaca_pn.part_number");
        $this->dev_gmf->join("m_acreg", "scc_trans.func_location = m_acreg.acreg");
        $this->dev_gmf->join("m_actype", "m_acreg.actype_id = m_actype.actype_id");
        $this->dev_gmf->where("scv_part_number.aircraft", "B737NG");
        $this->dev_gmf->where("scv_part_number.skema", "PBTH ACCESS");
	      $this->dev_gmf->where_in('scc_group_sla.grouping', $groups);
        $query1 = $this->dev_gmf->get();
        $total = $query1->num_rows();

        $output = array();
        $output["draw"] = $data["draw"];
        $output["recordsTotal"] = $output["recordsFiltered"] = $total;
        $output["data"] = array();
        if ($data["search"] != "") {
            $this->dev_gmf->like('func_location', $data["search"])
                ->where('func_location', $input['acreg'])
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom))
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));
            $this->dev_gmf->or_like('scc_trans.material', $data["search"])
                ->where('func_location', $input['acreg'])
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom))
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));
            $this->dev_gmf->or_like('grouping', $data["search"])
                ->where('func_location', $input['acreg'])
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom))
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));
            $this->dev_gmf->or_like('performance', $data["search"])
                ->where('func_location', $input['acreg'])
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom))
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));
            $this->dev_gmf->or_like('status', $data["search"])
                ->where('func_location', $input['acreg'])
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom))
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));
        }

        $this->dev_gmf->select('*');
        $this->dev_gmf->from('scc_trans');
        $this->dev_gmf->where('func_location', $input['acreg']);
        $this->dev_gmf->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom));
        $this->dev_gmf->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom));
        $this->dev_gmf->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo));
        $this->dev_gmf->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));
        if ($input['week'] != '' or $input['week'] != null) {
            $this->dev_gmf->where("posting_date != ", "00000000");
            $this->dev_gmf->where("DATEPART(week, CONVERT (DATE, posting_date, 104)) =", $input['week']);
        }
        $this->dev_gmf->join("scc_group_sla", "REPLACE(LEFT (scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = scc_group_sla.material or scc_trans.material = scc_group_sla.material", "left");
        $this->dev_gmf->join("scv_part_number", "REPLACE(LEFT (scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '') = scv_part_number.part_number or scc_trans.material = scv_part_number.part_number", "LEFT");
        $this->dev_gmf->join("m_slaca_pn", "REPLACE( LEFT ( scc_trans.material, CHARINDEX(':', scc_trans.material)), ':', '' ) = m_slaca_pn.part_number OR scc_trans.material = m_slaca_pn.part_number");
        $this->dev_gmf->join("m_acreg", "scc_trans.func_location = m_acreg.acreg");
        $this->dev_gmf->join("m_actype", "m_acreg.actype_id = m_actype.actype_id");
        $this->dev_gmf->where("scv_part_number.aircraft", "B737NG");
        $this->dev_gmf->where("scv_part_number.skema", "PBTH ACCESS");
		$this->dev_gmf->where_in('scc_group_sla.grouping', $groups);
        $this->dev_gmf->limit($data["length"], $data["start"]);
        $this->dev_gmf->order_by($data["order_column"], $data["order_dir"]);
        if ($data["search"] != "") {
            $this->dev_gmf->like('func_location', $data["search"])
                ->where('func_location', $input['acreg'])
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom))
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));
            $this->dev_gmf->or_like('scc_trans.material', $data["search"])
                ->where('func_location', $input['acreg'])
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom))
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));
            $this->dev_gmf->or_like('grouping', $data["search"])
                ->where('func_location', $input['acreg'])
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom))
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));
            $this->dev_gmf->or_like('performance', $data["search"])
                ->where('func_location', $input['acreg'])
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom))
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));
            $this->dev_gmf->or_like('status', $data["search"])
                ->where('func_location', $input['acreg'])
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('m', $monthFrom))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) >=", date('Y', $monthFrom))
                ->where("MONTH( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('m', $monthTo))
                ->where("YEAR( CASE WHEN ISDATE( posting_date ) = 1 THEN CAST ( posting_date AS DATE ) END ) <=", date('Y', $monthTo));

            $query = $this->dev_gmf->get();
            $output["recordsTotal"] = $output["recordsFiltered"] = $query->num_rows();
        } else {
            $query = $this->dev_gmf->get();
        }
        $nomor_urut = $data['start'] + 1;
        foreach ($query->result_array() as $data) {
            $availableDay = date_diff(new DateTime($data['req_date']), new DateTime($data['posting_date']));
            $reservationTime = date_diff(new DateTime($data['entered_at']), new DateTime($data['time_req']));
            $timeReq = date_create($data['time_req']);
            $output['data'][] = array(
                $nomor_urut,
                $data['id'],
				$data['order_number'],
                $data['func_location'],
                $data['material'],
                date('d-m-Y', strtotime($data['posting_date'])),
                date_format($timeReq, "H:i:s"),
                $availableDay->format("%R%a"),
                $reservationTime->format("%R%h" . ':' . "%i" . ':' . "%s"),
                $data['grouping'],
                $data['performance'] . '%',
                $data['status'],
            );
            $nomor_urut++;
        }
        echo json_encode($output, JSON_PRETTY_PRINT);
    }

    public function getRobbing()
    {
        $year = date('Y');
        $monthFrom = $year . '-01-01';
        $monthTo = $year . '-12-31';

        $start = (new DateTime($monthFrom))->modify('first day of this month');
        $end = (new DateTime($monthTo))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $value = array();
        foreach ($period as $dt) {
            $query = $this->dev_gmf->select('robbing_trans.*')
                ->distinct()
                ->from('robbing_trans')
                ->where("MONTH( CASE WHEN ISDATE( created_on ) = 1 THEN CAST ( created_on AS DATE ) END ) =", $dt->format('m'))
                ->where("YEAR( CASE WHEN ISDATE( created_on ) = 1 THEN CAST ( created_on AS DATE ) END ) =", $dt->format('Y'))
                ->join("scv_part_number", "REPLACE( LEFT ( robbing_trans.material, CHARINDEX( ':', robbing_trans.material )), ':', '' ) = scv_part_number.part_number OR robbing_trans.material = scv_part_number.part_number")
                ->join("scv_mtype", "REPLACE( LEFT ( robbing_trans.material, CHARINDEX( ':', robbing_trans.material )), ':', '' ) = scv_mtype.part_number OR robbing_trans.material = scv_mtype.part_number")
                ->join("m_slaca_pn", "REPLACE(LEFT ( robbing_trans.material, CHARINDEX(':', robbing_trans.material) ), ':', '') = m_slaca_pn.part_number OR robbing_trans.material = m_slaca_pn.part_number")
                // ->join("m_acreg", "robbing_trans.receive_func_loc = m_acreg.acreg")
				->where("scv_part_number.aircraft", "B737NG")
				->where("scv_part_number.skema", "PBTH ACCESS")
                ->where('scv_mtype.mtype !=', 'EXP')
                ->where('scv_part_number.skema !=', 'TMB')
                ->where('scv_part_number.skema !=', null)
                ->where('scv_part_number.skema !=', '')
                ->where('scv_part_number.skema !=', 'EXPENDABLE')
                ->not_like('robbing_trans.description', 'x-change')
                ->not_like('robbing_trans.description', 'cross change')
                ->not_like('robbing_trans.description', 'swap')
                ->not_like('robbing_trans.description', 'cancel')
                ->not_like('robbing_trans.description', 'xchange')
                ->not_like('robbing_trans.description', 'crosschange')
                ->get();

            array_push($value, $query->result());
        }

        return $value;
    }

    public function countRobbing()
    {
        $robbings = $this->getRobbing();
        $value = array();

        foreach ($robbings as $robbing) {
            array_push($value, count($robbing));
        }

        return $value;
    }

    //datatable

    var $column_order = array(
        'robbing_trans.created_on',
        'robbing_trans.material',
        'robbing_trans.description',
        'robbing_trans.donor_func_loc',
        'robbing_trans.receive_func_loc'
    );
    var $column_search = array(
        'robbing_trans.created_on',
        'robbing_trans.material',
        'robbing_trans.description',
        'robbing_trans.receive_func_loc'
    );
    var $order = array('material' => 'asc');

    private function _get_datatables_query($month)
    {
        $this->dev_gmf->select('robbing_trans.*'); //created_on, robbing_trans.material, robbing_trans.description, donor_func_loc, receive_func_loc
        $this->dev_gmf->distinct();
        $this->dev_gmf->from('robbing_trans');
        $this->dev_gmf->where("MONTH( CASE WHEN ISDATE( created_on ) = 1 THEN CAST ( created_on AS DATE ) END ) =", $month);
        $this->dev_gmf->where("YEAR( CASE WHEN ISDATE( created_on ) = 1 THEN CAST ( created_on AS DATE ) END ) =", date('Y'));
        $this->dev_gmf->join("scv_part_number", "REPLACE( LEFT ( robbing_trans.material, CHARINDEX( ':', robbing_trans.material )), ':', '' ) = scv_part_number.part_number OR robbing_trans.material = scv_part_number.part_number");
        $this->dev_gmf->join("scv_mtype", "REPLACE( LEFT ( robbing_trans.material, CHARINDEX( ':', robbing_trans.material )), ':', '' ) = scv_mtype.part_number OR robbing_trans.material = scv_mtype.part_number");
        $this->dev_gmf->join("m_slaca_pn", "REPLACE(LEFT ( robbing_trans.material, CHARINDEX(':', robbing_trans.material) ), ':', '') = m_slaca_pn.part_number OR robbing_trans.material = m_slaca_pn.part_number");
        // $this->dev_gmf->join("m_acreg", "robbing_trans.receive_func_loc = m_acreg.acreg");
		$this->dev_gmf->where("scv_part_number.aircraft", "B737NG");
        $this->dev_gmf->where("scv_part_number.skema", "PBTH ACCESS");
        $this->dev_gmf->where('scv_mtype.mtype !=', 'EXP');
        $this->dev_gmf->where('scv_part_number.skema !=', 'TMB');
        $this->dev_gmf->where('scv_part_number.skema !=', null);
        $this->dev_gmf->where('scv_part_number.skema !=', '');
        $this->dev_gmf->where('scv_part_number.skema !=', 'EXPENDABLE');
        $this->dev_gmf->not_like('robbing_trans.description', 'x-change');
        $this->dev_gmf->not_like('robbing_trans.description', 'cross change');
        $this->dev_gmf->not_like('robbing_trans.description', 'swap');
        $this->dev_gmf->not_like('robbing_trans.description', 'cancel');
        $this->dev_gmf->not_like('robbing_trans.description', 'xchange');
        $this->dev_gmf->not_like('robbing_trans.description', 'crosschange');
        // $this->dev_gmf->get();
        // echo $this->dev_gmf->last_query(); exit();
        $i = 0;

        foreach ($this->column_search as $item) // looping awal
        {
            if ($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                if ($i === 0) // looping awal
                {
                    $this->dev_gmf->group_start();
                    $this->dev_gmf->like($item, $_POST['search']['value']);
                } else {
                    $this->dev_gmf->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->dev_gmf->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->dev_gmf->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->dev_gmf->order_by(key($order), $order[key($order)]);
        }

    }

    function get_datatables($month)
    {
        $this->_get_datatables_query($month);
        if ($_POST['length'] != -1)
            $this->dev_gmf->limit($_POST['length'], $_POST['start']);
        $query = $this->dev_gmf->get();
        return $query->result();
    }

    function count_filtered($month)
    {
        $this->_get_datatables_query($month);
        $query = $this->dev_gmf->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->getRobbing();
        return $this->dev_gmf->count_all_results();
    }

    //SubModul upload data
    public function showDatatable($data)
    {

        $total = $this->dev_gmf->count_all_results("scc_group_sla");

        $output = array();
        $output["draw"] = $data["draw"];
        $output["recordsTotal"] = $output["recordsFiltered"] = $total;
        $output["data"] = array();
        if ($data["search"] != "") {
            $this->dev_gmf->like('material', $data["search"]);
            $this->dev_gmf->or_like('grouping', $data["search"]);
        }

        $this->dev_gmf->limit($data["length"], $data["start"]);
        $this->dev_gmf->order_by($data["order_column"], $data["order_dir"]);
        $query = $this->dev_gmf->get("scc_group_sla");
        if ($data["search"] != "") {
            $this->dev_gmf->like('material', $data["search"]);
            $this->dev_gmf->or_like('grouping', $data["search"]);

            $jum = $this->dev_gmf->get("scc_group_sla");
            $output["recordsTotal"] = $output["recordsFiltered"] = $jum->num_rows();
        }
        $nomor_urut = $data["start"] + 1;
        foreach ($query->result_array() as $sccGroupSla) {
            $output["data"][] = array(
                $nomor_urut,
                $sccGroupSla['material'],
                $sccGroupSla["grouping"],
            );
            $nomor_urut++;
        }
        echo json_encode($output, JSON_PRETTY_PRINT);
    }

    public function _emptyData()
    {
        $this->dev_gmf->from('scc_group_sla');
        $query = $this->dev_gmf->get();
        $rowcount = $query->num_rows();

        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }

    }

    public function getSccGroups()
    {
        $query = $this->dev_gmf->select('*')
            ->from('scc_group_sla')
            ->get();

        return $query->result();
    }

    public function insertData($datas)
    {
        $this->dev_gmf->truncate('scc_group_sla');
        foreach ($datas as $data) {
            $this->dev_gmf->insert('scc_group_sla', $data);
        }
    }

}
