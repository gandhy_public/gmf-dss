<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_slabaseops extends CI_Model{

  var $table = 'kpi_item';
  var $column_order = array('ki_id','target_val','target_year', 'target_type', 'target_aircraft', null);
  var $column_search = array('ki_id','target_val','target_year');
  var $order = array('target_val' => 'asc');

  public function __construct()
  {
    parent::__construct();
      $this->load->database();
      $this->db_dev = $this->load->database('dev_gmf', TRUE);
  }

  private function _get_datatables_query(){

    $this->db_dev->select('kpi_item.ki_id,
                       kpi_item.kf_id,
                       kpi_item.ki_author,
                       kpi_item.ki_name,
                       kpi_item.ki_uom,
                       kpi_item.ki_level,
                       kpi_item.ki_year_date,
                       kpi_item.ki_parent_id,
                       kpi_item.ki_weight,
                       kpi_item.ki_target,
                       kpi_formula.kf_name,
                       kpi_formula.fa_function_name'
                       );
    $this->db_dev->from($this->table);
    $this->db_dev->join('kpi_formula', 'kpi_item.kf_id = kpi_formula.kf_id', 'left');

  }

  function get_alldata()
  {
    $this->_get_datatables_query();
    $data = $this->db_dev->get();

    return $data->result();
  }

  function get_data_chart_item($type, $start_date, $end_date)
  {
    $awal = explode("-",$start_date);
    $awal_bulan = $awal[0];
    $awal_tahun = $awal[1];
    $akhir = explode("-",$end_date);
    $akhir_bulan = $akhir[0];
    $akhir_tahun = $akhir[1];
    $this->db_dev->select("ki.ki_uom, MONTH(kiad_date) as bulan, YEAR(kiad_date) tahun,
                        kiad.kiad_actual as avg_");
    $this->db_dev->from("kpi_item_assigment_detail kiad");
    $this->db_dev->join('kpi_item_assignment kia', 'kia.kia_id = kiad.kia_id');
    $this->db_dev->join('kpi_item ki', 'ki.ki_id = kia.ki_id');
    // if (!empty($type) || $type!="") {
      $this->db_dev->where('kia.kia_id', $type);
    // }
    $this->db_dev->where("MONTH(kiad_date) >=", $awal_bulan);
    $this->db_dev->where("YEAR(kiad_date) >=", $awal_tahun);
    $this->db_dev->where("MONTH(kiad_date) <=", $akhir_bulan);
    $this->db_dev->where("YEAR(kiad_date) <=", $akhir_tahun);
    // $this->db_dev->group_by('MONTH(kiad_date), YEAR(kiad_date)');
    $this->db_dev->order_by('MONTH(kiad_date), YEAR(kiad_date)', 'asc');
    $query = $this->db_dev->get();
    // print_r($query->result());
    // die;
    return $query->result();
  }

  function get_data_target_item($type, $start_date, $end_date)
  {
    $awal = explode("-",$start_date);
    $awal_bulan = $awal[0];
    $awal_tahun = $awal[1];
    $akhir = explode("-",$end_date);
    $akhir_bulan = $akhir[0];
    $akhir_tahun = $akhir[1];
    $this->db_dev->select("kid.kid_target as target, MONTH(kid_date) as bulan, YEAR(kid_date) as tahun"
                       );
    $this->db_dev->from("kpi_item_detail kid");
    $this->db_dev->join('kpi_item ki', 'kid.ki_id = ki.ki_id');
    // if (!empty($type) || $type!="") {
      $this->db_dev->where('ki.ki_id', $type);
    // }
    $this->db_dev->where("MONTH(kid_date) >=", $awal_bulan);
    $this->db_dev->where("YEAR(kid_date) >=", $awal_tahun);
    $this->db_dev->where("MONTH(kid_date) <=", $akhir_bulan);
    $this->db_dev->where("YEAR(kid_date) <=", $akhir_tahun);
    // $this->db_dev->group_by('MONTH(kid_date), YEAR(kid_date)');
    $this->db_dev->order_by('MONTH(kid_date), YEAR(kid_date)', 'asc');
    $query = $this->db_dev->get();
    return $query->result();
  }

  function get_item_chart($type, $start_date)
  {
    $awal = explode("-",$start_date);
    $awal_bulan = $awal[0];
    $awal_tahun = $awal[1];
    $this->db_dev->select("ki.ki_id, kia.kia_id, ki.ki_name");
    $this->db_dev->from("kpi_item ki");
    $this->db_dev->join('kpi_item_assignment kia', 'kia.ki_id = ki.ki_id', 'left');
    if (!empty($type) || $type!="") {
      $this->db_dev->where("ki_parent_id = (SELECT ki_id FROM kpi_item WHERE ki_name = '$type' and YEAR(ki_year_date) = '$awal_tahun')", NULL, FALSE);
    }
    // $this->db_dev->where("YEAR(kiad_date)", $year);
    // $this->db_dev->where('kia_id IS NOT NULL');
    // $this->db_dev->group_by('MONTH(kiad_date), YEAR(kiad_date)');
    $query = $this->db_dev->get();
    return $query->result();
  }

  function get_chart($type="", $start_date, $end_date)
  {
    $awal = explode("-",$start_date);
    $awal_bulan = $awal[0];
    $awal_tahun = $awal[1];
    $akhir = explode("-",$end_date);
    $akhir_bulan = $akhir[0];
    $akhir_tahun = $akhir[1];
    // $this->db_dev->select("MONTH(kiad_date) as bulan, YEAR(kiad_date) tahun,
    //                     AVG(kiad.kiad_actual) as avg_"
    //                    );
    $this->db_dev->select("MONTH(kiad_date) as bulan, YEAR(kiad_date) tahun,
                        AVG(case when kiad.kiad_actual <= 1 then (kiad.kiad_actual*100) else kiad.kiad_actual end) as avg_");
    // $this->db_dev->select("MONTH(kiad_date) as bulan, YEAR(kiad_date) tahun,
    //                     (case when kiad.kiad_actual <= 1 then (kiad.kiad_actual*100) else kiad.kiad_actual end) as avg_"
    //                    );
    $this->db_dev->from("kpi_item_assigment_detail kiad");
    $this->db_dev->join('kpi_item_assignment kia', 'kia.kia_id = kiad.kia_id');
    $this->db_dev->join('kpi_item ki', 'ki.ki_id = kia.ki_id');
    if (!empty($type) || $type!="") {
      $this->db_dev->where("ki_parent_id = (SELECT ki_id FROM kpi_item WHERE ki_name = '$type' and YEAR(ki_year_date) = '$awal_tahun')", NULL, FALSE);
    }
    $this->db_dev->where("MONTH(kiad_date) >=", $awal_bulan);
    $this->db_dev->where("YEAR(kiad_date) >=", $awal_tahun);
    $this->db_dev->where("MONTH(kiad_date) <=", $akhir_bulan);
    $this->db_dev->where("YEAR(kiad_date) <=", $akhir_tahun);
    $this->db_dev->group_by('MONTH(kiad_date), YEAR(kiad_date)');
    $this->db_dev->order_by('MONTH(kiad_date), YEAR(kiad_date)', 'asc');
    $query = $this->db_dev->get();
    return $query->result();
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db_dev->limit($_POST['length'], $_POST['start']);
      $query = $this->db_dev->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db_dev->from($this->table);
    return $this->db_dev->count_all_results();
  }

  public function get_by_id($id)
  {
    $this->_get_datatables_query();
    $this->db_dev->where('ki_id',$id);
    $query = $this->db_dev->get();

    return $query->row();
  }

  public function get_by_name($name)
  {
    $this->db_dev->from($this->table);
    $this->db_dev->like('ki_name',$name,'BOTH');
    $query = $this->db_dev->get();

    return $query->row();
  }

  public function save($data)
  {
    $this->db_dev->insert($this->table, $data);

    $id = $this->db_dev->insert_id();
    return (isset($id)) ? $id : FALSE;
  }

  public function update($where, $data)
  {
    $this->db_dev->update($this->table, $data, $where);
    return $this->db_dev->affected_rows();
  }

  public function delete($id)
  {
    $this->db_dev->where('ki_id', $id);
    $this->db_dev->delete($this->table);
  }
  public function listActualWhere($id,$month,$nowYear)
    {
      $sql = "SELECT
                a.ki_id,
              	SUM (e.kiad_actual) / COUNT (a.ki_id) AS kiad_actual,
              	SUM (e.kiad_actual_ytd) / COUNT (a.ki_id) AS kiad_actual_ytd,
              	a.ki_name,
              	a.ki_uom,
              	a.ki_year_date,
              	a.ki_parent_id,
              	a.ki_weight,
              	a.ki_target,
              	a.ki_target_ytd,
              	b.kf_name,
              	b.fa_function_name,
                c.kid_id,
               c.kid_date,
               c.kid_target,
               c.kid_target_ytd,
               c.kid_weight,
               c.kid_limit,
               c.kid_islock,
               c.kid_author
              FROM
              	kpi_item a
              JOIN kpi_formula b ON a.kf_id = b.kf_id
              LEFT JOIN kpi_item_detail c ON a.ki_id = c.ki_id
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, c.kid_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
              	MONTH,
              	CONVERT (DATE, c.kid_date, 20)
              ) = '".$month."'
              LEFT JOIN kpi_item_assignment d ON a.ki_id = d.ki_id
              LEFT JOIN kpi_item_assigment_detail e ON d.kia_id = e.kia_id
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, e.kiad_date, 20)
              ) = '".$nowYear."'
              AND DATEPART(
              	MONTH,
              	CONVERT (DATE, e.kiad_date, 20)
               ) = '".$month."'
              WHERE
              a.ki_id='".$id."'
              AND DATEPART(
              	YEAR,
              	CONVERT (DATE, a.ki_year_date, 20)
              ) = '".$nowYear."'
              GROUP BY
              	a.ki_id, 	a.ki_name,
              	a.ki_uom,
              	a.ki_year_date,
              	a.ki_parent_id,
              	a.ki_weight,
                a.ki_target,
              	a.ki_target_ytd,
              	b.kf_name,
              	b.fa_function_name,
                c.kid_id,
                 c.kid_date,
                 c.kid_target,
                 c.kid_target_ytd,
                 c.kid_weight,
                 c.kid_limit,
                 c.kid_islock,
                 c.kid_author
              ";
      // echo $sql; exit();
      $exec = $this->db_dev->query($sql);
      return $exec->result();
    }

}
