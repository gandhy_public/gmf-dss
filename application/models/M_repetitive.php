<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: ucup
 * Date: 4/26/18
 * Time: 5:25 PM
 */
class M_repetitive extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->dev_gmf = $this->load->database('dev_gmf', TRUE);
    }

    public function periode()
    {
        $periode = array(
            'start' => date('Y') . '-01' . '-01',
            'endMonth' => date('m'),
            'endYear' => date('Y')
        );

        return $periode;
    }

    public function acRegGA($data, $tipe)
    {
        if ($data == '') {
            $acRegs = $this->dev_gmf->select('acreg')
                ->from('m_acreg')
                ->where('own', $tipe)
                ->get()
                ->result_array();
        } else {
            $acType = $this->dev_gmf->select('actype_id')
                ->from('m_actype')
                ->where('actype_id', $data)
                ->get()->row();

            $acRegs = $this->dev_gmf->select('acreg')
                ->from('m_acreg')
                ->where('own', $tipe)
                ->where('actype_id', $acType->actype_id)
                ->get()
                ->result_array();
        }

        return $acRegs;
    }

    public function acRegCitilink($data)
    {
        if ($data == '') {
            $acRegs = $this->dev_gmf->select('acreg')
                ->from('m_acreg')
                ->where('own', 'CITILINK')
                ->get()
                ->result_array();
        } else {
            $acType = $this->dev_gmf->select('actype_id')
                ->from('m_actype')
                ->where('actype_id', $data)
                ->get()->row();
            $acRegs = $this->dev_gmf->select('acreg')
                ->from('m_acreg')
                ->where('own', 'CITILINK')
                ->where('actype_id', $acType)
                ->get()
                ->result_array();
        }

        return $acRegs;
    }

    public function getAllData($status)
    {
        if ($status == 'GA') {
            //array1Dimensional
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'GA')));
            foreach ($it as $v) {
                array_push($finalData, $v);
            }

        } else {
            //array1Dimensional
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'CITILINK'))); //acRegCitilink('')
            foreach ($it as $v) {
                array_push($finalData, $v);
            }
        }

        $allDatas = $this->dev_gmf->select("*")
            ->from('sap_repetitive_d2')
            ->where_in('functional_location', $finalData)
            ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
            ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
            ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
            ->get()->result_array();


        return $allDatas;
    }

    public function getDataGaruda()
    {
        //array1Dimensional
        $finalData = array();
        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'GA')));
        foreach ($it as $v) {
            array_push($finalData, $v);
        }

        $garudas = $this->dev_gmf->select('notification')
            ->from('repetitive_d5_final')
            ->where_in('functional_location', $finalData)
            ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
            ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
            ->group_by('notification')
            ->get()->result_array();

        return $garudas;

    }

    public function getDataGarudaPeriode()
    {
        //array1Dimensional
        $finalData = array();
        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'GA')));
        foreach ($it as $v) {
            array_push($finalData, $v);
        }

        $garudas = $this->dev_gmf->select('notification')
            ->from('repetitive_d5_final')
            ->where_in('functional_location', $finalData)
            ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
            ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
            ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
            ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
            ->distinct()
            ->get()->result_array();

        return $garudas;

    }

    public function getDataCitilink()
    {

        //array1Dimensional
        $finalData = array();
        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegCitilink('')));
        foreach ($it as $v) {
            array_push($finalData, $v);
        }

        $citilinks = $this->dev_gmf->select('notification')
            ->from('repetitive_d5_final')
            ->where_in('functional_location', $finalData)
            ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
            ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
            ->group_by('notification')
            ->get()->result_array();

        return $citilinks;
    }

    public function getDataCitilinkPeriode()
    {

        //array1Dimensional
        $finalData = array();
        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegCitilink('')));
        foreach ($it as $v) {
            array_push($finalData, $v);
        }

        $citilinks = $this->dev_gmf->select('*')
            ->from('repetitive_d5_final')
            ->where_in('functional_location', $finalData)
            ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
            ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
            ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
            ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
            ->distinct()
            ->get()->result_array();

        return $citilinks;
    }



    public function getTarget()
    {
        $getTarget = $this->db->select('target_val')
            ->from('tbl_all_target')
            ->where('target_type', 'Repetitive')
            ->where('target_year', date('Y'))
            ->get()->row();

        return $getTarget->target_val;
    }

    public function getNewActual($year){
        $sql="SELECT *, round( cast(A.opened as float) / cast(A.all_data as float), 2 )as ratio FROM (
                SELECT DISTINCT AT.actype, AR.OWN, count(DISTINCT c.notification) as opened, COUNT(DISTINCT df.notification) as all_data
                FROM
                    [dbo].[repetitive_d5_final] DF
                    JOIN m_acreg AR ON DF.functional_location = AR.acreg
                    JOIN m_actype AT ON AR.actype_id = AT.actype_id 
                    LEFT JOIN (SELECT notification FROM repetitive_d5_final WHERE system_status IN ('open','monitor','reminder')) c
                        ON c.notification = DF.notification
                WHERE   YEAR(CONVERT(date, DF.created_on, 104)) = '$year'
                GROUP BY
                    AT.actype, AR.own
                ) A";
        $query=$this->dev_gmf->query($sql);
        return $query->result_array();
    }

    public function showDatatable($data, $status)
    {
        if ($status == 'Garuda') {
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'GA')));
            foreach ($it as $v) {
                array_push($finalData, $v);
            }
        } elseif ($status == 'Citilink') {
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA('', 'CITILINK')));
            foreach ($it as $v) {
                array_push($finalData, $v);
            }
        }

//         $this->dev_gmf->select('*');
//         $this->dev_gmf->from('repetitive_d5_final');
//         $this->dev_gmf->where_in('functional_location', $finalData);
// //        $this->dev_gmf->where("CONVERT(date, created_on, 104) >=", $this->periode()['start']);
// //        $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth']);
// //        $this->dev_gmf->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear']);
//         $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'));
//         $this->dev_gmf->join('m_acreg', 'repetitive_d5_final.functional_location = m_acreg.acreg', 'left');
//         $this->dev_gmf->join('m_actype', 'm_actype.actype_id = m_acreg.actype_id', 'left');
//         $this->dev_gmf->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
$inf=implode("','",$finalData);
        // echo $inf;exit;
        $bln=date("m");
        $years=date("Y");
        $columns=array('jml','notification','actype','functional_location','description','created_on','req_start','system_status');
        $sql="SELECT * FROM (SELECT notification,max(id_repetitive_d5)as id FROM [dbo].[repetitive_d5_final]
                GROUP BY notification) a
                JOIN repetitive_d5_final b ON b.id_repetitive_d5=a.id AND b.notification= a.notification
        LEFT JOIN m_acreg ON b.functional_location = m_acreg.acreg
        LEFT JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
        WHERE functional_location IN('$inf')
        AND MONTH(CONVERT(date, created_on, 104)) = '$bln'
        AND YEAR(CONVERT(date, created_on, 104)) = ' $years'
        AND (system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')
        AND (m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')";
        if ($data["search"] != "") {
            $sql.= " AND (b.system_status like '%".$data['search']."%' or 
                          b.notification like '%".$data['search']."%'  or
                          m_actype.actype like '%".$data['search']."%' or 
                          m_acreg.acreg like '%".$data['search']."%' OR 
                          b.description like '%".$data['search']."%' OR 
                          b.created_on like '%".$data['search']."%' OR 
                          b.date_all like '%".$data['search']."%' ) ";
        }

        $sqls="SELECT * FROM (SELECT  ROW_NUMBER() OVER (ORDER BY id) jml ,b.*,m_acreg.actype_id,m_actype.actype FROM (SELECT notification,max(id_repetitive_d5)as id FROM [dbo].[repetitive_d5_final]
                GROUP BY notification) a
                JOIN repetitive_d5_final b ON b.id_repetitive_d5=a.id AND b.notification= a.notification
        LEFT JOIN m_acreg ON b.functional_location = m_acreg.acreg
        LEFT JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
        WHERE functional_location IN('$inf')
        AND MONTH(CONVERT(date, created_on, 104)) = '$bln'
        AND YEAR(CONVERT(date, created_on, 104)) = ' $years'
        AND (system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')
        AND (m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA') ";
        if ($data["search"] != "") {
            $sqls.= " AND (b.system_status like '%".$data['search']."%' or 
                          b.notification like '%".$data['search']."%'  or
                          m_actype.actype like '%".$data['search']."%' or 
                          m_acreg.acreg like '%".$data['search']."%' OR 
                          b.description like '%".$data['search']."%' OR 
                          b.created_on like '%".$data['search']."%' OR 
                          b.date_all like '%".$data['search']."%' ) ";
        }
        $sqls.=") G";
        $z=$_POST['order'][0]['column'];
        $za=$_POST['order'][0]['dir'];
        
       
        // $this->dev_gmf->select('notification,max(id_repetitive_d5)');
        // $this->dev_gmf->from('repetitive_d5_finals');
        // $this->dev_gmf->where_in('functional_location', $finalData);
        // $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) =", $month);
        // $this->dev_gmf->where("YEAR(CONVERT(date, created_on, 104)) =", $year);
        // $this->dev_gmf->join('m_acreg', 'repetitive_d5_final.functional_location = m_acreg.acreg', 'left');
        // $this->dev_gmf->join('m_actype', 'm_actype.actype_id = m_acreg.actype_id', 'left');
        // $this->dev_gmf->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
        // $this->dev_gmf->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        // $this->dev_gmf->group_by('notification');
        // $this->dev_gmf->group_by('id_repetitive_d5');
        $y=$data['start']+$data['length'] ;
        $x=$data['start'] ;
        $sqls.=" WHERE jml <= $y AND jml > $x ";
        
        $sqls.=" ORDER BY $columns[$z] $za ";
        
        

        $query1 = $this->dev_gmf->query($sql);    
        $total = $query1->num_rows();
       

        $output = array();
        $output["draw"] = $data["draw"];
        $output["recordsTotal"] = $output["recordsFiltered"] = $total;
        $output["data"] = array();
//         if ($data["search"] != "") {
//             $this->dev_gmf->like('actype', $data["search"])
//                 ->where_in('functional_location', $finalData)
// //                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
// //                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
// //                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
//                 ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
//                 ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
//             $this->dev_gmf->or_like('functional_location', $data["search"])
//                 ->where_in('functional_location', $finalData)
// //                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
// //                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
// //                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
//                 ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
//                 ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
//             $this->dev_gmf->or_like('description', $data["search"])
//                 ->where_in('functional_location', $finalData)
// //                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
// //                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
// //                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
//                 ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
//                 ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
//             $this->dev_gmf->or_like('system_status', $data["search"])
//                 ->where_in('functional_location', $finalData)
// //                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
// //                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
// //                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
//                 ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
//                 ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
//         }

//         $this->dev_gmf->select('*');
//         $this->dev_gmf->from('repetitive_d5_final');
//         $this->dev_gmf->where_in('functional_location', $finalData);
// //        $this->dev_gmf->where("CONVERT(date, created_on, 104) >=", $this->periode()['start']);
// //        $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth']);
// //        $this->dev_gmf->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear']);
//         $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'));
//         $this->dev_gmf->join('m_acreg', 'repetitive_d5_final.functional_location = m_acreg.acreg', 'left');
//         $this->dev_gmf->join('m_actype', 'm_actype.actype_id = m_acreg.actype_id', 'left');
//         $this->dev_gmf->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
//         $this->dev_gmf->limit($data["length"], $data["start"]);
//         $this->dev_gmf->order_by($data["order_column"], $data["order_dir"]);
//         if ($data["search"] != "") {
//             $this->dev_gmf->like('actype', $data["search"])
//                 ->where_in('functional_location', $finalData)
// //                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
// //                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
// //                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
//                 ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
//                 ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
//             $this->dev_gmf->or_like('functional_location', $data["search"])
//                 ->where_in('functional_location', $finalData)
// //                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
// //                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
// //                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
//                 ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
//                 ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
//             $this->dev_gmf->or_like('description', $data["search"])
//                 ->where_in('functional_location', $finalData)
// //                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
// //                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
// //                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
//                 ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
//                 ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
//             $this->dev_gmf->or_like('system_status', $data["search"])
//                 ->where_in('functional_location', $finalData)
// //                ->where("CONVERT(date, created_on, 104) >=", $this->periode()['start'])
// //                ->where("MONTH(CONVERT(date, created_on, 104)) <=", $this->periode()['endMonth'])
// //                ->where("YEAR(CONVERT(date, created_on, 104)) <=", $this->periode()['endYear'])
//                 ->where("MONTH(CONVERT(date, created_on, 104)) =", date('m'))
//                 ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);


//             $query = $this->dev_gmf->get();
//             $output["recordsTotal"] = $output["recordsFiltered"] = $query->num_rows();
//         } else {
//             $query = $this->dev_gmf->get();
//         }
        $query=$this->dev_gmf->query($sqls);
        $tblHaveMitigations = $this->dev_gmf->select('mitigasi_fk_id')
            ->from('tbl_mitigasi')
            ->where('mitigasi_type', 'repetitive')
            ->get()->result_array();
        $convertArrays = new RecursiveIteratorIterator(new RecursiveArrayIterator($tblHaveMitigations));
        $tblHaveMitigation = array();
        foreach ($convertArrays as $convertArray) {
            array_push($tblHaveMitigation, $convertArray);
        }
        $nomor_urut = $data['start'] + 1;
        foreach ($query->result_array() as $data) {
            if (in_array((int)$data['notification'], $tblHaveMitigation)) {
                $haveMitigation = 1;
            } else {
                $haveMitigation = 0;
            }

            // if (strpos($data['system_status'], 'open') !== false or strpos($data['system_status'], 'monitor') !== false) {
            //     $data['system_status'] = 'reminder';
            // }

            $output['data'][] = array(
                $nomor_urut,
                $data['notification'],
                $data['actype'],
                $data['functional_location'],
                $data['description'],
                date('d-m-Y', strtotime($data['created_on'])),
                date('d-m-Y', strtotime($data['req_start'])),
                $data['system_status'],
                $haveMitigation
            );
            $nomor_urut++;
        }
        echo json_encode($output, JSON_PRETTY_PRINT);
    }

    public function getDataChart($status, $input)
    {
        if ($input['startPeriode'] == '' and $input['endPeriode'] == '') {
            $year = date('Y');
            $monthFrom = $year . '-01';
            $monthTo = date('Y-m');
            $listAircraftType = $input['list_aircraft_type'];
        } else {
            $monthFrom = $input['startPeriode'];
            $monthTo = $input['endPeriode'];
            $listAircraftType = $input['list_aircraft_type'];
        }

        $start = (new DateTime($monthFrom))->modify('first day of this month');
        $end = (new DateTime($monthTo))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        $data = array();

        foreach ($period as $dt) {

            //array1Dimensional
            if ($status == 'Garuda') {
                $final = array();
                $its = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA($listAircraftType, 'GA')));
                foreach ($its as $v) {
                    array_push($final, $v);
                }
            } elseif ($status == 'Citilink') {
                $final = array();
                $its = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA($listAircraftType, 'CITILINK')));
                foreach ($its as $v) {
                    array_push($final, $v);
                }
            }

            $query = $this->dev_gmf->select('notification')
                ->from('repetitive_d5_final')
                ->where_in('functional_location', $final)
                ->where("MONTH(CONVERT(date, created_on, 104)) >=", date('m', strtotime($dt->format('Y-m') . '-01')))
                ->where("YEAR(CONVERT(date, created_on, 104)) >=", date('Y', strtotime($dt->format('Y-m') . '-01')))
                ->where("MONTH(CONVERT(date, created_on, 104)) <=", date('m', strtotime($dt->format('Y-m') . '-01')))
                ->where("YEAR(CONVERT(date, created_on, 104)) <=", date('Y', strtotime($dt->format('Y-m') . '-01')))
                ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
                ->group_by('notification')
                ->get()->result_array();
            // print_r($this->dev_gmf->last_query());
            // exit();
            array_push($data, count($query));
        }

        return $data;
    }

    public function showDatatableDetail($data, $aircraft, $month, $year, $input)
    {

        if ($aircraft == 'Garuda') {
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA($input['list_aircraft_type'], 'GA')));
            foreach ($it as $v) {
                array_push($finalData, $v);
            }
        } elseif ($aircraft == 'Citilink') {
            $finalData = array();
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->acRegGA($input['list_aircraft_type'], 'CITILINK')));
            foreach ($it as $v) {
                array_push($finalData, $v);
            }
        }

        $inf=implode("','",$finalData);
        // echo $inf;exit;
        $columns=array('jml','notification','actype','functional_location','description','created_on','req_start','system_status');
        $sql="SELECT * FROM (SELECT notification,max(id_repetitive_d5)as id FROM [dbo].[repetitive_d5_final] WHERE functional_location IN('$inf')
        AND MONTH(CONVERT(date, created_on, 104)) = '$month'
        AND YEAR(CONVERT(date, created_on, 104)) = '$year'
                GROUP BY notification) a
                JOIN repetitive_d5_final b ON b.id_repetitive_d5=a.id AND b.notification= a.notification
        LEFT JOIN m_acreg ON b.functional_location = m_acreg.acreg
        LEFT JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
        WHERE functional_location IN('$inf')
        AND MONTH(CONVERT(date, created_on, 104)) = '$month'
        AND YEAR(CONVERT(date, created_on, 104)) = '$year'
        AND (system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')
        AND (m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')";
        if ($data["search"] != "") {
            $sql.= " AND (b.system_status like '%".$data['search']."%' or 
                          b.notification like '%".$data['search']."%'  or
                          m_actype.actype like '%".$data['search']."%' or 
                          m_acreg.acreg like '%".$data['search']."%' OR 
                          b.description like '%".$data['search']."%' OR 
                          b.created_on like '%".$data['search']."%' OR 
                          b.date_all like '%".$data['search']."%' ) ";
        }

        $sqls="SELECT * FROM (SELECT  ROW_NUMBER() OVER (ORDER BY id) jml ,b.*,m_acreg.actype_id,m_actype.actype FROM (SELECT notification,max(id_repetitive_d5)as id FROM [dbo].[repetitive_d5_final]
        WHERE functional_location IN('$inf')
        AND MONTH(CONVERT(date, created_on, 104)) = '$month'
        AND YEAR(CONVERT(date, created_on, 104)) = '$year'
                GROUP BY notification) a
                JOIN repetitive_d5_final b ON b.id_repetitive_d5=a.id AND b.notification= a.notification
        LEFT JOIN m_acreg ON b.functional_location = m_acreg.acreg
        LEFT JOIN m_actype ON m_actype.actype_id = m_acreg.actype_id
        WHERE functional_location IN('$inf')
        AND MONTH(CONVERT(date, created_on, 104)) = '$month'
        AND YEAR(CONVERT(date, created_on, 104)) = '$year'
        AND (system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')
        AND (m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA') ";
        if ($data["search"] != "") {
            $sqls.= " AND (b.system_status like '%".$data['search']."%' or 
                          b.notification like '%".$data['search']."%'  or
                          m_actype.actype like '%".$data['search']."%' or 
                          m_acreg.acreg like '%".$data['search']."%' OR 
                          b.description like '%".$data['search']."%' OR 
                          b.created_on like '%".$data['search']."%' OR 
                          b.date_all like '%".$data['search']."%' ) ";
        }
        $sqls.=") G";
        $z=$_POST['order'][0]['column'];
        $za=$_POST['order'][0]['dir'];
        
       
        // $this->dev_gmf->select('notification,max(id_repetitive_d5)');
        // $this->dev_gmf->from('repetitive_d5_finals');
        // $this->dev_gmf->where_in('functional_location', $finalData);
        // $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) =", $month);
        // $this->dev_gmf->where("YEAR(CONVERT(date, created_on, 104)) =", $year);
        // $this->dev_gmf->join('m_acreg', 'repetitive_d5_final.functional_location = m_acreg.acreg', 'left');
        // $this->dev_gmf->join('m_actype', 'm_actype.actype_id = m_acreg.actype_id', 'left');
        // $this->dev_gmf->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
        // $this->dev_gmf->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        // $this->dev_gmf->group_by('notification');
        // $this->dev_gmf->group_by('id_repetitive_d5');
        $y=$data['start']+$data['length'] ;
        $x=$data['start'] ;
        $sqls.=" WHERE jml <= $y AND jml > $x ";
        
        $sqls.=" ORDER BY $columns[$z] $za ";


        $query1 = $this->dev_gmf->query($sql);
        $total = $query1->num_rows();

        $output = array();
        $output["draw"] = $data["draw"];
        $output["recordsTotal"] = $output["recordsFiltered"] = $total;
        $output["data"] = array();
        // if ($data["search"] != "") {
        //     $this->dev_gmf->like('actype', $data["search"])
        //         ->where_in('functional_location', $finalData)
        //         ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
        //         ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
        //         ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
        //         ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        //     $this->dev_gmf->or_like('functional_location', $data["search"])
        //         ->where_in('functional_location', $finalData)
        //         ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
        //         ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
        //         ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
        //         ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        //     $this->dev_gmf->or_like('description', $data["search"])
        //         ->where_in('functional_location', $finalData)
        //         ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
        //         ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
        //         ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
        //         ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        //     $this->dev_gmf->or_like('system_status', $data["search"])
        //         ->where_in('functional_location', $finalData)
        //         ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
        //         ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
        //         ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
        //         ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        // }

        // $this->dev_gmf->select('*');
        // $this->dev_gmf->from('repetitive_d5_final');
        // $this->dev_gmf->where_in('functional_location', $finalData);
        // $this->dev_gmf->where("MONTH(CONVERT(date, created_on, 104)) =", $month);
        // $this->dev_gmf->where("YEAR(CONVERT(date, created_on, 104)) =", $year);
        // $this->dev_gmf->join('m_acreg', 'repetitive_d5_final.functional_location = m_acreg.acreg', 'left');
        // $this->dev_gmf->join('m_actype', 'm_actype.actype_id = m_acreg.actype_id', 'left');
        // $this->dev_gmf->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE);
        // $this->dev_gmf->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        // $this->dev_gmf->limit($data["length"], $data["start"]);
        // $this->dev_gmf->order_by($data["order_column"], $data["order_dir"]);
        // if ($data["search"] != "") {
        //     $this->dev_gmf->like('actype', $data["search"])
        //         ->where_in('functional_location', $finalData)
        //         ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
        //         ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
        //         ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
        //         ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        //     $this->dev_gmf->or_like('functional_location', $data["search"])
        //         ->where_in('functional_location', $finalData)
        //         ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
        //         ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
        //         ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
        //         ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        //     $this->dev_gmf->or_like('description', $data["search"])
        //         ->where_in('functional_location', $finalData)
        //         ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
        //         ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
        //         ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
        //         ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        //     $this->dev_gmf->or_like('system_status', $data["search"])
        //         ->where_in('functional_location', $finalData)
        //         ->where("MONTH(CONVERT(date, created_on, 104)) =", $month)
        //         ->where("YEAR(CONVERT(date, created_on, 104)) =", $year)
        //         ->where("(system_status ='open' OR system_status = 'monitor' OR system_status = 'reminder')", NULL, FALSE)
        //         ->where("(m_acreg.own = 'CITILINK' OR m_acreg.own = 'GA')", NULL, FALSE);
        //     $query = $this->dev_gmf->query($sql);
        //     $output["recordsTotal"] = $output["recordsFiltered"] = $query->num_rows();
        // } else {
        //     $query = $this->dev_gmf->query($sql);
        // }
        $query=$this->dev_gmf->query($sqls);
                    //   ->limit($data["length"], $data["start"]);
        // $query = $this->dev_gmf->get();
        $tblHaveMitigations = $this->dev_gmf->select('mitigasi_fk_id')
            ->from('tbl_mitigasi')
            ->where('mitigasi_type', 'repetitive')
            ->get()->result_array();
        $convertArrays = new RecursiveIteratorIterator(new RecursiveArrayIterator($tblHaveMitigations));
        $tblHaveMitigation = array();
        foreach ($convertArrays as $convertArray) {
            array_push($tblHaveMitigation, $convertArray);
        }
        $nomor_urut = $data['start'] + 1;

        foreach ($query->result_array() as $data) {
            if (in_array((int)$data['notification'], $tblHaveMitigation)) {
                $haveMitigation = 1;
            } else {
                $haveMitigation = 0;
            }

            // if (strpos($data['system_status'], 'open') !== false or strpos($data['system_status'], 'monitor') !== false) {
            //     $data['system_status'] = 'reminder';
            // }

            $output['data'][] = array(
                $nomor_urut,
                $data['notification'],
                $data['actype'],
                $data['functional_location'],
                $data['description'],
                date('d-m-Y', strtotime($data['created_on'])),
                date('d-m-Y', strtotime($data['req_start'])),
                $data['system_status'],
                $haveMitigation
            );
            $nomor_urut++;
        }

        echo json_encode($output, JSON_PRETTY_PRINT);
    }

    public function getMitigation($id)
    {
        $this->dev_gmf->select('*');
        $this->dev_gmf->from('tbl_mitigasi');
        $this->dev_gmf->where('mitigasi_fk_id', $id);
        $this->dev_gmf->where('mitigasi_type', 'repetitive');
        $query = $this->dev_gmf->get();
        return $query->result_array();
    }

    public function getNameMonth($input)
    {
        if ($input['startPeriode'] == '' and $input['endPeriode'] == '') {
            $year = date('Y');
            $monthFrom = $year . '-01';
            $monthTo = date('Y-m');
        } else {
            $monthFrom = $input['startPeriode'];
            $monthTo = $input['endPeriode'];
        }

        $start = (new DateTime($monthFrom))->modify('first day of this month');
        $end = (new DateTime($monthTo))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $monthNames = array();
        foreach ($period as $dt) {
            $monthName = $dt->format('M');

            array_push($monthNames, $monthName);
        }
        return $monthNames;
    }
}
