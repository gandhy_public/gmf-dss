<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_cotd_dashboard extends CI_Model
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        //$this->db_dboard = $this->load->database('db_dboard', TRUE);
        // $this->db_mcdr = $this->load->database('db_mcdr', TRUE);
        //$this->db_mrosystem = $this->load->database('db_mrosystem', TRUE);
        $this->dev_gmf = $this->load->database('dev_gmf', TRUE);


    }

    public function get_target($tahun, $type)
    {
        //db
        if ($type == 'garuda') {
            $type = 'PK';
        } else {
            $type = 'QG';
        }
        $query = $this->db->query("
				SELECT target_val as target_year from tbl_all_target where target_type='COTD' and target_year = '{$tahun}' and target_aircraft='{$type}'");
        $query = $query->row_array();

        return $query;
    }


    public function get_detail_actype($date, $reg)
    {
        //db_dboard
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and AC_REG in ({$reg}) ";
        }
        $awal = explode("-", $date);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $query = $this->dev_gmf->query("
				select AC_TYPE from TBL_FLT_SABRE where MONTH(ARR_DATE_ACT_LOCAL) = '{$awal_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) = '{$awal_tahun}' {$reg}
				GROUP BY AC_TYPE, MONTH(ARR_DATE_ACT_LOCAL), YEAR(ARR_DATE_ACT_LOCAL)");
        $query = $query->result_array();

        // echo $this->db->last_query();
        return $query;
    }


    function get_garuda_grafik($bulan, $tahun, $reg)
    {
        //db_dboard
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and AC_REG in ({$reg}) ";
        }
        $query = $this->dev_gmf->query("SELECT MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) as bulan, YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) AS tahun,
			sum(case when TECH_DELAY = 'Y' then 1 else null end) TOTALTECH,
			sum(case when CODE_DELAY != '' then 1 else null end) TOTALDELAY,
			sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else null end) FLIGHTDELAY,
			sum(case when LEG_STATE != 'Cancelled' then 1 else null end) TOTALDEPARTURE,
			((CAST(sum(case when TECH_DELAY = 'Y' then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when CODE_DELAY != '' then 1 else 0 end) AS FLOAT), 0))*
			(CAST(sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when LEG_STATE != 'Cancelled' then 1 else 0 end) AS FLOAT), 0))) as ACTUAL_GA
		FROM TBL_FLT_SABRE
		WHERE MONTH(DEP_DATE_ACT_UTC) = '{$bulan}' and YEAR(DEP_DATE_ACT_UTC) = '{$tahun}' {$reg}
		GROUP BY 
			MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC), YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC)
		HAVING 
			((CAST(sum(case when TECH_DELAY = 'Y' then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when CODE_DELAY != '' then 1 else 0 end) AS FLOAT), 0))*
			(CAST(sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when LEG_STATE != 'Cancelled' then 1 else 0 end) AS FLOAT), 0))) != 0");
        return $query->row_array();
    }

    function get_garuda($start, $end, $reg)
    {
        //db_dboard
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and AC_REG in ({$reg}) ";
        }
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $akhir = explode("-", $end);
        $akhir_bulan = $akhir[0];
        $akhir_tahun = $akhir[1];

        // $query = $this->db_dboard->query("select (
        // SELECT COUNT(*) as totaltech from TBL_FLT_SABRE where LEG_STATE != 'Cancelled' and TECH_DELAY = 'Y'
        // and MONTH(ARR_DATE_ACT_LOCAL) >= '{$awal_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) >= '{$awal_tahun}' and  MONTH(ARR_DATE_ACT_LOCAL) <= '{$akhir_bulan}'
        // and YEAR(ARR_DATE_ACT_LOCAL) <= '{$akhir_tahun}' {$reg} GROUP BY MONTH(ARR_DATE_ACT_LOCAL), YEAR(ARR_DATE_ACT_LOCAL)
        // ) as TOTALTECH, (
        // SELECT COUNT(*) AS totaldelay from TBL_FLT_SABRE where LEG_STATE != 'Cancelled' and CODE_DELAY != '' and MONTH(ARR_DATE_ACT_LOCAL) >= '{$awal_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) >= '{$awal_tahun}' and  MONTH(ARR_DATE_ACT_LOCAL) <= '{$akhir_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) <= '{$akhir_tahun}' {$reg}
        // GROUP BY MONTH(ARR_DATE_ACT_LOCAL), YEAR(ARR_DATE_ACT_LOCAL)
        // )AS TOTALDELAY,(
        // select sum(t.delay) as flightdelay from (
        // select CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) as delay, MONTH(ARR_DATE_ACT_LOCAL) as bulan, YEAR(ARR_DATE_ACT_LOCAL) as tahun
        // from TBL_FLT_SABRE
        // WHERE LEG_STATE != 'Cancelled' AND CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15 and  MONTH(ARR_DATE_ACT_LOCAL) >= '{$awal_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) >= '{$awal_tahun}' and  MONTH(ARR_DATE_ACT_LOCAL) <= '{$akhir_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) <= '{$akhir_tahun}' {$reg}  )
        // t GROUP BY t.bulan, t.tahun
        // )AS FLIGHTDELAY,(
        // SELECT COUNT(*) AS totaldeparture from TBL_FLT_SABRE where LEG_STATE != 'Cancelled' and MONTH(ARR_DATE_ACT_LOCAL) >= '{$awal_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) >= '{$awal_tahun}' and  MONTH(ARR_DATE_ACT_LOCAL) <= '{$akhir_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) <= '{$akhir_tahun}' {$reg}
        // GROUP BY MONTH(ARR_DATE_ACT_LOCAL), YEAR(ARR_DATE_ACT_LOCAL)
        // )AS TOTALDEPARTURE, t.bulan, t.tahun from
        // (
        // SELECT MONTH(ARR_DATE_ACT_LOCAL) as bulan, YEAR(ARR_DATE_ACT_LOCAL) as tahun	 from TBL_FLT_SABRE where LEG_STATE != 'Cancelled' and CODE_DELAY != '' and MONTH(ARR_DATE_ACT_LOCAL) >= '{$awal_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) >= '{$awal_tahun}' and  MONTH(ARR_DATE_ACT_LOCAL) <= '{$akhir_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) <= '{$akhir_tahun}'
        // GROUP BY MONTH(ARR_DATE_ACT_LOCAL), YEAR(ARR_DATE_ACT_LOCAL)
        // )AS t");
        // echo $this->db_dboard->last_query($query);
        $query = $this->dev_gmf->query("SELECT MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) as bulan, YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) AS tahun,
			sum(case when TECH_DELAY = 'Y' then 1 else null end) TOTALTECH,
			sum(case when CODE_DELAY != '' then 1 else null end) TOTALDELAY,
			sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else null end) FLIGHTDELAY,
			sum(case when LEG_STATE != 'Cancelled' then 1 else null end) TOTALDEPARTURE,
			((CAST(sum(case when TECH_DELAY = 'Y' then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when CODE_DELAY != '' then 1 else 0 end) AS FLOAT), 0))*
			(CAST(sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when LEG_STATE != 'Cancelled' then 1 else 0 end) AS FLOAT), 0))) as ACTUAL_GA
		FROM TBL_FLT_SABRE
		WHERE MONTH(DEP_DATE_ACT_UTC) >= '{$awal_bulan}' and YEAR(DEP_DATE_ACT_UTC) >= '{$awal_tahun}' and  MONTH(DEP_DATE_ACT_UTC) <= '{$akhir_bulan}' and YEAR(DEP_DATE_ACT_UTC) <= '{$akhir_tahun}' {$reg}
		GROUP BY 
			MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC), YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC)
		HAVING 
			((CAST(sum(case when TECH_DELAY = 'Y' then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when CODE_DELAY != '' then 1 else 0 end) AS FLOAT), 0))*
			(CAST(sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when LEG_STATE != 'Cancelled' then 1 else 0 end) AS FLOAT), 0))) != 0");
        return $query->result_array();
    }


    function get_detail_garuda($start, $reg)
    {
        //db_dboard
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and AC_REG in ({$reg}) ";
        }

        // $query = $this->dev_gmf->query("select (
        // SELECT COUNT(AC_TYPE) as totaltech from TBL_FLT_SABRE where LEG_STATE != 'Cancelled' and TECH_DELAY = 'Y'
        // and MONTH(ARR_DATE_ACT_LOCAL) = '{$awal_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) = '{$awal_tahun}' AND AC_TYPE ='{$type}' GROUP BY AC_TYPE, MONTH(ARR_DATE_ACT_LOCAL), YEAR(ARR_DATE_ACT_LOCAL)
        // ) as TOTALTECH, (
        // SELECT COUNT(AC_TYPE) AS totaldelay from TBL_FLT_SABRE where LEG_STATE != 'Cancelled' and CODE_DELAY != ''
        // and MONTH(ARR_DATE_ACT_LOCAL) = '{$awal_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) = '{$awal_tahun}'  AND AC_TYPE ='{$type}'
        // GROUP BY AC_TYPE, MONTH(ARR_DATE_ACT_LOCAL), YEAR(ARR_DATE_ACT_LOCAL)
        // )AS TOTALDELAY,(
        // select sum(t.delay) as flightdelay from (
        // select CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) as delay, MONTH(ARR_DATE_ACT_LOCAL) as bulan, YEAR(ARR_DATE_ACT_LOCAL) as tahun
        // from TBL_FLT_SABRE
        // WHERE LEG_STATE != 'Cancelled' AND CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15 and  MONTH(ARR_DATE_ACT_LOCAL) = '{$awal_bulan}'
        // and YEAR(ARR_DATE_ACT_LOCAL) = '{$awal_tahun}'
        // AND AC_TYPE ='{$type}')
        // t GROUP BY t.bulan, t.tahun
        // )AS FLIGHTDELAY,(
        // SELECT COUNT(AC_TYPE) AS totaldeparture from TBL_FLT_SABRE where LEG_STATE != 'Cancelled' and MONTH(ARR_DATE_ACT_LOCAL) = '{$awal_bulan}'
        // and YEAR(ARR_DATE_ACT_LOCAL) = '{$awal_tahun}' AND AC_TYPE ='{$type}'
        // GROUP BY AC_TYPE, MONTH(ARR_DATE_ACT_LOCAL), YEAR(ARR_DATE_ACT_LOCAL)
        // )AS TOTALDEPARTURE, t.bulan, t.tahun from
        // (
        // SELECT MONTH(ARR_DATE_ACT_LOCAL) as bulan, YEAR(ARR_DATE_ACT_LOCAL) as tahun	 from TBL_FLT_SABRE where LEG_STATE != 'Cancelled' and CODE_DELAY != ''
        // and MONTH(ARR_DATE_ACT_LOCAL) = '{$awal_bulan}' and YEAR(ARR_DATE_ACT_LOCAL) = '{$awal_tahun}'
        // GROUP BY MONTH(ARR_DATE_ACT_LOCAL), YEAR(ARR_DATE_ACT_LOCAL)
        // )AS t");

        $query = $this->dev_gmf->query("SELECT AC_TYPE, MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) as bulan, YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) AS tahun,
			sum(case when TECH_DELAY = 'Y' then 1 else null end) TOTALTECH,
			sum(case when CODE_DELAY != '' then 1 else null end) TOTALDELAY,
			sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else null end) FLIGHTDELAY,
			sum(case when LEG_STATE != 'Cancelled' then 1 else null end) TOTALDEPARTURE,
			((CAST(sum(case when TECH_DELAY = 'Y' then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when CODE_DELAY != '' then 1 else 0 end) AS FLOAT), 0))*
			(CAST(sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when LEG_STATE != 'Cancelled' then 1 else 0 end) AS FLOAT), 0))) as ACTUAL_GA
		FROM TBL_FLT_SABRE
		WHERE MONTH(DEP_DATE_ACT_UTC) = '{$awal_bulan}' and YEAR(DEP_DATE_ACT_UTC) = '{$awal_tahun}' {$reg}
		GROUP BY AC_TYPE,
			MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC), YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC)
		HAVING 
			((CAST(sum(case when TECH_DELAY = 'Y' then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when CODE_DELAY != '' then 1 else 0 end) AS FLOAT), 0))*
			(CAST(sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else 0 end) AS FLOAT) /
			NULLIF(CAST(sum(case when LEG_STATE != 'Cancelled' then 1 else 0 end) AS FLOAT), 0))) != 0");

        return $query->row_array();
    }


    public function get_citilink_all_grafik($bulan, $tahun, $reg)
    {
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and COL_AIRCRAFT_REGISTRATION in ({$reg}) ";
        }
        //db_mrosystem

        $query = $this->dev_gmf->query("
				SELECT COUNT(*) as totaldepar, MONTH(COL_PLAN_DEPARTURE_DATE) as bulan, YEAR(COL_PLAN_DEPARTURE_DATE) as  tahun from TBL_AC_MOVEMENT_PROD1 where COL_CARRIER_CODE like 'QG%' 
				and MONTH(COL_PLAN_DEPARTURE_DATE) = '{$bulan}' and YEAR(COL_PLAN_DEPARTURE_DATE) = '{$tahun}'{$reg} GROUP BY MONTH(COL_PLAN_DEPARTURE_DATE), YEAR(COL_PLAN_DEPARTURE_DATE)
			ORDER BY tahun ASC, bulan ASC	");
        $query = $query->row_array();


        return $query;
    }

    public function get_citilink_all($start, $end, $reg)
    {
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and COL_AIRCRAFT_REGISTRATION in ({$reg}) ";
        }
        //db_mrosystem
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $akhir = explode("-", $end);
        $akhir_bulan = $akhir[0];
        $akhir_tahun = $akhir[1];
        $query = $this->dev_gmf->query("
				SELECT COUNT(*) as totaldepar, MONTH(COL_PLAN_DEPARTURE_DATE) as bulan, YEAR(COL_PLAN_DEPARTURE_DATE) as  tahun from TBL_AC_MOVEMENT_PROD1 where COL_CARRIER_CODE like 'QG%' 
				and MONTH(COL_PLAN_DEPARTURE_DATE) >= '{$awal_bulan}' and YEAR(COL_PLAN_DEPARTURE_DATE) >= '{$awal_tahun}' and  MONTH(COL_PLAN_DEPARTURE_DATE) <= '{$akhir_bulan}' and YEAR(COL_PLAN_DEPARTURE_DATE) <= '{$akhir_tahun}' {$reg} GROUP BY MONTH(COL_PLAN_DEPARTURE_DATE), YEAR(COL_PLAN_DEPARTURE_DATE)
			ORDER BY tahun ASC, bulan ASC	");
        $query = $query->result_array();
        // echo $this->dev_gmf->last_query();


        return $query;
    }


    public function get_citilink_all_detail($start, $reg)
    {
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and COL_AIRCRAFT_REGISTRATION in ({$reg}) ";
        }
        //db_mrosystem
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        // $query = $this->dev_gmf->query("
        // SELECT COUNT(*) as totaldepar, MONTH(COL_PLAN_DEPARTURE_DATE) as bulan, YEAR(COL_PLAN_DEPARTURE_DATE) as  tahun from TBL_AC_MOVEMENT_PROD1 where COL_CARRIER_CODE like 'QG%'
        // and MONTH(COL_PLAN_DEPARTURE_DATE) = '{$awal_bulan}' and YEAR(COL_PLAN_DEPARTURE_DATE) = '{$awal_tahun}' GROUP BY MONTH(COL_PLAN_DEPARTURE_DATE), YEAR(COL_PLAN_DEPARTURE_DATE)
        // ORDER BY tahun ASC, bulan ASC	");
        // $query = $query->row_array();
        $query = $this->dev_gmf->query("
				SELECT COUNT(actype) as totaldepar, actype, MONTH(COL_PLAN_DEPARTURE_DATE) as bulan, YEAR(COL_PLAN_DEPARTURE_DATE) as tahun from TBL_AC_MOVEMENT_PROD1 
				LEFT JOIN m_acreg on m_acreg.acreg=TBL_AC_MOVEMENT_PROD1.COL_AIRCRAFT_REGISTRATION
				LEFT JOIN m_actype on m_actype.actype_id = m_acreg.actype_id
				where COL_CARRIER_CODE like 'QG%' and MONTH(COL_PLAN_DEPARTURE_DATE) = '{$awal_bulan}' and YEAR(COL_PLAN_DEPARTURE_DATE) = '{$awal_tahun}' {$reg} GROUP BY actype, MONTH(COL_PLAN_DEPARTURE_DATE), YEAR(COL_PLAN_DEPARTURE_DATE) ORDER BY tahun ASC, bulan ASC");
        $query = $query->result_array();
        // echo  $this->dev_gmf->last_query();

        return $query;
    }

    public function get_citilink_delay($bulan, $tahun, $reg)
    {
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and Reg in ({$reg}) ";
        }
        //db_mcdr
        // $awal = explode("-",$start);
        // $awal_bulan = $awal[0];
        // $awal_tahun = $awal[1];
        // $akhir = explode("-",$end);
        // $akhir_bulan = $akhir[0];
        // $akhir_tahun = $akhir[1];
        // $query = $this->dev_gmf->query("
        // SELECT COUNT(*) as totaltech, MONTH(DateEvent) as bulan, YEAR(DateEvent) as  tahun from tdamx where FlightNo like 'QG%'
        // and MONTH(DateEvent) >= '{$awal_bulan}' and YEAR(DateEvent) >= '{$awal_tahun}' and  MONTH(DateEvent) <= '{$akhir_bulan}' and YEAR(DateEvent) <= '{$akhir_tahun}' GROUP BY MONTH(DateEvent), YEAR(DateEvent)
        // ORDER BY tahun ASC, bulan ASC	");
        $query = $this->dev_gmf->query("
				SELECT COUNT(*) as totaltech, MONTH(DateEvent) as bulan, YEAR(DateEvent) as  tahun from tdamx where FlightNo like 'QG%' 
				and MONTH(DateEvent) = '{$bulan}' and YEAR(DateEvent) = '{$tahun}' {$reg} GROUP BY MONTH(DateEvent), YEAR(DateEvent)
				ORDER BY tahun ASC, bulan ASC	");
        $query = $query->row_array();

        return $query;
    }

    public function get_citilink_delay_detail($start, $reg)
    {
        //db_mcdr
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and tdamx.Reg in ({$reg}) ";
        }
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        // $query = $this->dev_gmf->query("
        // SELECT COUNT(*) as totaltech, MONTH(DateEvent) as bulan, YEAR(DateEvent) as  tahun from tdamx where FlightNo like 'QG%'
        // and MONTH(DateEvent) = '{$awal_bulan}' and YEAR(DateEvent) = '{$awal_tahun}' GROUP BY MONTH(DateEvent), YEAR(DateEvent)
        // ORDER BY tahun ASC, bulan ASC	");
        $query = $this->dev_gmf->query("
				SELECT count(m_actype.actype) totaltech, m_actype.actype, MONTH(DateEvent) as bulan, YEAR(DateEvent) as  tahun from tdamx
				LEFT JOIN m_acreg on m_acreg.acreg=tdamx.Reg
				LEFT JOIN m_actype on m_actype.actype_id = m_acreg.actype_id
				where FlightNo like 'QG%' 
				and MONTH(DateEvent) = '{$awal_bulan}' and YEAR(DateEvent) = '{$awal_tahun}' {$reg}
				GROUP BY m_actype.actype, MONTH(DateEvent), YEAR(DateEvent)
				ORDER BY tahun ASC, bulan ASC	");
        $query = $query->row_array();

        return $query;
    }


    public function get_pesawat($start, $end, $aircraft, $reg)
    {
        //db
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and Reg in ({$reg}) ";
        }
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $akhir = explode("-", $end);
        $akhir_bulan = $akhir[0];
        $akhir_tahun = $akhir[1];
        // if($aircraft=="Citilink"){
        // $pesawat = " AND ACtype='A320-200'";
        // }else{
        // $pesawat = " AND ACtype!='A320-200'";
        // }

        $query = $this->dev_gmf->query("SELECT ACtype FROM mcdrnew 
				WHERE YEAR(DateEvent)>='{$awal_tahun}' AND MONTH(DateEvent)>='{$awal_bulan}' AND YEAR(DateEvent)<='{$akhir_tahun}' AND MONTH(DateEvent)<='{$akhir_bulan}'
				AND DCP!='X' {$reg}
				GROUP BY ACtype");
        $query = $query->result_array();

        return $query;
    }

    public function get_pesawat_table($start, $end, $aircraft, $reg)
    {
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and Reg in ({$reg}) ";
        }
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $akhir = explode("-", $end);
        $akhir_bulan = $akhir[0];
        $akhir_tahun = $akhir[1];

        $query = $this->dev_gmf->query("SELECT DISTINCT  ACtype, Reg FROM mcdrnew 
				WHERE YEAR(DateEvent)>='{$awal_tahun}' AND MONTH(DateEvent)>='{$awal_bulan}' AND YEAR(DateEvent)<='{$akhir_tahun}' AND MONTH(DateEvent)<='{$akhir_bulan}'
				AND DCP!='X' {$reg}");
        $query = $query->result_array();

        return $query;
    }


    public function get_atatdm($start, $end, $aircraft, $reg)
    {
        //db
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and Reg in ({$reg}) ";
        }

        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $akhir = explode("-", $end);
        $akhir_bulan = $akhir[0];
        $akhir_tahun = $akhir[1];
        // if($aircraft=="Citilink"){
        // $pesawat = " AND ACtype='A320-200'";
        // }else{
        // $pesawat = " AND ACtype!='A320-200'";
        // }

        $query = $this->dev_gmf->query("SELECT ATAtdm FROM mcdrnew 
				WHERE  YEAR(DateEvent)>='{$awal_tahun}' AND MONTH(DateEvent)>='{$awal_bulan}' AND YEAR(DateEvent)<='{$akhir_tahun}' AND MONTH(DateEvent)<='{$akhir_bulan}'
				AND DCP!='X' {$reg}
				GROUP BY ATAtdm");
        $query = $query->result_array();

        return $query;
    }

    public function get_departure($start, $end, $aircraft, $reg)
    {
        //db
        if ($reg == '') {
            $reg = '';
        } else {
            $reg = " and Reg in ({$reg}) ";
        }
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $akhir = explode("-", $end);
        $akhir_bulan = $akhir[0];
        $akhir_tahun = $akhir[1];
        // if($aircraft=="Citilink"){
        // $pesawat = " AND ACtype='A320-200'";
        // }else{
        // $pesawat = " AND ACtype!='A320-200'";
        // }

        $query = $this->dev_gmf->query("
			SELECT DepSta FROM mcdrnew 
			WHERE YEAR(DateEvent)>='{$awal_tahun}' AND MONTH(DateEvent)>='{$awal_bulan}' AND YEAR(DateEvent)<='{$akhir_tahun}' AND MONTH(DateEvent)<='{$akhir_bulan}' 
			AND DCP!='X' {$reg}
			GROUP BY DepSta ORDER BY DepSta ASC");
        $query = $query->result_array();

        return $query;
    }

    public function get_cek_ata_total($type, $kategori, $start, $end, $aircraft)
    {
        //db
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $akhir = explode("-", $end);
        $akhir_bulan = $akhir[0];
        $akhir_tahun = $akhir[1];
        // if($aircraft=="Citilink"){
        // $pesawat = " AND ACtype='A320-200'";
        // }else{
        // $pesawat = " AND ACtype!='A320-200'";
        // }

        $query = $this->dev_gmf->query("SELECT ATAtdm, count(ATAtdm) as total, ACtype from mcdrnew 
				where  YEAR(DateEvent)>='{$awal_tahun}' AND MONTH(DateEvent)>='{$awal_bulan}' AND YEAR(DateEvent)<='{$akhir_tahun}' AND MONTH(DateEvent)<='{$akhir_bulan}'  And DCP!='X'  AND ACtype='{$type}' 
					And ATAtdm='{$kategori}'  
				group by ATAtdm, ACtype");
        $query = $query->row_array();

        return $query;
    }


    public function get_cek_ata_jam($type, $kategori, $start, $end, $aircraft)
    {
        //db
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $akhir = explode("-", $end);
        $akhir_bulan = $akhir[0];
        $akhir_tahun = $akhir[1];
        // if($aircraft=="Citilink"){
        // $pesawat = " AND ACtype='A320-200'";
        // }else{
        // $pesawat = " AND ACtype!='A320-200'";
        // }

        $query = $this->dev_gmf->query("SELECT ATAtdm, ACtype, HoursTot+(CAST(MinTot AS FLOAT)/60) as total from mcdrnew 
				where YEAR(DateEvent)>='{$awal_tahun}' AND MONTH(DateEvent)>='{$awal_bulan}' AND YEAR(DateEvent)<='{$akhir_tahun}' AND MONTH(DateEvent)<='{$akhir_bulan}' And DCP!='X'  AND ACtype='{$type}' 
					And ATAtdm='{$kategori}' 
				group by ATAtdm, ACtype, HoursTot+(CAST(MinTot AS FLOAT)/60)");
        $query = $query->row_array();

        return $query;
    }

    public function get_cek_depar_total($type, $kategori, $start, $end, $aircraft)
    {
        //db
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $akhir = explode("-", $end);
        $akhir_bulan = $akhir[0];
        $akhir_tahun = $akhir[1];
        // if($aircraft=="Citilink"){
        // $pesawat = " AND ACtype='A320-200'";
        // }else{
        // $pesawat = " AND ACtype!='A320-200'";
        // }

        $query = $this->dev_gmf->query("
					SELECT DepSta, count(DepSta) as total, ACtype  from mcdrnew 
					where  YEAR(DateEvent)>='{$awal_tahun}' AND MONTH(DateEvent)>='{$awal_bulan}' AND YEAR(DateEvent)<='{$akhir_tahun}' AND MONTH(DateEvent)<='{$akhir_bulan}'  
					And DCP!='X'  AND ACtype='{$type}' 
					And DepSta='{$kategori}'  
					group by DepSta, ACtype ORDER BY DepSta ASC");
        $query = $query->row_array();

        return $query;
    }

    public function get_cek_depar_jam($type, $kategori, $start, $end, $aircraft)
    {
        //db
        $awal = explode("-", $start);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $akhir = explode("-", $end);
        $akhir_bulan = $akhir[0];
        $akhir_tahun = $akhir[1];
        // if($aircraft=="Citilink"){
        // $pesawat = " AND ACtype='A320-200'";
        // }else{
        // $pesawat = " AND ACtype!='A320-200'";
        // }

        $query = $this->dev_gmf->query("
				SELECT DepSta, HoursTot+(CAST(MinTot AS FLOAT)/60) as total,  ACtype  from mcdrnew 
				where  YEAR(DateEvent)>='{$awal_tahun}' AND MONTH(DateEvent)>='{$awal_bulan}' AND YEAR(DateEvent)<='{$akhir_tahun}' AND MONTH(DateEvent)<='{$akhir_bulan}' 
				And DCP!='X' AND ACtype='{$type}' 
				And DepSta='{$kategori}'  
				group by DepSta, ACtype, HoursTot+(CAST(MinTot AS FLOAT)/60) ORDER BY DepSta ASC");
        // echo $this->dev_gmf->last_query();
        $query = $query->row_array();

        return $query;
    }


    public function get_mitigasi($tipe, $date)
    {
        //db
        $awal = explode("-", $date);
        $bulan = $awal[0];
        $bulan = str_pad($bulan, 2, "0", STR_PAD_LEFT);
        $tahun = $awal[1];
        $bulan_tahun = $tahun . '-' . $bulan;

        $query = $this->dev_gmf->query("
				select * from tbl_mitigasi where mitigasi_aircraft = '{$tipe}' and YEAR(mitigasi_date)='{$tahun}' AND MONTH(mitigasi_date)='{$bulan}' and mitigasi_type = 'cotd'");
        // echo  $this->dev_gmf->last_query();
        $query = $query->row_array();

        return $query;
    }


    public function get_actype()
    {
        //dev_gmf
        $query = $this->dev_gmf->query("
				SELECT b.actype, b.actype_id FROM m_acreg a
				JOIN m_actype b ON a.actype_id = b.actype_id
				WHERE own IN('CITILINK','GA')
				GROUP BY b.actype, b.actype_id");
        $query = $query->result_array();

        return $query;
    }

    public function get_reg($ac_type)
    {
        //dev_gmf
        $query = $this->dev_gmf->query("
				SELECT acreg, b.actype, b.actype_id FROM m_acreg a
				JOIN m_actype b ON a.actype_id = b.actype_id
				WHERE b.actype_id in ({$ac_type}) and a.own='GA'
				order by b.actype_id asc");
        // echo $this->db_dsssrv->last_query($query);
        // echo $this->dev_gmf->last_query();
        $query = $query->result_array();

        return $query;
    }

    public function get_reg_citilink($ac_type)
    {
        //dev_gmf
        $query = $this->dev_gmf->query("
				SELECT acreg, b.actype, b.actype_id FROM m_acreg a
				JOIN m_actype b ON a.actype_id = b.actype_id
				WHERE b.actype_id in ({$ac_type}) and a.own='CITILINK'
				order by b.actype_id asc");
        // echo $this->db_dsssrv->last_query($query);
        // echo $this->dev_gmf->last_query();
        $query = $query->result_array();

        return $query;
    }

    public function get_reg_all($ac_type)
    {
        if ($ac_type == 'garuda') {
            $ac_type = 'GA';
        } else {
            $ac_type = 'CITILINK';
        }

        //dev_gmf
        $query = $this->dev_gmf->query("
				SELECT acreg, b.actype, b.actype_id FROM m_acreg a
				JOIN m_actype b ON a.actype_id = b.actype_id
				WHERE a.own='{$ac_type}'
				order by b.actype_id asc");
        // echo $this->db_dsssrv->last_query($query);
        // echo $this->dev_gmf->last_query();
        $query = $query->result_array();

        return $query;
    }

    public function cek_actype($ac_type)
    {
        $query = $this->dev_gmf->query("
				select top 1  m_actype.actype, acreg
				from m_acreg 
				join m_actype on m_acreg.actype_id = m_actype.actype_id 
				where m_actype.actype_id ='{$ac_type}'");
        $query = $query->row_array();
        // echo $this->dev_gmf->last_query();
        return $query;
    }

    public function v_mitigasi($date, $tipe, $aircraft)
    {
        if ($tipe == '') {
            $tipe = "";
        } else {
            $tipe = " and ac_type in({$tipe})";
        }

        $awal = explode("-", $date);
        $awal_bulan = $awal[0];
        $awal_tahun = $awal[1];
        $query = $this->dev_gmf->query("
				select * from view_cotd_mitigation where bulan='{$awal_bulan}' and tahun='{$awal_tahun}' and aircraft='{$aircraft}' {$tipe}");
        $query = $query->result_array();
        // echo $this->dev_gmf->last_query();
        return $query;
    }


}