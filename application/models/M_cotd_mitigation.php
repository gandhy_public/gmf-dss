<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_cotd_mitigation extends CI_Model{
 
  var $table = 'TBL_FLT_SABRE';
  // var $column_order = array('TOTALTECH','TOTALTECH','TOTALTECH', 'TOTALTECH', 'TOTALTECH', 'TOTALTECH', null);
  // var $column_search = array('TOTALTECH','TOTALTECH');
  // var $order = array('TOTALTECH' => 'asc');

  var $column_order = array('','BULAN','AC_TYPE','AIRCRAFT', NULL,'ACTUAL', null);
  var $column_search = array('AC_TYPE','AIRCRAFT', 'ACTUAL');
  var $order = array('AC_TYPE' => 'asc');

  var $date_start="";
  var $date_end="";
  var $where_date_start = "AND CONVERT(DATE, DateEvent,112) >= ";
  var $where_date_end = "AND CONVERT(DATE, DateEvent,112) <= ";

  var $where_ = "";
  var $group_by_ = "TBL_FLT_SABRE.AC_TYPE, 
  MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC), YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC)";
  var $having_ = "((CAST(sum(case when TECH_DELAY = 'Y' then 1 else 0 end) AS FLOAT) /
    NULLIF(CAST(sum(case when CODE_DELAY != '' then 1 else 0 end) AS FLOAT), 0))*
    (CAST(sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else 0 end) AS FLOAT) /
    NULLIF(CAST(sum(case when LEG_STATE != 'Cancelled' then 1 else 0 end) AS FLOAT), 0))) != 0";

  public function __construct()
  {
    parent::__construct();
      $this->load->database();
      // $this->db_dev = $this->load->database('dev_gmf_crm', TRUE);
      $this->db_dev = $this->load->database('dev_gmf', TRUE);
  }

  private function _get_datatables_query(){

    $this->db_dev->select("AC_TYPE, MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) as bulan, 
                        YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) as tahun,
                        sum(case when TECH_DELAY = 'Y' then 1 else null end) TOTALTECH,
                        sum(case when CODE_DELAY != '' then 1 else null end) TOTALDELAY,
                        sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else null end) FLIGHTDELAY,
                        sum(case when LEG_STATE != 'Cancelled' then 1 else null end) TOTAL_LEG_STATE,
                        ((CAST(sum(case when TECH_DELAY = 'Y' then 1 else 0 end) AS FLOAT) /
                        NULLIF(CAST(sum(case when CODE_DELAY != '' then 1 else 0 end) AS FLOAT), 0))*
                        (CAST(sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else 0 end) AS FLOAT) /
                        NULLIF(CAST(sum(case when LEG_STATE != 'Cancelled' then 1 else 0 end) AS FLOAT), 0))) as ACTUAL_GA"
                       );
    $this->db_dev->from($this->table);
    // $this->db->join('m_acreg', 'TBL_FLT_SABRE.AC_REG = m_acreg.acreg', 'left');
    // $this->db->join("m_actype", "TBL_FLT_SABRE.AC_TYPE LIKE %m_actype.actype%");
    $this->db_dev->group_by('TBL_FLT_SABRE.AC_TYPE, 
                            MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC), 
                            YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC)');
    $this->db_dev->having("((CAST(sum(case when TECH_DELAY = 'Y' then 1 else 0 end) AS FLOAT) /
                        NULLIF(CAST(sum(case when CODE_DELAY != '' then 1 else 0 end) AS FLOAT), 0))*
                        (CAST(sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else 0 end) AS FLOAT) /
                        NULLIF(CAST(sum(case when LEG_STATE != 'Cancelled' then 1 else 0 end) AS FLOAT), 0))) !=", 0, FALSE);
    $i = 0;

    foreach ($this->column_search as $item)
    {
      if(!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db_dev->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db_dev->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db_dev->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db_dev->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order']))
    {
      $this->db_dev->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db_dev->order_by(key($order), $order[key($order)]);
    }
  }

  private function _get_datatables_query_new(){

    if (!empty($this->date_start) || $this->date_start!= "" || !empty($this->date_end) || $this->date_end!= "") {
    $this->db_dev->select("actype as AC_TYPE, (case when BULAN is not null then 'CITILINK' else null end) as AIRCRAFT, BULAN, TAHUN, (CAST(totaltech AS FLOAT)/CAST(totaldepar AS FLOAT)) AS ACTUAL FROM(
SELECT 
  MONTH(COL_PLAN_DEPARTURE_DATE) as BULAN, 
  YEAR(COL_PLAN_DEPARTURE_DATE) as TAHUN,
  actype,
  COUNT(COL_IDX) as totaldepar
from TBL_AC_MOVEMENT_PROD1 
JOIN m_acreg ON TBL_AC_MOVEMENT_PROD1.COL_AIRCRAFT_REGISTRATION = m_acreg.acreg
JOIN m_actype ON m_acreg.actype_id = m_actype.actype_id
where COL_CARRIER_CODE like 'QG%' 
GROUP BY actype, MONTH(COL_PLAN_DEPARTURE_DATE), 
YEAR(COL_PLAN_DEPARTURE_DATE)) as A
INNER JOIN (
SELECT 
MONTH(DateEvent) as BULAN_B, 
  YEAR(DateEvent) as TAHUN_B,
  ACtype as actype_b,
COUNT(*) as totaltech 
from tdamx 
where FlightNo like '%QG%' AND Reg IN (SELECT acreg FROM m_acreg where own = 'CITILINK')".
      $this->where_date_start.$this->date_start." ".$this->where_date_end.$this->date_end
."GROUP BY actype, MONTH(DateEvent), 
YEAR(DateEvent)) AS B ON A.BULAN = B.BULAN_B
AND a.TAHUN = b.TAHUN_B
AND a.actype = b.actype_b
UNION ALL
SELECT AC_TYPE, (case when YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) is not null then 'GA' else null end) as AIRCRAFT,
MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) as BULAN, 
YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) AS TAHUN,
    ((CAST(sum(case when TECH_DELAY = 'Y' then 1 else 0 end) AS FLOAT) /
    NULLIF(CAST(sum(case when CODE_DELAY != '' then 1 else 0 end) AS FLOAT), 0))*
    (CAST(sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else 0 end) AS FLOAT) /
    NULLIF(CAST(sum(case when LEG_STATE != 'Cancelled' then 1 else 0 end) AS FLOAT), 0))) as ACTUAL
FROM TBL_FLT_SABRE");

  }else {

    $this->db_dev->select("actype as AC_TYPE, (case when BULAN is not null then 'CITILINK' else null end) as AIRCRAFT, BULAN, TAHUN, (CAST(totaltech AS FLOAT)/CAST(totaldepar AS FLOAT)) AS ACTUAL FROM(
SELECT 
  MONTH(COL_PLAN_DEPARTURE_DATE) as BULAN, 
  YEAR(COL_PLAN_DEPARTURE_DATE) as TAHUN,
  actype,
  COUNT(COL_IDX) as totaldepar
from TBL_AC_MOVEMENT_PROD1 
JOIN m_acreg ON TBL_AC_MOVEMENT_PROD1.COL_AIRCRAFT_REGISTRATION = m_acreg.acreg
JOIN m_actype ON m_acreg.actype_id = m_actype.actype_id
where COL_CARRIER_CODE like 'QG%' 
GROUP BY actype, MONTH(COL_PLAN_DEPARTURE_DATE), 
YEAR(COL_PLAN_DEPARTURE_DATE)) as A
INNER JOIN (
SELECT 
MONTH(DateEvent) as BULAN_B, 
  YEAR(DateEvent) as TAHUN_B,
  ACtype as actype_b,
COUNT(*) as totaltech 
from tdamx 
where FlightNo like '%QG%' AND Reg IN (SELECT acreg FROM m_acreg where own = 'CITILINK')GROUP BY actype, MONTH(DateEvent), 
YEAR(DateEvent)) AS B ON A.BULAN = B.BULAN_B
AND a.TAHUN = b.TAHUN_B
AND a.actype = b.actype_b
UNION ALL
SELECT AC_TYPE, (case when YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) is not null then 'GA' else null end) as AIRCRAFT,
MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) as BULAN, 
YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC) AS TAHUN,
    ((CAST(sum(case when TECH_DELAY = 'Y' then 1 else 0 end) AS FLOAT) /
    NULLIF(CAST(sum(case when CODE_DELAY != '' then 1 else 0 end) AS FLOAT), 0))*
    (CAST(sum(case when (CAST(DELAYTIME1 AS int)+CAST(DELAYTIME2 AS int) >= 15) then 1 else 0 end) AS FLOAT) /
    NULLIF(CAST(sum(case when LEG_STATE != 'Cancelled' then 1 else 0 end) AS FLOAT), 0))) as ACTUAL
FROM TBL_FLT_SABRE");
  }

    $this->db_dev->where("AC_REG IN (select acreg FROM m_acreg where own = 'GA')");

    $i = 0;

    foreach ($this->column_search as $item)
    {
      if(!empty($_POST['search']['value'])) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db_dev->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db_dev->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db_dev->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db_dev->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order']))
    {
      $this->db_dev->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db_dev->order_by(key($order), $order[key($order)]);
    }
  }

  private function get_citilink($month, $year){
    $this->db_dev->select("COUNT(*) as totaldepar, 
                          (SELECT COUNT(*) as totaltech from tdamx where FlightNo like 'QG%' 
                          and MONTH(DateEvent) = {$month} and YEAR(DateEvent) = {$year}) as totaltech, 
                          (CONVERT(FLOAT, (SELECT COUNT(*) as totaltech from tdamx where FlightNo like 'QG%' 
                          and MONTH(DateEvent) = {$month} and YEAR(DateEvent) = {$year})) / CONVERT(FLOAT, COUNT(*))) as hasil,
                          MONTH(COL_PLAN_DEPARTURE_DATE) as bulan, 
                          YEAR(COL_PLAN_DEPARTURE_DATE) as  tahun"
                       );
    $this->db_dev->from('TBL_AC_MOVEMENT_PROD1');
    $this->db_dev->like('COL_CARRIER_CODE', 'QG', 'BOTH');
    $this->db_dev->group_by('TBL_FLT_SABRE.AC_TYPE, 
                            MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC), 
                            YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC)');
    $this->db_dev->where('MONTH(COL_PLAN_DEPARTURE_DATE)', $month);
    $this->db_dev->where('YEAR(COL_PLAN_DEPARTURE_DATE)', $year);
    $this->db_dev->group_by('MONTH(COL_PLAN_DEPARTURE_DATE), YEAR(COL_PLAN_DEPARTURE_DATE)');
    $query = $this->db_dev->get();
    return $query->result();
  }

  function get_alldata()
  {
    $this->_get_datatables_query_new();
    $data = $this->db_dev->get();

    return $data->result();
  }

  function get_datatables($mitigation_type="", $p_start="", $p_end="")
  {
    if (!empty($p_start) || $p_start!= "" || !empty($p_end) || $p_end!= "") {
      $awal = explode("-",$p_start);
      $awal_tahun = $awal[0];
      $awal_bulan = $awal[1];
      $akhir = explode("-",$p_end);
      $akhir_tahun = $akhir[0];
      $akhir_bulan = $akhir[1];
      // $year_s = substr($p_start, 0, 4);
      // $month_s = substr($p_start, 5, 6);
      // $year_e = substr($p_end, 0, 4);
      // $month_e = substr($p_end, 5, 6);
      // $ds = cal_days_in_month(CAL_GREGORIAN, $awal_bulan, $awal_tahun);
      $de = cal_days_in_month(CAL_GREGORIAN, $akhir_bulan, $akhir_tahun);
      $p_start = $p_start.'-01';
      $p_end = $p_end.'-'.$de;
      $this->date_start = "'".$p_start."'";
      $this->date_end = "'".$p_end."'";
      $this->_get_datatables_query_new();
      $this->db_dev->where('CONVERT(DATE, TBL_FLT_SABRE.DEP_DATE_ACT_UTC,112) >=', $p_start);
      $this->db_dev->where('CONVERT(DATE, TBL_FLT_SABRE.DEP_DATE_ACT_UTC,112) <=', $p_end);
    }else{
      $this->_get_datatables_query_new();
    }
    $this->db_dev->group_by($this->group_by_);
    $this->db_dev->having($this->having_);
    if($_POST['length'] != -1)
      $this->db_dev->limit($_POST['length'], $_POST['start']);
      $query = $this->db_dev->get();
    return $query->result();
  }

  function count_filtered($mitigation_type="", $p_start="", $p_end="")
  {
    if (!empty($p_start) || $p_start!= "" || !empty($p_end) || $p_end!= "") {
      $awal = explode("-",$p_start);
      $awal_tahun = $awal[0];
      $awal_bulan = $awal[1];
      $akhir = explode("-",$p_end);
      $akhir_tahun = $akhir[0];
      $akhir_bulan = $akhir[1];
      // $year_s = substr($p_start, 0, 4);
      // $month_s = substr($p_start, 5, 6);
      // $year_e = substr($p_end, 0, 4);
      // $month_e = substr($p_end, 5, 6);
      // $ds = cal_days_in_month(CAL_GREGORIAN, $awal_bulan, $awal_tahun);
      $de = cal_days_in_month(CAL_GREGORIAN, $akhir_bulan, $akhir_tahun);
      $p_start = $p_start.'-01';
      $p_end = $p_end.'-'.$de;
      $this->date_start = "'".$p_start."'";
      $this->date_end = "'".$p_end."'";
      $this->_get_datatables_query_new();
      $this->db_dev->where('CONVERT(DATE, TBL_FLT_SABRE.DEP_DATE_ACT_UTC,112) >=', $p_start);
      $this->db_dev->where('CONVERT(DATE, TBL_FLT_SABRE.DEP_DATE_ACT_UTC,112) <=', $p_end);
    }else{
      $this->_get_datatables_query_new();
    }
    $this->db_dev->group_by($this->group_by_);
    $this->db_dev->having($this->having_);
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  public function count_all($mitigation_type="", $p_start="", $p_end="", $target="")
  {
    if (!empty($p_start) || $p_start!= "" || !empty($p_end) || $p_end!= "") {
      $awal = explode("-",$p_start);
      $awal_tahun = $awal[0];
      $awal_bulan = $awal[1];
      $akhir = explode("-",$p_end);
      $akhir_tahun = $akhir[0];
      $akhir_bulan = $akhir[1];
      // $year_s = substr($p_start, 0, 4);
      // $month_s = substr($p_start, 5, 6);
      // $year_e = substr($p_end, 0, 4);
      // $month_e = substr($p_end, 5, 6);
      // $ds = cal_days_in_month(CAL_GREGORIAN, $awal_bulan, $awal_tahun);
      $de = cal_days_in_month(CAL_GREGORIAN, $akhir_bulan, $akhir_tahun);
      $p_start = $p_start.'-01';
      $p_end = $p_end.'-'.$de;
      $this->date_start = "'".$p_start."'";
      $this->date_end = "'".$p_end."'";
      $this->_get_datatables_query_new();
      $this->db_dev->where('CONVERT(DATE, TBL_FLT_SABRE.DEP_DATE_ACT_UTC,112) >=', $p_start);
      $this->db_dev->where('CONVERT(DATE, TBL_FLT_SABRE.DEP_DATE_ACT_UTC,112) <=', $p_end);
    }else{
      $this->_get_datatables_query_new();
    }
    $this->db_dev->group_by($this->group_by_);
    $this->db_dev->having($this->having_);
    $query = $this->db_dev->get();
    return $query->num_rows();
  }

  public function get_mitigasi($month, $year, $aircraft, $type)
  {
    $this->_get_datatables_query_new();
    $this->db_dev->where('MONTH(TBL_FLT_SABRE.DEP_DATE_ACT_UTC)',$month);
    $this->db_dev->where('YEAR(TBL_FLT_SABRE.DEP_DATE_ACT_UTC)',$year);
    $this->db_dev->like('AC_TYPE',$aircraft, 'BOTH');
    $query = $this->db_dev->get();

    return $query->row();
  }

  public function get_by_id($id)
  {
    $this->db_dev->from($this->table);
    $this->db_dev->where('id_master_mitigation',$id);
    $query = $this->db_dev->get();

    return $query->row();
  }

  public function save($data)
  {
    $this->db_dev->insert($this->table, $data);

    $id = $this->db_dev->insert_id();
    return (isset($id)) ? $id : FALSE;
  }

  public function update($where, $data)
  {
    $this->db_dev->update($this->table, $data, $where);
    return $this->db_dev->affected_rows();
  }

  public function delete($id)
  {
    $this->db_dev->where('id_master_mitigation', $id);
    $this->db_dev->delete($this->table);
  }

}
