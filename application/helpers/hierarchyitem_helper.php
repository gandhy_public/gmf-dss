<?php


  function buildMenu($array, $ischild = false)
  {
    echo '<ul>';
    foreach ($array as $item)
    {
      echo '<li>';
      echo $item->ki_name;
      if (!empty($item->children))
      {
        buildMenu($item->children, true);
      }
      echo '</li>';
    }
    echo '</ul>';
  }

  function generatePageTree($datas, $depth = 1, $parent = 0){
  	if($depth > 1000) return '';
  	$tree = '';
  	for($i=0, $ni=count($datas); $i < $ni; $i++){
  		if($datas[$i]->ki_parent_id == $parent){
  			$tree .= "<option class='item' data-ki_id='".$datas[$i]->ki_id."' data-ki_parent_id='".$datas[$i]->ki_parent_id."' data-level='".($depth+1)."' data-deep='".$datas[$i]->ki_level."' value='".$datas[$i]->ki_id."'> <span>L".$depth."</span>&nbsp;".str_repeat('&nbsp;&nbsp;', $depth);
  			$tree .= $datas[$i]->ki_name."</option>";
  			$tree .= generatePageTree($datas, $depth+1, $datas[$i]->ki_id);
  		}
  	}
  	return $tree;
  }

  function getDeepChild($id)
  {
    $ci = & get_instance();
    $ci->dev_gmf = $ci->load->database("dev_gmf", true);
    $sql = "select count(ki_parent_id) as total from kpi_item where ki_parent_id = '".$id."'";
    $exec = $ci->dev_gmf->query($sql);
    $arr = $exec->row();
    return $arr->total;
  }

  function getChildrenOf($ary, $id)
  {
    foreach ($ary as $el)
    {
      if ($el['id'] == $id)
        return $el;
    }
    return FALSE; // use false to flag no result.
  }

  function generatePageTreeTable($datas, $year, $depth = 1, $parent = 0){
    $ci = & get_instance();
    $ci->dev_gmf = $ci->load->database("dev_gmf", true);
    if($depth > 1000) return '';
    $deepChild = 1;
    $tree = '';
    if(count($datas) < 1){
      return "<tr><td colspan='9' style='text-align:center'>Data not found</td></tr>";
    }
    for($i=0, $ni=count($datas); $i < $ni; $i++){
      if($datas[$i]->ki_parent_id == $parent){
        $target = explode("|", $datas[$i]->kid_target);
        $targetYtd = explode("|", $datas[$i]->kid_target_ytd);
        $targetYear = explode("|", $datas[$i]->ki_target);
        $targetYearYtd = explode("|", $datas[$i]->ki_target_ytd);
        $tree .= "<tr data-id='".$datas[$i]->ki_id."' data-parent='".$datas[$i]->ki_parent_id."' data-level='".$depth."'> <td data-column='name'> L".$depth."&nbsp;&nbsp;-".str_repeat('&nbsp;&nbsp;&nbsp;', $depth);
        $tree .= $datas[$i]->ki_name."</td>";
        $tree .= "<td>".$datas[$i]->ki_uom."</td>";
        $weight = 0;
        if($depth == 1){
          if(empty($datas[$i]->kid_weight)){
            // $sql = "select sum(ki_weight) as total from kpi_item where ki_parent_id = '".$datas[$i]->ki_id."'";
            $sql = "SELECT
                     SUM (weight) AS total
                   FROM
                     (
                       SELECT
                         CASE
                       WHEN b.kid_weight IS NULL THEN
                         a.ki_weight
                       ELSE
                         b.kid_weight
                       END AS weight
                       FROM
                         kpi_item a
                       LEFT JOIN kpi_item_detail b ON a.ki_id = b.ki_id
                       AND DATEPART(
                         MONTH,
                         CONVERT (DATE, kid_date, 20)
                       ) = '".date("m")."'
                       AND DATEPART(
                         YEAR,
                         CONVERT (DATE, kid_date, 20)
                       ) = '".$year."'
                       WHERE
                         ki_parent_id = '".$datas[$i]->ki_id."'
                     ) as A";
           } else{
             $sql = "SELECT
                      SUM (weight) AS total
                    FROM
                      (
                        SELECT
                          CASE
                        WHEN b.kid_weight IS NULL THEN
                          a.ki_weight
                        ELSE
                          b.kid_weight
                        END AS weight
                        FROM
                          kpi_item a
                        LEFT JOIN kpi_item_detail b ON a.ki_id = b.ki_id
                        AND DATEPART(
                          MONTH,
                          CONVERT (DATE, kid_date, 20)
                        ) = '".date("m")."'
                        AND DATEPART(
                          YEAR,
                          CONVERT (DATE, kid_date, 20)
                        ) = '".$year."'
                        WHERE
                          ki_parent_id = '".$datas[$i]->ki_id."'
                      ) as A";
           }
        $exec = $ci->dev_gmf->query($sql);
        $weight = $exec->row()->total;
      }  else{
        $weight = $datas[$i]->ki_weight;
      }

      $buttonEdit = "<button data-id='".$datas[$i]->ki_id."' data-level='".$depth."' data-formula='".$datas[$i]->fa_function_name."' id='editKpi' class='btn btn-default btn-flat'><i class='fa fa-pencil'></i></button>";
      // $buttonEdit = "";
        if(getDeepChild($datas[$i]->ki_id) == 0){
          $tree .= "<td>".$datas[$i]->kf_name."</td>";
          $tree .= "<td>".(($datas[$i]->kid_weight != null || $datas[$i]->kid_weight != "" || $datas[$i]->kid_weight == "0") ? $datas[$i]->kid_weight : $datas[$i]->ki_weight)."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kid_target && count($target) == 1) ? $datas[$i]->kid_target : " ".(!empty($datas[$i]->ki_target && count($targetYear) == 1) ? $datas[$i]->ki_target : "<span class='text-danger'>-</span>"))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kid_target_ytd && count($target) == 1) ? $datas[$i]->kid_target_ytd : " ".(!empty($datas[$i]->ki_target_ytd && count($targetYearYtd) == 1) ? $datas[$i]->ki_target_ytd : "<span class='text-danger'>-</span>"))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kid_target && count($target) > 1) ? $target[0] : " ".(!empty($datas[$i]->ki_target && count($targetYear) > 1) ? $targetYear[0] : "<span class='text-danger'>-</span>" ))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kid_target && count($target) > 1) ? $target[1] : " ".(!empty($datas[$i]->ki_target && count($targetYear) > 1) ? $targetYear[1] : "<span class='text-danger'>-</span>" ))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kid_target_ytd && count($targetYtd) > 1) ? $targetYtd[0] : " ".(!empty($datas[$i]->ki_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[0] : "<span class='text-danger'>-</span>" ))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kid_target_ytd && count($targetYtd) > 1) ? $targetYtd[1] : " ".(!empty($datas[$i]->ki_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[1] : "<span class='text-danger'>-</span>" ))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kid_limit) ? $datas[$i]->kid_limit : "<span class='text-danger'>-</span>")."</td>";
          $tree .= "<td>";
          $tree .= "<button type='button' id='detail' data-toggle='modal' data-target='#detailItem' data-weight='".$datas[$i]->ki_weight."' data-ki_target='".$datas[$i]->ki_target."' data-ki_target_ytd='".$datas[$i]->ki_target_ytd."' data-formula='".$datas[$i]->fa_function_name."' data-uom='".$datas[$i]->ki_uom."' data-ki_name='".$datas[$i]->ki_name."' data-id='".$datas[$i]->ki_id."' class='btn btn-flat bg-navy'><i class='fa fa-wrench'></i></button>";
          $tree .= "<button type='button' id='delete' data-id='".$datas[$i]->ki_id."' class='btn btn-flat btn-danger'><i class='fa fa-trash'></i></button>";
          $tree .= $buttonEdit;
          $tree .= "</td>";
        } else{
          $tree .= "<td>Aggregate Score</td>";
          $tree .= "<td>".(($datas[$i]->ki_weight != null || $datas[$i]->ki_weight != "" || $datas[$i]->ki_weight == "0") ? $weight : "<span class='text-danger'>0</span>")."</td>";
          $tree .= "<td>".(($datas[$i]->ki_target != null && $datas[$i]->ki_target != "" && $datas[$i]->ki_target != "0" && count($targetYear) == 1) ? $datas[$i]->ki_target : "<span class='text-danger'>-</span>")."</td>";
          $tree .= "<td>".(($datas[$i]->ki_target_ytd != null && $datas[$i]->ki_target_ytd != "" && $datas[$i]->ki_target_ytd != "0" && count($targetYearYtd) == 1) ? $datas[$i]->ki_target_ytd : "<span class='text-danger'>-</span>")."</td>";
          $tree .= "<td>".(!empty($datas[$i]->ki_target && count($targetYear) > 1) ? $targetYear[0] : "<span class='text-danger'>-</span>" )."</td>";
          $tree .= "<td>".(!empty($datas[$i]->ki_target && count($targetYear) > 1) ? $targetYear[1] : "<span class='text-danger'>-</span>" )."</td>";
          $tree .= "<td>".(!empty($datas[$i]->ki_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[0] : "<span class='text-danger'>-</span>" )."</td>";
          $tree .= "<td>".(!empty($datas[$i]->ki_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[1] : "<span class='text-danger'>-</span>" )."</td>";
          $tree .= "<td>"."<span class='text-danger'>-</span>"."</td>";
          $tree .= "<td>";
          $tree .= "<button type='button' id='delete' data-id='".$datas[$i]->ki_id."' class='btn btn-flat btn-danger'><i class='fa fa-trash'></i></button>";
          $tree .= $buttonEdit;
          $tree .= "</td>";
        }
        $tree .= "</tr>";
       $tree .= generatePageTreeTable($datas, $year, $depth+1, $datas[$i]->ki_id);
     }
   }
   return $tree;
 }


  function generatePageTreeTwo($datas, $depth = 0, $parent = 0){
  	if($depth > 1000) return ''; // Make sure not to have an endless recursion
  	$tree = '';
  	for($i=0, $ni=count($datas); $i < $ni; $i++){
  		if($datas[$i]['parent'] == $parent){
  			$tree .= str_repeat('-', $depth);
  			$tree .= $datas[$i]['name'] . '<br/>';
  			$tree .= generatePageTree($datas, $depth+1, $datas[$i]['id']);
  		}
  	}
  	return $tree;
  }

  function buildMenuUnit($array, $ischild = false)
  {
    echo '<ul>';
    foreach ($array as $item)
    {
      echo '<li>';
      echo $item->kia_name;
      if (!empty($item->children))
      {
        buildMenu($item->children, true);
      }
      echo '</li>';
    }
    echo '</ul>';
  }

  function generatePageTreeUnit($datas, $depth = 1, $parent = 0){
  	if($depth > 1000) return '';
  	$tree = '';
    $depth1= $depth+1;
  	for($i=0, $ni=count($datas); $i < $ni; $i++){
  		if($datas[$i]->kia_parent_id == $parent){
  			$tree .= "<option class='item' data-level='".($depth+1)."' value='".$datas[$i]->kia_id."'> <span>L".$depth."</span>&nbsp;".str_repeat('&nbsp;&nbsp;', $depth);
  			$tree .= $datas[$i]->kia_name."</option>";
  			$tree .= generatePageTreeUnit($datas, $depth+1, $datas[$i]->kia_id);
  		}
  	}
  	return $tree;
  }

  function getDeepChildUnit($id,$role_id)
  {
    $ci = & get_instance();
    $ci->dev_gmf = $ci->load->database("dev_gmf", true);
    $sql = "select count(kia_parent_id) as total from kpi_item_assignment where kia_parent_id = '".$id."' and role_id='".$role_id."'";
    $exec = $ci->dev_gmf->query($sql);
    $arr = $exec->row();
    return $arr->total;
  }

  function getChildrenOfUnit($ary, $id)
  {
    foreach ($ary as $el)
    {
      if ($el['id'] == $id)
        return $el;
    }
    return FALSE; // use false to flag no result.
  }

  function generatePageTreeTableUnit($datas,$year,$role, $isLocked, $depth = 1, $parent = 0){

    $ci = & get_instance();
    $ci->dev_gmf = $ci->load->database("dev_gmf", true);
    $ci->load->library('session');
    if($depth > 1000) return '';
    $deepChild = 1;
    $tree = '';
    for($i=0, $ni=count($datas); $i < $ni; $i++){
      if($datas[$i]->kia_parent_id == $parent){
        $target = explode("|", $datas[$i]->kiad_target);
        $targetYear = explode("|", $datas[$i]->kia_target);
        $targetYtd = explode("|", $datas[$i]->kiad_target_ytd);
        $targetYearYtd = explode("|", $datas[$i]->kia_target_ytd);
        $tree .= "<tr data-id='".$datas[$i]->kia_id."' data-parent='".$datas[$i]->kia_parent_id."' data-level='".$depth."'> <td data-column='name'> L".$depth."&nbsp;&nbsp;-".str_repeat('&nbsp;&nbsp;&nbsp;', $depth);
        $tree .= $datas[$i]->kia_name."</td>";
        $tree .= "<td>".$datas[$i]->kia_uom."</td>";
        $weight = 0;
        if($depth == 1){
          if(empty($datas[$i]->kiad_weight)){
            // $sql = "select sum(ki_weight) as total from kpi_item where ki_parent_id = '".$datas[$i]->ki_id."'";
            $sql = "SELECT
                     SUM (weight) AS total
                   FROM
                     (
                       SELECT
                         CASE
                       WHEN b.kiad_weight IS NULL THEN
                         a.kia_weight
                       ELSE
                         b.kiad_weight
                       END AS weight
                       FROM
                         kpi_item_assignment a
                       LEFT JOIN kpi_item_assigment_detail b ON a.kia_id = b.kia_id
                       AND DATEPART(
                         MONTH,
                         CONVERT (DATE, kiad_date, 20)
                       ) = '".date("m")."'
                       AND DATEPART(
                         YEAR,
                         CONVERT (DATE, kiad_date, 20)
                       ) = '".$year."'
                       WHERE
                         kia_parent_id = '".$datas[$i]->kia_id."'
                     ) as A";
           } else{
             $sql = "SELECT
                      SUM (weight) AS total
                    FROM
                      (
                        SELECT
                          CASE
                        WHEN b.kiad_weight IS NULL THEN
                          a.kia_weight
                        ELSE
                          b.kiad_weight
                        END AS weight
                        FROM
                          kpi_item_assignment a
                        LEFT JOIN kpi_item_assigment_detail b ON a.kia_id = b.kia_id
                        AND DATEPART(
                          MONTH,
                          CONVERT (DATE, kiad_date, 20)
                        ) = '".date("m")."'
                        AND DATEPART(
                          YEAR,
                          CONVERT (DATE, kiad_date, 20)
                        ) = '".$year."'
                        WHERE
                          kia_parent_id = '".$datas[$i]->kia_id."'
                      ) as A";
           }
        $exec = $ci->dev_gmf->query($sql);
        $weight = $exec->row()->total;
      }  else{
        $weight = $datas[$i]->kia_weight;
      }

      $buttonEdit = "<button data-id='".$datas[$i]->kia_id."' data-level='".$depth."' data-formula='".$datas[$i]->fa_function_name."' id='editKpi' class='btn btn-default btn-flat'><i class='fa fa-pencil'></i></button>";
        if(getDeepChildUnit($datas[$i]->kia_id,$role) == 0){
          $tree .= "<td>".$datas[$i]->kf_name."</td>";
          $tree .= "<td>".(($datas[$i]->kiad_weight != null || $datas[$i]->kiad_weight != "" || $datas[$i]->kiad_weight == "0") ? $datas[$i]->kiad_weight : $datas[$i]->kia_weight)."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kiad_target && count($target) == 1) ? $datas[$i]->kiad_target : " ".(!empty($datas[$i]->kia_target && count($targetYear) == 1) ? $datas[$i]->kia_target : "<span class='text-danger'>-</span>"))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kiad_target_ytd && count($targetYtd) == 1) ? $datas[$i]->kiad_target_ytd : " ".(!empty($datas[$i]->kia_target_ytd && count($targetYearYtd) == 1) ? $datas[$i]->kia_target_ytd : "<span class='text-danger'>-</span>"))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kiad_target && count($target) > 1) ? $target[0] : " ".(!empty($datas[$i]->kia_target && count($targetYear) > 1) ? $targetYear[0] : "<span class='text-danger'>-</span>" ))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kiad_target && count($target) > 1) ? $target[1] : " ".(!empty($datas[$i]->kia_target && count($targetYear) > 1) ? $targetYear[1] : "<span class='text-danger'>-</span>" ))."</td>";
          // YTD
          $tree .= "<td>".(!empty($datas[$i]->kiad_target_ytd && count($targetYtd) > 1) ? $targetYtd[0] : " ".(!empty($datas[$i]->kia_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[0] : "<span class='text-danger'>-</span>" ))."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kiad_target_ytd && count($targetYtd) > 1) ? $targetYtd[1] : " ".(!empty($datas[$i]->kia_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[1] : "<span class='text-danger'>-</span>" ))."</td>";
          // END
          $tree .= "<td>".(!empty($datas[$i]->kiad_limit) ? $datas[$i]->kiad_limit : "<span class='text-danger'>-</span>")."</td>";
          if($isLocked == false || $ci->session->userdata("new_role") == "td_corporate" || $ci->session->userdata("role") == "Super Admin"){
            $tree .= "<td>";
            $tree .= "<button type='button' onclick='detail_item(\"".$datas[$i]->fa_function_name."\",\"".$datas[$i]->kia_uom."\",\"".$datas[$i]->kia_name."\",\"".$datas[$i]->kia_id."\",\"".$depth."\",\"".$datas[$i]->kia_target_ytd."\", \"".$datas[$i]->kia_target."\")' class='btn btn-flat bg-navy'><i class='fa fa-wrench'></i></button>";
            if(empty($datas[$i]->ki_id)){
              $tree .= "<button type='button' id='delete' data-id='".$datas[$i]->kia_id."' class='btn btn-flat btn-danger'><i class='fa fa-trash'></i></button>";
            }
            $tree .= $buttonEdit;
            $tree .= "</td>";
          }
        } else{
          $tree .= "<td>Aggregate Score</td>";
          $tree .= "<td>".(($datas[$i]->kia_weight != null || $datas[$i]->kia_weight != "" || $datas[$i]->kia_weight == "0") ? $weight : "<span class='text-danger'>0</span>")."</td>";
          $tree .= "<td>".(($datas[$i]->kia_target != null && $datas[$i]->kia_target != "" && $datas[$i]->kia_target != "0" && count($targetYear) == 1) ? $datas[$i]->kia_target : "<span class='text-danger'>-</span>")."</td>";
          $tree .= "<td>".(($datas[$i]->kia_target_ytd != null && $datas[$i]->kia_target_ytd != "" && $datas[$i]->kia_target_ytd != "0" && count($targetYearYtd) == 1) ? $datas[$i]->kia_target_ytd : "<span class='text-danger'>-</span>")."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kia_target && count($targetYear) > 1) ? $targetYear[0] : "<span class='text-danger'>-</span>" )."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kia_target && count($targetYear) > 1) ? $targetYear[1] : "<span class='text-danger'>-</span>" )."</td>";
          // YTD
          $tree .= "<td>".(!empty($datas[$i]->kia_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[0] : "<span class='text-danger'>-</span>" )."</td>";
          $tree .= "<td>".(!empty($datas[$i]->kia_target_ytd && count($targetYearYtd) > 1) ? $targetYearYtd[1] : "<span class='text-danger'>-</span>" )."</td>";
          // END
          $tree .= "<td>"."<span class='text-danger'>-</span>"."</td>";
          if($isLocked == false || $ci->session->userdata("new_role") == "td_corporate" || $ci->session->userdata("role") == "Super Admin"){
            $tree .= "<td>";
            if(empty($datas[$i]->ki_id)){
              $tree .= "<button type='button' id='delete' data-id='".$datas[$i]->kia_id."' class='btn btn-flat btn-danger'><i class='fa fa-trash'></i></button>";
            }
            // $tree .= "<button type='button' id='delete' data-id='".$datas[$i]->kia_id."' class='btn btn-flat btn-danger'><i class='fa fa-trash'></i></button>";
            $tree .= $buttonEdit;
            $tree .= "</td>";
          }
        }
        $tree .= "</tr>";
       $tree .= generatePageTreeTableUnit($datas, $year,$role,$isLocked, $depth+1, $datas[$i]->kia_id);
     }
   }
   return $tree;
  }

  function generatePageTreeTwoUnit($datas, $depth = 0, $parent = 0){
  	if($depth > 1000) return ''; // Make sure not to have an endless recusion
  	$tree = '';
  	for($i=0, $ni=count($datas); $i < $ni; $i++){
  		if($datas[$i]['parent'] == $parent){
  			$tree .= str_repeat('-', $depth);
  			$tree .= $datas[$i]['name'] . '<br/>';
  			$tree .= generatePageTreeUnit($datas, $depth+1, $datas[$i]['id']);
  		}
  	}
  	return $tree;
  }





 ?>
