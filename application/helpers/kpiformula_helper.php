                                                      <?php

  function achievment($target, $actual, $type)
  {
    if($target == null || empty($target)){
      return 0;
    }
    switch ($type) {
      case 'AverageMax':
          if($target == 0){
            return 0;
          }
          $target = floatval($target);
          $actual = floatval($actual);
          return floatval($actual/$target)*100;
      break;
      case 'AverageMin':
        $target = floatval($target);
        $actual = floatval($actual);
        if($actual > $target){
          return floatval($target/$actual)*100;
        } else{
          return floatval(1-(($actual-$target)/$target))*100;
        }
      break;
      case "Progressifmax":
          $target = floatval($target);
          $actual = floatval($actual);
          if($target == 0){
            return 1*100;
          } else{
            return floatval($actual/$target)*100;
          }
      break;
      case "ProgressiveMin":
        $target = floatval($target);
        $actual = floatval($actual);
        if($target == 0){
          return 1*100;
        } else{
          if($actual > $target){
            return floatval($target/$actual)*100;
          } else{
            return floatval(1-($actual-$target)/$target)*100;
          }
        }
      break;
      case "SumMax":
        $target = floatval($target);
        $actual = floatval($actual);
        if($target == 0){
          return 1*100;
        } else{
          return floatval($actual/$target)*100;
        }
      break;
      case "SumMin":
        $target = floatval($target);
        $actual = floatval($actual);
        if($actual == 0){
          return 1*100;
        } else{
          if($actual > $target){
            return floatval($target/$actual)*100;
          } else{
            return floatval(1-($actual-$target)/$target)*100;
          }
        }
      break;
      case "Stabilize":
        if(empty($target) || $target == null || $target == "0|0"){
          return 0;
        }
        $target = explode("|", $target);
        $upper = (empty(floatval($target[0])) ? 0 : floatval($target[0]));
        $lower = (empty(floatval($target[1])) ? 0 : floatval($target[1]));
        $actual = floatval($actual);
        if($actual > $lower){
          if($actual < $upper){
            return 1*100;
          } else{
            return floatval($upper/$actual)*100;
          }
        } else {
          return floatval($actual/$lower)*100;
        }
      break;
      case "coba":
          return 0;
      break;
      default:
          return 0*100;
      break;
    }
  }

  function score($ach, $lim, $weight)
  {
      $ach = floatval($ach);
      $lim = floatval($lim);
      $weight = floatval($weight);

      if($ach > $lim){
        return floatval($lim*$weight)/100;
      } else{
        return floatval($ach*$weight)/100;
      }
  }
  
                        