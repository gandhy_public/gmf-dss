<?php

function randomColor()
{
    $background_colors = array('#009688', '#05354D', '#E6E7E8', '#F2C573', '#8EC3A7');
    shuffle($background_colors);
    return $background_colors;
}

function standardColor()
{
    $colors = array('#93648d', '#4cc3d9', '#f7ca78', '#05354d', '#1f7f91', '#074b6d', '#7bc8a4', '#d18700', '#ffdd9e');

    return $colors;
}
