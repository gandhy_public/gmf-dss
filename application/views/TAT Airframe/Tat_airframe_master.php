<section class="content">
  <h3><?php echo $title;?></h3>
  <div class="box">
      <div class="box-body">
        <form role="form">

        <div class="col-md-2" id="monthly_start_date">
         <label for="inputEmail3" class="control-label">Start Date</label>

         <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right"  placeholder="Start Date" id="date1" value="<?= date('d-m-Y', strtotime(date("d-m-Y"))); ?>">
                  </div>
        </div>
        <div class="col-md-2" id="monthly_end_date">
            <label for="inputEmail3" class="control-label">End Date</label>

         <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right"  placeholder="End Date" id="date2" value="<?= date('d-m-Y', strtotime(date("d-m-Y"))); ?>">
                  </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label for="inputEmail3" class="control-label">TAT Type</label>
             <div class="col-sm-10" style="padding-right: 0px;padding-left: 0px;">
              <!-- <div class="ddcustomer">
              </div> -->
              <select class="form-control" id="tatcheck">
                <option>TAT A Check</option>
                <option>TAT B Check</option>
              </select>
             </div>
           </div>
        </div>
      </form>
        <div class="col-md-2" style="padding-top: 20px;">
        <button type="button"class="btn btn-flat bg-navy" onclick="search()" id="search" ><i class="fa fa-search"></i> Search</button>
        </div>

        <div class="col-md-12">
          <div class="row " id="tabledata1">
            <h2> Garuda Aircraft </h2>
              <table id="tabelselect" class="table table-hover table-bordered table-gmf">

                <thead>
                <th class="text-center">No</th>
                <th class="text-center">Type</th>
                <th class="text-center">Register</th>
                <th class="text-center">Inspection</th>
                <th class="text-center">Plan</th>
                <th class="text-center">Customer Agreed</th>
                <th class="text-center">Act</th>
                <th class="text-center">Dev</th>
                <th class="text-center">Remack</th>
                <th class="text-center">Bil Biner</th>
                </thead>

              <tbody class="text-center">
                <tr>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                </tr>
              </tbody>
              </table>
          </div>

          <div class="row " id="tabledata2">
            <h2> Citylink Aircraft </h2>
              <table id="tabelselect" class="table table-hover table-bordered table-gmf">

                <thead>
                <th class="text-center">No</th>
                <th class="text-center">Type</th>
                <th class="text-center">Register</th>
                <th class="text-center">Inspection</th>
                <th class="text-center">Plan</th>
                <th class="text-center">Customer Agreed</th>
                <th class="text-center">Act</th>
                <th class="text-center">Dev</th>
                <th class="text-center">Remack</th>
                <th class="text-center">Bil Biner</th>
                </thead>

              <tbody class="text-center">
                <tr>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                </tr>
              </tbody>
              </table>
          </div>

          <div class="row " id="tabledata3">
            <h2> Non GA Aircraft </h2>
              <table id="tabelselect" class="table table-hover table-bordered table-gmf">

                <thead>
                <th class="text-center">No</th>
                <th class="text-center">Type</th>
                <th class="text-center">Register</th>
                <th class="text-center">Inspection</th>
                <th class="text-center">Plan</th>
                <th class="text-center">Customer Agreed</th>
                <th class="text-center">Act</th>
                <th class="text-center">Dev</th>
                <th class="text-center">Remack</th>
                <th class="text-center">Bil Biner</th>
                </thead>

              <tbody class="text-center">
                <tr>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                  <td>1</td>
                </tr>
              </tbody>
              </table>
          </div>
        </div>

        </div>
  </div>

</section>

<script>
for(var i=1; i<=6; i++){
   //Date picker
   $('#date'+i).datepicker({
   format: "dd-mm-yyyy",
     viewMode: "days",
     minViewMode: "days",
     autoclose: true
 });

   $('#dateyear'+i).datepicker({
     format: "yyyy",
     viewMode: "years",
     minViewMode: "years",
     autoclose: true
   });
}
$(function () {


});
</script>
