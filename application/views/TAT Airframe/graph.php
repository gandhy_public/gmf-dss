<style>
    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 18px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }

    .form-group {
        margin-bottom: 8px;
    }

    .box {
        position: relative;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 0px;
        width: 100%;
        box-shadow: 0 8px 12px 4px rgba(0, 0, 0, 0.2);
        margin-top: 0px;
    }

    .widget-user .widget-user-header {
        padding: 20px;
        height: 63px;
        border-top-right-radius: 11px;
        border-top-left-radius: 11px;
    }

    .title-manufacture {
        margin-top: 20px;
        text-align: center;
        color: #F2C573;
        font-size: 16px;
    }

    .widget-user .widget-user-image {
        position: absolute;
        top: 5px;
        left: 36%;
    }

    .widget-user .box-footer {
        padding-top: 0px;
        border-bottom-right-radius: 11px;
        border-bottom-left-radius: 11px;
    }

    .label-panel {
        color: #F2C573;
    }

    .bootstrap-select.btn-group .dropdown-toggle .filter-option {
        display: inline-block;
        overflow: hidden;
        width: 100%;
        text-align: left;
        font-size: 12px;
    }

    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0e1f2e;
    }

    .text-center {
        text-align: center;
    }

    .merah {
        color: #F24738;
    }

    table.table-bordered tbody th, table.table-bordered tbody td {
        border-left-width: 0;
        border-bottom-width: 0;
        text-align: center;
    }
</style>

<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title; ?></h3>
            <div class="box-tools pull-right">
                <label class="control-label" style="font-weight: 100;">
                    <i class="fa  fa-calendar"></i> Year To Date :
                </label>
                <label class="control-label pull-right"><?= date('d-M-Y') ?></label>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <select class="form-control selectpicker btn-default" multiple id="aircraft" data-size="12"
                                title="- Select Aircraft -" disabled style="font-size: 5px;">
                        </select>
                    </div>
                    <div class="form-group">
                        <select type="text" class="form-control" id="aircraft_type" title="- Select Aircraft Type -"
                                placeholder="- Select Aircraft Type -" disabled>
                            <option value="All">All</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label style="font-weight: 100;font-size: 12px;">
                            <input type="radio" name="optionsRadios1" class="minimal optionsRadios1" value="Line">
                            Line Maint.
                            &nbsp;
                            <input type="radio" name="optionsRadios1" class="minimal optionsRadios1" value="Base" checked>
                            Base Maint.
                        </label>
                    </div>
					<div class="form-group" id="categoryRadio">
                        <label style="font-weight: 100;font-size: 11px;">
                            <input type="checkbox" name="optionsRadios2[]" class="minimal optionsRadios2wide"
                                   id="radiobuttonwide" value="WIDE">
                            Wide
                            &nbsp;
                            <input type="checkbox" name="optionsRadios2[]" class="minimal optionsRadios2narrow" value="NARROW" checked>
                            Narrow
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-flat bg-navy" id="search" style="width: 100%">
                            <i class="fa fa-search"></i> Search
                        </button>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px">
                    <div id="grafik_akumulasi" style="height:200px;width:100%;"></div>
                </div>
                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px">
                    <div class="box box-widget widget-user" style="border-radius: 11px">
                        <div class="widget-user-header bg-navy">
                            <h3 class="title-manufacture">Work In Process</h3>
                        </div>
                        <div class="widget-user-image">
                            <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                 style="border: transparent;width: 125px;">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-4 border-right" style="padding-left: 0px;padding-right: 0px;">
                                    <div class="description-block">
                                        <span style="cursor:pointer;text-decoration: underline;font-size: 12px"
                                              class="description-text"
                                              onclick="detail_type_air('Garuda','GA')">Garuda</span>
                                        <div class="col-lg-12">
                                            <h2 style="margin:0px;font-size: 35px;" class="md" data-id="engine_grd"
                                                id="value_garuda"></h2>
                                            <label class="label-panel md" style="font-size: 12px;">Events</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 border-right" style="padding-left: 0px;padding-right: 0px;">
                                    <div class="description-block">
                                        <span style="cursor:pointer;text-decoration: underline;font-size: 12px"
                                              class="description-text" onclick="detail_type_air('Citilink','CITILINK')">Citilink</span>
                                        <div class="col-lg-12">
                                            <h2 style="margin:0px;font-size: 35px;" class="md" data-id="engine_grd"
                                                id="value_citi"></h2>
                                            <label class="label-panel md" style="font-size: 12px;">Events</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;">
                                    <div class="description-block">
                                        <span style="cursor:pointer;text-decoration: underline;font-size: 12px"
                                              class="description-text"
                                              onclick="detail_type_air('Non GA','NON')">Non Ga</span>
                                        <div class="col-lg-12">
                                            <h2 style="margin:0px;font-size: 35px;" class="md" data-id="engine_grd"
                                                id="value_non"></h2>
                                            <label class="label-panel md" style="font-size: 12px;">Events</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body" style="padding: 0px;">
            <div class="row">
                <div class="wadah col-lg-4 col-md-12 col-sm-12 col-xs-12" id="chartgaruda" style="padding-left: 0px;">
                    <div class="box-header with-border"
                         style="border-right:1px solid #d9dde2;border-bottom: 1px solid transparent;padding: 0px;">
                        <div class="wadah col-lg-12">
                            <div id="grafik_aircraft1" style="height:250px;width:100%;"></div>
                        </div>
                    </
                    >
                </div>
            </div>
            <div class="wadah col-lg-4 col-md-12 col-sm126 col-xs-12" id="chartcitilink" style="padding-left: 0px;">
                <div class="box-header with-border"
                     style="border-right:1px solid #d9dde2;border-bottom: 1px solid transparent;padding: 0px;">
                    <div class="wadah col-lg-12">
                        <div id="grafik_aircraft2" style="height:250px;width:100%;"></div>
                    </div>
                </
                >
            </div>
        </div>
        <div class="wadah col-lg-4 col-md-12 col-sm-12 col-xs-12" id="chartnonga" style="padding-left: 0px;">
            <div class="box-header with-border"
                 style="border-right:1px solid transparent;border-bottom: 1px solid transparent;padding: 0px;">
                <div class="wadah col-lg-12">
                    <div id="grafik_aircraft3" style="height:250px;width:100%;"></div>
                </
                >
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class="row tabel_detail hidden" id="detail_ac_panel">
        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title" id="title_detail"></h4>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" id="back">
                            <i class="fa fa-close"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">

                        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="box-header with-border" style="border-bottom: 1px solid #fff;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover table-bordered table-gmf" id="tabel_detail_type"
                                               style="width:100%;">
                                            <thead class="bg-navy">
                                            <tr>
                                                <th class="text-center">No</th>
                                                <th class="text-center">Date</th>
												<th class="text-center">Revision</th>
                                                <th class="text-center">Aircraft Type</th>
                                                <th class="text-center">Register</th>
                                                <th class="text-center">Inspection</th>
                                                <th class="text-center">Target TAT</th>
                                                <th class="text-center">Actual TAT</th>
                                                <th class="text-center">Devation</th>
                                                <th class="text-center">Performance</th>
                                                <th class="text-center hidden"></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box hidden" id="detail_type">
        <div class="box-header with-border">
            <h4 class="box-title" id="detail_type_title">Garuda</h4>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="close">
                    <i class="fa fa-close"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="wadah col-lg-12">
                    <div class="box-header with-border" style="border-bottom: 1px solid #fff;">
                        <table class="table table-hover table-bordered table-gmf" id="tabel_detail_2">
                            <thead class="bg-navy">
                            <tr>
                                <th class="text-center">No</th>
								<th class="text-center">Maint Date</th>
								<th class="text-center">Revision</th>
                                <th class="text-center">Type</th>
                                <th class="text-center">Register</th>
                                <th class="text-center">Inspection</th>
                                <th class="text-center">Target TAT</th>
                                <!--<th class="text-center">Customer Agreed</th>-->
                                <th class="text-center">Total Days</th>
                            </tr>
                            </thead>
                            <tbody>


                            </tbody>
                        </table>

                        <div class="wadah col-lg-1 col-md-1 col-sm-6 col-xs-6">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script>
    $("#search").on("click", (e) => {
        e.preventDefault();
        search();
    });
    var yAxisLimit = [0, 25, 50, 75, 100];

    function search() {
        var tat_type = $('.optionsRadios1:checked').val();
        var fleet = $('.optionsRadios2:checked').val();
        var aircraft = $('#aircraft').val();
        var air_type = $('#aircraft_type').val();
        var leng = aircraft.length;
        var colang = 12 / leng;
        if (tat_type == "" || fleet == "" || aircraft == "" || air_type == "") {
            notif("error", "Please Complete Field");
            return;
        }
        $("#chartgaruda").addClass("hidden");
        $("#chartcitilink").addClass("hidden");
        $("#chartnonga").addClass("hidden");

        $('#input_tat').val(tat_type);
        $('#input_flet').val(fleet);
        $('#input_airc').val(aircraft);
        $('#input_airt').val(air_type);
        $('#detail_type').addClass('hidden');
        $('#tabel_detail_2').DataTable().destroy();

        if (jQuery.inArray("GA", aircraft) != -1) {
            $('#garuda_detail_box').removeClass('hidden');
            $('#garuda_detail_box').removeClass('col-lg-4');
            $('#garuda_detail_box').removeClass('col-lg-6');
            $('#garuda_detail_box').removeClass('col-lg-12');
            $('#garuda_detail_box').addClass('col-lg-' + colang);
            $('#chartgaruda').removeClass('hidden');
            $('#chartgaruda').removeClass("col-lg-4");
            $('#chartgaruda').removeClass("col-lg-6");
            $('#chartgaruda').removeClass("col-lg-12");
            $('#chartgaruda').addClass("col-lg-" + colang);

            load_chart_GA();
        }
        if (jQuery.inArray("CITILINK", aircraft) != -1) {
            $('#citilink_detail_box').removeClass('hidden');
            $('#citilink_detail_box').removeClass('col-lg-4');
            $('#citilink_detail_box').removeClass('col-lg-6');
            $('#citilink_detail_box').removeClass('col-lg-12');
            $('#citilink_detail_box').addClass('col-lg-' + colang);
            $('#chartcitilink').removeClass('hidden');
            $('#chartcitilink').removeClass("col-lg-4");
            $('#chartcitilink').removeClass("col-lg-6");
            $('#chartcitilink').removeClass("col-lg-12");
            $('#chartcitilink').addClass("col-lg-" + colang);

            load_chart_CL();
        }
        if (jQuery.inArray("NON GA", aircraft) != -1) {
            $('#nonga_detail_box').removeClass('hidden');
            $('#nonga_detail_box').removeClass('col-lg-4');
            $('#nonga_detail_box').removeClass('col-lg-6');
            $('#nonga_detail_box').removeClass('col-lg-12');
            $('#nonga_detail_box').addClass('col-lg-' + colang);
            $('#chartnonga').removeClass('hidden');
            $('#chartnonga').removeClass("col-lg-4");
            $('#chartnonga').removeClass("col-lg-6");
            $('#chartnonga').removeClass("col-lg-12");
            $('#chartnonga').addClass("col-lg-" + colang);

            load_chart_non_ga();
        }
        load_chart_acumulation();
        workinproses();
    }

    function detail_type_air(airtype, type) {
        $('#detail_type').removeClass('hidden');
        $('#detail_type_title').html(airtype + ' | ' + 'Detail work in progress');
        var tat_type = $('.optionsRadios1:checked').val();
        var fleet = [];
        fleet[0] = $('.optionsRadios2wide:checked').val();
        fleet[1] = $('.optionsRadios2narrow:checked').val();
        var aircraft = $('#aircraft').val();
        var air_type = $('#aircraft_type').val();
        $('html, body').animate({
            scrollTop: $("#detail_type").position().top
        }, 500);
        $('#tabel_detail_2').DataTable().destroy();
        table = $('#tabel_detail_2').DataTable({
            "processing": true,
            "serverSide": true,
            language: {
                searchPlaceholder: "Search"
            },
            "ajax": {
                "type": 'POST',
                "url": '<?= base_url() ?>index.php/Tat_airframe/search_table_by',
                "data": {
                    "param": type,
                    "param2": fleet,
                    "param3": tat_type,
                    "param4": air_type,
                    "type": type
                }

            },
            "createdRow": function (row, data, index) {
                if (data[6] > data[5]) {
                    $('td', row).addClass('merah');
                }
                $("#loadingApprove").hide();
            }
        });
    }

    $(document).ready(function () {
        load_chart_acumulation();
        load_chart_GA();
        load_chart_CL();
        load_chart_non_ga();
        workinproses();

        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

        $('#aircraft').val('');
        $(".selectpicker").selectpicker("refresh");
        var list_actype;
        $.ajax({
            url: '<?= base_url() ?>index.php/Tat_airframe/get_aircraft',
            type: 'GET',
            dataType: 'JSON'
        })
            .done(function (resp) {
                list_actype = '<select class="selectpicker btn-default" id="aircraft" data-live-search="false" multiple title="- Select Aircraft Type -">';
                $(resp.data).each(function (key, val) {
                    list_actype += '<option value="' + val.own + '" nama="' + val.own + '">' + val.own + '</option>';
                });
                list_actype += '</select>';
                $('#aircraft').html(list_actype);
                $('#aircraft').removeAttr("disabled");
                $(".selectpicker").selectpicker("refresh");
            })
            .fail(function () {
            })
            .always(function () {
            });
        $(document).on("click", "#back", function (e) {
            $("#detail_ac_panel").addClass('hidden');
        });
    });

    function workinproses() {
        var tat_type = $('.optionsRadios1:checked').val();
        var fleet = [];
        fleet[0] = $('.optionsRadios2wide:checked').val();
        fleet[1] = $('.optionsRadios2narrow:checked').val();
        var aircraft = $('#aircraft').val();
        var air_type = $('#aircraft_type').val();

        if (tat_type == "") {
            tat_type = "Base";
        }
        // if (fleet[0] == "" || fleet[1] == "") {
        //     fleet = "WIDE";
        // }
        if (aircraft == "") {
            aircraft = Array("GA", "CITILINK", "NON GA");
        }
        if (air_type == "All") {
            air_type = "";
        }

        $.ajax({
            url: '<?= base_url() ?>index.php/Tat_airframe/search_tat',
            type: 'POST',
            data: {
                param: aircraft,
                param2: fleet,
                param3: tat_type,
                param4: air_type
            },
            dataType: 'JSON',
        })

            .done(function (e) {
                $('#value_garuda').html(e.no_ga);
                $('#value_citi').html(e.no_citi);
                $('#value_non').html(e.no_nga);

            });

    }

    $('#aircraft').on("change", function () {
        var air = $(this).val();
        var leng = air.length;
        var colang = 12 / leng;
        $("#aircraft_type").html("");
        $("#aircraft_type").find('.data').remove();
        $.ajax({
            url: '<?= base_url() ?>index.php/Tat_airframe/get_airtype',
            type: 'POST',
            data: {
                param: air
            },
            dataType: 'JSON',
            success: function (data) {
                if (leng == 1) {
                    var tipe = '<option value="All">All</option>';
                    $(data.data).each(function (key, val) {
                        tipe += '<option class="data" value="' + val.actype + '">' + val.own + '-/-' + val.actype + '</option>';
                    });
                } else {
                    var tipe = '<option value="All">All</option>';
                }

                $("#aircraft_type").append(tipe);
                $('#aircraft_type').removeAttr("disabled");
            }
        });

        $('#detail_type').addClass('hidden');
        $('#garuda_detail_box').addClass('hidden');
        $('#citilink_detail_box').addClass('hidden');
        $('#nonga_detail_box').addClass('hidden');
    });

    $("#close").on("click", function () {
        $('#detail_type').addClass('hidden');
    });

    function load_chart_acumulation() {
        var tat_type = $('.optionsRadios1:checked').val();
        var fleet = [];
        fleet[0] = $('.optionsRadios2wide:checked').val();
        fleet[1] = $('.optionsRadios2narrow:checked').val();
        var aircraft = $('#aircraft').val();
        var air_type = $('#aircraft_type').val();

        if (tat_type == "") {
            tat_type = "Base";
        }
        // if (fleet[0] == "" || fleet[1] == "") {
        //     fleet[0] = "WIDE";
        // }
        if (aircraft == "") {
            aircraft = Array("GA", "CITILINK", "NON GA");
        }
        if (air_type == "All") {
            air_type = "";
        }

        $.ajax({
            url: '<?= base_url() ?>index.php/Tat_airframe/load_chart_accumulation',
            type: 'POST',
            data: {
                param: aircraft,
                param2: fleet,
                param3: tat_type,
                param4: air_type
            },
            headers: {
                'token': localStorage.getItem('status')
            },
            dataType: 'JSON',
            success: function (data) {
                // console.log(data);
                $('#grafik_akumulasi').highcharts({
                    title: {
                        text: ""
                    },
                    xAxis: {
                        categories: data.categories,
                        id: 'x-axis'
                    },
                    yAxis: {
                        title: {
                            text: "Percentage (%)"
                        },
                        min: 0,
                        tickPositioner: function () {
                            return yAxisLimit;
                        },
                    },
                    credits: {
                        enabled: false,

                    },
                    exporting: {
                        enabled: false
                    },
                    series: [{
                        type: 'areaspline',
                        fillColor: {
                            linearGradient: [0, 0, 0, 200],
                            stops: [
                                [0, '#05354D'],
                                [1, "#fff3"]
                            ]
                        },
                        lineWidth: 1,
                        name: 'Actual',
                        data: data.actual

                    },
                        {
                            name: 'Target',
                            data: data.target,
                            color: "#FD9214",
                            marker: {
                                enabled: false
                            }
                        }
                    ]

                });
            }


        });
    }


    function load_chart_GA() {

        var tat_type = $('.optionsRadios1:checked').val();
        var fleet = [];
        fleet[0] = $('.optionsRadios2wide:checked').val();
        fleet[1] = $('.optionsRadios2narrow:checked').val();
        var aircraft = Array("GA");
        var air_type = $('#aircraft_type').val();

        if (tat_type == "") {
            tat_type = "Base";
        }
        // if (fleet == "") {
        //     fleet = "WIDE";
        // }
        if (air_type == "All") {
            air_type = "";
        }


        $.ajax({
            url: '<?= base_url() ?>index.php/Tat_airframe/load_chart_GA',
            type: 'POST',
            data: {
                param: aircraft,
                param2: fleet,
                param3: tat_type,
                param4: air_type
            },
            headers: {
                'token': localStorage.getItem('status')
            },
            dataType: 'JSON',
            success: function (data) {
                $('#grafik_aircraft1').highcharts({
                    title: {
                        useHTML: true,
                        text: '<a href="javascript:void(0)" style=" text-decoration: underline;color: #0f2233;" onclick="detail_aircraft_type(1)" title="Detail Garuda">Garuda</a>'

                    },
                    xAxis: {
                        categories: data.categories,
                        id: 'x-axis',
                    },
                    yAxis: {
                        title: {
                            text: "Percentage (%)"
                        },
                        min: 0,
                        tickPositioner: function () {
                            return yAxisLimit;
                        },
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false,

                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, data.color],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: data.actype,
                            data: data.data,
                            color: data.color
                        },
                        {
                            name: 'Target',
                            data: data.target,
                            color: "#FD9214",
                            marker: {
                                enabled: false
                            }
                        }
                    ],
                });
            }


        });
    }

    function load_chart_CL() {
        var tat_type = $('.optionsRadios1:checked').val();
        var fleet = [];
        fleet[0] = $('.optionsRadios2wide:checked').val();
        fleet[1] = $('.optionsRadios2narrow:checked').val();
        var aircraft = Array("CITILINK");
        var air_type = $('#aircraft_type').val();

        if (tat_type == "") {
            tat_type = "Base";
        }
        if (fleet == "") {
            fleet = "WIDE";
        }
        if (air_type == "All") {
            air_type = "";
        }


        $.ajax({
            url: '<?= base_url() ?>index.php/Tat_airframe/load_chart_GA',
            type: 'POST',
            data: {
                param: aircraft,
                param2: fleet,
                param3: tat_type,
                param4: air_type
            },
            headers: {
                'token': localStorage.getItem('status')
            },
            dataType: 'JSON',
            success: function (data) {
                // console.log(data);
                $('#grafik_aircraft2').highcharts({
                    title: {
                        useHTML: true,
                        text: '<a href="javascript:void(0)" style=" text-decoration: underline;color:#0f2233;" onclick="detail_aircraft_type(2)" title="Detail Citilink">Citilink</a>'

                    },
                    xAxis: {
                        categories: data.categories,
                        id: 'x-axis'
                    },
                    yAxis: {
                        title: {
                            text: "Percentage  (%)"
                        },
                        min: 0,
                        tickPositioner: function () {
                            return yAxisLimit;
                        },
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false,

                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, data.color],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: data.actype,
                            data: data.data,
                            color: data.color
                        },
                        {
                            name: 'Target',
                            data: data.target,
                            color: "#FD9214",
                            marker: {
                                enabled: false
                            }
                        }
                    ],
                });
            }
        });
    }

    function load_chart_non_ga() {
        var tat_type = $('.optionsRadios1:checked').val();
        var fleet = [];
        fleet[0] = $('.optionsRadios2wide:checked').val();
        fleet[1] = $('.optionsRadios2narrow:checked').val();
        var aircraft = Array("NON GA");
        var air_type = $('#aircraft_type').val();

        if (tat_type == "") {
            tat_type = "Base";
        }
        if (fleet == "") {
            fleet = "NARROW";
        }
        if (air_type == "All") {
            air_type = "";
        }


        $.ajax({
            url: '<?= base_url() ?>index.php/Tat_airframe/load_chart_GA',
            type: 'POST',
            data: {
                param: aircraft,
                param2: fleet,
                param3: tat_type,
                param4: air_type
            },
            headers: {
                'token': localStorage.getItem('status')
            },
            dataType: 'JSON',
            success: function (data) {
                $('#grafik_aircraft3').highcharts({
                    title: {
                        useHTML: true,
                        text: '<a href="javascript:void(0)" class="putih_hover" style=" text-decoration: underline;color:#0f2233;" onclick="detail_aircraft_type(3)" title="Detail Non GA">Non GA</a>'

                    },
                    xAxis: {
                        categories: data.categories,
                        id: 'x-axis'
                    },
                    yAxis: {
                        title: {
                            text: "Percentage  (%)"
                        },
                        min: 0,
                        tickPositioner: function () {
                            return yAxisLimit;
                        },
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false,

                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, data.color],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: data.actype,
                            data: data.data,
                            color: data.color,
                        },
                        {
                            name: 'Target',
                            data: data.target,
                            color: "#FD9214",
                            marker: {
                                enabled: false
                            }
                        }
                    ],
                });
            }


        });
    }

    function detail_aircraft_type(aircraft) {
        var tat_type = $('.optionsRadios1:checked').val();
        var fleet = [];
        fleet[0] = $('.optionsRadios2wide:checked').val();
        fleet[1] = $('.optionsRadios2narrow:checked').val();
        var air_type = $('#aircraft_type').val();

        if (tat_type == "") {
            tat_type = "Line";
        }
        // if (fleet == "") {
        //     fleet = "WIDE";
        // }
        if (air_type == "All") {
            air_type = "";
        }

        if (aircraft == "1") {
            tes = "Garuda";
            aircraft = "GA";
        } else if (aircraft == "2") {
            tes = "Citilink";
            aircraft = "CITILINK";
        } else {
            tes = "Non GA";
            aircraft = "NON GA";
        }
        $('#tabel_detail_type').DataTable().destroy();

        $("#tabel_detail_type").DataTable({
            "processing": true,
            "serverSide": true,
            language: {
                searchPlaceholder: "Search"
            },
            "ajax": {
                "url": '<?= base_url() ?>index.php/Tat_airframe/get_table_ga_det',
                "type": 'POST',
                "data": {
                    'param': aircraft,
                    'param2': fleet,
                    'param3': tat_type,
                    'param4': air_type
                }
            },
            "createdRow": function (row, data, index) {
                if (data[9] < 100) {
                    $('td', row).addClass('merah');
                }
                $("#loadingApprove").hide();
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false
            },
                {
                    "targets": [10],
                    "visible": false,
                    "searchable": false
                },
            ],
        });

        $('#detail_ac_panel').removeClass('hidden');
        $('html, body').animate({
            scrollTop: $("#detail_ac_panel").position().top
        }, 500);
        $("#title_detail").html('Detail' + ' ' + tes);
    }

    function get_value_event() {
        $.post('<?= base_url() ?>index.php/Tat_airframe/get_value_ga', function (data) {
            $('#value_garuda').html(data);
        });
    }

    function get_value_event_citi() {
        $.post('<?= base_url() ?>index.php/Tat_airframe/get_value_citi', function (data) {
            $('#value_citi').html(data);
        });
    }

    function get_value_event_non() {
        $.post('<?= base_url() ?>index.php/Tat_airframe/get_value_non', function (data) {
            $('#value_non').html(data);
        });
    }


    $('#date_end, #date_start').datepicker({
        todayBtn: "linked",
        format: 'yyyy-mm',
        viewMode: 'months',
        minViewMode: 'months',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
</script>
