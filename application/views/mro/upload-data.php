<style>
    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }

    .wadah {
        padding-left: 5px;
        padding-right: 5px;
    }

    .center {
        text-align: center;
    }
    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0e1f2e;
    }
</style>

<!-- <section class="content-header">
    <h1>
        <?= $title ?>
        <small><?= $small_tittle ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <?php foreach ($breadcrumb as $data): ?>
            <li><?= $data ?></li>
        <?php endforeach; ?>
    </ol>
</section> -->

<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-lg-9"></div>
                    <div class="col-lg-3 form-group">
                        <div class="col-lg-6">
                            <a class="btn btn-flat bg-navy" id="add" name="button">
                                Upload Data <i class="fa fa-cloud-upload"></i>
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <a class="btn btn-flat btn-danger" id="deleteUpload" name="deleteUpload">Delete Upload 
                            <i class="fa fa-trash"></i>
                            </a>
                        </div>                    
                    </div>
                </div>


                <div class="col-md-12">
                    <table class="table table-hover table-gmf" id="table-mro">
                        <thead style="background-color: #05354D">
                        <tr>
                            <th class="center">No</th>
                            <th class="center">Year</th>
                            <th class="center">Operator</th>
                            <th class="center">Aircraft Type</th>
                            <th class="center">Engine Family</th>
                            <th class="center">Expense Type</th>
                            <th class="center">Total Cost</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal_add" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233">
                <h4 class="modal-title" id="title-edit" style="color: #fff">Upload Data MRO</h4>
            </div>
            <div class="modal-body">
                <form action="" name="formModal" id="formModal" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        <!-- <div class="form-group">
                          <label class="col-sm-3 control-label">Year</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control pull-right" id="datepicker" name="datepicker">
                          </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Upload Data</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control" name="files" id="files" value="" required>
                            </div>
                        </div>
                        <br><br>
                        <div class="form-group col-sm-12">
                            <a id="downloadFormat" name="downloadFormat" style="cursor: pointer;">Download Format Upload
                                <i class="fa fa-cloud-download"></i></a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i
                                    class="fa fa-rotate-left"></i> Cancel
                        </button>
                        <button type="submit" id="upload" class="pull-right btn btn-flat bg-navy"><i
                                    class="fa fa-save"></i> Save
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade modal-fullscreen" id="modal_history" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0e1f2e;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="title-modal">History Upload File MRO</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover table-gmf" id="table-history" width="100%">
                            <thead style="background-color: #05354D">
                            <tr>
                                <th class="center">No</th>
                                <th class="center">Date</th>
                                <th class="center">Name File</th>
                                <th class="center">Action</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<script>

    $("#upload").prop("disabled", true);

    function openFile(file) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        var val;
        switch (extension) {
            case 'xlsx':
                val = true;
                break;
            case 'xls':
                val = true;
                break;
            default :
                val = false;
                break;
        }
        return val;
    }

    function dialogWarning(message) {
        swal({
            title: "Warning",
            text: message,
            showConfirmButton: true,
            type: 'warning'
        });
    }

    function dialogSuccess(message) {
        swal({
            title: "Done",
            text: message,
            timer: 1500,
            showConfirmButton: false,
            type: 'success'
        });
    }

    var table;
    var table_history;
    $(document).ready(function () {
        table = $('#table-mro').DataTable({
            "pagingType": "simple",
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('index.php/mro_upload/list_mro') ?>",
                "type": "POST"
            },
        });

        table_history = $('#table-history').DataTable({
            "bAutoWidth": false,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?= base_url() . 'index.php/mro_upload/list_history_upload/'?>",
                "type": "POST"
            },
            "aoColumns": [
                {sWidth: '20px'},
                {sWidth: '110px'},
                {sWidth: '150px'},
                {sWidth: '20px'}
            ],
            "columnDefs": [
                {
                    "targets": [-1],
                    "orderable": false,
                    "className": "center",
                },
            ],
        });

    });

    var fileUpload = document.getElementById('files');
    $('#files').bind('change', function () {
       
        if (this.files[0].size >= 12000000) { // size >= 2 MB
            $("#upload").prop("disabled", true);
            dialogWarning("Max Size File Upload 10 Mb");
        } else if (!openFile(this.files[0].name)) {
            $("#upload").prop("disabled", true);
            dialogWarning("Format File yang diperbolehkan : File Excel (xlsx dan xls)");
        } else {
            $("#upload").prop("disabled", false);
        }
    });

    function confirm_delete(id, date) {
        swal({
            title: "Warning",
            text: "Seluruh Data Upload MRO pada tanggal "+date+" akan dihapus. Anda Yakin . ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#F24738",
            confirmButtonText: "Delete",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    delete_history(id);
                });
            },
            allowOutsideClick: false
        });
    }

    function delete_history(id) {
        // waitingDialog.show("Delete Data MRO, Please Wait...");
        $.ajax({
            url: "<?php echo base_url() ?>index.php/mro_upload/delete_history/" + id,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
                // waitingDialog.hide();
                console.log(data);
                if (data.sukses) {
                    dialogSuccess(data.message);
                    reload_table();
                    reload_table_history();
                } else {
                    dialogWarning(data.message);
                    reload_table();
                    reload_table_history();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                alert('Error delete data');
            }
        });
    }

    $("#formModal").on("submit", function (e) {
        waitingDialog.show("Upload File, Please Wait...");
        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url() ?>index.php/mro_upload/uploadFile",
            type: "POST",
            processData: false,
            contentType: false,
            data: new FormData(this),
            dataType: "JSON",
            success: function (results) {
                waitingDialog.hide();
                console.log(results);
                // res = JSON.parse(results);
                if (results.sukses) {
                    dialogSuccess(results.message);
                    reload_table();
                    reload_table_history();
                } else {
                    dialogWarning(results.message);
                    reload_table();
                    reload_table_history();
                }
            },
            error: function (status, errorThrown) {
                console.log(status);
                console.log(errorThrown);
                waitingDialog.hide();
                dialogWarning("Gagal Upload File!! (" + status[0] + ")\n" + errorThrown);
            }
        });
    });

    $("#downloadFormat").on("click", function (e) {
        window.location.href = "<?php echo base_url() ?>assets/template-mro/Format Upload Data MRO.xlsx";
    });

    $('#add').click(function (e) {
        $('#modal_add').modal("show")
    });

    $('#deleteUpload').click(function (e) {
        $('#modal_history').modal("show")
    });

    $('#datepicker').datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });

    function reload_table() {
        table.ajax.reload(null, false);
    }

    function reload_table_history() {
        table_history.ajax.reload(null, false);
    }

    // $('#upload-data').DataTable();

</script>
.
