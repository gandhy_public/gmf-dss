<style type="text/css">
.box-header>.fa,
.box-header>.glyphicon,
.box-header>.ion,
.box-header .box-title {
    display: inline-block;
    font-size: 20px;
    font-weight: bold;
    margin: 0;
    line-height: 1;
}

.highlight {
    /* background-color: #00E676; */
    color: red;
}

.spinner {
    margin: 100px auto;
    width: 50px;
    height: 40px;
    text-align: center;
    font-size: 10px;
}

.spinner>div {
    background-color: #333;
    height: 100%;
    width: 6px;
    display: inline-block;

    -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
    animation: sk-stretchdelay 1.2s infinite ease-in-out;
}

.spinner .rect2 {
    -webkit-animation-delay: -1.1s;
    animation-delay: -1.1s;
}

.spinner .rect3 {
    -webkit-animation-delay: -1.0s;
    animation-delay: -1.0s;
}

.spinner .rect4 {
    -webkit-animation-delay: -0.9s;
    animation-delay: -0.9s;
}

.spinner .rect5 {
    -webkit-animation-delay: -0.8s;
    animation-delay: -0.8s;
}

@-webkit-keyframes sk-stretchdelay {
    0%,
    40%,
    100% {
        -webkit-transform: scaleY(0.4)
    }
    20% {
        -webkit-transform: scaleY(1.0)
    }
}

@keyframes sk-stretchdelay {
    0%,
    40%,
    100% {
        transform: scaleY(0.4);
        -webkit-transform: scaleY(0.4);
    }
    20% {
        transform: scaleY(1.0);
        -webkit-transform: scaleY(1.0);
    }
}

.stripe {
    white-space: nowrap;
    background-color: white;
}

.pagination>.active>a,
.pagination>.active>a:focus,
.pagination>.active>a:hover,
.pagination>.active>span,
.pagination>.active>span:focus,
.pagination>.active>span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #0f2233;
    border-color: #0e1f2e;
}
</style>
<section class="content-header">
    <h1>
        <b><?= $title?></b>
    </h1>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title" style="font-weight: unset;"><i class="fa fa-edit"></i> Hil Target</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <form id="form_target_hill" method="post" action="" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-1 control-label">Target</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="target" id="target" placeholder="Target">
                                <input type="hidden" value="<?= date('Y', strtotime(date(" Y "))); ?>" name="year" id="year">
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" id="submit" name="submit" class="btn btn-flat bg-navy">
                                    <i class="fa fa-save"></i> Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-2" style="margin-left: 15px;">
                    <label class="control-label" style="font-weight: 100;">* Current Year :</label>
                    <label class="control-label" style="font-weight: 100;">
                        <?= date('Y', strtotime(date("Y"))); ?>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title" style="font-weight: unset;"><i class="fa fa-edit"></i> Hil Mitigation</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Start Period</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control" id="period1" value="01<?= date('-Y'); ?>">
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>End Period</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control" id="period2" value="<?= date('m-Y'); ?>">
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label></label>
                    <button class="btn btn-block btn-flat bg-navy" id="search" name="search" style="margin-top: 4px;">
                        <i class="fa fa-search"></i> Search
                    </button>
                </div>
            </div>
            <div class="col-lg-12">
                <div id="loadingApprove" class="back-loading" style="display: none;background: white;position: absolute;z-index: 9999;width: 99%;height: 100%;margin: 0 auto;left: 1;/* display: none; */left: 0;right: 0;/* top: 10px; */bottom: 0;/* padding:  50%; */padding top:;/* padding-top:  50px; */">
                    <!-- <h4 style="text-align:  center;margin-top:  50px;font-size: 20px;">Data Diproses</h4> -->
                    <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                    </div>
                </div>
                <table id="tabelmitigation" class="table table-hover table-bordered table-gmf" style="width:100%;">
                    <thead class="bg-navy">
                        <th class="text-center">No</th>
                        <th class="text-center">Aircraft</th>
                        <th class="text-center">Aircraft Type</th>
                        <th class="text-center">Notification</th>
                        <th class="text-center">Start Date</th>
                        <th class="text-center">Due Date</th>
                        <th class="text-center">Due Date</th>
                        <th class="text-center">Action</th>
                    </thead>
                    <tbody class="text-center"></tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal_add_mitigation">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0e1f2e;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Mitigation</h4>
            </div>
            <form id="form_mitigasi" name="form_mitigasi" method="post" action="" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Why?</label>
                        <textarea rows="2" cols="8" type="text" class="form-control" placeholder="Why..." name="why" id="why" required></textarea>
                        <input type="hidden" name="idmitigasi" id="idmitigasi">
                    </div>
                    <div class="form-group">
                        <label>Solution</label>
                        <textarea rows="2" cols="8" type="text" class="form-control" placeholder="Solution..." name="solution" id="solution" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close <i class="fa fa-rotate-left"></i></button>
                    <button type="submit" name="submit" id="submit" class="btn btn-flat bg-navy"> Save <i class="fa fa-save"></i></button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
var table;
$(document).ready(function() {
    $('#period1').datepicker({
        todayBtn: "linked",
        format: 'mm-yyyy',
        viewMode: "months",
        minViewMode: "months",
        autoclose: true
    });
    $('#period2').datepicker({
        todayBtn: "linked",
        format: 'mm-yyyy',
        viewMode: 'months',
        minViewMode: 'months',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    // First Load Pages
    $('#submit').attr('disabled', true);
    $.post('<?= base_url() ?>index.php/Master_hill/get_target', function(data) {
        console.log(data);
        $('#target').val(data);
        $('#submit').attr('disabled', false);
    });

    // Function For Data Table
    table = $("#tabelmitigation").DataTable({
        "processing": true,
        "serverSide": true,
        language: {
            searchPlaceholder: "Search"
        },
        "ajax": {
            "url": '<?= base_url() ?>index.php/Master_hill/datatables',
            "type": 'POST'
        },
        "createdRow": function(row, data, index) {
            if (data[6] == true) {
                $('td', row).addClass('highlight');
            }
            // $("#loadingApprove").hide();
        },
        "columnDefs": [{
                "targets": [-1],
                "orderable": false
            },
            {
                "targets": [6],
                "visible": false,
                "searchable": false
            },
        ],
    });

    // Function On Search Periode
    $("#search").click(function(event) {
        /* Act on the event */
        event.preventDefault();
        var param1 = $("#period1").val();
        var param2 = $("#period2").val();
        table.ajax.url("<?php echo base_url().'index.php/Master_hill/datatables/'?>" + param1 + "/" + param2).load();
    });

    //  Start Add And Cek  Mitigasion
    $(document).on("click", "#add", function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        $.ajax({
                url: '<?= base_url() ?>index.php/Master_hill/get_mitigasi',
                type: 'POST',
                data: { param1: id }
            })
            .done(function(resp) {
                var data = JSON.parse(resp);
                $("#why").val(data.why);
                $("#solution").val(data.solution);
                $("#idmitigasi").val(id);
                console.log(data);
            })
            .fail(function() {
                console.log("error");
                notif('error', 'periksa  Koneksi');
            })
            .always(function() {
                console.log("complete");
                $("#modal_add_mitigation").modal('show');
            });
    });
    // End Add And Cek  Mitigasion

    // Start Action Form Mitigation
    $("#form_mitigasi").submit(function(event) {
        /* Act on the event */
        event.preventDefault();
        $('.bg-navy').attr('disabled', true);
        var formData = new FormData($('#form_mitigasi')[0]);
        $.ajax({
                url: '<?= base_url() ?>index.php/Master_hill/store_mitigasi',
                type: 'POST',
                processData: false,
                contentType: false,
                data: formData
            })
            .done(function(resp) {
                var data = JSON.parse(resp);
                notif(data.info, data.msg);
            })
            .fail(function() {
                console.log("error");
                notif('error', 'periksa  Koneksi');
            })
            .always(function() {
                console.log("complete");
                $('.bg-navy').attr('disabled', false);
                $("#idmitigasi").val();
                $("#modal_add_mitigation").modal('hide');
                $("#form_mitigasi")[0].reset();
                table.ajax.reload();
                $("#loadingApprove").hide();
            });
    });
    // End Action Form Mitigation

    // Start Satpam Decimal
    $("#target").keyup(function() {
        // $(this).val()
        if ($(this).val() > 1) {
            notif('warning', 'Maximal Value Input 1 ');
            $("#submit").attr('disabled', true);
        } else {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            $("#submit").attr('disabled', false);
        }
    });
    // End Satpam Decimal


    // Action Target Hill
    $("#form_target_hill").submit(function(event) {
        /* Act on the event */
        event.preventDefault();
        $('#submit').attr('disabled', true);
        var formData = new FormData($('#form_target_hill')[0]);
        $.ajax({
                url: '<?= base_url() ?>index.php/Master_hill/store_target',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false
            })
            .done(function(resp) {
                var data = JSON.parse(resp);
                notif(data.info, data.msg);
                console.log("success");
            })
            .fail(function() {
                console.log("error");
                notif('error', 'periksa  Koneksi');
            })
            .always(function() {
                $('#submit').attr('disabled', false);
                console.log("complete");
            });

    });
});
// End Action Target Hill
</script>