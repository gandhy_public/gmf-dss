<style type="text/css">
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin: 0;
  line-height: 1;
}
.inner_in{
  padding:20px;
}
.title-manufacture{
  color: #F2C573;
  text-align: center;
  font-size: 25px;
  font-weight: bold;
  margin-top: 30px;
}
.title-actual{
  color: #fff;
  text-align: left;
  font-size: 25px;
  margin-top: 70px;
  margin-left: 25px;
}
.title-target{
  color: #fff;
  text-align: left;
  font-size: 20px;
  margin-top: 0px;
  margin-left: 25px;
}
.value-actual-manufacture-garuda{
  font-size: 30px;
  color: #fff;
  margin-right: 20px;
  text-align: left;
  margin-top: -40px;
}
.value-actual-manufacture-citilink{
  font-size: 30px;
  color: #fff;
  margin-right: 20px;
  text-align: left;
  margin-top: -40px;
}
.value-target{
  font-size: 20px;
  margin-right: 20px;
  color: #fff;
  margin-top: -35px;
}
.global-text-center{
  text-align: center;
}
.inner_in {
  padding: 0px;
  border-radius: 10px;
}
.small-box>.inner {
  padding: 3px;
}
.widget-user .widget-user-header {
  padding: 1px;
  height: 60px;
  border-top-right-radius: 11px;
  border-top-left-radius: 11px;
}
.widget-user .widget-user-image {
  position: absolute;
  top: 5px;
  left: 48%;
  margin-left: -45px;
}
.box .border-right {
  border-right: 1px solid #eac8c8;
}
.form-group{
  margin-bottom: 5px;
}
.widget-user .box-footer {
  padding-top: 0px;
}
.box-footer {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 11px;
  border-bottom-left-radius: 11px;
  border-top: 1px solid #f4f4f4;
  padding: 0px;
  background-color: #fff;
}
.modal-title-panel {
  text-align: center;
  font-weight: bold;
}
.modal-header{
  background-color: #0f2233;
  color: #fff;
}
.back{
  background: grey;
  color: #fff;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #0f2233;
    border-color: #0e1f2e;
}
.description-block {
    display: block;
    margin: 0px 0;
    text-align: center;
}
</style>

<!--
LAYOUT LAMA
<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section> -->

<!--//=======================================Actual vs Target==============================================\\-->
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Hil Index</h3>
      <div class="box-tools pull-right">
        <!--
        LAYOUT LAMA
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
          <i class="fa fa-minus"></i>
        </button> -->
        <label class="control-label" style="font-weight: 100;"><i class="fa  fa-calendar"></i> Year To Date :</label>
        <label class="control-label pull-right" id="date"></label>
      </div>
    </div>
    <div class="box-body" style="padding: 0px;">
    <!-- LAYOUT LAMA -->
      <!-- <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
        <div class="small-box bg-navy inner_in">
          <div class="inner center ">
            <h3 class="title-manufacture">Garuda</h3>
            <h4 class="title-actual">Actual</h4>
            <h2 class="value-actual-manufacture-garuda pull-right" id="actual-garuda"></h2>
            <hr style="width: 90%;">
            <h4 class="title-target">Target</h4>
            <h2 class="value-target pull-right" id="target-garuda"><i class="fa fa-minus" style="color:#F2C573;"></i></h2>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
        <div class="small-box bg-navy inner_in">
          <div class="inner center ">
            <h3 class="title-manufacture">Citilink</h3>
            <h4 class="title-actual">Actual</h4>
            <h2 class="value-actual-manufacture-citilink pull-right" id="actual-citilink"></h2>
            <hr style="width: 90%;">
            <h4 class="title-target">Target</h4>
            <h2 class="value-target pull-right" id="target-citilink"><i class="fa fa-minus" style="color:#F2C573;"></i></h2>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
        <div class="small-box bg-navy inner_in">
          <div class="inner center" style="color:white;">
            <center><h3 style="color:#F2C573;">Garuda</h3>
              <hr>
              <h2 id="count-open-garuda"></h2>
              <p style="color:#F2C573;">OPEN</p>
            </center>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
        <div class="small-box bg-navy inner_in">
          <div class="inner center" style="color:white;">
            <center><h3 style="color:#F2C573;">Citilink</h3>
              <hr>
              <h2 id="count-open-citilink"></h2>
              <p style="color:#F2C573;">OPEN</p>
            </center>
          </div>
        </div>
      </div>-->
      <div class="box-header with-border" style="padding: 8px;">
      <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
        <div class="box box-widget widget-user" style="border-radius: 11px;">
            <div class="widget-user-header bg-navy">
              <h3 class="title-manufacture">GARUDA</h3>
            </div>
            <div class="widget-user-image">
              <img src="<?=base_url()?>/assets/dist/img/icon/logo.png" alt="User Avatar"style="border: transparent;width: 100px;">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-6 border-right">
                  <div class="description-block">
                    <h5 class="description-header" id="actual-garuda" style="font-size: 28px;font-weight: bold"></h5>
                    <span class="description-text">Actual</span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="description-block">
                    <h5 class="description-header" id="target-garuda" style="font-size: 28px;font-weight: bold"></h5>
                    <span class="description-text">Target</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
          <div class="box box-widget widget-user" style="border-radius: 11px;">
            <div class="widget-user-header bg-navy">
              <h3 class="title-manufacture">CITILINK</h3>
            </div>
            <div class="widget-user-image">
              <img src="<?=base_url()?>/assets/dist/img/icon/logo.png" alt="User Avatar"style="border: transparent;width: 100px;">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-6 border-right">
                  <div class="description-block">
                    <h5 class="description-header" id="actual-citilink" style="font-size: 28px;font-weight: bold;"></h5>
                    <span class="description-text">Actual</span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="description-block">
                    <h5 class="description-header" id="target-citilink" style="font-size: 28px;font-weight: bold;"></h5>
                    <span class="description-text">Target</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
          <div class="box box-widget widget-user" style="cursor: pointer;border-radius: 11px;" id="garuda-hil">
            <div class="widget-user-header bg-navy">
              <h3 class="title-manufacture">GARUDA HIL</h3>
            </div>
            <div class="widget-user-image">
              <img src="<?=base_url()?>/assets/dist/img/icon/logo.png" alt="User Avatar"style="border: transparent;width: 100px;">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-12">
                  <div class="description-block">
                    <h5 class="description-header box-header with-border" id="count-open-garuda" style="font-size: 28px;font-weight: bold;"></h5>
                    <span class="description-text" style="margin-left: 13px;">OPEN</span>
                    <i class="fa fa-ellipsis-v pull-right" style="margin-top: 3px;margin-right: 12px;"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
          <div class="box box-widget widget-user" style="cursor: pointer;border-radius: 11px;" id="citilink-hil">
            <div class="widget-user-header bg-navy">
              <h3 class="title-manufacture">CITILINK HIL</h3>
            </div>
            <div class="widget-user-image">
              <img src="<?=base_url()?>/assets/dist/img/icon/logo.png" alt="User Avatar"style="border: transparent;width: 100px;">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-12">
                  <div class="description-block">
                    <h5 class="description-header box-header with-border" id="count-open-citilink" style="font-size: 28px;font-weight: bold;"></h5>
                    <span class="description-text" style="margin-left: 13px;">OPEN</span>
                    <i class="fa fa-ellipsis-v pull-right" style="margin-top: 3px;margin-right: 12px;"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        <div class="col-lg-2">
          <h4 class="box-title"><i class="fa fa-filter"></i> Filter Hil</h4>
            <div class="form-group">
              <select class="form-control select2" id="list_manufacture">
                <option value="00" selected="">--Select Customer--</option>
              </select>
            </div>
            <div class="form-group">
              <select class="form-control select2" id="list_aircraft_type">
                <option value="00" selected="">--Select Type--</option>
              </select>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="week1">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="week2">
            </div>
            <div class="form-group">
              <button type="button"class="btn btn-flat bg-navy" id="pencarian" style="width: 100%;" ><i class="fa fa-search"></i> Search</button>
            </div>
        </div>
        <div id="clas_ga" class="col-lg-5">
          <br>
          <div id="grafik-trend-hil-ga" style="height: 300px;width: 100%"></div>
        </div>
        <div id="clas_citi" class="col-lg-5">
          <br>
          <div id="grafik-trend-hil" style="height: 300px;width: 100%"></div>
        </div>
    </div>
  </div>

<!--//=======================================Trend Hil Data==============================================\\-->

<!--
  LAYOUT LAMA
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Trend Hil Data</h3>
      <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
        <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-3">
          <div class="form-group">
            <label>Aircraft </label>
            <select class="form-control select2" id="list_manufacture">
              <option value="00" selected="">--Select Customer--</option>
            </select>
          </div>
          <div class="form-group">
            <label>Aircraft Type</label>
            <select class="form-control select2" id="list_aircraft_type">
              <option value="00" selected="">--Select Type--</option>
            </select>
          </div>
          <div class="form-group">
            <label>Start Week</label>
            <input type="text" class="form-control" id="week1">
          </div>
          <div class="form-group">
            <label>End Week</label>
            <input type="text" class="form-control" id="week2">
          </div>
          <div class="form-group">
           <label></label>
            <button type="button"class="btn btn-flat bg-navy" id="pencarian" style="width: 100%;" ><i class="fa fa-search"></i> Search</button>
          </div>
        </div>
        <div class="col-lg-9">
          <br>
          <div id="grafik-trend-hil"></div>
        </div>
      </div>
    </div>
  </div> -->

</section>

<script>
var date    = new Date();
var month   = new Array();
  month[0]  = "January";
  month[1]  = "February";
  month[2]  = "March";
  month[3]  = "April";
  month[4]  = "May";
  month[5]  = "June";
  month[6]  = "July";
  month[7]  = "August";
  month[8]  = "September";
  month[9]  = "October";
  month[10] = "November";
  month[11] = "December";
var tgl     = date.getDate();
var bulan   = month[date.getUTCMonth()];
var tahun   = date.getUTCFullYear();
var base_url  = '<?php echo base_url(); ?>index.php/';


$(document).ready(function(){
  $('#week1').datepicker({
    format: "dd-mm-yyyy",
    autoclose: true,
  });
  $('#week2').datepicker({
    format: "dd-mm-yyyy",
    autoclose: true,
  });

  $('#date').html('&nbsp;'+ tgl + '-' + bulan + '-' + tahun);
  $(".select2").select2();
  table_garuda_open();
  table_citilink_open();
  target_maskapai();
  combo_manufacture();
  combo_aircraft_type();

  var myDate1     = new Date();
  var newMonth1   = myDate1.getMonth()+1;
  var prettyDate1 = myDate1.getDate() + '-' + (newMonth1 < 9 ? "0"+newMonth1:newMonth1) + '-' +  myDate1.getFullYear();
  var beforedate  = new Date();
  var priordate   = new Date();
  var count_date  = daysInMonth(priordate.getMonth(), priordate.getFullYear());
  priordate.setDate(beforedate.getDate()-count_date);
  var newMonth2   = priordate.getMonth()+1;
  var prettyDate2 = priordate.getDate() + '-' + (newMonth2 < 9 ? "0"+newMonth2:newMonth2) + '-' +  priordate.getFullYear();

  $('#week1').val(prettyDate2);
  $('#week2').val(prettyDate1);
  var week_start    = $('#week1').val();
  var week_complete = $('#week2').val();
  trend_hil_ga(week_start, week_complete);
  trend_hil_citi(week_start, week_complete);

    $('#garuda-hil').click(function(e) {
      $('#modal-garuda-hil').modal('show');
      $('#title-garuda-hil').html('Garuda Hil Open');
    });
    $('#citilink-hil').click(function(e) {
      $('#modal-citilink-hil').modal('show');
      $('#title-citilink-hil').html('Citilink Hil Open');
   });

});

  function back_garuda_hil(){
    $('#modal-detail-garuda').modal('hide');
    $('#modal-garuda-hil').modal('show');
  }

  function back_citilink_hil(){
    $('#modal-detail-citilink').modal('hide');
    $('#modal-citilink-hil').modal('show');
  }

  function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
  }

  $('#pencarian').click(function(e) {
    var week_start    = $('#week1').val();
    var week_complete = $('#week2').val();
    var get_month1    = week_start.split('-')[1];
    var get_month2    = week_complete.split('-')[1];
    var get_year1     = week_start.split('-')[2];
    var get_year2     = week_complete.split('-')[2];

      if(get_year1 == get_year2 && (get_month2 - get_month1) <= 3){
        var mfact = $("#list_manufacture").val();
        if(mfact == '00'){
          $("#clas_ga").removeClass("col-lg-10");
          $("#clas_ga").removeClass("hidden");
          $("#clas_citi").removeClass("col-lg-10");
          $("#clas_citi").removeClass("hidden");
          $("#clas_ga").addClass("col-lg-5");
          $("#clas_citi").addClass("col-lg-5");
          trend_hil_ga(week_start, week_complete);
          trend_hil_citi(week_start, week_complete);
        }else if(mfact == '12'){
          $("#clas_ga").removeClass("col-lg-5");
          $("#clas_ga").addClass("col-lg-10");
          $("#clas_citi").addClass("hidden");
          $("#clas_ga").removeClass("hidden");
          trend_hil_ga(week_start, week_complete);
        }else if(mfact == '11'){
          $("#clas_citi").removeClass("col-lg-5");
          $("#clas_citi").addClass("col-lg-10");
          $("#clas_ga").addClass("hidden");
          $("#clas_citi").removeClass("hidden");
          trend_hil_citi(week_start, week_complete);
        }
        // trend_hil(week_start, week_complete);
      }else{
        notif('error', "Sorry can't, month must be less than 3 months and year must be one period");
      }
  });

  function combo_manufacture(){
    var list_manufacture;
    $.ajax({
      url: base_url + 'hil/master_manufacture',
      type: "GET",
      dataType: "JSON",
      success: function (data) {
        list_manufacture = '<option value="00" selected="">--All Customer--</option>';
        $(data).each(function (key, val) {
          list_manufacture += '<option value="' + val.mn_id + '">' + val.mn_name + '</option>';
        });
        $('#list_manufacture').html(list_manufacture);
      }
    });
  }

  function combo_aircraft_type(){
    $('#list_manufacture').change(function () {
      var list_aircraft_type;
      var list_manufacture   = $('#list_manufacture').val();
      $.ajax({
        url: base_url + 'hil/master_type/' + list_manufacture,
        type: "GET",
        dataType: "JSON",
        success: function (data){
          if(list_manufacture =='00'){
            list_aircraft_type = '<option value="00">--Select Type--</option>';
          }else{
            list_aircraft_type += '<option value="000" selected>--All--</option>';
            $(data).each(function (key, val) {
              list_aircraft_type += '<option value="' + val.actype_id + '">' + val.actype + '</option>';
            });
          }
          $('#list_aircraft_type').html(list_aircraft_type);
        }
      });
    })
  }

  function target_maskapai(){
    $.ajax({
      url: base_url + 'hil/actual_target_manufacture',
      type: "GET",
      dataType: "JSON",
      success: function (data){
        var target      = data.target;
        if (data.garuda == undefined){
          var actual_garuda = 0;
        }else{
          var actual_garuda = data.garuda;
        }

        if (data.citilink == undefined){
          var actual_citilink = 0;
        }else{
          var actual_citilink = data.citilink;
        }

        $('#target-garuda').html(target+ ' <i class="fa fa-minus" style="color:#F2C573;"></i>');
        $('#target-citilink').html(target+ ' <i class="fa fa-minus" style="color:#F2C573;"></i>');
        if(data.garuda > target){
          $('#actual-garuda').html(actual_garuda+ ' <i class="fa fa-caret-down" style="color:#f24738"></i>');
          $('.value-actual-manufacture-garuda').css('color', '#f24738');
        }else{
          $('#actual-garuda').html(actual_garuda+ ' <i class="fa fa-caret-up" style="color:#8ec3a7"></i>');
          $('.value-actual-manufacture-garuda').css('color', '#8ec3a7');
        }
        if(data.citilink > target){
          $('#actual-citilink').html(actual_citilink+ ' <i class="fa fa-caret-down" style="color:#f24738"></i>');
           $('.value-actual-manufacture-citilink').css('color', '#f24738');
        }else{
          $('#actual-citilink').html(actual_citilink+ ' <i class="fa fa-caret-up" style="color:#8ec3a7"></i>');
           $('.value-actual-manufacture-citilink').css('color', '#8ec3a7');
        }
      }
    });
  }

  function table_garuda_open(){
    $.ajax({
      url: base_url + 'hil/count_garuda_open',
      type: "GET",
      dataType: "JSON",
      success: function (data){
        $('#count-open-garuda').html(data);
      }
    });

    $("#garuda-open-progress").DataTable({
      "destroy": true,
      "ordering": true,
      "searching": true,
      "paging": true,
      lengthChange: true,
      "aLengthMenu": [[5, 10, 20, 50, -1], [5, 10, 20, 50, "All"]],
      "iDisplayLength": 5,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'type'},
        {data : 'open'}
      ],
        ajax: {
          type: 'GET',
          url: base_url + 'hil/garuda_open',
          dataType: 'JSON',
          dataSrc : function (json) {
            if(json['status'] == 0){
              var return_data = new Array();
              var no=1;
                for(var i=0;i<json.length; i++){
                  return_data.push({
                    'no'      : '<center style="width: 150px;">No data available in table</center>',
                    'type'    : '<center</center>',
                    'open'    : '<center></center>'
                });
                no++;
              }
            return return_data;
            }else{
              var return_data = new Array();
              var no=1;
                for(var i=0;i<json['data'].length; i++){
                  return_data.push({
                    'no'      : '<center>'+no+'</center>',
                    'type'    : '<center>'+json['data'][i].actype+'</center>',
                    'open'    : '<center><a href="javascript:void(0)" style="color: #333;" onclick="modal_detail_garuda(\'' + json['data'][i].actype_id + '\', \'' + json['data'][i].actype + '\')">'+json['data'][i].count_data+'</a></center>'
                });
                no++;
              }
            return return_data;
          }
        },
      },
    });
  }

  function modal_detail_garuda(id, tipe){
    //$('#modal-garuda-hil').modal('hide');
    $('#modal-detail-garuda').modal('show');
    $('#title-detail-garuda').html(tipe);
    var table = $("#table-detail-garuda-open-progress").DataTable({
      "destroy": true,
      "ordering": true,
      "searching": true,
      "paging": true,
      lengthChange: true,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'reg'},
        {data : 'notification'},
        {data : 'start'},
        {data : 'due'},
        {data : 'desc'},
        {data : 'status'}
      ],
      ajax: {
        type: 'GET',
        url: base_url + 'hil/garuda_detail_open/' + id,
        dataType: 'JSON',
        dataSrc : function (json) {
          var return_data = new Array();
          var no=1;
            for(var i=0;i<json.length; i++){
              if(json[i].due_date){
                var convert_due_date = json[i].due_date.split('-');
                convert_due_date     = convert_due_date[2]+'-'+convert_due_date[1]+'-'+convert_due_date[0];
              }
              if(json[i].start_date){
                var convert_start_date = json[i].start_date.split('-');
                convert_start_date     = convert_start_date[2]+'-'+convert_start_date[1]+'-'+convert_start_date[0];
              }
              return_data.push({
                'no'          : no,
                'reg'         : json[i].nm_reg,
                'notification': json[i].notification,
                'start'       : convert_start_date,
                'due'         : convert_due_date,
                'desc'        : json[i].description,
                'status'      : json[i].system_status
              });
              no++;
            }
            return return_data;
          },
        },
      createdRow: function(row, data, index){
        $(row).css('text-align', 'center');
        var desc              = data['desc'];
        var notification      = data['notification'];
        var status            = data['status'];
        var due_Date          = data['due'];
        due_Date              = due_Date.split('-');
        due_Date              = due_Date[2]+'-'+due_Date[1]+'-'+due_Date[0];
        var MyDate            = new Date();
        var MyDateString;
        MyDate.setDate(MyDate.getDate());
        MyDateString          = MyDate.getFullYear() + '-'+ ('0' + (MyDate.getMonth()+1)).slice(-2) + '-'
                                +  ('0' + MyDate.getDate()).slice(-2);

          var due_date        = due_Date.split('-');
          var today           = MyDateString.split('-');
          var dueDate         = new Date();
          dueDate.setFullYear(due_date[0],(due_date[1] - 1 ),due_date[2]);
          var todayDate=new Date();
          todayDate.setFullYear(today[0],(today[1] - 1 ),today[2]);

          if(todayDate >= dueDate){
            $(row).find('td').css('background-color', '#F24738');
            $(row).find('td').css('cursor', 'pointer');
            $(row).css('color', '#fff');
             $(row).on('click', function () {
              $.ajax({
                url: base_url + 'hil/mitigasi_hil/' + notification,
                type: "GET",
                dataType: "JSON",
                success: function (data){
                    for (var i = 0; i < data.length; i++) {
                    notif('warning', '<h3>MITIGATION !!!</h3><table class="table table-bordered"><tr><td>Why</td><td>'+data[i]['mitigasi_why']+'</td></tr><tr><td>Solution</td><td>'+data[i]['mitigasi_solution']+'</td></tr></table>');
                    }
                }
              })
            });
          }else{
            $(row).find('td').css('background-color', '#fff');
            $(row).css('color', '#333');
          }
        },
    });
  }

  function table_citilink_open(){
    $.ajax({
      url: base_url + 'hil/count_citilink_open',
      type: "GET",
      dataType: "JSON",
      success: function (data){
        $('#count-open-citilink').html(data);
      }
    });
    $("#citilink-open-progress").DataTable({
      "destroy": true,
      "ordering": true,
      "searching": true,
      "paging": true,
      "aLengthMenu": [[5, 10, 20, 50, -1], [5, 10, 20, 50, "All"]],
      "iDisplayLength": 5,
      lengthChange: true,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'type'},
        {data : 'open'}
      ],
        ajax: {
          type: 'GET',
          url: base_url + 'hil/citilink_open',
          dataType: 'JSON',
          dataSrc : function (json) {
            if(json['status'] == 0){
              var return_data = new Array();
              var no=1;
                for(var i=0;i<json.length; i++){
                  return_data.push({
                    'no'      : '<center style="width: 150px;">No data available in table</center>',
                    'type'    : '<center</center>',
                    'open'    : '<center></center>'
                });
                no++;
              }
            return return_data;
            }else{
              var return_data = new Array();
              var no=1;
                for(var i=0;i<json['data'].length; i++){
                  return_data.push({
                    'no'      : '<center>'+no+'</center>',
                    'type'    : '<center>'+json['data'][i].actype+'</center>',
                    'open'    : '<center><a href="javascript:void(0)" style="color: #333;" onclick="modal_detail_citilink(\'' + json['data'][i].actype_id + '\', \'' + json['data'][i].actype + '\')" >'+json['data'][i].count_data+'</a></center>'
                });
                no++;
              }
            return return_data;
            }
        },
      },
    });
  }

  function modal_detail_citilink(id, tipe){
    //$('#modal-citilink-hil').modal('hide');
    $('#modal-detail-citilink').modal('show');
    $('#title-detail-citilink').html(tipe);
    var table = $("#table-detail-citilink-open-progress").DataTable({
      "destroy": true,
      "ordering": true,
      "searching": true,
      "paging": true,
      lengthChange: true,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'reg'},
        {data : 'notification'},
        {data : 'start'},
        {data : 'due'},
        {data : 'desc'},
        {data : 'status'}
      ],
      ajax: {
        type: 'GET',
        url: base_url + 'hil/citilink_detail_open/' + id,
        dataType: 'JSON',
        dataSrc : function (json) {
          var return_data = new Array();
          var no=1;
            for(var i=0;i<json.length; i++){
              if(json[i].due_date){
                var convert_due_date = json[i].due_date.split('-');
                convert_due_date     = convert_due_date[2]+'-'+convert_due_date[1]+'-'+convert_due_date[0];
              }
              if(json[i].start_date){
                var convert_start_date = json[i].start_date.split('-');
                convert_start_date     = convert_start_date[2]+'-'+convert_start_date[1]+'-'+convert_start_date[0];
              }
              return_data.push({
                'no'          : no,
                'reg'         : json[i].nm_reg,
                'notification': json[i].notification,
                'start'       : convert_start_date,
                'due'         : convert_due_date,
                'desc'        : json[i].description,
                'status'      : json[i].system_status
              });
              no++;
            }
            return return_data;
          },
        },
      createdRow: function(row, data, index){
        $(row).css('text-align', 'center');
        var desc              = data['desc'];
        var notification      = data['notification'];
        var status            = data['status'];
        var due_Date          = data['due'];
        due_Date              = due_Date.split('-');
        due_Date              = due_Date[2]+'-'+due_Date[1]+'-'+due_Date[0]
        var MyDate            = new Date();
        var MyDateString;
        MyDate.setDate(MyDate.getDate());
        MyDateString          = MyDate.getFullYear() + '-'+ ('0' + (MyDate.getMonth()+1)).slice(-2) + '-'
                                +  ('0' + MyDate.getDate()).slice(-2);
          var due_date        = due_Date.split('-');
          var today           = MyDateString.split('-');
          var dueDate         = new Date();
          dueDate.setFullYear(due_date[0],(due_date[1] - 1 ),due_date[2]);
          var todayDate       = new Date();
          todayDate.setFullYear(today[0],(today[1] - 1 ),today[2]);

          if(todayDate >= dueDate){
            $(row).find('td').css('background-color', '#F24738');
            $(row).find('td').css('cursor', 'pointer');
            $(row).css('color', '#fff');
             $(row).on('click', function () {
              $.ajax({
                url: base_url + 'hil/mitigasi_hil/' + notification,
                type: "GET",
                dataType: "JSON",
                success: function (data){
                    for (var i = 0; i < data.length; i++) {
                    notif('warning', '<h3>MITIGATION !!!</h3><table class="table table-bordered"><tr><td>Why</td><td>'+data[i]['mitigasi_why']+'</td></tr><tr><td>Solution</td><td>'+data[i]['mitigasi_solution']+'</td></tr></table>');
                    }
                }
              })
            });
          }else{
            $(row).find('td').css('background-color', '#fff');
            $(row).css('color', '#333');

          }
        },
    });
  }

  function trend_hil_ga(week1, week2){
    var id_manufacture = '12'; //$('#list_manufacture').val()
    var id_type        = $('#list_aircraft_type').val();
    // var yAxisLimit     = [0, 50, 100, 150, 200, 250, 300, 350, 400];

    $.ajax({
      url: base_url + 'hil/data_tren_hil/' + id_manufacture + '/' + id_type + '/' + week1 + '/' + week2,
      type: "GET",
      dataType: "JSON",
      error: function(){
        notif('error', 'Connection disorders occurred, Please try again');
      },
      success: function (data) {
        notif('success', 'Data trend hil successfully displayed');
        var grafik =  Highcharts.chart('grafik-trend-hil-ga', {
          chart: {
            type: 'line'
          },
          exporting: {
            enabled: false
          },
          title: {
            text: 'Trend Data Hil',
            margin: 0
          },
          credits:{
            enabled: false
          },
          yAxis: {
            min: 0,
              // tickPositioner: function() {
              // return yAxisLimit;
              // },
            title: {
              text: 'Data',
              style: {
                fontSize: '15px',
                fontWeight: 'bold',
                color: '#333333'
              }
            },
          },
          xAxis: {
            categories: data.categories,
          },
          legend: {
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0,
            enabled:true,
             itemStyle: {
               fontSize:'15px',
              },
        },
          plotOptions: {
            series: {
              label: {
                connectorAllowed: false
              },
              dataLabels: {
                enabled: true
              },
              cursor: 'pointer',
              point: {
                    events: {
                        click: function (e) {
                          var id    = 12;
                          var name  = this.series.name;
                          var week_start    = this.category;
                          var week_plus_one = this.category+1;
                          var week_end      = grafik.xAxis[0].categories;
                          week_end          = Math.max.apply(Math,week_end);
                          modal_detail(id, name, id_type, week_start, week_plus_one, week_end);
                        },
                    }
                },
            }
          },
          series: [
            {
              type: 'areaspline',
              fillColor: {
                linearGradient: [0,0,0,200],
                stops: [
                  [0, "#05354d"],
                  [1, "#fff3"]
                ]
              },
              lineWidth: 1,
              name: "Garuda",
              data: data.data_garuda,
              color : "#05354d"
            },
          ],

          responsive: {
            rules: [{
              condition: {
                maxWidth: 500
              },
              // chartOptions: {
              //   legend: {
              //     layout: 'horizontal',
              //     align: 'center',
              //     itemMarginTop: 50,
              //     verticalAlign: 'bottom'
              //   }
              // }
            }]
          }
        });
      }
    });

  }

  function trend_hil_citi(week1, week2){
    var id_manufacture = '11'; //$('#list_manufacture').val()
    var id_type        = $('#list_aircraft_type').val();
    // var yAxisLimit     = [0, 50, 100, 150, 200, 250, 300, 350, 400];

    $.ajax({
      url: base_url + 'hil/data_tren_hil/' + id_manufacture + '/' + id_type + '/' + week1 + '/' + week2,
      type: "GET",
      dataType: "JSON",
      error: function(){
        notif('error', 'Connection disorders occurred, Please try again');
      },
      success: function (data) {
        notif('success', 'Data trend hil successfully displayed');
        var grafik =  Highcharts.chart('grafik-trend-hil', {
          chart: {
            type: 'line'
          },
          exporting: {
            enabled: false
          },
          title: {
            text: 'Trend Data Hil',
            margin: 0
          },
          credits:{
            enabled: false
          },
          yAxis: {
            min: 0,
              // tickPositioner: function() {
              // return yAxisLimit;
              // },
            title: {
              text: 'Data',
              style: {
                fontSize: '15px',
                fontWeight: 'bold',
                color: '#333333'
              }
            },
          },
          xAxis: {
            categories: data.categories,
          },
          legend: {
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0,
            enabled:true,
             itemStyle: {
               fontSize:'15px',
              },
        },
          plotOptions: {
            series: {
              label: {
                connectorAllowed: false
              },
              dataLabels: {
                enabled: true
              },
              cursor: 'pointer',
              point: {
                    events: {
                        click: function (e) {
                          var id    = 11;
                          var name  = this.series.name;
                          var week_start    = this.category;
                          var week_plus_one = this.category+1;
                          var week_end      = grafik.xAxis[0].categories;
                          week_end          = Math.max.apply(Math,week_end);
                          modal_detail(id, name, id_type, week_start, week_plus_one, week_end);
                        },
                    }
                },
            }
          },
          series: [
            {
              type: 'areaspline',
              fillColor: {
                linearGradient: [0,0,0,200],
                stops: [
                  [0, "#8ec3a7"],
                  [1, "#fff3"]
                ]
              },
              // lineWidth: 1,
              name: "Citilink",
              data: data.data_citilink,
              color : "#8ec3a7"
            },
          ],

          responsive: {
            rules: [{
              condition: {
                maxWidth: 500
              },
              // chartOptions: {
              //   legend: {
              //     layout: 'horizontal',
              //     align: 'center',
              //     itemMarginTop: 50,
              //     verticalAlign: 'bottom'
              //   }
              // }
            }]
          }
        });
      }
    });
  }

  function trend_hil(week1, week2){
    var id_manufacture = $('#list_manufacture').val();
    var id_type        = $('#list_aircraft_type').val();
    var yAxisLimit     = [0, 50, 100, 150, 200, 250, 300, 350, 400];

    // ALL CUSTOMER
    if(id_manufacture == 00 && id_type == 00){
    $.ajax({
        url: base_url + 'hil/data_tren_hil/' + id_manufacture + '/' + id_type + '/' + week1 + '/' + week2,
        type: "GET",
        dataType: "JSON",
        error: function(){
          notif('error', 'Connection disorders occurred, Please try again');
        },
        success: function (data) {
          notif('success', 'Data trend hil successfully displayed');
          var grafik =  Highcharts.chart('grafik-trend-hil', {
            chart: {
              type: 'line'
            },
            exporting: {
              enabled: false
            },
            title: {
              text: 'Trend Data Hil',
            margin: 0
            },
            credits:{
              enabled: false
            },
            yAxis: {
              min: 0,
                tickPositioner: function() {
                return yAxisLimit;
                },
              title: {
                text: 'Data',
                style: {
                  fontSize: '15px',
                  fontWeight: 'bold',
                  color: '#333333'
                }
              },
            },
            xAxis: {
              categories: data.categories,
            },
          legend: {
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0,
            enabled:true,
             itemStyle: {
               fontSize:'15px',
              },
        },
            plotOptions: {
              series: {
                label: {
                  connectorAllowed: false
                },
                dataLabels: {
                  enabled: true
                },
                cursor: 'pointer',
                point: {
                      events: {
                          click: function (e) {
                            if (this.series.name == 'Garuda'){
                              var id    = 12;
                              var name  = this.series.name;
                            }else if (this.series.name == 'Citilink'){
                              var id    = 11;
                              var name  = this.series.name;
                            }
                            var week_start    = this.category;
                            var week_plus_one = this.category+1;
                            var week_end      = grafik.xAxis[0].categories;
                            week_end          = Math.max.apply(Math,week_end);
                            modal_detail(id, name, id_type, week_start, week_plus_one, week_end);
                          },
                      }
                  },
              }
            },
            series: [
              {
                type: 'areaspline',
                fillColor: {
                  linearGradient: [0,0,0,200],
                  stops: [
                    [0, "#05354d"],
                    [1, "#fff3"]
                  ]
                },
                lineWidth: 1,
                name: "Garuda",
                data: data.data_garuda,
                color : "#05354d"
              },
              {
                type: 'areaspline',
                fillColor: {
                  linearGradient: [0,0,0,200],
                  stops: [
                    [0, "#8ec3a7"],
                    [1, "#fff3"]
                  ]
                },
                lineWidth: 1,
                name: "Citilink",
                data: data.data_citilink,
                color : "#8ec3a7"
              },
            ],

            responsive: {
              rules: [{
                condition: {
                  maxWidth: 500
                },
                // chartOptions: {
                //   legend: {
                //     layout: 'horizontal',
                //     align: 'center',
                //     itemMarginTop: 50,
                //     verticalAlign: 'bottom'
                //   }
                // }
              }]
            }
          });
        }
      });
    // CITILINK
    }else if(id_manufacture == 11){
      if(id_type ==00){
        $.ajax({
          url: base_url + 'hil/data_tren_hil/' + id_manufacture + '/' + id_type + '/' + week1 + '/' + week2,
          type: "GET",
          dataType: "JSON",
          error: function(){
            notif('error', 'Connection disorders occurred, Please try again');
          },
          success: function (data) {
            notif('success', 'Data trend hil successfully displayed');
            var grafik =  Highcharts.chart('grafik-trend-hil', {
              chart: {
                type: 'line'
              },
              exporting: {
                enabled: false
              },
              title: {
                text: 'Trend Data Hil',
            margin: 0
              },
              credits:{
                enabled: false
              },
              yAxis: {
                min: 0,
                  // tickPositioner: function() {
                  // return yAxisLimit;
                  // },
                title: {
                  text: 'Data',
                  style: {
                    fontSize: '15px',
                    fontWeight: 'bold',
                    color: '#333333'
                  }
                },
              },
              xAxis: {
                categories: data.categories,
              },
          legend: {
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0,
            enabled:true,
             itemStyle: {
               fontSize:'15px',
              },
        },
              plotOptions: {
                series: {
                  label: {
                    connectorAllowed: false
                  },
                  dataLabels: {
                    enabled: true
                  },
                  cursor: 'pointer',
                  point: {
                        events: {
                            click: function (e) {
                              var id    = 11;
                              var name  = this.series.name;
                              var week_start    = this.category;
                              var week_plus_one = this.category+1;
                              var week_end      = grafik.xAxis[0].categories;
                              week_end          = Math.max.apply(Math,week_end);
                              modal_detail(id, name, id_type, week_start, week_plus_one, week_end);
                            },
                        }
                    },
                }
              },
              series: [
                {
                  type: 'areaspline',
                  fillColor: {
                    linearGradient: [0,0,0,200],
                    stops: [
                      [0, "#8ec3a7"],
                      [1, "#fff3"]
                    ]
                  },
                  lineWidth: 1,
                  name: "Citilink",
                  data: data.data_citilink,
                  color : "#8ec3a7"
                },
              ],

              responsive: {
                rules: [{
                  condition: {
                    maxWidth: 500
                  },
                  // chartOptions: {
                  //   legend: {
                  //     layout: 'horizontal',
                  //     align: 'center',
                  //     itemMarginTop: 50,
                  //     verticalAlign: 'bottom'
                  //   }
                  // }
                }]
              }
            });
          }
        });
      }else{
        $.ajax({
          url: base_url + 'hil/data_tren_hil/' + id_manufacture + '/' + id_type + '/' + week1 + '/' + week2,
          type: "GET",
          dataType: "JSON",
          error: function(){
            notif('error', 'Connection disorders occurred, Please try again');
          },
          success: function (data) {
            notif('success', 'Data trend hil successfully displayed');
            var grafik =  Highcharts.chart('grafik-trend-hil', {
              chart: {
                type: 'line'
              },
              exporting: {
                enabled: false
              },
              title: {
                text: 'Trend Data Hil',
            margin: 0
              },
              credits:{
                enabled: false
              },
              yAxis: {
                min: 0,
                  // tickPositioner: function() {
                  // return yAxisLimit;
                  // },
                title: {
                  text: 'Data',
                  style: {
                    fontSize: '15px',
                    fontWeight: 'bold',
                    color: '#333333'
                  }
                },
              },
              xAxis: {
                categories: data.categories,
              },
          legend: {
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0,
            enabled:true,
             itemStyle: {
               fontSize:'15px',
              },
        },
              plotOptions: {
                series: {
                  label: {
                    connectorAllowed: false
                  },
                  dataLabels: {
                    enabled: true
                  },
                  cursor: 'pointer',
                  point: {
                        events: {
                            click: function (e) {
                              var id    = 11;
                              var name  = this.series.name;
                              var week_start    = this.category;
                              var week_plus_one = this.category+1;
                              var week_end      = grafik.xAxis[0].categories;
                              week_end          = Math.max.apply(Math,week_end);
                              modal_detail(id, name, id_type, week_start, week_plus_one, week_end);
                            },
                        }
                    },
                }
              },
              series: [
                {
                  type: 'areaspline',
                  fillColor: {
                    linearGradient: [0,0,0,200],
                    stops: [
                      [0, "#8ec3a7"],
                      [1, "#fff3"]
                    ]
                  },
                  lineWidth: 1,
                  name: "Citilink",
                  data: data.data_citilink,
                  color : "#8ec3a7"
                },
              ],

              responsive: {
                rules: [{
                  condition: {
                    maxWidth: 500
                  },
                  // chartOptions: {
                  //   legend: {
                  //     layout: 'horizontal',
                  //     align: 'center',
                  //     itemMarginTop: 50,
                  //     verticalAlign: 'bottom'
                  //   }
                  // }
                }]
              }
            });
          }
        });
      }
    // GARUDA
    }else if(id_manufacture == 12){
      if(id_type ==00){
        $.ajax({
          url: base_url + 'hil/data_tren_hil/' + id_manufacture + '/' + id_type + '/' + week1 + '/' + week2,
          type: "GET",
          dataType: "JSON",
          error: function(){
            notif('error', 'Connection disorders occurred, Please try again');
          },
          success: function (data) {
            notif('success', 'Data trend hil successfully displayed');
            var grafik =  Highcharts.chart('grafik-trend-hil', {
              chart: {
                type: 'line'
              },
              exporting: {
                enabled: false
              },
              title: {
                text: 'Trend Data Hil',
            margin: 0
              },
              credits:{
                enabled: false
              },
              yAxis: {
                min: 0,
                  tickPositioner: function() {
                  return yAxisLimit;
                  },
                title: {
                  text: 'Data',
                  style: {
                    fontSize: '15px',
                    fontWeight: 'bold',
                    color: '#333333'
                  }
                },
              },
              xAxis: {
                categories: data.categories,
              },
          legend: {
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0,
            enabled:true,
             itemStyle: {
               fontSize:'15px',
              },
        },
              plotOptions: {
                series: {
                  label: {
                    connectorAllowed: false
                  },
                  dataLabels: {
                    enabled: true
                  },
                  cursor: 'pointer',
                  point: {
                        events: {
                            click: function (e) {
                              var id    = 12;
                              var name  = this.series.name;
                              var week_start    = this.category;
                              var week_plus_one = this.category+1;
                              var week_end      = grafik.xAxis[0].categories;
                              week_end          = Math.max.apply(Math,week_end);
                              modal_detail(id, name, id_type, week_start, week_plus_one, week_end);
                            },
                        }
                    },
                }
              },
              series: [
                {
                  type: 'areaspline',
                  fillColor: {
                    linearGradient: [0,0,0,200],
                    stops: [
                      [0, "#05354d"],
                      [1, "#fff3"]
                    ]
                  },
                  lineWidth: 1,
                  name: "Garuda",
                  data: data.data_garuda,
                  color : "#05354d"
                },
              ],

              responsive: {
                rules: [{
                  condition: {
                    maxWidth: 500
                  },
                  // chartOptions: {
                  //   legend: {
                  //     layout: 'horizontal',
                  //     align: 'center',
                  //     itemMarginTop: 50,
                  //     verticalAlign: 'bottom'
                  //   }
                  // }
                }]
              }
            });
          }
        });
      }else{
        $.ajax({
          url: base_url + 'hil/data_tren_hil/' + id_manufacture + '/' + id_type + '/' + week1 + '/' + week2,
          type: "GET",
          dataType: "JSON",
          error: function(){
            notif('error', 'Connection disorders occurred, Please try again');
          },
          success: function (data) {
            notif('success', 'Data trend hil successfully displayed');
            var grafik =  Highcharts.chart('grafik-trend-hil', {
              chart: {
                type: 'line'
              },
              exporting: {
                enabled: false
              },
              title: {
                text: 'Trend Data Hil',
            margin: 0
              },
              credits:{
                enabled: false
              },
              yAxis: {
                min: 0,
                  tickPositioner: function() {
                  return yAxisLimit;
                  },
                title: {
                  text: 'Data',
                  style: {
                    fontSize: '15px',
                    fontWeight: 'bold',
                    color: '#333333'
                  }
                },
              },
              xAxis: {
                categories: data.categories,
              },
          legend: {
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0,
            enabled:true,
             itemStyle: {
               fontSize:'15px',
              },
        },
              plotOptions: {
                series: {
                  label: {
                    connectorAllowed: false
                  },
                  dataLabels: {
                    enabled: true
                  },
                  cursor: 'pointer',
                  point: {
                        events: {
                            click: function (e) {
                              var id    = 12;
                              var name  = this.series.name;
                              var week_start    = this.category;
                              var week_plus_one = this.category+1;
                              var week_end      = grafik.xAxis[0].categories;
                              week_end          = Math.max.apply(Math,week_end);
                              modal_detail(id, name, id_type, week_start, week_plus_one, week_end);
                            },
                        }
                    },
                }
              },
              series: [
                {
                  type: 'areaspline',
                  fillColor: {
                    linearGradient: [0,0,0,200],
                    stops: [
                      [0, "#05354d"],
                      [1, "#fff3"]
                    ]
                  },
                  lineWidth: 1,
                  name: "Garuda",
                  data: data.data_garuda,
                  color : "#05354d"
                },
              ],

              responsive: {
                rules: [{
                  condition: {
                    maxWidth: 500
                  },
                  // chartOptions: {
                  //   legend: {
                  //     layout: 'horizontal',
                  //     align: 'center',
                  //     itemMarginTop: 50,
                  //     verticalAlign: 'bottom'
                  //   }
                  // }
                }]
              }
            });
          }
        });
      }
    }
  }

  function modal_detail(id, name, id_type, week1, week_plus_one, week2){
    $('#modal-detail-grafik').modal('show');
    detail_table_grafik(id, name, id_type, week1, week_plus_one, week2);
  }

  function detail_table_grafik(id, name, id_type, week1, week_plus_one, week2){
    $('#modal-detail-grafik').modal('show');
    $('#detail-tittle-grafik').html(name + ' | Week : ' + week1);
    var table = $("#table-detail-grafik").DataTable({
      "destroy": true,
      "ordering": true,
      "searching": true,
      "paging": true,
      lengthChange: true,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'reg'},
        {data : 'notification'},
        {data : 'start'},
        {data : 'due'},
        {data : 'desc'},
        {data : 'status'}
      ],
      ajax: {
        type: 'GET',
        url: base_url + 'hil/detail_grafik/' + id + '/' + id_type + '/' + week1 + '/' + week_plus_one + '/' + week2,
        dataType: 'JSON',
        dataSrc : function (json) {
          var return_data = new Array();
          var no=1;
            for(var i=0;i<json.length; i++){
              if(json[i].due_date){
                var convert_due_date = json[i].due_date.split('-');
                convert_due_date     = convert_due_date[2]+'-'+convert_due_date[1]+'-'+convert_due_date[0];
              }
              if(json[i].start_date){
                var convert_start_date = json[i].start_date.split('-');
                convert_start_date     = convert_start_date[2]+'-'+convert_start_date[1]+'-'+convert_start_date[0];
              }
              return_data.push({
                'no'          : no,
                'reg'         : json[i].nm_reg,
                'notification': json[i].notification,
                'start'       : convert_start_date,
                'due'         : convert_due_date,
                'desc'        : json[i].description,
                'status'      : json[i].system_status
              });
              no++;
            }
            return return_data;
          },
        },
      createdRow: function(row, data, index){
        $(row).css('text-align', 'center');
        var desc              = data['desc'];
        var status            = data['status'];
        var notification      = data['notification'];
        var due_Date          = data['due'];
        due_Date              = due_Date.split('-');
        due_Date              = due_Date[2]+'-'+due_Date[1]+'-'+due_Date[0]
        var MyDate            = new Date();
        var MyDateString;
        MyDate.setDate(MyDate.getDate());
        MyDateString          = MyDate.getFullYear() + '-'+ ('0' + (MyDate.getMonth()+1)).slice(-2) + '-'
                                +  ('0' + MyDate.getDate()).slice(-2);
          var due_date        = due_Date.split('-');
          var today           = MyDateString.split('-');
          var dueDate         = new Date();
          dueDate.setFullYear(due_date[0],(due_date[1] - 1 ),due_date[2]);
          var todayDate       = new Date();
          todayDate.setFullYear(today[0],(today[1] - 1 ),today[2]);


          if(todayDate >= dueDate){
            $(row).find('td').css('background-color', '#fff');
            $(row).find('td').css('cursor', 'pointer');
            $(row).css('color', '#F24738');
             $(row).on('click', function () {
              $.ajax({
                url: base_url + 'hil/mitigasi_hil/' + notification,
                type: "GET",
                dataType: "JSON",
                success: function (data){
                    for (var i = 0; i < data.length; i++) {
                    notif('warning', '<h3>MITIGATION !!!</h3><table class="table table-bordered"><tr><td>Why</td><td>'+data[i]['mitigasi_why']+'</td></tr><tr><td>Solution</td><td>'+data[i]['mitigasi_solution']+'</td></tr></table>');
                    }
                }
              })
            });
          }else{
            $(row).find('td').css('background-color', '#fff');
            $(row).css('color', '#333');
          }
        },
    });
  }
</script>

<!-- MODAL HIL GARUDA -->
<div class="modal bounceIn" id="modal-garuda-hil" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title-panel" id="title-garuda-hil"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive" style="overflow-x:hidden">
              <table class="table table-hover table-bordered table-gmf " id="garuda-open-progress" style="width: 100%">
                <thead class="bg-navy">
                  <tr>
                    <th class="global-text-center">No</th>
                    <th class="global-text-center">Aircraft Type</th>
                    <th class="global-text-center">Open</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- MODAL DETAIL HIL GARUDA -->
<div class="modal bounceIn" id="modal-detail-garuda" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title-panel" id="title-detail-garuda"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <button type="button" class="btn bg-grey pull-right back" onclick="back_garuda_hil()">
            <i class="fa  fa-arrow-circle-left"></i> Back
            </button>
          </div>
          <div class="col-md-12">
            <br>
            <div class="table-responsive" style="overflow-x:hidden">
              <table style="width:100%;" class="table table-hover table-bordered table-gmf" id="table-detail-garuda-open-progress">
                <thead class="bg-navy">
                  <tr>
                    <th class="global-text-center">No</th>
                    <th class="global-text-center">Aircraft Reg</th>
                    <th class="global-text-center">Notification</th>
                    <th class="global-text-center">Start Date</th>
                    <th class="global-text-center">Due Date</th>
                    <th class="global-text-center">Description</th>
                    <th class="global-text-center">Status</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- MODAL HIL CITILINK -->
<div class="modal bounceIn" id="modal-citilink-hil" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title-panel" id="title-citilink-hil"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive" style="overflow-x:hidden">
              <table class="table table-hover table-bordered table-gmf " id="citilink-open-progress" style="width: 100%">
                <thead class="bg-navy">
                  <tr>
                    <th class="global-text-center">No</th>
                    <th class="global-text-center">Aircraft Type</th>
                    <th class="global-text-center">Open</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- MODAL DETAIL HIL CITILINK -->
<div class="modal bounceIn" id="modal-detail-citilink" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title-panel" id="title-detail-citilink"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <button type="button" class="btn bg-grey pull-right back" onclick="back_citilink_hil()">
            <i class="fa  fa-arrow-circle-left"></i> Back
            </button>
          </div>
          <div class="col-md-12">
            <br>
            <div class="table-responsive" style="overflow-x:hidden">
              <table style="width:100%;" class="table table-hover table-bordered table-gmf" id="table-detail-citilink-open-progress">
                <thead class="bg-navy">
                  <tr>
                    <th class="global-text-center">No</th>
                    <th class="global-text-center">Aircraft Reg</th>
                    <th class="global-text-center">Notification</th>
                    <th class="global-text-center">Start Date</th>
                    <th class="global-text-center">Due Date</th>
                    <th class="global-text-center">Description</th>
                    <th class="global-text-center">Status</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- MODAL DETAIL GRAFIK TREND HIL -->
<div class="modal bounceIn" id="modal-detail-grafik" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0f2233;color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-detail-title-panel" id="detail-tittle-grafik"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive" style="overflow-x:hidden">
              <table style="width:100%;" class="table table-hover table-bordered" id="table-detail-grafik">
                <thead class="bg-navy">
                  <tr>
                    <th class="global-text-center">No</th>
                    <th class="global-text-center">Aircraft Reg</th>
                    <th class="global-text-center">Notification</th>
                    <th class="global-text-center">Start Date</th>
                    <th class="global-text-center">Due Date</th>
                    <th class="global-text-center">Description</th>
                    <th class="global-text-center">Status</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
