<style media="screen">
  .wellcome{
    position: relative;
    /* margin-top: 20px; */
    /* margin-bottom: 20px; */
    /* overflow: hidden; */
  }

  .page {
  padding: 40px;

  }
  /* html, body {
    height: 100%;
    margin: 0;
    padding: 0;
    font-family: Helvetica, Arial, sans-serif;
  } */

  #splash {
    background-color: #0f2233;
    position: absolute;
    margin: auto;
    top: -100;
    left: 0;
    right: 0;
    width: 100%;
    height: 100%;
    animation: splash 3s ease-in;
    animation-fill-mode: forwards;
    -webkit-animation-fill-mode: forwards;
  }

  #loader {
    position: absolute;
    left: 50%;
    top: 0;
    transform: translate(-50%,0);
  }

  #loader:after {
    content: '';
    position: absolute;
    left: 50%;
    margin-left: -8px;
    bottom: -170px;
    width: 3px;
    background: #fff;
    background: linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 50%, rgba(255,255,255,0) 100%);
    height: 200px;
  }

  #loader:before {
    content: '';
    position: absolute;
    left: 50%;
    margin-left: 8px;
    bottom: -190px;
    width: 3px;
    background: #000;
    background: linear-gradient(to bottom, rgba(0,0,0,.2) 0%, rgba(0,0,0,.2) 50%, rgba(0,0,0,0) 100%);
    height: 200px;
  }

  #splash .anim {
    height: 100%;
    position: absolute;
    left: 50%;
    width: 100px;
    transform: translate(-50%,100%);
    animation: loader 4s linear;
    animation-fill-mode: forwards;
    -webkit-animation-fill-mode: forwards;
  }

  @keyframes loader {
  0% {
    transform: translate(-50%,110%);
  }
  30% {
    transform: translate(-50%,50%);
  }
  100% {
    transform: translate(-50%,0%);
  }
  }

  @keyframes splash {
  0% {
    transform: translate(0%,0%);
  }
  50% {
    transform: translate(0%,0%);
  }
  100% {
    transform: translate(0%,-200%);
  }
  }

</style>

<div class="wellcome">
  <div id="splash">
    <div class="anim">
      <div id="loader">
        <svg version="1.1" width="60px" height="70px" viewBox="0 0 60 70">
          <defs>
            <filter id="f1" x="0" y="0">
              <feGaussianBlur in="SourceGraphic" stdDeviation="2" />
            </filter>
          </defs>
          <g id="airplane">
            <path fill="#fff" d="M0.677,20.977l4.355,1.631c0.281,0.104,0.579,0.162,0.88,0.16l9.76-0.004L30.46,41.58c0.27,0.34,0.679,0.545,1.112,0.541
            h1.87c0.992,0,1.676-0.992,1.322-1.918l-6.643-17.439l6.914,0.002l6.038,6.037c0.265,0.266,0.624,0.412,0.999,0.418l1.013-0.004
            c1.004-0.002,1.684-1.012,1.312-1.938l-2.911-7.277l2.912-7.278c0.372-0.928-0.313-1.941-1.313-1.938h1.017
            c-0.375,0-0.732,0.15-0.996,0.414l-6.039,6.039h-6.915l6.646-17.443c0.354-0.926-0.33-1.916-1.321-1.914l-1.87-0.004
            c-0.439,0.004-0.843,0.203-1.112,0.543L15.677,17.24l-9.765-0.002c-0.3,0.002-0.597,0.055-0.879,0.16L0.678,19.03
            C-0.225,19.36-0.228,20.637,0.677,20.977z" transform="translate(44,0) rotate(90 0 0)" />
          </g>
          <g id="shadow" transform="scale(.9)">
            <path fill="#000" fill-opacity="0.3" d="M0.677,20.977l4.355,1.631c0.281,0.104,0.579,0.162,0.88,0.16l9.76-0.004L30.46,41.58c0.27,0.34,0.679,0.545,1.112,0.541
            h1.87c0.992,0,1.676-0.992,1.322-1.918l-6.643-17.439l6.914,0.002l6.038,6.037c0.265,0.266,0.624,0.412,0.999,0.418l1.013-0.004
            c1.004-0.002,1.684-1.012,1.312-1.938l-2.911-7.277l2.912-7.278c0.372-0.928-0.313-1.941-1.313-1.938h1.017
            c-0.375,0-0.732,0.15-0.996,0.414l-6.039,6.039h-6.915l6.646-17.443c0.354-0.926-0.33-1.916-1.321-1.914l-1.87-0.004
            c-0.439,0.004-0.843,0.203-1.112,0.543L15.677,17.24l-9.765-0.002c-0.3,0.002-0.597,0.055-0.879,0.16L0.678,19.03
            C-0.225,19.36-0.228,20.637,0.677,20.977z" transform="translate(64,30) rotate(90 0 0)" filter="url(#f1)" />
          </g>
        </svg>
      </div>
    </div>
  </div>

  <div class="page" style="margin: auto;width: 100%;background: rgba(255, 255, 255, 0.9);margin-top: 10%;box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2);box-shadow: 1px 1px 20px 14px rgba(0,0,0,0.2);">
    <header>
      <center>
        <h3 style="color: #16467f !important;font-size:40px;font-weight:300 !important;margin-top:  0px;">
          Welcome to app portal
        </h3>
          <h4 style="COLOR: #16467f; font-size:  30px;font-weight: 300;">CORPORATE DASHBOARD</h4>
        <hr style="border-top: 5px double #16467f;">
      </center>
      <center>
        <img src="http://localhost/app_dss/assets/image/logo.png" style="width: 30%;height: auto;margin-top:  10px;" alt="">
      </center>
    </header>
  </div>

</div>

<script type="text/javascript">
  $(document).ready(function() {
    $(".content-wrapper").css('background', 'url(<?= base_url() ?>assets/image/landing-backgroung.jpg) no-repeat center center fixed');
    $(".content-wrapper").css('background-size', '1400px');
  })
</script>
