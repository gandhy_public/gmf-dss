<section class="content-header">
  <div class="box">
    <div class="box-body">
      <h3 class="box-title"></h3>

      <div class="col-md-2">
       <div class="form-group">
          <!-- <label for="inputEmail3" class="control-label">Range Date</label> -->
           <div class="col-sm-10">
             <select class="form-control" name="range" id="range">
              <option value="monthly">Monthly</option>
              <option value="semeterly">Semesterly</option>
            </select>
          </div>
         </div>
       </div>
      <div class="col-md-2" id="monthly_start_date">
        <div class="form-group">
           <div class="col-sm-10">
             <input type="text" placeholder="Start Date" class="form-control pull-right" id="date1">
           </div>
         </div>
      </div>
      <div class="col-md-2" id="monthly_end_date">
        <div class="form-group">
           <div class="col-sm-10">
             <input type="text" placeholder="End Date" class="form-control pull-right" id="date2">
           </div>
         </div>
      </div>
      <div class="col-md-3 hidden" id="semesterlystartdate">
        <div class="form-group">
           <div class="col-sm-6">
             <input type="text" placeholder="Start Date" class="form-control pull-right" id="dateyear3">
           </div>
           <div class="col-sm-6">
            <select class="form-control" id="semester_start">
              <option value="1">1</option>
              <option value="2">2</option>
            </select>
           </div>
         </div>
      </div>
      <div class="col-md-3 hidden" id="semesterlyenddate">
        <div class="form-group">
           <div class="col-sm-6">
             <input type="text" placeholder="End Date" class="form-control pull-right" id="dateyear4">
           </div>
           <div class="col-sm-6">
            <select class="form-control" id="semester_end">
              <option value="1">1</option>
              <option value="2">2</option>
            </select>
           </div>
         </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
           <div class="col-sm-10">
            <div class="ddcustomer">
            </div>
           </div>
         </div>
      </div>
      <div class="col-md-2">
      <button type="button"class="btn btn-flat bg-navy" name="add" id="add"><i class="fa fa-search"></i> Search</button>
      </div>

    </div>
  </div>

</section>

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= $title ?></h3>
      </div>
    <div class="box-body">
      <div class="col-md-12">
          <!-- LINE CHART -->
          <div class="row">
            <div class="box-header with-border">
              <h3 class="box-title">Final Score</h3>
              </div>
              <div id="container7"></div> <!-- chart final score-->
          </div>

          <div class="row hidden" id="allchart2">  <!-- all chart 3 -->
            <div class="box-header with-border">
              <h3 class="box-title">Each Customer</h3>
            </div>
            <div class="col-md-4">
              <div id="container25"></div> <!-- chart each cus-->
          </div>
          <div class="col-md-4">
              <div id="container22"></div> <!-- chart each cus-->
          </div>
          <div class="col-md-4">
            <div id="container23"></div> <!-- chart each cus-->
          </div>

          </div>

          <div class="row" id="onechart"> <!-- one chart 3 -->
            <div class="box-header with-border">
              <h3 class="box-title">Each Customer</h3>
            </div>
              <div id="container26"></div> <!-- chart each cus-->
            <!-- /.box-body -->
          </div>

          <div class="col-md-6">
            <div class="row">
            <div class="box-header with-border">
              <h3 class="box-title">Business Unit</h3>

            </div>

              <div id="container31"></div> <!-- chart final score-->
            <!-- /.box-body -->
          </div>
          </div>

          <div class="col-md-6">
            <div class="row">
            <div class="box-header with-border">
              <h3 class="box-title">Supporting unit</h3>

            </div>

              <div id="container32"></div> <!-- chart final score-->
            <!-- /.box-body -->
          </div>
          </div>

        </div>
      </div>
  </div>

</section>

<script>
 for(var i=1; i<=6; i++){
    //Date picker
    $('#date'+i).datepicker({
    format: "mm/yyyy",
      viewMode: "months",
      minViewMode: "months",
      autoclose: true
  });

    $('#dateyear'+i).datepicker({
      format: "yyyy",
      viewMode: "years",
      minViewMode: "years",
      autoclose: true
    });
}

  $(function () {
     $('.ddcustomer').load('<?= base_url() ?>index.php/csi_dashboard/get_customer');

     load_page_3_month();

  });

$('#range').change(function(){
var value = $('#range').val();
//value 1 = monthly, val 2 = semesterly
if(value == 'monthly'){
  $('#monthly_start_date').removeClass('hidden');
  $('#monthly_end_date').removeClass('hidden');
  $('#semesterlystartdate').addClass('hidden');
  $('#semesterlyenddate').addClass('hidden');
}else{
  $('#semesterlystartdate').removeClass('hidden');
  $('#semesterlyenddate').removeClass('hidden');
  $('#monthly_start_date').addClass('hidden');
  $('#monthly_end_date').addClass('hidden');
}

});

function load_page_3_month(){
  $.ajax({
        url: '<?= base_url() ?>index.php/csi_dashboard/load_3_month',
        type: 'GET',
        headers: {
            'token': localStorage.getItem('status')
        },
        dataType: 'JSON',
        beforeSend: function () {
            // $('#loading_purple').show();
        },
        success: function (data) {
          console.log(data.data_chart[0].finalscore);
          $('#container7').highcharts({
                    // chart: {
                    //     type: "column"
                    // },
                    title: {
                        text: ""
                    },
                    xAxis: {
                        type: 'category',
                        allowDecimals: true,
                        title: {
                            text: ""
                        }
                    },
                    yAxis: {
                        title: {
                            text: "Scores"
                        }
                    },
                    series: [{
                        name: 'Customer',
                        data: data.data_chart[0].finalscore
                    }]
                });
           $('#container32').highcharts({
                    // chart: {
                    //     type: "column"
                    // },
                    title: {
                        text: ""
                    },
                    xAxis: {
                        type: 'category',
                        allowDecimals: true,
                        title: {
                            text: ""
                        }
                    },
                    yAxis: {
                        title: {
                            text: "Scores"
                        }
                    },
                    series: [{
                        name: 'Customer',
                        data: data.data_chart2[0].busw
                    }]
                });
            $('#container31').highcharts({
                    // chart: {
                    //     type: "column"
                    // },
                    title: {
                        text: ""
                    },
                    xAxis: {
                        type: 'category',
                        allowDecimals: true,
                        title: {
                            text: ""
                        }
                    },
                    yAxis: {
                        title: {
                            text: "Scores"
                        }
                    },
                    series: [{
                        name: 'Customer',
                        data: data.data_chart[0].finalscore
                    }]
                });

          $('#container25').highcharts({
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: ""
                    },
                    xAxis: {
                        type: 'category',
                        allowDecimals: true,
                        title: {
                            text: ""
                        }
                    },
                    yAxis: {
                        title: {
                            text: "Scores"
                        }
                    },
                    series: [{
                        name: 'Customer',
                        data: data.data_chart[0].finalscore
                    },{
                        name: 'Garuda',
                        data: data.data_chart2[0].busw
                    }
                    ]
                });
          $('#container26').highcharts({
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: ""
                    },
                    xAxis: {
                        type: 'category',
                        allowDecimals: true,
                        title: {
                            text: ""
                        }
                    },
                    yAxis: {
                        title: {
                            text: "Scores"
                        }
                    },
                    series: [{
                        name: 'Customer',
                        data: data.data_chart[0].finalscore
                    },{
                        name: 'Garuda',
                        data: data.data_chart2[0].busw
                    }
                    ]
                });

                $('#container22').highcharts({
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: ""
                    },
                    xAxis: {
                        type: 'category',
                        allowDecimals: true,
                        title: {
                            text: ""
                        }
                    },
                    yAxis: {
                        title: {
                            text: "Scores"
                        }
                    },
                    series: [{
                        name: 'Customer',
                        data: data.data_chart[0].finalscore
                    },{
                        name: 'Garuda',
                        data: data.data_chart2[0].busw
                    }
                    ]
                });

                $('#container23').highcharts({
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: ""
                    },
                    xAxis: {
                        type: 'category',
                        allowDecimals: true,
                        title: {
                            text: ""
                        }
                    },
                    yAxis: {
                        title: {
                            text: "Scores"
                        }
                    },
                    series: [{
                        name: 'Customer',
                        data: data.data_chart[0].finalscore
                    },{
                        name: 'Garuda',
                        data: data.data_chart2[0].busw
                    }
                    ]
                });
            }


});
}

$('#add').click(function(){
  var range = $('#range').val();
  var start_date = $('#date1').val();
  var end_date = $('#date2').val();
  var cust = $('#customer').val();
  if(cust == 'all'){
  $('#allchart2').removeClass('hidden');
  $('#onechart').addClass('hidden');
  }else{
  $('#allchart2').addClass('hidden');
  $('#onechart').removeClass('hidden');

  }
    alert(range+"-"+start_date+"-"+end_date+"-"+cust);
  })



// Highcharts.chart('container32', {

//     title: {
//         text: 'Solar Employment Growth by Sector, 2010-2016'
//     },

//     subtitle: {
//         text: 'Source: thesolarfoundation.com'
//     },

//     yAxis: {
//         title: {
//             text: 'Number of Employees'
//         }
//     },
//     legend: {
//         layout: 'vertical',
//         align: 'right',
//         verticalAlign: 'middle'
//     },

//     plotOptions: {
//         series: {
//             label: {
//                 connectorAllowed: false
//             },
//             pointStart: 2010
//         }
//     },

//     series: [{
//         name: 'Sales & Distribution',
//         data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
//     }],

//     responsive: {
//         rules: [{
//             condition: {
//                 maxWidth: 500
//             },
//             chartOptions: {
//                 legend: {
//                     layout: 'horizontal',
//                     align: 'center',
//                     verticalAlign: 'bottom'
//                 }
//             }
//         }]
//     }

// });


// Highcharts.chart('container2', {
//     chart: {
//         type: 'column'
//     },
//     title: {
//         text: 'Monthly Average Rainfall'
//     },
//     subtitle: {
//         text: 'Source: WorldClimate.com'
//     },
//     xAxis: {
//         categories: [
//             'Jan',
//             'Feb',
//             'Mar',
//             'Apr',
//             'May',
//             'Jun',
//             'Jul',
//             'Aug',
//             'Sep',
//             'Oct',
//             'Nov',
//             'Dec'
//         ],
//         crosshair: true
//     },
//     yAxis: {
//         min: 0,
//         title: {
//             text: 'Rainfall (mm)'
//         }
//     },
//     tooltip: {
//         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//         pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
//             '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
//         footerFormat: '</table>',
//         shared: true,
//         useHTML: true
//     },
//     plotOptions: {
//         column: {
//             pointPadding: 0.2,
//             borderWidth: 0
//         }
//     },
//     series: [{
//         name: 'Tokyo',
//         data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

//     }, {
//         name: 'New York',
//         data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

//     }, {
//         name: 'London',
//         data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

//     }, {
//         name: 'Berlin',
//         data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

//     }]
// });


// Highcharts.chart('container22', {
//     chart: {
//         type: 'column'
//     },
//     title: {
//         text: 'Monthly Average Rainfall'
//     },
//     subtitle: {
//         text: 'Source: WorldClimate.com'
//     },
//     xAxis: {
//         categories: [
//             'Jan',
//             'Feb',
//             'Mar',
//             'Apr',
//             'May',
//             'Jun',
//             'Jul',
//             'Aug',
//             'Sep',
//             'Oct',
//             'Nov',
//             'Dec'
//         ],
//         crosshair: true
//     },
//     yAxis: {
//         min: 0,
//         title: {
//             text: 'Rainfall (mm)'
//         }
//     },
//     tooltip: {
//         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//         pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
//             '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
//         footerFormat: '</table>',
//         shared: true,
//         useHTML: true
//     },
//     plotOptions: {
//         column: {
//             pointPadding: 0.2,
//             borderWidth: 0
//         }
//     },
//     series: [{
//         name: 'Tokyo',
//         data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

//     }, {
//         name: 'New York',
//         data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

//     }, {
//         name: 'London',
//         data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

//     }, {
//         name: 'Berlin',
//         data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

//     }]
// });


// Highcharts.chart('container23', {
//     chart: {
//         type: 'column'
//     },
//     title: {
//         text: 'Monthly Average Rainfall'
//     },
//     subtitle: {
//         text: 'Source: WorldClimate.com'
//     },
//     xAxis: {
//         categories: [
//             'Jan',
//             'Feb',
//             'Mar',
//             'Apr',
//             'May',
//             'Jun',
//             'Jul',
//             'Aug',
//             'Sep',
//             'Oct',
//             'Nov',
//             'Dec'
//         ],
//         crosshair: true
//     },
//     yAxis: {
//         min: 0,
//         title: {
//             text: 'Rainfall (mm)'
//         }
//     },
//     tooltip: {
//         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//         pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
//             '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
//         footerFormat: '</table>',
//         shared: true,
//         useHTML: true
//     },
//     plotOptions: {
//         column: {
//             pointPadding: 0.2,
//             borderWidth: 0
//         }
//     },
//     series: [{
//         name: 'Tokyo',
//         data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

//     }, {
//         name: 'New York',
//         data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

//     }, {
//         name: 'London',
//         data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

//     }, {
//         name: 'Berlin',
//         data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

//     }]
// });


</script>
