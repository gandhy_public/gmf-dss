
<style>
.ui-datepicker-calendar {
    display: none;
    }
</style>


<section class="content-header">
  <!-- <div class="box"> -->
    <!-- <div class="box-body"> -->
      <h3 class="box-title">Aircraft Serviceability</h3>

</section>

<section class="content">
<div class="box">
    <div class="box-header with-border">
      </div>
    <div class="box-body">

  <div class="col-md-4">
    <div class="box">
      <div class="box-header">
      <h3 class="box-title">Year to Date</h3>
      </div>
      <div class="box-body">
        <label>Date : 10/03/2018</label>
        <div class="row">
                <div class="col-md-12">
                  <div class="col-md-10">
                    <div class="description-block">
                    <h2>98.9 %</h2>
                    <span class="description-text">TOTAL REVENUE</span>
                  </div>
                  </div>
                  <div class="col-md-2">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>
                  </div>

                  <!-- /.description-block -->
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="col-md-6">
                    <div class="description-block">
                    <h5>98.9%</h5>
                  </div>
                  </div>
                  <div class="col-md-2">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>
                  </div>

                  <!-- /.description-block -->
                </div>
                <div class="col-md-6">
                  <div class="col-md-6">
                    <div class="description-block">
                    <h5>98.9%</h5>
                  </div>
                  </div>
                  <div class="col-md-2">
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i></span>
                  </div>

                  <!-- /.description-block -->
                </div>

            </div>
            <div class="row">
              <div class="col-md-6">
                    <button type="button" onclick="button1()" class="btn btn-block btn-default btn-lg">98.9%</button>
                    <label class="description-text" style="align:center;">B777</label>
              </div>
              <div class="col-md-6">
                    <button type="button" onclick="button2()"  class="btn btn-block btn-default btn-lg">97.9%</button>
                    <label class="description-text" style="align:center;">B777</label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                    <button type="button" class="btn btn-block btn-default btn-lg">98.9%</button>
                    <label class="description-text" style="align:center;">B777</label>
              </div>
              <div class="col-md-6">
                    <button type="button" class="btn btn-block btn-default btn-lg">97.9%</button>
                    <label class="description-text" style="align:center;">B777</label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                    <button type="button" class="btn btn-block btn-default btn-lg">98.9%</button>
                    <label class="description-text" style="align:center;">B777</label>
              </div>
              <div class="col-md-6">
                    <button type="button" class="btn btn-block btn-default btn-lg">97.9%</button>
                    <label class="description-text" style="align:center;">B777</label>
              </div>
            </div>

      </div>
    </div>
  </div>

  <div class="col-md-8">
    <div class="box">
      <div class="box-header ">
      <h3 class="box-title">Group Name</h3>
      </div>
      <div class="box-body">

        <div class="col-md-12">
        <label>Date : 10/03/2018</label>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" placeholder="Start date" class="form-control pull-right" id="date1">
                </div>
                <!-- /.input group -->
              </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" placeholder="End date" class="form-control pull-right" id="date2">
                </div>
                <!-- /.input group -->
              </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div id="container1"></div>
          </div>
          <div class="col-md-6">
            <div id="container2"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div id="container3"></div>
          </div>
          <div class="col-md-6">
            <div id="container4"></div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div id="container5"></div>
          </div>
          <div class="col-md-6">
            <div id="container6"></div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>

</div>
</div>
</section>

        <div class="modal fade" id="modal_year">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">B777</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-xs-6 col-md-3 text-center">
                    <input type="text" class="knob" value="30" data-width="90" data-height="90" data-fgColor="#3c8dbc" data-readonly="true">
                    <div class="knob-label">Nama Bulan</div>
                  </div>
                  <div class="col-xs-6 col-md-3 text-center">
                    <input type="text" class="knob" value="30" data-width="90" data-height="90" data-fgColor="#3c8dbc" data-readonly="true">
                    <div class="knob-label">Nama Bulan</div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modal_chart">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">B777</h4>
              </div>
              <div class="modal-body">
                <div class="col-md-12">
                  <div id="container8"></div>
                </div>
              </div>
              <div class="modal-footer">
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

         <div class="modal fade" id="modal_chart2">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">B777</h4>
              </div>
              <div class="modal-body">
                <div class="col-md-12">
                  <div id="container8"></div>
                </div>
              </div>
              <div class="modal-footer">
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


<script type="text/javascript">
  $(".knob").knob()
  //Date range picker
  for(var i=1; i<=6; i++){
    //Date picker
    $('#date'+i).datepicker({
    format: "yyyy",
      viewMode: "years",
      minViewMode: "years",
      autoclose: true
  });

    // $('#date'+i).datepicker({
    //   autoclose: true
    // })
    // $('#datepicker'+i).datepicker({
      // format: "yyyy",

    // });
  }
  function button1(){
    $('#modal_chart').modal('show');
  }
  function button2(){
    $('#modal_year').modal('show');
  }

  Highcharts.chart('container1', {

    title: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Number of Employees'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2012
        }
    },

    series: [{
        name: 'Installation',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175,97031, 119931, 137133, 154175]
    }, ],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

  Highcharts.chart('container2', {

    title: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Number of Employees'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2012
        }
    },

    series: [{
        name: 'Installation',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175,97031, 119931, 137133, 154175]
    }, ],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

  Highcharts.chart('container3', {

    title: {
        text: ''
    },


    yAxis: {
        title: {
            text: 'Number of Employees'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2012
        }
    },

    series: [{
        name: 'Installation',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175,97031, 119931, 137133, 154175]
    }, ],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

  Highcharts.chart('container4', {


    title: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Number of Employees'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2012
        }
    },

    series: [{
        name: 'Installation',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175,97031, 119931, 137133, 154175]
    }, ],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

  Highcharts.chart('container5', {


    title: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Number of Employees'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2012
        }
    },

    series: [{
        name: 'Installation',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175,97031, 119931, 137133, 154175]
    }, ],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

  Highcharts.chart('container6', {

    title: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Number of Employees'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2012
        }
    },

    series: [{
        name: 'Installation',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175,97031, 119931, 137133, 154175]
    }, ],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

    Highcharts.chart('container8', {

    title: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Number of Employees'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2012
        }
    },

    series: [{
        name: 'Installation',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175,97031, 119931, 137133, 154175]
    }, ],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

</script>
