<style media="screen">
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin: 0;
  line-height: 1;
}

.spinner {
  margin: 100px auto;
  width: 50px;
  height: 40px;
  text-align: center;
  font-size: 10px;
}

.spinner > div {
  background-color: #333;
  height: 100%;
  width: 6px;
  display: inline-block;

  -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
  animation: sk-stretchdelay 1.2s infinite ease-in-out;
}

.spinner .rect2 {
  -webkit-animation-delay: -1.1s;
  animation-delay: -1.1s;
}

.spinner .rect3 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

.spinner .rect4 {
  -webkit-animation-delay: -0.9s;
  animation-delay: -0.9s;
}

.spinner .rect5 {
  -webkit-animation-delay: -0.8s;
  animation-delay: -0.8s;
}

@-webkit-keyframes sk-stretchdelay {
  0%, 40%, 100% { -webkit-transform: scaleY(0.4) }
  20% { -webkit-transform: scaleY(1.0) }
}

@keyframes sk-stretchdelay {
  0%, 40%, 100% {
    transform: scaleY(0.4);
    -webkit-transform: scaleY(0.4);
    }  20% {
      transform: scaleY(1.0);
      -webkit-transform: scaleY(1.0);
    }
  }
  .stripe {
    white-space: nowrap;
    background-color: white;
  }
  th{
    text-align: center;
  }

</style>
<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= $title ?></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="button-add-place pull-right">
            <button type="button" data-toggle="modal"  class="btn btn-flat bg-navy" name="master" id="master">Master Customer     <i class="fa fa-clone"></i></button>
            <button type="button" data-toggle="modal"  class="btn btn-flat bg-navy" name="add" id="add">Upload Data     <i class="fa fa-cloud-upload"></i></button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="table table-hover table-bordered table-gmf" id="upload-data" width="100%">
            <thead style="background-color: #05354D">
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Tanggal Transaksi</th>
                <!-- <th class="text-center">Tipe Penilaian</th> -->
                <th class="text-center">Date Range</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody class="text-center">
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="rows">

    </div>
  </div>

  <!-- Modal -->
  <!-- The Modal -->
  <div class="modal fade" id="myModal"  style="border-radius:border-radius: 50px 20px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Upload Data Satisfaction Index</h4>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div id="loadingApprove" class="back-loading" style="display: none;background: white;position: absolute;z-index: 9999;width: 99%;height: 100%;margin: 0 auto;left: 1;/* display: none; */left: 0;right: 0;/* top: 10px; */bottom: 0;/* padding:  50%; */padding top:;/* padding-top:  50px; */">
            <h4 style="text-align:  center;margin-top:  50px;font-size: 20px;">Data Diproses</h4>
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
          </div>
          <form action="" name="formfile" id="formfile" method="post" enctype="multipart/form-data">
            <div class="form-group" id="tipe_penilaian">
              <label for="email">Tipe Penilaian</label>
              <select class="form-control" name="periode" id="periode" required>
                <option value="semesterly">Semester</option>
                <option value="monthly">Month</option>
              </select>
            </div>
            <!-- Date range -->
            <div class="form-group">
              <label>Date range:</label>

              <div class="form-group" id="form_semesterly">
                <!-- <label>Date range:</label> -->
                <div class="form-group col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control pull-right" placeholder="Tahun / Year " name="year" id="year" value="<?php echo date('Y'); ?>">
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <select class="form-control pull-right" name="semester" id="semester" >
                      <option value="">Pilih Semester</option>
                      <option value="06">Semester 1</option>
                      <option value="12">Semester 2</option>
                    </select>
                  </div>
                </div>

              </div>
              <div class="input-group" id="form_mountly">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input class="form-control pull-right" name="mounth" id="mounth"  >
              </div>
            </div>
            <!-- /.form group -->
            <div class="form-group">
              <label>Upload Files</label>
              <input type="file" class="form-control" name="files" id="files" value="" required>
            </div>
            <div class="form-group">
              <label>Download Template</label>
              <div class="col-md-12">
                <!-- <div class="col-md-6">
                  <center>  <a href="<?php echo base_url(); ?>assets/upload/template/template_csi_monthly.xlsx">Template Monthly</a></center>
                </div> -->
                <div class="col-md-12">
                  <center><a href="<?php echo base_url(); ?>assets/upload/template/template_csi_semester.xlsx">Template Semesterly</a></center>
                </div>
              </div>
            </div>
            <div class="row">

            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close  <i class="fa fa-rotate-left"></i></button>
            <button type="submit" name="submit" id="submit" class="btn btn-flat bg-navy"> Save <i class="fa fa-save"></i></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!--  -->


  <!-- Start Modal Master Cusomer -->
  <div class="modal fade" id="mastermodal"  style="border-radius:border-radius: 50px 20px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Master Customer</h4>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div id="loadingApprove2" class="back-loading" style="display: none;background: white;position: absolute;z-index: 9999;width: 99%;height: 100%;margin: 0 auto;left: 1;/* display: none; */left: 0;right: 0;/* top: 10px; */bottom: 0;/* padding:  50%; */padding top:;/* padding-top:  50px; */">
            <h4 style="text-align:  center;margin-top:  50px;font-size: 20px;">Data Diproses</h4>
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
          </div>
          <div class="form-group pull-right">
            <button type="button" name="add_customer" id="add_customer" class="btn btn-flat bg-navy"> Tambah Customer <i class="fa fa-plus"></i></button>
          </div>
          <div class="col-lg-12" id="modal_add_customer">
            <center><u><h4>Tambah Customer</h4></u></center>
            <div class="row">
            </div>
            <form action="" method="POST"  name="form_add_customer" id="form_add_customer" enctype="multipart/form-data">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Customer</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="input_customer" id="input_customer" value="">
                  <input type="hidden" class="form-control" name="id_c" id="id_c" >
                </div>
              </div>
              <div class="form-group pull-right">
                <button type="button" name="cancel" id="cancel" class="btn btn-flat bg-navy"> Batal <i class="fa fa-rotate-left"></i></button>
                <button type="submit" name="submit" id="submit" class="btn btn-flat bg-navy"> Simpan <i class="fa fa-save"></i></button>
              </div>
            </form>
          </div>
          <div class="col-lg-12">
            <table class="stripe row-border order-column table datatable table-bordered" id="tablecustomer" width="100%">
              <thead style="background-color: #05354D">
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Customer Name</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody class="text-center">
              </tbody>
            </table>
          </div>
          <div class="row">
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close  <i class="fa fa-rotate-left"></i></button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Master Cusomer -->

<!--
Data tables modal
Show Data From Excel
-->

<div class="modal fade" id="myModal1" style="border-radius:border-radius: 50px 20px;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Upload Data Satisfaction Index</h4>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div id="loadingApprove" class="back-loading" style="display: none;background: white;position: absolute;z-index: 9999;width: 99%;height: 100%;margin: 0 auto;left: 1;/* display: none; */left: 0;right: 0;/* top: 10px; */bottom: 0;/* padding:  50%; */padding top:;/* padding-top:  50px; */">
          <h4 style="text-align:  center;margin-top:  50px;font-size: 20px;">Data Diproses</h4>
          <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
          </div>
        </div>
        <div class="form-group" >
          <table id="example" class="stripe row-border order-column table datatable table-bordered" >
            <thead>
              <tr>
                <td>O</td>
                <td>O</td>
                <td>O</td>
                <td>O</td>
                <td>O</td>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close  <i class="fa fa-rotate-left"></i></button>
      </div>
    </div>
  </div>
</div>

</section>



<script type="text/javascript">
$(document).ready(function() {
  $("#tipe_penilaian").hide();
  $("#modal_add_customer").hide();
// =============================================================================
  $("#cancel").on('click', function(event) {
    event.preventDefault();
    /* Act on the event */
    $("#modal_add_customer").hide('slow/600/slow', function() {
    });
    $("#add_customer").show('slow/600/slow', function() {
    });
  });
// =============================================================================
  $("#add_customer").on('click', function(event) {
    event.preventDefault();
    /* Act on the event */
    $("#form_add_customer")[0].reset();
    $("#add_customer").hide('slow/400/fast', function() {
    });
    $("#modal_add_customer").show('slow/600/slow', function() {
    });
  });
// =============================================================================
  $('#myModal1').on('shown.bs.modal', function (e) {
    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
  });
// =============================================================================
  $('#add').on('click', function(event) {
    event.preventDefault();
    $("#form_semesterly").show();
    $("#form_mountly").hide();
    $("#formfile")[0].reset();
    $('#myModal').modal('show');
  });
// =============================================================================
  $('#master').on('click', function(event) {
    event.preventDefault();
    $('#mastermodal').modal('show');
  });
// ============================================================================
var table_customer=$("#tablecustomer").DataTable({
  "processing": true,
  "serverSide": true,
  "paging":true,
  "ajax": {
    "url": '<?= base_url() ?>index.php/Csi_controller/data_customer',
    "type" :'POST',
  }
});
// ============================================================================
  var table=$("#upload-data").DataTable({
    "processing": true,
    "serverSide": true,
    "paging":true,
    "ajax": {
      "url": '<?= base_url() ?>index.php/Csi_controller/datatable',
      "type" :'POST',
    }
  });
// =============================================================================
  $("#form_add_customer").submit(function(event) {
    /* Act on the event */
    event.preventDefault();
    var formData = new FormData($('#form_add_customer')[0]);
    $.ajax({
      url:'<?= base_url() ?>index.php/Csi_controller/store_customer',
      type: 'POST',
      processData: false,
      contentType: false,
      data: formData,
    })
    .done(function(reps) {
      var data= JSON.parse(reps);
      notif(data.notif,data.msg);
      $("#id_c").val('');
      console.log(reps[notif]);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
      $("#form_add_customer")[0].reset();
      table_customer.ajax.reload();
    });

  });
// =============================================================================
  $("#formfile").submit(function(event) {
    /* Act on the event */
    event.preventDefault();
    $('#submit').text("Proses Simpan");
    $('#submit').attr('disabled', true);
    var formData = new FormData($('#formfile')[0]);
    $.ajax({
      url:'<?= base_url() ?>index.php/Csi_controller/storefiles',
      type: 'POST',
      processData: false,
      contentType: false,
      data: formData,
      beforeSend: function(){
        $("#loadingApprove").show()
        console.log("loading");
      }
    })
    .done(function(resp) {
      var dt= JSON.parse(resp);
      if(dt[0].data=='Sukses'){
        notif("success",dt[0].msg);
        $('#myModal').modal('hide');
      }else if(dt[0].data=='warning'){
        notif("warning",dt[0].msg);
        $("#form_semesterly").show();
        $("#form_mountly").hide();
      }else{
        notif("error",dt[0].msg);
        $('#myModal').modal('hide');
      }
      console.log(dt[0].data);
      $("#formfile")[0].reset();


    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
      $('#submit').attr('disabled', false);
      $('#submit').text("Simpan");
      $("#loadingApprove").fadeOut(1000);
      table.ajax.reload();
    })

  });
// =============================================================================
  function checkExtention(ini) {
    var namefile=ini.val();
    var ext=namefile.replace(/^.*\./, '');
    ext=ext.toLowerCase();
    extList=["xls","xlsx"];
    return ($.inArray(ext,extList)== -1) ? false:true;
  }
  $(document).on("change", "#files", function (e) {
    // console.log(checkExtention($(this)));
    if(this.files[0].size > 1300000 || checkExtention($(this)) == false){
      notif("error", "Cek Format File yang Diupload");
      $("#files").val('');
    }
  });
// =============================================================================
$(document).on('click', "#del_customer", function(event) {
  event.preventDefault();
  /* Act on the event */
  var id = $(this).data("id");
  var ini = $(this);
  swal({
    title: 'Are you sure?',
    text: "Delete this data..",
    type: 'warning',
    showCancelButton: true,
    confirmButtonClass: 'btn bg-navy',
    cancelButtonClass: 'btn bg-red',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    $.ajax({
      url: '<?= base_url() ?>' +"index.php/Csi_controller/deletecustomer/"+id,
      type: 'GET',
      dataType: 'json',
    })
    .done(function(reps) {
      // var dt= JSON.parse(data);
      if(reps.data=='Sukses')
      {
        notif("success", "Data Deleted");
      } else {
        notif("error", "Failed to delete");
      }
    })
    .fail(function() {
      notif("error", "Failed to delete");
    })
    .always(function() {
      console.log("complete");
      table_customer.ajax.reload();
    });
  })
});
// =============================================================================
$(document).on('click', "#edit_customer", function(event) {
  event.preventDefault();
  /* Act on the event */
  var id = $(this).data("id");
  var nm = $(this).data("nama");
  var ini = $(this);
  $("#add_customer").hide('slow/400/fast', function() {
  });
  $("#modal_add_customer").show('slow/600/slow', function() {
  });
  $("#input_customer").val(nm);
  $("#id_c").val(id);
});
// =============================================================================
  $(document).on("click", "#del", function(e){
    e.preventDefault();
    var id = $(this).data("id");
    var ini = $(this);
    swal({
      title: 'Are you sure?',
      text: "Delete this data..",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn bg-navy',
      cancelButtonClass: 'btn bg-red',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      $.ajax({
        url: '<?= base_url() ?>' +"index.php/Csi_controller/delete/"+id,
        type: 'GET',
        dataType: 'json',
      })
      .done(function(reps) {
        // var dt= JSON.parse(data);
        if(reps.data=='Sukses')
        {
          notif("success", "Data Deleted");
        } else {
          notif("error", "Failed to delete");
        }
        console.log(reps);

      })
      .fail(function() {
        notif("error", "Failed to delete");
      })
      .always(function() {
        console.log("complete");
        table.ajax.reload();
      });
    })
  });

  $( "#mounth" ).datepicker({
    minViewMode:1,
    format: 'yyyy-mm',
    autoclose:true
  });
  $( "#year" ).datepicker({
    viewMode: "years",
    minViewMode: "years",
    format: 'yyyy',
    autoclose:true
  });
// =============================================================================
  $( "#periode" ).change(function(e) {
    var cek=$(this).val();
    if (cek=='semesterly') {
      // console.log("a");
      $("#year").val('');
      $("#semester").val('');
      $("#form_semesterly").show();
      $("#form_mountly").hide();
    }else{
      $("#mounth").val('');
      $("#form_mountly").show();
      $("#form_semesterly").hide();
    }
  });
// =============================================================================
  $(document).on('click', '#view', function(event) {
    event.preventDefault();
    /* Act on the event */
    var id = $(this).data("id");
    $.ajax({
      url: '<?= base_url() ?>' +"index.php/Csi_controller/view/"+id,
      type: 'POST'
      // beforeSend: function(){
      //   $("#loadingApprove").show()
      //    console.log("loading");
      // }
    })
    .done(function(data) {
      datatab(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    $("#myModal1").modal('show');
  });

});
// =============================================================================
function datatab(data) {
  $('#example').DataTable( {
    scrollY:        "300px",
    scrollX:        true,
    scrollCollapse: true,
    paging:         false,
    fixedColumns:   {
      leftColumns: 1
    },data
  });

}
</script>
