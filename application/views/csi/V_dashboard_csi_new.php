<style media="screen">
  .box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
    display: inline-block;
    font-size: 20px;
    font-weight: bold;
    margin: 0;
    line-height: 1;
  }

.loading {
    position: absolute;
    left: 50%;
    top: 50%;
    z-index: 1;
    width: 150px;
    height: 150px;
    margin: -75px 0 0 -75px;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }


      @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
      }

      @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
      }

      /* Add animation to "page content" */
      .animate-bottom {
        position: relative;
        -webkit-animation-name: animatebottom;
        -webkit-animation-duration: 1s;
        animation-name: animatebottom;
        animation-duration: 1s
      }

      @-webkit-keyframes animatebottom {
        from { bottom:-100px; opacity:0 }
        to { bottom:0px; opacity:1 }
      }

      @keyframes animatebottom {
        from{ bottom:-100px; opacity:0 }
        to{ bottom:0; opacity:1 }
      }
.label-bottom{
    margin-bottom: 0px;
    padding-top: 7px;
    font-size: 16px;
    color: white;
    cursor: pointer;

}
.kotak-bawah{
  height:40px;
  text-align:center;
  cursor: pointer;
}

</style>
<section class="content-header hidden">
      <h3 class="title"><?= $title ?></h3>
  <div class="box">
    <div class="box-body">
      <form role="form" id="form_csi">
      <div class="col-xs-2" id="semesterlystartdate">
          <label>Start Date</label>
        <div class="form-group " >
           <div class="col-md-7" style="padding-right: 0px;padding-left: 0px;">
               <input type="text" placeholder="Start Date" value="<?= $startPeriode ?>" class="form-control"
                      id="dateyear3">
           </div>
           <div class="col-md-5" style="padding-right: 0px;padding-left: 0px;">
            <select class="form-control" id="semester_start" name="semester_start" >
              <option value="06">1</option>
              <option value="12">2</option>
            </select>
           </div>
         </div>
      </div>

        <div class="col-xs-2" id="semesterlyenddate">
          <label>End Date</label>
          <div class="form-group " >
           <div class="col-md-7" style="padding-right: 0px;padding-left: 0px;">
               <input type="text" placeholder="End Date" value="<?= $endPeriode ?>" class="form-control"
                      id="dateyear4">
           </div>
           <div class="col-md-5" style="padding-right: 0px;padding-left: 0px;">
            <select class="form-control" id="semester_end" name="semester_end" >
              <option value="06">1</option>
              <option value="12">2</option>
            </select>
           </div>
         </div>
      </div>

      <div class="col-md-2">
        <div class="form-group">
          <label for="inputEmail3" class="control-label">Customer</label>
           <div class="col-sm-10" style="padding-right: 0px;padding-left: 0px;">
            <div class="ddcustomer">
            </div>
           </div>
         </div>
      </div>

    </form>
      <div class="col-md-2" style="padding-top: 20px;">
      <button type="button"class="btn btn-flat bg-navy" onclick="addsemesterly()" id="addsemesterly"><i class="fa fa-search"></i> Search</button>
      </div>

    </div>
  </div>
</section>


<!-- Load ROw 1 chart final score & bisnis support unit-->
<section class="content">
  <div class="row">
    <div class="col-md-4" style="padding-right:0px;">
    <div class="box box-success">
      <div class="box-header with-border">
        <h1 class="box-title">Final Score</h1>
        </div>
        <div class="box-body">
        <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="row">
              <div class="box-header with-border">
                <!-- <h3 class="box-title">Final Score</h3> -->
                </div>

                <div style="display:none;" class="loading" id="loader"></div>
                <div class="col-md-12">
                <div id="chart_csi_1"></div> <!-- chart final score-->
              </div>
            </div>
            <div class="row">
              <button type="button" class="btn btn-block btn-success btn-flat" onclick="detail_chart(1)">View Detail</button>
              <!-- <div class="kotak-bawah" onclick="detail_chart(1)" style="background-color:#009e58;">
                <label class="label-bottom">View Detail</label>
                <label style="padding-top:12px;padding-right:12px;color:white;" class="pull-right">
                <i class="fa fa-ellipsis-v pull-right"></i>
              </label>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-8">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Business & Support Unit</h3>
          </div>
          <div class="box-body">
          <div class="col-md-12">

            <div style="display:none;" class="loading" id="loader_bisnis"></div>


              <div class="col-md-6" id="bisnisblok" style="padding-left:20px;padding-left:0px;">
                <div class="row">
                <div class="box-header with-border">
                  <!-- <h3 class="box-title">Business Unit</h3> -->
                  </div>
                  <div id="chart_bisnis"></div>
                </div>
                <div class="row" style="margin-left:0px;">
                  <button type="button" class="btn btn-block btn-primary btn-flat" onclick="detail_chart(2)">Business Unit</button>
                  <!-- <div class="kotak-bawah" onclick="detail_chart(2)" style="background-color:#378ab7;">
                    <label class="label-bottom">Business Unit</label>
                    <label style="padding-top:12px;padding-right:12px;color:white;" class="pull-right">
                    <i class="fa fa-ellipsis-v pull-right"></i>
                  </label>
                  </div> -->
                </div>
              </div>

              <div class="col-md-6" id="supportblok" style="padding-left:0px;padding-left:20px;">
                <div class="row">
                <div class="box-header with-border">
                  <!-- <h3 class="box-title">Supporting unit</h3> -->
                </div>
                  <div id="chart_support"></div>
                </div>
                <div class="row" style="margin-left:0px;">
                  <button type="button" class="btn btn-block btn-primary btn-flat" onclick="detail_chart(3)">Supporting Unit</button>

                  <!-- <div class="kotak-bawah" onclick="detail_chart(3)" style="background-color:#378ab7;">
                    <label class="label-bottom">Supporting Unit</label>
                    <label style="padding-top:12px;padding-right:12px;color:white;" class="pull-right">
                    <i class="fa fa-ellipsis-v pull-right"></i>
                  </label>
                  </div> -->
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
  </div>

  <!-- mockup muncul detail chart-->

  <div class="row">
    <div class="col-md-12">
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Each Customer</h3>
        </div>
        <div class="box-body">
        <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="row" id="allchart2">  <!-- all chart 3 -->
              <!-- <label>Key Customer</label> -->
              <div class="col-md-4" style="padding-left:20px;padding-right:20px;">
                <div class="row">
                  <div style="display:none;" class="loading" id="loader_key"></div>
                    <div id="chart_key"></div>
                </div>
                </br>
                <div class="row">
                  <button type="button" class="btn btn-block btn-warning btn-flat" onclick="detail_chart(4)">Key Customer</button>

                  <!-- <div class="kotak-bawah" onclick="detail_chart(4)" style="background-color:#f59c0d;">
                    <label class="label-bottom">Key Customer</label>
                    <label style="padding-top:12px;padding-right:12px;color:white;" class="pull-right">
                    <i class="fa fa-ellipsis-v pull-right"></i>
                  </label>
                  </div> -->
                </div>
              </div>
              <!-- <label>High Value Brand Customer</label> -->
              <div class="col-md-4" style="padding-left:20px;padding-right:20px;">
                <div class="row">
                  <div style="display:none;" class="loading" id="loader_high"></div>
                    <div id="chart_high"></div>
                </div>
                </br>
                <div class="row">
                  <button type="button" class="btn btn-block btn-warning btn-flat" onclick="detail_chart(5)">High Value Brand Customer</button>

                  <!-- <div class="kotak-bawah" onclick="detail_chart(5)"  style="background-color:#f59c0d;">
                    <label class="label-bottom">High Value Brand Customer</label>
                    <label style="padding-top:12px;padding-right:12px;color:white;" class="pull-right">
                    <i class="fa fa-ellipsis-v pull-right"></i>
                  </label>
                  </div> -->
                </div>
              </div>
              <!-- <label>General Customer</label> -->
              <div class="col-md-4" style="padding-left:20px;padding-right:20px;">
                <div class="row">
                  <div style="display:none;" class="loading" id="loader_general"></div>
                    <div id="chart_general"></div>
                </div>
                </br>
                <div class="row">
                  <button type="button" class="btn btn-block btn-warning btn-flat" onclick="detail_chart(6)">General Customer</button>

                  <!-- <div class="kotak-bawah" onclick="detail_chart(6)" style="background-color:#f59c0d;">
                    <label class="label-bottom">General Customer</label>
                    <label style="padding-top:12px;padding-right:12px;color:white;" class="pull-right">
                    <i class="fa fa-ellipsis-v pull-right"></i>
                  </label>
                  </div> -->
                </div>
              </div>
            </div>

            <div class="row hidden" id="onechart"> <!-- one chart 3 -->

              <div style="display:none;" class="loading" id="loader_one"></div>
                <div id="chart_one"></div>
            </div>

          </div>
        </div>
      </div>
    </div>
    </div>

    <div class="row hidden" id="detail_chart_top">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title" id="title_detail_chart">Business Unit</h3>

            <div class="pull-right">
              <button type="button" class="close"  aria-label="Close" onclick="close_btn()">
                    <span aria-hidden="true">×</span></button>
                  <!-- <button type="button" class="btn btn-info btn-sm" data-dismiss="modal" onclick="close_btn()">
                    <i class="fa fa-times"></i></button> -->
                </div>
            </div>
          <div class="box-body">
            <div class="row" id="filter_top">
              <div class="col-md-12">
              <form role="form" id="form_csi">
              <div class="col-xs-2" id="semesterlystartdate">
                  <label>Start Date</label>
                <div class="form-group " >
                   <div class="col-md-7" style="padding-right: 0px;padding-left: 0px;">
                       <input type="text" placeholder="Start Date" value="<?= $startPeriode ?>" class="form-control"
                              id="dateyear3">
                   </div>
                   <div class="col-md-5" style="padding-right: 0px;padding-left: 0px;">
                    <select class="form-control" id="semester_start" name="semester_start" >
                      <option value="06">1</option>
                      <option value="12">2</option>
                    </select>
                   </div>
                 </div>
              </div>

                <div class="col-xs-2" id="semesterlyenddate">
                  <label>End Date</label>
                  <div class="form-group " >
                   <div class="col-md-7" style="padding-right: 0px;padding-left: 0px;">
                       <input type="text" placeholder="End Date" value="<?= $endPeriode ?>" class="form-control"
                              id="dateyear4">
                   </div>
                   <div class="col-md-5" style="padding-right: 0px;padding-left: 0px;">
                    <select class="form-control" id="semester_end" name="semester_end" >
                      <option value="06">1</option>
                      <option value="12">2</option>
                    </select>
                   </div>
                 </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <label for="inputEmail3" class="control-label">Customer</label>
                   <div class="col-sm-10" style="padding-right: 0px;padding-left: 0px;">
                    <div class="ddcustomer">
                    </div>
                   </div>
                 </div>
              </div>

            </form>
              <div class="col-md-2" style="padding-top: 20px;">
              <button type="button"class="btn btn-flat bg-navy" onclick="addsemesterly()" id="addsemesterly"><i class="fa fa-search"></i> Search</button>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div style="display:none;" class="loading" id="loader_chart_top"></div>

              <div id="chart_detail_top"></div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row hidden" id="detail_chart_bottom">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title" id="title_detail_chart_bottom">Business Unit</h3>
            <div class="pull-right box-tools">
              <button type="button" class="close"  aria-label="Close" onclick="close_btn_2()">
                    <span aria-hidden="true">×</span></button>
                  <!-- <button type="button" class="btn btn-info btn-sm" onclick="close_btn_2()">
                    <i class="fa fa-times"></i></button> -->
                </div>
            </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
              <form role="form" id="form_csi">
              <div class="col-xs-2" id="semesterlystartdate">
                  <label>Start Date</label>
                <div class="form-group " >
                   <div class="col-md-7" style="padding-right: 0px;padding-left: 0px;">
                       <input type="text" placeholder="Start Date" value="<?= $startPeriode ?>" class="form-control"
                              id="dateyear3">
                   </div>
                   <div class="col-md-5" style="padding-right: 0px;padding-left: 0px;">
                    <select class="form-control" id="semester_start" name="semester_start" >
                      <option value="06">1</option>
                      <option value="12">2</option>
                    </select>
                   </div>
                 </div>
              </div>

                <div class="col-xs-2" id="semesterlyenddate">
                  <label>End Date</label>
                  <div class="form-group " >
                   <div class="col-md-7" style="padding-right: 0px;padding-left: 0px;">
                       <input type="text" placeholder="End Date" value="<?= $endPeriode ?>" class="form-control"
                              id="dateyear4">
                   </div>
                   <div class="col-md-5" style="padding-right: 0px;padding-left: 0px;">
                    <select class="form-control" id="semester_end" name="semester_end" >
                      <option value="06">1</option>
                      <option value="12">2</option>
                    </select>
                   </div>
                 </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <label for="inputEmail3" class="control-label">Customer</label>
                   <div class="col-sm-10" style="padding-right: 0px;padding-left: 0px;">
                    <div class="ddcustomer">
                    </div>
                   </div>
                 </div>
              </div>

            </form>
              <div class="col-md-2" style="padding-top: 20px;">
              <button type="button"class="btn btn-flat bg-navy" onclick="addsemesterly()" id="addsemesterly"><i class="fa fa-search"></i> Search</button>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div style="display:none;" class="loading" id="loader_chart_bottom"></div>

              <div id="chart_detail_bottom"></div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>

      <!-- <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Business & Support Unit</h3>
          </div>
          <div class="box-body">
          <div class="col-md-12">

            <div style="display:none;" class="loading" id="loader_bisnis"></div>


              <div class="col-md-6" id="bisnisblok">
                <div class="row">
                <div class="box-header with-border">
                  </div>
                  <div id="chart_bisnis"></div>
                </div>
              </div>

              <div class="col-md-6" id="supportblok">
                <div class="row">
                <div class="box-header with-border">
                </div>
                  <div id="chart_support"></div>
                </div>
              </div>

            </div>
          </div>
        </div> -->


</section>

<div class="modal fade" id="modal_detail_chart">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Chart Detail Period (<span id="titlechart"></span>)</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" id="customerdetail">
                <input type="hidden" id="rangedetail">
                <div id="modal_chart1"></div>
              </div>
              <div class="modal-footer">
              </div>
            </div>
          </div>
        </div>

<script>
 for(var i=1; i<=6; i++){
    //Date picker
    $('#date'+i).datepicker({
    format: "yyyy-mm",
      viewMode: "months",
      minViewMode: "months",
      autoclose: true
  });

    $('#dateyear'+i).datepicker({
      format: "yyyy",
      viewMode: "years",
      minViewMode: "years",
      autoclose: true
    });
}

function detail_chart(id){
  var nama_top = '';
  var nama_bottom = '';
  if(id == "1"){
    nama_top = "Final Score";
    $("#title_detail_chart").html(nama_top);
    $("#detail_chart_top").removeClass("hidden");
    load_chart_detail_top();
    $("#filter_top").addClass("hidden");
  }else if(id == "2"){
    nama_top = "Business Unit";
    $("#title_detail_chart").html(nama_top);
    $("#detail_chart_top").removeClass("hidden");
    load_chart_detail_top();
    $("#filter_top").removeClass("hidden");
  }else if(id == "3"){
    nama_top = "Supporting Unit";
    $("#title_detail_chart").html(nama_top);
    $("#detail_chart_top").removeClass("hidden");
    load_chart_detail_top();
    $("#filter_top").removeClass("hidden");
  }else if(id == "4"){
    nama_bottom = "Key Customer";
    $("#title_detail_chart_bottom").html(nama_bottom);
    load_chart_detail_bottom();
    $("#detail_chart_bottom").removeClass("hidden");
  }else if(id == "5"){
    nama_bottom = "High Value Brand Customer";
    $("#title_detail_chart_bottom").html(nama_bottom);
    load_chart_detail_bottom();
    $("#detail_chart_bottom").removeClass("hidden");
  }else if(id == "6"){
    nama_bottom = "General Customer";
    $("#title_detail_chart_bottom").html(nama_bottom);
    load_chart_detail_bottom();
    $("#detail_chart_bottom").removeClass("hidden");
  }

}

function close_btn(){
  $("#detail_chart_top").addClass("hidden");
}
function close_btn_2(){
  $("#detail_chart_bottom").addClass("hidden");
}


  $(function () {
     $('.ddcustomer').load('<?= base_url() ?>index.php/csi_dashboard_new/get_customer');

     var dateNow = new Date();
     var month = dateNow.getMonth()+1;

     if (month == "01" || month == "02" || month == "03" || month == "04" || month == "05" || month == "06") {
         month = "06";
     } else if (month == "07" || month == "08" || month == "09" || month == "10" || month == "11"|| month == "12") {
         month = "12";
     }


     $("#semester_start").val(month);
     $("#semester_end").val(month);

     load_chart_final();
     load_chart_key();
     load_chart_high();
     load_chart_general();
     load_chart_bisnis();

  });

  function load_chart_detail_top(){
    var year_start = $("#dateyear3").val();
    var year_end = $("#dateyear4").val();
    var semt_start = $("#semester_start").val();
    var semt_end = $("#semester_end").val();
    var cust = $("#customer").val();

    if (cust == null){
      cust = "all";
    }

    $.ajax({
         url: '<?= base_url() ?>index.php/csi_dashboard_new/show_chart_accumulation',
         type: 'POST',
         dataType: 'JSON',
         data:{
           y_start : year_start,
           y_end : year_end,
           s_start : semt_start,
           s_end : semt_end,
           cus : cust
         },
         headers: {
         },
         beforeSend: function () {
           $('html, body').animate({
               scrollTop: $("#detail_chart_top").position().top
           }, 500);
           $("#loader_chart_top").fadeIn(1000);
             // $('#loading_purple').show();
         },
         success: function (data) {
           $("#loader_chart_top").fadeOut(1000);

           $('#chart_detail_top').highcharts({

                     title: {
                         text: " "
                     },
                     // exporting: {
                     //     enabled: false
                     // },
                     chart: {
                        type: 'column'
                    },
                     xAxis: {
                         categories: data.categ[0].category,
                         id: 'x-axis'
                     },
                     yAxis: {
                         title: {
                             text: "<b>Scores</b>"
                         }
                     },
                     credits:{
                       enabled: false,

                     },
                     tooltip: {
                         // headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
                         // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                         //         '<td style="padding:0"><b>USD {point.y}</b></td></tr>',
                         // footerFormat: '</table>',
                         // shared: true,
                         // useHTML: true
                         enabled:false
                     },
                     plotOptions: {
                               series: {
                                 label: {
                                   connectorAllowed: false
                                 },
                                 cursor: 'pointer',
                                 point: {
                                   events: {
                                     click: function () {
                                        detail_grafik_cus(this.category)
                                     }
                                   }
                                 }
                               }
                             },
                     colors: [
                             '#05354D',
                             '#E6E7EB',
                             '#F2C573',
                             '#8EC3A7',
                             '#F24738',
                             '#92A8CD'
                             ],
                     series: [{
                         name: 'Final Score',
                         data: data.data[0].finalscore,
                         lineWidth: 5
                     },{
                       showInLegend: false,
                       type: 'spline',
                       data: data.data[0].finalscore,
                       color: '#ffdf6d',
                       marker: {
                           lineWidth: 2,
                           lineColor: 'orange',
                           fillColor: 'white'
                       }
                     }]
                 });
               }
             });
  }

    function load_chart_detail_bottom(){
      var year_start = $("#dateyear3").val();
      var year_end = $("#dateyear4").val();
      var semt_start = $("#semester_start").val();
      var semt_end = $("#semester_end").val();
      var cust = $("#customer").val();

      if (cust == null){
        cust = "all";
      }

      $.ajax({
           url: '<?= base_url() ?>index.php/csi_dashboard_new/show_chart_accumulation',
           type: 'POST',
           dataType: 'JSON',
           data:{
             y_start : year_start,
             y_end : year_end,
             s_start : semt_start,
             s_end : semt_end,
             cus : cust
           },
           headers: {
           },
           beforeSend: function () {
             $('html, body').animate({
                 scrollTop: $("#detail_chart_bottom").position().top
             }, 500);
             $("#loader_chart_bottom").fadeIn(1000);
               // $('#loading_purple').show();
           },
           success: function (data) {
             $("#loader_chart_bottom").fadeOut(1000);

             $('#chart_detail_bottom').highcharts({

                       title: {
                           text: " "
                       },
                       // exporting: {
                       //     enabled: false
                       // },
                       chart: {
                          type: 'column'
                      },
                       xAxis: {
                           categories: data.categ[0].category,
                           id: 'x-axis'
                       },
                       yAxis: {
                           title: {
                               text: "<b>Scores</b>"
                           }
                       },
                       credits:{
                         enabled: false,

                       },
                       tooltip: {
                           // headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
                           // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                           //         '<td style="padding:0"><b>USD {point.y}</b></td></tr>',
                           // footerFormat: '</table>',
                           // shared: true,
                           // useHTML: true
                           enabled:false
                       },
                       plotOptions: {
                                 series: {
                                   label: {
                                     connectorAllowed: false
                                   },
                                   cursor: 'pointer',
                                   point: {
                                     events: {
                                       click: function () {
                                          detail_grafik_cus(this.category)
                                       }
                                     }
                                   }
                                 }
                               },
                       colors: [
                               '#05354D',
                               '#E6E7EB',
                               '#F2C573',
                               '#8EC3A7',
                               '#F24738',
                               '#92A8CD'
                               ],
                       series: [{
                           name: 'Final Score',
                           data: data.data[0].finalscore,
                           lineWidth: 5
                       },{
                         showInLegend: false,
                          type: 'spline',
                          data: data.data[0].finalscore,
                          color: '#ffdf6d',
                          marker: {
                              lineWidth: 2,
                              lineColor: 'orange',
                              fillColor: 'white'
                          }
                       }]
                   });
                 }
               });
    }

  function load_chart_final(){
    var year_start = $("#dateyear3").val();
    var year_end = $("#dateyear4").val();
    var semt_start = $("#semester_start").val();
    var semt_end = $("#semester_end").val();
    var cust = $("#customer").val();

    if (cust == null){
      cust = "all";
    }

    $.ajax({
         url: '<?= base_url() ?>index.php/csi_dashboard_new/show_chart_accumulation',
         type: 'POST',
         dataType: 'JSON',
         data:{
           y_start : year_start,
           y_end : year_end,
           s_start : semt_start,
           s_end : semt_end,
           cus : cust
         },
         headers: {
         },
         beforeSend: function () {
           $("#loader").fadeIn(1000);
             // $('#loading_purple').show();
         },
         success: function (data) {
           $("#loader").fadeOut(1000);

           $('#chart_csi_1').highcharts({

                     title: {
                         text: " "
                     },
                     exporting: {
                         enabled: false
                     },
                     chart: {
                       height:250,
                        type: 'column'
                    },
                     xAxis: {
                         categories: data.categ[0].category,
                         id: 'x-axis'
                     },
                     yAxis: {
                         title: {
                             text: "<b>Scores</b>"
                         }
                     },
                     credits:{
                       enabled: false,

                     },
                     tooltip: {
                         headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
                         pointFormat: '<tr><td style="color:{series.color};padding:0">Final Score: </td>' +
                                 '<td style="padding:0"><b> {point.y}</b></td></tr>',
                         footerFormat: '</table>',
                         shared: true,
                         useHTML: true
                     },
                     plotOptions: {
                               series: {
                                 label: {
                                   connectorAllowed: false
                                 },
                                 cursor: 'pointer',
                                 point: {
                                   events: {
                                     click: function () {
                                        detail_grafik_cus(this.category)
                                     }
                                   }
                                 }
                               }
                             },
                     colors: [
                             '#05354D',
                             '#E6E7EB',
                             '#F2C573',
                             '#8EC3A7',
                             '#F24738',
                             '#92A8CD'
                             ],
                     series: [{
                       showInLegend: false,
                         // name: 'Final Score',
                         data: data.data[0].finalscore,
                         // lineWidth: 5
                     },{
                       showInLegend: false,
                        type: 'spline',
                        name: 'Final Score',
                        data: data.data[0].finalscore,
                        color: '#ffdf6d',
                        marker: {
                            lineWidth: 2,
                            lineColor: 'orange',
                            fillColor: 'white'
                        }
                     }]
                 });
               }
             });
      }

  function load_chart_key(){
    var year_start = $("#dateyear3").val();
    var year_end = $("#dateyear4").val();
    var semt_start = $("#semester_start").val();
    var semt_end = $("#semester_end").val();
    var cust = $("#customer").val();

    if (cust == null){
      cust = "all";
    }

    $.ajax({
         url: '<?= base_url() ?>index.php/csi_dashboard_new/show_chart_key',
         type: 'POST',
         dataType: 'JSON',
         data:{
           y_start : year_start,
           y_end : year_end,
           s_start : semt_start,
           s_end : semt_end,
           cus : cust,
           level : 1
         },
         headers: {
         },
         beforeSend: function () {
           $("#loader_key").fadeIn(1000);
         },
         success: function (data) {
           $("#loader_key").fadeOut(1000);

           $('#chart_key').highcharts({
                     title: {
                         text: " "
                     },
                     exporting: {
                         enabled: false
                     },
                     chart: {
                       height:250,
                        type: 'column'
                    },
                     xAxis: {
                         categories: data.categ[0].category,
                         id: 'x-axis'
                     },
                     yAxis: {
                         title: {
                             text: "<b>Scores</b>"
                         }
                     },
                     credits:{
                       enabled: false,

                     },
                     colors: [
                             '#0D2633',
                             '#05354D',
                             '#E6E7EB',
                             '#F2C573',
                             '#8EC3A7',
                             '#F24738',
                             '#92A8CD'
                             ],
                     series: [
                     {
                         // type: 'areaspline',
                         // fillColor: {
                         //    linearGradient: [0,0,0,200],
                         //    stops: [
                         //       [0, "#0D2633"],
                         //       [1, "#fff3"]
                         //    ]
                         // },
                         showInLegend: false,
                         lineWidth: 1,
                         name: 'Communication',
                         data: data.chart2_com[0].com_csi
                     },{
                       showInLegend:false,
                        type: 'spline',
                        name: 'Communication',
                        data: data.chart2_com[0].com_csi,
                        color: '#ffdf6d',
                        marker: {
                            lineWidth: 2,
                            lineColor: 'orange',
                            fillColor: 'white'
                        }
                    }
                     // ,{
                     //   // type: 'areaspline',
                     //   //   fillColor: {
                     //   //      linearGradient: [0,0,0,200],
                     //   //      stops: [
                     //   //         [0, "#05354D"],
                     //   //         [1, "#fff3"]
                     //   //      ]
                     //   //   },
                     //   showInLegend: false,
                     //     lineWidth: 1,
                     //     name: 'Excellent Quality Standards',
                     //     data: data.chart2_bqs[0].bqs_csi
                     // },{
                     //     // type: 'areaspline',
                     //     // fillColor: {
                     //     //    linearGradient: [0,0,0,200],
                     //     //    stops: [
                     //     //       [0, "#E6E7EB"],
                     //     //       [1, "#fff3"]
                     //     //    ]
                     //     // },
                     //     showInLegend: false,
                     //     lineWidth: 1,
                     //     name: 'On time Delivery',
                     //     data: data.chart2_otd[0].otd_csi
                     // },{
                     //     // type: 'areaspline',
                     //     // fillColor: {
                     //     //    linearGradient: [0,0,0,200],
                     //     //    stops: [
                     //     //       [0, "#F2C573"],
                     //     //       [1, "#fff3"]
                     //     //    ]
                     //     // },
                     //     showInLegend: false,
                     //     lineWidth: 1,
                     //     name: 'Personnel Competency & Availability',
                     //     data: data.chart2_pca[0].pca_csi
                     // },{
                     //     // type: 'areaspline',
                     //     // fillColor: {
                     //     //    linearGradient: [0,0,0,200],
                     //     //    stops: [
                     //     //       [0, "#8EC3A7"],
                     //     //       [1, "#fff3"]
                     //     //    ]
                     //     // },
                     //     showInLegend: false,
                     //     lineWidth: 1,
                     //     name: 'Safety Standard',
                     //     data: data.chart2_ss[0].ss_csi
                     // },{
                     //     // type: 'areaspline',
                     //     // fillColor: {
                     //     //    linearGradient: [0,0,0,200],
                     //     //    stops: [
                     //     //       [0, "#F24738"],
                     //     //       [1, "#fff3"]
                     //     //    ]
                     //     // },
                     //     showInLegend: false,
                     //     lineWidth: 1,
                     //     name: 'Other Supporting Service',
                     //     data: data.chart2_oss[0].oss_csi
                     // },{
                     //     // type: 'areaspline',
                     //     // fillColor: {
                     //     //    linearGradient: [0,0,0,200],
                     //     //    stops: [
                     //     //       [0, "#92A8CD"],
                     //     //       [1, "#fff3"]
                     //     //    ]
                     //     // },
                     //     showInLegend: false,
                     //     lineWidth: 1,
                     //     name: 'Facilities Management',
                     //     data: data.chart2_fm[0].fm_csi
                     // }
                     ]
                 });
            }
        });
      }

  function load_chart_high(){
    var year_start = $("#dateyear3").val();
    var year_end = $("#dateyear4").val();
    var semt_start = $("#semester_start").val();
    var semt_end = $("#semester_end").val();
    var cust = $("#customer").val();

    if (cust == null){
      cust = "all";
    }

    $.ajax({
         url: '<?= base_url() ?>index.php/csi_dashboard_new/show_chart_key',
         type: 'POST',
         dataType: 'JSON',
         data:{
           y_start : year_start,
           y_end : year_end,
           s_start : semt_start,
           s_end : semt_end,
           cus : cust,
           level : 2
         },
         headers: {
         },
         beforeSend: function () {
             $("#loader_high").fadeIn(1000);
         },
         success: function (data) {
             $("#loader_high").fadeOut(1000);

           $('#chart_high').highcharts({
                     title: {
                         text: " "
                     },
                     exporting: {
                         enabled: false
                     },
                     chart: {
                       height:250,
                        type: 'column'
                    },
                     xAxis: {
                         categories: data.categ[0].category,
                         id: 'x-axis'
                     },
                     yAxis: {
                         title: {
                             text: "<b>Scores</b>"
                         }
                     },
                     credits:{
                       enabled: false,

                     },
                     colors: [
                             '#0D2633',
                             '#05354D',
                             '#E6E7EB',
                             '#F2C573',
                             '#8EC3A7',
                             '#F24738',
                             '#92A8CD'
                             ],
                     series: [
                     {
                       showInLegend: false,
                         name: 'Communication',
                         data: data.chart2_com[0].com_csi
                     },{
                       showInLegend:false,
                        type: 'spline',
                        name: 'Communication',
                        data: data.chart2_com[0].com_csi,
                        color: '#ffdf6d',
                        marker: {
                            lineWidth: 2,
                            lineColor: 'orange',
                            fillColor: 'white'
                        }
                    }
                     // ,{
                     //     name: 'Excellent Quality Standards',
                     //     data: data.chart2_bqs[0].bqs_csi
                     // },{
                     //     name: 'On time Delivery',
                     //     data: data.chart2_otd[0].otd_csi
                     // },{
                     //     name: 'Personnel Competency & Availability',
                     //     data: data.chart2_pca[0].pca_csi
                     // },{
                     //     name: 'Safety Standard',
                     //     data: data.chart2_ss[0].ss_csi
                     // },{
                     //     name: 'Other Supporting Service',
                     //     data: data.chart2_oss[0].oss_csi
                     // },{
                     //     name: 'Facilities Management',
                     //     data: data.chart2_fm[0].fm_csi
                     // }
                     ]
                 });
               }
            });
        }

  function load_chart_general(){
    var year_start = $("#dateyear3").val();
    var year_end = $("#dateyear4").val();
    var semt_start = $("#semester_start").val();
    var semt_end = $("#semester_end").val();
    var cust = $("#customer").val();

    if (cust == null){
      cust = "all";
    }

    $.ajax({
         url: '<?= base_url() ?>index.php/csi_dashboard_new/show_chart_key',
         type: 'POST',
         dataType: 'JSON',
         data:{
           y_start : year_start,
           y_end : year_end,
           s_start : semt_start,
           s_end : semt_end,
           cus : cust,
           level : 3
         },
         headers: {
         },
         beforeSend: function () {
            $("#loader_general").fadeIn(1000);
         },
         success: function (data) {
            $("#loader_general").fadeOut(1000);

           $('#chart_general').highcharts({
                     title: {
                         text: " "
                     },
                     exporting: {
                         enabled: false
                     },
                     chart: {
                       height:250,
                        type: 'column'
                    },
                     xAxis: {
                         categories: data.categ[0].category,
                         id: 'x-axis'
                     },
                     yAxis: {
                         title: {
                             text: "<b>Scores</b>"
                         }
                     },
                     credits:{
                       enabled: false,

                     },
                     colors: [
                             '#0D2633',
                             '#05354D',
                             '#E6E7EB',
                             '#F2C573',
                             '#8EC3A7',
                             '#F24738',
                             '#92A8CD',
                             '#ffbb2b'
                             ],
                     series: [
                     {
                       showInLegend: false,
                         name: 'Communication',
                         data: data.chart2_com[0].com_csi
                     },{
                       showInLegend:false,
                        type: 'spline',
                        name: 'Communication',
                        data: data.chart2_com[0].com_csi,
                        color: '#ffdf6d',
                        marker: {
                            lineWidth: 2,
                            lineColor: 'orange',
                            fillColor: 'white'
                        }
                    }
                     // ,{
                     //     name: 'Excellent Quality Standards',
                     //     data: data.chart2_bqs[0].bqs_csi
                     // },{
                     //     name: 'On time Delivery',
                     //     data: data.chart2_otd[0].otd_csi
                     // },{
                     //     name: 'Personnel Competency & Availability',
                     //     data: data.chart2_pca[0].pca_csi
                     // },{
                     //     name: 'Safety Standard',
                     //     data: data.chart2_ss[0].ss_csi
                     // },{
                     //     name: 'Other Supporting Service',
                     //     data: data.chart2_oss[0].oss_csi
                     // },{
                     //     name: 'Facilities Management',
                     //     data: data.chart2_fm[0].fm_csi
                     // }
                     ]
                 });
            }
        });
    }

  function load_chart_one(){
      var year_start = $("#dateyear3").val();
      var year_end = $("#dateyear4").val();
      var semt_start = $("#semester_start").val();
      var semt_end = $("#semester_end").val();
      var cust = $("#customer").val();

      $.ajax({
           url: '<?= base_url() ?>index.php/csi_dashboard_new/show_chart_key',
           type: 'POST',
           dataType: 'JSON',
           data:{
             y_start : year_start,
             y_end : year_end,
             s_start : semt_start,
             s_end : semt_end,
             cus : cust,
             level : 3
           },
           headers: {
           },
           beforeSend: function () {
                $("#loader_one").fadeIn(1000);
           },
           success: function (data) {
                $("#loader_one").fadeOut(1000);

             $('#chart_one').highcharts({
                       title: {
                           text: " "
                       },
                       exporting: {
                           enabled: false
                       },
                       xAxis: {
                           categories: data.categ[0].category,
                           id: 'x-axis'
                       },
                       yAxis: {
                           title: {
                               text: "<b>Scores</b>"
                           }
                       },
                       credits:{
                         enabled: false,

                       },
                       colors: [
                               '#0D2633',
                               '#05354D',
                               '#E6E7EB',
                               '#F2C573',
                               '#8EC3A7',
                               '#F24738',
                               '#92A8CD'
                               ],
                       series: [
                       {
                           name: 'Communication',
                           data: data.chart2_com[0].com_csi
                       },{
                           name: 'Excellent Quality Standards',
                           data: data.chart2_bqs[0].bqs_csi
                       },{
                           name: 'On time Delivery',
                           data: data.chart2_otd[0].otd_csi
                       },{
                           name: 'Personnel Competency & Availability',
                           data: data.chart2_pca[0].pca_csi
                       },{
                           name: 'Safety Standard',
                           data: data.chart2_ss[0].ss_csi
                       },{
                           name: 'Other Supporting Service',
                           data: data.chart2_oss[0].oss_csi
                       },{
                           name: 'Facilities Management',
                           data: data.chart2_fm[0].fm_csi
                       }
                       ]
                   });
              }
          });
      }

  function load_chart_bisnis(){
        var year_start = $("#dateyear3").val();
        var year_end = $("#dateyear4").val();
        var semt_start = $("#semester_start").val();
        var semt_end = $("#semester_end").val();
        var cust = $("#customer").val();

            if (cust == null){
              cust = "all";
            }

        $.ajax({
             url: '<?= base_url() ?>index.php/csi_dashboard_new/show_chart_bisnis',
             type: 'POST',
             dataType: 'JSON',
             data:{
               y_start : year_start,
               y_end : year_end,
               s_start : semt_start,
               s_end : semt_end,
               cus : cust
             },
             headers: {
             },
             beforeSend: function () {
                    $("#loader_bisnis").fadeIn(1000);
             },
             success: function (data) {
                    $("#loader_bisnis").fadeOut(1000);

               $('#chart_bisnis').highcharts({

                       title: {
                           text: ""
                       },
                       exporting: {
                           enabled: false
                       },
                       chart: {
                         height:250,
                          type: 'column'
                      },
                       xAxis: {
                           categories: data.categ[0].category,
                           id: 'x-axis'
                       },
                       yAxis: {
                           title: {
                               text: "<b>Scores</b>"
                           }
                       },
                       credits:{
                         enabled: false,

                       },
                       colors: [
                               '#05354D',
                               '#E6E7EB',
                               '#F2C573',
                               '#8EC3A7',
                               '#F24738',
                               '#92A8CD'
                               ],
                       series: [{
                         showInLegend: false,
                           name: 'Business Unit Score',
                           data: data.data_bus[0].avg_bus
                       },{
                         showInLegend: false,
                          type: 'spline',
                          data: data.data_bus[0].avg_bus,
                          color: '#ffdf6d',
                          marker: {
                              lineWidth: 2,
                              lineColor: 'orange',
                              fillColor: 'white'
                          }
                      }]
                   });

                   $('#chart_support').highcharts({

                           title: {
                               text: ""
                           },
                           chart: {
                             height:250,
                              type: 'column'
                          },
                          exporting: {
                              enabled: false
                          },
                           xAxis: {
                               categories: data.categ[0].category,
                               id: 'x-axis'
                           },
                           yAxis: {
                               title: {
                                   text: "<b>Scores</b>"
                               }
                           },
                           credits:{
                             enabled: false,

                           },
                           colors: [
                                   '#05354D',
                                   '#E6E7EB',
                                   '#F2C573',
                                   '#8EC3A7',
                                   '#F24738',
                                   '#92A8CD'
                                   ],
                           series: [{
                             showInLegend: false,
                               name: 'Supporting Unit Score',
                               data: data.data_sus[0].avg_sus
                           },{
                              showInLegend: false,
                              type: 'spline',
                              data: data.data_sus[0].avg_sus,
                              color: '#ffdf6d',
                              marker: {
                                  lineWidth: 2,
                                  lineColor: 'orange',
                                  fillColor: 'white'
                              }
                          }]
                       });

                }
            });
      }

  function addsemesterly(){
    load_chart_final();
    load_chart_bisnis();
    var cus = $("#customer").val();

    if(cus == 'all'){
      $('#allchart2').removeClass('hidden');
      $('#onechart').addClass('hidden');
      load_chart_key();
      load_chart_high();
      load_chart_general();
    }else{
      $('#allchart2').addClass('hidden');
      $('#onechart').removeClass('hidden');
      load_chart_one();
    }
  }

  function detail_grafik_cus(datas){
    $('#titlechart').html("Semester "+datas);
    $('#modal_detail_chart').modal('show');
    var customer = $('#customerdetail').val();
    var rangedet = $('#rangedetail').val();

    if(customer ==''){
    	customer = "all";
    }

    $.ajax({
            url: '<?= base_url() ?>index.php/csi_dashboard_new/load_barchart_total_score',
            type: 'POST',
            data:{
              date : datas,
              cus : customer
            },
            headers: {
                'token': localStorage.getItem('status')
            },
            dataType: 'JSON',
            beforeSend: function () {
                // $('#loading_purple').show();
            },
            success: function (data) {
              $('#modal_chart1').highcharts({
                        chart: {
                            type: "column"
                        },
                        title: {
                            text: "Detail Total Chart"
                        },
                        xAxis: {
                            categories: data.data_short[0].shortness,
                            id: 'x-axis'
                        },
                        yAxis: {
                            title: {
                                text: "<b>Average</b>"
                            }
                        },
                        credits:{
                          enabled: false,

                        },
                        colors: [
                                '#05354D',
                                '#E6E7EB',
                                '#F2C573',
                                '#8EC3A7',
                                '#F24738',
                                '#92A8CD'
                                ],
                        series: [{
                            name: 'Total Score',
                            data: data.data_total[0].total_score
                        }]

                    });
                }
          });
      }

       function load_chart(){
         var year_start = $("#dateyear3").val();
         var year_end = $("#dateyear4").val();
         var semt_start = $("#semester_start").val();
         var semt_end = $("#semester_end").val();
         var cust = $("#customer").val();

         if (cust == null){
           cust = "all";
         }

         $.ajax({
              url: '<?= base_url() ?>index.php/csi_dashboard_new/show_chart_semesterly',
              type: 'POST',
              dataType: 'JSON',
              data:{
                y_start : year_start,
                y_end : year_end,
                s_start : semt_start,
                s_end : semt_end,
                cus : cust
              },
              headers: {
              },
              beforeSend: function () {
                  // $('#loading_purple').show();
              },
              success: function (data) {

                $('#chart_csi_1').highcharts({
                          title: {
                              text: "Final Score"
                          },
                          xAxis: {
                              categories: data.cat1_val[0].date_type,
                              id: 'x-axis'
                          },
                          yAxis: {
                              title: {
                                  text: "<b>Scores</b>"
                              }
                          },
                          credits:{
                            enabled: false,

                          },
                          plotOptions: {
                                    series: {
                                      label: {
                                        connectorAllowed: false
                                      },
                                      cursor: 'pointer',
                                      point: {
                                        events: {
                                          click: function () {
                                             detail_grafik_cus(this.category)
                                          }
                                        }
                                      }
                                    }
                                  },
                          colors: [
                                  '#05354D',
                                  '#E6E7EB',
                                  '#F2C573',
                                  '#8EC3A7',
                                  '#F24738',
                                  '#92A8CD'
                                  ],
                          series: [{
                              name: 'Final Score',
                              data: data.chart1_final[0].finalscore,
                              lineWidth: 5
                          }]
                      });

                  $('#chart_bisnis').highcharts({

                          title: {
                              text: "Business Unit"
                          },
                          xAxis: {
                              categories: data.cat3_val[0].date_type,
                              id: 'x-axis'
                          },
                          yAxis: {
                              title: {
                                  text: "<b>Scores</b>"
                              }
                          },
                          credits:{
                            enabled: false,

                          },
                          colors: [
                                  '#05354D',
                                  '#E6E7EB',
                                  '#F2C573',
                                  '#8EC3A7',
                                  '#F24738',
                                  '#92A8CD'
                                  ],
                          series: [{
                              name: 'Business Unit Score',
                              data: data.chart3_bus[0].avg_bus
                          }]
                      });
                  $('#chart_support').highcharts({
                          title: {
                              text: "Supporting Unit"
                          },
                          xAxis: {
                              categories: data.cat3_val[0].date_type,
                              id: 'x-axis'
                          },
                          yAxis: {
                              title: {
                                  text: "<b>Scores</b>"
                              }
                          },
                          credits:{
                            enabled: false,

                          },
                          colors: [
                                  '#05354D',
                                  '#E6E7EB',
                                  '#F2C573',
                                  '#8EC3A7',
                                  '#F24738',
                                  '#92A8CD'
                                  ],
                          series: [{
                              name: 'Support Unit Score',
                              data: data.chart3_sus[0].avg_sus
                          }]
                      });
                $('#chart_one').highcharts({
                          title: {
                              text: ""
                          },
                          xAxis: {
                              categories: data.cat2_val[0].date_type,
                              id: 'x-axis'
                          },
                          yAxis: {
                              title: {
                                  text: "<b>Scores</b>"
                              }
                          },
                          credits:{
                            enabled: false,

                          },
                          colors: [
                                  '#0D2633',
                                  '#05354D',
                                  '#E6E7EB',
                                  '#F2C573',
                                  '#8EC3A7',
                                  '#F24738',
                                  '#92A8CD'
                                  ],
                          series: [
                          {
                              name: 'Communication',
                              data: data.chart2_com[0].com_csi
                          },{
                              name: 'Excellent Quality Standards',
                              data: data.chart2_bqs[0].bqs_csi
                          },{
                              name: 'On time Delivery',
                              data: data.chart2_otd[0].otd_csi
                          },{
                              name: 'Personnel Competency & Availability',
                              data: data.chart2_pca[0].pca_csi
                          },{
                              name: 'Safety Standard',
                              data: data.chart2_ss[0].ss_csi
                          },{
                              name: 'Other Supporting Service',
                              data: data.chart2_oss[0].oss_csi
                          },{
                              name: 'Facilities Management',
                              data: data.chart2_fm[0].fm_csi
                          }
                          ]
                      });
                $('#chart_key').highcharts({
                          title: {
                              text: "Key Customer"
                          },
                          xAxis: {
                              categories: data.cat2_val[0].date_type,
                              id: 'x-axis'
                          },
                          yAxis: {
                              title: {
                                  text: "<b>Scores</b>"
                              }
                          },
                          credits:{
                            enabled: false,

                          },
                          colors: [
                                  '#0D2633',
                                  '#05354D',
                                  '#E6E7EB',
                                  '#F2C573',
                                  '#8EC3A7',
                                  '#F24738',
                                  '#92A8CD'
                                  ],
                          series: [
                          {
                              name: 'Communication',
                              data: data.chart2_com[0].com_csi
                          },{
                              name: 'Excellent Quality Standards',
                              data: data.chart2_bqs[0].bqs_csi
                          },{
                              name: 'On time Delivery',
                              data: data.chart2_otd[0].otd_csi
                          },{
                              name: 'Personnel Competency & Availability',
                              data: data.chart2_pca[0].pca_csi
                          },{
                              name: 'Safety Standard',
                              data: data.chart2_ss[0].ss_csi
                          },{
                              name: 'Other Supporting Service',
                              data: data.chart2_oss[0].oss_csi
                          },{
                              name: 'Facilities Management',
                              data: data.chart2_fm[0].fm_csi
                          }
                          ]
                      });

                $('#chart_high').highcharts({
                          title: {
                              text: "High Value Brand Customer"
                          },
                          xAxis: {
                              categories: data.cat2_val[0].date_type,
                              id: 'x-axis'
                          },
                          yAxis: {
                              title: {
                                  text: "<b>Scores</b>"
                              }
                          },
                          credits:{
                            enabled: false,

                          },
                          colors: [
                                  '#0D2633',
                                  '#05354D',
                                  '#E6E7EB',
                                  '#F2C573',
                                  '#8EC3A7',
                                  '#F24738',
                                  '#92A8CD'
                                  ],
                          series: [
                          {
                              name: 'Communication',
                              data: data.chart2_high_com[0].com_csi
                          },{
                              name: 'Excellent Quality Standards',
                              data: data.chart2_high_bqs[0].bqs_csi
                          },{
                              name: 'On time Delivery',
                              data: data.chart2_high_otd[0].otd_csi
                          },{
                              name: 'Personnel Competency & Availability',
                              data: data.chart2_high_pca[0].pca_csi
                          },{
                              name: 'Safety Standard',
                              data: data.chart2_high_ss[0].ss_csi
                          },{
                              name: 'Other Supporting Service',
                              data: data.chart2_high_oss[0].oss_csi
                          },{
                              name: 'Facilities Management',
                              data: data.chart2_high_fm[0].fm_csi
                          }
                          ]
                      });

                $('#chart_general').highcharts({
                          title: {
                              text: "General Customer"
                          },
                          xAxis: {
                              categories: data.cat2_val[0].date_type,
                              id: 'x-axis'
                          },
                          yAxis: {
                              title: {
                                  text: "<b>Scores</b>"
                              }
                          },
                          credits:{
                            enabled: false,

                          },
                          colors: [
                                  '#0D2633',
                                  '#05354D',
                                  '#E6E7EB',
                                  '#F2C573',
                                  '#8EC3A7',
                                  '#F24738',
                                  '#92A8CD'
                                  ],
                          series: [
                          {
                              name: 'Communication',
                              data: data.chart2_gen_com[0].com_csi
                          },{
                              name: 'Excellent Quality Standards',
                              data: data.chart2_gen_bqs[0].bqs_csi
                          },{
                              name: 'On time Delivery',
                              data: data.chart2_gen_otd[0].otd_csi
                          },{
                              name: 'Personnel Competency & Availability',
                              data: data.chart2_gen_pca[0].pca_csi
                          },{
                              name: 'Safety Standard',
                              data: data.chart2_gen_ss[0].ss_csi
                          },{
                              name: 'Other Supporting Service',
                              data: data.chart2_gen_oss[0].oss_csi
                          },{
                              name: 'Facilities Management',
                              data: data.chart2_gen_fm[0].fm_csi
                          }
                          ]
                      });

                  }
              });
       }

// });
function detail_grafik(datas){
// alert(data);
$('#titlechart').html(datas);
$('#modal_detail_chart').modal('show');

$.ajax({
        url: '<?= base_url() ?>index.php/csi_dashboard_new/load_chart1_total_score?date='+datas,
        type: 'GET',
        headers: {
            'token': localStorage.getItem('status')
        },
        dataType: 'JSON',
        beforeSend: function () {
            // $('#loading_purple').show();
        },
        success: function (data) {
          // console.log(data.cat_1[0].date_type);
          $('#modal_chart1').highcharts({
                    chart: {
                        type: "column"
                    },
                    title: {
                        text: "Detail Total Score"
                    },
                    xAxis: {
                        categories: data.data_short[0].shortness,
                        id: 'x-axis'
                    },
                    yAxis: {
                        title: {
                            text: "<b>Average</b>"
                        }
                    },
                    credits:{
                      enabled: false,

                    },
                    colors: [
                            '#05354D',
                            '#E6E7EB',
                            '#F2C573',
                            '#8EC3A7',
                            '#F24738',
                            '#92A8CD'
                            ],
                    series: [{
                        name: 'Total Score',
                        data: data.data_total[0].total_score
                    }]

                });
            }
      });

}

// $('#range').change(function(){
// var value = $('#range').val();
// //value 1 = monthly, val 2 = semesterly
//   if(value == 'monthly'){
//     $('#monthly_start_date').removeClass('hidden');
//     $('#monthly_end_date').removeClass('hidden');
//     $('#semesterlystartdate').addClass('hidden');
//     $('#semesterlyenddate').addClass('hidden');
//     $('#addmonthly').removeClass('hidden');
//     $('#addsemesterly').addClass('hidden');
//   }else{
//     $('#semesterlystartdate').removeClass('hidden');
//     $('#semesterlyenddate').removeClass('hidden');
//     $('#monthly_start_date').addClass('hidden');
//     $('#monthly_end_date').addClass('hidden');
//     $('#addmonthly').addClass('hidden');
//     $('#addsemesterly').removeClass('hidden');
// }
//
// });



// function load_chart1_month(){
//   $.ajax({
//         url: '<?= base_url() ?>index.php/csi_dashboard_new/load_chart1_month',
//         type: 'GET',
//         headers: {
//             'token': localStorage.getItem('status')
//         },
//         dataType: 'JSON',
//         beforeSend: function () {
//             // $('#loading_purple').show();
//         },
//         success: function (data) {
//           $('#chart_csi_1').highcharts({
//                     title: {
//                         text: ""
//                     },
//                     xAxis: {
//                         categories: data.cat_1[0].date_type,
//                         id: 'x-axis'
//                     },
//                     yAxis: {
//                         title: {
//                             text: "Scores"
//                         }
//                     },
//                     credits:{
//                       enabled: false,
//
//                     },
//                     plotOptions: {
//                               series: {
//                                 label: {
//                                   connectorAllowed: false
//                                 },
//                                 cursor: 'pointer',
//                                 point: {
//                                   events: {
//                                     click: function () {
//                                        detail_grafik_cus(this.category)
//                                     }
//                                   }
//                                 }
//                               }
//                             },
//                     colors: [
//                             '#05354D',
//                             '#E6E7EB',
//                             '#F2C573',
//                             '#8EC3A7',
//                             '#F24738',
//                             '#92A8CD'
//                             ],
//                     series: [{
//                         name: 'Final Score',
//                         data: data.data_chart[0].finalscore
//                     }]
//
//                 });
//             }
//
//
// });
// }

// function load_chart2_month(){
//     $.ajax({
//         url: '<?= base_url() ?>index.php/csi_dashboard_new/load_chart2_month',
//         type: 'GET',
//         headers: {
//             'token': localStorage.getItem('status')
//         },
//         dataType: 'JSON',
//         beforeSend: function () {
//             // $('#loading_purple').show();
//         },
//         success: function (data) {
//           $('#chart_key').highcharts({
//                     title: {
//                         text: "Key Customer"
//                     },
//                     xAxis: {
//                         categories: data.cat_val[0].date_type,
//                         id: 'x-axis'
//
//                     },
//                     yAxis: {
//                         title: {
//                             text: "Scores"
//                         }
//                     },
//
//                     credits:{
//                       enabled: false,
//
//                     },
//                     colors: [
//                             '#0D2633',
//                             '#05354D',
//                             '#E6E7EB',
//                             '#F2C573',
//                             '#8EC3A7',
//                             '#F24738',
//                             '#92A8CD'
//                             ],
//                     series: [{
//                         name: 'Communication',
//                         data: data.chart_key_com[0].com_csi
//                     },{
//                         name: 'Excellent Quality Standards',
//                         data: data.chart_key_bqs[0].bqs_csi
//                     },{
//                         name: 'On time Delivery',
//                         data: data.chart_key_otd[0].otd_csi
//                     },{
//                         name: 'Personnel Competency & Availability',
//                         data: data.chart_key_pca[0].pca_csi
//                     },{
//                         name: 'Quality Assurance',
//                         data: data.chart_key_qa[0].qa_csi
//                     }
//                     ]
//                 });
//
//                 $('#chart_high').highcharts({
//                     // chart: {
//                     //     type: "column"
//                     // },
//                     title: {
//                         text: "High Value Brand Customer"
//                     },
//                     xAxis: {
//                         categories: data.cat_val[0].date_type,
//                         id: 'x-axis'
//                     },
//                     yAxis: {
//                         title: {
//                             text: "Scores"
//                         }
//                     },
//
//                     credits:{
//                       enabled: false,
//
//                     },
//                     colors: [
//                             '#0D2633',
//                             '#05354D',
//                             '#E6E7EB',
//                             '#F2C573',
//                             '#8EC3A7',
//                             '#F24738',
//                             '#92A8CD'
//                             ],
//                     series: [{
//                         name: 'Communication',
//                         data: data.chart_high_com[0].com_csi
//                     },{
//                         name: 'Excellent Quality Standards',
//                         data: data.chart_high_bqs[0].bqs_csi
//                     },{
//                         name: 'On time Delivery',
//                         data: data.chart_high_otd[0].otd_csi
//                     },{
//                         name: 'Personnel Competency & Availability',
//                         data: data.chart_high_pca[0].pca_csi
//                     },{
//                         name: 'Quality Assurance',
//                         data: data.chart_high_qa[0].qa_csi
//                     }
//                     ]
//                 });
//
//                 $('#chart_general').highcharts({
//                     // chart: {
//                     //     type: "column"
//                     // },
//                     title: {
//                         text: "General Customer"
//                     },
//                     xAxis: {
//                         categories: data.cat_val[0].date_type,
//                         id: 'x-axis'
//                     },
//                     yAxis: {
//                         title: {
//                             text: "Scores"
//                         }
//                     },
//
//                     credits:{
//                       enabled: false,
//
//                     },
//                     colors: [
//                             '#0D2633',
//                             '#05354D',
//                             '#E6E7EB',
//                             '#F2C573',
//                             '#8EC3A7',
//                             '#F24738',
//                             '#92A8CD'
//                             ],
//                     series: [{
//                         name: 'Communication',
//                         data: data.chart_general_com[0].com_csi
//                     },{
//                         name: 'Excellent Quality Standards',
//                         data: data.chart_general_bqs[0].bqs_csi
//                     },{
//                         name: 'On time Delivery',
//                         data: data.chart_general_otd[0].otd_csi
//                     },{
//                         name: 'Personnel Competency & Availability',
//                         data: data.chart_general_pca[0].pca_csi
//                     },{
//                         name: 'Quality Assurance',
//                         data: data.chart_general_qa[0].qa_csi
//                     }
//                     ]
//                 });
//             }
//
//
// });
// }

// function load_chart3_month(){
//     $.ajax({
//         url: '<?= base_url() ?>index.php/csi_dashboard_new/load_chart3_month',
//         type: 'GET',
//         headers: {
//             'token': localStorage.getItem('status')
//         },
//         dataType: 'JSON',
//         beforeSend: function () {
//             // $('#loading_purple').show();
//         },
//         success: function (data) {
//
//            $('#container33').highcharts({
//                     title: {
//                         text: ""
//                     },
//                     xAxis: {
//                         categories: data.cat_bus[0].date_type,
//                         id: 'x-axis'
//                     },
//                     yAxis: {
//                         title: {
//                             text: "Scores"
//                         }
//                     },
//                     credits:{
//                       enabled: false,
//
//                     },
//                     colors: [
//                             '#05354D',
//                             '#E6E7EB',
//                             '#F2C573',
//                             '#8EC3A7',
//                             '#F24738',
//                             '#92A8CD'
//                             ],
//                     series: [{
//                         name: 'Business Unit Score',
//                         data: data.chart3_bus[0].bus
//                     }]
//                 });
//             }
//
//
// });
// }

// function addmonthly(){
//   var range = $('#range').val();
//   var start_date = $('#date1').val();
//   var end_date = $('#date2').val();
//   var cust = $('#customer').val();
//   $('#customerdetail').val(cust);
//   $('#rangedetail').val(range);
//
//   $('#bisnisblok').addClass('hidden');
//   $('#supportblok').addClass('hidden');
//   $('#bisnisone').removeClass('hidden');
//   var url1 = '<?= base_url() ?>index.php/csi_dashboard_new/show_chart2_all?range='+range+'&start_date='+start_date+'&end_date='+end_date+'&cus='+cust;
//   var url2 = '<?= base_url() ?>index.php/csi_dashboard_new/show_chart2?range='+range+'&start_date='+start_date+'&end_date='+end_date+'&cus='+cust;
//   var url_user = '';
//   if(cust == 'all'){
//   $('#allchart2').removeClass('hidden');
//   $('#onechart').addClass('hidden');
//   url_user = url1;
//   }else{
//   $('#allchart2').addClass('hidden');
//   $('#onechart').removeClass('hidden');
//   url_user = url2;
//
//   }
//   var vas = '';
//
//     $.ajax({
//         url: url_user ,
//         type: 'GET',
//         headers: {
//         },
//         dataType: 'JSON',
//         beforeSend: function () {
//             // $('#loading_purple').show();
//         },
//         success: function (data) {
//           vas = data.cat1_val[0].date_type;
//           $('#chart_csi_1').highcharts({
//                     title: {
//                         text: ""
//                     },
//                     xAxis: {
//                         categories: data.cat1_val[0].date_type,
//                         id: 'x-axis'
//                     },
//                     yAxis: {
//                         title: {
//                             text: "Scores"
//                         }
//                     },
//                     credits:{
//                       enabled: false,
//
//                     },
//                     plotOptions: {
//                               series: {
//                                 label: {
//                                   connectorAllowed: false
//                                 },
//                                 cursor: 'pointer',
//                                 point: {
//                                   events: {
//                                     click: function () {
//                                        detail_grafik_cus(this.category)
//                                     }
//                                   }
//                                 }
//                               }
//                             },
//                     colors: [
//                             '#05354D',
//                             '#E6E7EB',
//                             '#F2C573',
//                             '#8EC3A7',
//                             '#F24738',
//                             '#92A8CD'
//                             ],
//                     series: [{
//                         name: 'Final Score',
//                         data: data.chart1_final[0].finalscore
//                     }]
//                 });
//           $('#chart_one').highcharts({
//                     title: {
//                         text: ""
//                     },
//                     xAxis: {
//                         categories: data.cat2_val[0].date_type,
//                         id: 'x-axis'
//                     },
//                     yAxis: {
//                         title: {
//                             text: "Scores"
//                         }
//                     },
//                     credits:{
//                       enabled: false,
//
//                     },
//                     colors: [
//                             '#0D2633',
//                             '#05354D',
//                             '#E6E7EB',
//                             '#F2C573',
//                             '#8EC3A7',
//                             '#F24738',
//                             '#92A8CD'
//                             ],
//                     series: [
//                     {
//                         name: 'Communication',
//                         data: data.chart2_com[0].com_csi
//                     },{
//                         name: 'Excellent Quality Standards',
//                         data: data.chart2_bqs[0].bqs_csi
//                     },{
//                         name: 'On time Delivery',
//                         data: data.chart2_otd[0].otd_csi
//                     },{
//                         name: 'Personnel Competency & Availability',
//                         data: data.chart2_pca[0].pca_csi
//                     },{
//                         name: 'Quality Assurance',
//                         data: data.chart2_qa[0].qa_csi
//                     }
//                     ]
//                 });
//
//             $('#container33').highcharts({
//                     title: {
//                         text: ""
//                     },
//                     xAxis: {
//                         categories: data.cat3_val[0].date_type,
//                         id: 'x-axis'
//                     },
//                     yAxis: {
//                         title: {
//                             text: "Scores"
//                         }
//                     },
//                     credits:{
//                       enabled: false,
//
//                     },
//                     colors: [
//                             '#05354D',
//                             '#E6E7EB',
//                             '#F2C573',
//                             '#8EC3A7',
//                             '#F24738',
//                             '#92A8CD'
//                             ],
//                     series: [{
//                         name: 'Business Unit Score',
//                         data: data.chart3_bus[0].avg_bus
//                     }]
//                 });
//           $('#chart_key').highcharts({
//                     title: {
//                         text: "Key Customer"
//                     },
//                     xAxis: {
//                         categories: data.cat2_val[0].date_type,
//                         id: 'x-axis'
//                     },
//                     yAxis: {
//                         title: {
//                             text: "Scores"
//                         }
//                     },
//                     credits:{
//                       enabled: false,
//
//                     },
//                     colors: [
//                             '#0D2633',
//                             '#05354D',
//                             '#E6E7EB',
//                             '#F2C573',
//                             '#8EC3A7',
//                             '#F24738',
//                             '#92A8CD'
//                             ],
//                     series: [
//                     {
//                         name: 'Communication',
//                         data: data.chart2_com[0].com_csi
//                     },{
//                         name: 'Excellent Quality Standards',
//                         data: data.chart2_bqs[0].bqs_csi
//                     },{
//                         name: 'On time Delivery',
//                         data: data.chart2_otd[0].otd_csi
//                     },{
//                         name: 'Personnel Competency & Availability',
//                         data: data.chart2_pca[0].pca_csi
//                     },{
//                         name: 'Quality Assurance',
//                         data: data.chart2_qa[0].qa_csi
//                     }
//                     ]
//                 });
//           $('#chart_high').highcharts({
//                     title: {
//                         text: "High Value Brand Customer"
//                     },
//                     xAxis: {
//                         categories: data.cat2_val[0].date_type,
//                         id: 'x-axis'
//                     },
//                     yAxis: {
//                         title: {
//                             text: "Scores"
//                         }
//                     },
//                     credits:{
//                       enabled: false,
//
//                     },
//                     colors: [
//                             '#0D2633',
//                             '#05354D',
//                             '#E6E7EB',
//                             '#F2C573',
//                             '#8EC3A7',
//                             '#F24738',
//                             '#92A8CD'
//                             ],
//                     series: [
//                     {
//                         name: 'Communication',
//                         data: data.chart2_high_com[0].com_csi
//                     },{
//                         name: 'Excellent Quality Standards',
//                         data: data.chart2_high_bqs[0].bqs_csi
//                     },{
//                         name: 'On time Delivery',
//                         data: data.chart2_high_otd[0].otd_csi
//                     },{
//                         name: 'Personnel Competency & Availability',
//                         data: data.chart2_high_pca[0].pca_csi
//                     },{
//                         name: 'Quality Assurance',
//                         data: data.chart2_high_qa[0].qa_csi
//                     }
//                     ]
//                 });
//
//           $('#chart_general').highcharts({
//                     title: {
//                         text: "General Customer"
//                     },
//                     xAxis: {
//                         categories: data.cat2_val[0].date_type,
//                         id: 'x-axis'
//                     },
//                     yAxis: {
//                         title: {
//                             text: "Scores"
//                         }
//                     },
//                     credits:{
//                       enabled: false,
//
//                     },
//                     colors: [
//                             '#0D2633',
//                             '#05354D',
//                             '#E6E7EB',
//                             '#F2C573',
//                             '#8EC3A7',
//                             '#F24738',
//                             '#92A8CD'
//                             ],
//                     series: [
//                     {
//                         name: 'Communication',
//                         data: data.chart2_gen_com[0].com_csi
//                     },{
//                         name: 'Excellent Quality Standards',
//                         data: data.chart2_gen_bqs[0].bqs_csi
//                     },{
//                         name: 'On time Delivery',
//                         data: data.chart2_gen_otd[0].otd_csi
//                     },{
//                         name: 'Personnel Competency & Availability',
//                         data: data.chart2_gen_pca[0].pca_csi
//                     },{
//                         name: 'Quality Assurance',
//                         data: data.chart2_gen_qa[0].qa_csi
//                     }
//                     ]
//                 });
//
//             }
//         });
//   }






</script>
