<style media="screen">
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin: 0;
  line-height: 1;
}
</style>
<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>


<section class="content">


  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= $title ?></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <form id="role_form" action="index.html" method="post">
        <div class="row">
          <div class="col-md-6">
            <label>Role Name</label>
            <input type="text" class="form-control" name="role_name" id="role_name" value="" required>
            <input type="hidden" class="form-control" name="role_id" id="role_id" value="<?= ($role_id != "") ? $role_id : "" ?>">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 ">
            <br>
            <button type="submit" style="margin-left:10px;" class="pull-right  btn btn-flat bg-navy" name="button">Save &nbsp; <i class="fa fa-plus"></i></button>
            <button type="button" class=" pull-right  btn btn-flat btn-default" onclick="window.history.back();"><i class="fa fa-arrow-left"></i> &nbsp Back</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>

<script>
  $("#role_form").parsley();
  $("#role_form").on("submit", function(e){
    e.preventDefault();
    $.ajax({
      url: '<?= base_url("index.php/role/save_role") ?>',
      type: 'POST',
      dataType: 'JSON',
      data: new FormData(this),
      cache: false,
      processData: false,
      contentType: false,
    })
    .done(function(data) {
      if(data.success){
        alert("Data Saved");
        window.location.href="<?= base_url() ?>index.php/role";
      } else{
        notif("error", "Failed to save")
      }
    })
    .fail(function() {
      notif("error", "Failed to save")
    })
    .always(function() {
      console.log("complete");
    });
  })

  $(document).ready(function() {
    var id = $("#role_id").val();
    if(id != ""){
      $.ajax({
        url: '<?= base_url() ?>'+'index.php/role/edit_data_form/'+id,
        type: 'GET',
        dataType: 'json',
      })
      .done(function() {
        notif("success", "Data Loaded")
      })
      .fail(function() {
        notif("error", "Failed to loaded")
      })
      .always(function(data) {
        $("#role_name").val(data.role_name);
      });

    }
  });
</script>
