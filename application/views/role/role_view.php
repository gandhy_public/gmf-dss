<style media="screen">
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin: 0;
  line-height: 1;
}
</style>
<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= $title ?></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="button-add-place pull-left">
            <a href="<?= base_url("index.php/role/role_form"); ?>" class="btn btn-flat bg-navy" name="button">
              Add New Role <i class="fa fa-plus"></i>
            </a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="table table-hover table-bordered table-gmf" id="roleTable">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Role Name</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>


<div class="modal fade" id="modal_role" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header"style="background-color: #001f3f;color: #fff;">
                <h4 class="modal-title" id="title-edit">Edit User Role <span class="nm_tmp"></span></h4>
            </div>
            <div class="modal-body">
				<div class="row">
					<form id="myform" class="myform" method="post" name="myform">
							<input type="hidden" id="id_tmp">
							<div class="col-md-2 label-input" style="padding-top:7px;font-size:20px;font-weight:bold;">Menu </div>
							<div class="col-md-10" id="tampung_role">

							</div>
					</form>
				</div>
            </div>
            <div class="modal-footer">
				<button type="button" class="btn btn-flat btn-default"  data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
				<button id="s_add" type="submit"  class="btn btn-flat bg-navy pull-right" name="submit" value="Submit"><i class="fa fa-save"></i> Simpan</button>
			</div>
        </div>
    </div>
</div>

<script>
  var jumlah_induk = 0;
  var jumlah_anak = 0;

  $("#roleTable").DataTable({
    ordering: true,
    processing: true,
    serverSide: true,
    ajax: {
      url: "<?= base_url() ?>index.php/role/datatable",
      type:'POST',
    },
    "aoColumns": [
      {
        "mData": "0",
        "mRender": function ( data, type, row ) {
          return "<p class='text-center'>"+data+"</p>";
        }
      },
      {
        "mData": "2",
        "mRender": function ( data, type, row ) {
          return "<p class='text-center'>"+data+"</p>";
        }
      },
      {
        "mData": "1",
        "mRender": function ( data, type, row ) {
          var button_del = '<button type="button" data-id="'+data+'" class="btn btn-danger btn-flat" id="del_role" name="button"><i class="fa fa-trash-o"></i></button>';
          var button_edit = '<a href="<?= base_url() ?>index.php/role/role_form/'+data+'" class="btn btn-primary btn-flat" id="edit_role" name="button"><i class="fa fa-wrench"></i></a>';
          var button_menu = '<a  class="btn btn-success btn-flat"  onclick="detail_menu(\''+data+'\')"><i class="fa fa-edit"></i></a>';
          return "<center>"+button_del+" "+button_edit+" "+button_menu+"</center>";
        }
      }
    ]
  });



// function submitForm() {
	$(document).on("click", "#s_add", function(e){
	var id = $("#id_tmp").val();
	 var send_induk = new Array();
	 var send_anak = new Array();
     console.log(jumlah_induk)
            for(var a=0;a<jumlah_induk;a++)
            {
                send_induk[a] = {};
                send_induk[a]['id'] = $('#no-'+a+'').val();
                if($('#no-'+a+'').is(':checked'))
                {
                    send_induk[a]['check'] = 1;
                }
                else
                    send_induk[a]['check'] = 0;
            }

			for(var a=0;a<jumlah_anak;a++)
            {
                send_anak[a] = {};
                send_anak[a]['id'] = $('#ank-'+a+'').val();
                if($('#ank-'+a+'').is(':checked'))
                {
                    send_anak[a]['check'] = 1;
                }
                else
                    send_anak[a]['check'] = 0;
            }
			console.log(send_induk)
			console.log(send_anak)
            $.ajax({
                 url: "<?= base_url() ?>index.php/role/update_user_role",
                type: 'POST',
                data: {
                    "id_user" : id,
                    "induk" : send_induk,
                    "anak" : send_anak,
                },
                success: function(results){
					 var results = JSON.parse(results);
					notif("success", results.message);
                    $('#modal_role').modal('hide');
                },
                error: function(){
                    show_toaster(2,'','Edit Role Gagal!!!');
                }
            });
      });


  function detail_menu(id){
			$("#id_tmp").val(id)
			// $(".nm_tmp").html(nama)
			$('#modal_role').modal("show")

            $.ajax({
                url: "<?= base_url() ?>index.php/role/view_role",
                type: "POST",
                data: {
                    'id' : id,
                },
                dataType: "JSON",
                success: function (data) {
					 console.log(data);
					// console.log(data.length)
						var role ="";
						var cek = "";
						var cek1 = "";
						var hasil = "";
						var total =0;
						var total1 =0;
						var no = 0;
						var nok = 0;
						var tot1 = 0;
						var tot2 = 0;
							var tmp = "";
						$(data.all).each(function (key, val) {
							$(data.induk).each(function (key, val1) {
								if(val.menu_item_name==val1.menu_item_name){
										tot1 +=1;
										cek = "checked";
										role += '<div class="checkbox col-md-12"><label><input type="checkbox" id="no-'+no+'"value="'+val.menu_item_id+'"   class="mainmenu" '+cek+'>';
										role += '<strong>'+val.menu_item_name +'</strong>';
										role += '</label></div>';
										total=1;
								}

							});
							if (total==0){
										tot1 +=1;
										cek = "";
										role += '<div class="checkbox col-md-12"><label><input type="checkbox"  id="no-'+no+'"   value="'+val.menu_item_id+'"  class="mainmenu" '+cek+'>';
										role += '<strong>'+val.menu_item_name +'</strong>';
										role += '</label></div>';
							}
										total=0;



								role += '<div class="checkbox col-md-12 submenu" style="padding-left:15px">';

									$(val.child).each(function (key, val2) {
											$(data.anak).each(function (key, val3) {
												console.log(val2.menu_item_id+" : "+val3.menu_item_id);
												if(val2.menu_item_id==val3.menu_item_id){
													tot2 +=1;
													 cek1 = "  checked";
													role += '<div class="col-md-12">';
													role += '<label><input type="checkbox"  id="ank-'+nok+'"   value="'+val2.menu_item_id+'" '+cek1+'>'+val2.menu_item_name+'</label>';
													role += '</div>';
													// console.log("hore "+ val2.menu_item_id + "\n")
													total1=1;
												}
											});
											if (total1==0){
												tot2 +=1;
												cek1 ="";
												role += '<div class="checkbox col-md-12"><label><input type="checkbox" value="'+val2.menu_item_id+'"   id="ank-'+nok+'"   class="mainmenu" '+cek1+'>';
												role += val2.menu_item_name ;
												role += '</label></div>';
											}
											total1=0;
											nok++
										});
								role += '</div>';
								no++;
						});
						jumlah_induk = tot1;
						jumlah_anak	= tot2;
						$("#tampung_role").html(role)
						console.log(role);
						// console.log(total)

                },
                error: function(){
                    // alert("Gagal");
                }
            });

  }

   $(document).on("click", ".mainmenu", function(e){
	   // alert(1)
				var checkBoxes = $(this).parents(".checkbox").next(".submenu").find("input[type=checkbox]");
                checkBoxes.prop("checked", !checkBoxes.prop("checked"));
                //console.log(checkBoxes);
            });

  $(document).on("click", "#del_role", function(e){
    e.preventDefault();
    var id = $(this).data("id");
    var ini = $(this);
    swal({
      title: 'Are you sure?',
      text: "Delete this data..",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn bg-navy',
      cancelButtonClass: 'btn bg-red',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        $.ajax({
          url: '<?= base_url() ?>' +"index.php/role/role_to_bin/"+id,
          type: 'GET',
          dataType: 'json',
        })
        .done(function(data) {
          if(data.success)
          {
            notif("success", "Data Deleted");
            ini.closest('tr').remove();
          } else {
            notif("error", "Failed to delete");
          }
        })
        .fail(function() {
          notif("error", "Failed to delete");
        })
        .always(function() {
          console.log("complete");
        });
    })

  })
</script>
