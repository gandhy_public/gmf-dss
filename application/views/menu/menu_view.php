<style media="screen">
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin: 0;
  line-height: 1;
}
</style>
<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>


<section class="content">


  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= $title ?></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body ">
      <div class="row">
        <div class="col-md-12">
          <div class="button-add-place pull-left">
            <a href="<?= base_url("index.php/menu/menu_form"); ?>" class="btn btn-flat bg-navy" name="button">
              Add Menu Item <i class="fa fa-plus"></i>
            </a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 table-responsive table-bordered">

            <table class="table table-hover table-bordered" id="menuTable">
              <thead style="background-color: #05354D; color:#fff;">
                <th class="text-center">#</th>
                <th class="text-center">Menu Name</th>
                <th class="text-center">Link</th>
                <th class="text-center">Icon</th>
				<th class="text-center">Status</th>
                <th class="text-center">Action</th>
              </thead>
              <tbody>
                <?php if (isset($center)){

                 $no = 1;
                 foreach ($center as $cen){
					$span_cen = '';
					if($cen['menu_item_isactive']==1){
						 $span_cen = '<span id="span_s" class="label label-info">AKTIF</span>';
					 }else{
						 $span_cen = '<span id="span_s" class="label label-default">NON AKTIF</span>';
					 }
                  echo '<tr >
                    <td class="text-bold text-left">&nbsp;&nbsp;&nbsp - </td>
                    <td class="text-bold">&nbsp;&nbsp;&nbsp '.$cen['menu_item_name'].'</td>
                    <td class="text-bold"><a href="'.$cen['menu_item_link'].'" class="text-info">&nbsp;&nbsp;&nbsp '.$cen['menu_item_name'].'</a></td>
                    <td class="text-bold">&nbsp;&nbsp;&nbsp <i class="'.$cen['menu_item_icon'].'"></i></td>
					<td class="text-bold text-center" id="span_sta">'.$span_cen.'</td>
                    <td class="text-bold text-center">
					  <button id = "change_status" data-id="'.SaveTextPass("GMF", $cen["menu_item_id"]).'" class="btn btn-success btn-xs"><i class="fa fa-check-circle"></i></button>
                      <a href="'.base_url("index.php/menu/menu_form/".$cen['menu_item_id']).'" class="btn btn-xs btn-info"><i class="fa fa-wrench"></i></a>
                      <button href="#" class="delete btn btn-xs btn-danger" data-id="'. $cen["menu_item_id"] .'"><i class="fa fa-trash"></i></button>
                    </td>
                  </tr>';

        foreach ($results as $data){
			$span_data = '';
			if($data['menu_item_isactive']==0){
				 $span_data = '<span id="span_s" class="label label-default">NON AKTIF</span>';
			 }else{
				 $span_data = '<span id="span_s" class="label label-info">AKTIF</span>';
			 }
          if($cen['menu_item_id']==$data['menu_item_iscenter']){
                  echo '<tr class="text-center">
                    <td  style="border-top: 0 !important; background-color: #bdb7b7; color: #fff"> - </td>
                    <td  style="border-top: 0 !important; background-color: #bdb7b7; color: #fff">'.$data['menu_item_name'].'</td>
                    <td  style="border-top: 0 !important; background-color: #bdb7b7; color: #fff"><a href="'.$data['menu_item_link'].'" class="text-info">'.$data['menu_item_name'].'</a></td>
                    <td  style="border-top: 0 !important; background-color: #bdb7b7; color: #fff"><i class="'.$data['menu_item_icon'].'"></i></td>
					<td  style="border-top: 0 !important; background-color: #bdb7b7; color: #fff" id="span_sta">'.$span_data.'</td>
                    <td  style="border-top: 0 !important; background-color: #bdb7b7; color: #fff">
					  <button id = "change_status" data-id="'.SaveTextPass("GMF", $data["menu_item_id"]).'" class="btn btn-success btn-xs"><i class="fa fa-check-circle"></i></button>
                      <a href="'.base_url("index.php/menu/menu_form/".$data['menu_item_id']).'" class="btn btn-xs btn-info"><i class="fa fa-wrench"></i></a>
                      <button href="#" class="delete btn btn-xs btn-danger" data-id="'.$data["menu_item_id"].'"><i class="fa fa-trash"></i></button>
                    </td>
                  </tr>';
                   foreach ($data['child'] as $child){
					$span_child = '';
					if($child->menu_item_isactive==0 or $data['menu_item_isactive']==0 or $cen['menu_item_isactive']==0){
						 $span_child = '<span id="span_s" class="label label-default">NON AKTIF</span>';
					 }else{
						 $span_child = '<span id="span_s" class="label label-info">AKTIF</span>';
					 }
                    echo '<tr class="text-center">
                      <td style="border-top: 0 !important; background-color: grey; color: #fff" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - </td>
                      <td style="border-top: 0 !important; background-color: grey; color: #fff" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child->menu_item_name .'</td>
                      <td style="border-top: 0 !important; background-color: grey; color: #fff" class="">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="'.base_url("index.php/".$child->menu_item_link).'" class="text-info" style="color:#fff;">
                          '.$child->menu_item_name.'
                        </a>
                     </td>
                      <td style="border-top: 0 !important; background-color: grey; color: #fff" class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="'. $child->menu_item_icon.'"></i></td>
					  <td style="border-top: 0 !important; background-color: grey; color: #fff" class="" id="span_sta">'.$span_child.'</td>
                      <td style="border-top: 0 !important; background-color: grey; color: #fff" class="">
						<button id = "change_status" data-id="'.SaveTextPass("GMF", $child->menu_item_id).'" class="btn btn-success btn-xs"><i class="fa fa-check-circle"></i></button>
                        <a href="'.base_url("index.php/menu/menu_form/".$child->menu_item_id).'" class="btn btn-xs btn-info"><i class="fa fa-wrench"></i></a>
                        <button href="#" class="delete btn btn-xs btn-danger" data-id="'. $child->menu_item_id .'"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>';
                   }
                   }
                   }
                $no++; }
                 } ?>
              </tbody>
            </table>
            <p><?php echo (isset($links) ? $links : "") ?></p>
        </div>
      </div>
    </div>
  </div>

</section>

<script>
  $(document).on("click", ".delete", function(e){
    e.preventDefault();
    id = $(this).data("id");
    var ini = $(this);
    swal({
      title: 'Are you sure?',
      text: "Delete this data..",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn bg-navy',
      cancelButtonClass: 'btn bg-red',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        $.ajax({
          url: '<?= base_url() ?>' +"index.php/menu/menu_to_bin/"+id,
          type: 'GET',
          dataType: 'json',
        })
        .done(function(data) {
          if(data.success)
          {
            notif("success", "Data Deleted");
            ini.closest('tr').remove();
          } else {
            notif("error", "Failed to delete");
          }
        })
        .fail(function() {
          notif("error", "Failed to delete");
        })
        .always(function() {
          console.log("complete");
        });
    })
  })

  $(document).on("click", "#change_status", function(e){
	 e.preventDefault();
	var id = $(this).data("id");
	var ini = $(this);
	change_status(id, ini);
  });
  function change_status(id, ini){
	var new_status = ini.closest('tr').find('span#span_s').html();
	var stat = status = '';
    if(new_status=='NON AKTIF'){
		stat = '<span id="span_s" class="label label-info">AKTIF</span>';
		status = 1;
	}else{
		stat = '<span id="span_s" class="label label-default">NON AKTIF</span>';
		status = 0;
	}
	$.ajax({
		url			: '<?= base_url()?>'+"index.php/menu/change_status_menu/"+id+"/"+status,
		type		: 'GET',
		dataType	: 'json',
		success		: function(data){
			if(data.success){
				notif("success", "Status Changed");
				ini.closest('tr').find('td#span_sta').html(stat);
			} else {
				notif("error", "Staus Unchanged");
			}
		}
	});
  }

</script>
