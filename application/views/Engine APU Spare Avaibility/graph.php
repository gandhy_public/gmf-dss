<style>
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
	display: inline-block;
	font-size: 20px;
	font-weight: bold;
	margin: 0;
	line-height: 1;
}
	.wadah {
		padding-left: 1px;
		padding-right: 0px;
	}

	.highcharts-title {
		cursor: pointer;
	}

</style>

<section class="content-header">
	<h1>
		<?= $title ?>
		<small><?= $small_tittle ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
		<?php foreach ($breadcrumb as $data): ?>
			<li><?= $data ?></li>
		<?php endforeach; ?>
	</ol>
</section>

<section class="content">
	<div class="">
		<div class="box-body">
			<div class="row">

				<div id="tes" class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<section class="content">
						<div class="box">
							<div class="box-body">
								<div class="row">
									<div class="col-lg-5">
										<div class="col-sm-4" style="margin-top: 8px;">Material Number</div>
										<div class="col-sm-8">
											<input type="text" class="form-control pull-right" id="search">

										</div>
									</div>

									<div class="col-lg-4">
										<button type="button" class="btn btn-flat bg-navy"><i class="fa fa-search"></i>
											search
										</button>
									</div>
								</div>
								<br>
								<div class="box">
									<div class="box-header with-border">
										<h3 class="box-title">Availability</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse"
													data-toggle="tooltip"
													title="Collapse">
												<i class="fa fa-minus"></i></button>
										</div>
									</div>
									<div class="box-body" id="box-body">
										<div class="row">
											<div class="col-lg-2">
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="small-box bg-navy">
														<div class="inner center">
															<center><br>
																<h3 class="underlineclasss" style="color:#F2C573;cursor:pointer;"
																	href="#tabel_detail" id="panel_garuda"
																	title="Show Detail">Garuda</h3>
																<hr>
																<h2>10 Item</h2>
																<p style="color:#F2C573">Available</p>
																<br>
																<h2>20 Item</h2>
																<p style="color:#F2C573">Not Available</p>
																<br>
															</center>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="small-box bg-navy">
														<div class="inner center">
															<center><br>
																<h3 class="underlineclasss" style="color:#F2C573;cursor:pointer;"
																	id="panel_citilink" title="Show Detail">
																	Citilink</h3>
																<hr>
																<h2>23 Item</h2>
																<p style="color:#F2C573">Available</p>
																<br>
																<h2>16 Item</h2>
																<p style="color:#F2C573">Not Available</p>
																<br>
															</center>
														</div>
													</div>
												</div>
											</div>

											<div class="col-lg-2">
											</div>

										</div>
									</div>
								</div>

								<div class="box tabel_detail" id="tabel_detail" name="tabel_detail"
									 style="display:none;">
									<div class="box-header with-border">
										<h3 class="box-title">Group Name</h3>
										<div class="box-tools pull-right">

											<button type="button" class="btn btn-primary btn-flat" id="back"><i
													class="fa fa-arrow-left"></i> Back
											</button>
										</div>
									</div>
									<div class="box-body">
										<div class="row">

											<div class="col-lg-12">
												<div class="form-group">
													<div class="row">
														<div class="col-md-12">
															<div class="box box-primary box-solid">
																<div class="box-header">
																	<h4 class="box-title-detail"></h4>
																</div>
																<table id="example1"
																	   class="table table-bordered table-striped dataTable"
																	   role="grid" aria-describedby="example1_info">
																	<thead>
																	<tr>
																		<th class="center">Mat. Number</th>
																		<th class="center">Mat. Description</th>
																		<th class="center">Plan</th>
																		<th class="center">Location</th>
																		<th class="center">Sloc</th>
																		<th class="center">Sloc Description</th>
																	</tr>
																	</thead>
																	<tbody>
																	<tr>
																		<td>10020111</td>
																		<td>Bearing</td>
																		<td>W01</td>
																		<td>GAEM</td>
																		<td>1000</td>
																		<td>Ready</td>
																	</tr>
																	<tr>
																		<td>10020112</td>
																		<td>Turbin</td>
																		<td>W03</td>
																		<td>GAH</td>
																		<td>1000</td>
																		<td>Ready</td>
																	</tr>
																	<tr>
																		<td>10020113</td>
																		<td>Turbo</td>
																		<td>W02</td>
																		<td>GAH</td>
																		<td>3000</td>
																		<td>Unserviceable</td>
																	</tr>
																	<tr>
																		<td>10020114</td>
																		<td>jet</td>
																		<td>W01</td>
																		<td>GAEM</td>
																		<td>4100</td>
																		<td>Ready 1</td>
																	</tr>
																	<tr>
																		<td>10020115</td>
																		<td>Bearing</td>
																		<td>W01</td>
																		<td>GAH</td>
																		<td>1000</td>
																		<td>Ready</td>
																	</tr>
																	<tr>
																		<td>10020116</td>
																		<td>Turbine</td>
																		<td>W01</td>
																		<td>GAEM</td>
																		<td>4200</td>
																		<td>Ready 2</td>
																	</tr>
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>


										</div>
									</div>
								</div>


							</div>
						</div>

					</section>
				</div>

			</div>
		</div>
	</div>
</section>


<script>

	var month = new Array();
	month[0] = "Januari";
	month[1] = "Februari";
	month[2] = "Maret";
	month[3] = "April";
	month[4] = "Mei";
	month[5] = "Juni";
	month[6] = "Juli";
	month[7] = "Agustus";
	month[8] = "September";
	month[9] = "Oktober";
	month[10] = "November";
	month[11] = "Desember";
	var today = new Date();
	var tanggal = today.getUTCDate();
	var bulan = month[today.getUTCMonth()];
	var tahun = today.getUTCFullYear();

	//Date range picker
	$('#date_end, #date_start').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true
	});
	var body = $("html, body");

	$(document).ready(function () {
		$(document).on("click", "#panel_garuda", function (e) {
			$(".box-title-detail").html("Garuda");
			$(".tabel_detail").show();
			body.stop().animate({scrollTop:3000}, 1000);
		});
		$(document).on("click", "#panel_citilink", function (e) {
			$(".box-title-detail").html("Citilink");
			$(".tabel_detail").show();
			body.stop().animate({scrollTop:3000}, 1000);
		});
		$(document).on("click", "#back", function (e) {
			$(".tabel_detail").hide();
			body.stop().animate({scrollTop:0}, 500);
		});
	});
</script>
=======
<style>
.wadah{
  padding-left: 1px;
  padding-right: 0px;
}
.highcharts-title{
  cursor: pointer;
}
input.empty {
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
}
.spinner {
  margin: 100px auto;
  width: 50px;
  height: 40px;
  text-align: center;
  font-size: 10px;
}

.spinner > div {
  background-color: #333;
  height: 100%;
  width: 6px;
  display: inline-block;

  -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
  animation: sk-stretchdelay 1.2s infinite ease-in-out;
}

.spinner .rect2 {
  -webkit-animation-delay: -1.1s;
  animation-delay: -1.1s;
}

.spinner .rect3 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

.spinner .rect4 {
  -webkit-animation-delay: -0.9s;
  animation-delay: -0.9s;
}

.spinner .rect5 {
  -webkit-animation-delay: -0.8s;
  animation-delay: -0.8s;
}

@-webkit-keyframes sk-stretchdelay {
  0%, 40%, 100% { -webkit-transform: scaleY(0.4) }
  20% { -webkit-transform: scaleY(1.0) }
}

@keyframes sk-stretchdelay {
  0%, 40%, 100% {
    transform: scaleY(0.4);
    -webkit-transform: scaleY(0.4);
    }  20% {
      transform: scaleY(1.0);
      -webkit-transform: scaleY(1.0);
    }
  }
  .stripe {
    white-space: nowrap;
    background-color: white;
  }
</style>

<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>

<section class="content">
  <div class="">
    <div class="box-body">
      <div class="row">
        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <section class="content">
            <div class="box" style="padding-left: 10px; padding-right: 10px">
              <div class="box-header with-border">
                <h3 class="box-title"><?= $title ?></h3>
                <div class="box-tools pull-right">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="period_start" class="col-sm-4 control-label" style="margin-top: 5px;">Material Number</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control empty" id="iconified" placeholder="&#xF002; Materia Number" name="" value="" >
                        </div>
                        <div class="col-sm-2">
                          <button type="button"class="btn btn-flat bg-navy " id="add_category" name="add_category"> <i class="fa fa-search"></i> Search</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="box" style="padding-left: 10px; padding-right: 10px">
              <div class="box-header with-border">
                <h3 class="box-title" style="padding-bottom: 10px; padding-top: 10px;">Availability</h3>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    YTD : <?php  echo date('d-M-Y');?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-8">
                      <div class="form-group">
                        <div class="small-box bg-navy">
                          <div class="inner center">
                            <center><br>
                              <h3 style="color:#F2C573;cursor:pointer;"
                                href="#tabel_detail" id="panel_garuda"
                                title="Show Detail">Garuda</h3>
                              <hr>
                              <div class="row">
                                <div class="col-lg-4">
                                  <h2 style="cursor:pointer;" class="md" data-id="engine_grd">Engine</h2>
                                </div>
                                <div class="col-lg-4">
                                  <h2 id="engine_avaibility_grd">10 Item</h2>
                                  <p style="color:#F2C573">Available</p>
                                </div>
                                <div class="col-lg-4">
                                  <h2 id="engine_navaibility_grd">10 Item</h2>
                                  <p style="color:#F2C573">Not Available</p>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-lg-4">
                                    <h2 style="cursor:pointer;" class="md" data-id="apu_grd">APU</h2>
                                </div>
                                <div class="col-lg-4">
                                  <h2 id="apu_avaibility_grd">10 Item</h2>
                                  <p style="color:#F2C573">Available</p>
                                </div>
                                <div class="col-lg-4">
                                  <h2 id="apu_navaibility_grd">10 Item</h2>
                                  <p style="color:#F2C573">Not Available</p>
                                </div>
                              </div>
                            </center>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-8">
                      <div class="form-group">
                        <div class="small-box bg-navy">
                          <div class="inner center">
                            <center><br>
                              <h3 style="color:#F2C573;cursor:pointer;"
                                href="#tabel_detail" id="panel_garuda"
                                title="Show Detail">Citilink</h3>
                              <hr>
                              <div class="row">
                                <div class="col-md-4">
                                  <h2 style="cursor:pointer;"  class="md" data-id="engine_citilink">Engine</h2>
                                </div>
                                <div class="col-md-4">
                                  <h2 id="engine_avaibility_ct">10 Item</h2>
                                  <p style="color:#F2C573">Available</p>
                                </div>
                                <div class="col-md-4">
                                  <h2 id="engine_navaibility_ct">10 Item</h2>
                                  <p style="color:#F2C573">Not Available</p>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-lg-4">
                                    <h2 style="cursor:pointer;" class="md" data-id="apu_citilink">APU</h2>
                                </div>
                                <div class="col-lg-4">
                                  <h2 id="apu_avaibility_ct">10 Item</h2>
                                  <p style="color:#F2C573">Available</p>
                                </div>
                                <div class="col-lg-4">
                                  <h2 id="apu_navaibility_ct">10 Item</h2>
                                  <p style="color:#F2C573">Not Available</p>
                                </div>
                              </div>
                            </center>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                    </div>
                  </div>
                </div>
              </div>
            </div>


            <!-- Start Detail Avaibility -->
            <div class="box" style="padding-left: 10px; padding-right: 10px" id='detail'>
              <div class="box-header with-border">
                <h3 class="box-title" style="padding-bottom: 10px; padding-top: 10px;">Detail Availability</h3>
              </div>
              <div class="body">
                <div id="loadingApprove" class="back-loading" style="display: none;background: white;position: absolute;z-index: 9999;width: 99%;height: 100%;margin: 0 auto;left: 1;/* display: none; */left: 0;right: 0;/* top: 10px; */bottom: 0;/* padding:  50%; */padding top:0px;/* padding-top:  50px; */">
                  <p style="text-align:  center;margin-top:  50px;font-size: 20px;">Data Diproses</p>
                  <div class="spinner">
                    <div class="rect1"></div>
                    <div class="rect2"></div>
                    <div class="rect3"></div>
                    <div class="rect4"></div>
                    <div class="rect5"></div>
                  </div>
                </div>
                <div class="row">
                  <center><br>
                    <h2 style="cursor:pointer;"
                      href="#tabel_detail" id="panel_garuda"
                      title="Show Detail">Citilink</h2>
                  </center>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <center>
                            <h3>Available</h3>
                        </center>
                        <table class="table table-bordered">
                          <thead>
                          <tr>
                            <th>Mat Number</th>
                            <th>Mat Description</th>
                            <th>Plan</th>
                            <th>Location</th>
                            <th>Sloc</th>
                            <th>Sloc Description</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>

                          </tr>
                        </tbody>
                      </table>
                      </div>
                      <div class="col-md-6">
                        <center>
                            <h3>Not Available</h3>
                        </center>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th >Mat Number</th>
                              <th>Mat Description</th>
                              <th>Plan</th>
                              <th>Location</th>
                              <th>Sloc</th>
                              <th>Sloc Description</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- END Detail Avaibility -->
          </section>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
  $('#iconified').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
      input.addClass('empty');
    } else {
      input.removeClass('empty');
    }
  });
  $("#detail").hide();
  var value=10;
  $("#avaibility_grd").text(value+' item');
  // Get data first open pages
  $.ajax({
    url: '<?= base_url() ?>index.php/Engine_apu_spare/landing',
    type: 'POST'
  })
  .done(function(resp) {
    var data=JSON.parse(resp);
    $("#engine_avaibility_grd").text(data.grd_engine_av+ ' Item');
    $("#engine_navaibility_grd").text(data.grd_engine_nav+ ' Item');
    $("#apu_avaibility_grd").text(data.grd_apu_av+ ' Item');
    $("#apu_navaibility_grd").text(data.grd_apu_nav+ ' Item');
    $("#engine_avaibility_ct").text(data.ct_engine_av+ ' Item');
    $("#engine_navaibility_ct").text(data.ct_engine_nav+ ' Item');
    $("#apu_avaibility_ct").text(data.ct_apu_av+ ' Item');
    $("#apu_navaibility_ct").text(data.ct_apu_nav+ ' Item');
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });

  $(".md").click(function(event) {
    /* Act on the event */
    var id=$(this).data("id");
    alert(id); 
    $("#detail").show('slow/400/fast', function() {
      $.ajax({
        url: '<?= base_url() ?>index.php/Engine_apu_spare/store',
        type: 'POST',
        dataType: 'JSON',
        data: {param1: id},
        beforeSend: function(){
          $("#loadingApprove").show();
          console.log("loading");
        }
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
          $("#loadingApprove").hide();
      });

    });
  });
});
</script>
