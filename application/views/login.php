<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log in || GMF - CORPORATE DASHBOARD</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/animate/animate.min.css">
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/parsley/parsley.css">
  <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/gmf.css">
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/toastr/build/toastr.min.css">
  <style media="screen">
  .btn-primary.disabled.focus, .btn-primary.disabled:focus, .btn-primary.disabled:hover, .btn-primary[disabled].focus, .btn-primary[disabled]:focus, .btn-primary[disabled]:hover, fieldset[disabled] .btn-primary.focus, fieldset[disabled] .btn-primary:focus, fieldset[disabled] .btn-primary:hover {
    background-color: #fff;
    border-color: #fff;
    color: #111;
  }
  .btn-primary:active, .btn-primary:hover, .btn-primary:focus{
    background-color: #fff;
    border-color: #fff;
    color: #111;
  }
  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box wow bounceInUp" data-wow-duration="2000ms">
  <div class="login-box-body" >
    <div class="login-logo">
      <img src="<?= base_url() ?>assets/dist/img/icon/logo.png" class="logo-image-login" />
      <a href="<?= base_url(); ?>assets/index2.html">
          Corporate Dashboard
      </a>
    </div>

    <form method="post"  id="form">
      <p class="login-box-msg">Sign in to start your session</p>
      <div class="form-group has-feedback">
        <input type="text" id="email" class="form-control" placeholder="Username" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" id="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <a href="#" id="link_reg" style="font-size: 12px;">click here to register</a>
      <div class="row">
        <div class="col-xs-4 pull-right">
          <button type="submit" id="login_user" class="btn btn-primary btn-block btn-flat gmf-login">Sign In &nbsp; <i class="fa fa-arrow-circle-o-right"></i></button>
        </div>
      </div>
    </form>

    <form method="post"  id="form_register" style="display:none;">
      <p class="login-box-msg">Form Register</p>
      <div class="form-group has-feedback">
        <input type="text" id="username_register" name="username_register" class="form-control" placeholder="Username" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <select class="form-control" id="role_register" name="role_register" required>
          <option value="">Chose Role</option>
          <?php foreach ($roles as $role ): ?>
            <option value="<?= $role->role_id ?>"><?= $role->role_name ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="form-group has-feedback">
        <textarea name="desc_register" class="form-control" id="desc_register" rows="8" cols="80" required></textarea>
      </div>
      <a href="#" id="link_login" style="font-size: 12px;">click here to login</a>
      <div class="row">
        <div class="col-xs-5 pull-right">
          <button type="submit" id="register_user" class="btn btn-primary btn-block btn-flat gmf-login">Register &nbsp; <i class="fa fa-arrow-circle-o-right"></i></button>
        </div>
      </div>
    </form>

  </div>
</div>

<script src="<?= base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/wow/wow.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/session/jquerysession.js"></script>
<script src="<?= base_url(); ?>assets/plugins/toastr/build/toastr.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/parsley/parsley.min.js"></script>
<script>


$("#link_reg").on("click", (e) => {
  e.preventDefault();
  $("#form").fadeOut(1000, function() {
    $("#form_register").fadeIn(1000);
  });
})
$("#link_login").on("click", (e) => {
  e.preventDefault();
  $("#form_register").fadeOut(1000, function() {
    $("#form").fadeIn(1000);
  });
})

$("#form_register").parsley();
$("#form_register").on("submit", (e) => {
  e.preventDefault();
  var username = $("#username_register").val();
  var role_id = $("#role_register").val();
  var description = $("#desc_register").val();
  $.ajax({
    url: '<?= base_url() ?>index.php/login/register',
    type: 'POST',
    dataType: 'JSON',
    data: {username: username, role_id: role_id, description: description},
    beforeSend: function(){
      $("#link_login").prop('disabled', true);
      $("#register_user").prop('disabled', true).html("Wait.. &nbsp; <i class='fa fa-refresh fa-spin'></i>");
    }
  })
  .done(function(data) {
    if(data.success){
      notif("success", data.message);
      $("#username_register, #role_register, #desc_register").val("");
    } else {
      notif("error", "Error not detected");
    }
  })
  .fail(function(data) {
    if(data.responseJSON.code == "1"){
      notif("warning", data.responseJSON.message);
    } else if (data.responseJSON.code == "2"){
      notif("error", data.responseJSON.message);
    } else {
      notif("error", "Error not detected");
    }
  })
  .always(function(data) {
    $("#link_login").prop('disabled', false);
    $("#register_user").prop('disabled', false).html("Register &nbsp; <i class='fa fa-arrow-circle-o-right'></i>");
  });

})

$(document).ready(function () {
	 var base_url = '<?php echo base_url(); ?>index.php/';
   $('#form').parsley();
	  $('#form').on("submit", function(e) {
      e.preventDefault();
			var user = $('#email').val();
			var pass = $('#password').val();
			$('#login_user').text('Proses..');
				$.ajax({
					url: base_url+ 'login/masuk',
					method: 'post',
					dataType: 'json',
					data: {
						'username': user,
						'password': pass
					},
					success: function (data) {
						if(data.status == "1"){
							 localStorage.setItem("user_id", data.sess.user_id);
							 localStorage.setItem("username", data.sess.username);
							 localStorage.setItem("role", data.sess.role);
							 localStorage.setItem("role_id", data.sess.roleid);
							 localStorage.setItem("status", data.sess.status);
							 localStorage.setItem("new_role_id", data.sess.new_role_id);
							 localStorage.setItem("new_role", data.sess.new_role);
							 notif("success", "Login success, redirect to home");
							 window.location.href = base_url + 'layouts';
						}else {
							// alert(data.status)
							var user = $('#email').val('');
							var pass = $('#password').val('');
							$('#login_user').attr('disabled', false);
							$('#login_user').text('Sign In');
							notif("error", "Username or password is wrong");
						}
					}
				});
		});
	});

	  function notif(type, msg)
    {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      };
      switch (type) {
        case "success":
          toastr.success(msg);
        break;
        case "error":
            toastr.error(msg);
        break;
        case "warning":
            toastr.warning(msg);
        break;
        default:
      }
    }

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });

    wow = new WOW(
    {
      animateClass: 'animated',
      offset:       100,
      callback:     function(box) {
        null;
      }
    }
  );
  wow.init();

  });
</script>
</body>
</html>
