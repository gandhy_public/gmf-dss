<style>
    .box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        /*      font-weight: bold;
        */
        margin: 0;
        line-height: 1;
    }
    .red {
        color: #F24738 !important;
    }

    .black {
        color: black !important;
    }

    .pagination > .active > a,
    .pagination > .active > a:focus,
    .pagination > .active > a:hover,
    .pagination > .active > span,
    .pagination > .active > span:focus,
    .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0e1f2e;
    }

    .modal-header {
        background-color: #0e1f2e;
        color: #fff;
    }
</style>
<section class="content-header">
    <h1 style="font-weight: bold">
        <?= $title ?>
    </h1>
</section>

<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-edit"></i> Target</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary fa fa-plus pull-right" onclick="addTargetMonth_new()"></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- <table id="tabeltarget" class="table table-hover table-bordered table-gmf">
                        <thead style="background-color: #05354D">
                        <th class="text-center">No</th>
                        <th class="text-center">A/C Type</th>
                        <th class="text-center">Target</th>
                        <th class="text-center">Action</th>
                        </thead>
                        <tbody class="text-center" id="show_data"></tbody>
                    </table> -->
                    <table id="tabeltarget" class="table table-hover table-bordered table-gmf">
                        <thead style="background-color: #05354D">
                        <th class="text-center">No</th>
                        <th class="text-center">A/C Type</th>
                        <th class="text-center">Target</th>
                        <th class="text-center">Action</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-edit"></i> Mitigation </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Start Period</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control monthPickerDispatch" placeholder="Start Date"
                                   id="month_from" value="<?php echo $month_from ?>">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>End Period</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control monthPickerDispatch" placeholder="End Date"
                                   id="month_to" value="<?php echo $month_to ?>">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <button class="btn btn-flat bg-navy" id="search" style="margin-top: 24px;">
                            <i class="fa fa-search"></i> Search
                        </button>
                    </div>
                </div>
                <div class="col-md-12">
                    <table id="tabelmitigation" class="table table-hover table-bordered table-gmf">
                        <thead style="background-color: #05354D">
                        <th class="text-center">No</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Range Type</th>
                        <th class="text-center">A/C Type</th>
                        <th class="text-center">A/C Reg</th>
                        <th class="text-center">Target</th>
                        <th class="text-center">Actual</th>
                        <th class="text-center">Action</th>
                        </thead>
                        <tbody class="text-center" id="show_data"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

    $(document).on("click", "#search", function (e) {
        get_mitigations();
        //get_targets();
    });

    $(document).ready(function () {
        get_mitigations();   //pemanggilan fungsi tampil mitigasi.
        //get_targets();

    });

    //table target
    function get_targets() {
        $("#tabeltarget").DataTable({
            ordering: true,
            destroy: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: {
                url: "<?= base_url() ?>index.php/Master_dispatch_reability/data_targets",
                type: 'POST'
            },
            "aoColumns": [
                {
                    "mData": "0",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "1",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "2",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "3",
                    "mRender": function (data, type, row) {
                        var button_del = '<button type="button" data-id="' + data + '" class="btn btn-danger btn-flat" id="delete_target" name="button"><i class="fa fa-trash-o"></i></button>';
                        var button_edit = '<a  class="btn btn-success btn-flat"  onclick="editTarget(\'' + data + '\')"><i class="fa fa-edit"></i></a>';
                        return "<center> " + button_edit + " " + button_del + "</center>";
                    }
                }
            ]
        });
    }

    //fungsi tampil mitigation
    function get_mitigations() {
        var month_from = $("#month_from").val();
        var month_to = $("#month_to").val();

        $("#tabelmitigation").DataTable({
            ordering: true,
            destroy: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: {
                url: "<?= base_url() ?>index.php/Master_dispatch_reability/data_mitigations",
                data: {
                    'month_from': month_from,
                    'month_to': month_to
                },
                type: 'POST'
            },
            "rowCallback": function (row, data, index) {
                $node = this.api().row(row).nodes().to$();
                if (data[8]) {
                    $node.addClass('red')
                } else {
                    $node.addClass('black')
                }
            },
            "aoColumns": [
                {
                    "mData": "0",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "2",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "3",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "4",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "5",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "6",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "7",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "1",
                    "mRender": function (data, type, row) {
                        var comment_mitigation = '<a  class="btn btn-success btn-flat"  onclick="addComment(\'' + data + '\')"><i class="fa fa-edit"></i></a>';
                        return "<center>" + comment_mitigation + "</center>";
                    }
                }
            ]
        });
    }

    $(document).on("click", "#delete_target", function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var ini = $(this);
        swal({
            title: 'Are you sure?',
            text: "Delete this data..",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn bg-navy',
            cancelButtonClass: 'btn bg-red',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            $.ajax({
                url: '<?= base_url() ?>' + "index.php/Master_dispatch_reability/delete_target/" + id,
                type: 'GET',
                dataType: 'json',
            })
                .done(function (data) {
                    if (data.success) {
                        notif("success", "Data Deleted");
                        ini.closest('tr').remove();
                    } else {
                        notif("error", "Failed to delete");
                    }
                })
                .fail(function () {
                    notif("error", "Failed to delete");
                })
                .always(function () {
                    console.log("complete");
                });
        })
    });

    function editTarget(id) {
        $("#id").val(id);
        $.ajax({
            type: "POST",
            url: "<?= base_url() ?>index.php/Master_dispatch_reability/editTarget",
            dataType: "JSON",
            data: {
                'id': id
            },
            success: function (data) {
                $.each(data, function () {
                    $('#edit_target').modal('show');
                    $('[name="dr_target_val"]').val(data.dr_target_val);
                    $('[name="aircraft_type"]').val(data.dr_target_type);
                });
            }
        });
        return false;
    }

    //GET comment
    function addComment(dr_id) {
        $("#dr_id").val(dr_id);
        $.ajax({
            type: "GET",
            url: "<?= base_url() ?>index.php/Master_dispatch_reability/getCommentMitigation",
            dataType: "JSON",
            data: {'dr_id': dr_id},
            success: function (data) {
                if (data.mitigasi_why != null) {
                    $.each(data, function () {
                        $('#comment_mitigation').modal('show');
                        $('[name="dr_id"]').val(data.dr_id);
                        $('[name="mitigasi_why"]').val(data.mitigasi_why);
                        $('[name="mitigasi_solution"]').val(data.mitigasi_solution);
                    });
                } else {
                    $('#add_mitigation').modal('show');
                    $('[name="dr_id"]').val(data.dr_id);
                    $('[name="mitigasi_why"]').val(data.mitigasi_why);
                    $('[name="mitigasi_solution"]').val(data.mitigasi_solution);
                }
            }
        });
        return false;
    }

    function addTargetMonth() {
        $('#modal_add_target_month').modal('show');
    }

    $('.monthPickerDispatch').datepicker({
        todayBtn: "linked",
        format: 'mm-yyyy',
        viewMode: 'months',
        minViewMode: 'months',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    //TAMBAHAN DIMAS
    var save_method;
    var table;
    $(document).ready(function () {
       table = $('#tabeltarget').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('index.php/Master_dispatch_reability/list_target') ?>",
                "type": "POST"
            },
            "columnDefs": [
                {
                    "targets": [-1],
                    "orderable": false,
                },
            ],
        });
        $("input").change(function () {
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });

    function reload_table(){
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    function addTargetMonth_new(){
        save_method = 'add';
        $('#form-target')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_add_target_month').modal('show');
    }

    function edit_target(id){

        save_method = 'update';
        $('#form-target')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('index.php/Master_dispatch_reability/editTarget_new') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function (data){

                $('[name="id"]').val(data.id);
                $('[name="aircraft_type"]').val(data.dr_target_type);
                $('[name="target"]').val(data.dr_target_val);
                $('#modal_add_target_month').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error');
            }
        });
    }

    function save_target() {
        $('#saveTarget').text('Processing...');
        $('#saveTarget').attr('disabled', true);
        var url;

        if (save_method == 'add') {
            url = "<?php echo site_url('index.php/Master_dispatch_reability/addTarget_new') ?>";
        } else {
            url = "<?php echo site_url('index.php/Master_dispatch_reability/updateTarget_new') ?>";
        }

        if($('[name="target"]').val() <= 100){
            var formData = new FormData($('#form-target')[0]);
            $.ajax({
                url: url,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function (data){
                    if (data.status){
                        notif('success', 'Data successfully saved');
                        $('#modal_add_target_month').modal('hide');
                        reload_table();
                    } else {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                        }
                    }
                    $('#saveTarget').html('<i class="fa fa-save"></i> Save');
                    $('#saveTarget').attr('disabled', false);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notif('error', 'Connection disorders occurred, Please try again');
                    $('#saveTarget').html('<i class="fa fa-save"></i> Save');
                    $('#saveTarget').attr('disabled', false);
                }
            });
        }else{
            notif('error', 'Maximum target of 100');
            $('#saveTarget').html('<i class="fa fa-save"></i> Save');
            $('#saveTarget').attr('disabled', false);
        }

        


    }

    function delete_target(id){
        if (confirm('Are you sure delete this data?')){
            $.ajax({
                url: "<?php echo site_url('index.php/Master_dispatch_reability/delete_target_new') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data){
                    notif('success', 'Data successfully deleted');
                    $('#modal_add_target_month').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notif('error', 'Connection disorders occurred, Please try again');
                }
            });

        }
    }
</script>

<div class="modal fade" id="modal_add_target_month">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <h4 class="modal-title">Input Target</h4>
            </div>
            <div class="modal-body">
                <form id="form-target">
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <select class="form-control" name="aircraft_type">
                            <option selected disabled>Select A/C Type</option>
                            <option value="garuda">Garuda</option>
                            <option value="citilink">Citilink</option>
                            <?php foreach ($acTypes as $acType): ?>
                                <option value="<?php echo $acType['actype'] ?>"><?php echo $acType['actype'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Target</label>
                        <input type="number" step=".01" class="form-control" placeholder="Target..." name="target" required>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i
                            class="fa fa-rotate-left"></i> Cancel
                </button>
                <button type="button" class="pull-right btn btn-flat bg-navy" id="saveTarget" onclick="save_target()"><i class="fa fa-save"></i>
                    Save
                </button>
            </div>

        </div>
    </div>
</div>
<!-- <div class="modal fade" id="edit_target">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <h4 class="modal-title">Edit Target</h4>
            </div>
            <form>
                <div class="modal-body">
                    <input type="text" id="id" name="id" hidden>
                    <div class="form-group">
                        <select class="form-control" name="aircraft_type">
                            <option selected disabled>Select A/C Type</option>
                            <option value="garuda">Garuda</option>
                            <option value="citilink">Citilink</option>
                            <?php foreach ($acTypes as $acType): ?>
                                <option value="<?php echo $acType['actype'] ?>"><?php echo $acType['actype'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Target</label>
                        <input type="number" step=".01" class="form-control" name="dr_target_val"
                               required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close <i
                                class="fa fa-rotate-left"></i></button>
                    <button type="submit" class="btn btn-success pull-right" id="saveTarget"><i class="fa fa-edit"></i>
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div> -->
<div class="modal fade" id="comment_mitigation">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <h4 class="modal-title">Mitigation</h4>
            </div>
            <form action="<?php echo base_url() . 'index.php/Master_dispatch_reability/updateCommentMitigation'; ?>"
                  method="post">
                <div class="modal-body">
                    <input type="text" id="dr_id" name="dr_id" hidden>
                    <div class="form-group">
                        <label>Why?</label>
                        <input type="text" class="form-control" placeholder="Why..." id="why" name="mitigasi_why"
                               required>
                    </div>
                    <div class="form-group">
                        <label>Solution</label>
                        <input type="text" class="form-control" placeholder="solution..." id="solution"
                               name="mitigasi_solution" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close <i
                                class="fa fa-rotate-left"></i></button>
                    <button type="submit" class="btn btn-success pull-right" id="saveComment"><i class="fa fa-edit"></i>
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="add_mitigation">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <h4 class="modal-title">Mitigation</h4>
            </div>
            <form action="<?php echo base_url() . 'index.php/Master_dispatch_reability/addCommentMitigation'; ?>"
                  method="post">
                <div class="modal-body">
                    <input type="text" id="dr_id" name="dr_id" hidden>
                    <div class="form-group">
                        <label>Why?</label>
                        <input type="text" class="form-control" placeholder="Why..." id="why" name="mitigasi_why"
                               required>
                    </div>
                    <div class="form-group">
                        <label>Solution</label>
                        <input type="text" class="form-control" placeholder="solution..." id="solution"
                               name="mitigasi_solution" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close <i
                                class="fa fa-rotate-left"></i></button>
                    <button type="submit" class="btn btn-success pull-right" id="saveComment"><i class="fa fa-save"></i>
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>