<script src="<?= base_url() ?>assets/plugins/rounded-corner/rounded-corner.js"></script>
<style>
    .box-header > .fa,
    .box-header > .glyphicon,
    .box-header > .ion,
    .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }

    .center {
        text-align: center;
    }

    .box-header > .fa,
    .box-header > .glyphicon,
    .box-header > .ion,
    .box-header .box-title {
        font-weight: bold;
    }

    .pagination > .active > a,
    .pagination > .active > a:focus,
    .pagination > .active > a:hover,
    .pagination > .active > span,
    .pagination > .active > span:focus,
    .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0e1f2e;
    }
</style>
<script src="https://code.highcharts.com/js/highcharts.js"></script>
<script src="https://code.highcharts.com/js/modules/exporting.js"></script>
<!-- <section class="content-header">
    <h1>
        <?= $title ?>
        <small><?= $small_tittle ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <?php foreach ($breadcrumb as $data): ?>
            <li><?= $data ?></li>
        <?php endforeach; ?>
    </ol>
</section> -->
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="isi">
                <div class="overlay-box"
                     style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                    <div style="" class="loading" id="loader"></div>
                </div>
                <div class="row">
                    <form id="filterDispatch" action="index.html" method="post">
                        <div class="col-md-2">
                            <h4 class="box-title"><i class="fa fa-filter"></i> Filter Dispatch</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="hidden" id="_actype" name="_actype" value="">
                                        <select class="form-control select2" style="width: 100%;" id="actype">
                                            <option value="GA">1. GARUDA</option>
                                            <?php $no = 1;
                                            foreach ($acTypeGa as $data): ?>
                                                <option value="<?= $data->actype ?>">&nbsp; &nbsp;
                                                    <?= "1." . $no . ". " . $data->actype ?>
                                                </option>
                                                <?php $no++; endforeach; ?>
                                            <option value="CITILINK">2. CITILINK</option>
                                            <?php $noB = 1;
                                            foreach ($acTypeQg as $data): ?>
                                                <option value="<?= $data->actype ?>">&nbsp; &nbsp;
                                                    <?= "2. " . $noB . ". " . $data->actype ?>
                                                </option>
                                                <?php $noB++; endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="hidden" id="_acreg" name="_acreg" value="">
                                        <select class="form-control select2" style="width: 100%;" multiple="multiple"
                                                id="acReg">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control" style="width: 100%;" id="range_type">
                                            <option value="monthly" selected="">Monthly</option>
                                            <option value="weekly">Weekly</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="month">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control yearpicker" id="date2" name="date2"
                                                   value="<?= $endDateMonthly ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control yearpicker" id="date1" name="date1"
                                                   value="<?= $startDateMonthly ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-flat bg-navy"
                                                    style="margin-top: 4px;">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="display:none;" id="week">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-group ">
                                                <div class="col-md-7" style="padding-right: 0px;padding-left: 0px;">
                                                    <input type="text" placeholder="Start Date"
                                                           value="<?= $startDateMonthly ?>" class="form-control"
                                                           id="year1" name="year1" readonly>
                                                </div>
                                                <div class="col-md-5" style="padding-right: 0px;padding-left: 0px;">
                                                    <select class="form-control" id="week1" id="week1">
                                                        <?php
                                                        $range = range(1, 52);
                                                        foreach ($range as $week) {
                                                            echo "<option value='" . $week . "'>" . $week . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-group ">
                                                <div class="col-md-7" style="padding-right: 0px;padding-left: 0px;">
                                                    <input type="text" placeholder="Start Date"
                                                           value="<?= $endDateMonthly ?>" class="form-control"
                                                           id="year1" name="year1" readonly>
                                                </div>
                                                <div class="col-md-5" style="padding-right: 0px;padding-left: 0px;">
                                                    <select class="form-control" id="week2" id="week2">
                                                        <option value="<?= $currentWeek ?>">
                                                            <?= $currentWeek ?>
                                                        </option>
                                                        <?php
                                                        $range = range(1, 52);
                                                        foreach ($range as $week) {
                                                            $selected = "";
                                                            echo "<option value='" . $week . "' " . $selected . ">" . $week . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label></label>
                                            <button type="submit" class="btn btn-block btn-flat bg-navy"
                                                    style="margin-top: 4px;">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-10">
                        <div id="grafik1" style="height: 450px;width: 100%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade in" id="modal-poin" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document" style="margin-right: 750px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: #fff;">×</span>
                </button>
                <p style="text-align:center; font-size:10px; margin-bottom: -5px;" id="titleDetail"></p>
                <h4 class="modal-title-panel" id="title-panel" style="text-align: center;font-weight: bold">
                    Aircraft Type
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade in" id="modal-detail-table" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin-right: 100px;margin-top: 30px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: #fff;">×</span>
                </button>
                <h4 class="modal-title-panel" id="tittle-detail-table"
                    style="text-align: center;font-weight: bold"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table style="width:100%;"
                                           class="table table-hover table-bordered dataTable no-footer"
                                           id="detailAgain">
                                        <thead style="background-color: #05354D; color:#fff;">
                                        <tr>
                                            <th class="center">No</th>
                                            <th class="center">Date</th>
                                            <th class="center">Aircraft Reg</th>
                                            <th class="center">Cancel</th>
                                            <th class="center">Delay</th>
                                            <th class="center">Revenue</th>
                                            <th class="center">Dispatch Value</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDetailDua" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background: #0f2233;color: #fff;text-align: center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id=""></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table style="width:100%;" class="table table-hover table-bordered dataTable no-footer" id="detailAgain2">
                            <thead style="background-color: #05354D; color:#fff;">
                            <tr>
                                <th class="center">No</th>
                                <th class="center">Date</th>
                                <th class="center">Aircraft Reg</th>
                                <th class="center">Cancel</th>
                                <th class="center">Delay</th>
                                <th class="center">Revenue</th>
                                <th class="center">Dispatch Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var symbol = '●';


    $(document).ready(function () {
        monthlyChart();
        $("#detailAgain2").DataTable();
        $("#detailAgain").DataTable();
        changeActype();
    });

    $("#manufacture").on("change", function (e) {
        e.preventDefault();
        var ini = $(this);
        $("#actype").find('option').remove();
        $("#acReg").find('option').remove();
        $.ajax({
            url: '<?= base_url() ?>index.php/Dispatch_Reability/showAllActype',
            type: 'GET',
            data: {
                manufacture: $(this).val()
            },
            dataType: "JSON"
        })
            .done(function (data) {
                $.each(data, function (i, val) {
                    $("#actype").append('<option value="' + val.actype + '">' + val.actype + '</option>');
                })
            })
            .fail(function () {
                notif("error", "error to load data");
            })
            .always(function () {
                var text = $("#manufacture option:selected").text();
                notif("success", "Change to " + text);
            });

    });

    $("#filterDispatch").on("submit", function (e) {
        e.preventDefault();
        monthlyChart();
        $("#_actype").val($("#actype").val());
        $("#_acreg").val($("#acReg").val());
    });

    $("#range_type").on("change", function (e) {
        var val = $(this).val();
        if (val == "monthly") {
            $("#week").fadeOut(1000, function () {
                $("#month").fadeIn();
                $("#actype").select2("val", ["GA"]);
            });
        } else {
            $("#month").fadeOut(1000, function () {
                $("#week").fadeIn();
                $("#actype").select2("val", ["B737-800"]);
            });
        }
    });

    function changeActype() {
        $("#_actype").val($("#actype").val());
        var acType = $("#actype").val();
        var manufacture = $("#manufacture").val();
        var ini = $("#actype");
        $("#acReg").find('.data').remove();
        if (acType == "GA" || acType == "CITILINK") {
            $("#acReg").html('<option value="all" class="data" id="all" selected="">All</option>');
        } else {
            $.ajax({
                url: '<?= base_url() ?>index.php/Dispatch_Reability/dataReg/' + acType,
                dataType: 'JSON',
            })
                .done(function (data) {
                    $("#acReg").find('option.data#all').remove();
                    $.each(data, function (index, reg) {
                        $("#acReg").append('<option class="data" value="' + reg.acreg + '">' + reg.acreg + '</option>');
                    });
                })
                .fail(function () {
                    notif("error", "Failed load Aircraft Reg Data");
                })
                .always(function () {
                    console.log("OK");
                });
        }
    }

    $("#actype").on("change keyup", function (e) {
        changeActype();
    });

    $(".yearpicker").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });

    $(".yearpicker").on("change", function (e) {
        moreThan();
    });

    function moreThan() {
        var date1 = $("#date1").val();
        var date2 = $("#date2").val();
        if (date2 > date1) {
            notif("warning", "input for the intended year should be less than the start year");
            $("#date2").val(date1);
        }
    }

    function modalDetailDua(date, actype) {
        $("#modalDetailDua .modal-header .target-type-again").remove();
        $("#modalDetailDua").find('.modal-title').html(actype);
        // $("#modalDetailDua").find('.modal-body').find('#detailAgain2 tbody tr').remove();
        $("#detailAgain2").DataTable().clear().draw();
        if (actype == 'target') {
            null;
        } else {
            var currentType = $("#_actype").val();
            var currentReg = $("#_acreg").val();
            $.ajax({
                url: '<?= base_url() ?>index.php/Dispatch_Reability/chartDetailAgainDua',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    currentReg: currentReg,
                    date: date,
                    actype: actype
                },
                beforeSend: function () {
                    $("#loadings").fadeIn(1000);
                }
            })
                .done(function (data) {
                    var no = 1;
                    var target = '<p class="target-type-again" style="font-size: 15px;margin-bottom: -5px;margin-left: -15px;" id="titleDetail">Target : ' + ((data[0].dr_target_val == null) ? 'Empty' : data[0].dr_target_val) + '</p>';
                    $("#modalDetailDua .modal-header").append(target);
                    $.each(data, function (index, reg) {
                        var bg = "";
                        var style = "";
                        var aidi = "";
                        if (parseFloat(reg.dr_result) < parseFloat(reg.dr_target_val)) {
                            // bg = "bg-red";
                            style = "cursor: pointer; color: #F24738;";
                            aidi = "mitigationWeekly";
                        }
                        // var content = "<tr style='"+style+"' class='"+bg+"' id='"+aidi+"' data-id='"+reg.dr_id+"'>"+
                        //                   "<td>"+no+"</td>"+
                        //                   "<td>"+reg.dr_date+"</td>"+
                        //                   "<td>"+reg.dr_acreg+"</td>"+
                        //                   "<td>"+parseFloat(reg.dr_result).toFixed(2)+"</td>"+
                        //               "</tr>";
                        // $("#detailAgain2").find('tbody').append(content);
                        var row = $("#detailAgain2").DataTable().row.add([no, date, reg.dr_acreg, reg.dr_total_cancel, reg.dr_total_delay, reg.dr_total_rev, parseFloat(reg.dr_result).toFixed(2)]).draw().node();
                        $(row)
                            .attr('style', style)
                            .attr('id', aidi)
                            .attr("data-id", reg.dr_id);
                        no++;
                    })
                    // $("#detailAgain2").DataTable();
                })

                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    $("#loadings").fadeOut(1000, function () {
                        $("#modalDetailDua").modal("show");
                    });
                });

        }
    }

    $(document).on("click", "#mitigationWeekly", function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        getMitigation(id);
    });

    function monthlyChart() {
        var acType = $("#actype").val();
        var acReg = $("#acReg").val();
        var rangType = $("#range_type").val();

        var date1, date2;

        if (rangType == "weekly") {
            date1 = $("#week1").val();
            date2 = $("#week2").val();
        } else {
            date1 = $("#date1").val();
            date2 = $("#date2").val();
        }

        $.ajax({
            url: '<?= base_url() ?>index.php/Dispatch_Reability/chartDispatchMonthly/' + rangType,
            type: 'POST',
            dataType: 'JSON',
            data: {acType: acType, acReg: acReg, date1: date1, date2: date2},
            beforeSend: function () {
                $(".overlay-box").fadeIn(1000);
            }
        })
            .done(function (data) {

                var plotOptions = {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    },
                    series: {
                        cursor: 'pointer',
                        borderRadiusTopLeft: '10px',
                        borderRadiusTopRight: '10px',
                        point: {
                            events: {
                                click: function (e) {
                                    if (rangType == 'weekly') {
                                        var date = this.category;
                                        var actype = e.point.series.name;
                                        modalDetailDua(date, actype);
                                    } else {
                                        var month = this.category;
                                        var year = e.point.series.name;
                                        show_modal(month, year);
                                    }
                                }
                            }
                        }
                    }
                };
                var legend = {
                    itemStyle: {
                        fontSize: '15px',
                    },
                };
                data["plotOptions"] = plotOptions;
                data["legend"] = legend;
                $('#grafik1').highcharts(data);
            })
            .fail(function () {
                console.log("error");
            })
            .always(function (data) {
                $(".overlay-box").fadeOut(1000);
            });

    }

    $("#modal-detail-table").on("show.bs.modal", function (e) {
        // $("#detailAgain").find('tbody tr').remove();
        $(this).find('.target-type-again').remove();
        $("#detailAgain").DataTable().clear().draw();
        $("#detailAgain").DataTable().destroy();
        var year = $(e.relatedTarget).data("year");
        var month = $(e.relatedTarget).data("month");
        var actype = $(e.relatedTarget).data("actype");
        var acreg = $(e.relatedTarget).data("acreg");
        // $(this).find('.modal-title-panel').text("Aircraft Type " + actype);
        $(this).find('.modal-title-panel').text(actype);
        var ini = $(this);
        $.ajax({
            url: '<?= base_url() ?>index.php/Dispatch_Reability/chartDetailAgainHuhkaa',
            type: 'POST',
            dataType: 'JSON',
            data: {
                year: year,
                month: month,
                actype: actype,
                acreg: acreg,
            },
            beforeSend: function () {
                $("#loadings").fadeIn(1000);
            }
        })
            .done(function (data) {
                var no = 1;
                var target = '<p class="target-type-again" style="text-align:center;font-size: 15px;margin-bottom: -5px;    margin-left: -15px;" id="titleDetail">Target : ' + ((data[0].dr_target_val == null) ? 'Empty' : data[0].dr_target_val) + '</p>';
                ini.find('.modal-header').append(target);
                $.each(data, function (index, reg) {
                    var bg = "";
                    var style = "";
                    var aidi = "";
                    if (parseFloat(reg.dr_result) < parseFloat(reg.dr_target_val)) {
                        // bg = "bg-red";
                        style = "cursor: pointer; color: #F24738;";
                        aidi = "mitigation";
                    }
                    // var content = "<tr style='"+style+"' class='"+bg+"' id='"+aidi+"' data-id='"+reg.dr_id+"'>"+
                    //                   "<td style='text-align:center;'>"+no+"</td>"+
                    //                   "<td style='text-align:center;'>"+reg.dr_acreg+"</td>"+
                    //                   "<td style='text-align:center;'>"+parseFloat(reg.dr_result).toFixed(2)+"</td>"+
                    //               "</tr>";
                    // $("#detailAgain").find('tbody').append(content);
                    var row = $("#detailAgain").DataTable().row.add([no, numToDate(month) + "-" + year, reg.dr_acreg, reg.dr_total_cancel, reg.dr_total_delay, reg.dr_total_rev, parseFloat(reg.dr_result).toFixed(2)]).draw().node();
                    $(row)
                        .addClass(bg)
                        .attr('style', style)
                        .attr('id', aidi)
                        .attr("data-id", reg.dr_id);
                    no++;
                })
            })
            .fail(function () {
                notif("error", "failed to load data");
            })
            .always(function (data) {
                $("#loadings").fadeOut(1000);
            });

    });

    $(document).on("click", "#mitigation", function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        getMitigation(id);

    });

    function getMitigation(id) {
        $.ajax({
            url: '<?= base_url() ?>index.php/Dispatch_Reability/getMitigation/' + id,
            type: 'GET',
            dataType: 'JSON'
        })
            .done(function (data) {
                if (data != null) {
                    notif("warning", '<h3>MITIGATION !!!</h3><table class="table table-bordered"><tr><td>Why</td><td>' + data.mitigasi_why + '</td></tr><tr><td>Solution</td><td>' + data.mitigasi_solution + '</td></tr></table>');
                }
            })
            .fail(function () {
                notif("error", "failed to load data");
            })
            .always(function (data) {
                console.log("complete");
            });
    }

    function show_modal(month, year) {
        $("#modal-poin").find('.modal-body').find('a').remove();
        $("#titleDetail").html("Detail " + month + " " + year);
        $("#modal-poin p.target-type").remove();
        if (year == 'target') {
            notif("error", "This is not series");
        } else {
            var currentType = $("#_actype").val();
            var currentReg = $("#_acreg").val();
            $.ajax({
                url: '<?= base_url() ?>index.php/Dispatch_Reability/chartDetail',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    currentType: currentType,
                    currentReg: currentReg,
                    month: dateToNum(month),
                    year: year
                },
                beforeSend: function () {
                    $("#loadings").fadeIn(1000);
                }
            })
                .done(function (data) {
                    // var target = '<p class="target-type" style="text-align:center;font-size: 15px;margin-bottom: -5px;" id="titleDetail">Target : '+data[0].dr_target_val+'</p>';
                    // $("#modal-poin .modal-header").append(target);
                    $.each(data, function (index, detail) {
                        var color;
                        if (parseFloat(detail.dr_result) < parseFloat(detail.dr_target_val)) {
                            color = "btn-danger";
                        } else {
                            color = "bg-navy";
                        }
                        console.log(parseFloat(detail.dr_result) + " < " + parseFloat(detail.dr_target_val) + " = " + color);
                        var button = '<a class="btn btn-block ' + color + '" data-month="' + dateToNum(month) + '" data-year="' + year + '" data-actype="' + detail.dr_actype + '" data-acreg="' + currentReg + '" data-toggle="modal" data-target="#modal-detail-table">' +
                            detail.dr_actype + "(" + parseFloat(detail.dr_result).toFixed(2) + ")";
                        '</a>';
                        $("#modal-poin").find('.modal-body').append(button);
                    })
                })
                .fail(function () {
                    notif("error", "Failed to load");
                })
                .always(function (data) {
                    $("#loadings").fadeOut(1000, function () {
                        $('#modal-poin').modal('show');
                    });
                });
        }
    }


    function dateToNum(month) {
        switch (month) {
            case 'Jan':
                return '1';
                break;
            case 'Feb':
                return '2';
                break;
            case 'Mar':
                return '3';
                break;
            case 'Apr':
                return '4';
                break;
            case 'May':
                return '5';
                break;
            case 'Jun':
                return '6';
                break;
            case 'Jul':
                return '7';
                break;
            case 'Aug':
                return '8';
                break;
            case 'Sep':
                return '9';
                break;
            case 'Oct':
                return '10';
                break;
            case 'Nov':
                return '11';
                break;
            case 'Des':
                return '12';
                break;
        }
    }

    function numToDate(month) {
        switch (month) {
            case 1:
                return 'Jan';
                break;
            case 2:
                return 'Feb';
                break;
            case 3:
                return 'Mar';
                break;
            case 4:
                return 'Apr';
                break;
            case 5:
                return 'May';
                break;
            case 6:
                return 'Jun';
                break;
            case 7:
                return 'Jul';
                break;
            case 8:
                return 'Aug';
                break;
            case 9:
                return 'Sep';
                break;
            case 10:
                return 'Oct';
                break;
            case 11:
                return 'Nov';
                break;
            case 12:
                return 'Des';
                break;
        }
    }

    function show_detail_table(aircraft_type) {
        $('#modal-detail-table').modal('show');
        $('#tittle-detail-table').html(aircraft_type);
    }
</script>
.