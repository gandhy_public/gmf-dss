<style>
    .box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
      display: inline-block;
      font-size: 20px;
      font-weight: bold;
      margin: 0;
      line-height: 1;
    }
    .wadah{
      padding-left: 1px;
      padding-right: 0px;
    }
    .highcharts-title{
      cursor: pointer;
    }
</style>

<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>

<section class="content">
  <div class="">
    <div class="box-body">
      <div class="row">

<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <section class="content">
    <div class="box" style="padding-left: 10px; padding-right: 10px">
      <div class="box-header with-border">
        <h3 class="box-title"><?= $title ?></h3>
        <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
          <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="form-group">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label for="period_start" class="col-sm-4 control-label" style="margin-top: 5px;">Period Start</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control input-sm pull-right" id="date_start" value="<?= date('Y-m'); ?>">
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label for="period_end" class="col-sm-4 control-label" style="margin-top: 5px;">Period End</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control input-sm pull-right" id="date_end" value="<?= date('Y-m'); ?>">
                </div>
              </div>
            </div>
            <div class="col-sm-1">
              <button type="button" class="btn btn-default pull-right"><i class="fa fa-search"></i> Search
              </button>
            </div>
          </div>
        </div>

        <br>
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">RPCR Target</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>

          <div class="box-body">
            <br>
        <form action="#" id="form-input" class="form-horizontal">
            <div class="row">
              <div class="form-group">
                <label for="inputTarget" class="col-sm-2 control-label" style="margin-top: 5px;">Target Garuda</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="terget_garuda" name="terget_garuda" placeholder="Target Garuda">
                </div>
              </div>
            </div>

            <br>
            <div class="row">
              <div class="form-group">
                <label for="inputTarget" class="col-sm-2 control-label" style="margin-top: 5px;">Target Citylink</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="terget_citylink" name="terget_citylink" placeholder="Target Citylink">
                </div>
                <div class="col-sm-1">
                  <button id="button_save" type="button" class="btn btn-primary" onclick="save()"><i class="fa fa-save"></i> Save
                </button>
                </div>
              </div>
            </div>
            <br><br>
            <p><b>Current Year: 2018</b></p>
        </form>

          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>

    <div class="box" style="padding-left: 10px; padding-right: 10px">
      <div class="box-header with-border">
        <h3 class="box-title">Mitigation</h3>
        <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
          <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <table class="table table-hover" id="table-cotd">
              <thead>
                <tr>
                  <th class="center">No</th>
                  <th class="center">Date</th>
                  <th class="center">Aircraft</th>
                  <th class="center">Aircraft Type</th>
                  <th class="center">Start Date</th>
                  <th class="center">Due Date</th>
                  <th class="center">Action</th>
                </tr>
              </thead>

              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

      </div>
    </div>
  </div>
</section>


<script>

  // $(document).ready(function(){
  //   var table;
  //   $(document).ready(function () {
  //     table = $('#table-cotd').DataTable({
  //             "processing": true,
  //             "serverSide": true,
  //             "ordering": false,
  //             "ajax": {
  //               "url": "<?php echo site_url('index.php/master_cotd/data_list') ?>",
  //               "type": "POST"
  //             },
  //       });
  //   });
  // });

  function dialogWarning(message){
    swal({
        title: "Warning",
        text: message,
        showConfirmButton: true,
        type: 'warning'
    });
  }

  function dialogSuccess(message){
    swal({
        title: "Done",
        text: message,
        timer: 1500,
        showConfirmButton: false,
        type: 'success'
    });
  }

  $('#date_end, #date_start').datepicker({
    todayBtn: "linked",
    format: 'yyyy-mm',
    viewMode: 'months',
    minViewMode: 'months',
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
  });

  function save(){
    var formData = new FormData($('#form-input')[0]);
    $.ajax({
      url: "<?php echo base_url() ?>index.php/master_cotd/save",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      // dataType: "JSON",
      success: function(results)
      {
        results = JSON.parse(results);
        console.log(results);
        if (results['sukses']) {
            dialogSuccess(results['message']);
        }else{
            dialogWarning(results['message']);
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          console.log(errorThrown);
          alert('Error adding / update data');
      }
    });
  }

</script>
