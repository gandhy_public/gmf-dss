<style>
    .box-header > .fa,
    .box-header > .glyphicon,
    .box-header > .ion,
    .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }

    .wadah {
        padding-left: 5px;
        padding-right: 5px;
    }

    div.dataTables_wrapper {
        width: 500px;
        margin: 0 auto;
    }

    .box {
        margin-bottom: 4px;
    }

    .highcharts-title {
        cursor: pointer;
    }

    .box-title > .title_garuda:hover {
        color: #4cc3d9;
    }

    .box-title > .title_citilink:hover {
        color: #4cc3d9;
    }

    .box-title > .title_ata_total:hover {
        color: #4cc3d9;
    }

    .box-title > .title_ata_jam:hover {
        color: #4cc3d9;
    }

    .box-title > .title_depar_total:hover {
        color: #4cc3d9;
    }

    .box-title > .title_depar_jam:hover {
        color: #4cc3d9;
    }

    .title-manufacture {
        color: #F2C573;
        text-align: center;
        font-size: 25px;
        font-weight: bold;
        margin-top: 30px;
    }

    .widget-user .widget-user-header {
        padding: 1px;
        height: 60px;
        border-top-right-radius: 11px;
        border-top-left-radius: 11px;
        background: linear-gradient(to bottom, #001f3f 50%, #111111 100%);
    }

    .widget-user .widget-user-image {
        position: absolute;
        top: 5px;
        left: 48%;
        margin-left: -45px;
    }

    .box .border-right {
        border-right: 1px solid #eac8c8;
    }

    .scrollYudiPoenya {
        width: auto;
        height: 600px;
        overflow-x: scroll;
        overflow-y: scroll;
    }
</style>

<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol> -->
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box" style="box-shadow:none; border-top:none;">
                                <div class="box-header" style="padding-top:0px;">
                                    <h3 class="box-title" style="font-size: 20px;font-weight: bold;">Year to Date (
                                        <?= date("d") . " " . convert_month(date("m")) . " " . date("Y") ?>)
                                    </h3>
                                </div>
                                <div class="box-body" style="padding-top:0px;">

                                    <div class="col-md-5" style="margin-left: 10%;">
                                        <div class="box box-widget widget-user" style="border-radius: 11px;">
                                            <div class="widget-user-header bg-navy">
                                                <h3 class="title-manufacture">GARUDA</h3>
                                            </div>
                                            <div class="widget-user-image">
                                                <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                     alt="User Avatar" style="border: transparent;width: 100px;">
                                            </div>
                                            <div class="box-footer"
                                                 style="border-bottom-left-radius: 11px;border-bottom-right-radius: 11px; padding-top:0px; padding:0px;">
                                                <div class="row">
                                                    <div class="col-sm-6 border-right">
                                                        <div class="description-block">
                                                            <h5 class="description-header" id="actual-garuda"
                                                                style="font-size: 33px;font-weight: bold;margin-left:20px">
                                                                <span class="actual_garuda"></span>
                                                                <span class="panah_garuda"></span>
                                                            </h5>
                                                            <span class="description-text">Actual</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="description-block">
                                                            <h5 class="description-header" id="target-garuda"
                                                                style="font-size: 33px;font-weight: bold;margin-left:20px">
                                                                <span class="target_garuda"></span>
                                                                <i class="fa fa-minus" style="color:#F2C573;"></i>
                                                            </h5>
                                                            <span class="description-text">Target</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="box box-widget widget-user"
                                             style="border-radius: 11px; margin-top:1px;">
                                            <div class="widget-user-header bg-navy">
                                                <h3 class="title-manufacture">CITILINK</h3>
                                            </div>
                                            <div class="widget-user-image">
                                                <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                     alt="User Avatar" style="border: transparent;width: 100px;">
                                            </div>
                                            <div class="box-footer"
                                                 style="border-bottom-left-radius: 11px;border-bottom-right-radius: 11px; padding-top:0px; padding:0px;">
                                                <div class="row">
                                                    <div class="col-sm-6 border-right">
                                                        <div class="description-block">
                                                            <h5 class="description-header" id="actual-garuda"
                                                                style="font-size: 33px;font-weight: bold;margin-left:20px">
                                                                <span class="actual_citilink"></span>
                                                                <span class="panah_citilink"></span>
                                                            </h5>
                                                            <span class="description-text">Actual</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="description-block">
                                                            <h5 class="description-header" id="target-garuda"
                                                                style="font-size: 33px;font-weight: bold;margin-left:20px">
                                                                <span class="target_citilink"></span>
                                                                <span class="">
                                                                    <i class="fa fa-minus" style="color:#F2C573;"></i>
                                                                </span>
                                                            </h5>
                                                            <span class="description-text">Target</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <hr style="margin-top: 0px; margin-bottom: 2px; border-top: 3px solid #cdd1d9;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box" style="box-shadow:none; border-top:none;">
                                <div class="box-header" style="padding-top: 7px;">
                                    <h3 class="box-title" style="font-size: 20px;font-weight: bold;">Detail Delay</h3>
                                </div>
                                <div class="box-body" style="padding-top:0px;">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Aircraft</label>
                                                <select class="form-control selectpicker" multiple id="aircraft"
                                                        data-size="12" title="- All Aircraft -">
                                                    <option value="all" selected>All</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Start Date</label>
                                                <input type="text" class="form-control" id="date_start">
                                            </div>
                                            <div class="form-group">
                                                <label>End Date</label>
                                                <input type="text" class="form-control pull-right" id="date_end"
                                                       value="<?= date('m-Y'); ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <button type="button" class="btn bg-navy btn-flat form-control"
                                                        id="searc"><i class="fa fa-search"></i>
                                                    Search
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-5" id="garuda_place" style="height:300px;">
                                            <div class="overlay-box" id="loaderGaruda"
                                                 style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                <div style="" class="loading"></div>
                                            </div>
                                            <h3 class="box-title" style="margin-top: 0px;text-align: center;"><span
                                                        class="title_garuda"
                                                        style="margin: 0 auto; font-weight: bold; cursor: pointer;text-decoration:underline; text-align: center;">Garuda</span>
                                            </h3>
                                            <div id="grafik_garuda" style=""></div>
                                        </div>
                                        <div class="col-md-5" id="citilink_place" style="height:300px;">
                                            <div class="overlay-box" id="loaderCitilink"
                                                 style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                <div style="" class="loading"></div>
                                            </div>
                                            <h3 class="box-title" style="margin-top: 0px;text-align: center;"><span
                                                        class="title_citilink"
                                                        style="margin: 0 auto; font-weight: bold; cursor: pointer;text-decoration:underline; text-align: center;">Citilink</span>
                                            </h3>
                                            <div id="grafik_citilink" style=""></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title" style="font-size: 20px; font-weight: bold;">General Chart</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">

      <div class="col-lg-2">
      </div>

      <div class="col-lg-4">
        <div class="form-group">
          <div class="small-box bg-navy">
            <span><h1 style="color:#F2C573;text-align: center;font-weight:bold" >Garuda</h1></span>
            <div class="row" style="    margin-top: 25px;" >
              <div class="col-lg-5" style="text-align: center;margin-top: 53px;">
                <h4>Actual</h4>
              </div>
              <div class="col-lg-7" style="margin-left: -14px;    margin-top: 20px;">
                <h1 style="font-size: 42px;" href="#tabel_detail" id="panel_garuda">
                <span class=""></span> <span class=""></span>
                </h1>
              </div>

            </div>
            <hr style="margin-top:0px;margin-bottom:0px;    width: 78%;">
            <div class="row" >
              <div class="col-lg-5" style="text-align: center;    margin-top: -2px;">
                <h4>Target</h4>
              </div>
              <div class="col-lg-7">
                <h1 style="font-size: 40px;   margin-top: 4px; margin-left: 26px;" href="#tabel_detail" id="panel_garuda">
                  <span class=""></span> <span class="description-percentage "><i style="color:#F2C573;"
                                          class="fa fa-minus"></i></span></h1>
              </div>

            </div>
            <br/><br/><br/>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
          <div class="small-box bg-navy">
            <span><h1 style="color:#F2C573;   text-align: center;font-weight:bold">Citilink</h1></span>
            <div class="row" style="    margin-top: 25px;">
              <div class="col-lg-5" style="text-align: center;margin-top:53px">
                <h4>Actual</h4>
              </div>
              <div class="col-lg-7" style="margin-left: -14px;    margin-top: 20px;">
                <h1 style="font-size: 42px;" href="#tabel_detail" id="panel_garuda">
                 <span class=""></span> <span class=""></span>
                </h1>
              </div>

            </div>
            <hr style="margin-top:0px;margin-bottom:0px;    width: 78%;">
            <div class="row">
              <div class="col-lg-5" style="text-align: center;    margin-top: -2px;">
                <h4>Target</h4>
              </div>
              <div class="col-lg-7">
                <h1 style="font-size: 40px; margin-top: 4px;   margin-left: 26px;" href="#tabel_detail" id="panel_garuda">
                   <span class=""></span> <span class="description-percentage "><i style="color:#F2C573;"
                                          class="fa fa-minus"></i></span></h1>
              </div>

            </div>
            <br/><br/><br/>
          </div>
        </div>
      </div>

      <div class="col-lg-2">
      </div>


    </div>
  </div> -->
    <div class="row" style="display: none" id="detail">
        <div class="col-md-12">
            <div class="box" id="boxDetail">
                <div class="box-header with-border">
                    <h3 class="box-title box-title-detail"></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" id="toggleDetail">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="wadah col-lg-6 col-md-5 col-sm-12 col-xs-12">
                            <div class="box" style="box-shadow: none;">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><span class="title_ata_total"
                                                                style="font-weight: bold; cursor: pointer;text-decoration:underline;">Delay by Frequent</span>
                                    </h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                                data-toggle="tooltip" title="Collapse">
                                            <i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="box-header with-border" style="border-bottom: 1px solid #fff;">
                                                <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="overlay-box" id="loader1"
                                                         style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                        <div style="" class="loading"></div>
                                                    </div>
                                                    <div id="detail_delay_grafik1"
                                                         style="height:250px;width:100%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wadah col-lg-6 col-md-5 col-sm-12 col-xs-12">
                            <div class="box" style="box-shadow: none;">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><span class="title_ata_jam"
                                                                style="font-weight: bold; cursor: pointer;text-decoration:underline;">Delay by Duration</span>
                                    </h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                                data-toggle="tooltip" title="Collapse">
                                            <i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="box-header with-border" style="border-bottom: 1px solid #fff;">
                                                <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="overlay-box" id="loader2"
                                                         style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                        <div style="" class="loading"></div>
                                                    </div>
                                                    <div id="detail_delay_grafik2"
                                                         style="height:250px;width:100%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tabel_1 wadah col-lg-6 col-md-6 col-sm-12 col-xs-12" style="display:none">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box" style="box-shadow: none;">
                                            <div class="box-header">
                                                <h4 class="box-title title_1"></h4>
                                            </div>
                                            <div class='table-responsive scrollYudiPoenya'>
                                                <table id="tabel_1" class="table table-bordered table-striped dataTable"
                                                       role="grid" aria-describedby="example1_info">
                                                    <thead id='head_1' style="background-color: #bfbbbb;">
                                                    </thead>
                                                    <tbody id='isi_1'>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="tabel_2 wadah col-lg-6 col-md-6 col-sm-12 col-xs-12" style="display:none">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box" style="box-shadow: none;">
                                            <div class="box-header">
                                                <h4 class="box-title  title_2"></h4>
                                            </div>
                                            <div class='table-responsive scrollYudiPoenya'>
                                                <table id="tabel_2" class="table table-bordered table-striped dataTable"
                                                       role="grid" aria-describedby="example1_info">
                                                    <thead id='head_2' style="background-color: #bfbbbb;">
                                                    </thead>
                                                    <tbody id='isi_2'>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="wadah col-lg-6 col-md-5 col-sm-12 col-xs-12">
                            <div class="box" style="box-shadow: none;">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><span class="title_depar_total"
                                                                style="font-weight: bold; cursor: pointer;text-decoration:underline;">Delay by Departure (Total)</span>
                                    </h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                                data-toggle="tooltip" title="Collapse">
                                            <i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="box-header with-border" style="border-bottom: 1px solid #fff;">
                                                <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="overlay-box" id="loader3"
                                                         style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                        <div style="" class="loading"></div>
                                                    </div>
                                                    <div id="detail_delay_grafik3"
                                                         style="height:250px;width:100%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wadah col-lg-6 col-md-5 col-sm-12 col-xs-12">
                            <div class="box" style="box-shadow: none;">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><span class="title_depar_jam"
                                                                style="font-weight: bold; cursor: pointer;text-decoration:underline;">Delay by Departure (Time)</span>
                                    </h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                                data-toggle="tooltip" title="Collapse">
                                            <i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="box-header with-border" style="border-bottom: 1px solid #fff;">
                                                <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="overlay-box" id="loader4"
                                                         style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                        <div style="" class="loading"></div>
                                                    </div>
                                                    <div id="detail_delay_grafik4"
                                                         style="height:250px;width:100%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"  style="font-size: 20px; font-weight: bold;"><?= $title ?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">

      <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-3">
          <div class="form-group">
            <label for="inputEmail3" style="margin-top: 7px;"
                 class="col-sm-3 control-label">Aircraft</label>

            <div class="col-sm-9">

            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">



          </div>
        </div>
        <div class="col-sm-4" style="margin-left: -48px;">
          <div class="form-group">


          </div>
        </div>
        <div class="col-sm-1" style="text-align:center;">
        </div>
      </div>
      <div class="row"><div class="wadah col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="box">
            <div class="box-header with-border">

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"
                    data-toggle="tooltip"
                    title="Collapse">
                  <i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="box-header with-border" style="border-bottom: 1px solid #fff;">
                    <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="wadah col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"
                    data-toggle="tooltip"
                    title="Collapse">
                  <i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="box-header with-border" style="border-bottom: 1px solid #fff;">
                    <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div> -->
</section>

<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233">
                <h4 class="modal-title title_mitigasi" id="title-edit" style="color: #fff" </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">

                        <table class="table table-hover table-bordered table-gmf" id="tabel_detail">
                            <thead style='background-color:#05354D'>
                            <tr>
                                <th class="text-center">Month</th>
                                <th class="text-center">A/C Type</th>
                                <th class="text-center">Target</th>
                                <th class="text-center">Actual</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Jan</td>
                                <td>B737</td>
                                <td>0.5</td>
                                <td>0.4</td>
                            </tr>
                            <tr>
                                <td>Jan</td>
                                <td>B777</td>
                                <td>0.5</td>
                                <td>0.4</td>
                            </tr>
                            <tr>
                                <td>Jan</td>
                                <td>A330</td>
                                <td>0.5</td>
                                <td>0.6</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal"><i
                                class="fa fa-rotate-left"></i> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#toggleDetail").on("click", (e) => {
        e.preventDefault();
        $("#detail").fadeOut('fast');
    });
    var d = new Date();
    var awal_tahun = '01-' + d.getUTCFullYear();
    $("#date_start").val(awal_tahun);

    var month = [];
    month[0] = "JAN";
    month[1] = "FEB";
    month[2] = "MAR";
    month[3] = "APR";
    month[4] = "MAY";
    month[5] = "JUN";
    month[6] = "JUL";
    month[7] = "AUG";
    month[8] = "SEPT";
    month[9] = "OCT";
    month[10] = "NOV";
    month[11] = "DEC";

    $(document).ready(function () {

        $('#aircraft').val('');
        $(".selectpicker").selectpicker("refresh");
        var list_actype;
        $.ajax({
            url: "<?= base_url() ?>index.php/Cotd/combo_actype",
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                // console.log(data);
                list_actype = '<select class="selectpicker" id="aircraft" data-live-search="false" multiple title="- Select Aircraft Type -"><option value="All" nama="All" selected>All</option>';
                $(data.data).each(function (key, val) {
                    list_actype += '<option value="' + val.actype_id + '" nama="' + val.actype + '">' + val.actype + '</option>';
                });
                list_actype += '</select>';

                // console.log(list_actype);
                $('#aircraft').html(list_actype);
                $('#aircraft').val('selectAll');
                $('#aircraft').removeAttr("disabled");
                $(".selectpicker").selectpicker("refresh");
            }
        });


        actual_garuda();
        actual_citilink();
        grafik_garuda();
        grafik_citilink();
        $(document).on("click", "#searc", function (e) {
            var start_date = $("#date_start").val();
            var start_date = start_date.split("-");
            var end_date = $("#date_end").val();
            var end_date = end_date.split("-");
            if (start_date[1] != end_date[1]) {
                notif("warning", "Tidak boleh beda tahun!!!");
            } else if (parseInt(start_date[0]) > parseInt(end_date[0])) {
                notif("warning", "Start date harus lebih awal dari end date!!!");
            } else {
                $("#detail").hide();
                grafik_garuda();
                grafik_citilink()

            }
        });

        $('.title_garuda, .title_citilink').click(function () {
            var start_date = $("#date_start").val();
            var end_date = $("#date_end").val();
            detailDelayShow($(this).text(), start_date, end_date);

        });


        $('#aircraft').selectpicker().change(function () {
            toggleSelectAll($(this));
        }).trigger('change');


    });

    function detail_grafik(aircraft, date) {
        var bultan = date.split("-");
        $(".title_mitigasi").html(aircraft + '/' + month[bultan[0] - 1] + '/' + bultan[1]);
        $("#modal_detail").modal("show");
        if (aircraft == 'Garuda') {
            var link = "<?= base_url() ?>index.php/Cotd/view_detail_garuda";
        } else {
            var link = "<?= base_url() ?>index.php/Cotd/view_detail_citilink";
        }

        var aircraft = [];
        var nm_aircraft = [];
        var aircraft = $('#aircraft').val();
        var nm_aircraft = $('#aircraft').text();
        var table = $("#tabel_detail").DataTable({
            "destroy": true,
            "serverSide": false,
            "processing": false,
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false,

            columns: [{
                data: 'bulan'
            },
                {
                    data: 'ac_type'
                },
                {
                    data: 'target'
                },
                {
                    data: 'actual'
                },
            ],
            ajax: {
                type: 'POST',
                url: link,
                data: {
                    'aircraft': aircraft,
                    'nm_aircraft': nm_aircraft,
                    'date': date,
                },
                dataType: 'JSON',
                dataSrc: function (json) {
                    if (json.status == 'Tidak ada data') {
                        var return_data = [];
                        return_data.push({
                            'bulan': '<center style="width: 202px;">No data available in table</center>',
                            'ac_type': '<left></left>',
                            'target': '<left></left>',
                            'actual': '<center></center>',
                        });

                        return return_data;

                    } else {
                        var u = 1;

                        var totala = '';
                        var return_data = [];
                        for (var i = 0; i < json.tabel.length; i++) {
                            var bulan = month[json.tabel[i].bulan - 1];
                            return_data.push({
                                'bulan': bulan,
                                'ac_type': json.tabel[i].ac_type,
                                'target': json.target,
                                'actual': json.tabel[i].actual,
                            });
                            u++
                        }
                        return return_data;

                    }

                }
            },
            // rowCallback: function(row, data, index){
            createdRow: function (row, data, index) {
                if (data['actual'] > data['target']) {
                    $(row).css('color', '#F24738');
                    $(row).css('cursor', 'pointer');
                    $(row).on('click', function () {
                        note_mitigasi(data['ac_type'], date)
                    });

                }
            }
        });

        // $(document).on('click','#tabel_detail tr',function(e){
        // var bulan         = table.row(this).data()['bulan'];
        // var ac_type        = table.row(this).data()['ac_type'];
        // var target       = table.row(this).data()['target'];
        // var actual        = table.row(this).data()['actual'];
        // if(actual>target){
        // note_mitigasi(ac_type, date)
        // }
        // });

    }

    function tabel_detail_delay(tipe, aircraft, start, end) {
        $(".tabel_1, .tabel_2").show();
        if (tipe == '1') {
            var link1 = "<?= base_url() ?>index.php/Cotd/view_ata_total";
            var link2 = "<?= base_url() ?>index.php/Cotd/view_ata_jam";
            var title1 = 'Delay by Frequent';
            var title2 = 'Delay by Duration';
            $('.title_1').html(title1);
            $('.title_2').html(title2);
        } else {
            var link1 = "<?= base_url() ?>index.php/Cotd/view_depar_total_table";
            var link2 = "<?= base_url() ?>index.php/Cotd/view_depar_jam_table";
            var title1 = 'Delay by Departure (Time)';
            var title2 = 'Delay by Departure (Total)';
            $('.title_1').html(title1);
            $('.title_2').html(title2);
        }
        var actype = [];
        var ac_type = $('#aircraft').val();
        $.ajax({
            url: link1,
            type: "POST",
            data: {
                'start': start,
                'end': end,
                'tipe': aircraft,
                'aircraft': ac_type,
                'status': 'table'
            },
            dataType: "JSON",
            beforeSend: function () {
                $('#loadings').fadeIn("fast");
            },
            success: function (data) {
                $('#loadings').fadeOut(1000);
                var list_title = '<tr><td>Aircraft Type</td>' + '<td>Aircraft Reg</td>';
                for (var i = 0; i < data.kategori.length; i++) {
                    list_title += '<td>' + data.kategori[i] + '</td>';
                }
                list_title += '</tr>';
                $('#head_1').html(list_title);
                // console.log(list_title);
                var list_tabel = '';
                for (var i = 0; i < data.grafik.length; i++) {
                    list_tabel += '<tr>';
                    list_tabel += '<td>' + data.grafik[i].name + '</td>' + '<td>' + data.grafik[i].reg + '</td>';
                    for (var a = 0; a < data.grafik[i].data.length; a++) {
                        list_tabel += '<td>' + data.grafik[i].data[a] + '</td>';
                    }
                    list_tabel += '</tr>';
                }

                $('#isi_1').html(list_tabel);
                // console.log(list_tabel);
            }
        });
        $.ajax({
            url: link2,
            type: "POST",
            data: {
                'start': start,
                'end': end,
                'tipe': aircraft,
                'aircraft': ac_type,
                'status': 'table'
            },
            dataType: "JSON",
            beforeSend: function () {
                $('#loadings').fadeIn("fast");
            },
            success: function (data) {
                $('#loadings').fadeOut(1000);
                var list_title1 = '<tr><td>Aircraft Type</td>' + '<td>Aircraft Reg</td>';
                for (var i = 0; i < data.kategori.length; i++) {
                    list_title1 += '<td>' + data.kategori[i] + '</td>';
                }
                list_title1 += '</tr>';

                $('#head_2').html(list_title1);
                // console.log(list_title1);
                var list_tabel1 = '';
                for (var i = 0; i < data.grafik.length; i++) {
                    list_tabel1 += '<tr>';
                    list_tabel1 += '<td>' + data.grafik[i].name + '</td>' + '<td>' + data.grafik[i].reg + '</td>';
                    for (var a = 0; a < data.grafik[i].data.length; a++) {
                        list_tabel1 += '<td>' + data.grafik[i].data[a] + '</td>';
                    }
                    list_tabel1 += '</tr>';
                }

                $('#isi_2').html(list_tabel1);
                // console.log(list_tabel1);

            }
        });

    }


    function note_mitigasi(tipe, date) {
        $.ajax({
            url: "<?= base_url() ?>index.php/Cotd/view_mitigasi",
            type: "POST",
            data: {
                'tipe': tipe,
                'date': date,
            },
            dataType: "JSON",
            success: function (data) {
                notif('warning', '<h3>MITIGATION !!!</h3><table class="table table-bordered"><tr><td>Why</td><td>' + data.why + '</td></tr><tr><td>Solution</td><td>' + data.solusi + '</td></tr></table>');
            }
        });
    }

    //grafik detail
    function detailDelayShow(aircraft, start, end) {
        var ac_type = [];
        var ac_type = $('#aircraft').val();
        $("#detail").show();
        $(".box-title-detail").html(aircraft);


        $('.title_ata_jam, .title_ata_total').click(function () {
            var start_date = $("#date_start").val();
            var end_date = $("#date_end").val();
            tabel_detail_delay("1", aircraft, start_date, end_date);
        });


        $('.title_depar_total, .title_depar_jam').click(function () {
            var start_date = $("#date_start").val();
            var end_date = $("#date_end").val();
            tabel_detail_delay("2", aircraft, start_date, end_date);
        });


        $.ajax({
            url: "<?= base_url() ?>index.php/Cotd/view_ata_total",
            type: "POST",
            data: {
                'start': start,
                'end': end,
                'tipe': aircraft,
                'aircraft': ac_type,
            },
            dataType: "JSON",
            beforeSend: function () {
                $('html, body').animate({
                    scrollTop: $("#detail").position().top
                }, 500);
                $("#loader1").fadeIn('fast');
            },
            success: function (data) {
                $("#loader1").fadeOut(1000);
                var chart1 = new Highcharts.chart('detail_delay_grafik1', {
                        title: {
                            text: ''
                        },
                        credits: {
                            enabled: false
                        },
                        xAxis: {
                            categories: data.kategori,
                            title: {
                                text: 'ATA Chapter'
                            }
                        },
                        yAxis: {
                            title: {
                                text: 'Occurrences'
                            },
                            // min: 1,
                            // max: 6
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },

                        plotOptions: {
                            series: {
                                label: {
                                    connectorAllowed: false
                                }
                            }
                        },
                        colors: [
                            '#05354d',
                            '#f7ca78',
                            '#7bc8a4',
                            '#93648d',
                            '#4cc3d9',
                            '#1f7f91',
                            '#074b6d',
                            '#d18700',
                            '#ffdd9e',
                        ],


                        series: data.grafik,

                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }
                    },
                    // function(chart1){
                    // $('.highcharts-title').click(function(){
                    // var start_date = $("#date_start").val();
                    // var end_date = $("#date_end").val();
                    // tabel_detail_delay("1",aircraft, start_date, end_date);
                    // });
                    // }
                );
            }
        });

        $.ajax({
            url: "<?= base_url() ?>index.php/Cotd/view_ata_jam",
            type: "POST",
            data: {
                'start': start,
                'end': end,
                'tipe': aircraft,
                'aircraft': ac_type,
            },
            dataType: "JSON",
            beforeSend: function () {
                $('#loader2').fadeIn("fast");
            },
            success: function (data) {
                $('#loader2').fadeOut(1000);
                var chart2 = new Highcharts.chart('detail_delay_grafik2', {
                        title: {
                            text: ''
                        },
                        credits: {
                            enabled: false
                        },
                        xAxis: {
                            categories: data.kategori,
                            title: {
                                text: 'ATA Chapter'
                            }
                        },
                        yAxis: {
                            title: {
                                text: 'Delay Length (Hours)'
                            },
                            // min: 1,
                            // max: 6
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },

                        plotOptions: {
                            series: {
                                label: {
                                    connectorAllowed: false
                                }
                            }
                        },
                        colors: [
                            '#05354d',
                            '#f7ca78',
                            '#7bc8a4',
                            '#93648d',
                            '#4cc3d9',
                            '#1f7f91',
                            '#074b6d',
                            '#d18700',
                            '#ffdd9e',
                        ],

                        series: data.grafik,

                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }
                    },
                    // function(chart2){
                    // $('.highcharts-title').click(function(){
                    // var start_date = $("#date_start").val();
                    // var end_date = $("#date_end").val();
                    // tabel_detail_delay("1",aircraft, start_date, end_date);
                    // });
                    // }
                );
            }
        });
        $.ajax({
            url: "<?= base_url() ?>index.php/Cotd/view_depar_total",
            type: "POST",
            data: {
                'start': start,
                'end': end,
                'tipe': aircraft,
                'aircraft': ac_type,
            },
            dataType: "JSON",
            beforeSend: function () {
                $('#loader3').fadeIn('fast');
            },
            success: function (data) {
                $('#loader3').fadeOut(1000);
                var chart3 = new Highcharts.chart('detail_delay_grafik3', {
                        title: {
                            text: ''
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            title: {
                                text: 'Delay Length (Hours)'
                            },
                            // min: 1,
                            // max: 6
                        },
                        xAxis: {
                            categories: data.kategori,
                            title: {
                                text: 'Airport'
                            }
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },

                        plotOptions: {
                            series: {
                                label: {
                                    connectorAllowed: false
                                }
                            }
                        },
                        colors: [
                            '#05354d',
                            '#f7ca78',
                            '#7bc8a4',
                            '#93648d',
                            '#4cc3d9',
                            '#1f7f91',
                            '#074b6d',
                            '#d18700',
                            '#ffdd9e',
                        ],


                        series: data.grafik,

                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }
                    },
                    // function(chart3){
                    // $('.highcharts-title').click(function(){
                    // var start_date = $("#date_start").val();
                    // var end_date = $("#date_end").val();
                    // tabel_detail_delay("2",aircraft, start_date, end_date);
                    // });
                    // }
                );
            }
        });

        $.ajax({
            url: "<?= base_url() ?>index.php/Cotd/view_depar_jam",
            type: "POST",
            data: {
                'start': start,
                'end': end,
                'tipe': aircraft,
                'aircraft': ac_type,
            },
            dataType: "JSON",
            beforeSend: function () {
                $('#loader4').fadeIn('fast');
            },
            success: function (data) {
                $('#loader4').fadeOut(1000);
                var chart4 = new Highcharts.chart('detail_delay_grafik4', {
                        title: {
                            text: ''
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            title: {
                                text: 'Delay Length (Hours)'
                            },
                            // min: 1,
                            // max: 6
                        },
                        xAxis: {
                            categories: data.kategori,
                            title: {
                                text: 'Airport'
                            }
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },

                        plotOptions: {
                            series: {
                                label: {
                                    connectorAllowed: false
                                }
                            }
                        },
                        colors: [
                            '#05354d',
                            '#f7ca78',
                            '#7bc8a4',
                            '#93648d',
                            '#4cc3d9',
                            '#1f7f91',
                            '#074b6d',
                            '#d18700',
                            '#ffdd9e',
                        ],


                        series: data.grafik,

                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }
                    },
                    // function(chart4){
                    // $('.highcharts-title').click(function(){
                    // var start_date = $("#date_start").val();
                    // var end_date = $("#date_end").val();
                    // tabel_detail_delay("2",aircraft, start_date, end_date);
                    // });
                    // }
                );
            }
        });
    }

    //master grafik
    function grafik_garuda() {
        var aircraft = [];
        var tes_nama = [];
        var aircraft = $('#aircraft').val();
        var tes_nama = $('#aircraft').text();
        // var aircraft     = aircraft.replace(/,/g, ' ');
        var start = $("#date_start").val();
        var end = $("#date_end").val();
        $.ajax({
            url: "<?= base_url() ?>index.php/Cotd/view_garuda",
            type: "POST",
            data: {
                'aircraft': aircraft,
                'start': start,
                'end': end,
            },
            dataType: "JSON",
            beforeSend: function () {
                $('#loaderGaruda').fadeIn(1000);
            },
            success: function (data) {
                $("#garuda_place").css('height', '100%');
                $('#loaderGaruda').fadeOut(1000);
                var chart5 = new Highcharts.chart('grafik_garuda', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            height: 60 + '%'
                        },
                        exporting: {
                            enabled: false
                        },
                        legend: {
                            itemStyle: {
                                fontSize: '13px',
                            }
                        },
                        title: {
                            text: ''
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            title: {
                                text: 'Constant'
                            },
                            labels: {
                                style: {
                                    fontSize: "13px"
                                }
                            },
                            min: 0,
                            max: 1.5,
                            plotLines: [{
                                value: data.target,
                                color: '#F2C573',
                                dashStyle: 'shortdash',
                                width: 2,
                                label: {
                                    text: 'Target ' + data.target
                                }
                            }],
                        },
                        xAxis: {
                            categories: data.grafik[0].datat,
                            labels: {
                                style: {
                                    fontSize: "13px"
                                }
                            },
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },

                        plotOptions: {
                            series: {
                                label: {
                                    connectorAllowed: false
                                },
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function () {
                                            detail_grafik(this.series.name, this.category);
                                        }
                                    }
                                }
                            }
                        },

                        series: [{
                            showInLegend: false,
                            name: 'Garuda',
                            color: '#05354D',
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 140],
                                stops: [
                                    [0, "#05354D"],
                                    [1, "#fff3"]
                                ]
                            },
                            dataLabels: {
                                enabled: true,
                                rotation: -90,
                                color: '#000',
                                align: 'right',
                                format: '{point.y:.2f}', // one decimal
                                y: -30, // 10 pixels down from the top
                            },
                            lineWidth: 1,
                            data: data.grafik[0].data
                        }],

                        // responsive: {
                        //   rules: [{
                        //     condition: {
                        //       maxWidth: 500
                        //     },
                        //     chartOptions: {
                        //       legend: {
                        //         layout: 'horizontal',
                        //         align: 'center',
                        //         verticalAlign: 'bottom'
                        //       }
                        //     }
                        //   }]
                        // }
                    },
                    // function(chart5){
                    // $('.highcharts-title').click(function(){
                    // var start_date = $("#date_start").val();
                    // var end_date = $("#date_end").val();
                    // detailDelayShow($(this).text(), start_date, end_date);
                    // });
                    // }
                );
            }

        });
    }

    function grafik_citilink() {

        var start = $("#date_start").val();
        var end = $("#date_end").val();
        var aircraft = [];
        var aircraft = $('#aircraft').val();
        $.ajax({
            url: "<?= base_url() ?>index.php/Cotd/view_citilink",
            type: "POST",
            data: {
                'aircraft': aircraft,
                'start': start,
                'end': end,
            },
            dataType: "JSON",
            beforeSend: function () {
                $('#loaderCitilink').fadeIn('fast');
            },
            success: function (data) {
                $("#citilink_place").css('height', '100%');
                $("#loaderCitilink").fadeOut(1000);
                var chart6 = new Highcharts.chart('grafik_citilink', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            height: 60 + '%'
                        },
                        exporting: {
                            enabled: false
                        },
                        title: {
                            text: ''
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            title: {
                                text: 'Constant'
                            },
                            labels: {
                                style: {
                                    fontSize: "13px"
                                }
                            },
                            min: 0,
                            max: 1.5,
                            plotLines: [{
                                value: data.target,
                                color: '#F2C573',
                                dashStyle: 'shortdash',
                                width: 2,
                                label: {
                                    text: 'Target ' + data.target
                                }
                            }],
                        },
                        xAxis: {
                            categories: data.grafik[0].datat,
                            labels: {
                                style: {
                                    fontSize: "13px"
                                }
                            },
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },

                        plotOptions: {
                            series: {
                                label: {
                                    connectorAllowed: false
                                },
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function () {
                                            detail_grafik(this.series.name, this.category);
                                        }
                                    }
                                }
                            }
                        },


                        series: [{
                            showInLegend: false,
                            name: 'Citilink',
                            color: '#05354D',
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 140],
                                stops: [
                                    [0, "#05354D"],
                                    [1, "#fff3"]
                                ]
                            },
                            dataLabels: {
                                enabled: true,
                                rotation: -90,
                                color: '#000',
                                align: 'right',
                                format: '{point.y:.2f}', // one decimal
                                y: -30, // 10 pixels down from the top
                            },
                            lineWidth: 1,
                            data: data.grafik[0].data
                        }],

                        // responsive: {
                        //   rules: [{
                        //     condition: {
                        //       maxWidth: 500
                        //     },
                        //     chartOptions: {
                        //       legend: {
                        //         layout: 'horizontal',
                        //         align: 'center',
                        //         verticalAlign: 'bottom'
                        //       }
                        //     }
                        //   }]
                        // }
                    },
                    // function(chart6){
                    // $('.highcharts-title').click(function(){
                    // var start_date = $("#date_start").val();
                    // var end_date = $("#date_end").val();
                    // detailDelayShow($(this).text(), start_date, end_date);

                    // });
                    // }
                );
            }

        });
    }

    function actual_garuda() {

        $.ajax({
            url: "<?= base_url() ?>index.php/Cotd/view_actual_garuda",
            type: "POST",
            data: {},
            dataType: "JSON",
            success: function (data) {

                $(".actual_garuda").html(data.actual);
                $(".target_garuda").html(data.target);
                $(".panah_garuda").html(data.panah);
            }

        });
    }

    function actual_citilink() {

        $.ajax({
            url: "<?= base_url() ?>index.php/Cotd/view_actual_citilink",
            type: "POST",
            data: {},
            dataType: "JSON",
            success: function (data) {
                $(".actual_citilink").html(data.actual);
                $(".target_citilink").html(data.target);
                $(".panah_citilink").html(data.panah);

            }

        });
    }


    function toggleSelectAll(control) {
        var allOptionIsSelected = (control.val() || []).indexOf("All") > -1;

        function valuesOf(elements) {
            return $.map(elements, function (element) {
                return element.value;
            });
        }

        if (control.data('allOptionIsSelected') != allOptionIsSelected) {
            // User clicked 'All' option
            if (allOptionIsSelected) {
                // Can't use .selectpicker('selectAll') because multiple "change" events will be triggered
                control.selectpicker('val', valuesOf(control.find('option')));
            } else {
                control.selectpicker('val', []);
            }
        } else {
            // User clicked other option
            if (allOptionIsSelected && control.val().length != control.find('option').length) {
                // All options were selected, user deselected one option
                // => unselect 'All' option
                control.selectpicker('val', valuesOf(control.find('option:selected[value!=All]')));
                allOptionIsSelected = false;
            } else if (!allOptionIsSelected && control.val().length == control.find('option').length - 1) {
                // Not all options were selected, user selected all options except 'All' option
                // => select 'All' option too
                control.selectpicker('val', valuesOf(control.find('option')));
                allOptionIsSelected = true;
            }
        }
        control.data('allOptionIsSelected', allOptionIsSelected);
    }


    $('#date_end, #date_start').datepicker({
        todayBtn: "linked",
        format: 'mm-yyyy',
        viewMode: 'months',
        minViewMode: 'months',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
</script>
