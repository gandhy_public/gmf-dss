<style type="text/css">
    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }

    .wadah {
        padding-left: 1px;
        padding-right: 0px;
    }

    .highcharts-title {
        cursor: pointer;
    }

    input.empty {
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        text-decoration: inherit;
    }

    .spinner {
        margin: 100px auto;
        width: 50px;
        height: 40px;
        text-align: center;
        font-size: 10px;
    }

    .spinner > div {
        background-color: #333;
        height: 100%;
        width: 6px;
        display: inline-block;
        -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
        animation: sk-stretchdelay 1.2s infinite ease-in-out;
    }

    .spinner .rect2 {
        -webkit-animation-delay: -1.1s;
        animation-delay: -1.1s;
    }

    .spinner .rect3 {
        -webkit-animation-delay: -1.0s;
        animation-delay: -1.0s;
    }

    .underlineclasss {
        text-decoration: underline;

    }

    .underlineclasss:hover {
        color: #f9e6b3;
    }

    .spinner .rect4 {
        -webkit-animation-delay: -0.9s;
        animation-delay: -0.9s;
    }

    .spinner .rect5 {
        -webkit-animation-delay: -0.8s;
        animation-delay: -0.8s;
    }

    @-webkit-keyframes sk-stretchdelay {
        0%, 40%, 100% {
            -webkit-transform: scaleY(0.4);
        }
        20% {
            -webkit-transform: scaleY(1.0);
        }
    }

    @keyframes sk-stretchdelay {
        0%, 40%, 100% {
            transform: scaleY(0.4);
            -webkit-transform: scaleY(0.4);
        }
        20% {
            transform: scaleY(1.0);
            -webkit-transform: scaleY(1.0);
        }
    }

    .stripe {
        white-space: nowrap;
        background-color: white;
    }

    tr {
        background-color: white;
    }

    tr:hover {
        background-color: #eaf3ff;
    }

    .hov:hover {
        color: #f7f7f7;
    }

    .dataTables_filter {
        margin-left: -28px;
    }

    .small-box {
        box-shadow: 0 8px 12px 4px rgba(0, 0, 0, 0.2);
        border-radius: 10px;
    }

    .widget-user .widget-user-image {
        position: absolute;
        top: 20px;
        left: 43%;
        margin-left: -45px;
    }

    .widget-user .widget-user-header {
        padding: 20px;
        height: 120px;
        border-top-right-radius: 11px;
        border-top-left-radius: 11px;
    }

    .widget-user .widget-user-image > img {
        width: 150px;
        height: auto;
        border: transparent;
    }

    .box-footer {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 11px;
        border-bottom-left-radius: 11px;
        border-top: 1px solid #f4f4f4;
        padding: 10px;
        background-color: #fff;
    }
    .title-manufacture {
        color: #F2C573;
        text-align: center;
        font-size: 25px;
        font-weight: bold;
        margin-top: 50px;
    }
    .description-block>.description-header {
        margin: 0;
        padding: 0;
        font-weight: 600;
        font-size: 25px;
    }

    .description-block > .description-text {
        text-transform: uppercase;
        font-size: 11px;
    }

    .modal {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 45px;
        z-index: 1050;
        display: none;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
    }

    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0e1f2e;
    }
</style>

<!-- <section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section> -->

<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
            <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body">
            <div class="row">
                <label for="period_start" class="col-lg-2 col-md-6 col-sm-12 col-xs-12 control-label">Material
                    Number</label>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <select class="form-control selectpicker" multiple id="aircraft" data-size="12" style="width: 100%"
                            title="- Select Material Number -" disabled>
                    </select>
                </div>
                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                    <button type="button" class="btn btn-flat bg-navy " id="seachby" name="seachby"><i
                                class="fa fa-search"></i> Search
                    </button>
                </div>
                <div class="col-sm-2"></div>
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label pull-right" style="font-weight: 100;"><i class="fa  fa-calendar"></i>
                        Year To Date : <?php echo date('d-M-Y'); ?></label>
                </div>
            </div>
        </div>
        <br>
        <div class="box-body">
            <div id="loadingApprove2" class="back-loading"
                 style="display: none;background: white;position: absolute;z-index: 9999;width: 99%;height: 100%;margin: 0 auto;left: 1;/* display: none; */left: 0;right: 0;/* top: 10px; */bottom: 0;/* padding:  50%; */padding top:0px;/* padding-top:  50px; */">
                <p style="text-align:  center;margin-top:  50px;font-size: 20px;">Data Diproses</p>
                <div class="spinner">
                    <div class="rect1"></div>
                    <div class="rect2"></div>
                    <div class="rect3"></div>
                    <div class="rect4"></div>
                    <div class="rect5"></div>
                </div>
            </div>
            <div class="row">

                <!--  LAYOUT LAMA
                  <div class="col-md-6">
                  <div class="form-group">
                    <div class="small-box bg-navy">
                      <div class="inner center">
                        <center><br>
                          <h3 style="color:#F2C573;"href="#tabel_detail" id="panel_garuda"title="Show Detail">Garuda</h3>
                          <hr>
                          <div class="row">
                            <div class="col-lg-4">
                              <h2 style="cursor:pointer;" class="md underlineclasss" data-nama="Garuda" data-id="Engine" >Engine</h2>
                            </div>
                            <div class="col-lg-4">
                              <h2 id="engine_avaibility_grd">10 Item</h2>
                              <p style="color:#F2C573">Available</p>
                            </div>
                            <div class="col-lg-4">
                              <h2 id="engine_navaibility_grd">10 Item</h2>
                              <p style="color:#F2C573">Not Available</p>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-4">
                              <h2 style="cursor:pointer;" class="md underlineclasss" data-nama="Garuda" data-id="APU">APU</h2>
                            </div>
                            <div class="col-lg-4">
                              <h2 id="apu_avaibility_grd">10 Item</h2>
                              <p style="color:#F2C573">Available</p>
                            </div>
                            <div class="col-lg-4">
                              <h2 id="apu_navaibility_grd">10 Item</h2>
                              <p style="color:#F2C573">Not Available</p>
                            </div>
                          </div>
                        </center>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="small-box bg-navy">
                      <div class="inner center">
                        <center><br>
                          <h3 style="color:#F2C573;"href="#tabel_detail" id="panel_garuda"title="Show Detail">Citilink</h3>
                          <hr>
                          <div class="row">
                            <div class="col-md-4">
                              <h2 style="cursor:pointer;"  class="md underlineclasss" data-nama="Citilink" data-id="Engine">Engine</h2>
                            </div>
                            <div class="col-md-4">
                              <h2 id="engine_avaibility_ct">10 Item</h2>
                              <p style="color:#F2C573">Available</p>
                            </div>
                            <div class="col-md-4">
                              <h2 id="engine_navaibility_ct">10 Item</h2>
                              <p style="color:#F2C573">Not Available</p>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-4">
                              <h2 style="cursor:pointer;" class="md underlineclasss" data-nama="Citilink" data-id="APU">APU</h2>
                            </div>
                            <div class="col-lg-4">
                              <h2 id="apu_avaibility_ct">10 Item</h2>
                              <p style="color:#F2C573">Available</p>
                            </div>
                            <div class="col-lg-4">
                              <h2 id="apu_navaibility_ct">10 Item</h2>
                              <p style="color:#F2C573">Not Available</p>
                            </div>
                          </div>
                        </center>
                      </div>
                    </div>
                  </div>
                </div>-->

                <div class="col-md-6">
                    <div class="box box-widget widget-user" style="border-radius: 11px;">
                        <div class="widget-user-header bg-navy">
                            <h3 class="title-manufacture">GARUDA</h3>
                        </div>
                        <div class="widget-user-image">
                            <img src="<?= base_url(); ?>/assets/dist/img/icon/logo.png" alt="User Avatar">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-6 border-right">
                                    <div class="col-sm-6 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header" id="engine_avaibility_grd">1</h5>
                                            <span class="description-text">Available</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="description-block">
                                            <h5 class="description-header" id="engine_navaibility_grd">0</h5>
                                            <span class="description-text">Not Available</span>
                                        </div>
                                    </div>
                                    <div class="description-block">
                                        <button class="md btn btn-sm  btn-primary" data-nama="Garuda" data-id="Engine"
                                                style="width: 100%"><span class="description-text"
                                                                          style="font-size: 20px;"><i
                                                        class="fa fa-gears"></i> ENGINE</span></button>
                                    </div>
                                </div>
                                <div class="col-sm-6 border-right">
                                    <div class="col-sm-6 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header" id="apu_avaibility_grd">0</h5>
                                            <span class="description-text">Available</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="description-block">
                                            <h5 class="description-header" id="apu_navaibility_grd">0</h5>
                                            <span class="description-text">Not Available</span>
                                        </div>
                                    </div>
                                    <div class="description-block">
                                        <button class="md btn btn-sm  btn-primary" data-nama="Garuda" data-id="APU"
                                                style="width: 100%"><span class="description-text"
                                                                          style="font-size: 20px;"><i
                                                        class="fa fa-gear"></i> APU</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-widget widget-user" style="border-radius: 11px;">
                        <div class="widget-user-header bg-navy">
                            <h3 class="title-manufacture">CITILINK</h3>
                        </div>
                        <div class="widget-user-image">
                            <img src="<?= base_url(); ?>/assets/dist/img/icon/logo.png" alt="User Avatar">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-6 border-right">
                                    <div class="col-sm-6 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header" id="engine_avaibility_ct"></h5>
                                            <span class="description-text">Available</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="description-block">
                                            <h5 class="description-header" id="engine_navaibility_ct"></h5>
                                            <span class="description-text">Not Available</span>
                                        </div>
                                    </div>
                                    <div class="description-block">
                                        <button class="md btn btn-sm  btn-primary" data-nama="Citilink" data-id="Engine"
                                                style="width: 100%"><span class="description-text"
                                                                          style="font-size: 20px;"><i
                                                        class="fa fa-gears"></i> ENGINE</span></button>
                                    </div>
                                </div>
                                <div class="col-sm-6 border-right">
                                    <div class="col-sm-6 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header" id="apu_avaibility_ct"></h5>
                                            <span class="description-text">Available</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="description-block">
                                            <h5 class="description-header" id="apu_navaibility_ct"></h5>
                                            <span class="description-text">Not Available</span>
                                        </div>
                                    </div>
                                    <div class="description-block">
                                        <button class="md btn btn-sm  btn-primary" data-nama="Citilink" data-id="APU"
                                                style="width: 100%"><span class="description-text"
                                                                          style="font-size: 20px;"><i
                                                        class="fa fa-gear"></i> APU</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="width: 1000px;">
        <div class="modal-content">
            <div class="modal-header" style="background: #001c39;color: #fff;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="title_modal" style="text-transform: uppercase;"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <center>
                            <h3 class="hov"
                                style="background-color:#8ec3a7;padding-top: 10px;padding-bottom: 10px;color:white;">
                                Serviceable</h3>
                        </center>
                        <input type="hidden" name="select_material_number" value="" id="select_material_number">
                        <table class="table table-bordered fixed" id="tbl_available" name="tbl_available" style="font-size: 12px;">
                            <thead>
                            <tr>
                                <th style="background-color: #05354D; color:white;">Mat Number</th>
                                <th style="background-color: #05354D; color:white;">Mat Description</th>
                                <th style="background-color: #05354D; color:white;">Plant</th>
                                <th style="background-color: #05354D; color:white;">Sloc</th>
                                <th style="background-color: #05354D; color:white;">Sloc Description</th>
                                <th style="background-color: #05354D; color:white;">Total</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="col-lg-6 " style="border-left:1px #e6e6e6 solid">
                        <center>
                            <h3 class="hov"
                                style="background-color:  #f24738;padding-top: 10px;padding-bottom: 10px; color:white; ">
                                Not Serviceable</h3>
                        </center>
                        <table class="table table-bordered" id="tbl_navailable" name="tbl_navailable"
                               style="font-size: 12px;">
                            <thead>
                            <tr>
                                <th style="background-color: #05354D; color:white;">Mat Number</th>
                                <th style="background-color: #05354D; color:white;">Mat Description</th>
                                <th style="background-color: #05354D; color:white;">Plant</th>
                                <th style="background-color: #05354D; color:white;">Sloc</th>
                                <th style="background-color: #05354D; color:white;">Sloc Description</th>
                                <th style="background-color: #05354D; color:white;">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i
                            class="fa fa-rotate-left"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        land();
        $('#iconified').on('keyup', function () {
            var input = $(this);
            if (input.val().length === 0) {
                input.addClass('empty');
            } else {
                input.removeClass('empty');
            }
        });

        // Start Setting  Datatable On Modal
        $('#myModal1').on('shown.bs.modal', function (e) {
            $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
        });
        // End Setting  Datatable On Modal

        // Start On click Function
        $(".md").click(function (event) {
            /* Act on the event */
            var id = $(this).data("id");
            var nm = $(this).data("nama");
            var sear = $("#select_material_number").val();
            $("#title_modal").text(nm + ' - ' + id);
            // $("#panel_name").text(id);
            datatables(id, nm, sear);
            $("#myModal1").modal('show');
        });
        // End On click Function

        // Start Initial Selected On First Load
        $('#aircraft').val('');
        $(".selectpicker").selectpicker("refresh");

        var list_actype;
        $.ajax({
            url: '<?= base_url() ?>index.php/Engine_APU_Spare_Availability/material',
            type: 'GET',
            dataType: 'JSON'
        })
            .done(function (resp) {
                console.log(resp);
                list_actype = '<select class="selectpicker" id="aircraft" data-live-search="false" multiple title="- Select Material Number -"><option value="All" nama="All" Selected>All</option>';
                $(resp.data).each(function (key, val) {
                    list_actype += '<option value="' + val.ex_long_mat_numb + '" nama="' + val.ex_long_mat_numb + '">' + val.ex_long_mat_numb + '</option>';
                });
                list_actype += '</select>';
                $('#aircraft').html(list_actype);
                $('#aircraft').removeAttr("disabled");
                $(".selectpicker").selectpicker("refresh");
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });
        // End Initial Selected On First Load

        // Start Js When Multiple Selected Action / Seach Action
        $("#seachby").click(function (event) {
            /* Act on the event */
            var search = $("#aircraft").val();
            // if (search == '') {
                // land();
            // } else {
                $.ajax({
                    url: '<?= base_url() ?>index.php/Engine_APU_Spare_Availability/searchby',
                    type: 'POST',
                    data: {param: search},
                    // beforeSend: function () {
                    //     $("#loadingApprove2").show();
                    // }
                })
                    .done(function (resp) {
                        notif('success', 'Data successfully displayed');
                        var data = JSON.parse(resp);
                        $("#select_material_number").val(search);
                        $("#engine_avaibility_grd").html(data.grd_engine_av + ' Item');
                        $("#engine_navaibility_grd").text(data.grd_engine_nav + ' Item');
                        $("#apu_avaibility_grd").text(data.grd_apu_av + ' Item');
                        $("#apu_navaibility_grd").text(data.grd_apu_nav + ' Item');
                        $("#engine_avaibility_ct").text(data.ct_engine_av + ' Item');
                        $("#engine_navaibility_ct").text(data.ct_engine_nav + ' Item');
                        $("#apu_avaibility_ct").text(data.ct_apu_av + ' Item');
                        $("#apu_navaibility_ct").text(data.ct_apu_nav + ' Item');
                        console.log("success");
                    })
                    .fail(function () {
                        console.log("error");
                    })
                    .always(function () {
                        console.log("complete");
                        // $("#loadingApprove2").hide('slow/400/slow', function () {
                        // });
                    });
            // }
        });
        // End Js When Multiple Selected Action / Seach Action

        // Start Js Multiple Select
        $('#aircraft').selectpicker().change(function () {
            toggleSelectAll($(this));
        }).trigger('change');

        function toggleSelectAll(control) {
            var allOptionIsSelected = (control.val() || []).indexOf("All") > -1;

            function valuesOf(elements) {
                return $.map(elements, function (element) {
                    return element.value;
                });
            }

            if (control.data('allOptionIsSelected') != allOptionIsSelected) {
                // User clicked 'All' option
                if (allOptionIsSelected) {
                    // Can't use .selectpicker('selectAll') because multiple "change" events will be triggered
                    control.selectpicker('val', valuesOf(control.find('option')));
                } else {
                    control.selectpicker('val', []);
                }
            } else {
                // User clicked other option
                if (allOptionIsSelected && control.val().length != control.find('option').length) {
                    // All options were selected, user deselected one option
                    // => unselect 'All' option
                    control.selectpicker('val', valuesOf(control.find('option:selected[value!=All]')));
                    allOptionIsSelected = false;
                } else if (!allOptionIsSelected && control.val().length == control.find('option').length - 1) {
                    // Not all options were selected, user selected all options except 'All' option
                    // => select 'All' option too
                    control.selectpicker('val', valuesOf(control.find('option')));
                    allOptionIsSelected = true;
                }
            }
            control.data('allOptionIsSelected', allOptionIsSelected);
        }

        // End Js Multiple Select

        // Start Ajax When Landing Page
        function land() {
            var search = $("#aircraft").val();
            $.ajax({
                url: '<?= base_url() ?>index.php/Engine_APU_Spare_Availability/searchby', //landing
                type: 'POST',
                data: {param: search},
                // beforeSend: function () {
                //     $("#loadingApprove2").show();
                //     console.log("loading");
                // }
            })
                .done(function (resp) {
                    notif('success', 'Data successfully displayed');
                    var data = JSON.parse(resp);
                    $("#select_material_number").val(search);
                    $("#engine_avaibility_grd").html(data.grd_engine_av + ' Item');
                    $("#engine_navaibility_grd").text(data.grd_engine_nav + ' Item');
                    $("#apu_avaibility_grd").text(data.grd_apu_av + ' Item');
                    $("#apu_navaibility_grd").text(data.grd_apu_nav + ' Item');
                    $("#engine_avaibility_ct").text(data.ct_engine_av + ' Item');
                    $("#engine_navaibility_ct").text(data.ct_engine_nav + ' Item');
                    $("#apu_avaibility_ct").text(data.ct_apu_av + ' Item');
                    $("#apu_navaibility_ct").text(data.ct_apu_nav + ' Item');

                    console.log("success");
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                    // $("#loadingApprove2").hide('slow/400/slow', function () {
                    // });
                });
        }
    });
    // End Ajax When Landing Page

    // Start Func Datatable
    function datatables(id, nm, sear) {

        $('#tbl_available').DataTable().destroy();
        var table = $('#tbl_available').DataTable({
            "processing": true,
            "serverSide": true,
            scrollY: "300px",
            scrollX: "100%",
            "sScrollXInner": "150%",
            scrollCollapse: true,
            ordering: false,
            paging: true,
            fixedColumns: {
                leftColumns: 1
            },
            // columnDefs: [{width: 125, targets: 0}],
            "ajax": {
                "url": '<?= base_url() ?>index.php/Engine_APU_Spare_Availability/avability',
                "type": 'POST',
                "data": {
                    'param1': id,
                    'param2': nm,
                    'param3': sear
                }
            }
        });

        $('#tbl_navailable').DataTable().destroy();
        $("#tbl_navailable").DataTable({
            "processing": true,
            "serverSide": true,
            scrollY: "300px",
            scrollX: "100%",
            "sScrollXInner": "150%",
            scrollCollapse: true,
            paging: true,
            ordering: false,
            fixedColumns: {
                leftColumns: 1
            },
            // columnDefs: [
            //     {width: 125, targets: 0}
            // ],
            "ajax": {
                "url": '<?= base_url() ?>index.php/Engine_APU_Spare_Availability/nonavability',
                "type": 'POST',
                "data": {
                    'param1': id,
                    'param2': nm,
                    'param3': sear
                }
            }
        });
    }

    // END Func Datatable
</script>
