<style>
  .wadah{
    padding-left: 5px;
    padding-right: 5px;
  }
  .center{
    text-align: center;
  }
</style>

<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>


<section class="content">
	<div class="box">
		<div class="box-header with-border">
		  <h3 class="box-title"><?= $title ?></h3>
		  <div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
					title="Collapse">
			  <i class="fa fa-minus"></i></button>
		  </div>
		</div>
		<div class="box-body">
		  <div class="row">
        <div class="col-md-12">
          <div class="button-add-place pull-left">
            <a class="btn btn-flat bg-navy" onclick="delete_data()" name="deleteUpload">Delete All <i class="fa fa-trash"></i></a>
            </a>
          </div>
          <div class="button-add-place pull-right">
            <a class="btn btn-flat bg-navy" name="button" onclick="upload_data()">
             Upload Data <i class="fa fa-cloud-upload"></i>
            </a>
          </div>
        </div>
      </div>
		  <div class="row">
        <div class="col-md-12">
          <table class="table table-hover table-gmf" id="table-finance" style="width: 100%">
            <thead style="background-color: #05354D">
              <tr>
                <th>No</th>
                <th>Nama File Upload</th>
                <th>Upload By</th>
                <th>Waktu Upload</th>
                <th>Action</th>
                <!-- <th>Indikator</th>
                <th>Tipe</th>
                <th>Dinas</th>
                <th class="center">Month</th>
                <th class="center">Year</th>
                <th>Value</th> -->
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
		  </div>
		</div>
	</div>
</section>

<script>
  var table;
  $(document).ready(function () 
  {
    table = $('#table-finance').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "ordering": false,
      "dom": 'lrtip',
      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": "<?php echo site_url('index.php/finance/list_finance') ?>",
        "type": "POST"
      },
    });
    $('#formModal').on('submit', function (e) 
    {
      $('#button-save').text('Processing...'); //change button text
      $('#button-save').attr('disabled', true); //set button disable 
      e.preventDefault();
      var formData = new FormData($('#formModal')[0]);
      $.ajax({
        url: "<?php echo site_url('index.php/finance/upload_proses') ?>",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        error: function(){
          notif('error', 'Connection disorders occurred, Please try again');
        },
        success: function (data) {
          $('.content').append(data);
          /*$('#modal_add').modal('hide');
          $(this).closest('.modal').modal('toggle');
          notif('success', 'Data successfully saved');
          reload_table();   
          $('#files').val('');*/          
        },
        error: function (jqXHR, textStatus, errorThrown) {
          alert("Error !!!");
        }
      });
    });
  });

  function reload_table()
  {
    table.ajax.reload(null, false); //reload datatable ajax 
  }

  function upload_data(){
    $('#modal_add').modal('show');
    $('#button-save').html('<i class="fa fa-save"></i> Save'); //change button text
    $('#button-save').attr('disabled', false); //set button disable 
  }

  function delete_data(){
    if (confirm('Are you sure delete all data?'))
        {
            // ajax delete data to database
            $.ajax({
                url: "<?php echo site_url('index.php/finance/delete_all') ?>",
                type: "POST",
                dataType: "JSON",
                error: function(){
                  notif('error', 'Connection disorders occurred, Please try again');
                },
                success: function (data)
                {
                  notif('success', 'Data successfully deleted');
                  reload_table();   
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error');
                }
            });
        }
  }

 function download_format(){
    window.location.href = "<?php echo base_url() ?>assets/Finance/Template_DSS_Finance_V3.xlsx";
 }
</script>

<div class="modal fade" id="modal_add" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0f2233">
        <h4 class="modal-title" id="title-edit" style="color: #fff">Upload Data Finance</h4>
      </div>
      <div class="modal-body">
        <form id="formModal" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label">Upload Data</label>
              <div class="col-sm-9">
              <input type="file" class="form-control" name="files" id="files">
              </div>
            </div>
            <br><br>
            <div class="form-group col-sm-12">
              <a href="<?php echo base_url() ?>assets/Finance/Template_DSS_Finance_V3.xlsx" style="cursor: pointer;">Download Format Upload <i class="fa fa-cloud-download"></i></a>
            </div>
          </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-flat bg-navy" id="button-save"> Save</button>
            <button type="button" class="pull-right btn btn-flat btn-default"  data-dismiss="modal"><i class="fa fa-rotate-left"></i> Cancel</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>