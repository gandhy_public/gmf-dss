<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style>
    .wadah{
        /* padding-left: 5px;
        padding-right: 5px; */
        padding: 0;
    }
    .wadah h4{
        font-size: 14px;
    }
    .wadah p{
        font-size: 13px;
        margin-top: -4px;
    }
    .value-panel{
        margin-top: -10px;
        line-height: 1px;
    }
    .text-red{
        color:#f24738;
    }
    .text-center{
        text-align: -webkit-right;
    }
    .label-title{
        font-weight: bold;
        text-align: center;
        font-size: 15px;
        margin-top: 3px;
    }
    .table-gmf thead {
        background-color: grey;
        color: #fff;
    }
    .col-lg-3 {
        width: 25%;
        padding-left: 3px;
        padding-right: 0px;
    }
    .col-lg-4 {
        width: 31.333333%;
    }
    .form-control {
        display: block;
        width: 93%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
    }
    .box-body {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        padding: 0px;
        line-height: 1;
    }
    .form-group {
        margin-bottom: 0px;
    }
    .direct-chat-text {
        position: relative;
        padding: 0px 0px;
        background: #ecf0f5;
        border: 1px solid #d2d6de;
        margin: 8px 9px 0px 4px;
        color: #444;
        border-radius: 0px;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 5px;
        line-height: 1.3;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .bg-green, .callout.callout-success, .alert-success, .label-success, .modal-success .modal-body {
        background-color: grey !important;
    }
</style>

<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Financial Dashboard</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row" style="margin: 0">
                <div class="box-header with-border" style="border-bottom: 1px solid #e2d5d5;">
                    <div class="wadah col-lg-7 col-md-7 col-sm-12 col-xs-12">
                        <div class="box-header with-border" style="border-right: 1px solid #e2d5d5;border-bottom: 0px solid #fff;padding: 3px;">
                            <!--FILTER UTAMA-->
                            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12" id="wadah-mtd-ytd">
                                <div class="form-group">
                                    <select class="form-control" id="mtd_ytd">
                                        <option value="MTD">MTD</option>
                                        <option value="YTD" selected>YTD</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12" id="wadah-tahun">
                                <div class="form-group">
                                    <select class="form-control" id="tahun"></select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12" id="wadah-bulan">
                                <div class="form-group">
                                    <select class="form-control" id="bulan"></select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12" id="wadah-comparison">
                                <div class="form-group">
                                    <select class="form-control" id="comparison">
                                        <option value="Budget" selected>Budget</option>
                                        <option value="Rofo" >Rofo</option>
                                    </select>
                                </div>
                            </div>
                            <!--REVENUE-->
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 direct-chat-text">
                                <div class="form-group" style="text-align: center">
                                    <h3 class="label-title">REVENUE</h3>
                                    <div class="wadah col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p id="comparison-revenue">Budget</p>
                                        <div class="inner value-panel">
                                            <h4 id="budget-revenue" style="font-weight: bold;"></h4>
                                            <h4 id="rofo-revenue" style="font-weight: bold;"></h4>
                                            <p id="satuan-budget-revenue">MUSD</p>
                                            <p id="satuan-rofo-revenue">MUSD</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p>Actual</p>
                                        <div class="inner value-panel">
                                            <h4 id="actual-revenue" style="font-weight: bold;margin-left: 0px;"></h4>
                                            <p id="satuan-actual-revenue">MUSD</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12" style="padding-left: 30px;">
                                        <p style="margin-left: -5px;">Ach</p>
                                        <div class="inner value-panel">
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-budget-revenue"></h4>
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-rofo-revenue"></h4>
                                            <p style="margin-left: -15px;">Percent</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--EBITDA-->
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 direct-chat-text">
                                <div class="form-group" style="text-align: center">
                                    <h3 class="label-title">EBITDA</h3>
                                    <div class="wadah col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p id="comparison-ebitda">Budget</p>
                                        <div class="inner value-panel">
                                            <h4 id="budget-ebitda" style="font-weight: bold;"></h4>
                                            <h4 id="rofo-ebitda" style="font-weight: bold;"></h4>
                                            <p id="satuan-budget-ebitda">MUSD</p>
                                            <p id="satuan-rofo-ebitda">MUSD</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p>Actual</p>
                                        <div class="inner value-panel">
                                            <h4 id="actual-ebitda" style="font-weight: bold;margin-left: 0px;"></h4>
                                            <p id="satuan-actual-ebitda">MUSD</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12" style="padding-left: 30px;">
                                        <p style="margin-left: -5px;">Ach</p>
                                        <div class="inner value-panel">
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-budget-ebitda"></h4>
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-rofo-ebitda"></h4>
                                            <p style="margin-left: -15px;">Percent</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--NETT PROFIT-->
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 direct-chat-text">
                                <div class="form-group" style="text-align: center">
                                    <h3 class="label-title">NET PROFIT</h3>
                                    <div class="wadah col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p id="comparison-nett-profit">Budget</p>
                                        <div class="inner value-panel">
                                            <h4 id="budget-nett-profit" style="font-weight: bold;"></h4>
                                            <h4 id="rofo-nett-profit" style="font-weight: bold;"></h4>
                                            <p id="satuan-budget-nett-profit">MUSD</p>
                                            <p id="satuan-rofo-nett-profit">MUSD</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p>Actual</p>
                                        <div class="inner value-panel">
                                            <h4 style="font-weight: bold;margin-left: 0px;" id="actual-nett-profit"></h4>
                                            <p id="satuan-actual-nett-profit">MUSD</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12" style="padding-left: 30px;">
                                        <p style="margin-left: -5px;">Ach</p>
                                        <div class="inner value-panel">
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-budget-nett-profit"></h4>
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-rofo-nett-profit"></h4>
                                            <p style="margin-left: -15px;">Percent</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--EXPENSE-->
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 direct-chat-text">
                                <div class="form-group" style="text-align: center">
                                    <h3 class="label-title">EXPENSE</h3>
                                    <div class="wadah col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p id="comparison-expense">Budget</p>
                                        <div class="inner value-panel">
                                            <h4 id="budget-expense" style="font-weight: bold;"></h4>
                                            <h4 id="rofo-expense" style="font-weight: bold;"></h4>
                                            <p id="satuan-budget-expense">MUSD</p>
                                            <p id="satuan-rofo-expense">MUSD</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p>Actual</p>
                                        <div class="inner value-panel">
                                            <h4 id="actual-expense" style="font-weight: bold;margin-left: 0px;"></h4>
                                            <p id="satuan-actual-expense">MUSD</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12" style="padding-left: 30px;">
                                        <p style="margin-left: -5px;">Ach</p>
                                        <div class="inner value-panel">
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-budget-expense"></h4>
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-rofo-expense"></h4>
                                            <p style="margin-left: -15px;">Percent</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--EBITDA MARGIN-->
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 direct-chat-text">
                                <div class="form-group" style="text-align: center">
                                    <h3 class="label-title">EBITDA MARGIN</h3>
                                    <div class="wadah col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p id="comparison-ebitda-margin">Budget</p>
                                        <div class="inner value-panel">
                                            <h4 id="budget-ebitda-margin" style="font-weight: bold;"></h4>
                                            <h4 id="rofo-ebitda-margin" style="font-weight: bold;"></h4>
                                            <p>Percent</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p>Actual</p>
                                        <div class="inner value-panel">
                                            <h4 style="font-weight: bold;margin-left: 0px;" id="actual-ebitda-margin"></h4>
                                            <p>Percent</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12" style="padding-left: 30px;">
                                        <p style="margin-left: -5px;">Ach</p>
                                        <div class="inner value-panel">
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-rofo-ebitda-margin"></h4>
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-budget-ebitda-margin"></h4>
                                            <p style="margin-left: -15px;">Percent</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--NPM-->
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 direct-chat-text">
                                <div class="form-group" style="text-align: center">
                                    <h3 class="label-title">NPM</h3>
                                    <div class="wadah col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p id="comparison-npm">Budget</p>
                                        <div class="inner value-panel">
                                            <h4 id="budget-npm" style="font-weight: bold;"></h4>
                                            <h4 id="rofo-npm" style="font-weight: bold;"></h4>
                                            <p>Percent</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12">
                                        <p>Actual</p>
                                        <div class="inner value-panel">
                                            <h4 style="font-weight: bold;margin-left:0px;" id="actual-npm"></h4>
                                            <p>Percent</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-2 col-sm-12 col-xs-12" style="padding-left: 30px;">
                                        <p style="margin-left: -5px;">Ach</p>
                                        <div class="inner value-panel">
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-budget-npm"></h4>
                                            <h4 style="font-weight: bold;margin-left: -15px;" id="ach-rofo-npm"></h4>
                                            <p style="margin-left: -15px;">Percent</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--TABLE SEGMENT-->
                    <div class="wadah col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <div class="box-header with-border" style="border-right: 0px solid #fff;border-bottom: 1px solid transparent;padding: 0px;margin-bottom: -25px;">
                            <div class="row" style="margin: 0; padding: 0 15px">
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="padding:0;margin-right: 14px;box-sizing: border-box">
                                    <div class="form-group">
                                        <select class="form-control" id="mtd_ytd_yte" style="width: 100%;">
                                            <option value="MTD-TABEL">MTD</option>
                                            <!-- <option value="YTD-TABEL" >YTD</option> -->
                                            <option value="YTE-TABEL" selected>YTE</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="padding:0;margin-right: 14px">
                                    <div class="form-group">
                                        <select class="form-control" id="combo-recovery" style="width: 100%;box-sizing: border-box">
                                            <option value="1" selected>Revenue</option>
                                            <option value="2">Ebitda</option>
                                            <!--<option value="02" >Ebitda Margin</option>-->
                                            <option value="3" >Operating Profit</option>
                                            <!--<option value="04" >OPM</option>-->
                                            <option value="4" >Expense</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="padding: 0">
                                    <div class="form-group">
                                        <select class="form-control" id="combo-month" style="width: 100%;box-sizing: border-box">
                                            <?php
                                            $nama_bulan = [
                                                '01'=>'Januari','02'=>'Februari','03'=>'Maret',
                                                '04'=>'April','05'=>'Mei','06'=>'Juni',
                                                '07'=>'Juli','08'=>'Agustus','09'=>'September',
                                                '10'=>'Oktober','11'=>'November','12'=>'Desember'
                                            ];
                                            for($a=str_replace("0","",date('m'));$a<=12;$a++){
                                                if($a<10) $a = '0'.$a;
                                                echo "<option value='$a'>$nama_bulan[$a]</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin: 0">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <table class="table table-hover table-gmf" id="table" style="font-size: 12px">
                                        <thead>
                                            <tr>
                                                <th>B/U</th>
                                                <th style="text-align: center">ROFO</th>
                                                <th style="text-align: center">REQUIRED</th>
                                                <th style="text-align: center">GAP</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Base Maintenance</td>
                                                <td style="text-align: center" id="bm1"></td>
                                                <td style="text-align: center" id="bm2"></td>
                                                <td style="text-align: center; font-weight: 900" id="bm3" ></td>
                                            </tr>
                                            <tr>
                                                <td>Line Maintenance</td>
                                                <td style="text-align: center" id="lm1"></td>
                                                <td style="text-align: center" id="lm2"></td>
                                                <td style="text-align: center; font-weight: 900" id="lm3"></td>
                                            </tr>
                                            <tr>
                                                <td>Component</td>
                                                <td style="text-align: center" id="c1"></td>
                                                <td style="text-align: center" id="c2"></td>
                                                <td style="text-align: center; font-weight: 900" id="c3"></td>
                                            </tr>
                                            <tr>
                                                <td>Engine</td>
                                                <td style="text-align: center" id="e1"></td>
                                                <td style="text-align: center" id="e2"></td>
                                                <td style="text-align: center; font-weight: 900" id="e3"></td>
                                            </tr>

                                            <tr>
                                                <td>Others</td>
                                                <td style="text-align: center" id="o1"></td>
                                                <td style="text-align: center" id="o2"></td>
                                                <td style="text-align: center; font-weight: 900" id="o3"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--GRAFIK FINANCE-->
                <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="box-header with-border" style="border-top: 0px solid #fff;padding: 15px 0px">
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" style="margin-bottom: 8px">
                                <select class="form-control" id="financial-parameter">
                                    <option value="Revenue" selected>Revenue</option>
                                    <option value="Ebitda">Ebitda</option>
                                    <!-- <option value="Ebitda Margin">Ebitda Margin</option> -->
                                    <option value="Nett Profit">Net Profit</option>
                                    <!-- <option value="NPM">Nett Profit Margin</option> -->
                                    <option value="Expense">Expense</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12"></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<!--                             <label class="control-label" style="font-weight: 100;">
                                
                            </label> -->
                            <span class="label label-success" id="nilai-max" style="font-size: 100%;display: inline-block">
                                <i class="fa fa-money"></i> Year End Gap : 0 MUSD
                            </span>
<!--                             <div class="small-box bg-green" style="background-color: #808080 !important;">
                                <div class="inner">
                                    <p style="font-family: sans-serif;">Year End Gap</p>
                                    <h3 style="margin: 0px 5px 10px 20px;font-size: 20px;" id="nilai-max"></h3>
                                    <p style="font-family: sans-serif;margin-left: 18px;" id="msud-grafik"></p>
                                </div>
                            </div> -->
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div id="grafik" style="height: 380px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var url_api_findash = '<?php echo base_url(); ?>index.php/FinDashGw/index/';
    var url_api_findata = '<?php echo base_url(); ?>index.php/FinDashGw/findata/';
    var url_api_table   = '<?php echo base_url(); ?>index.php/FinDashGw/findata_table/';
    var date = new Date();
    var month = new Array();
    month[0] = "001";
    month[1] = "002";
    month[2] = "003";
    month[3] = "004";
    month[4] = "005";
    month[5] = "006";
    month[6] = "007";
    month[7] = "008";
    month[8] = "009";
    month[9] = "010";
    month[10] = "011";
    month[11] = "012";
    month[12] = "013";
    month[13] = "014";
    month[14] = "015";
    month[15] = "016";

    var bulan       = month[(date.getUTCMonth() == 0) ? 11 : (date.getUTCMonth() - 1)];
    var bulan_jalan = month[date.getUTCMonth()];
    var tahun       = (date.getUTCMonth() == 0) ? date.getUTCFullYear() - 1 : date.getUTCFullYear();

    function nFormatter(num_s, digits) {
        num = Math.abs(num_s);
        /*var si = [
            {value: 1, symbol: ""},
            {value: 1E3, symbol: "k"},
            {value: 1E6, symbol: "M"},
            {value: 1E9, symbol: "G"},
            {value: 1E12, symbol: "T"},
            {value: 1E15, symbol: "P"},
            {value: 1E18, symbol: "E"}
        ];
        */
        var si = [
            {value: 1, symbol: ""},
            {value: 1E3, symbol: ""},
            {value: 1E6, symbol: ""},
            {value: 1E9, symbol: ""},
            {value: 1E12, symbol: ""},
            {value: 1E15, symbol: ""},
            {value: 1E18, symbol: ""}
        ];
        var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
        var i;
        for (i = si.length - 1; i > 0; i--) {
            if (num >= si[i].value) {
                break;
            }
        }
        return ((num_s < 0) ? '-' : '') + (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
    }

    $(document).ready(function () {
        combo_tahun();
        combo_bulan();
        change_combo();


        grafik(tahun);
        
        income_statement(bulan, tahun);
        table();

        $('#financial-parameter').change(function () {
            for(var a = 0;a < promises.length; a++){
                if(promises[a].status!=200)
                promises[a].abort();
            }

            for(var a = 0;a < promises_jenis.length; a++){
                if(promises_jenis[a].status!=200)
                promises_jenis[a].abort();
            }
            var tahun = $('#tahun').val();
            grafik(tahun);
        });

        $('#mtd_ytd').change(function () {
            var tahun = $('#tahun').val();
            var bulan = $('#bulan').val();
            grafik(tahun);
            income_statement(bulan, tahun);
        });
    });

    function combo_tahun() {
        $.ajax({
            url: url_api_findash + 'financialdash/period_years',
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                var combo_tahun = '';
                for (var thn in data.items) {
                    combo_tahun = '<option value="' + data.items[thn] + '" ' + ((data.items[thn] == tahun) ? 'selected' : '') + '>' + data.items[thn] + '</option>' + combo_tahun;
                }
                $('#tahun').html(combo_tahun);
            }
        });
    }

    function combo_bulan() {
        $.ajax({
            url: url_api_findash + 'financialdash/period_months',
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                var combo_bulan = '';
                for (var bln = 0; bln < data.items.length - 1; bln++) {
                    combo_bulan = '<option value="' + data.items[bln] + '" ' + ((data.items[bln] == bulan) ? 'selected' : '') + '>' + data.items[bln] + '</option>' + combo_bulan;
                }
                $('#bulan').html(combo_bulan);
            }
        });
    }

    function change_combo() {
        $('#budget-revenue').show();
        $('#satuan-budget-revenue').show();
        $('#budget-ebitda').show();
        $('#satuan-budget-ebitda').show();
        $('#budget-nett-profit').show();
        $('#satuan-budget-nett-profit').show();
        $('#budget-expense').show();
        $('#satuan-budget-expense').show();
        $('#budget-ebitda-margin').show();
        $('#budget-npm').show();

        $('#ach-budget-revenue').show();
        $('#ach-budget-expense').show();
        $('#ach-budget-npm').show();
        $('#ach-budget-nett-profit').show();
        $('#ach-budget-ebitda').show();
        $('#ach-budget-ebitda-margin').show();

        $('#rofo-revenue').hide();
        $('#satuan-rofo-revenue').hide();
        $('#rofo-ebitda').hide();
        $('#satuan-rofo-ebitda').hide();
        $('#rofo-nett-profit').hide();
        $('#satuan-rofo-nett-profit').hide();
        $('#rofo-expense').hide();
        $('#satuan-rofo-expense').hide();
        $('#rofo-ebitda-margin').hide();
        $('#rofo-npm').hide();

        $('#ach-rofo-revenue').hide();
        $('#ach-rofo-expense').hide();
        $('#ach-rofo-npm').hide();
        $('#ach-rofo-nett-profit').hide();
        $('#ach-rofo-ebitda').hide();
        $('#ach-rofo-ebitda-margin').hide();

        $('#comparison').change(function () {
            var comparison = $('#comparison').val();
            $('#budget-revenue').toggle();
            $('#satuan-budget-revenue').toggle();
            $('#budget-ebitda').toggle();
            $('#satuan-budget-ebitda').toggle();
            $('#budget-nett-profit').toggle();
            $('#satuan-budget-nett-profit').toggle();
            $('#budget-expense').toggle();
            $('#satuan-budget-expense').toggle();
            $('#budget-ebitda-margin').toggle();
            $('#budget-npm').toggle();

            $('#rofo-revenue').toggle();
            $('#satuan-rofo-revenue').toggle();
            $('#rofo-ebitda').toggle();
            $('#satuan-rofo-ebitda').toggle();
            $('#rofo-nett-profit').toggle();
            $('#satuan-rofo-nett-profit').toggle();
            $('#rofo-expense').toggle();
            $('#satuan-rofo-expense').toggle();
            $('#rofo-ebitda-margin').toggle();
            $('#rofo-npm').toggle();

            $('#ach-budget-revenue').toggle();
            $('#ach-budget-expense').toggle();
            $('#ach-budget-npm').toggle();
            $('#ach-budget-nett-profit').toggle();
            $('#ach-budget-ebitda').toggle();
            $('#ach-budget-ebitda-margin').toggle();

            $('#ach-rofo-revenue').toggle();
            $('#ach-rofo-expense').toggle();
            $('#ach-rofo-npm').toggle();
            $('#ach-rofo-nett-profit').toggle();
            $('#ach-rofo-ebitda').toggle();
            $('#ach-rofo-ebitda-margin').toggle();

            if (comparison == 'Rofo') {
                $('#comparison-revenue').html('Rofo');
                $('#comparison-ebitda').html('Rofo');
                $('#comparison-nett-profit').html('Rofo');
                $('#comparison-expense').html('Rofo');
                $('#comparison-ebitda-margin').html('Rofo');
                $('#comparison-npm').html('Rofo');
            } else {
                $('#comparison-revenue').html('Budget');
                $('#comparison-ebitda').html('Budget');
                $('#comparison-nett-profit').html('Budget');
                $('#comparison-expense').html('Budget');
                $('#comparison-ebitda-margin').html('Budget');
                $('#comparison-npm').html('Budget');
            }
        });

        $('#tahun').change(function () {
            var tahun = $('#tahun').val();
            $.ajax({
                url: url_api_findash + 'financialdash/period_months/' + tahun,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    var combo_bulan = '';
                    for (var bln = 0; bln < data.items.length - 1; bln++) {
                        combo_bulan = '<option value="' + data.items[bln] + '" ' + ((data.items[bln] == bulan) ? 'selected' : '') + '>' + data.items[bln] + '</option>' + combo_bulan;
                    }
                    $('#bulan').html(combo_bulan);
                }
            });
            grafik(tahun);
        });

        $('#tahun, #bulan').change(function () {
            var tahun = $('#tahun').val();
            var bulan = $('#bulan').val();
            income_statement(bulan, tahun);
        });
    }

    function income_statement(bulan, tahun) {
        var mtd_ytd             = $('#mtd_ytd').val();
        var actual_revenue      = 0.0, actual_expense = 0.0, actual_npm = 0.0, actual_net_profit = 0.0;
        var budget_revenue      = 0.0, budget_expense = 0.0, budget_npm = 0.0, budget_net_profit = 0.0;
        var rofo_revenue        = 0.0, rofo_expense = 0.0, rofo_npm = 0.0, rofo_net_profit = 0.0;
        var ach_budget_revenue  = 0.0, ach_budget_expense = 0.0, ach_budget_npm = 0.0, ach_budget_net_profit = 0.0;
        var ach_rofo_revenue    = 0.0, ach_rofo_expense = 0.0, ach_rofo_npm = 0.0, ach_rofo_net_profit = 0.0;

        var actual_ebitda       = 0.0, actual_ebitda_margin = 0.0;
        var budget_ebitda       = 0.0, budget_ebitda_margin = 0.0;
        var rofo_ebitda         = 0.0, rofo_ebitda_margin = 0.0;
        var ach_budget_ebitda   = 0.0, ach_budget_ebitda_margin = 0.0;
        var ach_rofo_ebitda     = 0.0, ach_rofo_ebitda_margin = 0.0;

        $('#actual-revenue').html(actual_revenue);
        $('#budget-revenue').html(budget_revenue);
        $('#rofo-revenue').html(rofo_revenue);
        $('#ach-budget-revenue').html(ach_budget_revenue);
        $('#ach-rofo-revenue').html(ach_rofo_revenue);

        $('#actual-expense').html(actual_expense);
        $('#budget-expense').html(budget_expense);
        $('#rofo-expense').html(rofo_expense);
        $('#ach-budget-expense').html(ach_budget_expense);
        $('#ach-rofo-expense').html(ach_rofo_expense);

        $('#actual-npm').html(actual_npm);
        $('#budget-npm').html(budget_npm);
        $('#rofo-npm').html(rofo_npm);
        $('#ach-budget-npm').html(ach_budget_npm);
        $('#ach-rofo-npm').html(ach_rofo_npm);

        $('#actual-nett-profit').html(actual_net_profit);
        $('#budget-nett-profit').html(budget_net_profit);
        $('#rofo-nett-profit').html(rofo_net_profit);
        $('#ach-budget-nett-profit').html(ach_budget_net_profit);
        $('#ach-rofo-nett-profit').html(ach_rofo_net_profit);

        $('#actual-ebitda').html(actual_ebitda);
        $('#budget-ebitda').html(budget_ebitda);
        $('#rofo-ebitda').html(rofo_ebitda);
        $('#ach-budget-ebitda').html(ach_budget_ebitda);
        $('#ach-rofo-ebitda').html(ach_rofo_ebitda);

        $('#actual-ebitda-margin').html(actual_ebitda_margin);
        $('#budget-ebitda-margin').html(budget_ebitda_margin);
        $('#rofo-ebitda-margin').html(rofo_ebitda_margin);
        $('#ach-budget-ebitda-margin').html(ach_budget_ebitda_margin);
        $('#ach-rofo-ebitda-margin').html(ach_rofo_ebitda_margin);

        var promises    = [];
        var request     = $.ajax({
            url: url_api_findata + '0/' + tahun + '/' + bulan,
            type: "GET",
            dataType: "JSON",
            error: function(){
                notif('error', 'Connection disorders occurred, Please try again');
            },
            success: function (data) {
                notif('success', 'Data successfully displayed');
                if (mtd_ytd == 'MTD') {
                    budget_revenue = data.budget.revenue.mtd[0].value;
                    budget_expense = data.budget.expense.mtd[0].value;
                    budget_net_profit = data.budget.nett_profit.mtd[0].value;
                    budget_npm = Math.abs(budget_net_profit / budget_revenue) * 100.0;

                    budget_ebitda = data.budget.ebitda.mtd[0].value;
                    budget_ebitda_margin = budget_ebitda / budget_revenue * 100.0;

                    rofo_revenue = data.rofo.revenue.mtd[0].value;
                    rofo_expense = data.rofo.expense.mtd[0].value;
                    rofo_net_profit = data.rofo.nett_profit.mtd[0].value;
                    rofo_npm = Math.abs(rofo_net_profit / rofo_revenue) * 100.0;

                    rofo_ebitda = data.rofo.ebitda.mtd[0].value;
                    rofo_ebitda_margin = rofo_ebitda / rofo_revenue * 100.0;
                } else if (mtd_ytd == 'YTD') {
                    budget_revenue = data.budget.revenue.ytd[0].value;
                    budget_expense = data.budget.expense.ytd[0].value;
                    budget_net_profit = data.budget.nett_profit.ytd[0].value;
                    budget_npm = Math.abs(budget_net_profit / budget_revenue) * 100.0;

                    budget_ebitda = data.budget.ebitda.ytd[0].value;
                    budget_ebitda_margin = budget_ebitda / budget_revenue * 100.0;

                    rofo_revenue = data.rofo.revenue.ytd[0].value;
                    rofo_expense = data.rofo.expense.ytd[0].value;
                    rofo_net_profit = data.rofo.nett_profit.ytd[0].value;
                    rofo_npm = Math.abs(rofo_net_profit / rofo_revenue) * 100.0;

                    rofo_ebitda = data.rofo.ebitda.ytd[0].value;
                    rofo_ebitda_margin = rofo_ebitda / rofo_revenue * 100.0;
                }

                $('#budget-revenue').html(nFormatter(budget_revenue, 2));
                $('#satuan-budget-revenue').text(ratusan_ribuan(budget_revenue));
                $('#budget-expense').html(nFormatter(budget_expense, 2));
                $('#satuan-budget-expense').text(ratusan_ribuan(budget_expense));
                $('#budget-npm').html(nFormatter(budget_npm, 2));
                $('#budget-nett-profit').html(nFormatter(budget_net_profit, 2));
                $('#satuan-budget-nett-profit').text(ratusan_ribuan(budget_net_profit));

                $('#budget-ebitda').html(nFormatter(budget_ebitda, 2));
                $('#satuan-budget-ebitda').text(ratusan_ribuan(budget_ebitda));
                $('#budget-ebitda-margin').html(nFormatter(budget_ebitda_margin, 2));

                $('#rofo-revenue').html(nFormatter(rofo_revenue, 2));
                $('#satuan-rofo-revenue').text(ratusan_ribuan(rofo_revenue));
                $('#rofo-expense').html(nFormatter(rofo_expense, 2));
                $('#satuan-rofo-expense').text(ratusan_ribuan(rofo_expense));
                $('#rofo-npm').html(nFormatter(rofo_npm, 2));
                $('#rofo-nett-profit').html(nFormatter(rofo_net_profit, 2));
                $('#satuan-rofo-nett-profit').text(ratusan_ribuan(rofo_net_profit));

                $('#rofo-ebitda').html(nFormatter(rofo_ebitda, 2));
                $('#satuan-rofo-ebitda').text(ratusan_ribuan(rofo_ebitda));
                $('#rofo-ebitda-margin').html(nFormatter(rofo_ebitda_margin, 2));
            }
        });
        promises.push(request);

        $.when.apply(null, promises).done(function () {
            var promises = [];
            var request = $.ajax({
                url: url_api_findash + 'financialdash/incomestatement/' + bulan + '/' + tahun,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    if (mtd_ytd == 'MTD') {
                        actual_revenue = data.revenue.mtd;
                        actual_expense = data.expense.mtd;
                        actual_net_profit = data.netincome.mtd;
                        actual_npm = Math.abs(actual_net_profit / actual_revenue) * 100.0;
                    } else if (mtd_ytd == 'YTD') {
                        actual_revenue = data.revenue.ytd;
                        actual_expense = data.expense.ytd;
                        actual_net_profit = data.netincome.ytd;
                        for (var finratios in data.finratio) {
                            if (data.finratio[finratios].desc == 'Net Profit Margin') {
                                actual_npm = Math.abs(data.finratio[finratios].Value);
                            }
                        }
                    }

                    $('#actual-revenue').html(nFormatter(actual_revenue, 2));
                    $('#satuan-actual-revenue').text(ratusan_ribuan(actual_revenue));
                    $('#actual-expense').html(nFormatter(actual_expense, 2));
                    $('#satuan-actual-expense').text(ratusan_ribuan(actual_expense));
                    $('#actual-npm').html(nFormatter(actual_npm, 2));
                    $('#actual-nett-profit').html(nFormatter(actual_net_profit, 2));
                    $('#satuan-actual-nett-profit').text(ratusan_ribuan(actual_net_profit));
                }
            });
            promises.push(request);

            $.when.apply(null, promises).done(function () {
                ach_budget_revenue = actual_revenue / budget_revenue * 100.0;
                if(ach_budget_revenue >= 100.00 ){
                    $('#ach-budget-revenue').css('color', '#0eb90e');
                }else{
                    $('#ach-budget-revenue').css('color', '#f24738');
                }
                ach_budget_expense = actual_expense / budget_expense * 100.0;
                if(ach_budget_expense > 100.00 ){
                    $('#ach-budget-expense').css('color', '#f24738');
                }else{
                    $('#ach-budget-expense').css('color', '#0eb90e');
                }
                ach_budget_npm = actual_npm / budget_npm * 100.0;
                if(ach_budget_npm >= 100.00 ){
                    $('#ach-budget-npm').css('color', '#0eb90e');
                }else{
                    $('#ach-budget-npm').css('color', '#f24738');
                }
                ach_budget_net_profit = actual_net_profit / budget_net_profit * 100.0;
                if(ach_budget_net_profit >= 100.00 ){
                    $('#ach-budget-nett-profit').css('color', '#0eb90e');
                }else{
                    $('#ach-budget-nett-profit').css('color', '#f24738');
                }

                $('#ach-budget-revenue').html(nFormatter(ach_budget_revenue, 2));
                $('#ach-budget-expense').html(nFormatter(ach_budget_expense, 2));
                $('#ach-budget-npm').html(nFormatter(ach_budget_npm, 2));
                $('#ach-budget-nett-profit').html(nFormatter(ach_budget_net_profit, 2));

                ach_rofo_revenue = actual_revenue / rofo_revenue * 100.0;
                if(ach_rofo_revenue >= 100.00 ){
                    $('#ach-rofo-revenue').css('color', '#0eb90e');
                }else{
                    $('#ach-rofo-revenue').css('color', '#f24738');
                }
                ach_rofo_expense = actual_expense / rofo_expense * 100.0;
                if(ach_rofo_expense > 100.00 ){
                    $('#ach-rofo-expense').css('color', '#f24738');
                }else{
                    $('#ach-rofo-expense').css('color', '#0eb90e');
                }
                ach_rofo_npm = actual_npm / rofo_npm * 100.0;
                if(ach_rofo_npm >= 100.00 ){
                    $('#ach-rofo-npm').css('color', '#0eb90e');
                }else{
                    $('#ach-rofo-npm').css('color', '#f24738');
                }
                ach_rofo_net_profit = actual_net_profit / rofo_net_profit * 100.0;
                if(ach_rofo_net_profit >= 100.00 ){
                    $('#ach-rofo-nett-profit').css('color', '#0eb90e');
                }else{
                    $('#ach-rofo-nett-profit').css('color', '#f24738');
                }

                $('#ach-rofo-revenue').html(nFormatter(ach_rofo_revenue, 2));
                $('#ach-rofo-expense').html(nFormatter(ach_rofo_expense, 2));
                $('#ach-rofo-npm').html(nFormatter(ach_rofo_npm, 2));
                $('#ach-rofo-nett-profit').html(nFormatter(ach_rofo_net_profit, 2));

                var promises = [];
                var request = $.ajax({
                    url: url_api_findash + 'financialdash/financialstatement/' + bulan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        for (var finratios in data.finratio) {
                            if (data.finratio[finratios].desc == 'EBITDA') {
                                actual_ebitda = data.finratio[finratios].Value;
                            }
                        }
                    }
                });
                promises.push(request);

                $.when.apply(null, promises).done(function () {
                    if (mtd_ytd == 'MTD' && bulan > 1) {
                        var request = $.ajax({
                            url: url_api_findash + 'financialdash/financialstatement/' + ((bulan - 1) + '').padStart(3, '0') + '/' + tahun,
                            type: "GET",
                            dataType: "JSON",
                            success: function (data) {
                                for (var finratios in data.finratio) {
                                    if (data.finratio[finratios].desc == 'EBITDA') {
                                        actual_ebitda -= data.finratio[finratios].Value;
                                    }
                                }
                            }
                        });
                        promises.push(request);
                    }
                    $.when.apply(null, promises).done(function () {
                        actual_ebitda_margin = actual_ebitda / actual_revenue * 100.0;
                        $('#actual-ebitda').html(nFormatter(actual_ebitda, 2));
                        $('#satuan-actual-ebitda').text(ratusan_ribuan(actual_ebitda));
                        $('#actual-ebitda-margin').html(nFormatter(actual_ebitda_margin, 2));

                        ach_budget_ebitda = actual_ebitda / budget_ebitda * 100;
                        if(ach_budget_ebitda >= 100.00 ){
                            $('#ach-budget-ebitda').css('color', '#0eb90e');
                        }else{
                            $('#ach-budget-ebitda').css('color', '#f24738');
                        }
                        ach_budget_ebitda_margin = actual_ebitda_margin / budget_ebitda_margin * 100;
                        if(ach_budget_ebitda_margin >= 100.00 ){
                            $('#ach-budget-ebitda-margin').css('color', '#0eb90e');
                        }else{
                            $('#ach-budget-ebitda-margin').css('color', '#f24738');
                        }

                        $('#ach-budget-ebitda').html(nFormatter(ach_budget_ebitda, 2));
                        $('#ach-budget-ebitda-margin').html(nFormatter(ach_budget_ebitda_margin, 2));

                        ach_rofo_ebitda = actual_ebitda / rofo_ebitda * 100;
                        if(ach_rofo_ebitda >= 100.00 ){
                            $('#ach-rofo-ebitda').css('color', '#0eb90e');
                        }else{
                            $('#ach-rofo-ebitda').css('color', '#f24738');
                        }
                        ach_rofo_ebitda_margin = actual_ebitda_margin / rofo_ebitda_margin * 100;
                        if(ach_rofo_ebitda_margin >= 100.00 ){
                            $('#ach-rofo-ebitda-margin').css('color', '#0eb90e');
                        }else{
                            $('#ach-rofo-ebitda-margin').css('color', '#f24738');
                        }

                        $('#ach-rofo-ebitda').html(nFormatter(ach_rofo_ebitda, 2));
                        $('#ach-rofo-ebitda-margin').html(nFormatter(ach_rofo_ebitda_margin, 2));
                    });
                });
            });
        });
    }


    function table() {      
         $('#combo-month').prop('disabled', true);
        $.ajax({
            url: url_api_table + '1/YTE/' + bulan_jalan + '/' + tahun,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $.each(data, function(key, val){
                    $('#bm1').html(val.base.rofo + ' MUSD');
                    $('#bm2').html(val.base.required + ' MUSD');
                    $('#bm3').html(val.base.gap + '%');
                    if(val.base.required > 0){
                        $('#bm3').css('color', '#F24738');
                    }else{
                        $('#bm3').css('color', '#0EB90E');
                        $('#bm3').html('0%');
                    }
                    $('#lm1').html(val.line.rofo + ' MUSD');
                    $('#lm2').html(val.line.required + ' MUSD');
                    $('#lm3').html(val.line.gap + '%');
                    if(val.line.required > 0){
                        $('#lm3').css('color', '#F24738');
                    }else{
                        $('#lm3').css('color', '#0EB90E');
                        $('#lm3').html('0%');
                    }
                    $('#c1').html(val.comp.rofo + ' MUSD');
                    $('#c2').html(val.comp.required + ' MUSD');
                    $('#c3').html(val.comp.gap + '%');
                    if(val.comp.required > 0){
                        $('#c3').css('color', '#F24738');
                    }else{
                        $('#c3').css('color', '#0EB90E');
                        $('#c3').html('0%');
                    }
                    $('#e1').html(val.engine.rofo + ' MUSD');
                    $('#e2').html(val.engine.required + ' MUSD');
                    $('#e3').html(val.engine.gap + '%');
                    if(val.engine.required > 0){
                        $('#e3').css('color', '#F24738');
                    }else{
                        $('#e3').css('color', '#0EB90E');
                        $('#e3').html('0%');
                    }
                    $('#o1').html(val.other.rofo + ' MUSD');
                    $('#o2').html(val.other.required + ' MUSD');
                    $('#o3').html(val.other.gap + '%');
                    if(val.other.required > 0){
                        $('#o3').css('color', '#F24738');
                    }else{
                        $('#o3').css('color', '#0EB90E');
                        $('#o3').html('0%');
                    }
                });
            }
        });       

        $('#mtd_ytd_yte, #combo-recovery, #combo-month').change(function () {
            var param1 = $('#combo-recovery').val();
            var param2 = $('#mtd_ytd_yte').val();
            var param3 = $('#combo-month').val();
            var type   = param2.replace('-TABEL','');
            var bln    = '0'+param3;

            if(param2 != "MTD-TABEL"){
                bln = bulan_jalan;
                $('#combo-month').prop('disabled', true);
            }else{
                $('#combo-month').prop('disabled', false);
            }

            $.ajax({
                url: url_api_table + param1 + '/' + type + '/' + bln + '/' + tahun,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $.each(data, function(key, val){
                        $('#bm1').html(val.base.rofo + ' MUSD');
                        $('#bm2').html(val.base.required + ' MUSD');
                        $('#bm3').html(val.base.gap + '%');
                        if(param1 != 4){
                            if(val.base.required > 0){
                                $('#bm3').css('color', '#F24738');
                            }else{
                                $('#bm3').css('color', '#0EB90E');
                                $('#bm3').html('0%');
                            }
                        }else{
                            if(val.base.gap > 0){
                                $('#bm3').css('color', '#0EB90E');
                            }else{
                                $('#bm3').css('color', '#F24738');
                            }
                        }
                        $('#lm1').html(val.line.rofo + ' MUSD');
                        $('#lm2').html(val.line.required + ' MUSD');
                        $('#lm3').html(val.line.gap + '%');
                        if(param1 != 4){
                            if(val.line.required > 0){
                                $('#lm3').css('color', '#F24738');
                            }else{
                                $('#lm3').css('color', '#0EB90E');
                                $('#lm3').html('0%');
                            }
                        }else{
                            if(val.line.gap > 0){
                                $('#lm3').css('color', '#0EB90E');
                            }else{
                                $('#lm3').css('color', '#F24738');
                            }
                        }
                        $('#c1').html(val.comp.rofo + ' MUSD');
                        $('#c2').html(val.comp.required + ' MUSD');
                        $('#c3').html(val.comp.gap + '%');
                        if(param1 != 4){
                            if(val.comp.required > 0){
                                $('#c3').css('color', '#F24738');
                            }else{
                                $('#c3').css('color', '#0EB90E');
                                $('#c3').html('0%');
                            }
                        }else{
                            if(val.comp.gap > 0){
                                $('#c3').css('color', '#0EB90E');
                            }else{
                                $('#c3').css('color', '#F24738');
                            }
                        }
                        $('#e1').html(val.engine.rofo + ' MUSD');
                        $('#e2').html(val.engine.required + ' MUSD');
                        $('#e3').html(val.engine.gap + '%');
                        if(param1 != 4){
                            if(val.engine.required > 0){
                                $('#e3').css('color', '#F24738');
                            }else{
                                $('#e3').css('color', '#0EB90E');
                                $('#e3').html('0%');
                            }
                        }else{
                            if(val.engine.gap > 0){
                                $('#e3').css('color', '#0EB90E');
                            }else{
                                $('#e3').css('color', '#F24738');
                            }
                        }
                        $('#o1').html(val.other.rofo + ' MUSD');
                        $('#o2').html(val.other.required + ' MUSD');
                        $('#o3').html(val.other.gap + '%');
                        if(param1 != 4){
                            if(val.other.required > 0){
                                $('#o3').css('color', '#F24738');
                            }else{
                                $('#o3').css('color', '#0EB90E');
                                $('#o3').html('0%');
                            }
                        }else{
                            if(val.other.gap > 0){
                                $('#o3').css('color', '#0EB90E');
                            }else{
                                $('#o3').css('color', '#F24738');
                            }
                        }
                    });
                }
            });

            /*if(param1 == '1' && param2 == 'MTD-TABEL'){
               $.ajax({
                    url: url_api_table + '1/MTD/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#F24738');
                            }else{
                                $('#bm3').css('color', '#0EB90E');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#F24738');
                            }else{
                                $('#lm3').css('color', '#0EB90E');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                            if(val.comp.gap < 0){
                                $('#c3').css('color', '#F24738');
                            }else{
                                $('#c3').css('color', '#0EB90E');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#F24738');
                            }else{
                                $('#e3').css('color', '#0EB90E');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#F24738');
                            }else{
                                $('#o3').css('color', '#0EB90E');
                            }
                        });
                    }
                });
            }
            else if(param1 == '2' && param2 == 'MTD-TABEL'){
                $.ajax({
                    url: url_api_table + '2/MTD/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#F24738');
                            }else{
                                $('#bm3').css('color', '#0EB90E');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#F24738');
                            }else{
                                $('#lm3').css('color', '#0EB90E');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                            if(val.comp.gap < 0){
                                $('#c3').css('color', '#F24738');
                            }else{
                                $('#c3').css('color', '#0EB90E');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#F24738');
                            }else{
                                $('#e3').css('color', '#0EB90E');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#F24738');
                            }else{
                                $('#o3').css('color', '#0EB90E');
                            }
                        });
                    }
                });
            }
            else if(param1 == '3' && param2 == 'MTD-TABEL'){
                $.ajax({
                    url: url_api_table + '3/MTD/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#F24738');
                            }else{
                                $('#bm3').css('color', '#0EB90E');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#F24738');
                            }else{
                                $('#lm3').css('color', '#0EB90E');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                            if(val.comp.gap < 0){
                                $('#c3').css('color', '#F24738');
                            }else{
                                $('#c3').css('color', '#0EB90E');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#F24738');
                            }else{
                                $('#e3').css('color', '#0EB90E');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#F24738');
                            }else{
                                $('#o3').css('color', '#0EB90E');
                            }
                        });
                    }
                });
            }
            else if(param1 == '4' && param2 == 'MTD-TABEL'){
                $.ajax({
                    url: url_api_table + '4/MTD/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#0EB90E');
                            }else{
                                $('#bm3').css('color', '#F24738');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#0EB90E');
                            }else{
                                $('#lm3').css('color', '#F24738');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                              if(val.comp.gap < 0){
                                $('#c3').css('color', '#0EB90E');
                            }else{
                                $('#c3').css('color', '#F24738');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#0EB90E');
                            }else{
                                $('#e3').css('color', '#F24738');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#0EB90E');
                            }else{
                                $('#o3').css('color', '#F24738');
                            }
                        });
                    }
                });
            }
            else if(param1 == '1' && param2 == 'YTD-TABEL'){
               $.ajax({
                    url: url_api_table + '1/YTD/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#F24738');
                            }else{
                                $('#bm3').css('color', '#0EB90E');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#F24738');
                            }else{
                                $('#lm3').css('color', '#0EB90E');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                            if(val.comp.gap < 0){
                                $('#c3').css('color', '#F24738');
                            }else{
                                $('#c3').css('color', '#0EB90E');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#F24738');
                            }else{
                                $('#e3').css('color', '#0EB90E');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#F24738');
                            }else{
                                $('#o3').css('color', '#0EB90E');
                            }
                        });
                    }
                });
            }
            else if(param1 == '2' && param2 == 'YTD-TABEL'){
                $.ajax({
                    url: url_api_table + '2/YTD/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#F24738');
                            }else{
                                $('#bm3').css('color', '#0EB90E');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#F24738');
                            }else{
                                $('#lm3').css('color', '#0EB90E');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                            if(val.comp.gap < 0){
                                $('#c3').css('color', '#F24738');
                            }else{
                                $('#c3').css('color', '#0EB90E');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#F24738');
                            }else{
                                $('#e3').css('color', '#0EB90E');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#F24738');
                            }else{
                                $('#o3').css('color', '#0EB90E');
                            }
                        });
                    }
                });
            }
            else if(param1 == '3' && param2 == 'YTD-TABEL'){
                $.ajax({
                    url: url_api_table + '3/YTD/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#F24738');
                            }else{
                                $('#bm3').css('color', '#0EB90E');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#F24738');
                            }else{
                                $('#lm3').css('color', '#0EB90E');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                            if(val.comp.gap < 0){
                                $('#c3').css('color', '#F24738');
                            }else{
                                $('#c3').css('color', '#0EB90E');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#F24738');
                            }else{
                                $('#e3').css('color', '#0EB90E');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#F24738');
                            }else{
                                $('#o3').css('color', '#0EB90E');
                            }
                        });
                    }
                });
            }
            else if(param1 == '4' && param2 == 'YTD-TABEL'){
                $.ajax({
                    url: url_api_table + '4/YTD/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#0EB90E');
                            }else{
                                $('#bm3').css('color', '#F24738');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#0EB90E');
                            }else{
                                $('#lm3').css('color', '#F24738');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                              if(val.comp.gap < 0){
                                $('#c3').css('color', '#0EB90E');
                            }else{
                                $('#c3').css('color', '#F24738');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#0EB90E');
                            }else{
                                $('#e3').css('color', '#F24738');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#0EB90E');
                            }else{
                                $('#o3').css('color', '#F24738');
                            }
                        });
                    }
                });
            }
            else if(param1 == '1' && param2 == 'YTE-TABEL'){
               $.ajax({
                    url: url_api_table + '1/YTE/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#F24738');
                            }else{
                                $('#bm3').css('color', '#0EB90E');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#F24738');
                            }else{
                                $('#lm3').css('color', '#0EB90E');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                            if(val.comp.gap < 0){
                                $('#c3').css('color', '#F24738');
                            }else{
                                $('#c3').css('color', '#0EB90E');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#F24738');
                            }else{
                                $('#e3').css('color', '#0EB90E');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#F24738');
                            }else{
                                $('#o3').css('color', '#0EB90E');
                            }
                        });
                    }
                });
            }
            else if(param1 == '2' && param2 == 'YTE-TABEL'){
                $.ajax({
                    url: url_api_table + '2/YTE/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#F24738');
                            }else{
                                $('#bm3').css('color', '#0EB90E');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#F24738');
                            }else{
                                $('#lm3').css('color', '#0EB90E');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                            if(val.comp.gap < 0){
                                $('#c3').css('color', '#F24738');
                            }else{
                                $('#c3').css('color', '#0EB90E');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#F24738');
                            }else{
                                $('#e3').css('color', '#0EB90E');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#F24738');
                            }else{
                                $('#o3').css('color', '#0EB90E');
                            }
                        });
                    }
                });
            }
            else if(param1 == '3' && param2 == 'YTE-TABEL'){
                $.ajax({
                    url: url_api_table + '3/YTE/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#F24738');
                            }else{
                                $('#bm3').css('color', '#0EB90E');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#F24738');
                            }else{
                                $('#lm3').css('color', '#0EB90E');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                            if(val.comp.gap < 0){
                                $('#c3').css('color', '#F24738');
                            }else{
                                $('#c3').css('color', '#0EB90E');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#F24738');
                            }else{
                                $('#e3').css('color', '#0EB90E');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#F24738');
                            }else{
                                $('#o3').css('color', '#0EB90E');
                            }
                        });
                    }
                });
            }
            else if(param1 == '4' && param2 == 'YTE-TABEL'){
                $.ajax({
                    url: url_api_table + '4/YTE/' + bulan_jalan + '/' + tahun,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function(key, val){
                            $('#bm1').html(val.base.rofo + ' MUSD');
                            $('#bm2').html(val.base.required + ' MUSD');
                            $('#bm3').html(val.base.gap + '%');
                            if(val.base.gap < 0){
                                $('#bm3').css('color', '#0EB90E');
                            }else{
                                $('#bm3').css('color', '#F24738');
                            }
                            $('#lm1').html(val.line.rofo + ' MUSD');
                            $('#lm2').html(val.line.required + ' MUSD');
                            $('#lm3').html(val.line.gap + '%');
                            if(val.line.gap < 0){
                                $('#lm3').css('color', '#0EB90E');
                            }else{
                                $('#lm3').css('color', '#F24738');
                            }
                            $('#c1').html(val.comp.rofo + ' MUSD');
                            $('#c2').html(val.comp.required + ' MUSD');
                            $('#c3').html(val.comp.gap + '%');
                              if(val.comp.gap < 0){
                                $('#c3').css('color', '#0EB90E');
                            }else{
                                $('#c3').css('color', '#F24738');
                            }
                            $('#e1').html(val.engine.rofo + ' MUSD');
                            $('#e2').html(val.engine.required + ' MUSD');
                            $('#e3').html(val.engine.gap + '%');
                            if(val.engine.gap < 0){
                                $('#e3').css('color', '#0EB90E');
                            }else{
                                $('#e3').css('color', '#F24738');
                            }
                            $('#o1').html(val.other.rofo + ' MUSD');
                            $('#o2').html(val.other.required + ' MUSD');
                            $('#o3').html(val.other.gap + '%');
                            if(val.other.gap < 0){
                                $('#o3').css('color', '#0EB90E');
                            }else{
                                $('#o3').css('color', '#F24738');
                            }
                        });
                    }
                });
            }*/
        });

    }

    var label_month     = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var label_current   = label_month.slice(0, ((new Date().getMonth()) + 1));
    //COLOR GRAPH
    var color_actual    = '#F24738';
    var color_budget    = '#e2c692';
    var color_rofo      = '#0D2633';

    function ActToRofo(actual, rofo) {
        var month = new Date().getMonth();
        var tmp = [];
        var actuall = [];
        for (var i = 0; i < label_month.length; i++) {
            if (i > month) {
                tmp.push(rofo[i]);
            } else if (i == month) {
                actuall[i] = actual[i];
                tmp.push(actuall[i]);
            } else {
                actuall[i] = actual[i];
                tmp.push(actuall[i] = null);
            }
        }
        return tmp;
    }

    var promises = [];
    var promises_jenis = [];
    function grafik(tahun) {
        promises = [];
        var financial = $('#financial-parameter').val();
        var mtd_ytd = $('#mtd_ytd').val();

        // REVENUE
        var data_rofo1 = []; // [34384333, 73354165, 115927823, 151498178, 188353343, 226821888, 267269350, 305812228, 356122360, 407921274, 454309814, 499561788];
        var data_budget1 = []; // [32768591, 71415096, 114499187, 159852137, 206979879, 248540389, 292878145, 340429150, 390161225, 438150460, 483522823, 527056100];
        // EBITDA
        var data_rofo2 = []; // [4445626, 8817531, 17292610, 24479721, 29368027, 35822007, 44090382, 50946082, 62337669, 76473030, 87842036, 98859669]
        var data_budget2 = []; // [4992070, 12654320, 22530664, 33848077, 45900125, 53811074, 63889049, 76355202, 88472771, 99883284, 109400927, 117954212];
        // NET PROVIT
        var data_rofo3 = []; // [1449522, 2751794, 7505022, 11580190, 15514669, 18580367, 22997334, 26344666, 33067511, 41852641, 48562793, 54907218];
        var data_budget3 = []; // [1789135, 5551949, 10926681, 17350832, 24320619, 28104042, 33503211, 40683382, 47575713, 53942207, 58888837, 63010000];
        //EBITDA MARGIN
        var data_rofo4 = []; // [12.93, 12.02, 14.92, 16.16, 15.59, 15.79, 16.50, 16.66, 17.50, 18.75, 19.34, 19.79];
        var data_budget4 = []; // [15.23, 17.72, 19.68, 21.17, 22.18, 21.65, 21.81, 22.43, 22.68, 22.80, 22.63, 22.38];
        //NPM
        var data_rofo5 = []; // [4.22, 3.75, 6.47, 7.64, 8.24, 8.19, 8.60, 8.61, 9.29, 10.26, 10.69, 10.99];
        var data_budget5 = []; // [5.46, 7.77, 9.54, 10.85, 11.75, 11.31, 11.44, 11.95, 12.19, 12.31, 12.18, 11.96];
        //EXPENSE
        var data_rofo6 = []; // [31972152, 67893126, 102428377, 131846061, 165313714, 198936457, 232736423, 266057986, 306646119, 345973319, 382656785, 418691320];
        var data_budget6 = []; // [29424872, 62096085, 97055662, 132884870, 169761358, 205319097, 241499755, 278518993, 318103087, 356645457, 394464109, 431544295];

        
        for (var bln = 1; bln <= 12; bln++) {
            var request = $.ajax({
                url: url_api_findata + '0/' + tahun + '/' + bln,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    if (mtd_ytd == 'MTD') {
                        data_budget1[data.budget.revenue.ytd[0].fiscal_period - 1] = parseInt(data.budget.revenue.ytd[0].value);
                        data_budget6[data.budget.expense.ytd[0].fiscal_period - 1] = parseInt(data.budget.expense.ytd[0].value);
                        data_budget3[data.budget.nett_profit.ytd[0].fiscal_period - 1] = parseInt(data.budget.nett_profit.ytd[0].value);
                        data_budget5[data.budget.nett_profit.ytd[0].fiscal_period - 1] = parseInt(Math.abs(data_budget3[data.budget.nett_profit.ytd[0].fiscal_period - 1] / data_budget1[data.budget.revenue.ytd[0].fiscal_period - 1]) * 100.0);
                        data_budget2[data.budget.ebitda.ytd[0].fiscal_period - 1] = parseInt(data.budget.ebitda.ytd[0].value);
                        data_budget4[data.budget.ebitda.ytd[0].fiscal_period - 1] = parseInt(data_budget2[data.budget.ebitda.ytd[0].fiscal_period - 1] / data_budget1[data.budget.revenue.ytd[0].fiscal_period - 1] * 100.0);
                        data_rofo1[data.rofo.revenue.ytd[0].fiscal_period - 1] = parseInt(data.rofo.revenue.ytd[0].value);
                        data_rofo6[data.rofo.expense.ytd[0].fiscal_period - 1] = parseInt(data.rofo.expense.ytd[0].value);
                        data_rofo3[data.rofo.nett_profit.ytd[0].fiscal_period - 1] = parseInt(data.rofo.nett_profit.ytd[0].value);
                        data_rofo5[data.rofo.nett_profit.ytd[0].fiscal_period - 1] = parseInt(Math.abs(data_rofo3[data.rofo.nett_profit.ytd[0].fiscal_period - 1] / data_rofo1[data.rofo.revenue.ytd[0].fiscal_period - 1]) * 100.0);

                        data_rofo2[data.rofo.ebitda.ytd[0].fiscal_period - 1] = parseInt(data.rofo.ebitda.ytd[0].value);
                        data_rofo4[data.rofo.ebitda.ytd[0].fiscal_period - 1] = parseInt(data_rofo2[data.rofo.ebitda.ytd[0].fiscal_period - 1] / data_rofo1[data.rofo.revenue.ytd[0].fiscal_period - 1] * 100.0);
                    } else if (mtd_ytd == 'YTD') {
                        data_budget1[data.budget.revenue.ytd[0].fiscal_period - 1] = parseInt(data.budget.revenue.ytd[0].value);
                        data_budget6[data.budget.expense.ytd[0].fiscal_period - 1] = parseInt(data.budget.expense.ytd[0].value);
                        data_budget3[data.budget.nett_profit.ytd[0].fiscal_period - 1] = parseInt(data.budget.nett_profit.ytd[0].value);
                        data_budget5[data.budget.nett_profit.ytd[0].fiscal_period - 1] = parseInt(Math.abs(data_budget3[data.budget.nett_profit.ytd[0].fiscal_period - 1] / data_budget1[data.budget.revenue.ytd[0].fiscal_period - 1]) * 100.0);
                        data_budget2[data.budget.ebitda.ytd[0].fiscal_period - 1] = parseInt(data.budget.ebitda.ytd[0].value);
                        data_budget4[data.budget.ebitda.ytd[0].fiscal_period - 1] = parseInt(data_budget2[data.budget.ebitda.ytd[0].fiscal_period - 1] / data_budget1[data.budget.revenue.ytd[0].fiscal_period - 1] * 100.0);
                        data_rofo1[data.rofo.revenue.ytd[0].fiscal_period - 1] = parseInt(data.rofo.revenue.ytd[0].value);
                        data_rofo6[data.rofo.expense.ytd[0].fiscal_period - 1] = parseInt(data.rofo.expense.ytd[0].value);
                        data_rofo3[data.rofo.nett_profit.ytd[0].fiscal_period - 1] = parseInt(data.rofo.nett_profit.ytd[0].value);
                        data_rofo5[data.rofo.nett_profit.ytd[0].fiscal_period - 1] = parseInt(Math.abs(data_rofo3[data.rofo.nett_profit.ytd[0].fiscal_period - 1] / data_rofo1[data.rofo.revenue.ytd[0].fiscal_period - 1]) * 100.0);
                        data_rofo2[data.rofo.ebitda.ytd[0].fiscal_period - 1] = parseInt(data.rofo.ebitda.ytd[0].value);
                        data_rofo4[data.rofo.ebitda.ytd[0].fiscal_period - 1] = parseInt(data_rofo2[data.rofo.ebitda.ytd[0].fiscal_period - 1] / data_rofo1[data.rofo.revenue.ytd[0].fiscal_period - 1] * 100.0);
                    }
                }
            });
            promises.push(request);
        }

        $.when.apply(null, promises).done(function () {
            $.ajax({
                url: url_api_findash + 'financialdash/period_months/' + tahun,
                type: "GET",
                dataType: "JSON",
                success: function (data) {

                    //REVENUE
                    if (financial == 'Revenue') {
                        var data_actual1 = [];
                        promises_jenis = [];
                        for (var bln = 0; bln < data.items.length - 2; bln++) {
                            var request = $.ajax({
                                url: url_api_findash + 'financialdash/incomestatement/' + data.items[bln] + '/' + tahun,
                                type: "GET",
                                dataType: "JSON",
                                success: function (data2) {
                                    //data_actual1[parseInt(data2.financial_period, 10) - 1] = data2.revenue.ytd;
                                    if (typeof data2.revenue === "undefined") {
                                        //alert("something is undefined");
                                        data_actual1[parseInt(data2.financial_period, 10) - 1] = 0;
                                    }else{
                                        data_actual1[parseInt(data2.financial_period, 10) - 1] = data2.revenue.ytd;
                                    }
                                }
                            });
                            promises_jenis.push(request);
                        }
                        $.when.apply(null, promises_jenis).done(function () {
                            var convert_data1 = ActToRofo(data_actual1, data_rofo1);

                            var chart = Highcharts.chart('grafik', {
                                chart: {
                                    type: 'line',
                                },
                                credits: {
                                    enabled: false
                                },
                                exporting: {
                                    enabled: false
                                },
                                title: {
                                    text: ''
                                },
                                xAxis: {
                                    categories: label_month
                                },
                                yAxis: {
                                    labels: {
                                        formatter: function () {
                                            return Highcharts.numberFormat(this.value / 1000000, 2, ',', '.') + "M"
                                        },
                                    },
                                    title: {
                                        text: ''
                                    }
                                },
                                tooltip: {
                                    formatter: function () {
                                        return '<b>' + this.key + '</b><br/>' + this.point.series.name + ':' + '<b>' + Highcharts.numberFormat(this.point.y, 2, ',', '.') + '  MUSD</b><br/>';
                                    }
                                },
                                plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            formatter: function () {
                                                return   Highcharts.numberFormat(this.y / 1000000, 2, ',', '.') + "M";
                                            },
                                            borderRadius: 2,
                                            y: -10,
                                            shape: 'callout',
                                            style: {
                                                textOutline: 0
                                            },
                                        }
                                    },
                                    line: {
                                        dataLabels: {
                                            enabled: true
                                        }
                                    }
                                },
                                series: [{
                                        name: 'Rofo Revenue',
                                        data: convert_data1,
                                        color: color_rofo,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_rofo,
                                            align: 'left',
                                            enabled: true,
                                            x: 2,
                                            y: 23
                                        }
                                    }, {
                                        name: 'Actual Revenue',
                                        data: data_actual1,
                                        color: color_actual,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_actual,
                                            align: 'left',
                                            enabled: true,
                                            x: 2,
                                            y: 21
                                        }
                                    }, {
                                        name: 'Budget Revenue',
                                        data: data_budget1,
                                        color: color_budget,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_budget,
                                            align: 'left',
                                            enabled: true,
                                            x: 2,
                                            y: -5
                                        }
                                    }]
                            });
                            var len_budget          = data_budget1.length - 1;
                            var lastPoint_budget    = data_budget1[len_budget];
                            var len_rofo            = data_rofo1.length - 1;
                            var lastPoint_rofo      = data_rofo1[len_rofo];
                            var gap                 = lastPoint_budget - lastPoint_rofo;
                            // $('#nilai-max').html(Convert_Ribuan(gap));
                            // $('#msud-grafik').html('MUSD');
                            $('#nilai-max').html('<i class="fa fa-money"></i> Year End Gap : ' +Convert_Ribuan(gap)+ ' MUSD');
                            // $('#msud-grafik').html('MUSD');
                        });
                    } 
                    //EBITDA
                    else if (financial == 'Ebitda') {
                        var data_actual2 = [];
                        for(var x=0;x<data.items.length-2;x++){
                            data_actual2[x] = 0;
                        }
                        promises_jenis = [];
                        
                        for (var bln = 0; bln < data.items.length - 2; bln++) {
                            var request = $.ajax({
                                url: url_api_findash + 'financialdash/financialstatement/' + data.items[bln] + '/' + tahun,
                                type: "GET",
                                dataType: "JSON",
                                success: function (data2) {

                                        for (var finratios in data2.finratio) {
                                            if (data2.finratio[finratios].desc == 'EBITDA') {
                                                //console.log(data2.financial_period +" -> "+ parseInt(data2.financial_period, 10));
                                                data_actual2[parseInt(data2.financial_period, 10) - 1] = (data2.finratio[finratios].Value);
                                            }
                                        }
                                    
                                }
                            });
                            promises_jenis.push(request);
                        }
                            $.when.apply(null, promises_jenis).always(function () {
                                var convert_data2 = ActToRofo(data_actual2, data_rofo2);
                                var chart = Highcharts.chart('grafik', {
                                    chart: {
                                        type: 'line',
                                    },
                                    credits: {
                                        enabled: false
                                    },
                                    exporting: {
                                        enabled: false
                                    },
                                    title: {
                                        text: ''
                                    },
                                    xAxis: {
                                        categories: label_month
                                    },
                                    yAxis: {
                                        height: '98%',
                                        resize: {
                                            enabled: true
                                        },
                                        min: 0,
                                        labels: {
                                            formatter: function () {
                                                return Highcharts.numberFormat(this.value / 1000000, 2, ',', '.') + "M"
                                            },
                                        },
                                        title: {
                                            text: ''
                                        }
                                    },
                                    tooltip: {
                                        formatter: function () {
                                            return '<b>' + this.key + '</b><br/>' + this.point.series.name + ':' + '<b>' + Highcharts.numberFormat(this.point.y, 2, ',', '.') + '  MUSD</b><br/>';
                                        }
                                    },
                                    plotOptions: {
                                        series: {
                                            dataLabels: {
                                                enabled: true,
                                                formatter: function () {
                                                    return   Highcharts.numberFormat(this.y / 1000000, 2, ',', '.') + "M";
                                                },
                                                borderRadius: 2,
                                                y: -10,
                                                shape: 'callout',
                                                style: {
                                                    textOutline: 0
                                                },
                                            }
                                        },
                                        line: {
                                            dataLabels: {
                                                enabled: true
                                            }
                                        }
                                    },
                                    series: [{
                                            name: 'Rofo Ebitda',
                                            data: convert_data2,
                                            color: color_rofo,
                                            marker: {
                                                enabled: true,
                                                symbol: "circle",
                                                radius: 5
                                            },
                                            dataLabels: {
                                                enabled: true,
                                                color: color_rofo,
                                                x: 2,
                                                y: 23
                                            }
                                        }, {
                                            name: 'Actual Ebitda',
                                            data: data_actual2,
                                            color: color_actual,
                                            marker: {
                                                enabled: true,
                                                symbol: "circle",
                                                radius: 5
                                            },
                                            dataLabels: {
                                                enabled: true,
                                                color: color_actual,
                                                x: 2,
                                                y: 21
                                            }
                                        }, {
                                            name: 'Budget Ebitda',
                                            data: data_budget2,
                                            color: color_budget,
                                            marker: {
                                                enabled: true,
                                                symbol: "circle",
                                                radius: 5
                                            },
                                            dataLabels: {
                                                enabled: true,
                                                color: color_budget,
                                                x: 2,
                                                y: -5
                                            }
                                        }]
                                });
                                var len_budget          = data_budget2.length - 1;
                                var lastPoint_budget    = data_budget2[len_budget];
                                var len_rofo            = data_rofo2.length - 1;
                                var lastPoint_rofo      = data_rofo2[len_rofo];
                                var gap                 = lastPoint_budget - lastPoint_rofo;
                                // $('#nilai-max').html(Convert_Ribuan(gap));
                                // $('#msud-grafik').html('MUSD');
                                $('#nilai-max').html('<i class="fa fa-money"></i> Year End Gap : ' +Convert_Ribuan(gap)+ ' MUSD');
                            });
                    }
                    //NETT PROFIT
                    else if (financial == 'Nett Profit') {
                        var data_actual3 = [];
                        for(var x=0;x<data.items.length-2;x++){
                            data_actual3[x] = 0;
                        }
                        promises_jenis = [];
                        for (var bln = 0; bln < data.items.length - 2; bln++) {
                            var request = $.ajax({
                                url: url_api_findash + 'financialdash/incomestatement/' + data.items[bln] + '/' + tahun,
                                type: "GET",
                                dataType: "JSON",
                                success: function (data2) {
                                    // var actual_net_profit = 0.0;
                                    // if (mtd_ytd == 'MTD') {
                                    //     actual_net_profit = data.netincome.ytd;
                                    // } else if (mtd_ytd == 'YTD') {
                                    //     actual_net_profit = data.netincome.ytd;
                                    // }
                                    if (typeof data2.netincome === "undefined") {
                                        //alert("something is undefined");
                                        data_actual3[parseInt(data2.financial_period, 10) - 1] = 0;
                                    }else{
                                        data_actual3[parseInt(data2.financial_period, 10) - 1] = data2.netincome.ytd;
                                    }
                                    //data_actual3[parseInt(data2.financial_period, 10) - 1] = data2.netincome.ytd;
                                }
                            });
                            promises_jenis.push(request);
                        }

                        $.when.apply(null, promises_jenis).always(function () {
                            var convert_data3 = ActToRofo(data_actual3, data_rofo3);

                            var chart = Highcharts.chart('grafik', {
                                chart: {
                                    type: 'line',
                                },
                                credits: {
                                    enabled: false
                                },
                                exporting: {
                                    enabled: false
                                },
                                title: {
                                    text: ''
                                },
                                xAxis: {
                                    categories: label_month
                                },
                                yAxis: {
                                    height: '95%',
                                    resize: {
                                        enabled: true
                                    },
                                    labels: {
                                        formatter: function () {
                                            return Highcharts.numberFormat(this.value / 1000000, 2, ',', '.') + "M"
                                        },
                                    },
                                    title: {
                                        text: ''
                                    }
                                },
                                tooltip: {
                                    formatter: function () {
                                        return '<b>' + this.key + '</b><br/>' + this.point.series.name + ':' + '<b>' + Highcharts.numberFormat(this.point.y, 2, ',', '.') + '  MUSD</b><br/>';
                                    }
                                },
                                plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            formatter: function () {
                                                return   Highcharts.numberFormat(this.y / 1000000, 2, ',', '.') + "M";
                                            },
                                            borderRadius: 2,
                                            y: -10,
                                            shape: 'callout',
                                            style: {
                                                textOutline: 0
                                            },
                                        }
                                    },
                                    line: {
                                        dataLabels: {
                                            enabled: true
                                        }
                                    }
                                },
                                series: [{
                                        name: 'Rofo Nett Profit',
                                        data: convert_data3,
                                        color: color_rofo,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_rofo,
                                            x: 2,
                                            y: 23
                                        }
                                    }, {
                                        name: 'Actual Nett Profit',
                                        data: data_actual3,
                                        color: color_actual,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_actual,
                                            x: 2,
                                            y: 21
                                        }
                                    }, {
                                        name: 'Budget Nett Profit',
                                        data: data_budget3,
                                        color: color_budget,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_budget,
                                            x: 2,
                                            y: -5
                                        }
                                    }]
                            });
                            var len_budget = data_budget3.length - 1;
                            var lastPoint_budget = data_budget3[len_budget];
                            var len_rofo = data_rofo3.length - 1;
                            var lastPoint_rofo = data_rofo3[len_rofo];
                            var gap = lastPoint_budget - lastPoint_rofo;
                            // $('#nilai-max').html(Convert_Ribuan(gap));
                            // $('#msud-grafik').html('MUSD');
                            $('#nilai-max').html('<i class="fa fa-money"></i> Year End Gap : ' +Convert_Ribuan(gap)+ ' MUSD');
                        });
                    }
                    //EBITDA MARGIN
                    else if (financial == 'Ebitda Margin') {
                        var data_actual4 = [];
                        promises_jenis = [];
                        for (var bln = 0; bln < data.items.length - 2; bln++) {
                            var request = $.ajax({
                                url: url_api_findash + 'financialdash/financialstatement/' + data.items[bln] + '/' + tahun,
                                type: "GET",
                                dataType: "JSON",
                                success: function (data2) {
                                    for (var finratios in data2.finratio) {
                                        if (data2.finratio[finratios].desc == 'EBITDA') {
                                            data_actual4[parseInt(data2.financial_period, 10) - 1] = (data2.finratio[finratios].Value);
                                        }
                                    }
                                }
                            });
                            promises_jenis.push(request);
                        }
                            $.when.apply(null, promises_jenis).done(function () {
                                for (var bln = 0; bln < data.items.length - 2; bln++) {
                                    var request = $.ajax({
                                        url: url_api_findash + 'financialdash/incomestatement/' + data.items[bln] + '/' + tahun,
                                        type: "GET",
                                        dataType: "JSON",
                                        success: function (data2) {
                                            data_actual4[parseInt(data2.financial_period, 10) - 1] /= data2.revenue.ytd;
                                            data_actual4[parseInt(data2.financial_period, 10) - 1] *= 10000;
                                            data_actual4[parseInt(data2.financial_period, 10) - 1] = Math.round(data_actual4[parseInt(data2.financial_period, 10) - 1]);
                                            data_actual4[parseInt(data2.financial_period, 10) - 1] /= 100;
                                        }
                                    });
                                    promises.push(request);
                                }
                                $.when.apply(null, promises).done(function () {
                                    var convert_data4 = ActToRofo(data_actual4, data_rofo4);

                                    var chart = Highcharts.chart('grafik', {
                                        chart: {
                                            type: 'line',
                                        },
                                        credits: {
                                            enabled: false
                                        },
                                        exporting: {
                                            enabled: false
                                        },
                                        title: {
                                            text: ''
                                        },
                                        xAxis: {
                                            categories: label_month
                                        },
                                        yAxis: {
                                            labels: {
                                            },
                                            title: {
                                                text: ''
                                            }
                                        },
                                        tooltip: {
                                            formatter: function () {
                                                return '<b>' + this.key + '</b><br/>' + this.point.series.name + ':' + '<b>' + this.point.y + '%</b><br/>';
                                            }
                                        },
                                        plotOptions: {
                                            series: {
                                                dataLabels: {
                                                    enabled: true,
                                                    style: {
                                                        textOutline: 0
                                                    },
                                                }
                                            },
                                            line: {
                                                dataLabels: {
                                                    enabled: true
                                                }
                                            }
                                        },
                                        series: [{
                                                name: 'Rofo Ebitda Margin',
                                                data: convert_data4,
                                                color: color_rofo,
                                                marker: {
                                                    enabled: true,
                                                    symbol: "circle",
                                                    radius: 5
                                                },
                                                dataLabels: {
                                                    enabled: true,
                                                    color: color_rofo
                                                }
                                            }, {
                                                name: 'Actual Ebitda Margin',
                                                data: data_actual4,
                                                color: color_actual,
                                                marker: {
                                                    enabled: true,
                                                    symbol: "circle",
                                                    radius: 5
                                                },
                                                dataLabels: {
                                                    enabled: true,
                                                    color: color_actual
                                                }
                                            }, {
                                                name: 'Budget Ebitda Margin',
                                                data: data_budget4,
                                                color: color_budget,
                                                marker: {
                                                    enabled: true,
                                                    symbol: "circle",
                                                    radius: 5
                                                },
                                                dataLabels: {
                                                    enabled: true,
                                                    color: color_budget
                                                }
                                            }]
                                    });
                                    var len_budget          = data_budget4.length - 1;
                                    var lastPoint_budget    = data_budget4[len_budget];
                                    var len_rofo            = data_rofo4.length - 1;
                                    var lastPoint_rofo      = data_rofo4[len_rofo];
                                    var gap                 = lastPoint_budget - lastPoint_rofo;
                                    // $('#nilai-max').html(gap.toFixed(2));
                                    // $('#msud-grafik').html('Percent');
                                    $('#nilai-max').html('<i class="fa fa-money"></i> Year End Gap : ' +gap.toFixed(2)+ ' Percent');
                                });
                            });
                    }
                    //NPM
                    else if (financial == 'NPM') {
                        //NPM
                        var data_actual5 = []; // [4.21, 3.96, 7.11, 7.86, 7.67]

                        promises_jenis = [];
                        for (var bln = 0; bln < data.items.length - 2; bln++) {
                            var request = $.ajax({
                                url: url_api_findash + 'financialdash/incomestatement/' + data.items[bln] + '/' + tahun,
                                type: "GET",
                                dataType: "JSON",
                                success: function (data) {
                                    var actual_revenue = 0.0, actual_npm = 0.0, actual_net_profit = 0.0;
                                    if (mtd_ytd == 'MTD') {
                                        actual_revenue = data.revenue.ytd;
                                        actual_net_profit = data.netincome.ytd;
                                        for (var finratios in data.finratio) {
                                            if (data.finratio[finratios].desc == 'Net Profit Margin') {
                                                actual_npm = Math.abs(data.finratio[finratios].Value);
                                            }
                                        }
                                    } else if (mtd_ytd == 'YTD') {
                                        actual_revenue = data.revenue.ytd;
                                        actual_net_profit = data.netincome.ytd;
                                        for (var finratios in data.finratio) {
                                            if (data.finratio[finratios].desc == 'Net Profit Margin') {
                                                actual_npm = Math.abs(data.finratio[finratios].Value);
                                            }
                                        }
                                    }
                                    data_actual5[parseInt(data.financial_period, 10) - 1] = (actual_npm);
                                }
                            });
                            promises_jenis.push(request);
                        }

                        $.when.apply(null, promises_jenis).done(function () {
                            var convert_data5 = ActToRofo(data_actual5, data_rofo5);

                            var chart = Highcharts.chart('grafik', {
                                chart: {
                                    type: 'line',
                                },
                                credits: {
                                    enabled: false
                                },
                                exporting: {
                                    enabled: false
                                },
                                title: {
                                    text: ''
                                },
                                xAxis: {
                                    categories: label_month
                                },
                                yAxis: {
                                    labels: {
                                    },
                                    title: {
                                        text: ''
                                    }
                                },
                                tooltip: {
                                    formatter: function () {
                                        return '<b>' + this.key + '</b><br/>' + this.point.series.name + ':' + '<b>' + this.point.y + '%</b><br/>';
                                    }
                                },
                                plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            style: {
                                                textOutline: 0
                                            },
                                        }
                                    },
                                    line: {
                                        dataLabels: {
                                            enabled: true
                                        }
                                    }
                                },
                                series: [{
                                        name: 'Rofo NPM',
                                        data: convert_data5,
                                        color: color_rofo,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_rofo
                                        }
                                    }, {
                                        name: 'Actual NPM',
                                        data: data_actual5,
                                        color: color_actual,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_actual
                                        }
                                    }, {
                                        name: 'Budget NPM',
                                        data: data_budget5,
                                        color: color_budget,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_budget
                                        }
                                    }]
                            });
                            var len_budget = data_budget5.length - 1;
                            var lastPoint_budget = data_budget5[len_budget];
                            var len_rofo = data_rofo5.length - 1;
                            var lastPoint_rofo = data_rofo5[len_rofo];
                            var gap = lastPoint_budget - lastPoint_rofo;

                            // $('#nilai-max').html(gap.toFixed(2));
                            // $('#msud-grafik').html('Percent');
                            $('#nilai-max').html('<i class="fa fa-money"></i> Year End Gap : ' +gap.toFixed(2)+ ' Percent');
                        });
                    } 
                    //Expense
                    else if (financial == 'Expense') {
                        //EXPENSE
                        var data_actual6 = []; // [31975285, 67896089, 102638710, 132055220, 168750795];
                        for(var x=0;x<data.items.length-2;x++){
                            data_actual6[x] = 0;
                        }

                        promises_jenis = [];
                        for (var bln = 0; bln < data.items.length - 2; bln++) {
                            var request = $.ajax({
                                url: url_api_findash + 'financialdash/incomestatement/' + data.items[bln] + '/' + tahun,
                                type: "GET",
                                dataType: "JSON",
                                success: function (data2) {
                                    if (typeof data2.expense === "undefined") {
                                        //alert("something is undefined");
                                        data_actual6[parseInt(data2.financial_period, 10) - 1] = 0;
                                    }else{
                                        data_actual6[parseInt(data2.financial_period, 10) - 1] = data2.expense.ytd;
                                    }
                                    //data_actual6[parseInt(data2.financial_period, 10) - 1] = data2.expense.ytd;
                                }
                            });
                            promises_jenis.push(request);
                        }

                        $.when.apply(null, promises_jenis).always(function () {
                            var convert_data6 = ActToRofo(data_actual6, data_rofo6);

                            var chart = Highcharts.chart('grafik', {
                                chart: {
                                    type: 'line',
                                },
                                credits: {
                                    enabled: false
                                },
                                exporting: {
                                    enabled: false
                                },
                                title: {
                                    text: ''
                                },
                                xAxis: {
                                    categories: label_month
                                },
                                yAxis: {
                                    height: '98%',
                                    resize: {
                                        enabled: true
                                    },
                                    labels: {
                                        formatter: function () {
                                            return Highcharts.numberFormat(this.value / 1000000, 2, ',', '.') + "M"
                                        },
                                    },
                                    title: {
                                        text: ''
                                    }
                                },
                                tooltip: {
                                    formatter: function () {
                                        return '<b>' + this.key + '</b><br/>' + this.point.series.name + ':' + '<b>' + this.point.y + '%</b><br/>';
                                    }
                                },
                                plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            formatter: function () {
                                                return   Highcharts.numberFormat(this.y / 1000000, 2, ',', '.') + "M";
                                            },
                                            borderRadius: 2,
                                            y: -10,
                                            shape: 'callout',
                                            style: {
                                                textOutline: 0
                                            },
                                        }
                                    },
                                    line: {
                                        dataLabels: {
                                            enabled: true
                                        }
                                    }
                                },
                                series: [{
                                        name: 'Rofo Expense',
                                        data: convert_data6,
                                        color: color_rofo,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_rofo,
                                            x: 2,
                                            y: 23
                                        }
                                    }, {
                                        name: 'Actual Expense',
                                        data: data_actual6,
                                        color: color_actual,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_actual,
                                            x: 2,
                                            y: 21
                                        }
                                    }, {
                                        name: 'Budget Expense',
                                        data: data_budget6,
                                        color: color_budget,
                                        marker: {
                                            enabled: true,
                                            symbol: "circle",
                                            radius: 5
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: color_budget,
                                            x: 2,
                                            y: -5
                                        }
                                    }]
                            });
                            var len_budget          = data_budget6.length - 1;
                            var lastPoint_budget    = data_budget6[len_budget];
                            var len_rofo            = data_rofo6.length - 1;
                            var lastPoint_rofo      = data_rofo6[len_rofo];
                            var gap                 = lastPoint_budget - lastPoint_rofo;
                            // $('#nilai-max').html(Convert_Ribuan(gap));
                            // $('#msud-grafik').html('Percent');
                            $('#nilai-max').html('<i class="fa fa-money"></i> Year End Gap : ' +Convert_Ribuan(gap)+ ' MUSD');
                        });
                    }
                }
            });
        });

    }

    function Convert_Ribuan(angka) {
        var rev = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2 = '';
        for (var i = 0; i < rev.length; i++) {
            rev2 += rev[i];
            if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
                rev2 += ',';
            }
        }
        return rev2.split('').reverse().join('');
    }

    function ratusan_ribuan(angka) {
        console.log(angka);
        angka = Convert_Ribuan(angka);
        angka = angka.split('0.');
        //angka = angka.split(',');
        var units = [' ',  'TUSD','USD','MUSD','BUSD'];
        
        var segment3 = angka[0].split(',');
        return units[segment3.length];
    }
</script>
