<style>

    p.hovertest:hover {
        color: blue;
    }

    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }
</style>
<section class="content">
    <div class="row">
        <div class="wadah col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <!-- <h3 class="box-title"><span class="title_garuda" style="font-weight: bold; cursor: pointer;">YTD ()</span></h3> -->
                    <h3 class="box-title">
								<span class="title_garuda" style="font-weight: bold; cursor: pointer;">
									TAT Component Shop
								</span>
                    </h3>
                    <div class="box-tools pull-right">
                        <label class="control-label" style="font-weight: 100;"><i class="fa  fa-calendar"></i> Year To
                            Date :</label>
                        <label class="control-label"
                               id="date"><?= date("d") . " " . convert_month(date("m")) . " " . date("Y") ?></label>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="wadah col-md-4">
                            <div id="grafik_TCA" style="height:250px;width:100%"></div>
                        </div>
                        <div class="wadah col-md-4">
                            <div id="grafik_TCE" style="height:250px;width:100%"></div>
                        </div>
                        <div class="wadah col-md-4">
                            <div id="grafik_TCW" style="height:250px;width:100%"></div>
                        </div>
                        <!--
                        <div class="wadah col-md-3">
                            <div id="grafik_TCY" style="height:250px;width:100%"></div>
                        </div>
                        -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="wadah col-md-8">
            <div class="box" style="height: 230px;">
                <div class="box-header with-border">
                    <h3 class="box-title"><span class="title_garuda" style="font-weight: bold; cursor: pointer;">Detail Plan</span>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                data-toggle="tooltip"
                                title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div id="pie_TCA" style="height: 150px"></div>
                        </div>
                        <div class="col-md-4">
                            <div id="pie_TCE" style="height: 150px"></div>
                        </div>
                        <div class="col-md-4">
                            <div id="pie_TCW" style="height: 150px"></div>
                        </div>
                        <!--
                        <div class="col-md-3">
                            <div id="pie_TCY"></div>
                        </div>
                        -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box" style="height: 230px;">
                <div class="box-header with-border">
                    <h3 class="box-title"><span class="title_garuda" style="font-weight: bold; cursor: pointer;">Work In Process</span>
                    </h3>
                    <div class="box-tools pull-right hide">
                        <select class="form-control" style="padding: 0px;width: 100%;height: 10%;" style="width: 100%;"
                                id="aircraft">
                            <option selected="selected" value="all">- All -</option>
                            <option value="ga">GA</option>
                            <option value="nonga">Non GA</option>
                        </select>
                        <button type="button" style="display:none;" class="btn btn-primary btn-flat" id="search"><i
                                    class="fa fa-search"></i>
                            Search
                        </button>
                    </div>
                </div>
                <div class="box-body" style="padding:0px;">
                    <div class="row" style="margin:0px;">
                        <div class="col-md-6">
                            <div class="banner" style="margin-left:20%;cursor: pointer;" id="panel_tca" isi="TCA">
                                <h4 class="title_panel_tca"
                                    style="text-align:  center;font-weight: bold;font-size: 22px;margin-bottom:  0px;">
                                    TCA</h4>
                                <p style="margin-top: 2px;text-align:  center;font-size:  25px;font-weight: 100;">
                                    <span id="valpa_tca" style="color: #7bc8a4; ">..</span>
                                    <span style="color: #f7ca78;">Events<span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="banner" style="margin-right:20%;cursor: pointer;" id="panel_tce" isi="TCE">
                                <h4 class="title_panel_tce"
                                    style="text-align:  center;font-weight: bold;font-size: 22px;margin-bottom:  0px;">
                                    TCE</h4>
                                <p style="margin-top: 2px;text-align:  center;font-size:  25px;font-weight: 100;">
                                    <span id="valpa_tce" style="color: #7bc8a4; ">..</span>
                                    <span style="color: #f7ca78;">Events<span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin:0px;">
                        <div class="col-md-6">
                            <div class="banner" style="margin-left:20%;cursor: pointer; " id="panel_tcw" isi="TCW">
                                <h4 class="title_panel_tcw"
                                    style="text-align:  center;font-weight: bold;font-size: 22px;margin-bottom:  0px;">
                                    TCW</h4>
                                <p style="margin-top: 2px;text-align:  center;font-size:  25px;font-weight: 100;">
                                    <span id="valpa_tcw" style="color: #7bc8a4; ">..</span>
                                    <span style="color: #f7ca78;">Events<span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="banner" style="margin-right:20%;cursor: pointer; " id="panel_tcy" isi="TCY">
                                <h4 class="title_panel_tcy"
                                    style="text-align:  center;font-weight: bold;font-size: 22px;margin-bottom:  0px;">
                                    TCY</h4>
                                <p style="margin-top: 2px;text-align:  center;font-size:  25px;font-weight: 100;">
                                    <span id="valpa_tcy" style="color: #7bc8a4; ">..</span>
                                    <span style="color: #f7ca78;">Events<span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row tabel_atas" style="display:none">
        <div class="wadah col-md-12 ">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><span class="title_garuda" style="font-weight: bold; cursor: pointer;">Detail Plan</span>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool tutup_atas"
                                title="Close">
                            <i class="fa fa-close"></i></button>
                    </div>
                </div>
                <input type="text" style="display: none" id="inputPlan" value="">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"
                               style="margin-top: 5px;padding-right:0px;">Week</label>
                        <div class="col-sm-9" style="padding-right:0px;padding-left:0px;">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="number" class="form-control pull-right" id="week">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <button class="btn btn-flat bg-navy" id="searchWeek">
                            <i class="fa fa-search"></i> Search
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class='col-md-12'>
                            <table id="tabelselect" class="table table-hover table-bordered table-gmf">
                                <thead style="background-color:#05354d">
                                <th class="text-center">Order Number</th>
                                <th class="text-center">Plant</th>
                                <th class="text-center">Work Center</th>
                                <th class="text-center">Material (PN)</th>
                                <th class="text-center">Mat. Desc</th>
                                <th class="text-center">Serial Number</th>
                                <th class="text-center">WBS</th>
                                <th class="text-center">Date Start</th>
                                <th class="text-center">Date End</th>
                                <th class="text-center">User Status</th>
                                <th class="text-center">Target TAT</th>
                                <th class="text-center">Status</th>
                                </thead>
                                <tbody class="text-center">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row tabel_bawah" style="display:none">
        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><span class="title_garuda" style="font-weight: bold; cursor: pointer;">Detail Plan <span
                                    class="name_plan"></span></span></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool tutup_bawah"
                                title="Close">
                            <i class="fa fa-close"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <span id="title_plan_proses" style="font-size:50px; text-decoration: underline;"></span>
                    <div class="row">
                        <div class='col-md-12'>
                            <table id="tabelselect_belum" class="table table-hover table-bordered table-gmf">
                                <thead style="background-color:#05354d">
                                <th class="text-center">Order Number</th>
                                <th class="text-center">Plant</th>
                                <th class="text-center">Work Center</th>
                                <th class="text-center">Material (PN)</th>
                                <th class="text-center">Mat. Desc</th>
                                <th class="text-center">Serial Number</th>
                                <th class="text-center">WBS</th>
                                <th class="text-center">Date Start</th>
                                <th class="text-center">User Status</th>
                                <th class="text-center">Target TAT</th>
                                <th class="text-center">Total Days</th>
                                </thead>
                                <tbody class="text-center">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>


<script>
    for (var i = 1; i <= 6; i++) {
        //Date picker
        $('#date' + i).datepicker({
            format: "dd-mm-yyyy",
            viewMode: "days",
            minViewMode: "days",
            autoclose: true
        });

        $('#dateyear' + i).datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });
    }

    $(document).ready(function () {

        grafik_plan('TCA');
        grafik_plan('TCE');
        grafik_plan('TCW');
        //grafik_plan('TCY');
        grafik_pie('TCA');
        grafik_pie('TCE');
        grafik_pie('TCW');
        //grafik_pie('TCY');
        panel_proses('TCA');
        panel_proses('TCE');
        panel_proses('TCW');
        //panel_proses('TCY');

        $(document).on("click", "#panel_tca, #panel_tce, #panel_tcw, #panel_tcy", function (e) {
            var plan = $(this).attr("isi");
            console.log(plan);
            detail_plan_table_belum(plan, 'title', 'belum')
        });

        $(document).on("click", "#search", function (e) {
            panel_proses('TCA');
            panel_proses('TCE');
            panel_proses('TCW');
            panel_proses('TCY');
            $(".tabel_bawah").hide("slow");
        });


        $(document).on("click", ".tutup_atas", function (e) {
            $(".tabel_atas").hide("slow");
        });
        $(document).on("click", ".tutup_bawah", function (e) {
            $(".tabel_bawah").hide("slow");
        });

    });


    function grafik_plan(plan) {
        var range_yAxis = [0, 25, 50, 80, 100];

        $.ajax({
            url: "<?= base_url() ?>index.php/Tat_component/view_plan",
            type: "POST",
            data: {
                'plan': plan,
            },
            dataType: "JSON",
            beforeSend: function () {
                $('#loadings').fadeIn(1000);
            },
            success: function (data) {
                var chart = new Highcharts.chart('grafik_' + plan, {
                        title: {
                            text: plan,
                            margin: 0
                        },
                        exporting: {
                            enabled: false
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            title: {
                                text: 'Percentage (%)'
                            },
                            //min: 0,     
                            max: 100,
                            tickPositioner: function() {
                                return range_yAxis;
                            },
                        },
                        xAxis: {
                            categories: data.grafik[0].datat,
                            id: 'x-axis'
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },
                        series: [{
                            name: 'Target',
                            color: '#fd9214',
                            //CONTOH GRADIENT
                            data: data.grafik[0].datag,
                            lineWidth: 3,
                            marker: {
                                enabled: false
                            },

                        }, {
                            type: 'areaspline',
                            name: 'Performance',
                            color: '#05354d',
                            fillColor: {
                                linearGradient: [0, 0, 0, 140],
                                stops: [
                                    [0, "#05354d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            data: data.grafik[0].data
                        }
                        ],

                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }
                    },
                );

                // $('#loadings').fadeOut(1000);
            }

        });
    }

    function grafik_pie(plan) {

        $.ajax({
            url: "<?= base_url() ?>index.php/Tat_component/view_detail_plan",
            type: "POST",
            data: {
                'plan': plan,
            },
            dataType: "JSON",
            beforeSend: function () {
                $('#loadings').fadeIn(1000);
            },
            success: function (data) {
                // if(data.grafik=='not found'){

                // }else{

                $('#pie_' + plan).highcharts({

                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie',
                        //height: 80 + '%',
                        margin: [0, 0, 0, 0]
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        layout: 'vertical',
                        //x: ((plan == "TCW") ? -10 : -5),
                        x: -50,
                        y: 20,
                        itemStyle: {
                          fontSize:'12px',
                        }
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: '<p class="hovertest" style="margin-left: 40%; cursor: pointer;text-decoration: none;color: #05354d;top: 0px;text-align: center;font-weight: bold;" href="javascript:void(0)" onclick="detail_plan_table(\'' + plan + '\', \'title\', \'sudah\',\'\')">' + plan + '</p>'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            center: [25, 40],
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function (event) {
                                        detail_plan_table(this.name, 'irisan', 'sudah', '');
                                        $('#inputPlan').val(this.name);
                                    }
                                }
                            },
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true,
                            size: 90
                        }
                    },
                    credits: {
                        enabled: false,

                    },
                    colors: [
                        '#05354d',
                        '#f7ca78',
                        '#7bc8a4',
                        '#93648d',
                        '#4cc3d9',
                        '#1f7f91',
                        '#074b6d',
                        '#d18700',
                        '#ffdd9e',
                    ],

                    series: [{
                        name: 'Work Center',
                        colorByPoint: true,
                        data: data.grafik
                    }]
                });

                // $('#loadings').fadeOut(1000);
            }
            // }

        });
    }

    $(document).on("click", "#searchWeek", function (e) {
        var inputPlan = $('#inputPlan').val();
        var week = $('#week').val();
        detail_plan_table(inputPlan, 'irisan', 'sudah', week);
    });

    function detail_plan_table(plan, tipe, param, week) {
        $(".tabel_atas").fadeIn('fast');
        var table = $("#tabelselect").DataTable({
            "destroy": true,
            "serverSide": false,
            "processing": false,
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            // "scrollY":'50vh',
            "scrollCollapse": true,

            columns: [
                {data: 'order_number'},
                {data: 'plant'},
                {data: 'work_center'},
                {data: 'material'},
                {data: 'mat_desc'},
                {data: 'serial'},
                {data: 'wbs'},
                {data: 'start'},
                {data: 'end'},
                {data: 'user_status'},
                {data: 'tat'},
                {data: 'status'},
            ],
            ajax: {
                type: 'POST',
                url: "<?= base_url() ?>index.php/Tat_component/view_detail_table",
                data: {
                    'plan': plan,
                    'tipe': tipe,
                    'param': param,
                    'status': '',
                    'week': week,
                },
                // beforeSend: function () {
                // $('#loadings').fadeIn(1000);
                // },
                dataType: 'JSON',
                dataSrc: function (json) {
                    $('html, body').animate({
                        scrollTop: $(".tabel_atas").position().top
                    }, 500);
                    if (json.status == 'Tidak ada data') {
                        var return_data = [];
                        return_data.push({
                            'order_number': '<center style="width: 202px;">No data available in table</center>',
                            'plant': '<left></left>',
                            'work_center': '<left></left>',
                            'material': '<left></left>',
                            'mat_desc': '<center></center>',
                            'serial': '<left></left>',
                            'wbs': '<left></left>',
                            'start': '<center></center>',
                            'end': '<left></left>',
                            'user_status': '<left></left>',
                            'tat': '<center></center>',
                            'status': '',
                        });

                        return return_data;

                    }
                    else {
                        var u = 1;

                        var totala = '';
                        var return_data = [];
                        for (var i = 0; i < json.tabel.length; i++) {
                            var tat = parseInt(json.tabel[i].tat);
                            var jumlah_hari = parseInt(json.tabel[i].jumlah_hari);
                            var presentase = json.tabel[i].presentase;
                            console.log(jumlah_hari, tat);
                            if (jumlah_hari > tat) {
                                var status = 'Failed';
                            } else {
                                var status = 'Succed';
                            }
                            return_data.push({
                                'order_number': json.tabel[i].order_number,
                                'plant': json.tabel[i].plant,
                                'work_center': json.tabel[i].work_center,
                                'material': json.tabel[i].material,
                                'mat_desc': json.tabel[i].mat_desc,
                                'serial': json.tabel[i].serial,
                                'wbs': json.tabel[i].wbs,
                                'start': json.tabel[i].start,
                                'end': json.tabel[i].end,
                                'user_status': json.tabel[i].user_status,
                                'tat': json.tabel[i].tat,
                                'status': status+" - ("+presentase+"%)",
                            });
                            u++
                        }
                        return return_data;

                    }

                    // $('#loadings').fadeOut(1000);
                }
            },
            rowCallback: function (row, data, index) {
                if (data['status'].indexOf('Failed') != -1) {
                    $(row).css('color', '#F24738');
                }
            }
        });

    }

    function detail_plan_table_belum(plan, tipe, param) {
        $(".tabel_bawah").show("slow");
        $(".name_plan").html(plan);
        var status = $("#aircraft").val();
        var table = $("#tabelselect_belum").DataTable({
            "destroy": true,
            "serverSide": false,
            "processing": false,
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            // "scrollY": '50vh',
            "scrollCollapse": true,

            columns: [
                {data: 'order_number'},
                {data: 'plant'},
                {data: 'work_center'},
                {data: 'material'},
                {data: 'mat_desc'},
                {data: 'serial'},
                {data: 'wbs'},
                {data: 'start'},
                {data: 'user_status'},
                {data: 'tat'},
                {data: 'status'},
            ],
            ajax: {
                type: 'POST',
                url: "<?= base_url() ?>index.php/Tat_component/view_detail_table",
                data: {
                    'plan': plan,
                    'tipe': tipe,
                    'param': param,
                    'status': status,
                },
                // beforeSend: function () {
                // $('#loadings').fadeIn(1000);
                // },
                dataType: 'JSON',
                dataSrc: function (json) {
                    $('html, body').animate({
                        scrollTop: $(".tabel_bawah").position().top
                    }, 500);
                    if (json.status == 'Tidak ada data') {
                        var return_data = [];
                        return_data.push({
                            'order_number': '<center style="width: 202px;">No data available in table</center>',
                            'plant': '<left></left>',
                            'work_center': '<left></left>',
                            'material': '<left></left>',
                            'mat_desc': '<center></center>',
                            'serial': '<left></left>',
                            'wbs': '<left></left>',
                            'start': '<center></center>',
                            'user_status': '<left></left>',
                            'tat': '<center></center>',
                            'status': '',
                        });

                        return return_data;

                    }
                    else {
                        var u = 1;

                        var totala = '';
                        var return_data = [];
                        for (var i = 0; i < json.tabel.length; i++) {
                            var tat = parseInt(json.tabel[i].tat);
                            var jumlah_hari = parseInt(json.tabel[i].jumlah_hari);
                            return_data.push({
                                'order_number': json.tabel[i].order_number,
                                'plant': json.tabel[i].plant,
                                'work_center': json.tabel[i].work_center,
                                'material': json.tabel[i].material,
                                'mat_desc': json.tabel[i].mat_desc,
                                'serial': json.tabel[i].serial,
                                'wbs': json.tabel[i].wbs,
                                'start': json.tabel[i].start,
                                'user_status': json.tabel[i].user_status,
                                'tat': tat,
                                'status': jumlah_hari,
                            });
                            u++
                        }
                        return return_data;

                    }
                    // $('#loadings').fadeOut(1000);
                }

            },
            rowCallback: function (row, data, index) {
                if (data['status'] > data['tat'] && data['status'] != '') {
                    // $(row).css('background-color', 'rgb(255, 97, 97)');
                    $(row).css('color', '#F24738');
                }
            }
        });

    }

    $("#aircraft").on("change", function (e) {
        e.preventDefault();
        $("#search").trigger('click');
    });


    function panel_proses(plan) {
        var tipe = $("#aircraft").val();
        $.ajax({
            url: "<?= base_url() ?>index.php/Tat_component/view_panel_proses",
            type: "POST",
            data: {
                'plan': plan,
                'tipe': tipe,
            },
            dataType: "JSON",
            beforeSend: function () {
                $('#loadings').fadeIn(1000);
            },
            success: function (data) {
                if (plan == 'TCA') {
                    $("#valpa_tca").html(data.val)
                } else if (plan == 'TCE') {
                    $("#valpa_tce").html(data.val)
                } else if (plan == 'TCW') {
                    $("#valpa_tcw").html(data.val)
                } else {
                    $("#valpa_tcy").html(data.val)
                }

                $('#loadings').fadeOut(1000);
            }

        });
    }


</script>
