<style>
    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }

    .wadah {
        padding-left: 5px;
        padding-right: 5px;
    }

    .center {
        text-align: center;
    }

    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0e1f2e;
    }
</style>

<section class="content-header">
    <!--   <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1> -->
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <?php foreach ($breadcrumb as $data): ?>
            <li><?= $data ?></li>
        <?php endforeach; ?>
    </ol>
</section>
<br>


<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Master TAT Component Shop</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- <div class="button-add-place pull-left">
                      <a class="btn btn-flat bg-navy" id="downloadFormat" name="downloadFormat">Format Upload <i class="fa fa-cloud-download"></i></a>
                      </a>
                    </div> -->
                    <div class="button-add-place pull-right">
                        <a class="btn btn-flat bg-navy" id="add" name="button">
                            Upload Data <i class="fa fa-cloud-upload"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover table-gmf" id="table-upload">
                        <thead class="bg-navy">
                        <tr>
                            <th class="center">No</th>
                            <th class="center">Material</th>
                            <th class="center">Description</th>
                            <th class="center">TAT</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal_add" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233">
                <h4 class="modal-title" id="title-edit" style="color: #fff">Upload Data TAT Component Shop</h4>
            </div>
            <div class="modal-body">
                <form action="" name="formModal" id="formModal" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        <!-- <div class="form-group">
                          <label class="col-sm-3 control-label">Year</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control pull-right" id="datepicker" name="datepicker">
                          </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Upload Data</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control" name="files" id="files" value="" required>
                            </div>
                        </div>
                        <br><br>
                        <div class="form-group col-sm-12">
                            <a id="downloadFormat" style="cursor: pointer;" name="downloadFormat">Download Format Upload
                                <i class="fa fa-cloud-download"></i></a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i
                                    class="fa fa-rotate-left"></i> Cancel
                        </button>
                        <button type="submit" id="upload" class="pull-right btn btn-flat bg-navy"><i
                                    class="fa fa-save"></i> Save
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade modal-fullscreen" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                Some great content
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script>

    $("#upload").prop("disabled", true);

    function openFile(file) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        var val;
        switch (extension) {
            case 'xlsx':
                val = true;
                break;
            case 'xls':
                val = true;
                break;
            default :
                val = false;
                break;
        }
        return val;
    }

    function dialogWarning(message) {
        swal({
            title: "Warning",
            text: message,
            showConfirmButton: true,
            type: 'warning'
        });
    }

    function dialogSuccess(message) {
        swal({
            title: "Done",
            text: message,
            timer: 1500,
            showConfirmButton: false,
            type: 'success'
        });
    }

    var table;        
    table = $('#table-upload').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?php echo site_url('index.php/Tat_component/list_data') ?>",
            "type": "POST"
        },

    });



    var fileUpload = document.getElementById('files');
    $('#files').bind('change', function () {
        console.log(this.files[0].type);
        console.log(this.files[0].name);
        console.log(openFile(this.files[0].name));

        if (this.files[0].size >= 10000000) { // size >= 10 MB
            $("#upload").prop("disabled", true);
            dialogWarning("Max Size File Upload 10Mb");
        } else if (!openFile(this.files[0].name)) {
            $("#upload").prop("disabled", true);
            dialogWarning("Format File yang diperbolehkan : File Excel (xlsx dan xls)");
        } else {
            $("#upload").prop("disabled", false);
        }
    });

    $("#formModal").on("submit", function (e) {
        waitingDialog.show("Upload File, Please Wait...");
        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url() ?>index.php/Tat_component/uploadFile",
            type: "POST",
            processData: false,
            contentType: false,
            data: new FormData(this),
            // dataType: "JSON",
            success: function (results) {
                waitingDialog.hide();
                res = JSON.parse(results);
                if (res.message == 'Sukses') {
                    dialogSuccess(res.message);
                    reload_table();
                } else {
                    dialogWarning(res.message);
                }
            },
            error: function (status, errorThrown) {
                console.log(status);
                console.log(errorThrown);
                waitingDialog.hide();
                dialogWarning("Gagal Upload File!! (" + status[0] + ")\n" + errorThrown);
            }
        });
    });

    $("#downloadFormat").on("click", function (e) {
        window.location.href = "<?php echo base_url() ?>index.php/Tat_component/exportExcel";
    });

    $('#add').click(function (e) {
        $('#modal_add').modal("show")
    });

    $('#datepicker').datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax
    }

    $('#upload-data').DataTable();

</script>
.
