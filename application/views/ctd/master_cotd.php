<style>
    .wadah {
        padding-left: 1px;
        padding-right: 0px;
    }

    .highcharts-title {
        cursor: pointer;
    }

    .hijau {
        background-color: #00E676 !important;
        color: white;
    }
    .text_red {
        color: #F24738;
    }

    .putih {
        background-color: #ffff !important;
    }
</style>

<section class="content-header">
    <h1>
        <?= $title ?>
        <small><?= $small_tittle ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <?php foreach ($breadcrumb as $data): ?>
            <li><?= $data ?></li>
        <?php endforeach; ?>
    </ol>
</section>

<section class="content">
    <div class="">
        <div class="box-body">
            <div class="row">

                <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <section class="content">
                        <div class="box" style="padding-left: 10px; padding-right: 10px">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= $title ?></h3>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="period_start" class="col-sm-4 control-label"
                                                       style="margin-top: 5px;">Period Start</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm pull-right"
                                                           id="date_start" value="<?= '01-'.date('Y'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="period_end" class="col-sm-4 control-label"
                                                       style="margin-top: 5px;">Period End</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control input-sm pull-right"
                                                           id="date_end" value="<?= date('m-Y'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <button type="button" class="btn btn-default pull-right"
                                                    onclick="search_data()"><i class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <br>

                            </div>
                        </div>

                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">COTD Target</h3>
                            </div>

                            <div class="box-body">
                                <br>
                                <form action="#" id="form-input" class="form-horizontal">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="inputTarget" class="col-sm-2 control-label"
                                                   style="margin-top: 5px;">Target Garuda</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="terget_garuda"
                                                       name="terget_garuda" placeholder="Target Garuda"
                                                       value="<?php echo(!empty($target_garuda) ? $target_garuda->target_val : ""); ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="inputTarget" class="col-sm-2 control-label"
                                                   style="margin-top: 5px;">Target Citilink</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="terget_citylink"
                                                       name="terget_citylink" placeholder="Target Citylink"
                                                       value="<?php echo(!empty($target_citylink) ? $target_citylink->target_val : ""); ?>">
                                            </div>
                                            <div class="col-sm-1">
                                                <button id="button_save" type="button" class="btn btn-primary"
                                                        onclick="confirm_save()"><i class="fa fa-save"></i> Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <p><b>Current Year: <?php echo date('Y'); ?></b></p>
                                </form>

                            </div>
                            <!-- /.box-body -->
                        </div>

                        <div class="box" style="padding-left: 10px; padding-right: 10px">
                            <div class="box-header with-border">
                                <h3 class="box-title">Mitigation</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                            data-toggle="tooltip"
                                            title="Collapse">
                                        <i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover table-gmf" id="table-cotd">
                                        <thead style="background-color: #05354D">
                                            <tr>
                                                <th class="center">No</th>
                                                <th class="center">Date</th>
                                                <th class="center">Aircraft</th>
                                                <th class="center">Aircraft Type</th>
                                                <th class="center">Target</th>
                                                <th class="center">Actual</th>
                                                <th class="center">Action</th>
                                                <th class="text-center">color</th>
                                            </tr>
                                            </thead>

                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="modal_form" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233">
                <h4 class="modal-title" id="title-edit" style="color: #fff">Modal Form</h4>
            </div>
            <div class="modal-body">
                <form action="" id="form-modal" name="form-modal" class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <input type="hidden" class="form-control pull-right" id="mitigasi_id" name="mitigasi_id">
                            <input type="hidden" class="form-control pull-right" id="mitigasi_date"
                                   name="mitigasi_date">
                            <input type="hidden" class="form-control pull-right" id="mitigasi_aircraft"
                                   name="mitigasi_aircraft">
                            <input type="hidden" class="form-control pull-right" id="mitigasi_fk_id"
                                   name="mitigasi_fk_id">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Why</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control pull-right" id="input_why" name="input_why">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Solution</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control pull-right" id="input_solution"
                                           name="input_solution">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i
                                        class="fa fa-rotate-left"></i> Cancel
                            </button>
                            <button type="button" id="upload" class="pull-right btn btn-flat bg-navy"
                                    onclick="saveModal()"><i class="fa fa-save"></i> Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<script>

    var table;

    $(document).ready(function () {
        table = $('#table-cotd').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('index.php/master_cotd/data_list') ?>",
                "type": "POST",
            },
            "createdRow": function (row, data, dataIndex) {
                if (data[7]) {
                    $(row).addClass('text_red');
                }
            },
            "columnDefs": [
                {
                    "targets": [-1],
                    "orderable": false,
                },
                {
                    "targets": [7],
                    "visible": false,
                    "searchable": false
                },
            ],

        });
    });

    $("#terget_garuda, #terget_citylink").TouchSpin({
        min: 0,
        max: 1,
        step: 0.01,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10
        // postfix: '%'
    });

    function dialogSuccess(message) {
        swal({
            title: "Done",
            text: message,
            timer: 1500,
            showConfirmButton: false,
            type: 'success'
        });
    }

    $('#date_end, #date_start').datepicker({
        todayBtn: "linked",
        format: 'mm-yyyy',
        viewMode: 'months',
        minViewMode: 'months',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    function confirm_save() {
        swal({
            title: "Warning",
            text: "Simpan Data Target . ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#5568EE",
            confirmButtonText: "Simpan",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    ajax_save();
                });
            },
            allowOutsideClick: false
        });
    }

    function ajax_save() {
        var formData = new FormData($('#form-input')[0]);
        return $.ajax({
            url: "<?php echo base_url() ?>index.php/master_cotd/save",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON"
        })
            .done(function (data) {
                if (data.sukses) {
                    swal("Sukses", "Data Berhasil Disimpan", "success");
                    reload_table();
                }else{
                    swal("Warning", "Data Target Gagal disimpan", "warning");
                }
            })
            .fail(function () {
                swal("Oops...", "Something went wrong with ajax !", "error");
            })
    }

    // function showDialog(){
    //     swal({
    //         title: "Are you sure?",
    //         text: "Simpan Data Target .?",
    //         type: "warning",
    //         showCancelButton: true,
    //         confirmButtonColor: "#5778EF",
    //         confirmButtonText: " Yes ",
    //         cancelButtonText: " No ",
    //         closeOnConfirm: false,
    //         closeOnCancel: false
    //       },
    //       function(isConfirm) {
    //         if (isConfirm) {
    //           save();
    //         } else {
    //           swal("Cancelled", "Batal menyimpan data target", "error");
    //         }
    //       });

    // }

    // function save(){
    //   var formData = new FormData($('#form-input')[0]);
    //   $.ajax({
    //     url: "<?php echo base_url() ?>index.php/master_cotd/save",
    //     type: "POST",
    //     data: formData,
    //     contentType: false,
    //     processData: false,
    //     dataType: "JSON"
    //   })
    //   .done(function(data){
    //     if(data.sukses)
    //     {
    //         swal("Success", "Data Target Berhasil disimpan", "success");
    //     }
    //   })
    //   .fail(function(){
    //     swal("Oops...", "Something went wrong with ajax !", "Error");
    //   })
    // }

    function saveModal() {
        var formData = new FormData($('#form-modal')[0]);
        $.ajax({
            url: "<?php echo base_url() ?>index.php/master_cotd/save_modal",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
                console.log(data);
                if (data.sukses) {
                    dialogSuccess(data.message);
                }else{
                    console.log(data.message);
                }
                reload_table();
                $('#modal_form').modal('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                alert('Error adding / update data');
            }
        });
    }

    function ajax_edit(month, year, aircraft) {
        console.log(month);
        $('#form-modal')[0].reset();
        $('[name="mitigasi_id"]').prop('readonly', true);

        $.ajax({
            url: '<?php echo base_url() . "index.php/master_cotd/show_update/" ?>',
            type: "POST",
            data: {'month': month, 'year': year, 'aircraft': aircraft},
            dataType: "JSON",
            success: function (data) {
                $('[name="mitigasi_id"]').val("");

                if (data.edit) {
                    id = data.mitigasi_solution.target_id;

                    $('[name="mitigasi_id"]').val(data.mitigasi_solution.mitigasi_id);
                    $('[name="input_why"]').val(data.mitigasi_solution.mitigasi_why);
                    $('[name="input_solution"]').val(data.mitigasi_solution.mitigasi_solution);
                    $('[name="mitigasi_aircraft"]').val(data.mitigasi_solution.mitigasi_aircraft);
                    $('[name="mitigasi_date"]').val(data.mitigasi_solution.mitigasi_date);
                    $('.modal-title').text(data.mitigasi_solution.mitigasi_aircraft+'/'+data.mitigasi_solution.mitigasi_date);
                } else {
                    $('[name="mitigasi_aircraft"]').val(data.mitigasi.ac_type);
                    $('[name="mitigasi_date"]').val(data.mitigasi.tahun + "-" 
                        + ('0'+data.mitigasi.bulan).slice( -2 ));
                    $('.modal-title').text(data.mitigasi.ac_type+'/'+data.mitigasi.bulan+'/'+data.mitigasi.tahun);
                }

                $('#modal_form').modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function search_data() {
        var p_start = $('#date_start').val();
        var p_end = $('#date_end').val();
        console.log(p_start);
        console.log(p_end);

        var start = $("#date_start").val().split("-");
        var month_start = start[0];
        var start = start[1];
        var end = $("#date_end").val().split("-"); 
        var month_end = end[0];
        var end = end[1];

        if(start==end){
          if (month_end<month_start) {
            notif("warning", "Start Period Harus Lebih kecil dari End Period !!!");
          }else{
            table.ajax.url("<?php echo base_url() . 'index.php/master_cotd/data_list/'?>" + p_start + "/" + p_end).load();
          }
        }else{
            table.ajax.url("<?php echo base_url() . 'index.php/master_cotd/data_list/'?>" + p_start + "/" + p_end).load();
        }
    }

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax
    }

</script>
