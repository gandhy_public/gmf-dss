<style>
	.wadah {
		padding-left: 5px;
		padding-right: 5px;
	}

	div.dataTables_wrapper {
		width: 500px;
		margin: 0 auto;
	}
</style>

<section class="content-header">
	<h1>
		<?= $title ?>
		<small><?= $small_tittle ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
		<?php foreach ($breadcrumb as $data): ?>
			<li><?= $data ?></li>
		<?php endforeach; ?>
	</ol>
</section>


<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">General Chart</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">

			<div class="col-lg-2">
			</div>

			<div class="col-lg-4">
				<div class="form-group">
					<div class="small-box bg-navy">
						<span><h4 style="color:#F2C573;cursor:pointer;" href="#tabel_detail" id="panel_garuda"
								  title="Show Detail">Garuda</h4></span>
						<div class="row" style="margin-top:45px;">
							<div class="col-lg-5" style="text-align: center;margin-top: 12px;">
								<h5>Actual</h5>
							</div>
							<div class="col-lg-7">
								<h3 style="cursor:pointer;" href="#tabel_detail" id="panel_garuda" title="Show Detail">
									0,40 <span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>
								</h3>
							</div>

						</div>
						<hr style="margin-top:0px;margin-bottom:0px;">
						<div class="row">
							<div class="col-lg-5" style="text-align: center;    margin-top: -2px;">
								<h5>Target</h5>
							</div>
							<div class="col-lg-7">
								<h3 style="cursor:pointer;" href="#tabel_detail" id="panel_garuda" title="Show Detail">
									0,54 <span class="description-percentage "><i style="color:#F2C573;"
																				  class="fa fa-minus"></i></span></h3>
							</div>

						</div>
						<br/><br/><br/>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<div class="small-box bg-navy">
						<span><h4 style="color:#F2C573;cursor:pointer;    text-align: right;" href="#tabel_detail"
								  id="panel_garuda" title="Show Detail">Citilink</h4></span>
						<div class="row" style="margin-top:45px;">
							<div class="col-lg-5" style="text-align: center;margin-top: 12px;">
								<h5>Actual</h5>
							</div>
							<div class="col-lg-7">
								<h3 style="cursor:pointer;" href="#tabel_detail" id="panel_garuda" title="Show Detail">
									0,40 <span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>
								</h3>
							</div>

						</div>
						<hr style="margin-top:0px;margin-bottom:0px;">
						<div class="row">
							<div class="col-lg-5" style="text-align: center;    margin-top: -2px;">
								<h5>Target</h5>
							</div>
							<div class="col-lg-7">
								<h3 style="cursor:pointer;" href="#tabel_detail" id="panel_garuda" title="Show Detail">
									0,54 <span class="description-percentage "><i style="color:#F2C573;"
																				  class="fa fa-minus"></i></span></h3>
							</div>

						</div>
						<br/><br/><br/>
					</div>
				</div>
			</div>

			<div class="col-lg-2">
			</div>


		</div>
	</div>


	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= $title ?></h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
						title="Collapse">
					<i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">

			<div class="row" style="margin-bottom: 20px;">
				<div class="col-sm-3">
					<div class="form-group">
						<label for="inputEmail3" style="margin-top: 7px;"
							   class="col-sm-3 control-label">Aircraft</label>

						<div class="col-sm-9">
							<select class="form-control" style="width: 100%;" id="aircraft">
								<option selected="selected">- Select Aircraft -</option>
								<option>Boeing 737</option>
								<option>Boeing 770</option>
								<option>Boeing 700</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-4 control-label">Delay Type</label>

						<div class="col-sm-8">
							<select class="form-control" style="width: 100%;" id="delay_type">
								<option selected="selected">- Select Type -</option>
								<option>Delay by Frequent</option>
								<option>Delay by Duration</option>
								<option>Delay by Departure (Total)</option>
								<option>Delay by Departure (Hours)</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">

						<label for="inputEmail3" class="col-sm-4 control-label">Start Date</label>

						<div class="col-sm-8">
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>

								<input type="text" class="form-control pull-right" id="date_start"
									   value="<?= date('Y-m-d', strtotime(" -1 month", strtotime(date("Y-m-d")))); ?>">
							</div>
						</div>

					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">

						<label for="inputEmail3" class="col-sm-4 control-label">End Date</label>

						<div class="col-sm-8">
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>

								<input type="text" class="form-control pull-right" id="date_end"
									   value="<?= date('Y-m-d'); ?>">
							</div>
						</div>

					</div>
				</div>
				<br/>
				<div class="col-sm-12" style="text-align:center;margin-top: 8px;">
					<button type="button" class="btn btn-primary btn-flat" id="searc"><i class="fa fa-search"></i>
						Search
					</button>
				</div>
			</div>
			<div class="row">
				<div class="wadah col-lg-2 col-md-2 col-sm-12 col-xs-12">
				</div>
				<div class="wadah col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title"></h3>
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"
										data-toggle="tooltip"
										title="Collapse">
									<i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="box-header with-border" style="border-bottom: 1px solid #fff;">
										<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div id="grafik_general" style="height:250px;width:100%"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr/>

			<!--			detailDelayShow-->
			<div class="row" style="display: none" id="detail">
				<h3 class="box-title-detail" style="text-align: center">TitleResponsif</h3>
				<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="wadah col-lg-6 col-md-5 col-sm-12 col-xs-12">
						<div class="box">
							<div class="box-header with-border">
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"
											data-toggle="tooltip"
											title="Collapse">
										<i class="fa fa-minus"></i></button>
								</div>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="box-header with-border" style="border-bottom: 1px solid #fff;">
											<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div id="detail_delay_grafik1"
													 style="height:250px;width:100%"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="wadah col-lg-6 col-md-5 col-sm-12 col-xs-12">
						<div class="box">
							<div class="box-header with-border">
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"
											data-toggle="tooltip"
											title="Collapse">
										<i class="fa fa-minus"></i></button>
								</div>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="box-header with-border" style="border-bottom: 1px solid #fff;">
											<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div id="detail_delay_grafik2"
													 style="height:250px;width:100%"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="wadah col-lg-6 col-md-5 col-sm-12 col-xs-12">
						<table class="display nowrap dataTableWithScroll" style="width:100%">
							<thead>
							<tr>
								<th>Aircraft Type</th>
								<th>No 1</th>
								<th>No 2</th>
								<th>No 3</th>
								<th>No 4</th>
								<th>No 5</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>NA233</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							<tr>
								<td>NA233</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							<tr>
								<td>NA233</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							<tr>
								<td>NA233</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							<tr>
								<td>NA233</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							<tr>
								<td>NA233</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="wadah col-lg-6 col-md-5 col-sm-12 col-xs-12">
						<table class="display nowrap dataTableWithScroll" style="width:100%">
							<thead>
							<tr>
								<th>Aircraft Type</th>
								<th>No 1</th>
								<th>No 2</th>
								<th>No 3</th>
								<th>No 4</th>
								<th>No 5</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>B334</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							<tr>
								<td>B334</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							<tr>
								<td>B334</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							<tr>
								<td>B334</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							<tr>
								<td>B334</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							<tr>
								<td>B334</td>
								<td>12</td>
								<td>20</td>
								<td>30</td>
								<td>40</td>
								<td>50</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="wadah col-lg-6 col-md-5 col-sm-12 col-xs-12">
						<div class="box">
							<div class="box-header with-border">
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"
											data-toggle="tooltip"
											title="Collapse">
										<i class="fa fa-minus"></i></button>
								</div>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="box-header with-border" style="border-bottom: 1px solid #fff;">
											<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div id="detail_delay_grafik3"
													 style="height:250px;width:100%"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="wadah col-lg-6 col-md-5 col-sm-12 col-xs-12">
						<div class="box">
							<div class="box-header with-border">
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"
											data-toggle="tooltip"
											title="Collapse">
										<i class="fa fa-minus"></i></button>
								</div>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="box-header with-border" style="border-bottom: 1px solid #fff;">
											<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div id="detail_delay_grafik4"
													 style="height:250px;width:100%"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<script>
	//grafik detail
	function detailDelayShow(aircraft) {
		$("#detail").show();
		$(".box-title-detail").html(aircraft);

		Highcharts.chart('detail_delay_grafik1', {
			title: {
				text: 'Title'
			},
			credits: {
				enabled: false
			},
			yAxis: {
				title: {
					text: 'Occurrences'
				},
				min: 1,
				max: 6
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle'
			},

			plotOptions: {
				series: {
					label: {
						connectorAllowed: false
					},
					pointStart: 0.1
				}
			},

			series: [{
				name: 'Data1',
				data: [1, 3, 6]
			}, {
				name: 'Data2',
				data: [2, 4, 6]
			}, {
				name: 'Data3',
				data: [1, 5, 3]
			}, {
				name: 'Total',
				color: '#af0005',
				data: [1, 5, 6]
			}],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}
		});
		Highcharts.chart('detail_delay_grafik2', {
			title: {
				text: 'Title'
			},
			credits: {
				enabled: false
			},
			yAxis: {
				title: {
					text: 'Delay Length (Hours)'
				},
				min: 1,
				max: 6
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle'
			},

			plotOptions: {
				series: {
					label: {
						connectorAllowed: false
					},
					pointStart: 0.1
				}
			},

			series: [{
				name: 'Data1',
				data: [1, 3, 6]
			}, {
				name: 'Data2',
				data: [2, 4, 6]
			}, {
				name: 'Data3',
				data: [1, 5, 3]
			}, {
				name: 'Total',
				color: '#af0005',
				data: [1, 5, 6]
			}],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}
		});
		Highcharts.chart('detail_delay_grafik3', {
			title: {
				text: 'Title'
			},
			credits: {
				enabled: false
			},
			yAxis: {
				title: {
					text: 'Delay Length (Hours)'
				},
				min: 1,
				max: 6
			},
			xAxis: {
				categories: ['CGK', 'SUB', 'UPG']
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle'
			},

			plotOptions: {
				series: {
					label: {
						connectorAllowed: false
					},
					pointStart: 0.1
				}
			},

			series: [{
				name: 'Data1',
				data: [1, 3, 6]
			}, {
				name: 'Data2',
				data: [2, 4, 6]
			}, {
				name: 'Data3',
				data: [1, 5, 3]
			}, {
				name: 'Total',
				color: '#af0005',
				data: [1, 5, 6]
			}],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}
		});
		Highcharts.chart('detail_delay_grafik4', {
			title: {
				text: 'Title'
			},
			credits: {
				enabled: false
			},
			yAxis: {
				title: {
					text: 'Delay Length (Hours)'
				},
				min: 1,
				max: 6
			},
			xAxis: {
				categories: ['CGK', 'SUB', 'UPG']
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle'
			},

			plotOptions: {
				series: {
					label: {
						connectorAllowed: false
					},
					pointStart: 0.1
				}
			},

			series: [{
				name: 'Data1',
				data: [1, 3, 6]
			}, {
				name: 'Data2',
				data: [2, 4, 6]
			}, {
				name: 'Data3',
				data: [1, 5, 3]
			}, {
				name: 'Total',
				color: '#af0005',
				data: [1, 5, 6]
			}],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}
		});
	}

	//master grafik
	$(function () {
		Highcharts.chart('grafik_general', {
			title: {
				text: 'Detail Delay'
			},
			credits: {
				enabled: false
			},
			yAxis: {
				title: {
					text: 'Constant'
				},
				min: 0,
				max: 1
			},
			xAxis: {
				categories: ['Jan', 'Feb', 'Mar']
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle'
			},

			plotOptions: {
				series: {
					label: {
						connectorAllowed: false
					},
					cursor: 'pointer',
					point: {
						events: {
							click: function () {
								detailDelayShow(this.series.name);
							}
						}
					}
				}
			},

			series: [{
				name: 'Garuda',
				color: '#0110af',
				data: [0.1, 0.5, 1]
			}, {
				name: 'Citilink',
				color: '#07af94',
				data: [0.2, 0.3, 0.5]
			}, {
				name: 'Target',
				color: '#af0005',
				data: [0.6, 0.2, 0.1]
			}],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}
		});
	});

	$('#date_end, #date_start').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true
	});
</script>
