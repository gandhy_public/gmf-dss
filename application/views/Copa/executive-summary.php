<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>


<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
        <iframe id="myIframe" style="width: 100%;" src="https://app.powerbi.com/view?r=eyJrIjoiNzExMGY0ZTktOGI4My00NjE5LWI0ZTAtNWZiOGVmNjViOTVhIiwidCI6IjYwN2QwYjMzLTk1NWUtNGE2Yi1hMGQwLWY5ZjEyOTkxMWE2NSIsImMiOjEwfQ%3D%3D" frameborder="0" allowFullScreen="true"></iframe>
    </div>
  </div>
</section>

<script>
 $(function () {
        document.getElementById('myIframe').style.height = 'calc(230vh - 630px)';            
    });
</script>