<section class="content-header" style="padding: 6px 15px 0 15px !important">
    <h1 style="font-size: 19px !important">
        <?= $title ?>
        <small><?= $small_tittle ?></small>
    </h1>
    <ol class="breadcrumb" style="padding: 0px 5px !important">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <?php foreach ($breadcrumb as $data): ?>
            <li><?= $data ?></li>
        <?php endforeach; ?> -->
    </ol>
</section>


<section class="content" style="margin-top: -3px; width: 100%; margin-bottom: -5px">
    <div class="box" style="width: 100%;">
        <div class="box-header with-border" style="padding: 3px !important">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right" style="top: 0px !important">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body"> 
            <form id="formRefresh">
              <div class="box-body">
                <div class="form-group col-sm-6">
                  <label class="col-sm-6 control-label">Setting Interfal</label>
                  <div class="col-sm-6">
                  <input type="number" class="form-control" name="interfal" id="interfal">
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label class="col-sm-6 control-label">Setting Satuan</label>
                  <div class="col-sm-6">
                    <select class="form-control" id="satuan" name="satuan">
                        <option value="menit">MENIT</option>
                        <option value="jam">JAM</option>
                        <option value="hari">HARI</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <table border="0" width="100%">
                    <tr>
                        <td>
                            <h3 align="left">Auto Refresh : <?=$interfal?> <?= strtoupper($satuan)?></h3>
                        </td>
                        <td>
                            <button align="right" type="submit" class="btn btn-flat bg-navy" id="button-save"> Save</button>
                        </td>
                    </tr>
                </table>
                <!-- <button type="submit" class="btn btn-flat bg-navy" id="button-save"> Save</button> -->
              </div>
            </form>
        </div>
    </div>
</section>

<script>
    $(function() {
        $('#formRefresh').on('submit', function (e) {
            $('#button-save').text('Processing...'); //change button text
            $('#button-save').attr('disabled', true); //set button disable 
            e.preventDefault();
            var formData = new FormData($('#formRefresh')[0]);
            $.ajax({
                url: "<?php echo site_url('index.php/Copa/set_refresh') ?>",
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                error: function(){
                    notif('error', 'Connection disorders occurred, Please try again');
                },
                success: function (data) {
                    $('.content').append(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Error !!!");
                }
            });
        });
    })
</script>