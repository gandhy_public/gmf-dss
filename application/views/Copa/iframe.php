<section class="content-header" style="padding: 6px 15px 0 15px !important">
    <h1 style="font-size: 19px !important">
        <?= $title ?>
        <small><?= $small_tittle ?></small>
    </h1>
    <ol class="breadcrumb" style="padding: 0px 5px !important">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <?php foreach ($breadcrumb as $data): ?>
            <li><?= $data ?></li>
        <?php endforeach; ?>
    </ol>
</section>


<section class="content" style="margin-top: -3px; width: 100%; margin-bottom: -5px">
    <div class="box" style="width: 100%;">
        <div class="box-header with-border" style="padding: 3px !important">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right" style="top: 0px !important">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body"> 
            <iframe id="myIframe" name="iframe1" style="width: 100%; height: 530px" src="<?php echo $iframe_url; ?>" frameborder="0" allowFullScreen="true"></iframe>
        </div>
    </div>
</section>

<script>
    /*function refresh(){
        document.all.iframe1.src = document.all.iframe1.src;
    }
    window.setInterval("refresh()",10000);*/

    <?php if($waktu > 0){?>
        $(document).ready(function() {
          setInterval(function() {
            cache_clear()
          }, <?=$waktu?>);
        });
    <?php }?>

    function cache_clear() {
        console.log("reload...");
        window.location.reload(true);
        //window.location.reload(); use this if you do not remove cache
    }

    $(function () {
        document.getElementById('myIframe').height = Math.round(document.getElementById('myIframe').clientWidth / 1500 /* 1280 */ * 540 /* 986 */) + 48;
        /*window.onresize = function () {
            $('body').html('<div style="text-align:center;width:100%;"><img style="width:50%;" src="<?php echo base_url(); ?>/assets/image/loading.gif" /></div>');
            location.reload();
        };*/
        //document.getElementById('myIframe').style.height = 'calc(230vh - 611px)';
    });
</script>