<style>
    .color-title{
        font-color:#ffffff;
    }
    .spinner {
      margin: 100px auto;
      width: 50px;
      height: 40px;
      text-align: center;
      font-size: 10px;
    }

    .spinner > div {
      background-color: #333;
      height: 100%;
      width: 6px;
      display: inline-block;

      -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
      animation: sk-stretchdelay 1.2s infinite ease-in-out;
    }

    .spinner .rect2 {
      -webkit-animation-delay: -1.1s;
      animation-delay: -1.1s;
    }

    .spinner .rect3 {
      -webkit-animation-delay: -1.0s;
      animation-delay: -1.0s;
    }

    .spinner .rect4 {
      -webkit-animation-delay: -0.9s;
      animation-delay: -0.9s;
    }

    .spinner .rect5 {
      -webkit-animation-delay: -0.8s;
      animation-delay: -0.8s;
    }

    @-webkit-keyframes sk-stretchdelay {
      0%, 40%, 100% { -webkit-transform: scaleY(0.4) }
      20% { -webkit-transform: scaleY(1.0) }
    }

    @keyframes sk-stretchdelay {
      0%, 40%, 100% {
        transform: scaleY(0.4);
        -webkit-transform: scaleY(0.4);
        }  20% {
          transform: scaleY(1.0);
          -webkit-transform: scaleY(1.0);
        }
      }
      .stripe {
        white-space: nowrap;
        background-color: white;
      }
    .lds-ellipsis {
      display: inline-block;
      position: relative;
      width: 64px;
      height: 64px;
    }
    .lds-ellipsis div {
      position: absolute;
      top: 27px;
      width: 11px;
      height: 11px;
      border-radius: 50%;
      background: #080a44;
      animation-timing-function: cubic-bezier(0, 1, 1, 0);
    }
    .lds-ellipsis div:nth-child(1) {
      left: 6px;
      animation: lds-ellipsis1 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(2) {
      left: 6px;
      animation: lds-ellipsis2 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(3) {
      left: 26px;
      animation: lds-ellipsis2 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(4) {
      left: 45px;
      animation: lds-ellipsis3 0.6s infinite;
    }
    @keyframes lds-ellipsis1 {
      0% {
        transform: scale(0);
      }
      100% {
        transform: scale(1);
      }
    }
    @keyframes lds-ellipsis3 {
      0% {
        transform: scale(1);
      }
      100% {
        transform: scale(0);
      }
    }
    @keyframes lds-ellipsis2 {
      0% {
        transform: translate(0, 0);
      }
      100% {
        transform: translate(19px, 0);
      }
    }
    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }

    .wadah {
        padding-left: 5px;
        padding-right: 5px;
    }

    .title-manufacture {
        color: #F2C573;
        text-align: center;
        font-size: 18px;
        font-weight: bold;
        margin-top: 0px;
    }

    .description-block > .description-header {
        margin: 0;
        padding: 0;
        font-weight: bold;
        margin-top: 2px
    }

    .inner_in {
        padding: 0px;
        border-radius: 10px;
    }

    .small-box > .inner {
        padding: 3px;
    }

    .widget-user .widget-user-header {
        padding: 35px;
        height: 35px;
        border-top-right-radius: 11px;
        border-top-left-radius: 11px;
    }

    .widget-user .widget-user-image {
        position: absolute;
        top: 5px;
        left: 45%;
    }

    .box .border-right {
        border-right: 1px solid #eac8c8;
    }

    .form-group {
        margin-bottom: 5px;
    }

    .widget-user .box-footer {
        padding-top: 0px;
    }

    .box-footer {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 11px;
        border-bottom-left-radius: 11px;
        border-top: 1px solid #f4f4f4;
        padding: 0px;
        background-color: #fff;
    }

    .mini-icon {
        margin-top: 3px;
        margin-right: 12px;
    }

    .description-block {
        display: block;
        margin: 0px 0;
        text-align: center;
    }

    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 10px;
        width: 100%;
        box-shadow: 0 8px 12px 4px rgba(0, 0, 0, 0.2);
    }

    .aircraft_title {
        cursor: pointer;
    }

    .h4, .h5, .h6, h4, h5, h6 {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0e1f2e;
    }
    .color-progres-red{
        background-color: #dd4b39;
    }
    
</style>

<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title; ?></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body" style="padding: 5px;">
            <div class="row">
                <!-- Service Level Component Avaibility -->
                <div class="overlay-box" id="loader8"
                        style="/* display: none; */position:  absolute;background-color: #FFFFFD;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 39%;z-index: 99;">
                    <div style="" class="loading"></div>
                </div>
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <div class="box-header with-border" style="    margin-bottom: 5px;">
                        <div class="col-lg-6 col-sm-12 col-md-12 col-xs-12"
                             style="padding-left: 0px;border-right: 1px solid #f1f1f1">
                            <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12" style="padding-right: 0px">
                                <!-- GARUDA -->
                                <div class="box box-widget widget-user" style="border-radius: 11px;">
                                    <div class="widget-user-image" style="top: 10px;left: 45%;">
                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png" style="border: transparent;width: 100px;">
                                    </div>
                                    <div class="widget-user-header bg-navy">
                                        <h3 class="title-manufacture" style="margin-top: 4px;">GARUDA</h3>
                                    </div>
                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-sm-6 border-right" style="padding-right: 0px;">
                                                <div class="description-block">
                                                    <h5 class="description-header colors_garuda"
                                                        style="font-size: 16px;" id="actual_garuda"></h5>
                                                    <span class="description-text"
                                                          style="font-size: 11px;">Actual</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6" style="padding-left: 0px;">
                                                <div class="description-block">
                                                    <h5 class="description-header" style="font-size: 16px;"
                                                        id="target_garuda">
                                                    </h5>
                                                    <span class="description-text"
                                                          style="font-size: 11px;">Target</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12" style="padding-right: 0px">
                                <!-- CITILINK -->
                                <div class="box box-widget widget-user" style="border-radius: 11px;">
                                    <div class="widget-user-image" style="top: 10px;left: 45%;">
                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png" style="border: transparent;width: 100px;">
                                    </div>
                                    <div class="widget-user-header bg-navy">
                                        <h3 class="title-manufacture" style="margin-top: 4px;">CITILINK</h3>
                                    </div>
                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-sm-6 border-right" style="padding-right: 0px;">
                                                <div class="description-block">
                                                    <h5 class="description-header colors_citilink"
                                                        style="font-size: 16px;" id="actual_citilink"></h5>
                                                    <span class="description-text"
                                                          style="font-size: 11px;">Actual</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6" style="padding-left: 0px;">
                                                <div class="description-block">
                                                    <h5 class="description-header" style="font-size: 16px;"
                                                        id="target_citilink">
                                                    </h5>
                                                    <span class="description-text"
                                                          style="font-size: 11px;">Target</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12" style="padding-right: 0px">
                                <!-- NON GA -->
                                <div class="box box-widget widget-user" style="border-radius: 11px;">
                                    <div class="widget-user-image" style="top: 10px;left: 45%;">
                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png" style="border: transparent;width: 100px;">
                                    </div>
                                    <div class="widget-user-header bg-navy">
                                        <h3 class="title-manufacture" style="margin-top: 4px;">NON GA</h3>
                                    </div>
                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-sm-6 border-right" style="padding-right: 0px;">
                                                <div class="description-block">
                                                    <h5 class="description-header colors_nonga" style="font-size: 16px;"
                                                        id="actual_non_garuda"></h5>
                                                    <span class="description-text"
                                                          style="font-size: 11px;">Actual</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6" style="padding-left: 0px;">
                                                <div class="description-block">
                                                    <h5 class="description-header" style="font-size: 16px;"
                                                        id="target_non_garuda"></h5>
                                                    <span class="description-text"
                                                          style="font-size: 11px;">Target</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12"
                             style="border-right: 1px solid #f1f1f1;padding-left: 0px;padding-right: 0px">
                            <h5 style="margin-left: 5px"><i class="fa fa-plane"></i> GARUDA</h5>
                            <div class="scrollable-content" id="garuda_type"
                                 style="height: 140;overflow: auto;padding:0px;overflow-x: hidden;">
                            </div>
                        </div>

                        <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12"
                             style="border-right: 1px solid #f1f1f1;padding-left: 0px;padding-right: 0px">
                            <h5 style="margin-left: 5px"><i class="fa fa-plane"></i> CITILINK</h5>
                            <div class="col-lg-12 scrollable-content" id="citilink_type"
                                 style="height: 140;overflow: auto;padding:0px;overflow-x: hidden;">
                            </div>
                        </div>

                        <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12"
                             style="padding-left: 0px;padding-right: 0px">
                            <h5 style="margin-left: 5px"><i class="fa fa-plane"></i> NON GA</h5>
                            <div class="col-lg-12 scrollable-content" id="non_ga_type"
                                 style="height: 140;overflow: auto;padding:0px;overflow-x: hidden;">
                            </div>
                        </div>

                    </div>
                    <br><br> <br>
                </div>

               


                <!-- GRAFIK UTAMA -->
                <form action="" name="form_search" id="form_search" method="post" enctype="multipart/form-data">
                     <input type="hidden" name="status" value="1">
                    <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-5 control-label" style="margin-top: 5px;">Start Month</label>
                            <div class="col-sm-7">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" name="periode1" id="periode1"
                                           value="<?php echo "01-" . date('Y'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="col-sm-5 control-label" style="margin-top: 5px;">End Month</label>
                            <div class="col-sm-7">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" name="periode2" id="periode2"
                                           value="<?php echo date('m-Y'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <button class="btn btn-flat bg-navy" id="submit">
                                <i class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </div>
                </form>
                
                <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12">
                <br class="">
                <br class="">
                <br class="">
                    <div class="overlay-box loadinggrafik"
                            style="/* display: none; */position:  absolute;background-color: #FFFFFD;/* margin: auto; */top: 100 !important;left: 12px;right: 9px;width: 100%;height: 68%;z-index: 1;">
                        <div style="" class="loading"></div>
                    </div>
                    <div id="grafik_apu1" style="height:170px;width:100%;"></div>
                </div>
                <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12">
                <br class="">
                <br class="">
                <br class="">
                    <div class="overlay-box loadinggrafik"
                            style="/* display: none; */position:  absolute;background-color: #FFFFFD;/* margin: auto; */top: 100 !important;left: 12px;right: 9px;width: 100%;height: 68%;z-index: 1;">
                        <div style="" class="loading"></div>
                    </div>
                    <div id="grafik_apu2" style="height:170px;width:100%;"></div>
                </div>
                <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12">
                <br class="">
                <br class="">
                <br class="">
                    <div class="overlay-box loadinggrafik"
                            style="/* display: none; */position:  absolute;background-color: #FFFFFD;/* margin: auto; */top: 100 !important;left: 12px;padding-right: 2px;width: 95%;height: 68%;z-index: 1;">
                        <div style="" class="loading"></div>
                    </div>
                    <div id="grafik_apu3" style="height:170px;width:100%;"></div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>

    $(document).ready(function () {

        $("#periode1").datepicker({
            minViewMode: 1,
            format: 'mm-yyyy',
            autoclose: true
        });
        $("#periode2").datepicker({
            minViewMode: 1,
            format: 'mm-yyyy',
            autoclose: true
        });
        panel();
        search();

        var today = new Date();
        var month = today.getMonth() + 1;
        if (month < 10) month = '0' + month;
        var year = today.getFullYear();
        var thisMonth = month + '-' + year;

        $.ajax({
            url: '<?= base_url() ?>index.php/SLA_Component_Availability/searchgrafik',
            type: 'POST',
            data: {
                periode1: '01' + '-' + year,
                periode2: thisMonth,
                status: 0
            },
            beforeSend: function () {
                $(".loadinggrafik").fadeIn("fast");
            },
        })
            .done(function (resp) {
                // var data =JSON.parse(resp);
                grafik(resp);
                console.log("success");
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
                $(".loadinggrafik").fadeOut(1000);
            });

        $('.scrollable-content').css('height', '140px');

        $(document).on('click', ".detail", function(event) {
            event.preventDefault();
            var aircraft = $(this).data("aircraft");
            var actype = $(this).data("actype");
            var reg = $(this).data("reg");
            var thn = $(this).data("thn");
            var bln = $(this).data("bln");

            // alert();
            $("#modal_add_mitigation").modal('show');
            $.ajax({
                url: '<?= base_url() ?>index.php/SLA_Component_Availability/detai_on_month',
                type: 'POST',
                data: {
                    func_loc: reg,
                    aircrf: aircraft,
                    type: actype,
                    year: thn,
                    month: bln
                },
                beforeSend: function () {
                    $("#progres").html('');
                    $("#title_modal2").html("Detail " + actype);
                    $("#loadingApprove_modal_add_mitigation").show();
                    $("#modal_add_mitigation").modal('show');
                    console.log("loading");
                }
            })
                .done(function (resp) {
                    var data = JSON.parse(resp);
                    var htmls="<div class='col-md-12'>";
                    if(data.prints.length!=0){
                        $.each(data.prints, function (index, el) { 
                            htmls+="<br>"+
                                    "<div class='col-md-9'><p>"+el.skema+"</p>"+
                                    "<div class='progress'>"+
                                        "<div class='progress-bar "+el.color+"' role='progressbar'  aria-valuemin='0' aria-valuemax='100' style='width: "+el.aktual+"%'>"+
                                        "<span class='sr-only'></span> "+el.aktual+" %"+
                                        "</div>"+
                                    "</div>"+
                                    "</div>"+
                                    "<div class='col-md-3'><br> "+
                                    "<button class='btn btn-xs detailMonth' data-toggle='tooltip' data-placement='top' title='Detail Robbing' data-aircraft='"+el.aircraft+"'  data-type='"+el.type+"'  data-month='"+el.month+"' data-year='"+year+"' data-skema='"+el.skema+"' data-robbing='"+el.robbing+"'>  Robbing : "+el.robbing+" Item</button>"+
                                    "</div>"+
                                    "<div class='row'></div>";
                        });
                    }
                    htmls+="<br>"+
                                    "<div class='col-md-9'><p>Target</p>"+
                                    "<div class='progress'>"+
                                        "<div class='progress-bar progress-bar-green' role='progressbar'  aria-valuemin='0' aria-valuemax='100' style='width: "+data.target+"%' "+
                                        "<span class='sr-only'></span> "+data.target+" %"+
                                        "</div>"+
                                    "</div>"+
                                    "</div>"+
                                    "<div class='col-md-3'>"+
                                    "</div>"+
                                    "<div class='row'></div>";

                    htmls+="</div>";
                    $("#progres").html(htmls);
                    
                    console.log("success");
                })
                .fail(function () {
                    console.log("error");
                    notif('error', 'Check Your Connection !')
                })
                .always(function () {
                    console.log("complete");
                    $("#loadingApprove_modal_add_mitigation").hide('slow/400/fast', function () {

                    });
                });

        });

        $(document).on('click','.detailMonth',function(e){
            e.preventDefault();
            var robbing=$(this).data('robbing');
            if(robbing==0){
                notif('warning', "Data Robbing Not Avability");
            }else{
                var aircraft=$(this).data('aircraft');
                var aircraft=$(this).data('aircraft');
                var type=$(this).data('type');
                var month=$(this).data('month');
                var year=$(this).data('year');
                if(year=='tahun'){
                    var d = new Date();
                    var n = d.getFullYear();
                    year=n;
                }
                var skema=$(this).data('skema');
                $('#dateRobbing').html('Detail Robbing Skema '+skema+'  '+month+'-'+year)
                $('#detailRobbing').modal('show');
                robbingMontly(aircraft,type,month,year,skema);
            }
            
        });
        // $('.detailMonth').click(function (e) { 
        //     e.preventDefault();
        //     alert('a');
        // });

    });

    function search() {
        $("#form_search").submit(function (event) {
            event.preventDefault();
            var formData = new FormData($('#form_search')[0]);
            $.ajax({
                url: '<?= base_url() ?>index.php/SLA_Component_Availability/searchgrafik',
                type: 'POST',
                processData: false,
                contentType: false,
                data: formData,
                beforeSend: function () {
                $(".loadinggrafik").fadeIn("fast");
            },
            })
                .done(function (resp) {
                    notif('success', "Data successfully displayed");
                    grafik(resp);
                })
                .fail(function () {
                    notif('error', 'Please.. Check Your Connection !');
                })
                .always(function () {
                    $(".loadinggrafik").fadeOut(1000);
                });
        });
    }

    function panel() {
        $.ajax({
            url: '<?= base_url() ?>index.php/SLA_Component_Availability/type_pesawat',
            type: 'POST',
            beforeSend:function(){
                $('#loader8').fadeIn("fast");
            }
        })
            .done(function (resp) {
                var data        = JSON.parse(resp);
                var citilink    = "";
                var ga          = "";
                var non_ga      = "";
                var tampung_ga    = new Array();
                var tampung_citi  = new Array();
                var tampung_nga   = new Array();
                
                if(data.data_scc.GA.length!=0){
                    $.each(data.data_scc.GA, function (index, el) { 
                    var target  = ((el.target == null) ? 0 : el.target.toFixed(2));
                    var act     = ((el.actual == null) ? 0 : el.actual.toFixed(2));
                    var color   = ((el.actual <= target) ? '#f24738' : '#8ec3a7');
                    tampung_ga.push(el.actual);
                    ga+="<div class='wadah col-md-12'>";
                        ga+="<div class='box box-widget widget-user' style='border-radius: 11px;'>";
                            ga+="<div class='widget-user-header bg-navy' style='padding: 2px;height: 20px;'>"+
                                "<h3 class='title-manufacture aircraft_title hoverclass' style='font-size: 15px;'  onclick=aircraft('"+el.actype+"','GA')>"+el.actype+"<i class='fa fa-ellipsis-v pull-right' style='font-size: 10px;padding-right: 7px;padding-top: 4px;'></i></h3>"+
                            "</div>"+
                            "<div class='box-footer' style='padding: 0px;'>"+
                                "<div class='row'>"+
                                "<div class='col-sm-6 border-right' style='padding-left: 13px;'>"+
                                    "<div class='description-block'>"+
                                    "<h5 class='description-header colors_nonga' style='font-size:15px;margin-left: 10px;color:"+color+"'>"+act+ "%</h5>"+
                                    "<span class='description-text' style='font-size: 12px;margin-left: 10px;'>Actual</span>"+
                                    "</div>"+
                                "</div>"+
                                "<div class='col-sm-6' style='padding-left: 5px;'>"+
                                    "<div class='description-block'>"+
                                    "<h5 class='description-header' style='font-size:15px'>"+target+ "%</h5>"+
                                    "<span class='description-text' style='font-size: 12px;margin-left:0px;'>Target</span>"+
                                    "</div>"+
                                "</div>"+
                            "</div>"+
                            "</div>";
                        ga+="</div>";
                    ga+="</div>";
                    });
                    
                }
                if(data.data_scc.CITILINK.length!=0){
                    $.each(data.data_scc.CITILINK, function (index, el) { 
                    var target  = ((el.target == null) ? 0 : el.target.toFixed(2));
                    var act     = ((el.actual == null) ? 0 : el.actual.toFixed(2));
                    var color   = ((el.actual <= target) ? '#f24738' : '#8ec3a7');
                    tampung_citi.push(el.actual);
                    citilink+="<div class='wadah col-md-12'>";
                        citilink+="<div class='box box-widget widget-user' style='border-radius: 11px;'>";
                            citilink+="<div class='widget-user-header bg-navy' style='padding: 2px;height: 20px;'>"+
                                "<h3 class='title-manufacture aircraft_title hoverclass' style='font-size: 15px;'  onclick=aircraft('"+el.actype+"','CITILINK')>"+el.actype+"<i class='fa fa-ellipsis-v pull-right' style='font-size: 10px;padding-right: 7px;padding-top: 4px;'></i></h3>"+
                            "</div>"+
                            "<div class='box-footer' style='padding: 0px;'>"+
                                "<div class='row'>"+
                                "<div class='col-sm-6 border-right' style='padding-left: 13px;'>"+
                                    "<div class='description-block'>"+
                                    "<h5 class='description-header colors_nonga' style='font-size:15px;margin-left: 10px;color:"+color+"'>"+act+ "%</h5>"+
                                    "<span class='description-text' style='font-size: 12px;margin-left: 10px;'>Actual</span>"+
                                    "</div>"+
                                "</div>"+
                                "<div class='col-sm-6' style='padding-left: 5px;'>"+
                                    "<div class='description-block'>"+
                                    "<h5 class='description-header' style='font-size:15px'>"+target+ "%</h5>"+
                                    "<span class='description-text' style='font-size: 12px;margin-left:0px;'>Target</span>"+
                                    "</div>"+
                                "</div>"+
                            "</div>"+
                            "</div>";
                        citilink+="</div>";
                    citilink+="</div>";
                    });
                }
                if(data.data_scc.NGA.length!=0){
                    $.each(data.data_scc.NGA, function (index, el) { 
                    var target  = ((el.target == null) ? 0 : el.target.toFixed(2));
                    var act     = ((el.actual == null) ? 0 : el.actual.toFixed(2));
                    var color   = ((el.actual <= target) ? '#f24738' : '#8ec3a7');
                    tampung_nga.push(el.actual);
                    non_ga+="<div class='wadah col-md-12'>";
                        non_ga+="<div class='box box-widget widget-user' style='border-radius: 11px;'>";
                            non_ga+="<div class='widget-user-header bg-navy' style='padding: 2px;height: 20px;'>"+
                                "<h3 class='title-manufacture aircraft_title hoverclass' style='font-size: 15px;'  onclick=aircraft('"+el.actype+"','NGA')>"+el.actype+"<i class='fa fa-ellipsis-v pull-right' style='font-size: 10px;padding-right: 7px;padding-top: 4px;'></i></h3>"+
                            "</div>"+
                            "<div class='box-footer' style='padding: 0px;'>"+
                                "<div class='row'>"+
                                "<div class='col-sm-6 border-right' style='padding-left: 13px;'>"+
                                    "<div class='description-block'>"+
                                    "<h5 class='description-header colors_nonga' style='font-size:15px;margin-left: 10px;color:"+color+"'>"+act+ "%</h5>"+
                                    "<span class='description-text' style='font-size: 12px;margin-left: 10px;'>Actual</span>"+
                                    "</div>"+
                                "</div>"+
                                "<div class='col-sm-6' style='padding-left: 5px;'>"+
                                    "<div class='description-block'>"+
                                    "<h5 class='description-header' style='font-size:15px'>"+target+ "%</h5>"+
                                    "<span class='description-text' style='font-size: 12px;margin-left:0px;'>Target</span>"+
                                    "</div>"+
                                "</div>"+
                            "</div>"+
                            "</div>";
                        non_ga+="</div>";
                    non_ga+="</div>";
                    });
                }

                //Per Operator
                $("#non_ga_type").html(non_ga);
                $("#garuda_type").html(ga);
                $("#citilink_type").html(citilink);

                var total_act_ga    = (tampung_ga.reduce(add, 0)/tampung_ga.length).toFixed(2);
                var total_act_citi  = (tampung_citi.reduce(add, 0)/tampung_citi.length).toFixed(2);
                var total_act_nga   = (tampung_nga.reduce(add, 0)/tampung_nga.length).toFixed(2);
                
                var attr_ga;
                var attr_n_ga;
                var attr_citilink;

                if ((tampung_ga.reduce(add, 0)/tampung_ga.length) <= data.target_global) {
                    $('#actual_garuda').css('color', '#f24738');
                    attr_ga = '<i class="fa fa-caret-down" style="color:#f24738;">';
                } else {
                    attr_ga = '<i class="fa fa-caret-up" style="color:#8ec3a7;">';
                    $('#actual_garuda').css('color', '#8ec3a7');
                }
                if ((tampung_citi.reduce(add, 0)/tampung_citi.length) <= data.target_global) {
                    attr_citilink = '<i class="fa fa-caret-down" style="color:#f24738;">';
                    $('#actual_citilink').css('color', '#f24738');
                } else {
                    attr_citilink = '<i class="fa fa-caret-up" style="color:#8ec3a7;">';
                    $('#actual_citilink').css('color', '#8ec3a7');
                }
                if ((tampung_nga.reduce(add, 0)/tampung_nga.length) <= data.target_global) {
                    attr_n_ga = '<i class="fa fa-caret-down" style="color:#f24738;">';
                    $('#actual_non_garuda').css('color', '#f24738');
                } else {
                    attr_n_ga = '<i class="fa fa-caret-up" style="color:#8ec3a7;">';
                    $('#actual_non_garuda').css('color', '#8ec3a7');
                }
                // ACTUAL
                $("#actual_garuda").html(((total_act_ga > 100) ? 100 : total_act_ga) + attr_ga);
                $("#actual_citilink").html(((total_act_citi > 100) ? 100 : total_act_citi) + attr_citilink);
                $("#actual_non_garuda").html(((total_act_nga > 100) ? 100 : total_act_nga) + attr_n_ga);
                // TARGET
                $("#target_garuda").html(data.target_global + '%');
                $("#target_citilink").html(data.target_global + '%');
                $("#target_non_garuda").html(data.target_global + '%');
            })
            .fail(function () {
                notif('error', 'Please.. Check Your Connection !');
            })
            .always(function () {
                $('#loader8').fadeOut(1000);
            });
    }

    function add(a, b) {
        return a + b;
    }

    function aircraft(type, aircraft) {
        $.ajax({
            url: '<?= base_url() ?>index.php/SLA_Component_Availability/sladetailaircraft',
            type: 'POST',
            data: {
                param_aircraft: aircraft,
                param_type: type
            },
            beforeSend: function () {
                $("#title_modal2").html("Detail Aircraft " + type);
                $("#progres").html("");
                $("#loadingApprove_modal_add_mitigation").show();
                $("#modal_add_mitigation").modal('show');
            }
        })
            .done(function (resp) {
                var data = JSON.parse(resp);
                var htmls="<div class='col-md-12'>";
                if(data.prints.length!=0){
                    $.each(data.prints, function (index, el) { 
                         htmls+="<br>"+
                                "<div class='col-md-9'><p>"+el.skema+"</p>"+
                                "<div class='progress'>"+
                                    "<div class='progress-bar "+el.color+"' role='progressbar'  aria-valuemin='0' aria-valuemax='100' style='width: "+el.aktual+"%'>"+
                                    "<span class='sr-only'>"+el.aktual+" % Complete (success)</span> "+el.aktual+" %"+
                                    "</div>"+
                                "</div>"+
                                "</div>"+
                                "<div class='col-md-3'><br> "+
                                "<button class='btn btn-xs detailMonth' data-toggle='tooltip' data-placement='top' title='Detail Robbing' data-aircraft='"+aircraft+"'  data-type='"+type+"'  data-month='' data-year='tahun' data-skema='"+el.skema+"' data-robbing='"+el.robbing+"' >  Robbing : "+el.robbing+" Item</button>"+
                                "</div>"+
                                "<div class='row'></div>";
                    });
                }
                htmls+="<br>"+
                                "<div class='col-md-9'><p>Target</p>"+
                                "<div class='progress'>"+
                                    "<div class='progress-bar progress-bar-green' role='progressbar'  aria-valuemin='0' aria-valuemax='100' style='width: "+data.target+"%'>"+
                                    "<span class='sr-only'></span> "+data.target+" %"+
                                    "</div>"+
                                "</div>"+
                                "</div>"+
                                "<div class='col-md-3'>"+
                                "</div>"+
                                "<div class='row'></div>";

                htmls+="</div>";

                $("#progres").html(htmls);
                notif('success', "Data successfully displayed");
                console.log("success");
            })
            .fail(function () {
                console.log("error");
                notif('error', "Please... Check Your Connection");
            })
            .always(function () {
                console.log("complete");
                //$("#loadingApprove_modal_add_mitigation").show();
                $("#loadingApprove_modal_add_mitigation").hide('slow/400/fast');
            });

    }

    function grafik(resp) {
        var yAxisLimit  = [0, 25, 50, 75, 100];
        var data        = JSON.parse(resp);

        $.each(data, function (key, val){
            Highcharts.chart('grafik_apu1', {
                title: {
                    text: 'GARUDA',
                    margin: 0,
                },
                credits: {
                    enabled: false,
                },
                exporting: {
                    enabled: false
                },
                colors: [
                    '#0f2233',
                    '#05354D'
                ],
                xAxis: {
                    categories: data.month,
                    id: 'x-axis'
                },
                yAxis: {
                    title: {
                        text: 'Percentage (%)'
                    },
                    min: 0,
                    tickPositioner: function () {
                        return yAxisLimit;
                    },
                    // plotLines: [{
                    //     value: data.target,
                    //     color: '#F2C573',
                    //     dashStyle: 'shortdash',
                    //     width: 2,
                    //     label: {
                    //         text: 'Target ' + data.target
                    //     }
                    // }],
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    detail_grafik_cus(this.category, 'GA');
                                }
                            }
                        }
                    }
                },
                series: [{
                    type: 'areaspline',
                    fillColor: {
                        linearGradient: [0, 0, 0, 200],
                        stops: [
                            [0, "#0f2233"],
                            [1, "#fff3"]
                        ]
                    },
                    lineWidth: 1,
                    name: 'Actual',
                    data: val.GA,
                    showInLegend: false,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#000',
                        align: 'right',
                        format: '{point.y:.2f}', // one decimal
                        x: 0, // 10 pixels down from the top
                    }
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });

            Highcharts.chart('grafik_apu2', {
            title: {
                text: 'CITILINK',
                margin: 0,
            },
            credits: {
                enabled: false,
            },
            exporting: {
                enabled: false
            },
            colors: [
                '#0f2233',
                '#05354D'
            ],
            xAxis: {
                categories: data.month,
                id: 'x-axis'
            },
            yAxis: {
                title: {
                    text: 'Percentage (%)'
                },
                min: 0,
                tickPositioner: function () {
                    return yAxisLimit;
                },
                // plotLines: [{
                //     value: data.target,
                //     color: '#F2C573',
                //     dashStyle: 'shortdash',
                //     width: 2,
                //     label: {
                //         text: 'Target ' + data.target
                //     }
                // }],
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                detail_grafik_cus(this.category, 'CITILINK');
                            }
                        }
                    }
                }
            },
            series: [
                // {
                //   name: 'Target',
                //   data: data.target,
                //   marker: {
                //   enabled: false
                //   },
                //   showInLegend: false
                // },
                {
                    type: 'areaspline',
                    fillColor: {
                        linearGradient: [0, 0, 0, 200],
                        stops: [
                            [0, "#0f2233"],
                            [1, "#fff3"]
                        ]
                    },
                    lineWidth: 1,
                    name: 'Actual',
                    data: val.CITILINK,
                    showInLegend: false,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#000',
                        align: 'right',
                        format: '{point.y:.2f}', // one decimal
                        x: 0, // 10 pixels down from the top
                    }
                }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });
        Highcharts.chart('grafik_apu3', {
            title: {
                text: 'NON GA',
                margin: 0,
            },
            credits: {
                enabled: false,
            },
            exporting: {
                enabled: false
            },
            colors: [
                '#0f2233',
                '#05354D'
            ],
            xAxis: {
                categories: data.month,
                id: 'x-axis'
            },
            yAxis: {
                title: {
                    text: 'Percentage (%)'
                },
                min: 0,
                tickPositioner: function () {
                    return yAxisLimit;
                },
                // plotLines: [{
                //     value: data.target,
                //     color: '#F2C573',
                //     dashStyle: 'shortdash',
                //     width: 2,
                //     label: {
                //         text: 'Target ' + data.target
                //     }
                // }],
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                detail_grafik_cus(this.category, 'NON_GA');
                            }
                        }
                    }
                }
            },
            series: [{
                type: 'areaspline',
                fillColor: {
                    linearGradient: [0, 0, 0, 200],
                    stops: [
                        [0, "#0f2233"],
                        [1, "#fff3"]
                    ]
                },
                lineWidth: 1,
                name: 'Actual',
                data: val.NON_GA,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'right',
                    format: '{point.y:.2f}', // one decimal
                    x: 0, // 10 pixels down from the top
                }
            }
            ],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });
        });

        
    }

    function detail_grafik_cus(data, type) {
        datatable(data, type);
        $("#title_modal").html('Trend Data ' + data);
        $("#modal_table").modal('show');
    }

    function robbingMontly(aircraft,type,month,year,skema) { 
        $('#detailRobbingTable').DataTable().destroy();
        notif('success', "Data successfully displayed");
        $("#detailRobbingTable").DataTable({
            scrollY: "500px",
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            processing: true,
            serverSide: true,
            "ajax": {
                "url": '<?= base_url() ?>index.php/SLA_Component_Availability/TableDetailRobbing',
                "type": 'POST',
                "data": {
                    'aircrafts':aircraft,
                    'types':type,
                    'months':month,
                    'years':year,
                    'skemas':skema
                }
            }
        });

     }

    function datatable(data, type) {
        $('#table_ac').DataTable().destroy();
        notif('success', "Data successfully displayed");
        $("#table_ac").DataTable({
            scrollY: "500px",
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            // fixedColumns:   {
            //   leftColumns: 1
            // },
            columnDefs: [
                {width: 125, targets: 0}
            ],
            "ajax": {
                "url": '<?= base_url() ?>index.php/SLA_Component_Availability/detail_on_table_grafik',
                "type": 'POST',
                "data": {
                    'param1': data,
                    'param2': type
                }
            }
        });
    }

    

</script>


<div class="modal fade" id="modal_table">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-navy">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="title_modal" name="title_modal">
            </div>
            <div class="modal-body">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
                <table class="stripe row-border order-column table datatable table-bordered" id="table_ac" width="100%">
                    <thead class="bg-navy">
                    <th class="text-center textcolor">No</th>
                    <th class="text-center textcolor">A/C Type</th>
                    <th class="text-center textcolor">Action</th>
                    </thead>
                    <tbody class="text-center"></tbody>
                </table>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_add_mitigation">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-navy">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="title_modal2" name="title_modal2">
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div id="loadingApprove_modal_add_mitigation" class="back-loading"
                     style="display: none;background: white;position: relative;z-index: 9999;width: 99%;height: 100%;margin: 0 auto;left: 1;/* display: none; */left: 0;right: 0;/* top: 10px; */bottom: 0;">
                    <center>
                        <div class="lds-ellipsis">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </center>
                </div>
                <div id="progres"></div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<!--start modal detal SL-->
    <div class="modal fade" id="detailRobbing" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-navy">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="dateRobbing" name="dateRobbing">
            </div>
                <div class="modal-body">
                    <table class="table table-hover table-bordered table-gmf" id="detailRobbingTable" width="100%">
                        <thead style="background-color: #05354D">
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Material PN</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Donatur</th>
                            <th class="text-center">Recipient</th>
                            <th class="text-center">Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i
                                    class="fa fa-rotate-left"></i> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    end modal start detail sl-->
