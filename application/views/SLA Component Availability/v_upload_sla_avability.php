<style media="screen">
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin: 0;
  line-height: 1;
}
.spinner {
  margin: 30px auto;
  width: 50px;
  height: 40px;
  text-align: center;
  font-size: 10px;
}

.spinner > div {
  background-color:#333;
  height: 100%;
  width: 6px;
  display: inline-block;

  -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
  animation: sk-stretchdelay 1.2s infinite ease-in-out;
}

.spinner .rect2 {
  -webkit-animation-delay: -1.1s;
  animation-delay: -1.1s;
}

.spinner .rect3 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

.spinner .rect4 {
  -webkit-animation-delay: -0.9s;
  animation-delay: -0.9s;
}

.spinner .rect5 {
  -webkit-animation-delay: -0.8s;
  animation-delay: -0.8s;
}

@-webkit-keyframes sk-stretchdelay {
  0%, 40%, 100% { -webkit-transform: scaleY(0.4) }
  20% { -webkit-transform: scaleY(1.0) }
}

@keyframes sk-stretchdelay {
  0%, 40%, 100% {
    transform: scaleY(0.4);
    -webkit-transform: scaleY(0.4);
    }  20% {
      transform: scaleY(1.0);
      -webkit-transform: scaleY(1.0);
    }
  }
  .stripe {
    white-space: nowrap;
    background-color: white;
  }
  th{
    text-align: center;
  }

</style>
<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Upload Data</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="button-add-place pull-right">
            <button type="button" data-toggle="modal"  class="btn btn-flat bg-navy" name="add" id="add">Upload Data     <i class="fa fa-cloud-upload"></i></button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Master Target A/C Type </h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">

      <div class="row">
        <div class="col-md-12" >
          <button type="button" class="btn btn-flat bg-navy pull-right" name="add_target" id="add_target" ><i class="fa fa-plus"></i> Add</button></br>
        </div>
        <div class="col-md-12" style="padding-top:10px;">
          <table class="table table-hover table-bordered table-gmf" id="table_aircraft_target"style="width:100%;">
            <thead style="background-color: #05354D">
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">A/C Type</th>
                <th class="text-center">Target</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody class="text-center">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Table Data PN</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">

      <div class="row">
        <div class="col-md-12">
          <table class="table table-hover table-bordered table-gmf" id="table_pn">
            <thead style="background-color: #05354D">
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">PN REG</th>
                <th class="text-center">ESS</th>
                <th class="text-center">Part Number</th>
                <th class="text-center">Alternate</th>
                <th class="text-center">Description</th>
                <th class="text-center">Cap</th>
                <th class="text-center">Aircraft</th>
                <th class="text-center">Skema</th>
                <th class="text-center">Tot. Float Spare</th>
                <th class="text-center">Remarks</th>
              </tr>
            </thead>
            <tbody class="text-center">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Table Data MTYPE</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">

      <div class="row">
        <div class="col-md-12">
          <table class="table table-hover table-bordered table-gmf" id="table_mtype">
            <thead style="background-color: #05354D">
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">PART NUMBER</th>
                <th class="text-center">MTYPE</th>
              </tr>
            </thead>
            <tbody class="text-center">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <!-- The Modal -->
  <div class="modal fade" id="myModal"  style="border-radius:border-radius: 50px 20px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Upload Data SLA Component Availability</h4>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div id="loadingApprove" class="back-loading" style="display: none;background: white;position: absolute;z-index: 9999;width: 99%;height: 100%;margin: 0 auto;left: 1;/* display: none; */left: 0;right: 0;/* top: 10px; */bottom: 0;/* padding:  50%; */padding top:;/* padding-top:  50px; */">
            <h4 style="text-align:  center;margin-top:  50px;font-size: 20px;">Loading to save..</h4>
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
          </div>
          <form action="" name="formfile" id="formfile" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label>Select Type Template</label>
              <select class="form-control pull-right" name="tipe_template" id="tipe_template" required >
                <option value="">Pilih Template Data</option>
                <option value="database_pn">Data PN</option>
                <option value="database_mtype">Data MType</option>
              </select>
            </div>
            <div class="form-group">
              <label>Upload Files</label>
              <input type="file" class="form-control" name="files" id="files" required>
            </div>
            <div class="form-group">
              <label>Download Template</label>
              <div class="col-md-12">
                <div class="col-md-6">
                  <center>  <a href="<?= base_url() ?>index.php/SLA_Component_Availability/export_xls_datapn">Template Data PN</a></center>
                </div>
                <div class="col-md-6">
                  <center><a href="<?= base_url() ?>index.php/SLA_Component_Availability/export_xls_mtype">Template Data MTYPE</a></center>
                </div>
              </div>
            </div>
            <div class="row">

            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close  <i class="fa fa-rotate-left"></i></button>
            <button type="submit" name="submit" id="submit" class="btn btn-flat bg-navy"> Save <i class="fa fa-save"></i></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!--  -->

  <!-- The Modal -->
  <div class="modal fade" id="modal_add_target" name="modal_add_target"  style="border-radius:border-radius: 50px 20px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Input Target</h4>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div id="loadingApprove2" class="back-loading" style="display: none;background: white;position: absolute;z-index: 9999;width: 99%;height: 100%;margin: 0 auto;left: 1;/* display: none; */left: 0;right: 0;/* top: 10px; */bottom: 0;/* padding:  50%; */padding top:;/* padding-top:  50px; */">
            <h4 style="text-align:  center;margin-top:  50px;font-size: 20px;">Data Diproses</h4>
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
          </div>
          <form  class="form-horizontal" action="" name="formfile2" id="formfile2" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label class="control-label col-sm-3 pull-left" >A/C Type :</label>
              <div class="col-sm-9">
                <select class="form-control pull-right" name="aircraft" id="aircraft" required >
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3 pull-left" >Target :</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="val_target" name="val_target" placeholder="Enter Target" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3 pull-left" >Choose Year :</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="year_target" name="year_target" placeholder="Enter Years" value="<?php echo date('Y'); ?> " readonly>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close  <i class="fa fa-rotate-left"></i></button>
            <button type="submit" name="submit2" id="submit2" class="btn btn-flat bg-navy"> Save <i class="fa fa-save"></i></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!--  -->

  <!-- The Modal -->
  <div class="modal fade" id="modal_edit_target" name="modal_edit_target"  style="border-radius:border-radius: 50px 20px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Edit Target</h4>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div id="loadingApprove3" class="back-loading" style="display: none;background: white;position: absolute;z-index: 9999;width: 99%;height: 100%;margin: 0 auto;left: 1;/* display: none; */left: 0;right: 0;/* top: 10px; */bottom: 0;/* padding:  50%; */padding top:;/* padding-top:  50px; */">
            <h4 style="text-align:  center;margin-top:  50px;font-size: 20px;">Data Diproses</h4>
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
          </div>
          <form  class="form-horizontal" action="" name="formfile3" id="formfile3" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label class="control-label col-sm-2 pull-left" >A/C Type :</label>
              <div class="col-sm-10">
                <input type="hidden" class="form-control" id="aircraft_model_id" name="aircraft_model_id" >
                <input type="text" class="form-control" id="aircraft_model" name="aircraft_model" readonly >
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2 pull-left" >Target :</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="target_edit" name="target_edit" >
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close  <i class="fa fa-rotate-left"></i></button>
            <button type="submit" name="submit3" id="submit3" class="btn btn-flat bg-navy"> Save <i class="fa fa-save"></i></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!--  -->

</section>
<script type="text/javascript">
$(document).ready(function() {
   $(document).on("click", "#edit_target", function (e) {
    // alert($(this).data("id"));
    var param1=$(this).data("id");
    $.ajax({
      url: '<?=base_url()?>index.php/C_upload_sla_avability/edit_val_target',
      type: 'POST',
      data:{param:param1}
    })
    .done(function(resp) {
      var data=JSON.parse(resp);
      $("#aircraft_model_id").val(param1);
      $("#aircraft_model").val(data[0].type_aircraft);
      $("#target_edit").val(data[0].target_aircraft);
      $("#modal_edit_target").modal("show");
      console.log('success');
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });
  $("#add_target").click(function(event) {
    /* Act on the event */
    $("#aircraft").html('');
    var opsi='';
    $.ajax({
      url: '<?=base_url()?>index.php/C_upload_sla_avability/master_target',
      type: 'POST'
    })
    .done(function(resp) {
      var data= JSON.parse(resp);
      $.each(data, function( index, value ) {
        opsi+='<option value='+value+'>'+value+'</option>';
      });
      $("#aircraft").html(opsi);
      console.log(opsi);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      $("#modal_add_target").modal("show");
      console.log("complete");
    });
  });
  // =============================================================================
  $("#add").click(function(event) {
    /* Act on the event */
    $("#myModal").modal("show");
  });
  // =============================================================================
    function checkExtention(ini) {
      var namefile=ini.val();
      var ext=namefile.replace(/^.*\./, '');
      ext=ext.toLowerCase();
      extList=["xls","xlsx"];
      return ($.inArray(ext,extList)== -1) ? false:true;
    }
    $(document).on("change", "#files", function (e) {
      // console.log(checkExtention($(this)));
      if(this.files[0].size > 2000000 || checkExtention($(this)) == false){
        notif("error", "Cek Format File yang Diupload");
        $("#files").val('');
      }
    });
  // =============================================================================
  // Data Master A/C Type target
  var dt_master_target=$("#table_aircraft_target").DataTable({
    "processing":true,
    "serverSide":true,
    "ajax":{
      "url": '<?=base_url()?>index.php/C_upload_sla_avability/datatable_target',
      "type":'POST'
    }

  });
  // Data Table PN
  var dt_table_pn=$("#table_pn").DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": '<?= base_url() ?>index.php/C_upload_sla_avability/datatable_pn',
      "type" :'POST',
    }
  });
  var dt_table_mtype=$("#table_mtype").DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": '<?= base_url() ?>index.php/C_upload_sla_avability/datatable_mtype',
      "type" :'POST',
    }
    });
    $("#formfile3").submit(function(event) {
      /* Act on the event */
      event.preventDefault();
      // $('#submit2').attr('disabled', true);
      var formData = new FormData($('#formfile3')[0]);
      $.ajax({
        url:'<?= base_url() ?>index.php/C_upload_sla_avability/editstoretarget',
        type: 'POST',
        processData: false,
        contentType: false,
        data: formData
      })
      .done(function(resp) {
        var data=JSON.parse(resp);
        notif(data.info,data.msg);
        if (data.info=='success') {
          $("#modal_edit_target").modal("hide");
          dt_master_target.ajax.reload();
        }
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
        $("#formfile3")[0].reset();
      });
    });
    $("#formfile2").submit(function(event) {
      /* Act on the event */
      event.preventDefault();
      // $('#submit2').attr('disabled', true);
      var formData = new FormData($('#formfile2')[0]);
      $.ajax({
        url:'<?= base_url() ?>index.php/C_upload_sla_avability/storetarget',
        type: 'POST',
        processData: false,
        contentType: false,
        data: formData
      })
      .done(function(resp) {
        var data=JSON.parse(resp);
        notif(data.info,data.msg);
        if (data.info=='success') {
          $("#modal_add_target").modal("hide");
          dt_master_target.ajax.reload();
        }
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
        $("#formfile2")[0].reset();
      });

    });

  $("#formfile").submit(function(event) {
    /* Act on the event */
    event.preventDefault();
    $('#submit').text('Save...');
    $('#submit').attr('disabled', true);
    var formData = new FormData($('#formfile')[0]);
    $.ajax({
      url:'<?= base_url() ?>index.php/C_upload_sla_avability/storefiles',
      type: 'POST',
      processData: false,
      contentType: false,
      data: formData,
      beforeSend: function(){
        $('#loadingApprove').show();
      }
    })
    .done(function(resp) {
      var data=JSON.parse(resp);
      notif(data.data,data.msg);
      console.log("success");
    })
    .fail(function() {
      console.log("error");
      notif("error","Check Your Connection");
    })
    .always(function() {
      $('#loadingApprove').hide();
      console.log("complete");
      $('#submit').text('Save');
      $('#submit').attr('disabled', false);
      $("#myModal").modal("hide");
      $("#formfile")[0].reset();
      dt_table_pn.ajax.reload();
      dt_table_mtype.ajax.reload();
    });
  });

  $( "#target_edit,#val_target" ).keyup(function() {
    // $(this).val()
    if( $(this).val() > 100){
    notif('warning','Maximal Value Input 100 ');
    $("#target_edit").val('');
    $("#val_target").val('');
  }else{
    $(this).val($(this).val().replace(/[^0-9\.]/g,''));
  }
  });

  $(document).on('click', "#del_target", function(event) {
    event.preventDefault();
    /* Act on the event */
    var id = $(this).data("id");
    swal({
      title: 'Are you sure?',
      text: "Delete this data..",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn bg-navy',
      cancelButtonClass: 'btn bg-red',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      $.ajax({
        url: '<?= base_url() ?>index.php/C_upload_sla_avability/delstoretarget',
        type: 'POST',
        data:{param:id}
      })
      .done(function(reps) {
        var data= JSON.parse(reps);
        notif(data.info, data.msg);
      })
      .fail(function() {
        notif("error", "Failed to delete");
      })
      .always(function() {
        console.log("complete");
        dt_master_target.ajax.reload();
      });
    })
  });
  // =============================================================================



});

</script>
