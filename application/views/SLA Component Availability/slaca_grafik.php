
<style>
    .wadah{
        padding-left: 5px;
        padding-right: 5px;
    }
</style>

<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>



<section class="content">
	<div class="box">
		<div class="box-header with-border">
		  <h3 class="box-title"><?= $title ?></h3>
		  <div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
					title="Collapse">
			  <i class="fa fa-minus"></i></button>
		  </div>
		</div>
		<div class="box-body">
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="col-sm-3">
				<div class="form-group">
				
					<label for="inputEmail3" class="col-sm-4 control-label">Start Date</label>

					<div class="col-sm-8">
						<div class="input-group date">
							<div class="input-group-addon">
							  <i class="fa fa-calendar"></i>
							</div>
							
							<input type="text" class="form-control pull-right" id="date_start" value="<?= date('Y-m-d', strtotime(" -1 month", strtotime(date("Y-m-d")))); ?>">
						</div>
					</div>	
					
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
				
					<label for="inputEmail3" class="col-sm-4 control-label">End Date</label>

					<div class="col-sm-8">
						<div class="input-group date">
							<div class="input-group-addon">
							  <i class="fa fa-calendar"></i>
							</div>
							
							<input type="text" class="form-control pull-right" id="date_end" value="<?= date('Y-m-d'); ?>">
						</div>
					</div>	
					
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="form-group">
				  <label for="inputEmail3" style="margin-top: 7px;" class="col-sm-3 control-label">Aircraft</label>

				  <div class="col-sm-9">
					<select class="selectpicker" id="aircraft" data-live-search="false" multiple title="- Select Aircraft -">
					  <option selected>All</option>
					  <option>Garuda</option>
					  <option>Citilink</option>
					  <option>Non GA</option>
					</select>
				  </div>
				</div>
			</div>
			<div class="col-sm-3" style="text-align:center;">
				<button type="button"class="btn btn-primary btn-flat" id="searc"><i class="fa fa-search"></i> Search</button>
			</div>
		</div>
		
		<div class="row">
		
			<div class="wadah col-lg-2 col-md-2 col-sm-12 col-xs-12">
			</div>
			<div class="wadah col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Aircraft Percentage</h3>
					  <div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
								title="Collapse">
						  <i class="fa fa-minus"></i></button>
					  </div>
					</div>
					<div class="box-body">
						  <div class="row">
							
							<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="box-header with-border" style="border-bottom: 1px solid #fff;">
									<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div id="grafik_general" style="height:250px;width:100%"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
			
			<div class="wadah col-lg-2 col-md-2 col-sm-12 col-xs-12">
			</div>
		</div>	
		
		
		
		<div class="row">
			<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Aircraft Type</h3>
					  <div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
								title="Collapse">
						  <i class="fa fa-minus"></i></button>
					  </div>
					</div>
					<div class="box-body">
						  <div class="row">
							
							<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="box-header with-border" style="border-bottom: 1px solid #fff;">
									<div class="wadah col-lg-5 col-md-6 col-sm-6 col-xs-6">
										<div id="grafik_garuda" style="height:250px;width:100%"></div>
									</div>
									<div class="wadah col-lg-3 col-md-6 col-sm-6 col-xs-6">
										<div id="grafik_citilink" style="height:250px;width:100%"></div>
									</div>
									<div class="wadah col-lg-4 col-md-6 col-sm-6 col-xs-6">
										<div id="grafik_nonga" style="height:250px;width:100%"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>	

		<div class="row" id="detail_grafik" style="display:none">
		
					<div class="wadah col-lg-3 col-md-3 col-sm-12 col-xs-12">
					</div>
					<div class="wadah col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="box">
							<div class="box-header with-border">
							  <h3 class="box-title">Aircraft Type Detail</h3>
							  <div class="box-tools pull-right">
								
						<button type="button" class="btn btn-primary btn-flat" id="back"><i class="fa fa-arrow-left"></i> Back</button>
							  </div>
							</div>
							<div class="box-body">
								  <span class="name_aircraft" style=" text-decoration: underline;">Garuda</span><br />
								  <div class="row">
									
										<div class="wadah col-lg-8 col-md-8 col-sm-12 col-xs-12">
											<div class="box-header with-border" style="border-bottom: 1px solid #fff;">
												<div class="progress-group">
													<span class="progress-text">PBTH Availability</span>
													<span class="progress-number"><b>100 %</b></span>

													<div class="progress lg">
													  <div class="progress-bar" style="width: 100%;background-color:#0D2633"></div>
													</div>
												  </div>
											</div>
											<div class="box-header with-border" style="border-bottom: 1px solid #fff;    margin-top: -26px;">
												<div class="progress-group">
													<span class="progress-text">PBTH Access</span>
													<span class="progress-number"><b>90%</b></span>

													<div class="progress lg">
													  <div class="progress-bar" style="width: 90%;background-color:#F24738"></div>
													</div>
												  </div>
											</div>
											<div class="box-header with-border" style="border-bottom: 1px solid #fff;    margin-top: -26px;">
												<div class="progress-group">
													<span class="progress-text">Target</span>
													<span class="progress-number"><b>96%</b></span>

													<div class="progress lg">
													  <div class="progress-bar " style="width: 96% ;background-color:#8EC3A7"></div>
													</div>
												  </div>
											</div>
										</div>
										
										
										<div class="wadah col-lg-4 col-md-4 col-sm-12 col-xs-12">
											<div class="row" style="margin-top: 9px;">
												<div class="col-lg-5"><br />
													<span>Robbing</span>
												</div>
												<div class="col-lg-2"><br />
													<span>:</span>
												</div>
												
												<div class="col-lg-5"><br />
													<span>0 Item</span>
												</div>
											</div><div class="row" style="margin-top: 15px;">
												<div class="col-lg-5"><br />
													<span>Robbing</span>
												</div>
												<div class="col-lg-2"><br />
													<span>:</span>
												</div>
												
												<div class="col-lg-5"><br />
													<span>3 Item</span>
												</div>
											</div>
										</div>
										
									</div>
							</div>
						</div>	
					</div>
					
					
					<div class="wadah col-lg-3 col-md-3 col-sm-12 col-xs-12">
					</div>
				</div>			
		
		
				
		</div>
	</div>
</section>

<script>
// var d = new Date();
// var bulan = month[d.getUTCMonth()];
// var tahun = d.getUTCFullYear();

// $('#tahun').val(tahun);


$(document).ready(function () {
	
	$(document).on("click", "#back", function(e){
		$("#detail_grafik").hide();
	});
	
	function toggleSelectAll(control) {
		var allOptionIsSelected = (control.val() || []).indexOf("All") > -1;
		function valuesOf(elements) {
			return $.map(elements, function(element) {
				return element.value;
			});
		}

		if (control.data('allOptionIsSelected') != allOptionIsSelected) {
			// User clicked 'All' option
			if (allOptionIsSelected) {
				// Can't use .selectpicker('selectAll') because multiple "change" events will be triggered
				control.selectpicker('val', valuesOf(control.find('option')));
			} else {
				control.selectpicker('val', []);
			}
		} else {
			// User clicked other option
			if (allOptionIsSelected && control.val().length != control.find('option').length) {
				// All options were selected, user deselected one option
				// => unselect 'All' option
				control.selectpicker('val', valuesOf(control.find('option:selected[value!=All]')));
				allOptionIsSelected = false;
			} else if (!allOptionIsSelected && control.val().length == control.find('option').length - 1) {
				// Not all options were selected, user selected all options except 'All' option
				// => select 'All' option too
				control.selectpicker('val', valuesOf(control.find('option')));
				allOptionIsSelected = true;
			}
		}
		control.data('allOptionIsSelected', allOptionIsSelected);
	}

	$('#aircraft').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');
	
});

function detail_grafik(aircraft, category){
	$(".name_aircraft").html(aircraft +'('+category+')');
	$("#detail_grafik").show();
}

$(function () {
    Highcharts.chart('grafik_general', {
        chart: {
			height: 250,
            type: 'column'
        },
        credits:{
            enabled: false
        },
        title: {
            text: ''
        },
		 xAxis: {
			categories: ["Garuda","Citilink","Batik Air"],
			 title: {
                text: 'Aircraft'
            }
		},
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Percentage(%)'
            }
        },
		
		  tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}<br/>'
		  },
        legend: {
            enabled: true
        },
       
		series: [
			{
				name: "Target",
				data: [80, 87, 88],
				color : "#F24738"
			},
			{
				name: "Actual",
				data: [97, 95, 90],
				color : "#0D2633"
			},
		],
    });
	
	
    Highcharts.chart('grafik_garuda', {
        chart: {
			height: 250,
            type: 'column'
        },
        credits:{
            enabled: false
        },
        title: {
            text: 'Garuda'
        },
		 xAxis: {
			categories: ["B737NG","B777","B737CL","A330","ATR","CRJ"],
			 title: {
                text: 'Aircraft'
            }
		},
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Percentage(%)'
            }
        },
		
		  tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}<br/>'
		  },
        legend: {
            enabled: true
        },
		plotOptions: {
			series: {
				label: {
					connectorAllowed: false
				},
				cursor: 'pointer',
				point: {
					events: {
						click: function () {
							 detail_grafik("Garuda", this.category)
						}
					}
				}
			}
		},
       
		series: [
			{
				name: "Target",
				data: [80, 87, 88, 85, 87, 85],
				color : "#8EC3A7"
			},
			{
				name: "Actual",
				data: [97, 95, 90,97, 95, 90],
				color : "#F2C573"
			},
		],
    });
	Highcharts.chart('grafik_citilink', {
        chart: {
			height: 250,
            type: 'column'
        },
        credits:{
            enabled: false
        },
        title: {
            text: 'Citilink'
        },
		 xAxis: {
			categories: ["A320"],
			 title: {
                text: 'Aircraft'
            }
		},
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Percentage(%)'
            }
        },
		
		  tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}<br/>'
		  },
        legend: {
            enabled: true
        },
		plotOptions: {
			series: {
				label: {
					connectorAllowed: false
				},
				cursor: 'pointer',
				point: {
					events: {
						click: function () {
							 detail_grafik("Garuda", this.category)
						}
					}
				}
			}
		},
       
		series: [
			{
				name: "Target",
				data: [80],
				color : "#8EC3A7"
			},
			{
				name: "Actual",
				data: [97],
				color : "#F2C573"
			},
		],
    });
	
	Highcharts.chart('grafik_nonga', {
        chart: {
			height: 250,
            type: 'column'
        },
        credits:{
            enabled: false
        },
        title: {
            text: 'Non GA'
        },
		 xAxis: {
			categories: ["HBS","NAM"],
			 title: {
                text: 'Aircraft'
            }
		},
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Percentage(%)'
            }
        },
		plotOptions: {
			series: {
				label: {
					connectorAllowed: false
				},
				cursor: 'pointer',
				point: {
					events: {
						click: function () {
							 detail_grafik("Garuda", this.category)
						}
					}
				}
			}
		},
		
		  tooltip: {
			headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}<br/>'
		  },
        legend: {
            enabled: true
        },
       
		series: [
			{
				name: "Target",
				data: [80, 87],
				color : "#8EC3A7"
			},
			{
				name: "Actual",
				data: [97, 95],
				color : "#F2C573"
			},
		],
    });
	
});

 $('#date_end, #date_start').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true
    });
</script>