<?php
header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1" width="100%">
    <thead>
    <tr>
        <th>PART NUMBER</th>
        <th>MTYPE</th>
    </tr>
    </thead>
    <tbody>

    <?php $i = 1;
    foreach ($excels as $excel) { ?>
        <tr>
            <td><?php echo $excel->part_number; ?></td>
            <td><?php echo $excel->mtype; ?></td>
        </tr>
        <?php $i++;
    } ?>
    </tbody>
</table>
