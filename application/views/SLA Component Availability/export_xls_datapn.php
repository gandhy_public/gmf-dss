<?php
header("Content-type: application/export_example_xls");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <tdead>
    <tr>
      <td>PN REG</td>
      <td>ESS</td>
      <td>Part Number</td>
      <td>Alternate</td>
      <td>Description</td>
      <td>Cap</td>
      <td>Aircraft</td>
      <td>Skema</td>
      <td>Tot Float Spare</td>
      <td>Remarks</td>
    </tr>
    </tdead>
    <tbody>
      <?php
      foreach ($excels as $excel) { ?>
          <tr>
              <td><?php echo addslashes($excel->pnreg); ?></td>
              <td><?php echo addslashes($excel->ess); ?></td>
              <td><?php echo addslashes($excel->part_number); ?></td>
              <td><?php echo addslashes($excel->alternate); ?></td>
              <td><?php echo addslashes($excel->description); ?></td>
              <td><?php echo addslashes($excel->cap); ?></td>
              <td><?php echo addslashes($excel->aircraft); ?></td>
              <td><?php echo addslashes($excel->skema); ?></td>
              <td><?php echo addslashes($excel->tot_float_spare); ?></td>
              <td><?php echo addslashes($excel->remarks); ?></td>
          </tr>
          <?php
      } ?>
    </tbody>
</table>
