<section class="content">
  <h3><?php echo $title;?></h3>
  <div class="box">
      <div class="box-body">
        <div class="col-sm-8">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">Period Start</label>
             <div class="col-sm-3">
               <div class="input-group date">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right"  placeholder="Start Date" id="period1" value="<?= date('Y-m', strtotime(date("Y-m"))); ?>">
                </div>
            </div>
            <label for="inputEmail3" class="col-sm-3 control-label">Period End</label>
            <div class="col-sm-3">
             <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                 <input type="text" class="form-control pull-right"  placeholder="End Date" id="period2" value="<?= date('Y-m', strtotime(date("Y-m"))); ?>">
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <button type="button"class="btn btn-flat bg-navy" onclick="search()" id="search" ><i class="fa fa-search"></i> Search</button>
          </div>
        </div>
    </div>
  </div>
        <div class="col-md-12">
          <div class="row">
            <div class="box">
              <div class="box-header">
                <h4>Repetitive Problem Closing rate Target</h4>
              </div>
              <div class="box-body">
                <div class="row">
                <div class="col-md-8">
                  <form role="form" id="form_target_rpcr" method="post">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Target</label>
                    <div class="col-sm-3">
                      <input type="text" class="form-control" name="target" id="target" placeholder="Target">
                        <input type="hidden" value="<?= date('Y', strtotime(date("Y"))); ?>" name="year" id="year">
                    </div>
                  </form>
                    <div class="col-sm-3">
                      <button type="button" class="btn btn-success" onclick="save()"><i class="fa fa-save"></i>Save</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row" style="padding-top:20px;">
                <div class="col-md-12">
                <label>Current Year : <span><?= date('Y', strtotime(date("Y"))); ?></span></label>
              </div>
              </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="box">
              <div class="box-body">
            <h4> Mitigation </h4>
              <table id="tabelmitigation" class="table table-hover table-bordered table-gmf">

                <thead>
                <th class="text-center">Date</th>
                <th class="text-center">Aircrafe</th>
                <th class="text-center">Aircrafe Type</th>
                <th class="text-center">Start Date</th>
                <th class="text-center">Due Date</th>
                <th class="text-center">Action</th>
                </thead>

              <tbody class="text-center">

              </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>

</section>

<div class="modal fade" id="modal_add_mitigation">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
        <h4 class="modal-title">Mitigation</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label>Why?</label>
            <input type="text" class="form-control" placeholder="Why..." id="why">
        </div>
        <div class="form-group">
            <label>Solution</label>
            <input type="text" class="form-control" placeholder="Solution..." id="solution">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close  <i class="fa fa-rotate-left"></i></button>
          <button type="button" class="btn btn-success pull-right">Save</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
for(var i=1; i<=6; i++){
   //Date picker
   $('#period'+i).datepicker({
   format: "yyyy-mm",
     viewMode: "months",
     minViewMode: "months",
     autoclose: true
 });

   $('#dateyear'+i).datepicker({
     format: "yyyy",
     viewMode: "years",
     minViewMode: "years",
     autoclose: true
   });
}

$(function(){
  get_target();
  data_table1();
});

function save(){
  var targer = $('#target').val();
  $.ajax({
                type: 'POST',
                dataType: "json",
                url:'<?= base_url() ?>index.php/Master_RPCR/target_rpcr',
                data: $('#form_target_rpcr').serialize(),
                success: function (data) {

                      if(data = "1"){
                      notif("success", "Telah Berhasil Ditambahkan");
                      }else{
                      notif("success", "Telah Berhasil Diubah");
                      }
                      get_target();
                }
            });

}

function mitigation_modal(){
  $('#modal_add_mitigation').modal('show');
}

function get_target(){
  $.post('<?= base_url() ?>index.php/Master_RPCR/get_target', function (data){
    console.log(data);
        $('#target').val(data);
  });
}
function data_table1(){
  $('#tabelmitigation').DataTable().destroy();
            table = $('#tabelmitigation').DataTable({
                "ajax": {
                    "url": '<?= base_url() ?>index.php/Master_RPCR/datamitigation',

                },
                "columns": [
                    {"data": "date"},
                    {"data": "aircraft"},
                    {"data": "aircraftype"},
                    {"data": "start"},
                    {"data": "end"},
                    {"data": "action"}
                ]
            });
}
</script>
