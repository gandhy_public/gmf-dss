<style>
    .hijau {
        color: #F24738 !important;
    }

    .putih {
        color: black !important;
    }
    .box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
      display: inline-block;
      font-size: 20px;
      font-weight: bold;
      margin: 0;
      line-height: 1;
    }
</style>
<section class="content">
  <h3><?php echo $title;?></h3>
  <div class="box">
      <div class="box-body">
        <div class="form-group">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label for="period_start" class="col-sm-4 control-label" style="margin-top: 5px;">Period Start</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control input-sm pull-right" id="date_start" value="<?= '01-'.date('Y'); ?>">
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label for="period_end" class="col-sm-4 control-label" style="margin-top: 5px;">Period End</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control input-sm pull-right" id="date_end" value="<?= date('m-Y'); ?>">
                </div>
              </div>
            </div>
            <div class="col-sm-1">
              <button type="button" class="btn btn-default pull-right" onclick="search_data()"><i class="fa fa-search"></i> Search
              </button>
            </div>
          </div>
        </div>

      </div>
  </div>
      <div class="box">
        <div class="box-header">
          <h4>Master Data Repetitive Problem Closing Rate</h4>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-8">
              <form role="form" id="form_target_rpcr" method="post">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Target</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" name="target" id="target" placeholder="Target">
                    <input type="hidden" value="<?= date('Y', strtotime(date("Y"))); ?>" name="year" id="year">
                  </div>
                </div>
              </form>
              <div class="col-sm-3">
                <button type="button" class="btn btn-primary" onclick="confirm_save()"><i class="fa fa-save"></i> Save</button>
              </div>
            </div>
          </div>
        </div>
        <div class="row" style="padding-top:20px;">
          <div class="col-md-12">
            <label>&nbsp;&nbsp;&nbsp;&nbsp;Current Year : <span><?= date('Y', strtotime(date("Y"))); ?></span></label>
          </div>
          <br><br>
        </div>
      </div>

    <div class="box">
      <div class="box-body">
        <h4> Mitigation </h4>
        <table id="table-mitigation" class="table table-hover table-bordered table-gmf">
          <thead style="background-color: #05354D">
          <th class="text-center" style="width: 20px;">No</th>
          <th class="text-center">Notification</th>
          <th class="text-center">No Registration</th>
          <th class="text-center">Description</th>
          <th class="text-center">Aircraft</th>
          <th class="text-center">Aircraft Type</th>
          <th class="text-center">Start Date</th>
          <th class="text-center">Due Date</th>
          <th class="text-center">fk</th>
          <th class="text-center">Status</th>
          <th class="text-center">Update Date</th>
          <th class="text-center" style="width: 40px;"><center>Action</center></th>
          </thead>

        <tbody>
        </tbody>
        </table>
      </div>
    </div>


</section>

<div class="modal fade" id="modal_add_mitigation">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
        <h4 class="modal-title">Mitigation</h4>
      </div>
      <div class="modal-body">
      <form action="" id="form-modal" name="form-modal" class="form-horizontal">
        <input type="hidden" class="form-control pull-right" id="mitigasi_id" name="mitigasi_id">
        <input type="hidden" class="form-control pull-right" id="mitigasi_aircraft" name="mitigasi_aircraft">
        <input type="hidden" class="form-control pull-right" id="mitigasi_fk_id" name="mitigasi_fk_id">
        <div class="form-group">
          <label class="col-sm-3 control-label">Why</label>
          <div class="col-sm-9">
            <input type="text" class="form-control pull-right" id="input_why" name="input_why">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label">Solution</label>
          <div class="col-sm-9">
            <input type="text" class="form-control pull-right" id="input_solution" name="input_solution">
          </div>
        </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-flat btn-default"  data-dismiss="modal"><i class="fa fa-rotate-left"></i> Cancel</button>
        <button type="button" id="upload" class="pull-right btn btn-flat bg-navy" onclick="saveModal()"><i class="fa fa-save"></i> Save</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>


  var table;
  var date_s = $('date_start').val();
  var date_now = "<?= date('m-Y') ?>";
  $(document).ready(function(){
      get_target();
      table = $('#table-mitigation').DataTable({
          "processing": true,
          "serverSide": true,
          "order": [],
          "ajax": {
              "url": "<?= base_url().'index.php/Master_RPCR/data_list/'?>"+date_s+"/"+date_now,
              "type": "POST"
          },
          "createdRow": function( row, data, dataIndex){
              if( data[8] ){
                  $(row).addClass('hijau');
              }
          },
          "columnDefs": [
              {
                  "targets": [ -1 ],
                  "orderable": false
              },
              {
                "targets": [ 8 ],
                "visible": false,
                "searchable": false
              },
          ],

        });
  });

  $("input[name='target']").TouchSpin({
      min: 0,
      max: 1,
      step: 0.01,
      decimals: 2,
      boostat: 5,
      maxboostedstep: 10
      // postfix: '%'
  });

  function dialogSuccess(message){
    swal({
        title: "Done",
        text: message,
        timer: 1500,
        showConfirmButton: false,
        type: 'success'
    });
  }


  $('#date_end, #date_start').datepicker({
    todayBtn: "linked",
    format: 'mm-yyyy',
    viewMode: 'months',
    minViewMode: 'months',
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
  });


  function confirm_save(){
    swal({
      title: "Warning",
      text: "Simpan Data Target . ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#5568EE",
      confirmButtonText: "Simpan",
      showLoaderOnConfirm: true,
      preConfirm: function(){
          return new Promise(function(resolve){
            save();
          });
        },
      allowOutsideClick: false
    });
  }

  // function showDialog(){
  //     swal({
  //         title: "Are you sure?",
  //         text: "Simpan Data Target .?",
  //         type: "warning",
  //         showCancelButton: true,
  //         confirmButtonColor: "#5778EF",
  //         confirmButtonText: " Yes ",
  //         cancelButtonText: " No ",
  //         closeOnConfirm: false,
  //         closeOnCancel: false
  //       },
  //       function(isConfirm) {
  //         if (isConfirm) {
  //           save();
  //         } else {
  //           swal("Cancelled", "Batal menyimpan data target", "error");
  //         }
  //       });

  // }

  function save(){
    var targer = $('#target').val();
    $.ajax({
        type: 'POST',
        dataType: "json",
        url:'<?= base_url() ?>index.php/Master_RPCR/target_rpcr',
        data: $('#form_target_rpcr').serialize()
      })
      .done(function(data){
        if(data = "1"){
            swal("Success", "Data Target Berhasil disimpan", "success");
            reload_table();
        }else{
            swal("Warning", "Data Target Gagal disimpan", "warning");
        }
        get_target();
      })
      .fail(function(){
        swal("Oops...", "Something went wrong with ajax !", "error");
      })
  }

  function get_target(){
    $.post('<?= base_url() ?>index.php/Master_RPCR/get_target', function (data){
      console.log(data);
          $('#target').val(data);
    });
  }

  function saveModal(){
    var formData = new FormData($('#form-modal')[0]);
    $.ajax({
      url: "<?php echo base_url() ?>index.php/Master_RPCR/save_modal",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      // dataType: "JSON",
      success: function(data)
      {
        data = JSON.parse(data);
        console.log(data);
        if(data.sukses)
        {
            dialogSuccess(data.message);
            $('#modal_add_mitigation').modal('hide');
        }
        reload_table();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          console.log(errorThrown);
          $('#modal_add_mitigation').modal('hide');
          alert('Error adding / update data');
      }
    });
  }

  function ajax_edit(id) {
    $('#form-modal')[0].reset();

    $.ajax({
      url: '<?php echo base_url()."index.php/Master_RPCR/show_update/" ?>'+id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        if (data.mitigasi==null) {
          $('[name="mitigasi_fk_id"]').val(data.repetitive_d5.notification);
          $('[name="mitigasi_aircraft"]').val(data.repetitive_d5.actype);
        }else{
          id = data.mitigasi.target_id;

          $('[name="mitigasi_fk_id"]').val(data.repetitive_d5.notification);
          $('[name="mitigasi_aircraft"]').val(data.repetitive_d5.actype);
          $('[name="mitigasi_id"]').val(data.mitigasi.mitigasi_id);
          $('[name="input_why"]').val(data.mitigasi.mitigasi_why);
          $('[name="input_solution"]').val(data.mitigasi.mitigasi_solution);
        }

        $('#modal_add_mitigation').modal('show');

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
    });
  }

  function search_data(){
    var p_start = $('#date_start').val();
    var p_end = $('#date_end').val();
    console.log(p_start);
    console.log(p_end);

    var start = $("#date_start").val().split("-");
    var month_start = start[0];
    var start = start[1];
    var end = $("#date_end").val().split("-");
    var month_end = end[0];
    var end = end[1];

    if(start==end){
      if (month_end<month_start) {
        notif("warning", "Start Period Harus Lebih kecil dari End Period !!!");
      }else{
        table.ajax.url("<?php echo base_url().'index.php/Master_RPCR/data_list/'?>"+p_start+"/"+p_end).load();
      }
    }else{
      table.ajax.url("<?php echo base_url().'index.php/Master_RPCR/data_list/'?>"+p_start+"/"+p_end).load();
    }

  }

  function reload_table(){
      table.ajax.reload(null,false);
  }

</script>
