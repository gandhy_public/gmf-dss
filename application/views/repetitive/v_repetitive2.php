<style type="text/css">
    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }

    .title-manufacture {
        color: #F2C573;
        text-align: center;
        font-size: 25px;
        font-weight: bold;
        margin-top: 30px;
    }

    .title-actual {
        color: #fff;
        text-align: left;
        font-size: 25px;
        margin-top: 70px;
        margin-left: 25px;
    }

    .title-target {
        color: #fff;
        text-align: left;
        font-size: 20px;
        margin-top: 0px;
        margin-left: 25px;
    }

    .global-text-center {
        text-align: center;
    }

    .inner_in {
        padding: 0px;
        border-radius: 10px;
    }

    .small-box > .inner {
        padding: 3px;
    }

    .widget-user .widget-user-header {
        padding: 1px;
        height: 60px;
        border-top-right-radius: 11px;
        border-top-left-radius: 11px;
    }

    .widget-user .widget-user-image {
        position: absolute;
        top: 5px;
        left: 48%;
        margin-left: -45px;
    }

    .box .border-right {
        border-right: 1px solid #eac8c8;
    }

    .form-group {
        margin-bottom: 5px;
    }

    .widget-user .box-footer {
        padding-top: 0px;
    }

    .box-footer {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 11px;
        border-bottom-left-radius: 11px;
        border-top: 1px solid #f4f4f4;
        padding: 0px;
        background-color: #fff;
    }

    .description-block > .description-header {
        margin: 0;
        padding: 0;
        font-size: 28px;
        font-weight: bold;
    }

    .modal-title-panel {
        text-align: center;
        font-weight: bold;
    }

    .table-head-gmf {
        background-color: #05354D;
    }

    .modal-header {
        background-color: #0f2233;
        color: #fff;
    }

    .mini-icon {
        margin-top: 3px;
        margin-right: 12px;
    }

    .pointer-box {
        cursor: pointer;
        border-radius: 11px;
    }

    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0e1f2e;
    }

    .description-block {
        display: block;
        margin: 0px 0;
        text-align: center;
    }
</style>

<!--
LAYOUT LAMA
<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section> -->


<!--//=======================================Actual vs Target==============================================\\-->
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Repetitive Problem Closing Rate Index</h3>
            <div class="box-tools pull-right">
                <!-- LAYOUT LAMA
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i>
                </button> -->
                <label class="control-label" style="font-weight: 100;"><i class="fa  fa-calendar"></i> Year To Date
                    :</label>
                <label class="control-label pull-right" id="date"></label>
            </div>
        </div>
        <div class="box-body" style="padding: 0px;">
            <div class="box-header with-border" style="padding: 8px;">
                <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
                    <div class="box box-widget widget-user" style="border-radius: 11px;">
                        <div class="widget-user-header bg-navy">
                            <h3 class="title-manufacture">GARUDA</h3>
                        </div>
                        <div class="widget-user-image">
                            <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                 style="border: transparent;width: 100px;">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-6 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header" id="actual-garuda"></h5>
                                        <span class="description-text">Actual</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="description-block">
                                        <h5 class="description-header" id="target-garuda"></h5>
                                        <span class="description-text">Target</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
                    <div class="box box-widget widget-user" style="border-radius: 11px;">
                        <div class="widget-user-header bg-navy">
                            <h3 class="title-manufacture">CITILINK</h3>
                        </div>
                        <div class="widget-user-image">
                            <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                 style="border: transparent;width: 100px;">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-6 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header" id="actual-citilink"></h5>
                                        <span class="description-text">Actual</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="description-block">
                                        <h5 class="description-header" id="target-citilink"></h5>
                                        <span class="description-text">Target</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
                    <div class="box box-widget widget-user pointer-box" id="garuda-rep">
                        <div class="widget-user-header bg-navy">
                            <h3 class="title-manufacture">GARUDA REPETITIVE</h3>
                        </div>
                        <div class="widget-user-image">
                            <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                 style="border: transparent;width: 100px;">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="description-block">
                                        <h5 class="description-header box-header with-border"><?= $countGaruda ?></h5>
                                        <span class="description-text" style="margin-left: 13px;">Rep Problems</span>
                                        <i class="fa fa-ellipsis-v pull-right mini-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
                    <div class="box box-widget widget-user pointer-box" id="citilink-rep">
                        <div class="widget-user-header bg-navy">
                            <h3 class="title-manufacture">CITILINK REPETITIVE</h3>
                        </div>
                        <div class="widget-user-image">
                            <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                 style="border: transparent;width: 100px;">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="description-block">
                                        <h5 class="description-header box-header with-border"><?= $countCitilink ?></h5>
                                        <span class="description-text" style="margin-left: 13px;">Rep Problems</span>
                                        <i class="fa fa-ellipsis-v pull-right mini-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <h4 class="box-title"><i class="fa fa-filter"></i> Filter Repetitive</h4>
                <div class="form-group">
                    <select class="form-control select2" id="list_manufacture">
                        <option value="00" selected="">--Select Customer--</option>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control select2" id="list_aircraft_type">
                        <option value="00" selected="">--Select Type--</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control monthpicker" id="startPeriode" value="<?= $startPeriode ?>">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control monthpicker" id="endPeriode" value="<?= $endPeriode ?>">
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-flat bg-navy" id="search" style="width: 100%;"><i
                                class="fa fa-search"></i> Search
                    </button>
                </div>
            </div>
            <div id="clas_ga" class="col-lg-5">
                <br>
                <div id="chart_trend_ga" style="height: 300px;width: 100%"></div>
            </div>
            <div id="clas_citi" class="col-lg-5">
                <br>
                <div id="chart_trend_citi" style="height: 300px;width: 100%"></div>
            </div>
        </div>
    </div>
</section>


<script>
    var date = new Date();
    var month = [];
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var tgl = date.getDate();
    var bulan = month[date.getUTCMonth()];
    var tahun = date.getUTCFullYear();
    var base_url = '<?php echo base_url(); ?>index.php/';



    $(document).ready(function () {
        $('#date').html('&nbsp;' + tgl + ' ' + bulan + ' ' + tahun);
        getGaruda();   //pemanggilan fungsi tampil garuda.
        getCitilink();   //pemanggilan fungsi tampil citilink.
        load_chart_trend_ga();
        load_chart_trend_citi();
        targetActual();
        combo_manufacture();
        combo_aircraft_type();
        // tesdataga();
        $('#garuda-rep').click(function (e) {
            $('#modal-garuda-rep').modal('show');
            $('#title-garuda-rep').html('Garuda Repetitive Open');
        });
        $('#citilink-rep').click(function (e) {
            $('#modal-citilink-rep').modal('show');
            $('#title-citilink-rep').html('Citilink Repetitive Open');
        });
    });

    function tesdataga() {
        $.ajax({
            url: "<?= base_url() ?>index.php/Repetitive2/tesGA",
            type: "GET",
            dataType: "JSON",
            success: function (data) {
              console.log(data);
            }
        });
    }

    $(document).on("click", "#search", function (e) {
        var mfact = $("#list_manufacture").val();
        if (mfact == '00') {
            $("#clas_ga").removeClass("col-lg-10");
            $("#clas_ga").removeClass("hidden");
            $("#clas_citi").removeClass("col-lg-10");
            $("#clas_citi").removeClass("hidden");
            $("#clas_ga").addClass("col-lg-5");
            $("#clas_citi").addClass("col-lg-5");
            load_chart_trend_ga();
            load_chart_trend_citi();
        } else if (mfact == '12') {
            $("#clas_ga").removeClass("col-lg-5");
            $("#clas_ga").addClass("col-lg-10");
            $("#clas_citi").addClass("hidden");
            $("#clas_ga").removeClass("hidden");
            load_chart_trend_ga();
        } else if (mfact == '11') {
            $("#clas_citi").removeClass("col-lg-5");
            $("#clas_citi").addClass("col-lg-10");
            $("#clas_ga").addClass("hidden");
            $("#clas_citi").removeClass("hidden");
            load_chart_trend_citi();
        }
        // load_chart_trend();
        $("#detailbox").hide();
    });

    function targetActual() {
        $.ajax({
            url: "<?= base_url() ?>index.php/Repetitive2/targetActual",
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                var target = data[0]['target'];
                $('#target-garuda').html(target + ' <i class="fa fa-minus" style="color:#F2C573;"></i>');
                $('#target-citilink').html(target + ' <i class="fa fa-minus" style="color:#F2C573;"></i>');
                if (data[0]['garuda'] > target) {
                    $('#actual-garuda').html(data[0]['garuda'] + ' <i class="fa fa-caret-down" style="color:#F24738"></i>');
                    $('.value-actual-manufacture').css('color', '#F24738');
                } else {
                    $('#actual-garuda').html(data[0]['garuda'] + ' <i class="fa fa-caret-up" style="color:#8EC3A7"></i>');
                    $('.value-actual-manufacture').css('color', '#8EC3A7');
                }
                if (data[0]['citilink'] > target) {
                    $('#actual-citilink').html(data[0]['citilink'] + ' <i class="fa fa-caret-down" style="color:#F24738"></i>');
                    $('.value-actual-manufacture').css('color', '#F24738');
                } else {
                    $('#actual-citilink').html(data[0]['citilink'] + ' <i class="fa fa-caret-up" style="color:#8EC3A7"></i>');
                    $('.value-actual-manufacture').css('color', '#8EC3A7');
                }
            }
        });
    }

    function getGaruda() {
        var tableGaruda = $("#tableGaruda").DataTable({
            ordering: true,
            destroy: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: {
                url: "<?= base_url() ?>index.php/Repetitive2/tableGaruda",
                type: 'POST'
            },
            "aoColumns": [
                {
                    "mData": "0",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                 {
                    "mData": "1",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "2",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "3",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "4",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "5",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "6",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "7",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
            ],
            createdRow: function (row, data, index) {
                var due_Date          = data[6];
                due_Date              = due_Date.split('-');
                due_Date              = due_Date[2]+'-'+due_Date[1]+'-'+due_Date[0]
                var MyDate            = new Date();
                var MyDateString;
                MyDate.setDate(MyDate.getDate());
                MyDateString          = MyDate.getFullYear() + '-'+ ('0' + (MyDate.getMonth()+1)).slice(-2) + '-'
                                        +  ('0' + MyDate.getDate()).slice(-2);
                  var due_date        = due_Date.split('-');
                  var today           = MyDateString.split('-');
                  var dueDate         = new Date();
                  dueDate.setFullYear(due_date[0],(due_date[1] - 1 ),due_date[2]);
                  var todayDate       = new Date();
                  todayDate.setFullYear(today[0],(today[1] - 1 ),today[2]);


                  if(todayDate >= dueDate){
                    $(row).find('td').css('background-color', '#fff');
                    $(row).find('td').css('cursor', 'pointer');
                    $(row).css('color', '#F24738');
                     $(row).on('click', function () {
                       noteMitigation(data[1]);
                    });
                  }else{
                    $(row).find('td').css('background-color', '#fff');
                    $(row).css('color', '#333');
                  }
            }
        });
    }

    function getCitilink() {
        var tableCitilink = $("#tableCitilink").DataTable({
            ordering: true,
            destroy: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: {
                url: "<?= base_url() ?>index.php/Repetitive2/tableCitilink",
                type: 'POST'
            },
            "aoColumns": [
                {
                    "mData": "0",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "1",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "2",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "3",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "4",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "5",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "6",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "7",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
            ],

            createdRow: function (row, data, index) {
                var due_Date          = data[6];
                due_Date              = due_Date.split('-');
                due_Date              = due_Date[2]+'-'+due_Date[1]+'-'+due_Date[0]
                var MyDate            = new Date();
                var MyDateString;
                MyDate.setDate(MyDate.getDate());
                MyDateString          = MyDate.getFullYear() + '-'+ ('0' + (MyDate.getMonth()+1)).slice(-2) + '-'
                                        +  ('0' + MyDate.getDate()).slice(-2);
                  var due_date        = due_Date.split('-');
                  var today           = MyDateString.split('-');
                  var dueDate         = new Date();
                  dueDate.setFullYear(due_date[0],(due_date[1] - 1 ),due_date[2]);
                  var todayDate       = new Date();
                  todayDate.setFullYear(today[0],(today[1] - 1 ),today[2]);


                  if(todayDate >= dueDate){
                    $(row).find('td').css('background-color', '#fff');
                    $(row).find('td').css('cursor', 'pointer');
                    $(row).css('color', '#F24738');
                     $(row).on('click', function () {
                       noteMitigation(data[1]);
                    });
                  }else{
                    $(row).find('td').css('background-color', '#fff');
                    $(row).css('color', '#333');
                  }
            }
        });
    }

    function combo_manufacture() {
        var list_manufacture;
        $.ajax({
            url: "<?= base_url() ?>index.php/Repetitive/master_manufacture",
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                list_manufacture = '<option value="00" selected="">--All Customer--</option>';
                $(data).each(function (key, val) {
                    list_manufacture += '<option value="' + val.mn_id + '">' + val.mn_name + '</option>';
                });
                $('#list_manufacture').html(list_manufacture);
            }
        });
    }

    function combo_aircraft_type() {
        $('#list_manufacture').change(function () {
            var list_aircraft_type;
            var list_manufacture = $('#list_manufacture').val();
            $.ajax({
                url: base_url + 'Repetitive/master_type/' + list_manufacture,
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    if (list_manufacture == '00') {
                        list_aircraft_type = '<option value="">--Select Type--</option>';
                    } else {
                        list_aircraft_type += '<option value="">--All--</option>';
                        $(data).each(function (key, val) {
                            list_aircraft_type += '<option value="' + val.actype_id + '">' + val.actype + '</option>';
                        });
                    }
                    $('#list_aircraft_type').html(list_aircraft_type);
                }
            });
        })
    }

    function load_chart_trend_ga() {
        var startPeriode = $("#startPeriode").val();
        var endPeriode = $("#endPeriode").val();
        var list_aircraft_type = $('#list_aircraft_type').find(":selected").val(); //$('#list_aircraft_type').find(":selected").val()
        var list_manufacture = '12'; //$('#list_manufacture').find(":selected").val()

        $.ajax({
            url: "<?= base_url(); ?>index.php/Repetitive2/getDataChartTrend",
            data: {
                'startPeriode': startPeriode,
                'endPeriode': endPeriode,
                'list_aircraft_type': list_aircraft_type,
                'list_manufacture': list_manufacture
            },
            type: 'POST',
            dataType: "JSON",
            success: function (data) {
                notif('success', 'Data trend repetitive successfully displayed');
                Highcharts.chart('chart_trend_ga', {
                    chart: {
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: 'Tren Data Repetitive'
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: 0,
                        // max: 10,
                        title: {
                            text: 'Data',
                            style: {
                                fontSize: '15px',
                                fontWeight: 'bold',
                                color: '#333333'
                            }
                        },
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    legend: {
                        enabled: true,
                        itemStyle: {
                            fontSize: '15px',
                        },
                        layout: 'horizontal',
                        itemMarginBottom: 0,
                        align: 'center',
                        verticalAlign: 'bottom',
                    },
                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            dataLabels: {
                                enabled: true
                            },
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function () {
                                        detailTrendData(this.series.name, this.category);
                                    }
                                }
                            }
                        }
                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "Garuda",
                            data: data.data_garuda,
                            color: "#05354d"
                        },
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    itemMarginTop: 50,
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
    }

    function load_chart_trend_citi() {
        var startPeriode = $("#startPeriode").val();
        var endPeriode = $("#endPeriode").val();
        var list_aircraft_type = $('#list_aircraft_type').find(":selected").val(); //$('#list_aircraft_type').find(":selected").val()
        var list_manufacture = '11';//$('#list_manufacture').find(":selected").val()

        $.ajax({
            url: "<?= base_url(); ?>index.php/Repetitive2/getDataChartTrend",
            data: {
                'startPeriode': startPeriode,
                'endPeriode': endPeriode,
                'list_aircraft_type': list_aircraft_type,
                'list_manufacture': list_manufacture
            },
            type: 'POST',
            dataType: "JSON",
            success: function (data) {
                notif('success', 'Data trend repetitive successfully displayed');
                Highcharts.chart('chart_trend_citi', {
                    chart: {
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: 'Tren Data Repetitive'
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: 0,
                        // max: 10,
                        title: {
                            text: 'Data',
                            style: {
                                fontSize: '15px',
                                fontWeight: 'bold',
                                color: '#333333'
                            }
                        },
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    legend: {
                        enabled: true,
                        itemStyle: {
                            fontSize: '15px',
                        },
                        layout: 'horizontal',
                        itemMarginBottom: 0,
                        align: 'center',
                        verticalAlign: 'bottom',
                    },
                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            dataLabels: {
                                enabled: true
                            },
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function () {
                                        detailTrendData(this.series.name, this.category);
                                    }
                                }
                            }
                        }
                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#8ec3a7"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "Citilink",
                            data: data.data_citilink,
                            color: "#8ec3a7"
                        },
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    itemMarginTop: 50,
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
    }

    // Start chart Trend
    function load_chart_trend() {
        var startPeriode = $("#startPeriode").val();
        var endPeriode = $("#endPeriode").val();
        var list_aircraft_type = $('#list_aircraft_type').find(":selected").val();
        var list_manufacture = $('#list_manufacture').find(":selected").val();

        if (list_manufacture == 00 && list_aircraft_type == 00) {
            $.ajax({
                url: "<?= base_url(); ?>index.php/Repetitive2/getDataChartTrend",
                data: {
                    'startPeriode': startPeriode,
                    'endPeriode': endPeriode,
                    'list_aircraft_type': list_aircraft_type,
                    'list_manufacture': list_manufacture
                },
                type: 'POST',
                dataType: "JSON",
                success: function (data) {
                    notif('success', 'Data trend repetitive successfully displayed');
                    Highcharts.chart('chart_trend', {
                        chart: {
                            height: 230,
                            type: 'line'
                        },
                        exporting: {
                            enabled: false
                        },
                        title: {
                            text: 'Tren Data Repetitive'
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            min: 0,
                            max: 10,
                            title: {
                                text: 'Data',
                                style: {
                                    fontSize: '15px',
                                    fontWeight: 'bold',
                                    color: '#333333'
                                }
                            },
                        },
                        xAxis: {
                            categories: data.categories,
                        },
                        legend: {
                            enabled: true,
                            itemStyle: {
                                fontSize: '15px',
                            },
                            layout: 'horizontal',
                            itemMarginBottom: 0,
                            align: 'center',
                            verticalAlign: 'bottom',
                        },
                        plotOptions: {
                            series: {
                                label: {
                                    connectorAllowed: false
                                },
                                dataLabels: {
                                    enabled: true
                                },
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function () {
                                            detailTrendData(this.series.name, this.category);
                                        }
                                    }
                                }
                            }
                        },
                        series: [
                            {
                                type: 'areaspline',
                                fillColor: {
                                    linearGradient: [0, 0, 0, 200],
                                    stops: [
                                        [0, "#05354d"],
                                        [1, "#fff3"]
                                    ]
                                },
                                lineWidth: 1,
                                name: "Garuda",
                                data: data.data_garuda,
                                color: "#05354d"
                            },
                            {
                                type: 'areaspline',
                                fillColor: {
                                    linearGradient: [0, 0, 0, 200],
                                    stops: [
                                        [0, "#8ec3a7"],
                                        [1, "#fff3"]
                                    ]
                                },
                                lineWidth: 1,
                                name: "Citilink",
                                data: data.data_citilink,
                                color: "#8ec3a7"
                            },
                        ],
                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        itemMarginTop: 50,
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }

                    });
                }
            });
        }
        else if (list_manufacture == 11) {
            if (list_aircraft_type == 00) {
                $.ajax({
                    url: "<?= base_url(); ?>index.php/Repetitive2/getDataChartTrend",
                    data: {
                        'startPeriode': startPeriode,
                        'endPeriode': endPeriode,
                        'list_aircraft_type': list_aircraft_type,
                        'list_manufacture': list_manufacture
                    },
                    type: 'POST',
                    dataType: "JSON",
                    success: function (data) {
                        notif('success', 'Data trend repetitive successfully displayed');
                        Highcharts.chart('chart_trend', {
                            chart: {
                                height: 230,
                                type: 'line'
                            },
                            exporting: {
                                enabled: false
                            },
                            title: {
                                text: 'Tren Data Repetitive'
                            },
                            credits: {
                                enabled: false
                            },
                            yAxis: {
                                min: 0,
                                max: 10,
                                title: {
                                    text: 'Data',
                                    style: {
                                        fontSize: '15px',
                                        fontWeight: 'bold',
                                        color: '#333333'
                                    }
                                },
                            },
                            xAxis: {
                                categories: data.categories,
                            },
                            legend: {
                                enabled: true,
                                itemStyle: {
                                    fontSize: '15px',
                                },
                                layout: 'horizontal',
                                itemMarginBottom: 0,
                                align: 'center',
                                verticalAlign: 'bottom',
                            },
                            plotOptions: {
                                series: {
                                    label: {
                                        connectorAllowed: false
                                    },
                                    dataLabels: {
                                        enabled: true
                                    },
                                    cursor: 'pointer',
                                    point: {
                                        events: {
                                            click: function () {
                                                detailTrendData(this.series.name, this.category);
                                            }
                                        }
                                    }
                                }
                            },
                            series: [
                                {
                                    type: 'areaspline',
                                    fillColor: {
                                        linearGradient: [0, 0, 0, 200],
                                        stops: [
                                            [0, "#8ec3a7"],
                                            [1, "#fff3"]
                                        ]
                                    },
                                    lineWidth: 1,
                                    name: "Citilink",
                                    data: data.data_citilink,
                                    color: "#8ec3a7"
                                },
                            ],
                            responsive: {
                                rules: [{
                                    condition: {
                                        maxWidth: 500
                                    },
                                    chartOptions: {
                                        legend: {
                                            layout: 'horizontal',
                                            align: 'center',
                                            itemMarginTop: 50,
                                            verticalAlign: 'bottom'
                                        }
                                    }
                                }]
                            }

                        });
                    }
                });
            } else {
                $.ajax({
                    url: "<?= base_url(); ?>index.php/Repetitive2/getDataChartTrend",
                    data: {
                        'startPeriode': startPeriode,
                        'endPeriode': endPeriode,
                        'list_aircraft_type': list_aircraft_type,
                        'list_manufacture': list_manufacture
                    },
                    type: 'POST',
                    dataType: "JSON",
                    success: function (data) {
                        notif('success', 'Data trend repetitive successfully displayed');
                        Highcharts.chart('chart_trend', {
                            chart: {
                                height: 230,
                                type: 'line'
                            },
                            exporting: {
                                enabled: false
                            },
                            title: {
                                text: 'Tren Data Repetitive'
                            },
                            credits: {
                                enabled: false
                            },
                            yAxis: {
                                min: 0,
                                max: 10,
                                title: {
                                    text: 'Data',
                                    style: {
                                        fontSize: '15px',
                                        fontWeight: 'bold',
                                        color: '#333333'
                                    }
                                },
                            },
                            xAxis: {
                                categories: data.categories,
                            },
                            legend: {
                                enabled: true,
                                itemStyle: {
                                    fontSize: '15px',
                                },
                                layout: 'horizontal',
                                itemMarginBottom: 0,
                                align: 'center',
                                verticalAlign: 'bottom',
                            },
                            plotOptions: {
                                series: {
                                    label: {
                                        connectorAllowed: false
                                    },
                                    dataLabels: {
                                        enabled: true
                                    },
                                    cursor: 'pointer',
                                    point: {
                                        events: {
                                            click: function () {
                                                detailTrendData(this.series.name, this.category);
                                            }
                                        }
                                    }
                                }
                            },
                            series: [
                                {
                                    type: 'areaspline',
                                    fillColor: {
                                        linearGradient: [0, 0, 0, 200],
                                        stops: [
                                            [0, "#8ec3a7"],
                                            [1, "#fff3"]
                                        ]
                                    },
                                    lineWidth: 1,
                                    name: "Citilink",
                                    data: data.data_citilink,
                                    color: "#8ec3a7"
                                },
                            ],
                            responsive: {
                                rules: [{
                                    condition: {
                                        maxWidth: 500
                                    },
                                    chartOptions: {
                                        legend: {
                                            layout: 'horizontal',
                                            align: 'center',
                                            itemMarginTop: 50,
                                            verticalAlign: 'bottom'
                                        }
                                    }
                                }]
                            }

                        });
                    }
                });
            }
        } else if (list_manufacture == 12) {
            if (list_aircraft_type == 00) {
                $.ajax({
                    url: "<?= base_url(); ?>index.php/Repetitive2/getDataChartTrend",
                    data: {
                        'startPeriode': startPeriode,
                        'endPeriode': endPeriode,
                        'list_aircraft_type': list_aircraft_type,
                        'list_manufacture': list_manufacture
                    },
                    type: 'POST',
                    dataType: "JSON",
                    success: function (data) {
                        notif('success', 'Data trend repetitive successfully displayed');
                        Highcharts.chart('chart_trend', {
                            chart: {
                                height: 230,
                                type: 'line'
                            },
                            exporting: {
                                enabled: false
                            },
                            title: {
                                text: 'Tren Data Repetitive'
                            },
                            credits: {
                                enabled: false
                            },
                            yAxis: {
                                min: 0,
                                max: 10,
                                title: {
                                    text: 'Data',
                                    style: {
                                        fontSize: '15px',
                                        fontWeight: 'bold',
                                        color: '#333333'
                                    }
                                },
                            },
                            xAxis: {
                                categories: data.categories,
                            },
                            legend: {
                                enabled: true,
                                itemStyle: {
                                    fontSize: '15px',
                                },
                                layout: 'horizontal',
                                itemMarginBottom: 0,
                                align: 'center',
                                verticalAlign: 'bottom',
                            },
                            plotOptions: {
                                series: {
                                    label: {
                                        connectorAllowed: false
                                    },
                                    dataLabels: {
                                        enabled: true
                                    },
                                    cursor: 'pointer',
                                    point: {
                                        events: {
                                            click: function () {
                                                detailTrendData(this.series.name, this.category);
                                            }
                                        }
                                    }
                                }
                            },
                            series: [
                                {
                                    type: 'areaspline',
                                    fillColor: {
                                        linearGradient: [0, 0, 0, 200],
                                        stops: [
                                            [0, "#05354d"],
                                            [1, "#fff3"]
                                        ]
                                    },
                                    lineWidth: 1,
                                    name: "Garuda",
                                    data: data.data_garuda,
                                    color: "#05354d"
                                },
                            ],
                            responsive: {
                                rules: [{
                                    condition: {
                                        maxWidth: 500
                                    },
                                    chartOptions: {
                                        legend: {
                                            layout: 'horizontal',
                                            align: 'center',
                                            itemMarginTop: 50,
                                            verticalAlign: 'bottom'
                                        }
                                    }
                                }]
                            }

                        });
                    }
                });
            } else {
                $.ajax({
                    url: "<?= base_url(); ?>index.php/Repetitive2/getDataChartTrend",
                    data: {
                        'startPeriode': startPeriode,
                        'endPeriode': endPeriode,
                        'list_aircraft_type': list_aircraft_type,
                        'list_manufacture': list_manufacture
                    },
                    type: 'POST',
                    dataType: "JSON",
                    success: function (data) {
                        notif('success', 'Data trend repetitive successfully displayed');
                        Highcharts.chart('chart_trend', {
                            chart: {
                                height: 230,
                                type: 'line'
                            },
                            exporting: {
                                enabled: false
                            },
                            title: {
                                text: 'Tren Data Repetitive'
                            },
                            credits: {
                                enabled: false
                            },
                            yAxis: {
                                min: 0,
                                max: 10,
                                title: {
                                    text: 'Data',
                                    style: {
                                        fontSize: '15px',
                                        fontWeight: 'bold',
                                        color: '#333333'
                                    }
                                },
                            },
                            xAxis: {
                                categories: data.categories,
                            },
                            legend: {
                                enabled: true,
                                itemStyle: {
                                    fontSize: '15px',
                                },
                                layout: 'horizontal',
                                itemMarginBottom: 0,
                                align: 'center',
                                verticalAlign: 'bottom',
                            },
                            plotOptions: {
                                series: {
                                    label: {
                                        connectorAllowed: false
                                    },
                                    dataLabels: {
                                        enabled: true
                                    },
                                    cursor: 'pointer',
                                    point: {
                                        events: {
                                            click: function () {
                                                detailTrendData(this.series.name, this.category);
                                            }
                                        }
                                    }
                                }
                            },
                            series: [
                                {
                                    type: 'areaspline',
                                    fillColor: {
                                        linearGradient: [0, 0, 0, 200],
                                        stops: [
                                            [0, "#05354d"],
                                            [1, "#fff3"]
                                        ]
                                    },
                                    lineWidth: 1,
                                    name: "Garuda",
                                    data: data.data_garuda,
                                    color: "#05354d"
                                },
                            ],
                            responsive: {
                                rules: [{
                                    condition: {
                                        maxWidth: 500
                                    },
                                    chartOptions: {
                                        legend: {
                                            layout: 'horizontal',
                                            align: 'center',
                                            itemMarginTop: 50,
                                            verticalAlign: 'bottom'
                                        }
                                    }
                                }]
                            }

                        });
                    }
                });
            }
        }
    }

    function detailTrendData(aircraft, month) {
        $('#detailbox').modal('show');
        $('#detail-tittle-grafik').html(aircraft + ' | Month : ' + month);
        var detailTrendData = $("#detailTrendData").DataTable({
            ordering: true,
            destroy: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: {
                url: "<?= base_url() ?>index.php/Repetitive2/detailTrendData",
                type: 'POST',
                data: {
                    'aircraft': aircraft,
                    'month': month
                }
            },
            "aoColumns": [
                {
                    "mData": "0",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "1",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "2",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "3",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "4",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "5",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "6",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "7",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
            ],
            createdRow: function (row, data, index) {
                var due_Date          = data[6];
                due_Date              = due_Date.split('-');
                due_Date              = due_Date[2]+'-'+due_Date[1]+'-'+due_Date[0]
                var MyDate            = new Date();
                var MyDateString;
                MyDate.setDate(MyDate.getDate());
                MyDateString          = MyDate.getFullYear() + '-'+ ('0' + (MyDate.getMonth()+1)).slice(-2) + '-'
                                        +  ('0' + MyDate.getDate()).slice(-2);
                  var due_date        = due_Date.split('-');
                  var today           = MyDateString.split('-');
                  var dueDate         = new Date();
                  dueDate.setFullYear(due_date[0],(due_date[1] - 1 ),due_date[2]);
                  var todayDate       = new Date();
                  todayDate.setFullYear(today[0],(today[1] - 1 ),today[2]);


                  if(todayDate >= dueDate){
                    $(row).find('td').css('background-color', '#fff');
                    $(row).find('td').css('cursor', 'pointer');
                    $(row).css('color', '#F24738');
                     $(row).on('click', function () {
                       noteMitigation(data[1]);
                    });
                  }else{
                    $(row).find('td').css('background-color', '#fff');
                    $(row).css('color', '#333');
                  }
            }
        });
    }

    function noteMitigation(id) {
        $.ajax({
            url: "<?= base_url() ?>index.php//Repetitive2/viewMitigation",
            type: "POST",
            data: {
                'id': id,
            },
            dataType: "JSON",
            success: function (data) {
                notif('warning', '<h3>MITIGATION !!!</h3><table class="table table-bordered"><tr><td>Why</td><td>' + data.why + '</td></tr><tr><td>Solution</td><td>' + data.solusi + '</td></tr></table>');

            }

        });
    }

</script>

<!-- MODAL REPETITIVE GARUDA -->
<div class="modal bounceIn" id="modal-garuda-rep" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title-panel" id="title-garuda-rep"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive" style="overflow-x:hidden">
                            <table id="tableGaruda" class="table table-hover table-bordered table-gmf" role="grid"
                                   aria-describedby="example1_info" style="width: 100%">
                                <thead class="bg-navy">
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Notification</th>
                                    <th class="text-center">Aircraft Type</th>
                                    <th class="text-center">Aircraft Reg</th>
                                    <th class="text-center">Description</th>
                                    <th class="text-center">Create Date</th>
                                    <th class="text-center">Due Date</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center" style="display: none"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL REPETITIVE CITILINK -->
<div class="modal bounceIn" id="modal-citilink-rep" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title-panel" id="title-citilink-rep"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive" style="overflow-x:hidden">
                            <table id="tableCitilink" class="table table-hover table-bordered table-gmf" role="grid"
                                   aria-describedby="example1_info" style="width: 100%">
                                <thead class="bg-navy">
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Notification</th>
                                    <th class="text-center">Aircraft Type</th>
                                    <th class="text-center">Aircraft Reg</th>
                                    <th class="text-center">Description</th>
                                    <th class="text-center">Create Date</th>
                                    <th class="text-center">Due Date</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center" style="display: none"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL GRAFIK TREND DATA REPETITIVE-->
<div class="modal bounceIn" id="detailbox" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-detail-title-panel" id="detail-tittle-grafik"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive" style="overflow-x:hidden">
                            <table id="detailTrendData"
                                   class="table table-bordered table-striped dataTable"
                                   role="grid" aria-describedby="example1_info">
                                <thead class="bg-navy">
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Notification</th>
                                    <th class="text-center">Aircraft Type</th>
                                    <th class="text-center">Aircraft Reg</th>
                                    <th class="text-center">Description</th>
                                    <th class="text-center">Create Date</th>
                                    <th class="text-center">Due Date</th>
                                    <th class="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
