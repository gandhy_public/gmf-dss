<?php
$CI = & get_instance();
$this->load->database();
$CI->load->model('M_copa_menu');
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= base_url(); ?>assets/dist/img/icon/user_icon.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p class="name_user"></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul id="parent-sidebar" class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            <?php
            if(isset($menu['data1']) || count($menu['data1']) > 0){
                foreach ($menu['data1'] as $key1 => $data1){
                    if($data1['menu_item_id'] == 107){
                        if($data1["total"] > 0){
                            $clas1 = "treeview";
                            $link1 = "#";
                        }else{
                            $clas1 = "";
                            $link1 = base_url("index.php/" . $data1['menu_item_link']);
                        }
                        echo '<li class="'.$clas1.'">';
                        echo '<a href="'.$link1 .'"><i class="'.$data1['menu_item_icon'].'"></i><span>'.$data1['menu_item_name'].'</span>';

                        if($data1["total"] > 0){
                            echo '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';
                        };
                        echo '</a>';
                        echo '<ul class="treeview-menu">';

                        /// Customer Perspective
                        $idRole = $this->session->userdata('roleid');
                        if(isset($menu['data']) || count($menu['data']) > 0){
                            foreach ($menu['data'] as $key => $data){
                                if ($data1['menu_item_id'] == $data['menu_item_iscenter']){
                                //if($data1['menu_item_name']=='Customer Perspective'){
                                    if($idRole == $data['role_id']){
                                        if(count($data["child"]) > 0){
                                            $clas1 = "treeview";
                                            $link1 = "#";
                                        }else{
                                            $clas1 = "";
                                            $link1 = base_url("index.php/" . $data['menu_item_link']);
                                        }
                                        echo '<li id="parent-li-'.$key.'" class="'.$clas1.'">';

                                        echo '<a href="'.$link1 .'"><i class="'.$data['menu_item_icon'].'"></i><span>'.$data['menu_item_name'].'</span>';

                                        if (count($data["child"]) > 0){
                                            echo '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';
                                        };
                                        echo '</a>';
                                        if (count($data["child"]) > 0){
                                            echo '<ul class="treeview-menu">';
                                            foreach ($data["child"] as $child){
                                                if ($idRole == $child->role_id){
                                                    echo '<li><a class="link_menu" href="'.base_url("index.php/" . $child->menu_item_link) .'"><i class="fa fa-circle-o"></i> '.$child->menu_item_name .'</a></li>';
                                                };
                                            };
                                            echo '</ul>';
                                        };
                                        echo '</li>';
                                    };
                                }
                                    // }else{
                                    // if ($data1['menu_item_id'] == $data['menu_item_iscenter']){
                                            // if(count($data["child"]) > 0){
                                                // $clas1 = "treeview";
                                                // $link1 = "#";
                                            // }else{
                                                // $clas1 = "";
                                                // $link1 = base_url("index.php/" . $data['menu_item_link']);
                                            // }
                                             // echo '<li id="parent-li-'.$key.'" class="'.$clas1.'">';

                                                // echo '<a href="'.$link1.'"><i class="'.$data['menu_item_icon'].'"></i><span>'.$data['menu_item_name'].'</span>';

                                                // if (count($data["child"]) > 0){
                                                    // echo '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';
                                                // };
                                            // echo '</a>';
                                            // if (count($data["child"]) > 0){
                                                // echo '<ul class="treeview-menu">';
                                                    // foreach ($data["child"] as $child){
                                                            // echo '<li><a class="link_menu" href="'.base_url("index.php/" . $child->menu_item_link) .'"><i class="fa fa-circle-o"></i> '.$child->menu_item_name .'</a></li>';
                                                    // };
                                                // echo '</ul>';
                                            // };
                                        // echo '</li>';
                                    // };
                                // }
                            };
                        };
                        echo '</ul></li>';
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-money"></i> <span>COPA</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                                <?php
                                $row = $CI->M_copa_menu->getActiveMenu();
                                foreach ($row as $data) {
                                ?>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php/Copa/iframe/<?php echo $data->id; ?>">&nbsp;&nbsp;&nbsp;<i class="fa fa-circle-o"></i><?php echo $data->name; ?></a>
                                    </li>
                                <?php
                                }
                                ?>
                                    <li>
                                        <a href='<?php echo base_url(); ?>index.php/Copa/iframe_manager'>&nbsp;&nbsp;&nbsp;<i class="fa fa-magic"></i> Menu Manager</a>
                                    </li>
                                    <li>
                                        <a href='<?php echo base_url(); ?>index.php/Copa/refresh'>&nbsp;&nbsp;&nbsp;<i class="fa fa-circle-o"></i> Auto Refresh</a>
                                    </li>
                        </ul>
                    </li>
                    <?php
                    }else{
                        if($data1["total"] > 0){
                            $clas1 = "treeview";
                            $link1 = "#";
                        }else{
                            $clas1 = "";
                            $link1 = base_url("index.php/" . $data1['menu_item_link']);
                        }
                        echo '<li class="'.$clas1.'">';
                        echo '<a href="'.$link1 .'"><i class="'.$data1['menu_item_icon'].'"></i><span>'.$data1['menu_item_name'].'</span>';

                        if($data1["total"] > 0){
                            echo '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';
                        };
                        echo '</a>';
                        echo '<ul class="treeview-menu">';

                        /// Customer Perspective
                        $idRole = $this->session->userdata('roleid');
                        if(isset($menu['data']) || count($menu['data']) > 0){
                            foreach ($menu['data'] as $key => $data){
                                if ($data1['menu_item_id'] == $data['menu_item_iscenter']){
                                //if($data1['menu_item_name']=='Customer Perspective'){
                                    if($idRole == $data['role_id']){
                                        if(count($data["child"]) > 0){
                                            $clas1 = "treeview";
                                            $link1 = "#";
                                        }else{
                                            $clas1 = "";
                                            $link1 = base_url("index.php/" . $data['menu_item_link']);
                                        }
                                        echo '<li id="parent-li-'.$key.'" class="'.$clas1.'">';

                                        echo '<a href="'.$link1 .'"><i class="'.$data['menu_item_icon'].'"></i><span>'.$data['menu_item_name'].'</span>';

                                        if (count($data["child"]) > 0){
                                            echo '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';
                                        };
                                        echo '</a>';
                                        if (count($data["child"]) > 0){
                                            echo '<ul class="treeview-menu">';
                                            foreach ($data["child"] as $child){
                                                if ($idRole == $child->role_id){
                                                    echo '<li><a class="link_menu" href="'.base_url("index.php/" . $child->menu_item_link) .'"><i class="fa fa-circle-o"></i> '.$child->menu_item_name .'</a></li>';
                                                };
                                            };
                                            echo '</ul>';
                                        };
                                        echo '</li>';
                                    };
                                }
                                    // }else{
                                    // if ($data1['menu_item_id'] == $data['menu_item_iscenter']){
                                            // if(count($data["child"]) > 0){
                                                // $clas1 = "treeview";
                                                // $link1 = "#";
                                            // }else{
                                                // $clas1 = "";
                                                // $link1 = base_url("index.php/" . $data['menu_item_link']);
                                            // }
                                             // echo '<li id="parent-li-'.$key.'" class="'.$clas1.'">';

                                                // echo '<a href="'.$link1.'"><i class="'.$data['menu_item_icon'].'"></i><span>'.$data['menu_item_name'].'</span>';

                                                // if (count($data["child"]) > 0){
                                                    // echo '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';
                                                // };
                                            // echo '</a>';
                                            // if (count($data["child"]) > 0){
                                                // echo '<ul class="treeview-menu">';
                                                    // foreach ($data["child"] as $child){
                                                            // echo '<li><a class="link_menu" href="'.base_url("index.php/" . $child->menu_item_link) .'"><i class="fa fa-circle-o"></i> '.$child->menu_item_name .'</a></li>';
                                                    // };
                                                // echo '</ul>';
                                            // };
                                        // echo '</li>';
                                    // };
                                // }
                            };
                        };
                        echo '</ul></li>';
                    }
                }
            }
            ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>


<script>


    $(document).ready(function () {
        var url = window.location;
        var thisID = $('ul a[href="' + url + '"]').parent().parent().parent().attr('id');
        var mediumthisID = $('ul a[href="' + url + '"]').parent().parent().parent();
        var deepthisID = $('ul a[href="' + url + '"]').parent().parent().parent().parent().parent();
        $(deepthisID).addClass('active menu-open');
        $(mediumthisID).addClass('active menu-open');
        $('#' + thisID).addClass('active menu-open');
        $('ul a[href="' + url + '"]').parent().addClass('active');
        $('ul a').filter(function () {
            return this.href == url;
        }).parent().addClass('active');
    });

</script>
