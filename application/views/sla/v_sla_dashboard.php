<style>
    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        margin: -3px;
        line-height: 1;
    }

    .wadah {
        padding-left: 5px;
        padding-right: 5px;
    }

    td {
        text-align: center;
    }

    .highcharts-title {
        cursor: pointer;
    }

    .color-title {
        color: white;
    }

    .display-none {
        display: none;
    }

    .robbing {
        cursor: pointer;
    }

    .modal-ku {
        width: 1000px;
        margin: auto;
    }

    .red {
        color: #F24738 !important;
    }

    .black {
        color: black !important;
    }

    .center {
        display: block;
        margin: 1em 0;
    }

    .header-robbing {
        background-color: #0D2633;
        color: white;
        text-align: center;
        border-top-right-radius: 3px;
        border-top-left-radius: 3px;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        width: 50px;
        margin-left: 5px;
        margin-right: auto;
    }

    .value-robbing {
        color: black;
        text-align: center;
        border-top-right-radius: 3px;
        border-top-left-radius: 3px;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        width: 60px;
        height: 60px;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        margin-top: -20px;
        margin-left: auto;
        margin-right: auto;
    }

    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0e1f2e;
    }

	    .loading-sla {
        position: absolute;
        left: 50%;
        top: 10%;
        z-index: 1;
        width: 150px;
        height: 150px;
        margin: 80px 0 0 -70px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }
</style>
<!-- Start Body Pages -->
<section class="content">
    <div class="row">
        <div class="col-lg-12 col-md-8 col-sm-12 col-xs-12">

            <!-- Start Grafik Utama -->
            <div class="box">
                <h4 style="text-align: center;font-weight:  bold;font-size: 20px;"><?= $title ?></h4>
                <h5 id="date" style="text-align: center"></h5>
                <div class="box-body" style="border-bottom: 1px solid #c1c1c1;margin-bottom: 0px;">
                    <div class="row">
                        <div style="display: none;" class="loading-sla" id="loader1" style=""></div>
                        <div class="col-lg-12" id="grafik-utama">
                            <div id="general-sl" style="height:250px;width:100%"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-body" style="border-bottom: 1px solid #c1c1c1;margin-bottom: 0px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 style="text-align: center;font-weight: 600;"><i class="fa fa-info-circle"></i> Trend
                                Aircraft Reg</h4>
                            <form role="form" style="margin-top: 20px;">
                                <div class="col-md-1"></div>
                                <div class="col-md-2" id="monthly_start_date">
                                    <label for="inputEmail3" class="control-label">Start Date</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right monthpicker"
                                               placeholder="Start Date"
                                               id="startPeriode">
                                    </div>
                                </div>
                                <div class="col-md-2" id="monthly_end_date">
                                    <label for="inputEmail3" class="control-label">End Date</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right monthpicker"
                                               placeholder="End Date"
                                               id="endPeriode">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="control-label">Aircraft Reg</label>
                                        <div>
                                            <div>
                                                <select class="form-control selectpicker" multiple data-max-options="6"
                                                        id="aircraft_reg" data-size="5" data-live-search="true">
                                                    <option disabled>Select Aircraft</option>
                                                    <?php foreach ($acRegs as $acReg): ?>
                                                        <option value="<?php echo $acReg['acreg'] ?>"><?php echo $acReg['acreg'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="control-label">SL Type</label>
                                        <div class="col-sm-10" style="padding-right: 0px;padding-left: 0px;">
                                            <div class="ddcustomer">
                                                <select class="form-control" id="sl_type">
                                                    <option disabled>Select Type</option>
                                                    <option value="all" selected>ALL</option>
                                                    <option value="SL 1 3 Jam">SL 1 3 Jam</option>
                                                    <option value="SL 2 24 Jam">SL 2 24 Jam</option>
                                                    <option value="SL 3 72 Jam">SL 3 72 Jam</option>
                                                    <option value="SL 4 72 Jam">SL 4 72 Jam</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label for="inputEmail3" class="control-label"></label>
                                    <button type="button" class="btn btn-flat bg-navy"
                                            id="search" style="margin-top: 23px;"><i class="fa fa-search"></i> Search
                                    </button>
                                </div>
                                <div class="col-md-1"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="detailGrafik" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg modal-ku" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0f2233">
                            <h3 class="box-title-detail color-title"
                                style="text-align: center">Detail SL/ Aircraft Type</h3>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-4" id="detail-grafik-sl">
                                    <div id="detail-sl-1" class="grafik1 display-none"
                                         style="height:250px;width:100%"></div>
                                </div>
                                <div class="col-lg-4" id="detail-grafik-sl">
                                    <div id="detail-sl-2" class="grafik2 display-none"
                                         style="height:250px;width:100%"></div>
                                </div>
                                <div class="col-lg-4" id="detail-grafik-sl">
                                    <div id="detail-sl-3" class="grafik3 display-none"
                                         style="height:250px;width:100%"></div>
                                </div>
                            </div>
                            <div class="row">
								<div style="display: none;" class="loading-sla" id="loader2" style=""></div>
                                <div class="col-lg-4" id="detail-grafik-sl">
                                    <div id="detail-sl-4" class="grafik4 display-none"
                                         style="height:250px;width:100%"></div>
                                </div>
                                <div class="col-lg-4" id="detail-grafik-sl">
                                    <div id="detail-sl-5" class="grafik5 display-none"
                                         style="height:250px;width:100%"></div>
                                </div>
                                <div class="col-lg-4" id="detail-grafik-sl">
                                    <div id="detail-sl-6" class="grafik6 display-none"
                                         style="height:250px;width:100%"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i
                                            class="fa fa-rotate-left"></i> Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- End Detail SL / Aircraft -->
            <!--start modal detal SL-->
            <div class="modal fade" id="detailModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg modal-ku" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #0f2233">
                            <h3 class="box-title-detail color-title" id="nameAircraft"
                                style="text-align: center"></h3>
                        </div>
                        <input type="text" style="display: none" id="inputAcreg" value="">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="col-sm-3 control-label"
                                       style="margin-top: 5px;padding-right:0px;">Week</label>
                                <div class="col-sm-9" style="padding-right:0px;padding-left:0px;">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="number" class="form-control pull-right" id="week">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                <button class="btn btn-flat bg-navy" id="search_week">
                                    <i class="fa fa-search"></i> Search
                                </button>
                            </div>
                        </div>
                        <div class="modal-body">
                            <table class="table table-hover table-bordered table-gmf" id="detailSlTable"style="margin-left: -15px;">
                                <thead style="background-color: #05354D">
                                <tr>
                                    <th class="text-center">No</th>
									<th class="text-center">Order Number</th>
                                    <th class="text-center">Aircraft Reg</th>
                                    <th class="text-center">Material PN</th>
                                    <th class="text-center">Posting Date</th>
                                    <th class="text-center">Posting Time</th>
                                    <th class="text-center">Available (Day)</th>
                                    <th class="text-center">Reservation (Time)</th>
                                    <th class="text-center">Group</th>
                                    <th class="text-center">Performance</th>
                                    <th class="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i
                                            class="fa fa-rotate-left"></i> Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--    end modal start detail sl-->
            <!-- Start Detail SL Tabel -->
            <div class="box">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 style="text-align: center;font-weight: 600;margin-bottom: 3px;"><i
                                    class="fa fa-calendar"></i> Detail Robbing</h4>
                        <div class="box-body">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">JAN</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='01'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[0] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">FEB</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='02'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[1] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">MAR</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='03'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[2] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">APR</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='04'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[3] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">MAY</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='05'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[4] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">JUN</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='06'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[5] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">JUL</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='07'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[6] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">AUG</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='08'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[7] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">SEP</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='09'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[8] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">OCT</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='10'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[9] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">NOV</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='11'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[10] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="robbing">
                                            <div class="box-header header-robbing">
                                                <h5 class="box-title">DEC</h5>
                                            </div>
                                            <div class="value-robbing">
                                                <p data-value='12'
                                                   style="font-weight:bold;padding-top: 25px; font-size: 20px"><?= $dataRobbing[11] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Detail SL Tabel -->

    <!--start modal detal SL-->
    <div class="modal fade" id="detailRobbing" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #0f2233">
                    <h3 class="box-title-detail color-title" id="dateRobbing" style="text-align: center"></h3>
                </div>
                <div class="modal-body">
                    <table class="table table-hover table-bordered table-gmf" id="detailRobbingTable">
                        <thead style="background-color: #05354D">
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Material PN</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Donatur</th>
                            <th class="text-center">Recipient</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i
                                    class="fa fa-rotate-left"></i> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    end modal start detail sl-->
</section>
<!-- End Body Pages -->


<!-- Start JScript -->
<script type="text/javascript">
    //For Date
    var date = new Date();
    var month = [];
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var tgl = date.getDate();
    var bulan = month[date.getUTCMonth()];
    var tahun = date.getUTCFullYear();
    var base_url = '<?php echo base_url(); ?>index.php/';

    $(document).ready(function () {
        $('#date').html('YTD: ' + tgl + ' ' + bulan + ' ' + tahun);
        generalSlGrafik();
        $(".display-none").show();

    });

    $(document).on("click", "#search_week", function (e) {
        var inputAcreg = $('#inputAcreg').val();
        var week = $('#week').val();

        detailSlTable(inputAcreg, week);
    });

    $(document).on("click", "#search", function (e) {
        var startPeriode = $("#startPeriode").val();
        var endPeriode = $("#endPeriode").val();
        var acregSearch = [];
        var acregSearch = $('#aircraft_reg').val();
        var slType = $('#sl_type').find(":selected").val();
        if (startPeriode == '' || endPeriode == '' || acregSearch == '') {
            notif('error', "Please complete the fill first");
        } else {
            $("#detailGrafik").modal("show");
            var acregLength = [];
            var acregLength = $('#aircraft_reg').val();
            $(".display-none").hide();

            if (acregLength.length == 1) {
                $('div[id^="detail-grafik-sl"]').removeClass('col-lg-4').removeClass('col-lg-6').addClass('col-lg-12');
                detailSlGrafik1();
                $("#grafik-add").hide();
            } else if (acregLength.length == 2) {
                $('div[id^="detail-grafik-sl"]').removeClass('col-lg-4').removeClass('col-lg-12').addClass('col-lg-6');
                detailSlGrafik1();
                detailSlGrafik2();
                $("#grafik-add").hide();
            } else if (acregLength.length == 3) {
                $('div[id^="detail-grafik-sl"]').removeClass('col-lg-6').removeClass('col-lg-12').addClass('col-lg-4');
                detailSlGrafik1();
                detailSlGrafik2();
                detailSlGrafik3();
                $("#grafik-add").hide();
            } else if (acregLength.length == 4) {
                $('div[id^="detail-grafik-sl"]').removeClass('col-lg-6').removeClass('col-lg-12').addClass('col-lg-4');
                detailSlGrafik1();
                detailSlGrafik2();
                detailSlGrafik3();
                detailSlGrafik4();
                $("#grafik-add").show();
            } else if (acregLength.length == 5) {
                $('div[id^="detail-grafik-sl"]').removeClass('col-lg-6').removeClass('col-lg-12').addClass('col-lg-4');
                detailSlGrafik1();
                detailSlGrafik2();
                detailSlGrafik3();
                detailSlGrafik4();
                detailSlGrafik5();
                $("#grafik-add").show();
            } else {
                $('div[id^="detail-grafik-sl"]').removeClass('col-lg-6').removeClass('col-lg-12').addClass('col-lg-4');
                detailSlGrafik1();
                detailSlGrafik2();
                detailSlGrafik3();
                detailSlGrafik4();
                detailSlGrafik5();
                detailSlGrafik6();
                $("#grafik-add").show();
            }
        }

    });

    $(".robbing").click(function () {
        var value = $(this).find("p").data("value");
        detailRobbing(value);
    });


    // $('#aircraft_reg').selectpicker().change(function () {
        // toggleSelectAll($(this));
    // }).trigger('change');

    // start hight chart  general-sl
    function generalSlGrafik() {
        var status = 'General';
        var startPeriode = null;
        var endPeriode = null;

        $.ajax({
            url: "<?= base_url(); ?>index.php/Sla_Dashboard/getDataGrafikDash_new",
            data: {
                'status': status,
                'startPeriode': startPeriode,
                'endPeriode': endPeriode,

            },
            type: 'POST',
            dataType: "JSON",
                        beforeSend: function () {
                $("#loader1").fadeIn(1000);
            },
            success: function (data) {
                notif('success', 'Data successfully displayed');
                $("#loader1").fadeOut(1000, function () {
                    $("#grafik-utama").fadeIn(1000);
                });
                Highcharts.chart('general-sl', {
                    chart: {
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: false
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: 'Percentage (%)'
                        }
                    },
                    xAxis: {
                        categories: data.months,
                        id: 'x-axis'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#93648d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 1 3 Jam",
                            data: data.data.SL_1_3_Jam,
                            color: "#93648d"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#4cc3d9"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 2 24 Jam",
                            data: data.data.SL_2_24_Jam,
                            color: "#4cc3d9"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#f7ca78"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 3 72 Jam",
                            data: data.data.SL_3_72_Jam,
                            color: "#f7ca78"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 4 72 Jam",
                            data: data.data.SL_4_72_Jam,
                            color: "#05354d"
                        },
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
    }

    // end hight chart  aircraft_percentase js

    // start chart detail-sl
    function detailSlGrafik1() {
        var startPeriode = $("#startPeriode").val();
        var endPeriode = $("#endPeriode").val();
        var acregSearch = [];
        var acregSearch = $('#aircraft_reg').val();
        var slType = $('#sl_type').find(":selected").val();
        var status = 'Detail';
        var acregDefault = 'PK-GNV';
        var grafik = 0;
        $(".grafik1").show();

        $.ajax({
            url: "<?= base_url(); ?>index.php/Sla_Dashboard/getDetailGrafik",
            data: {
                'status': status,
                'acregDefault': acregDefault,
                'acregSearch': acregSearch,
                'startPeriode': startPeriode,
                'endPeriode': endPeriode,
                'slType': slType,
                'grafik': grafik
            },
            type: 'POST',
            dataType: "JSON",
            beforeSend: function(){
                $("#loader2").fadeIn(1000);
            },
            success: function (data) {
                $("#loader2").fadeOut(1000, function () {
                    $("#detail-grafik-sla").fadeIn(1000);
                });
                Highcharts.chart('detail-sl-1', {
                    chart: {
                        height: 250,
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: '<a href="javascript:void(0)" onclick="detailSlTable(\'' + data.acreg + '\' )">' + data.acreg + '</a>'
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: 'Percentage (%)'
                        }
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#93648d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 1 3 Jam",
                            data: data.series.sl1,
                            color: "#93648d"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#4cc3d9"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 2 24 Jam",
                            data: data.series.sl2,
                            color: "#4cc3d9"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#f7ca78"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 3 72 Jam",
                            data: data.series.sl3,
                            color: "#f7ca78"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 4 72 Jam",
                            data: data.series.sl4,
                            color: "#05354d"
                        },
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
        // $('.highcharts-title').click(function () {
        //     var chart = $("#detail-sl-1").highcharts();
        //     detailSlTable(chart.options.title.text);
        // });
    }

    function detailSlGrafik2() {
        var startPeriode = $("#startPeriode").val();
        var endPeriode = $("#endPeriode").val();
        var acregSearch = [];
        var acregSearch = $('#aircraft_reg').val();
        var slType = $('#sl_type').find(":selected").val();
        var status = 'Detail';
        var acregDefault = 'PK-GNU';
        var grafik = 1;
        $(".grafik2").show();

        $.ajax({
            url: "<?= base_url(); ?>index.php/Sla_Dashboard/getDetailGrafik",
            data: {
                'status': status,
                'acregDefault': acregDefault,
                'acregSearch': acregSearch,
                'startPeriode': startPeriode,
                'endPeriode': endPeriode,
                'slType': slType,
                'grafik': grafik
            },
            type: 'POST',
            dataType: "JSON",
            success: function (data) {
                Highcharts.chart('detail-sl-2', {
                    chart: {
                        height: 250,
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: '<a href="javascript:void(0)" onclick="detailSlTable(\'' + data.acreg + '\')">' + data.acreg + '</a>'
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: 'Percentage (%)'
                        }
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#93648d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 1 3 Jam",
                            data: data.series.sl1,
                            color: "#93648d"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#4cc3d9"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 2 24 Jam",
                            data: data.series.sl2,
                            color: "#4cc3d9"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#f7ca78"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 3 72 Jam",
                            data: data.series.sl3,
                            color: "#f7ca78"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 4 72 Jam",
                            data: data.series.sl4,
                            color: "#05354d"
                        },
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
    }

    function detailSlGrafik3() {
        var startPeriode = $("#startPeriode").val();
        var endPeriode = $("#endPeriode").val();
        var acregSearch = [];
        var acregSearch = $('#aircraft_reg').val();
        var slType = $('#sl_type').find(":selected").val();
        var status = 'Detail';
        var acregDefault = 'PK-GNT';
        var grafik = 2;
        $(".grafik3").show();

        $.ajax({
            url: "<?= base_url(); ?>index.php/Sla_Dashboard/getDetailGrafik",
            data: {
                'status': status,
                'acregDefault': acregDefault,
                'acregSearch': acregSearch,
                'startPeriode': startPeriode,
                'endPeriode': endPeriode,
                'slType': slType,
                'grafik': grafik
            },
            type: 'POST',
            dataType: "JSON",
            success: function (data) {
                Highcharts.chart('detail-sl-3', {
                    chart: {
                        height: 250,
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: '<a href="javascript:void(0)" onclick="detailSlTable(\'' + data.acreg + '\')">' + data.acreg + '</a>'
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: 'Percentage (%)'
                        }
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#93648d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 1 3 Jam",
                            data: data.series.sl1,
                            color: "#93648d"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#4cc3d9"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 2 24 Jam",
                            data: data.series.sl2,
                            color: "#4cc3d9"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#f7ca78"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 3 72 Jam",
                            data: data.series.sl3,
                            color: "#f7ca78"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 4 72 Jam",
                            data: data.series.sl4,
                            color: "#05354d"
                        },
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
    }

    function detailSlGrafik4() {
        var startPeriode = $("#startPeriode").val();
        var endPeriode = $("#endPeriode").val();
        var acregSearch = [];
        var acregSearch = $('#aircraft_reg').val();
        var slType = $('#sl_type').find(":selected").val();
        var status = 'Detail';
        var acregDefault = 'PK-GNS';
        var grafik = 3;
        $(".grafik4").show();

        $.ajax({
            url: "<?= base_url(); ?>index.php/Sla_Dashboard/getDetailGrafik",
            data: {
                'status': status,
                'acregDefault': acregDefault,
                'acregSearch': acregSearch,
                'startPeriode': startPeriode,
                'endPeriode': endPeriode,
                'slType': slType,
                'grafik': grafik
            },
            type: 'POST',
            dataType: "JSON",
            success: function (data) {
                Highcharts.chart('detail-sl-4', {
                    chart: {
                        height: 250,
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: '<a href="javascript:void(0)" onclick="detailSlTable(\'' + data.acreg + '\')">' + data.acreg + '</a>'
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: 'Percentage (%)'
                        }
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#93648d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 1 3 Jam",
                            data: data.series.sl1,
                            color: "#93648d"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#4cc3d9"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 2 24 Jam",
                            data: data.series.sl2,
                            color: "#4cc3d9"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#f7ca78"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 3 72 Jam",
                            data: data.series.sl3,
                            color: "#f7ca78"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 4 72 Jam",
                            data: data.series.sl4,
                            color: "#05354d"
                        },
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
    }

    function detailSlGrafik5() {
        var startPeriode = $("#startPeriode").val();
        var endPeriode = $("#endPeriode").val();
        var acregSearch = [];
        var acregSearch = $('#aircraft_reg').val();
        var slType = $('#sl_type').find(":selected").val();
        var status = 'Detail';
        var acregDefault = 'PK-GNR';
        var grafik = 4;
        $(".grafik5").show();

        $.ajax({
            url: "<?= base_url(); ?>index.php/Sla_Dashboard/getDetailGrafik",
            data: {
                'status': status,
                'acregDefault': acregDefault,
                'acregSearch': acregSearch,
                'startPeriode': startPeriode,
                'endPeriode': endPeriode,
                'slType': slType,
                'grafik': grafik
            },
            type: 'POST',
            dataType: "JSON",
            success: function (data) {
                Highcharts.chart('detail-sl-5', {
                    chart: {
                        height: 250,
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: '<a href="javascript:void(0)" onclick="detailSlTable(\'' + data.acreg + '\')">' + data.acreg + '</a>'
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: 'Percentage (%)'
                        }
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#93648d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 1 3 Jam",
                            data: data.series.sl1,
                            color: "#93648d"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#4cc3d9"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 2 24 Jam",
                            data: data.series.sl2,
                            color: "#4cc3d9"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#f7ca78"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 3 72 Jam",
                            data: data.series.sl3,
                            color: "#f7ca78"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 4 72 Jam",
                            data: data.series.sl4,
                            color: "#05354d"
                        },
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
    }

    function detailSlGrafik6() {
        var startPeriode = $("#startPeriode").val();
        var endPeriode = $("#endPeriode").val();
        var acregSearch = [];
        var acregSearch = $('#aircraft_reg').val();
        var slType = $('#sl_type').find(":selected").val();
        var status = 'Detail';
        var acregDefault = 'PK-GNG';
        var grafik = 5;
        $(".grafik6").show();

        $.ajax({
            url: "<?= base_url(); ?>index.php/Sla_Dashboard/getDetailGrafik",
            data: {
                'status': status,
                'acregDefault': acregDefault,
                'acregSearch': acregSearch,
                'startPeriode': startPeriode,
                'endPeriode': endPeriode,
                'slType': slType,
                'grafik': grafik
            },
            type: 'POST',
            dataType: "JSON",
            success: function (data) {
                Highcharts.chart('detail-sl-6', {
                    chart: {
                        height: 250,
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: '<a href="javascript:void(0)" onclick="detailSlTable(\'' + data.acreg + '\')">' + data.acreg + '</a>'
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: 'Percentage (%)'
                        }
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#93648d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 1 3 Jam",
                            data: data.series.sl1,
                            color: "#93648d"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#4cc3d9"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 2 24 Jam",
                            data: data.series.sl2,
                            color: "#4cc3d9"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#f7ca78"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 3 72 Jam",
                            data: data.series.sl3,
                            color: "#f7ca78"
                        },
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354d"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "SL 4 72 Jam",
                            data: data.series.sl4,
                            color: "#05354d"
                        },
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
    }

    function detailSlTable(acreg, week) {
        $('#inputAcreg').val(acreg);
        $("#detailModal").modal("show");
        $("#nameAircraft").html(acreg);
        var startPeriode = $("#startPeriode").val();
        var endPeriode = $("#endPeriode").val();
        var slType = $('#sl_type').find(":selected").val();

        var detailSlTable = $("#detailSlTable").DataTable({
            ordering: true,
            destroy: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: {
                url: "<?= base_url() ?>index.php/Sla_Dashboard/detailSlTable",
                type: 'POST',
                data: {
                    'acreg': acreg,
                    'startPeriode': startPeriode,
                    'endPeriode': endPeriode,
                    'slType': slType,
                    'week': week
                }
            },
            "rowCallback": function (row, data, index) {
                $node = this.api().row(row).nodes().to$();
                if (data[11] === 'DELAY') {
                    $node.addClass('red')
                } else {
                    $node.addClass('black')
                }
            },
            "aoColumns": [
                {
                    "mData": "0",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "2",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "3",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "4",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "5",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "6",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "7",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "8",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "9",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "10",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
				{
                    "mData": "11",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                }
            ]
        });
    }

    function detailRobbing(month) {
        $("#detailRobbing").modal("show");
        $('#dateRobbing').html('<i class="fa fa-calendar"></i> Detail Robbing | ' + month);
        $('#detailRobbingTable').DataTable().destroy();
        table = $('#detailRobbingTable').DataTable({

            "processing": true,
            "serverSide": true,
            "order": [],

            "ajax": {
                "url": "<?= base_url() ?>index.php/Sla_Dashboard/detailRobbingTable",
                "type": "POST",
                "data": {
                    'month': month
                }
            },

            "columnDefs": [
                {
                    "targets": [0],
                    "orderable": false
                }
            ]

        });
    }

</script>
<!-- End JScript -->
