<?php
/**
 * Created by PhpStorm.
 * User: ucup
 * Date: 5/19/18
 * Time: 1:23 AM
 */

header("Content-type: application/export_example_xls");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<table border="1" width="100%">
    <thead>
    <tr>
        <th>Material</th>
        <th>Group</th>
    </tr>
    </thead>
    <tbody>

    <?php $i = 1;
    foreach ($sccGroups as $sccGroup) { ?>
        <tr>
            <td><?php echo $sccGroup->material; ?></td>
            <td><?php echo $sccGroup->grouping; ?></td>
        </tr>
        <?php $i++;
    } ?>
    </tbody>
</table>