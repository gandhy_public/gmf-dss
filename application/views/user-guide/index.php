<style>
    .wadah {
        padding-left: 0px;
    }

    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }
</style>

<!-- <section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section> -->
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title_box ?></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <iframe id="myIframe" style="width: 100%"
                        src="<?php echo base_url(); ?>/assets/user-guide/User Guide DSS - GMF All Menu.pdf" frameborder="0"
                        allowFullScreen="true"></iframe>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        document.getElementById('myIframe').height = Math.round(document.getElementById('myIframe').clientWidth / 1280 * 986) + 54;
        window.onresize = function () {
            $('body').html('<div style="text-align:center;width:100%;"><img style="width:50%;" src="<?php echo base_url(); ?>/assets/image/loading.gif" /></div>');
            location.reload();
        };
//        document.getElementById('myIframe').style.height = 'calc(230vh - 611px)';
    });
</script>
