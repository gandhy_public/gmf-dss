<!--
START DIMAS ISLAMI
-->
<script src="<?= base_url() ?>assets/plugins/rounded-corner/rounded-corner.js"></script>
<style>
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin: 0;
  line-height: 1;
}
.wadah{
  padding-left: 5px;
  padding-right: 5px;
}
.font-panel1{
  cursor: unset;
  border-width: 0px;
  font-size: 40px;
  color: #0f2233;
  text-align: center;
}
.font-panel2{
  margin: 8px;
  color: #FD9214 !important;
  text-align: center;
  font-size: 25px;
}
.font-panel3{
  text-align: center;
  font-size: 13px;
  color: maroon;
}
.fa-style{
  font-size: 30px;
  position: relative;
  top: 7px;
  left: 100px;
}
.blt-merah{
  width: 20px;
  height: 20px;
  border-radius: 100%;
  background: #FD9214;
  margin: 7 auto;
}
.blt-navy{
  width: 20px;
  height: 20px;
  border-radius: 100%;
  background: #0f2233;
  margin: 7 auto;
}
.center{
  text-align: center;
}
.inner_in{
  padding:20px;
}
.title-manufacture{
  color: #F2C573;
  text-align: center;
  font-size: 22px;
  font-weight: bold;
  margin-top: 30px;
}
.title-actual{
  color: #fff;
  text-align: left;
  font-size: 25px;
  margin-top: 70px;
  margin-left: 25px;
}
.title-target{
  color: #fff;
  text-align: left;
  font-size: 20px;
  margin-top: 0px;
  margin-left: 25px;
}
.value-actual-manufacture-garuda{
  font-size: 60px;
  color: #fff;
  margin-right: 20px;
  text-align: left;
  margin-top: -65px;
}
.value-actual-manufacture-citilink{
  font-size: 60px;
  color: #fff;
  margin-right: 20px;
  text-align: left;
  margin-top: -65px;
}
.value-target{
  font-size: 30px;
  margin-right: 20px;
  color: #fff;
  margin-top: -35px;
}
  .widget-user .widget-user-header {
      padding: 1px;
      height: 60px;
      border-top-right-radius: 11px;
      border-top-left-radius: 11px;
  }
  .widget-user .widget-user-image {
      position: absolute;
      top: 5px;
      left: 44%;
  }
  .box .border-right {
      border-right: 1px solid #eac8c8;
  }
  .form-group{
      margin-bottom: 0px;
  }
  .widget-user .box-footer {
      padding-top: 0px;
  }
  .box-footer {
      border-top-left-radius: 0;
      border-top-right-radius: 0;
      border-bottom-right-radius: 11px;
      border-bottom-left-radius: 11px;
      border-top: 1px solid #f4f4f4;
      padding: 0px;
      background-color: #fff;
  }
  .description-block {
      display: block;
      margin: 3px 0;
      text-align: center;
  }
  .box {
      position: relative;
      border-radius: 3px;
      background: #ffffff;
      border-top: 3px solid #d2d6de;
      margin-bottom: 13px;
      width: 100%;
      box-shadow: 0 8px 12px 4px rgba(0,0,0,0.2);
  }
    .description-block>.description-header {
      margin: 0;
      padding: 0;
      font-size: 29px;
      font-weight: bold;
  }
  .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #0f2233;
    border-color: #0e1f2e;
}
</style>

<!-- <section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section> -->

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= $title_box ?></h3>
      <div class="box-tools pull-right">
         <label class="control-label" style="font-weight: 100;"><i class="fa  fa-calendar"></i> Year To Date
          :</label>
          <label class="control-label" id="date"></label>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="box-header with-border" style="line-height: 1;padding: 0px;margin-bottom: 5px;">
                <input type="hidden" class="form-control" id="year1">
                <input type="hidden" class="form-control" id="year2">
                <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12" style="margin-top: 10px;"></div>
                <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12" style="margin-top: 10px;">
                    <div class="box box-widget widget-user" style="border-radius: 11px;">
                        <div class="widget-user-header bg-navy">
                            <h3 class="title-manufacture">GARUDA</h3>
                        </div>
                        <div class="widget-user-image">
                            <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                 style="border: transparent;width: 110px;">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-6 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header" id="actual-garuda" style="margin-left:22px"></h5>
                                        <span class="description-text">Actual</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="description-block">
                                        <h5 class="description-header" id="target-garuda"></h5>
                                        <span class="description-text">Target</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12" style="margin-top: 10px;">
                    <div class="box box-widget widget-user" style="border-radius: 11px;">
                        <div class="widget-user-header bg-navy">
                            <h3 class="title-manufacture">CITILINK</h3>
                        </div>
                        <div class="widget-user-image">
                            <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                 style="border: transparent;width: 100px;">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-6 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header" id="actual-citilink"  style="margin-left:22px"></h5>
                                        <span class="description-text">Actual</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="description-block">
                                        <h5 class="description-header" id="target-citilink"></h5>
                                        <span class="description-text">Target</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-12 col-md-12 col-xs-12" style="margin-top: 10px;"></div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
              <label>Aircraft Type</label>
              <select class="form-control select2" multiple="multiple"  id="aircraft_type" style="width: 100%;">
              </select>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
              <label>Aircraft Reg</label>
              <select class="form-control select2" multiple="multiple" style="width: 100%;" id="aircraft_reg">
              </select>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
            <div class="form-group">
              <label>Start Period</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="year3">
              </div>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
            <div class="form-group">
              <label>End Period</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                <input type="text" class="form-control" id="year4">
              </div>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
            <div class="form-group">
              <label></label>
                <button class="btn btn-block btn-flat bg-navy" id="pencarian" style="margin-top: 4px;">
                  <i class="fa fa-search"></i> Search
                </button>
            </div>
          </div>
        </div>
      <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="box-header with-border" style="border-bottom: 1px solid #fff;">
            <div class="col-lg-4" id="div1">
              <br><br>
              <div id="grafik2" style="height: 250px"></div>
            </div>
            <div class="col-lg-4" id="div2">
              <br><br>
              <div id="grafik3" style="height: 250px"></div>
            </div>
            <div class="col-lg-4" id="div3">
              <div class="col-lg-6"></div>
              <div class="col-lg-6">
                <select class="form-control"  id="tipe-functional" style="width: 100%;">
                  <option value="Item">Item</option>
                  <option value="Reg">Type</option>
                </select>
              </div>
              <br><br>
              <div id="grafik4" style="display: none;height: 250px"></div>
              <div id="grafik5" style="height: 250px"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<script>

  var base_url    = '<?php echo base_url(); ?>index.php/';
  var yAxisLimit  = [0, 50, 100, 150];

  $(document).ready(function(){
    $(".select2").select2();
    $('#year1').datepicker({
      format: "yyyy-mm",
      viewMode: "months",
      minViewMode: "months",
      autoclose: true,
    });
    $('#year2').datepicker({
      format: "yyyy-mm",
      viewMode: "months",
      minViewMode: "months",
      autoclose: true,
    });
    $('#year3').datepicker({
      format: "mm-yyyy",
      viewMode: "months",
      minViewMode: "months",
      autoclose: true,
    });
    $('#year4').datepicker({
      format: "mm-yyyy",
      viewMode: "months",
      minViewMode: "months",
      autoclose: true,
    });

    $('input[type="checkbox"]').attr('checked', true);
    combo_aircraft_type();
    combo_aircraft_reg();

    var tgl    = new Date();
    var bulan1 = (tgl.getMonth() - 1, 1);
    var tahun1 = tgl.getFullYear();
    var bulan2 = tgl.getMonth()+1;
    var tahun2 = tgl.getFullYear();
    newDate1   = tahun1+ '-' +bulan1 ;
    newDate2   = tahun2+ '-' +bulan2;
    newDate3   = '0'+bulan1+ '-' +tahun1;
    newDate4   = ((bulan2.length < 2) ? '0' : '')+bulan2+ '-' +tahun2;

    var date    = new Date();
    var month   = new Array();
      month[0]  = "January";
      month[1]  = "February";
      month[2]  = "March";
      month[3]  = "April";
      month[4]  = "May";
      month[5]  = "June";
      month[6]  = "July";
      month[7]  = "August";
      month[8]  = "September";
      month[9]  = "October";
      month[10] = "November";
      month[11] = "December";
    var tgls     = date.getDate();
    var bulans   = month[date.getUTCMonth()];
    var tahuns   = date.getUTCFullYear();

    $('#date').html(tgls + ' ' + bulans + ' ' + tahuns);
    $('#year1').val(newDate1);
    $('#year2').val(newDate2);
    $('#year3').val(newDate3);
    $('#year4').val(newDate4);
    //gabungan(newDate1, newDate2);
    interior(newDate3, newDate4);
    exterior(newDate3, newDate4);
    functional_by_item(newDate3, newDate4);
    target_maskapai();

    $('#tipe-functional').change(function () {
      var tipe = $('#tipe-functional').val();
      var tgl1 =  $('#year3').val();
      var tgl2 =  $('#year4').val();
      if(tipe == 'Item'){
        $('#grafik5').show();
        $('#grafik4').hide();
        functional_by_item(tgl1, tgl2);
      }else{
        $('#grafik5').hide();
        $('#grafik4').show();
        functional_by_reg(tgl1, tgl2);
      }
    });

    /* $('input[type="checkbox"]').click(function(){
        var inputValue = $(this).attr("value");
        $("#" + inputValue).toggle(function () {
          if ($(this).is(":checked")) {
            $("#" + inputValue).removeClass('col-lg-4').addClass('col-lg-6').show();
          }else{
          }
        });
    });
    */

  });

  $('#pencarian').click(function(e) {
    var tgl1        = $('#year3').val();
    var tgl2        = $('#year4').val();
    var get_year1   = tgl1.split('-')[1];
    var get_year2   = tgl2.split('-')[1];
    var get_month1  = tgl1.split('-')[0];
    var get_month2  = tgl2.split('-')[0];

    if(get_year1 == get_year2 && get_month1 == get_month2){
      functional_by_reg(tgl1, tgl2);
      functional_by_item(tgl1, tgl2);
      interior(tgl1, tgl2);
      exterior(tgl1, tgl2);
    }else if(get_year1 == get_year2 && get_month1 < get_month2){
      functional_by_reg(tgl1, tgl2);
      functional_by_item(tgl1, tgl2);
      interior(tgl1, tgl2);
      exterior(tgl1, tgl2);
    }else{
      notif('error', "Sorry can't, check your date");
    }
  });

   function target_maskapai(){
    var tahun1                = $('#year1').val();
    var tahun2                = $('#year2').val();
    $.ajax({
      url: base_url + 'Cabin_Performance/data_akumulasi/' + tahun1 + '/' + tahun2,
      type: "GET",
      dataType: "JSON",
      success: function (data){
        var target                  = data.data_target;

        if (data.data_performance[0] == undefined){
          var actual_garuda = 0;
        }else{
          var actual_garuda = data.data_performance[0];
        }

        if (data.data_performance[1] == undefined){
          var actual_citilink = 0;
        }else{
          var actual_citilink = data.data_performance[1];
        }

        $('#target-garuda').html(target + '%' +  ' <i class="fa fa-minus" style="color:#F2C573;"></i>');
        $('#target-citilink').html(target + '%' +  ' <i class="fa fa-minus" style="color:#F2C573;"></i>');
        if(data.data_performance[0] > target){
          $('#actual-garuda').html(actual_garuda + '%' + ' <i class="fa fa-caret-up" style="color:#8ec3a7"></i>');
          $('.value-actual-manufacture-garuda').css('color', '#8ec3a7');
        }else{
          $('#actual-garuda').html(actual_garuda + '%' + ' <i class="fa fa-caret-down" style="color:#f24738"></i>');
          $('.value-actual-manufacture-garuda').css('color', '#f24738');
        }
        if(data.data_performance[1] > target){
          $('#actual-citilink').html(actual_citilink + '%' + ' <i class="fa fa-caret-up" style="color:#8ec3a7"></i>');
           $('.value-actual-manufacture-citilink').css('color', '#8ec3a7');
        }else{
          $('#actual-citilink').html(actual_citilink + '%' + ' <i class="fa fa-caret-down" style="color:#f24738"></i>');
           $('.value-actual-manufacture-citilink').css('color', '#f24738');
        }
      }
    });
  }

//=====================================================COMBOBOX=====================================================\\

  function combo_aircraft_type(){
    var list_aircraft_type;
    $.ajax({
      url: base_url + 'Cabin_Performance/combo_aircraft_type',
      type: "GET",
      dataType: "JSON",
      success: function (data) {
            $(data).each(function (key, val) {
                list_aircraft_type += '<option value="' + val.id + '">' + val.name_aircraft + '</option>';
            });
            $('#aircraft_type').html(list_aircraft_type);
      }
    });
  }

  function combo_aircraft_reg(){
    $('#aircraft_type').change(function () {
      var aircraft_type   = $('#aircraft_type').val().toString();
      aircraft_type       = aircraft_type.replace(/,/g, '-');
      var counting        = $('#aircraft_type').find('option:selected').length
      $.ajax({
        url: base_url + 'Cabin_Performance/combo_aircraft_reg/' + aircraft_type,
        type: "GET",
        dataType: "JSON",
        success: function (data){
          if(counting >= 2){
            var tampung = [];
            $(data).each(function (key, val) {
              tampung.push(val.id);
            });
            var list_aircraft_reg = '<option value="'+tampung+'" selected>All</option>';
          }else if(counting == 1){
            var list_aircraft_reg;
            $(data).each(function (key, val) {
            list_aircraft_reg += '<option value="' + val.id + '">' + val.name_ac_reg + '</option>';
            });
          }
          $('#aircraft_reg').html(list_aircraft_reg);
        }
      });
    })
  }

//=====================================================AKUMULASI=====================================================\\

/* function gabungan(tgl1, tgl2){
  $.ajax({
    url: base_url + 'Cabin_Performance/data_akumulasi/' + tgl1 + '/' + tgl2,
    type: "GET",
    dataType: "JSON",
    error: function(){
      notif('error', 'Connection disorders occurred, Please try again');
    },
    success: function (data) {
      notif('success', 'Data Accumulation successfully displayed');
        var chart2 =  Highcharts.chart('grafik1', {
          chart: {
              type: 'column'
          },
          title: {
              text: ''
          },
          credits:{
              enabled: false
          },
          exporting: {
            enabled: false
          },
          xAxis: {
            categories: data.categories,
            crosshair: true
          },
          yAxis: {
            min: 0,
              tickPositioner: function() {
              return yAxisLimit;
              },
            title: {
              text: 'Percent (%)'
            }
          },
          tooltip: {
            headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:#000;padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y} %</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0,
              minPointLength: 3,
            },
            series: {
              pointPadding: 0,
              groupPadding: 0.2
            }
          },
          series: [{
              name: 'Target',
              data: data.data_target,
              color: '#FD9214'
          }, {
              name: 'Performance',
              data: data.data_performance,
              color: '#05354d'
          }]
        });
      }
    });
  }
*/


//=======================================================INTERIOR=======================================================\\

  var faktor_in_global;
  var nm_aircraft_in_global;
  var nm_reg_in_global;

  function modal_interior(faktor, id_aircraft, nm_aircraft, id_reg, tgl1, tgl2){
    $('#modal-interior').modal('show');
    $('#div-reg-interior').show();
    faktor_in_global = faktor;
    nm_aircraft_in_global = nm_aircraft;
    reg_interior(nm_aircraft, id_reg, tgl1, tgl2);
    $('#title-interior').html(faktor + ' ' + nm_aircraft);
  }

  function modal_aspek_interior_show(id_reg, tgl1, tgl2, nm_reg){
    $('#div-aspek-interior').show();
    $('#div-reg-interior').hide();
    nm_reg_in_global = nm_reg;
    aspek_interior(id_reg, tgl1, tgl2);
    $('#title-interior').html(nm_aircraft_in_global + ' ' + nm_reg);
  }

  function detail_faktor_pendukung_interior(id_reg, id_item, tgl1, tgl2, nm_item){
    $('#div-list-interior').show();
    detail_interior(id_reg, id_item, tgl1, tgl2);
    $('#title-interior').html(nm_item);
    $('#div-aspek-interior').hide();
  }

  function back_to_reg_interior(){
    $('#div-reg-interior').show();
    $('#title-interior').html(faktor_in_global + ' ' + nm_aircraft_in_global);
    $('#div-aspek-interior').hide();
  }

  $(document).on('hide.bs.modal','#modal-interior', function () {
    $("#div-aspek-interior").css("display","none") ;
    $('#div-list-interior').css("display","none") ;
  });

  function back_to_table_interior(){
    $('#div-aspek-interior').show();
    $('#title-interior').html(nm_aircraft_in_global + ' ' + nm_reg_in_global);
    $('#div-list-interior').hide();
  }


  function interior(tgl1, tgl2){
    var aircraft_type  = ($('#aircraft_type').val().toString() == "") ? "00" : $('#aircraft_type').val().toString();
    aircraft_type      = aircraft_type.replace(/,/g, '-');
    var aircraft_reg   =($('#aircraft_reg').val().toString() == "") ? "00" : $('#aircraft_reg').val().toString();
    aircraft_reg       = aircraft_reg.replace(/,/g, '-');
    $.ajax({
      url: base_url + 'Cabin_Performance/data_interior/' + tgl1 + '/' + tgl2,
      type: "POST",
      dataType: "JSON",
      data:{
        aircraft_type: aircraft_type,
        aircraft_reg: aircraft_reg
      },
      error: function(){
        notif('error', 'Connection disorders occurred, Please try again');
      },
      success: function (data) {
        notif('success', 'Data interior successfully displayed');
        var chart2 =  Highcharts.chart('grafik2', {
          chart: {
              type: 'column',
              width: '350'
          },
          title: {
              text: 'Interior Appearance',
              margin: 0
          },
          credits:{
              enabled: false
          },
          exporting: {
            enabled: false
          },
          xAxis: {
            categories: data.categories,
            crosshair: true
          },
          yAxis: {
              min: 0,
              tickPositioner: function() {
              return yAxisLimit;
              },
              title: {
                  text: 'Percent (%)'
              }
          },
          tooltip: {
              headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:#000;padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y} %</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                          modal_interior(chart2.options.title.text, aircraft_type, this.category, aircraft_reg, tgl1, tgl2);
                        },
                    }
                },
                pointPadding: 0,
                groupPadding: 0.2,
                borderRadiusTopLeft: '10px',
                borderRadiusTopRight: '10px'
              }
          },
                    series: [{
                        name: 'Target',
                        data: data.data_target,
                        color: '#FD9214',
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#000',
                            align: 'right',
                            format: '{point.y:.2f}', // one decimal
                            y: -32, // 10 pixels down from the top
                        }
                    }, {
                        name: 'Performance',
                        data: data.data_performance,
                        color: '#8ec3a7',
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#000',
                            align: 'right',
                            format: '{point.y:.2f}', // one decimal
                            y: -32, // 10 pixels down from the top
                        }
                    }]
        });
      }
    });
  }


  function reg_interior(aircraft, id_reg, tgl1, tgl2){
    $("#table-reg-interior").DataTable({
      "destroy": true,
      "ordering": false,
      "searching": true,
      "paging": true,
      lengthChange: true,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'nm_reg'}
      ],
        ajax: {
          type: 'GET',
          url: base_url + 'Cabin_Performance/reg_interior/' + aircraft + '/' + id_reg,
          dataType: 'JSON',
          dataSrc : function (json) {
            var return_data = new Array();
            var no=1;
              for(var i=0;i<json.length; i++){
                return_data.push({
                  'no'      : '<center>'+no+'</center>',
                  'nm_reg'  : '<a style="cursor:pointer;color: #333;" onclick="modal_aspek_interior_show(\'' + json[i].id + '\',\'' + tgl1 +  '\',\'' + tgl2 +  '\',\'' + json[i].name_ac_reg +  '\')"><center>'+json[i].name_ac_reg+'</center></a>',
              });
              no++;
            }
          return return_data;
        },
      },
    });
  }


  function aspek_interior(id_reg, tgl1, tgl2){
    $.ajax({
      url: base_url + 'Cabin_Performance/aspek_interior/' + id_reg + '/' + tgl1 + '/' + tgl2,
      type: "GET",
      dataType: "JSON",
      success: function (data) {
        $('#div-aspek-interior').html('');
         var list = '<button class="btn btn-sm btn-flat pull-right" onclick="back_to_reg_interior();" type="submit" style="background-color: #0f2233;color: #fff;"><i class="fa  fa-arrow-circle-o-left"></i>&nbsp;Back</button><br><br>';
        for (var i = 0; i < data.length; i++) {

          if(data[i]['data_performance'] == 0){
            var perform = 100;
          }else{
            var perform = data[i]['data_performance'];
          }

          if(perform >= data[i]['data_target']){
            var fa    = 'fa fa-caret-up fa-style';
            var span  = 'description-percentage text-green';
            var note  = 0;
          }else{
            var fa    = 'fa fa-caret-down fa-style';
            var span  = 'description-percentage text-red';
            var note  = 1;
          }
                list +='<a href="javascript:void(0)" title="Detail Performance '+data[i]['categories']+'" onclick="detail_faktor_pendukung_interior(\'' +id_reg +  '\',\'' + data[i].id_item +  '\', \'' + tgl1 +  '\',\'' + tgl2 +  '\',\'' + data[i]['categories'] +  '\',\'' + note +  '\');">'+
                        '<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 with-border" style="border-right: 2px solid #121312;">'+
                          '<h2 class="font-panel1">'+perform+'%</h2>'+
                          '<h3 class="font-panel2">'+data[i]['data_target']+'%</h3>'+
                          '<h3 class="font-panel3" id="detail-aspect">'+data[i]['categories']+'</h3>'+
                          '<span class="'+span+'"><i class="'+fa+'"></i></span>'+
                        '</div>'+
                      '</a>';
          }
                list +='<br><br>'+
                          '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'+
                            '<table style="position: fixed;bottom: 15px;right: 5px;width: 200px;border: 2px solid #0f2233;">'+
                                '<tr>'+
                                  '<td>&nbsp;</td>'+
                                  '<td>&nbsp;</td>'+
                                  '<td><div class="blt-merah"></div></td>'+
                                  '<td>&nbsp;</td>'+
                                  '<td><h5 style="font-weight: 500;">Target</h5></td>'+
                                  '<td>&nbsp;&nbsp;&nbsp;</td>'+
                                  '<td><div class="blt-navy"></div></td>'+
                                  '<td>&nbsp;</td>'+
                                  '<td><h5 style="font-weight: 500;">Performance</h5></td>'+
                                '</tr>'+
                            '</table>'+
                          '</div>';
            $('#div-aspek-interior').append(list);
        }
    });
  }

  function detail_interior(id_reg, id_item, tgl1, tgl2, note){
    if(note == 0){
    }else{
      $.ajax({
        url: base_url + 'Cabin_Performance/mitigasi_interior/' + id_reg + '/' + id_item  + '/' + tgl1 + '/' + tgl2,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
          for (var i = 0; i < data.length; i++) {
            notif('warning', '<h3>MITIGATION !!!</h3><table class="table table-bordered"><tr><td>Why</td><td>'+data[i]['cabin_mitigation_why']+'</td></tr><tr><td>Solution</td><td>'+data[i]['cabin_mitigation_solution']+'</td></tr></table>');
          }
        }
      })
    }
    $("#table-aspek-interior").DataTable({
      "destroy": true,
      "ordering": false,
      "searching": true,
      "paging": true,
      lengthChange: true,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'nama'},
        {data : 'tot_dirty'},
        {data : 'tot_defect'},
        {data : 'tot_item'},
        {data : 'renmark'},
        {data : 'perform'},
      ],
        ajax: {
          type: 'GET',
          url: base_url + 'Cabin_Performance/detail_sub_interior/' + id_reg + '/' + id_item  + '/' + tgl1 + '/' + tgl2,
          dataType: 'JSON',
          dataSrc : function (json) {
            var return_data = new Array();
            var no=1;
              for(var i=0;i<json.length; i++){
                var perform = ((parseInt(json[i].total_item)-(parseInt(json[i].total_dirty)+parseInt(json[i].total_defect)))/parseInt(json[i].total_item))*100;
                perform = perform.toFixed(2);
                if(perform == 100.00){
                  var fix_perform = 100;
                }else{
                  var fix_perform = ((parseInt(json[i].total_item)-(parseInt(json[i].total_dirty)+parseInt(json[i].total_defect)))/parseInt(json[i].total_item))*100;
                  fix_perform     =  fix_perform.toFixed(2);
                }

                if (json[i].remark == null){
                  var remark = '-';
                }else if(json[i].remark == ''){
                  var remark = '-'
                }else{
                  var remark = json[i].remark;
                }

                return_data.push({
                  'no'        : '<center>'+no+'</center>',
                  'nama'      : '<center>'+json[i].nama_item_sub+'</center>',
                  'tot_dirty' : '<center>'+json[i].total_dirty+'</center>',
                  'tot_defect': '<center>'+json[i].total_defect+'</center>',
                  'tot_item'  : '<center>'+json[i].total_item+'</center>',
                  'renmark'   : '<center>'+remark+'</center>',
                  'perform'   : '<center>'+fix_perform+'%</center>',
              });
              no++;
            }
          return return_data;
        },
      },
    });
  }

//=======================================================EXTERIOR=======================================================\\

  var faktor_ex_global;
  var nm_aircraft_ex_global;
  var nm_reg_ex_global;

  function modal_exterior(faktor, id_aircraft, nm_aircraft, id_reg, tgl1, tgl2){
    $('#modal-exterior').modal('show');
    $('#div-reg-exterior').show();
    faktor_ex_global = faktor;
    nm_aircraft_ex_global = nm_aircraft;
    reg_exterior(nm_aircraft, id_reg, tgl1, tgl2);
    $('#title-exterior').html(faktor + ' ' + nm_aircraft);
  }

  function modal_aspek_exterior_show(id_reg, tgl1, tgl2, nm_reg){
    $('#div-aspek-exterior').show();
    $('#div-aspek-exterior').css('height', '280px');
    $('#div-reg-exterior').hide();
    nm_reg_ex_global = nm_reg;
    aspek_exterior(id_reg, tgl1, tgl2);
    $('#title-exterior').html(nm_aircraft_ex_global + ' ' + nm_reg);
  }

  function detail_faktor_pendukung_exterior(id_reg, id_item, tgl1, tgl2, nm_item){
    $('#div-list-exterior').show();
    detail_exterior(id_reg, id_item, tgl1, tgl2);
    $('#title-exterior').html(nm_item);
    $('#div-aspek-exterior').hide();
  }

  function back_to_reg_exterior(){
    $('#div-reg-exterior').show();
    $('#title-exterior').html(faktor_ex_global + ' ' + nm_aircraft_ex_global);
    $('#div-aspek-exterior').hide();
  }

  function back_to_table_exterior(){
    $('#div-aspek-exterior').show();
    $('#title-exterior').html(nm_aircraft_ex_global + ' ' + nm_reg_ex_global);
    $('#div-list-exterior').hide();
  }

  $(document).on('hide.bs.modal','#modal-exterior', function () {
    $("#div-aspek-exterior").css("display","none") ;
    $('#div-list-exterior').css("display","none") ;
  });

function exterior(tgl1, tgl2){

    var aircraft_type  = ($('#aircraft_type').val().toString() == "") ? "00" : $('#aircraft_type').val().toString();
    aircraft_type      = aircraft_type.replace(/,/g, '-');
    var aircraft_reg   = ($('#aircraft_reg').val().toString() == "") ? "00" : $('#aircraft_reg').val().toString();
    aircraft_reg       = aircraft_reg.replace(/,/g, '-');
    $.ajax({
      url: base_url + 'Cabin_Performance/data_exterior/' + tgl1 + '/' + tgl2,
      type: "POST",
      dataType: "JSON",
      data:{
        aircraft_type: aircraft_type,
        aircraft_reg: aircraft_reg
      },
      error: function(){
       notif('error', 'Connection disorders occurred, Please try again');
      },
      success: function (data) {
        notif('success', 'Data exterior successfully displayed');
        var chart3 =  Highcharts.chart('grafik3', {
          chart: {
              type: 'column',
              width: '350'
          },
          title: {
              text: 'Exterior Appearance',
              margin: 0
          },
          credits:{
              enabled: false
          },
          exporting: {
            enabled: false
          },
          xAxis: {
            categories: data.categories,
            crosshair: true
          },
          yAxis: {
              min: 0,
              tickPositioner: function() {
              return yAxisLimit;
              },
              title: {
                  text: 'Percent (%)'
              }
          },
          tooltip: {
              headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:#000;padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y} %</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                          modal_exterior(chart3.options.title.text, aircraft_type, this.category, aircraft_reg, tgl1, tgl2);
                        },
                    }
                },
                pointPadding: 0,
                groupPadding: 0.2,
                borderRadiusTopLeft: '10px',
                borderRadiusTopRight: '10px'
              }
          },
         series: [{
              name: 'Target',
              data: data.data_target,
              color: '#FD9214',
              dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#000',
                align: 'right',
                format: '{point.y:.2f}', // one decimal
                y: -32, // 10 pixels down from the top
              }
          }, {
              name: 'Performance',
              data: data.data_performance,
              color: '#8ec3a7',
              dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#000',
                align: 'right',
                format: '{point.y:.2f}', // one decimal
                y: -32, // 10 pixels down from the top
              }
          }]
        });
      }
    });
  }


  function reg_exterior(aircraft, id_reg, tgl1, tgl2){
    $("#table-reg-exterior").DataTable({
      "destroy": true,
      "ordering": false,
      "searching": true,
      "paging": true,
      lengthChange: true,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'nm_reg'}
      ],
        ajax: {
          type: 'GET',
          url: base_url + 'Cabin_Performance/reg_exterior/' + aircraft + '/' + id_reg,
          dataType: 'JSON',
          dataSrc : function (json) {
            var return_data = new Array();
            var no=1;
              for(var i=0;i<json.length; i++){
                return_data.push({
                  'no'      : '<center>'+no+'</center>',
                  'nm_reg'  : '<a style="cursor:pointer;color: #333;" onclick="modal_aspek_exterior_show(\'' + json[i].id + '\',\'' + tgl1 +  '\',\'' + tgl2 +  '\',\'' + json[i].name_ac_reg +  '\')"><center>'+json[i].name_ac_reg+'</center></a>',
              });
              no++;
            }
          return return_data;
        },
      },
    });
  }


  function aspek_exterior(id_reg, tgl1, tgl2){
    $.ajax({
      url: base_url + 'Cabin_Performance/aspek_exterior/' + id_reg + '/' + tgl1 + '/' + tgl2,
      type: "GET",
      dataType: "JSON",
      success: function (data) {
        $('#div-aspek-exterior').html('');
         var list = '<button class="btn btn-sm btn-flat pull-right" onclick="back_to_reg_exterior();" type="submit" style="background-color: #0f2233;color: #fff;"><i class="fa  fa-arrow-circle-o-left"></i>&nbsp;Back</button><br><br>';
        for (var i = 0; i < data.length; i++) {

          if(data[i]['data_performance'] == 0){
            var perform = 100;
          }else{
            var perform = data[i]['data_performance'];
          }

         if(perform >= data[i]['data_target']){
            var fa    = 'fa fa-caret-up fa-style';
            var span  = 'description-percentage text-green';
            var note  = 0;
          }else{
            var fa    = 'fa fa-caret-down fa-style';
            var span  = 'description-percentage text-red';
            var note  = 1;
          }
                list +='<a href="javascript:void(0)" title="Detail Performance '+data[i]['categories']+'" onclick="detail_faktor_pendukung_exterior(\'' +id_reg +  '\',\'' + data[i].id_item +  '\', \'' + tgl1 +  '\',\'' + tgl2 +  '\',\'' + data[i]['categories'] +  '\',\'' + note +  '\');">'+
                        '<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 with-border" style="border-right: 2px solid #121312;">'+
                          '<h2 class="font-panel1">'+perform+'%</h2>'+
                          '<h3 class="font-panel2">'+data[i]['data_target']+'%</h3>'+
                          '<h3 class="font-panel3" id="detail-aspect">'+data[i]['categories']+'</h3>'+
                          '<span class="'+span+'"><i class="'+fa+'"></i></span>'+
                        '</div>'+
                      '</a>';
          }
                list +='<br><br>'+
                          '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'+
                            '<table style="position: fixed;bottom: 15px;right: 5px;width: 200px;border: 2px solid #0f2233;">'+
                                '<tr>'+
                                  '<td>&nbsp;</td>'+
                                  '<td>&nbsp;</td>'+
                                  '<td><div class="blt-merah"></div></td>'+
                                  '<td>&nbsp;</td>'+
                                  '<td><h5 style="font-weight: 500;">Target</h5></td>'+
                                  '<td>&nbsp;&nbsp;&nbsp;</td>'+
                                  '<td><div class="blt-navy"></div></td>'+
                                  '<td>&nbsp;</td>'+
                                  '<td><h5 style="font-weight: 500;">Performance</h5></td>'+
                                '</tr>'+
                            '</table>'+
                          '</div>';
            $('#div-aspek-exterior').append(list);
        }
    });
  }

  function detail_exterior(id_reg, id_item, tgl1, tgl2, note){
    if(note == 0){
    }else{
      $.ajax({
        url: base_url + 'Cabin_Performance/mitigasi_exterior/' + id_reg + '/' + id_item  + '/' + tgl1 + '/' + tgl2,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
          for (var i = 0; i < data.length; i++) {
            notif('warning', '<h3>MITIGATION !!!</h3><table class="table table-bordered"><tr><td>Why</td><td>'+data[i]['cabin_mitigation_why']+'</td></tr><tr><td>Solution</td><td>'+data[i]['cabin_mitigation_solution']+'</td></tr></table>');
          }
        }
      })
    }
    $("#table-aspek-exterior").DataTable({
      "destroy": true,
      "ordering": false,
      "searching": true,
      "paging": true,
      lengthChange: true,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'nama'},
        {data : 'value'},
        {data : 'perform'}
      ],
        ajax: {
          type: 'GET',
          url: base_url + 'Cabin_Performance/detail_sub_exterior/' + id_reg + '/' + id_item  + '/' + tgl1 + '/' + tgl2,
          dataType: 'JSON',
          dataSrc : function (json) {
            var return_data = new Array();
            var no=1;
              for(var i=0;i<json.length; i++){
                if(json[i].value=='4'){
                  var value_perform = '100'
                }else if(json[i].value=='3'){
                  var value_perform = '98'
                }else if(json[i].value=='2'){
                  var value_perform = '85'
                }else if(json[i].value=='1'){
                  var value_perform = '75'
                }
                return_data.push({
                  'no'        : '<center>'+no+'</center>',
                  'nama'      : '<center>'+json[i].nm_sub_item+'</center>',
                  'value'     : '<center>'+json[i].value+'</center>',
                  'perform'     : '<center>'+value_perform+'%</center>',
             });
              no++;
            }
          return return_data;
        },
      },
    });
  }


//============================FUNCTIONALITY============================\\
  var faktor_func_global;
  var nm_aircraft_func_global;
  var nm_reg_func_global;

  function modal_functional_by_reg(faktor, id_aircraft, nm_aircraft, id_reg, tgl1, tgl2){
    $('#modal-functionality-reg').modal('show');
    faktor_func_global = faktor;
    nm_aircraft_func_global = nm_aircraft;
    aspek_functional_reg(nm_aircraft, tgl1, tgl2);
    $('#title-functionality-reg').html(faktor + ' ' + nm_aircraft);
  }

  function modal_functional_by_item(faktor, item, tgl1, tgl2){
    $('#modal-functionality-item').modal('show');
    $('#title-functionality-item').html(faktor + ' | ' + item);
    detail_functional_by_item(item, tgl1, tgl2);
  }

  function modal_aspek_functional_show(id_reg, tgl1, tgl2, nm_reg){
    $('#div-aspek-functionality').show();
    $('#div-reg-functionality').hide();
    nm_reg_func_global = nm_reg;
    aspek_functional(id_reg, tgl1, tgl2);
    $('#title-functionality').html(nm_aircraft_func_global + ' ' + nm_reg);
    $('#div-list-functionality-by-item').css('display', 'none');
  }

  function detail_faktor_pendukung_functional(id_type, id_item, tgl1, tgl2, nm_item, note){
    $('#div-aspek-functionality-reg').hide();
    $('#div-list-functionality-reg').show();
    detail_functional_by_reg(id_type, id_item, tgl1, tgl2, note);
    $('#title-functionality-reg').html(nm_item);
  }

  function back_to_table_functional_reg(){
    $('#div-aspek-functionality-reg').show();
    $('#title-functionality-reg').html('Functionality | ' + nm_aircraft_func_global);
    $('#div-list-functionality-reg').hide();
  }

  function functional_by_reg(tgl1, tgl2){
    var aircraft_type  = ($('#aircraft_type').val().toString() == "") ? "00" : $('#aircraft_type').val().toString();
    aircraft_type      = aircraft_type.replace(/,/g, '-');
    var aircraft_reg   = ($('#aircraft_reg').val().toString() == "") ? "00" : $('#aircraft_reg').val().toString();
    aircraft_reg       = aircraft_reg.replace(/,/g, '-');
    $.ajax({
      url: base_url + 'Cabin_Performance/data_functional_by_reg/' + tgl1 + '/' + tgl2,
      type: "POST",
      dataType: "JSON",
      data:{
        aircraft_type: aircraft_type,
        aircraft_reg: aircraft_reg
      },
      error: function(){
       notif('error', 'Connection disorders occurred, Please try again');
      },
      success: function (data) {
        notif('success', 'Data functional by type successfully displayed');
        var chart4 =  Highcharts.chart('grafik4', {
          chart: {
              type: 'column',
              width: '350'
          },
          title: {
              text: 'Functionality',
              margin: 0
          },
          credits:{
              enabled: false
          },
          exporting: {
            enabled: false
          },
          xAxis: {
            categories: data.categories,
            crosshair: true,
            labels: {
              style: {
                textOverflow: 'ellipsis',
                width: '20em',
                whiteSpace: 'nowrap'
              }
            }
          },
          yAxis: {
              min: 0,
              tickPositioner: function() {
              return yAxisLimit;
              },
              title: {
                  text: 'Percent (%)'
              },
              maxPadding: 0.2
          },
          tooltip: {
              headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:#000;padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y} %</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                          modal_functional_by_reg(chart4.options.title.text, aircraft_type, this.category, aircraft_reg, tgl1, tgl2);
                        },
                    }
                },
                pointPadding: 0,
                groupPadding: 0.2,
                borderRadiusTopLeft: '10px',
                borderRadiusTopRight: '10px'
              }
          },
        series: [{
              name: 'Target',
              data: data.data_target,
              color: '#FD9214',
              dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#000',
                align: 'right',
                format: '{point.y:.2f}', // one decimal
                y: -32, // 10 pixels down from the top
              }
          }, {
              name: 'Performance',
              data: data.data_performance,
              color: '#8ec3a7',
              dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#000',
                align: 'right',
                format: '{point.y:.2f}', // one decimal
                y: -32, // 10 pixels down from the top
              }
          }]
        });
      }
    });
  }

  function functional_by_item(tgl1, tgl2){
    $.ajax({
      url: base_url + 'Cabin_Performance/data_functional_by_item/' + tgl1 + '/' + tgl2,
      type: "GET",
      dataType: "JSON",
      error: function(){
       notif('error', 'Connection disorders occurred, Please try again');
      },
      success: function (data) {
        notif('success', 'Data functional by item successfully displayed');
        var chart4 =  Highcharts.chart('grafik5', {
          chart: {
              type: 'column',
              width: '350'
          },
          title: {
              text: 'Functionality',
              margin: 0
          },
          credits:{
              enabled: false
          },
          exporting: {
            enabled: false
          },
          xAxis: {
            categories: data.categories,
            crosshair: true,
            labels: {
              style: {
                textOverflow: 'ellipsis',
                width: '20em',
                whiteSpace: 'nowrap'
              }
            }
          },
          yAxis: {
              min: 0,
              tickPositioner: function() {
              return yAxisLimit;
              },
              title: {
                  text: 'Percent (%)'
              },
              maxPadding: 0.2
          },
          tooltip: {
              headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:#000;padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y} %</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                          modal_functional_by_item(chart4.options.title.text, this.category, tgl1, tgl2);
                        },
                    }
                },
                pointPadding: 0,
                groupPadding: 0.2,
                borderRadiusTopLeft: '5px',
                borderRadiusTopRight: '5px'
              }
          },
        series: [{
              name: 'Target',
              data: data.data_target,
              color: '#FD9214',
              dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#000',
                align: 'right',
                format: '{point.y:.2f}', // one decimal
                y: -35, // 10 pixels down from the top
              }
          }, {
              name: 'Performance',
              data: data.data_performance,
              color: '#8ec3a7',
              dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#000',
                align: 'right',
                format: '{point.y:.2f}', // one decimal
                y: -35, // 10 pixels down from the top
              }
          }]
        });
      }
    });
  }

  /*function reg_functional(aircraft, id_reg, tgl1, tgl2){
    $("#table-reg-functionality").DataTable({
      "destroy": true,
      "ordering": false,
      "searching": true,
      "paging": true,
      lengthChange: true,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'nm_reg'}
      ],
        ajax: {
          type: 'GET',
          url: base_url + 'Cabin_Performance/reg_functional/' + aircraft + '/' + id_reg,
          dataType: 'JSON',
          dataSrc : function (json) {
            var return_data = new Array();
            var no=1;
              for(var i=0;i<json.length; i++){
                return_data.push({
                  'no'      : '<center>'+no+'</center>',
                  'nm_reg'  : '<a style="cursor:pointer;color: #333;" onclick="modal_aspek_functional_show(\'' + json[i].id + '\',\'' + tgl1 +  '\',\'' + tgl2 +  '\',\'' + json[i].name_ac_reg +  '\')"><center>'+json[i].name_ac_reg+'</center></a>',
              });
              no++;
            }
          return return_data;
        },
      },
    });
  }*/


  function aspek_functional_reg(id_type, tgl1, tgl2){
    $.ajax({
      url: base_url + 'Cabin_Performance/aspek_functional/' + id_type + '/' + tgl1 + '/' + tgl2,
      type: "GET",
      dataType: "JSON",
      success: function (data) {
        $('#div-aspek-functionality-reg').html('');
         var list = '';
        for (var i = 0; i < data.length; i++) {
          if(data[i]['data_performance'] == 0){
            var perform = 100;
          }else{
            var perform = data[i]['data_performance'];
          }
          if(perform >= data[i]['data_target']){
            var fa    = 'fa fa-caret-up fa-style';
            var span  = 'description-percentage text-green';
            var note  = 0;
          }else{
            var fa    = 'fa fa-caret-down fa-style';
            var span  = 'description-percentage text-red';
            var note  = 1;
          }


                list +='<a href="javascript:void(0)" data-toggle="tooltip" title="Detail Performance '+data[i]['categories']+'" onclick="detail_faktor_pendukung_functional(\'' +id_type +  '\',\'' + data[i].id_item +  '\', \'' + tgl1 +  '\',\'' + tgl2 +  '\',\'' + data[i]['categories'] +  '\',\'' + note +  '\');">'+
                        '<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 with-border" style="border-right: 2px solid #121312;">'+
                          '<h2 class="font-panel1">'+perform+'%</h2>'+
                          '<h3 class="font-panel2">'+data[i]['data_target']+'%</h3>'+
                          '<h3 class="font-panel3" id="detail-aspect">'+data[i]['categories']+'</h3>'+
                          '<span class="'+span+'"><i class="'+fa+'"></i></span>'+
                        '</div>'+
                      '</a>';
          }
                list +='<br><br>'+
                          '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'+
                            '<table style="position: fixed;bottom: 15px;right: 5px;width: 200px;border: 2px solid #0f2233;">'+
                                '<tr>'+
                                  '<td>&nbsp;</td>'+
                                  '<td>&nbsp;</td>'+
                                  '<td><div class="blt-merah"></div></td>'+
                                  '<td>&nbsp;</td>'+
                                  '<td><h5 style="font-weight: 500;">Target</h5></td>'+
                                  '<td>&nbsp;&nbsp;&nbsp;</td>'+
                                  '<td><div class="blt-navy"></div></td>'+
                                  '<td>&nbsp;</td>'+
                                  '<td><h5 style="font-weight: 500;">Performance</h5></td>'+
                                '</tr>'+
                            '</table>'+
                          '</div>';
            $('#div-aspek-functionality-reg').append(list);
        }
    });
  }

  function detail_functional_by_reg(id_type, id_item, tgl1, tgl2, note){
    /*if(note == 0){
    }else{
      $.ajax({
        url: base_url + 'Cabin_Performance/mitigasi_functional_by_reg/' + id_type + '/' + id_item  + '/' + tgl1 + '/' + tgl2,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
          for (var i = 0; i < data.length; i++) {
            notif('warning', '<h3>MITIGATION !!!</h3><table class="table table-bordered"><tr><td>Why</td><td>'+data[i]['cabin_mitigation_why']+'</td></tr><tr><td>Solution</td><td>'+data[i]['cabin_mitigation_solution']+'</td></tr></table>');
          }
        }
      });
    }*/
    $("#table-aspek-functionality-reg").DataTable({
      "destroy": true,
      "ordering": false,
      "searching": true,
      "paging": true,
      lengthChange: true,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'nama'},
        {data : 'remark'},
        {data : 'item'},
        {data : 'defects'},
        {data : 'perform'},
      ],
        ajax: {
          type: 'GET',
          url: base_url + 'Cabin_Performance/detail_sub_functional_reg/' + id_type + '/' + id_item  + '/' + tgl1 + '/' + tgl2,
          dataType: 'JSON',
          dataSrc : function (json) {
            var return_data = new Array();
            var no=1;
              for(var i=0;i<json.length; i++){
                var perform = ((parseInt(json[i].total_item_sub)-parseInt(json[i].total_defects_item_sub))/parseInt(json[i].total_item_sub))*100;
                var perform = perform.toFixed(2);
                if(perform == 100.00){
                  var fix_perform = 100;
                }else{
                  var fix_perform = ((parseInt(json[i].total_item_sub)-parseInt(json[i].total_defects_item_sub))/parseInt(json[i].total_item_sub))*100;
                  fix_perform     = fix_perform.toFixed(2);
                }

                if (json[i].remark_item_sub == null){
                  var remark = '-';
                }else if(json[i].remark_item_sub == ''){
                  var remark = '-'
                }else{
                  var remark = json[i].remark_item_sub;
                }
                return_data.push({
                  'no'      : '<center>'+no+'</center>',
                  'nama'    : '<center>'+json[i].nama_item_sub+'</center>',
                  'remark'  : '<center>'+remark+'</center>',
                  'item'    : '<center>'+json[i].total_item_sub+'</center>',
                  'defects' : '<center>'+json[i].total_defects_item_sub+'</center>',
                  'perform' : '<center>'+fix_perform+'%</center>',
              });
              no++;
            }
          return return_data;
        },
      },
    });
  }

  function detail_functional_by_item(item, tgl1, tgl2, note){
    if(note == 0){
    }else{
      $.ajax({
        url: base_url + 'Cabin_Performance/mitigasi_functional_by_item/' + item  + '/' + tgl1 + '/' + tgl2,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
          for (var i = 0; i < data.length; i++) {
            notif('warning', '<h3>MITIGATION !!!</h3><table class="table table-bordered"><tr><td>Why</td><td>'+data[i]['cabin_mitigation_why']+'</td></tr><tr><td>Solution</td><td>'+data[i]['cabin_mitigation_solution']+'</td></tr></table>');
          }
        }
      })
    }
    $("#table-aspek-functionality-item").DataTable({
      "destroy": true,
      "ordering": false,
      "searching": true,
      "paging": true,
      lengthChange: true,
      info: true,
      autoWidth: true,
      columns : [
        {data : 'no'},
        {data : 'nama'},
        {data : 'remark'},
        {data : 'item'},
        {data : 'defects'},
        {data : 'perform'},
      ],
        ajax: {
          type: 'GET',
          url: base_url + 'Cabin_Performance/detail_sub_functional_by_item/' + item  + '/' + tgl1 + '/' + tgl2,
          dataType: 'JSON',
          dataSrc : function (json) {
            var return_data = new Array();
            var no=1;
              for(var i=0;i<json.length; i++){
                var perform = ((parseInt(json[i].total_item_sub)-parseInt(json[i].total_defects_item_sub))/parseInt(json[i].total_item_sub))*100;
                var perform = perform.toFixed(2);
                if(perform == 100.00){
                  var fix_perform = 100;
                }else{
                  var fix_perform = ((parseInt(json[i].total_item_sub)-parseInt(json[i].total_defects_item_sub))/parseInt(json[i].total_item_sub))*100;
                  fix_perform     = fix_perform.toFixed(2);
                }

                if (json[i].remark_item_sub == null){
                  var remark = '-';
                }else if(json[i].remark_item_sub == ''){
                  var remark = '-'
                }else{
                  var remark = json[i].remark_item_sub;
                }
                return_data.push({
                  'no'      : '<center>'+no+'</center>',
                  'nama'    : '<center>'+json[i].nama_item_sub+'</center>',
                  'remark'  : '<center>'+remark+'</center>',
                  'item'    : '<center>'+json[i].total_item_sub+'</center>',
                  'defects' : '<center>'+json[i].total_defects_item_sub+'</center>',
                  'perform' : '<center>'+fix_perform+'%</center>',
              });
              no++;
            }
          return return_data;
        },
      },
    });
  }

</script>

<!--==============================================MODAL INTERIOR==============================================-->
<div class="modal bounceIn" id="modal-interior" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0f2233;color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title-panel" id="title-interior" style="text-align: center;font-weight: bold"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <!--START TABEL REG-->
          <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div-reg-interior">
            <div class="col-md-12">
              <table class="table table-hover table-bordered table-gmf" id="table-reg-interior">
                <thead style="background-color: #05354D;">
                  <tr>
                    <th style="text-align:center">No</th>
                    <th style="text-align:center">Aircraft Reg</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
          <!--END TABEL REG-->
          <!--START TABEL ASPEK-->
          <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div-aspek-interior" style="display: none">
          </div>
          <!--END TABEL ASPEK-->
          <!--START DETAIL ASPEK-->
          <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div-list-interior" style="display: none">
             <button class="btn btn-sm btn-flat pull-right" onclick="back_to_table_interior();" type="submit" style="background-color: #0f2233;color: #fff;">
              <i class="fa  fa-arrow-circle-o-left"></i>&nbsp;Back
            </button>
            <br><br>
            <div class="col-md-12">
              <div class="table-responsive" style="overflow-x:hidden">
                <table style="width:100%;" class="table table-hover table-bordered table-gmf" id="table-aspek-interior">
                    <thead style="background-color: #05354D;">
                      <tr>
                        <th class="center">No</th>
                        <th class="center">Items</th>
                        <th class="center">Total Dirty</th>
                        <th class="center">Total Defect</th>
                        <th class="center">Total Item Sub</th>
                        <th class="center">Remark</th>
                        <th class="center">Performance</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
            </div>
          </div>
          <!--END DETAIL ASPEK-->
        </div>
      </div>
    </div>
  </div>
</div>


<!--==============================================MODAL EXTERIOR==============================================-->
<div class="modal bounceIn" id="modal-exterior" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0f2233;color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title-panel" id="title-exterior" style="text-align: center;font-weight: bold"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <!--START TABEL REG-->
          <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div-reg-exterior">
            <div class="col-md-12">
              <table class="table table-hover table-bordered table-gmf" id="table-reg-exterior">
                <thead style="background-color: #05354D;">
                  <tr>
                    <th style="text-align:center">No</th>
                    <th style="text-align:center">Aircraft Reg</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
          <!--END TABEL REG-->
          <!--START TABEL ASPEK-->
          <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div-aspek-exterior" style="display: none">
          </div>
          <!--END TABEL ASPEK-->
          <!--START DETAIL ASPEK-->
          <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div-list-exterior" style="display: none">
             <button class="btn btn-sm btn-flat pull-right" onclick="back_to_table_exterior();" type="submit" style="background-color: #0f2233;color: #fff;">
              <i class="fa  fa-arrow-circle-o-left"></i>&nbsp;Back
            </button>
            <br><br>
            <div class="col-md-12">
              <div class="table-responsive" style="overflow-x:hidden">
                <table style="width:100%;" class="table table-hover table-bordered table-gmf" id="table-aspek-exterior">
                    <thead style="background-color: #05354D;">
                      <tr>
                        <th class="center">No</th>
                        <th class="center">Items</th>
                        <th class="center">Value</th>
                        <th class="center">Performance</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
            </div>
          </div>
          <!--END DETAIL ASPEK-->
        </div>
      </div>
    </div>
  </div>
</div>

<!--==============================================MODAL FUNCTIONALITY==============================================-->
<div class="modal bounceIn" id="modal-functionality-reg" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0f2233;color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title-panel" id="title-functionality-reg" style="text-align: center;font-weight: bold"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <!--START TABEL ASPEK-->
          <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div-aspek-functionality-reg">
          </div>
          <!--END TABEL ASPEK-->
          <!--START DETAIL ASPEK-->
          <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div-list-functionality-reg" style="display: none">
            <div class="col-md-12">
             <button class="btn btn-sm btn-flat pull-right" onclick="back_to_table_functional_reg();" type="submit" style="background-color: #0f2233;color: #fff;">
              <i class="fa  fa-arrow-circle-o-left"></i>&nbsp;Back
            </button>
            </div>
            <br><br>
            <div class="col-md-12">
              <div id="mitigasi"></div>
              <div class="table-responsive" style="overflow-x:hidden">
                <table style="width:100%;" class="table table-hover table-bordered table-gmf" id="table-aspek-functionality-reg">
                    <thead style="background-color: #05354D;">
                      <tr>
                        <th class="center">No</th>
                        <th class="center" style="width: 150px;">Items</th>
                        <th class="center">Remark</th>
                        <th class="center">Total Items</th>
                        <th class="center">Total Defects</th>
                        <th class="center">Performance</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
            </div>
          </div>
          <!--END DETAIL ASPEK-->
        </div>
      </div>
    </div>
  </div>
</div>


<!--==============================================MODAL FUNCTIONALITY BY ITEM==============================================-->
<div class="modal bounceIn" id="modal-functionality-item" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0f2233;color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title-panel" id="title-functionality-item" style="text-align: center;font-weight: bold"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <!--START DETAIL ASPEK-->
          <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="div-list-functionality-item">
            <div class="col-md-12">
              <div id="mitigasi"></div>
              <div class="table-responsive" style="overflow-x:hidden">
                <table style="width:100%;" class="table table-hover table-bordered table-gmf" id="table-aspek-functionality-item">
                    <thead style="background-color: #05354D;">
                      <tr>
                        <th class="center">No</th>
                        <th class="center" style="width: 150px;">Items</th>
                        <th class="center">Remark</th>
                        <th class="center">Total Items</th>
                        <th class="center">Total Defects</th>
                        <th class="center">Performance</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
            </div>
          </div>
          <!--END DETAIL ASPEK-->
        </div>
      </div>
    </div>
  </div>
</div>

<!--
END DIMAS ISLAMI
-->
