<!--
START DIMAS ISLAMI
-->
<style>
  .box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
    display: inline-block;
    font-size: 20px;
    font-weight: bold;
    margin: 0;
    line-height: 1;
  }
  .wadah {
      padding-left: 5px;
      padding-right: 5px;
  }
  .highlight {
      background-color: #FFF;
      color: #F24738;
  }
  .normal {
      background-color: #ffffff;
  }
  .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
      z-index: 3;
      color: #fff;
      cursor: default;
      background-color: #0f2233;
      border-color: #0e1f2e;
  }
</style>

<section class="content-header">
    <h1>
        <?= $title ?>
        <small><?= $small_tittle ?></small>
    </h1>
<!--     <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <?php foreach ($breadcrumb as $data): ?>
            <li><?= $data ?></li>
        <?php endforeach; ?>
    </ol> -->
</section>

<section class="content">
    <div class="box">
<!--         <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div> -->
        <div class="box-body">
            <div class="row">
                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Start Period</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="year1">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>End Period</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="year2">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label></label>
                        <button class="btn btn-block btn-flat bg-navy" id="pencarian" style="margin-top: 4px;">
                            <i class="fa fa-search"></i> Search
                        </button>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12"></div>
                <div class="col-lg-3 col-md-1 col-sm-12 col-xs-12">
                    <div class="small-box bg-green" style="background-color: #808080 !important;">
                        <div class="inner" style="padding: 5px;">
                            <h3 id="target_value"></h3>
                            <p>Target Of Year</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-arrow-circle-up"></i>
                        </div>
                        <a href="javascript:void(0)" onclick="modal_target()" class="small-box-footer">
                            Add Target
                        </a>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -20px;">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> Master Mitigation</h4>
                    <br>
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab">Mitigation Interior</a></li>
                            <li><a href="#tab_2" data-toggle="tab">Mitigation Exterior</a></li>
                            <li><a href="#tab_3" data-toggle="tab">Mitigation Functionality</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <table class="table table-hover table-gmf" id="mitigation-interior" style="width: 100%">
                                    <thead style="background-color: #05354D;">
                                    <tr>
                                        <th style="text-align: center">No</th>
                                        <th style="text-align: center;display: none">id_type</th>
                                        <th style="text-align: center;display: none">id_reg</th>
                                        <th style="text-align: center;display: none">id_item</th>
                                        <th style="text-align: center">Manufacture</th>
                                        <th style="text-align: center">Aircraft Type</th>
                                        <th style="text-align: center">Aircraft Reg</th>
                                        <th style="text-align: center">Item</th>
                                        <th style="text-align: center">Target</th>
                                        <th style="text-align: center">Performance</th>
                                        <th style="text-align: center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <table class="table table-hover table-gmf" id="mitigation-exterior" style="width: 100%">
                                    <thead style="background-color: #05354D;">
                                    <tr>
                                        <th style="text-align: center">No</th>
                                        <th style="text-align: center;display: none">id_type</th>
                                        <th style="text-align: center;display: none">id_reg</th>
                                        <th style="text-align: center;display: none">id_item</th>
                                        <th style="text-align: center">Manufacture</th>
                                        <th style="text-align: center">Aircraft Type</th>
                                        <th style="text-align: center">Aircraft Reg</th>
                                        <th style="text-align: center">Item</th>
                                        <th style="text-align: center">Target</th>
                                        <th style="text-align: center">Performance</th>
                                        <th style="text-align: center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_3">
                                <table class="table table-hover table-gmf" id="mitigation-functionality"
                                       style="width: 100%">
                                    <thead style="background-color: #05354D;">
                                    <tr>
                                        <th style="text-align: center">No</th>
                                        <th style="text-align: center;display: none">id_item</th>
                                        <th style="text-align: center;display: none">date</th>
<!--                                         <th style="text-align: center">Manufacture</th>
                                        <th style="text-align: center">Aircraft Type</th>
                                        <th style="text-align: center">Aircraft Reg</th> -->
                                        <th style="text-align: left">Item</th>
                                        <th style="text-align: center">Target</th>
                                        <th style="text-align: center">Performance</th>
                                        <th style="text-align: center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>index.php/';

    $(document).ready(function () {
        $('#year1').datepicker({
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months",
            autoclose: true,
        });
        $('#year2').datepicker({
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months",
            autoclose: true,
        });

        var tgl = new Date();
        var bulan1 = (tgl.getMonth() - 1, 1);
        var tahun1 = tgl.getFullYear();
        var bulan2 = tgl.getMonth() + 1;
        var tahun2 = tgl.getFullYear();
        var tahun = tgl.getFullYear();
        newDate1 = bulan1 + '-' + tahun1;
        newDate2 = bulan2 + '-' + tahun2;

        $('#year1').val(newDate1);
        $('#year2').val(newDate2);

        table_mitigation_interior(newDate1, newDate2);
        table_mitigation_exterior(newDate1, newDate2);
        table_mitigation_functionality(newDate1, newDate2);
        load_target(tahun);
        search();
    });

    function search() {
        $('#pencarian').click(function (e) {
            var tgl1 = $('#year1').val();
            var tgl2 = $('#year2').val();
            var get_year1 = tgl1.split('-')[1];
            var get_year2 = tgl2.split('-')[1];
            var get_month1 = tgl1.split('-')[0];
            var get_month2 = tgl2.split('-')[0];

            if (get_year1 == get_year2 && get_month1 == get_month2) {
                table_mitigation_interior(tgl1, tgl2);
                table_mitigation_exterior(tgl1, tgl2);
                table_mitigation_functionality(tgl1, tgl2);
            } else if (get_year1 == get_year2 && get_month1 < get_month2) {
                table_mitigation_interior(tgl1, tgl2);
                table_mitigation_exterior(tgl1, tgl2);
                table_mitigation_functionality(tgl1, tgl2);
            }
             else if (get_year1 == get_year2 && get_month1 > get_month2) {
                table_mitigation_interior(tgl1, tgl2);
                table_mitigation_exterior(tgl1, tgl2);
                table_mitigation_functionality(tgl1, tgl2);
            } else {
                notif('error', "Sorry can't, check your date");
            }
        });
    }

    function modal_target() {
        $('#modal-target').modal('show');
    }

    function modal_interior(id_reg, id_item, nm_item, date, id_type) {
        $('#modal-interior').modal('show');
        $('#tittle-mitigasi-interior').html(nm_item);
        $.ajax({
            url: base_url + 'Cabin_Performance/show_mitigation_in',
            type: "GET",
            data: {
              date: date,
              id_reg: id_reg,
              id_type: id_type,
              id_item: id_item
            },
            dataType: "JSON",
            success: function (data) {
                if (data.length == 0) {
                    $('[name="cabin_mitigation_id"]').val('');
                    $('[name="cabin_mitigation_date"]').val(date);
                    $('[name="actype"]').val(id_type);
                    $('[name="acreg"]').val(id_reg);
                    $('[name="type_mitigasi"]').val('Interior');
                    $('[name="item"]').val(id_item);
                    $('[name="nm_item"]').val(nm_item);
                    $('[name="cabin_mitigation_why"]').val('');
                    $('[name="cabin_mitigation_solution"]').val('');
                } else {
                    $('[name="cabin_mitigation_id"]').val(data[0]['cabin_mitigation_id']);
                    $('[name="cabin_mitigation_date"]').val(data[0]['cabin_mitigation_date']);
                    $('[name="actype"]').val(data[0]['actype']);
                    $('[name="acreg"]').val(data[0]['acreg']);
                    $('[name="type_mitigasi"]').val('Interior');
                    $('[name="item"]').val(data[0]['item']);
                     $('[name="nm_item"]').val(data[0]['nm_item']);
                    $('[name="cabin_mitigation_why"]').val(data[0]['cabin_mitigation_why']);
                    $('[name="cabin_mitigation_solution"]').val(data[0]['cabin_mitigation_solution']);
                }
            }
        });
    }

    function modal_exterior(id_reg, id_item, nm_item, date,id_type) {
        $('#modal-exterior').modal('show');
        $('#tittle-mitigasi-exterior').html(nm_item);
        $.ajax({
            url: base_url + 'Cabin_Performance/show_mitigation_in/' + date +  '/' + id_reg +  '/' + id_type + '/' + id_item,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                if (data.length == 0) {
                    $('[name="cabin_mitigation_id"]').val('');
                    $('[name="cabin_mitigation_date"]').val(date);
                    $('[name="actype"]').val(id_type);
                    $('[name="acreg"]').val(id_reg);
                    $('[name="type_mitigasi"]').val('Exterior');
                    $('[name="item"]').val(id_item);
                    $('[name="nm_item"]').val(nm_item);
                    $('[name="cabin_mitigation_why"]').val('');
                    $('[name="cabin_mitigation_solution"]').val('');
                } else {
                    $('[name="cabin_mitigation_id"]').val(data[0]['cabin_mitigation_id']);
                    $('[name="cabin_mitigation_date"]').val(data[0]['cabin_mitigation_date']);
                    $('[name="actype"]').val(data[0]['actype']);
                    $('[name="acreg"]').val(data[0]['acreg']);
                    $('[name="item"]').val(data[0]['item']);
                     $('[name="nm_item"]').val(data[0]['nm_item']);
                    $('[name="cabin_mitigation_why"]').val(data[0]['cabin_mitigation_why']);
                    $('[name="cabin_mitigation_solution"]').val(data[0]['cabin_mitigation_solution']);
                }
            }
        });
    }

    function modal_functionality(id_item, nm_item, date) {
        $('#modal-functionality').modal('show');
        $('#tittle-mitigasi-functionality').html(nm_item);
        $.ajax({
            url: base_url + 'Cabin_Performance/show_mitigation_funct/' + date + '/' + id_item,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                if (data.length == 0) {
                    $('[name="cabin_mitigation_id"]').val('');
                    $('[name="cabin_mitigation_date"]').val(date);
                    $('[name="actype"]').val(0);
                    $('[name="type_mitigasi"]').val('Functionality');
                    $('[name="item"]').val(id_item);
                    $('[name="nm_item"]').val(nm_item);
                    $('[name="cabin_mitigation_why"]').val('');
                    $('[name="cabin_mitigation_solution"]').val('');
                } else {
                    $('[name="cabin_mitigation_id"]').val(data[0]['cabin_mitigation_id']);
                    $('[name="cabin_mitigation_date"]').val(data[0]['cabin_mitigation_date']);
                    $('[name="actype"]').val(data[0]['actype']);
                    $('[name="type_mitigasi"]').val('Functionality');
                    $('[name="acreg"]').val(data[0]['acreg']);
                    $('[name="item"]').val(data[0]['item']);
                     $('[name="nm_item"]').val(data[0]['nm_item']);
                    $('[name="cabin_mitigation_why"]').val(data[0]['cabin_mitigation_why']);
                    $('[name="cabin_mitigation_solution"]').val(data[0]['cabin_mitigation_solution']);
                }
            }
        });

    }

    function load_target(thn) {
        $.ajax({
            url: base_url + 'Cabin_Performance/data_target_cabin/' + thn,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $('#target_value').html(data[0]['target_val'] + '<sup style="font-size: 20px">%</sup>');
                $('[name="target_id"]').val(data[0]['target_id']);
                $('[name="target_val"]').val(data[0]['target_val']);
                $('[name="target_type"]').val(data[0]['target_type']);
                $('[name="target_year"]').val(data[0]['target_year']);
            }
        });
    }


    function save_target() {
        var formData = new FormData($('#form-target')[0]);
        $.ajax({
            url: base_url + 'Cabin_Performance/update_target_cabin',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
                if (data.status == false) {
                    notif('error', "Sorry can't, maximum value of one hundred percent");
                }else{
                    notif('success', "data successfully saved ");
                    $('#modal-target').modal('hide');
                    var tgl = new Date();
                    var tahun = tgl.getFullYear();
                    load_target(tahun);
                }
            }
        });
    }


    function table_mitigation_interior(tgl1, tgl2) {
        $("#mitigation-interior").DataTable({
            "destroy": true,
            "ordering": false,
            "searching": true,
            "paging": true,
            lengthChange: true,
            info: true,
            autoWidth: true,
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [2],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [3],
                    "visible": false,
                    "searchable": false
                },
            ],
            columns: [
                {data: 'no'},
                {data: 'id_type'},
                {data: 'id_reg'},
                {data: 'id_item'},
                {data: 'man'},
                {data: 'type'},
                {data: 'reg'},
                {data: 'item'},
                {data: 'target'},
                {data: 'perform'},
                {data: 'opsi'},
            ],

            ajax: {
                type: 'GET',
                url: base_url + 'Cabin_Performance/master_mitigasi_interior/' + tgl1 + '/' + tgl2,
                dataType: 'JSON',
                dataSrc: function (json) {
                    var return_data = [];
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {
                        return_data.push({
                            'no': '<center>' + no + '</center>',
                            'id_type': json[i].id_type,
                            'id_reg': json[i].id_reg,
                            'id_item': json[i].id_item,
                            'man': '<center>' + json[i].name_manf + '</center>',
                            'type': '<center>' + json[i].name_aircraft + '</center>',
                            'reg': '<center>' + json[i].name_ac_reg + '</center>',
                            'item': '<left>' + json[i].nama_item + '</left>',
                            'target': '<center>' + json[i].nilai_target + '%</center>',
                            'perform': '<center>' + json[i].nilai_perform + '%</center>',
                            'opsi': '<center><a href="javascript:void(0)" class="btn btn-flat btn-sm btn-success" onclick="modal_interior(\'' + json[i].id_reg + '\',\'' + json[i].id_item + '\', \'' + json[i].nama_item + '\', \'' + json[i].date + '\', \'' + json[i].id_type + '\')" ><i class="fa fa-edit"></i></a></center>',
                        });
                        no++;
                    }
                    return return_data;
                },
            },
            "createdRow": function (row, datax, index) {
                $.ajax({
                    url: base_url + 'Cabin_Performance/data_mitigation/'  + tgl1 + '/' + tgl2,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function (key, val) {
                            var id_item = val.item;
                            var id_type = val.actype;
                            if (datax.id_type == id_type && datax.id_item == id_item) {
                                $('td', row).addClass('highlight');
                            }
                        });

                    }
                });
            },
        });
    }

    function table_mitigation_exterior(tgl1, tgl2) {
        $("#mitigation-exterior").DataTable({
            "destroy": true,
            "ordering": false,
            "searching": true,
            "paging": true,
            lengthChange: true,
            info: true,
            autoWidth: true,
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [2],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [3],
                    "visible": false,
                    "searchable": false
                },
            ],
            columns: [
                {data: 'no'},
                {data: 'id_type'},
                {data: 'id_reg'},
                {data: 'id_item'},
                {data: 'man'},
                {data: 'type'},
                {data: 'reg'},
                {data: 'item'},
                {data: 'target'},
                {data: 'perform'},
                {data: 'opsi'},
            ],
            ajax: {
                type: 'GET',
                url: base_url + 'Cabin_Performance/master_mitigasi_exterior/' + tgl1 + '/' + tgl2,
                dataType: 'JSON',
                dataSrc: function (json) {
                    var return_data = [];
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {
                         return_data.push({
                            'no': '<center>' + no + '</center>',
                            'id_type': json[i].id_type,
                            'id_reg': json[i].id_reg,
                            'id_item': json[i].id_item,
                            'man': '<center>' + json[i].name_manf + '</center>',
                            'type': '<center>' + json[i].name_aircraft + '</center>',
                            'reg': '<center>' + json[i].name_ac_reg + '</center>',
                            'item': '<left>' + json[i].nama_item + '</left>',
                            'target': '<center>' + json[i].nilai_target + '%</center>',
                            'perform': '<center>' + json[i].nilai_perform + '%</center>',
                            'opsi': '<center><a href="javascript:void(0)" class="btn btn-flat btn-sm btn-success" onclick="modal_exterior(\'' + json[i].id_reg + '\',\'' + json[i].id_item + '\', \'' + json[i].nama_item + '\', \'' + json[i].date + '\', \'' + json[i].id_type + '\')" ><i class="fa fa-edit"></i></a></center>',
                        });
                        no++;
                    }
                    return return_data;
                },
            },
            "createdRow": function (row, datax, index) {
                $.ajax({
                    url: base_url + 'Cabin_Performance/data_mitigation/'  + tgl1 + '/' + tgl2,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        $.each(data, function (key, val) {
                            var id_type = val.actype;
                            var id_item = val.item;
                            if (datax.id_type == id_type && datax.id_item == id_item) {
                                $('td', row).addClass('highlight');
                            }
                        });

                    }
                });
            },
        });
    }

    function table_mitigation_functionality(tgl1, tgl2) {
        $("#mitigation-functionality").DataTable({
            "destroy": true,
            "ordering": false,
            "searching": true,
            "paging": true,
            lengthChange: true,
            info: true,
            autoWidth: true,
            "columnDefs": [
                {
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [2],
                    "visible": false,
                    "searchable": false
                },
            ],
            columns: [
                {data: 'no'},
                // {data: 'id_reg'},
                {data: 'date'},
                {data: 'id_item'},
                // {data: 'man'},
                // {data: 'type'},
                // {data: 'reg'},
                {data: 'item'},
                {data: 'target'},
                {data: 'perform'},
                {data: 'opsi'},
            ],
            ajax: {
                type: 'GET',
                url: base_url + 'Cabin_Performance/master_mitigasi_functionality/' + tgl1 + '/' + tgl2,
                dataType: 'JSON',
                dataSrc: function (json) {
                    var return_data = [];
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {

                        return_data.push({
                            'no': '<center>' + no + '</center>',
                            // 'id_reg': json[i].id_reg,
                            'date': json[i].date,
                            'id_item': json[i].id_item,
                            // 'man': '<center>' + json[i].name_manf + '</center>',
                            // 'type': '<center>' + json[i].name_aircraft + '</center>',
                            // 'reg': '<center>' + json[i].name_ac_reg + '</center>',
                            'item': '<left>' + json[i].nama_item + '</left>',
                            'target': '<center>' + json[i].target + '%</center>',
                            'perform': '<center>' + json[i].perform + '%</center>',
                            'opsi': '<center><a href="javascript:void(0)" class="btn btn-flat btn-sm btn-success" onclick="modal_functionality(\'' + json[i].id_item + '\', \'' + json[i].nama_item + '\', \'' + json[i].date + '\')" ><i class="fa fa-edit"></i></a></center>',
                        });
                        no++;
                    }
                    return return_data;
                },
            },
            "createdRow": function (row, datax, index) {
                $.ajax({
                    url: base_url + 'Cabin_Performance/data_mitigation/'  + tgl1 + '/' + tgl2,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data) {
                        console.log(data)
                        $.each(data, function (key, val) {
                            if (val.cabin_mitigation_why != "" && val.cabin_mitigation_solution != "") {
                                var id_item = val.item;
                                if (datax.id_item == id_item) {
                                    $('td', row).addClass('highlight');
                                }
                            }

                        });

                    }
                });
            },
        });
    }

    function save_mitigation_in() {
        var formData = new FormData($('#form-interior')[0]);
        if ($('[name="cabin_mitigation_id"]').val() == '') {
            $.ajax({
                url: base_url + 'Cabin_Performance/add_mitigation_in',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function (data) {
                    notif('success', 'Data successfully saved');
                    if (data.status) {
                        $('#modal-interior').modal('hide');
                        $('#mitigation-interior').DataTable().ajax.reload();
                    }
                },
                error: function(){
                  notif('error', 'Connection disorders occurred, Please try again');
                },
            });
        } else {
            $.ajax({
                url: base_url + 'Cabin_Performance/update_mitigation_in',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function (data) {
                    notif('success', 'Data successfully saved');
                    if (data.status) {
                        $('#modal-interior').modal('hide');
                        $('#mitigation-interior').DataTable().ajax.reload();
                    }
                },
                error: function(){
                  notif('error', 'Connection disorders occurred, Please try again');
                },
            });
        }
    }

    function save_mitigation_ext() {
        var formData = new FormData($('#form-exterior')[0]);
        if ($('[name="cabin_mitigation_id"]').val() == '') {
            $.ajax({
                url: base_url + 'Cabin_Performance/add_mitigation_ext',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function (data) {
                    notif('success', 'Data successfully saved');
                    if (data.status) {
                        $('#modal-exterior').modal('hide');
                        $('#mitigation-exterior').DataTable().ajax.reload();
                    }
                },
                error: function(){
                  notif('error', 'Connection disorders occurred, Please try again');
                },
            });
        } else {
            $.ajax({
                url: base_url + 'Cabin_Performance/update_mitigation_ext',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function (data) {
                    notif('success', 'Data successfully saved');
                    if (data.status) {
                        $('#modal-exterior').modal('hide');
                        $('#mitigation-exterior').DataTable().ajax.reload();
                    }
                },
                error: function(){
                  notif('error', 'Connection disorders occurred, Please try again');
                },
            });
        }
    }

    function save_mitigation_funct() {
        var formData = new FormData($('#form-functionality')[0]);
        if ($('[name="cabin_mitigation_id"]').val() == '') {
            $.ajax({
                url: base_url + 'Cabin_Performance/add_mitigation_funct',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function (data) {
                    notif('success', 'Data successfully saved');
                    if (data.status) {
                        $('#modal-functionality').modal('hide');
                        $('#mitigation-functionality').DataTable().ajax.reload();
                    }
                },
                error: function(){
                  notif('error', 'Connection disorders occurred, Please try again');
                },
            });
        } else {
            $.ajax({
                url: base_url + 'Cabin_Performance/update_mitigation_funct',
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function (data) {
                    notif('success', 'Data successfully saved');
                    if (data.status) {
                        $('#modal-functionality').modal('hide');
                        $('#mitigation-functionality').DataTable().ajax.reload();
                    }
                },
                error: function(){
                  notif('error', 'Connection disorders occurred, Please try again');
                },
            });
        }
    }

</script>

<!--==============================================MODAL TARGET==============================================-->
<div class="modal bounceIn" id="modal-target" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title-panel" style="text-align: center;font-weight: bold">Target Cabin</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="form-target" action="" method="post">
                        <div class="form-group">
                            <div class="col-sm-8">
                                <input type="hidden" name="target_id">
                                <input type="hidden" name="target_year">
                                <input type="hidden" name="target_type">
                                <input type="number" class="form-control" name="target_val">
                                <label style="font-style: italic;">* Current Year : 2018</label>
                            </div>
                            <div class="wadah col-sm-4">
                                <button type="submit" onclick="save_target()" class="btn btn-flat bg-navy"><i
                                            class="fa fa-save"></i> Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--==============================================MODAL INTERIOR==============================================-->
<div class="modal bounceIn" id="modal-interior" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title-panel" id="tittle-mitigasi-interior"
                    style="text-align: center;font-weight: bold"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form-horizontal" method="post" id="form-interior">
                        <div class="box-body">
                            <input type="hidden" name="cabin_mitigation_id">
                            <input type="hidden" name="cabin_mitigation_date">
                            <input type="hidden" name="actype">
                            <input type="hidden" name="acreg">
                            <input type="hidden" name="type_mitigasi">
                            <input type="hidden" name="item">
                            <input type="hidden" name="nm_item">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Mitigation (WHY)sssss</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="3" name="cabin_mitigation_why"
                                              placeholder="Text ..."></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Mitigation (SOLUTION)</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="3" name="cabin_mitigation_solution"
                                              placeholder="Text ..."></textarea>
                                </div>
                            </div>
                        </div>
                        <div style="margin-left: 165px;">
                            <button type="submit" class="btn btn-flat bg-navy" onclick="save_mitigation_in()"><i
                                        class="fa fa-save"></i> Submit
                            </button>
                            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i
                                        class="fa fa-rotate-left"></i> Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--==============================================MODAL EXTERIOR==============================================-->
<div class="modal bounceIn" id="modal-exterior" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title-panel" id="tittle-mitigasi-exterior"
                    style="text-align: center;font-weight: bold"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form-horizontal" method="post" id="form-exterior">
                        <div class="box-body">
                            <input type="hidden" name="cabin_mitigation_id">
                            <input type="hidden" name="cabin_mitigation_date">
                            <input type="hidden" name="actype">
                            <input type="hidden" name="acreg">
                            <input type="hidden" name="type_mitigasi">
                            <input type="hidden" name="item">
                            <input type="hidden" name="nm_item">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Mitigation (WHY)</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="3" name="cabin_mitigation_why"
                                              placeholder="Text ..."></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Mitigation (SOLUTION)</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="3" name="cabin_mitigation_solution"
                                              placeholder="Text ..."></textarea>
                                </div>
                            </div>
                        </div>
                        <div style="margin-left: 165px;">
                            <button type="submit" class="btn btn-flat bg-navy" onclick="save_mitigation_ext()"><i
                                        class="fa fa-save"></i> Submit
                            </button>
                            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i
                                        class="fa fa-rotate-left"></i> Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--==============================================MODAL FUNCTIONALITY==============================================-->
<div class="modal bounceIn" id="modal-functionality" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title-panel" id="tittle-mitigasi-functionality"
                    style="text-align: center;font-weight: bold"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form-horizontal" method="post" id="form-functionality">
                        <div class="box-body">
                            <input type="hidden" name="cabin_mitigation_id">
                            <input type="hidden" name="cabin_mitigation_date">
                            <input type="hidden" name="actype">
                            <input type="hidden" name="acreg" value="0">
                            <input type="hidden" name="type_mitigasi">
                            <input type="hidden" name="item">
                            <input type="hidden" name="nm_item">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Mitigation (WHY)</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="3" name="cabin_mitigation_why"
                                              placeholder="Text ..."></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Mitigation (SOLUTION)</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="3" name="cabin_mitigation_solution"
                                              placeholder="Text ..."></textarea>
                                </div>
                            </div>
                        </div>
                        <div style="margin-left: 165px;">
                            <button type="submit" class="btn btn-flat bg-navy" onclick="save_mitigation_funct()"><i
                                        class="fa fa-save"></i> Submit
                            </button>
                            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i
                                        class="fa fa-rotate-left"></i> Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--
END DIMAS ISLAMI
-->
