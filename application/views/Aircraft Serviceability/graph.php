<style>

    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0f2233;
    }

    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }

    .wadah {
        padding-left: 1px;
        padding-right: 0px;
    }

    .border {
        border-bottom: solid;
        border-bottom-width: thin;
        border-right: solid;
        border-right-width: thin;
    }

    p.hovertest:hover {
        color: blue;
    }

    .title-manufacture {
        color: #F2C573;
        text-align: center;
        font-size: 25px;
        font-weight: bold;
    }

    .widget-user .widget-user-image {
        position: absolute;
        top: 50px;
        left: 42%;
        margin-left: -15px;
    }

    .card {
        overflow-x: scroll;
        white-space: nowrap;
        /* max-width: 150px; */
        /* padding: 1rem; */
    }

    .gaol {
        margin: auto;
        position: relative;
        width: 65%;
        padding-top: 5px;
        padding-top: 0px;
    }

    .scrolls {
        overflow-x: scroll;
        overflow-y: hidden;
        height: 182px;
        white-space: nowrap;
        display: -webkit-inline-box;
    }

    .box-banner {
        margin: 0px 10px 47px 0px;
        border-radius: 20px;
        background: background: linear-gradient(to bottom, #001f3f 50%, #111111 100%);
        width: 244px;
        cursor: pointer;
    }

    .box-banner:hover {
        opacity: 0.8;
    }

    .box-banner .box-header {
        border-top-right-radius: 20px;
        border-top-left-radius: 20px;
        background: linear-gradient(to bottom, #001f3f, #111111);
    }

    .box-header .text-place {
        display: inline-flex;
        margin-top: 7px;
        margin-left: 40px;
    }

    .text-place h3 {
        color: #ff0505;
        font-size: 27px;
        margin-top: 26%;
    }

    .text-place .small-title.up {
        margin-top: 12%;
        /* margin-left: 94%; */
        font-size: 37px;
        position: absolute;
        right: 65px;
        bottom: -18px;
    }

    .text-place .small-title.down {
        margin-top: 12%;
        /* margin-left: 94%; */
        font-size: 37px;
        position: absolute;
        right: 65px;
        bottom: -3px;
    }

    .title-image {
        top: 10px !important;
        left: 40% !important;
    }

    .box-banner .footer-banner {
        border-bottom-right-radius: 20px;
        border-bottom-left-radius: 20px;
        padding: 0px;
        background: #fff;
    }

    .footer-banner .text-place {
        font-size: 15px;
        /* font-weight: bold; */
        color: #111;
        text-align: center;
        /* text-decoration: underline; */
        padding-right: 32px;
    }

    .box-header h3 {
        font-weight: bold;
    }

    .content {
        margin-top: -14px;
    }

    .widget-user .widget-user-header {
        height: 100px;
    }
    .title-manufacture2 {
        color: #F2C573;
        text-align: center;
        font-size: 20px;
        font-weight: bold;
        /* margin-top: 27px; */
        margin-bottom:0px;
        margin-left: 11px;
    }
    .row-md4-title{
      padding-right: 0px;
      padding-left: 40px;
      padding-top: 5px;
    }
    .title_panel_h3{
      margin-top:0px;
      margin-bottom:0px;
      font-size:15px;
    }
    .garis_hr{
      margin-top:0px;
      border-top: 2px solid #c1c1c1;
    }
</style>
<section class="content">
    <div class="">
        <div class="box-body">
            <div class="row">
                <div class="wadah col-lg-12">
                    <section class="content">
                        <div class="box" style="margin-bottom:-20px">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?= $title ?></h3>
                                <div class="box-tools pull-right">
                                    <label class="control-label" style="font-weight: 100;"><i
                                                class="fa  fa-calendar"></i> Year To Date :</label>
                                    <label class="control-label" id="date"><?= $ytd ?></label>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="box box-widget widget-user" style="border-radius: 11px;">
                                            <div class="widget-user-header bg-navy"
                                                 style="background: linear-gradient(to bottom, #001f3f, #111111);padding: 1px;height: 60px;border-top-right-radius: 11px;border-top-left-radius: 11px;">
                                            </div>
                                            <div class="widget-user-image" style="top: 10px;left: 43%;">
                                                <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                     style="border: transparent;width: 135px;">
                                            </div>
                                            <div class="box-footer"
                                                 style="border-top-left-radius: 0;border-top-right-radius: 0;border-bottom-right-radius: 11px;border-bottom-left-radius: 11px;border-top: 1px solid #f4f4f4;padding: 0px;background-color: #fff;">
                                                <div class="row">
                                                    <div class="col-sm-6 border-right">
                                                        <!-- <div class="description-block">
                                                          <h5 class="description-header" style="margin-left: 15px; font-size:25px; font-weight:bold;">
                                                          <?= $sekarang ?>
                                                          <?php
                                                        if ($sekarang >= 100) {
                                                            echo '<i class="fa fa-caret-up" style="color:#8ec3a7; font-size:20px;"></i>';
                                                        } else {
                                                            echo '<i class="fa fa-caret-down" style="color:#f24738; font-size:20px;"></i>';
                                                        }
                                                        ?>
                                                          </h5>
                                                          <span class="description-text" style="font-size: 15px;">Actual</span>
                                                      </div> -->
                                                        <div class="description-block"
                                                             style="display: block;margin: 2px 0;text-align: center;     padding: 5px;">
                                                            <h5 class="description-header"
                                                                style="margin-left: 15px;padding: 0;font-size: 25px;font-weight: bold;">
                                                                <?= $sekarang ?>
                                                                <?php
                                                                if ($sekarang >= 100) {
                                                                    echo '<i class="fa fa-caret-up" style="color:#8ec3a7; font-size:20px;"></i>';
                                                                } else {
                                                                    echo '<i class="fa fa-caret-down" style="color:#f24738; font-size:20px;"></i>';
                                                                }
                                                                ?>
                                                            </h5>
                                                            <span class="description-text">Actual</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="description-block">
                                                            <!-- <h5 class="description-header" style="font-size:25px; font-weight:bold;"><?= $target ?><i class="fa fa-minus" style="color:#F2C573;"></i></h5>
                                                          <span class="description-text" style="margin-left: -10px; font-size:15px;">Target</span> -->
                                                            <div class="description-block"
                                                                 style="display: block;margin: -10px 0;text-align: center;     padding: 5px;">
                                                                <h5 class="description-header"
                                                                    style="padding: 0;font-size: 25px;font-weight: bold;">
                                                                    <?= $target ?>
                                                                    <i class="fa fa-minus"
                                                                       style="color:#F2C573; font-size:20px;"></i>
                                                                </h5>
                                                                <span class="description-text">Target</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-3">
                                    <div class="box box-widget widget-user" style="border-radius: 20px;background:   background: linear-gradient(to bottom, #001f3f 50%, #111111 100%);/* background: linear-gradient(to bottom, #001f3f 50%, #111111 100%); */">
                                        <div class="widget-user-header bg-navy" style="border-top-right-radius: 20px;border-top-left-radius: 20px;/* background: linear-gradient(to bottom, #001f3f, #111111); */height: 99px;">
                                          <div style="display:  inline-flex;">
                                            <h3 style="margin-bottom: 0px;color:#ff0505;font-size: 33px;margin-top: 29%;"><?= $sekarang ?></h3>
                                              <span style="margin-top: 10%;/* margin-left: 131px; */font-size: 34px;position:  absolute;right: 28px;">
                                                <?= (($w_sekarang == "#ff0505") ? '<i class="fa fa-sort-down" style="color: red;display: initial;"></i>' : '<i class="fa fa-sort-up" style="color: red;display: initial;"></i>') ?>
                                              </span>
                                            </div>
                                        </div>
                                        <div class="widget-user-image" style="top: 10px; left: 40%;">
                                            <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png" alt="User Avatar" style="border: transparent;width: 150px;">
                                        </div>
                                        <div class="box-footer" style="border-bottom-right-radius: 20px;border-bottom-left-radius: 20px;padding:  0px;background: #001f3f;/* background: linear-gradient(to bottom, #001f3f 50%, #111111 100%); */background: linear-gradient(to bottom, #001f3f, #111111);">
                                            <div class="row" style="">
                                                <div class="col-sm-4 " style="margin-left: 4%;margin-top: 2%;font-size:  17px;color:  #F2C573;">
                                                  Target:
                                                </div>
                                                <div class="col-sm-4 " style="margin-left: 4%;margin-top: 2%;margin-bottom:  8px;font-size: 17px;color:  #fff;">
                                                  <?= $target ?>                                      </div>
                                                <div class="col-sm-4" style="margin-left: -8%;margin-top: -1%;font-size: 21px;padding:  0px;">
                                                  <i class="fa fa-minus" style="margin-left: 45px;color:  #F2C573;"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div> -->
                                    <div class="gaol" style="display:  inline-flex;">
                                        <div class="scrolls">
                                            <?php if ($A333 > 0): ?>
                                                <div onclick="detail_panel('A333')"
                                                     class="box box-widget widget-user box-banner" style="">
                                                    <div class="widget-user-header bg-navy box-header" style="">

                                                        <div class="col-md-12" style="padding-top:15px;padding-left: 0px;padding-right: 0px;">
                                                          <div class="row">
                                                            <div class="col-md-4 row-md4-title">
                                                              <h4 class="title_panel_h3">Actual</h4>
                                                            </div>
                                                              <div class="col-md-8">

                                                                <label class="title-manufacture2" style="color:#F2C573;"><?= $A333 ?></label>
                                                                <span>
                                                                  <?= (($WA333 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;font-size:24px;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;font-size:24px;"></i>') ?>
                                                                </span>

                                                              <!-- <h3 style="color:#F2C573;"><?= $A333 ?></h3>
                                                              <span class="small-title <?= (($WA333 == "#ff0505") ? "down" : "up") ?>">
                                                                <?= (($WA333 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;"></i>') ?>
                                                              </span> -->
                                                            </div>
                                                          </div>
                                                          <div class="row">
                                                            <div class="col-md-4 row-md4-title">
                                                              <h4 class="title_panel_h3">Pulsa</h4>
                                                            </div>
                                                            <div class="col-md-8">
                                                              <label class="title-manufacture2"><?= $P_A333 ?></label>
                                                            </div>
                                                          </div>
                                                        </div>

                                                    </div>
                                                    <div class="widget-user-image title-image">
                                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                             alt="User Avatar"
                                                             style="border: transparent;width: 100px;">
                                                    </div>
                                                    <!-- <div class="widget-user-image">
                                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                             style="border: transparent;width: 100px;">
                                                    </div> -->
                                                    <div class="box-footer footer-banner">
                                                        <div class="row" style="">
                                                            <div class="col-sm-12 text-place">
                                                                A333
                                                                <i class="fa fa-ellipsis-v pull-right"
                                                                   style="margin-top:  4px;"> </i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($A332 > 0): ?>
                                                <div onclick="detail_panel('A332')"
                                                     class="box box-widget widget-user box-banner" style="">
                                                    <div class="widget-user-header bg-navy box-header" style="">

                                                      <div class="col-md-12" style="padding-top:15px;padding-left: 0px;padding-right: 0px;">
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Actual</h4>
                                                          </div>
                                                            <div class="col-md-8">

                                                              <label class="title-manufacture2" style="color:#F2C573;"><?= $A332 ?></label>
                                                              <span>
                                                                <?= (($WA332 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;font-size:24px;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;font-size:24px;"></i>') ?>
                                                              </span>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Pulsa</h4>
                                                          </div>
                                                          <div class="col-md-8">
                                                            <label class="title-manufacture2"><?= $P_A332 ?></label>
                                                          </div>
                                                        </div>
                                                      </div>

                                                    </div>
                                                    <div class="widget-user-image title-image">
                                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                             alt="User Avatar"
                                                             style="border: transparent;width: 100px;">
                                                    </div>
                                                    <div class="box-footer footer-banner">
                                                        <div class="row" style="">
                                                            <div class="col-sm-12 text-place">
                                                                A332
                                                                <i class="fa fa-ellipsis-v pull-right"
                                                                   style="margin-top:  4px;"> </i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($ATR72 > 0): ?>
                                                <div onclick="detail_panel('ATR72')"
                                                     class="box box-widget widget-user box-banner" style="">
                                                    <div class="widget-user-header bg-navy box-header" style="">

                                                      <div class="col-md-12" style="padding-top:15px;padding-left: 0px;padding-right: 0px;">
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Actual</h4>
                                                          </div>
                                                            <div class="col-md-8">

                                                              <label class="title-manufacture2" style="color:#F2C573;"><?= $ATR72 ?></label>
                                                              <span>
                                                                <?= (($WATR72 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;font-size:24px;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;font-size:24px;"></i>') ?>
                                                              </span>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Pulsa</h4>
                                                          </div>
                                                          <div class="col-md-8">
                                                            <label class="title-manufacture2"><?= $P_ATR72 ?></label>
                                                          </div>
                                                        </div>
                                                      </div>

                                                        <!-- <div class="text-place">
                                                            <h3 style="color: #F2C573;"><?= $ATR72 ?></h3>
                                                            <span class="small-title  <?= (($WATR72 == "#ff0505") ? "down" : "up") ?>">
                                                  <?= (($WATR72 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;font-size:"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;"></i>') ?>
                                                </span>
                                                        </div> -->

                                                    </div>
                                                    <div class="widget-user-image title-image">
                                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                             alt="User Avatar"
                                                             style="border: transparent;width: 100px;">
                                                    </div>
                                                    <div class="box-footer footer-banner">
                                                        <div class="row" style="">
                                                            <div class="col-sm-12 text-place">
                                                                ATR72
                                                                <i class="fa fa-ellipsis-v pull-right"
                                                                   style="margin-top:  4px;"> </i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($B777 > 0): ?>
                                                <div onclick="detail_panel('B777')"
                                                     class="box box-widget widget-user box-banner" style="">
                                                    <div class="widget-user-header bg-navy box-header" style="">

                                                      <div class="col-md-12" style="padding-top:15px;padding-left: 0px;padding-right: 0px;">
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Actual</h4>
                                                          </div>
                                                            <div class="col-md-8">

                                                              <label class="title-manufacture2" style="color:#F2C573;"><?= $B777 ?></label>
                                                              <span>
                                                                <?= (($WB777 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;font-size:24px;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;font-size:24px;"></i>') ?>
                                                              </span>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Pulsa</h4>
                                                          </div>
                                                          <div class="col-md-8">
                                                            <label class="title-manufacture2"><?= $P_B777 ?></label>
                                                          </div>
                                                        </div>
                                                      </div>

                                                        <!-- <div class="text-place">
                                                            <h3 style="color: #F2C573;"><?= $B777 ?></h3>
                                                            <span class="small-title <?= (($WB777 == "#ff0505") ? "down" : "up") ?>">
                                                  <?= (($WB777 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;"></i>') ?>
                                                </span>
                                                        </div> -->

                                                    </div>
                                                    <div class="widget-user-image title-image">
                                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                             alt="User Avatar"
                                                             style="border: transparent;width: 100px;">
                                                    </div>
                                                    <div class="box-footer footer-banner">
                                                        <div class="row" style="">
                                                            <div onclick="detail_panel('B777')"
                                                                 class="col-sm-12 text-place">
                                                                B777
                                                                <i class="fa fa-ellipsis-v pull-right"
                                                                   style="margin-top:  4px;"> </i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($B744 > 0): ?>
                                                <div onclick="detail_panel('B744')"
                                                     class="box box-widget widget-user box-banner" style="">
                                                    <div class="widget-user-header bg-navy box-header" style="">

                                                      <div class="col-md-12" style="padding-top:15px;padding-left: 0px;padding-right: 0px;">
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Actual</h4>
                                                          </div>
                                                            <div class="col-md-8">

                                                              <label class="title-manufacture2" style="color:#F2C573;"><?= $B744 ?></label>
                                                              <span>
                                                                <?= (($WB744 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;font-size:24px;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;font-size:24px;"></i>') ?>
                                                              </span>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Pulsa</h4>
                                                          </div>
                                                          <div class="col-md-8">
                                                            <label class="title-manufacture2"><?= $P_B744 ?></label>
                                                          </div>
                                                        </div>
                                                      </div>

                                                        <!-- <div class="text-place">
                                                            <h3 style="color: #F2C573;"><?= $B744 ?></h3>
                                                            <span class="small-title <?= (($WB744 == "#ff0505") ? "down" : "up") ?>">
                                                  <?= (($WB744 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;"></i>') ?>
                                                </span>
                                                        </div> -->

                                                    </div>
                                                    <div class="widget-user-image title-image">
                                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                             alt="User Avatar"
                                                             style="border: transparent;width: 100px;">
                                                    </div>
                                                    <div class="box-footer footer-banner">
                                                        <div class="row" style="">
                                                            <div class="col-sm-12 text-place">
                                                                B744
                                                                <i class="fa fa-ellipsis-v pull-right"
                                                                   style="margin-top:  4px;"> </i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($B738 > 0): ?>
                                                <div onclick="detail_panel('B738')"
                                                     class="box box-widget widget-user box-banner" style="">
                                                    <div class="widget-user-header bg-navy box-header" style="">

                                                      <div class="col-md-12" style="padding-top:15px;padding-left: 0px;padding-right: 0px;">
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Actual</h4>
                                                          </div>
                                                            <div class="col-md-8">

                                                              <label class="title-manufacture2" style="color:#F2C573;"><?= $B738 ?></label>
                                                              <span>
                                                                <?= (($WB738 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;font-size:24px;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;font-size:24px;"></i>') ?>
                                                              </span>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Pulsa</h4>
                                                          </div>
                                                          <div class="col-md-8">
                                                            <label class="title-manufacture2"><?= $P_B738 ?></label>
                                                          </div>
                                                        </div>
                                                      </div>

                                                        <!-- <div class="text-place">
                                                            <h3 style="color: #F2C573;"><?= $B738 ?></h3>
                                                            <span class="small-title <?= (($WB738 == "#ff0505") ? "down" : "up") ?>">
                                                  <?= (($WB738 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;"></i>') ?>
                                                </span>
                                                        </div> -->

                                                    </div>
                                                    <div class="widget-user-image title-image">
                                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                             alt="User Avatar"
                                                             style="border: transparent;width: 100px;">
                                                    </div>
                                                    <div class="box-footer footer-banner">
                                                        <div class="row" style="">
                                                            <div class="col-sm-12 text-place">
                                                                B738
                                                                <i class="fa fa-ellipsis-v pull-right"
                                                                   style="margin-top:  4px;"> </i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($B735 > 0): ?>
                                                <div onclick="detail_panel('B735')"
                                                     class="box box-widget widget-user box-banner" style="">
                                                    <div class="widget-user-header bg-navy box-header" style="">

                                                      <div class="col-md-12" style="padding-top:15px;padding-left: 0px;padding-right: 0px;">
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Actual</h4>
                                                          </div>
                                                            <div class="col-md-8">

                                                              <label class="title-manufacture2" style="color:#F2C573;"><?= $B735 ?></label>
                                                              <span>
                                                                <?= (($WB735 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;font-size:24px;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;font-size:24px;"></i>') ?>
                                                              </span>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Pulsa</h4>
                                                          </div>
                                                          <div class="col-md-8">
                                                            <label class="title-manufacture2"><?= $P_B735 ?></label>
                                                          </div>
                                                        </div>
                                                      </div>

                                                        <!-- <div class="text-place">
                                                            <h3 style="color: #F2C573;"><?= $B735 ?></h3>
                                                            <span class="small-title <?= (($WB735 == "#ff0505") ? "down" : "up") ?>">
                                                  <?= (($WB735 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;"></i>') ?>
                                                </span>
                                                        </div>
                                                         -->
                                                    </div>
                                                    <div class="widget-user-image title-image">
                                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                             alt="User Avatar"
                                                             style="border: transparent;width: 100px;">
                                                    </div>
                                                    <div class="box-footer footer-banner">
                                                        <div class="row" style="">
                                                            <div class="col-sm-12 text-place">
                                                                B735
                                                                <i class="fa fa-ellipsis-v pull-right"
                                                                   style="margin-top:  4px;"> </i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($CRJ1000 > 0): ?>
                                                <div onclick="detail_panel('CRJ1000')"
                                                     class="box box-widget widget-user box-banner" style="">
                                                    <div class="widget-user-header bg-navy box-header" style="">

                                                      <div class="col-md-12" style="padding-top:15px;padding-left: 0px;padding-right: 0px;">
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Actual</h4>
                                                          </div>
                                                            <div class="col-md-8">

                                                              <label class="title-manufacture2" style="color:#F2C573;"><?= $CRJ1000 ?></label>
                                                              <span>
                                                                <?= (($WCRJ1000 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;font-size:24px;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;font-size:24px;"></i>') ?>
                                                              </span>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Pulsa</h4>
                                                          </div>
                                                          <div class="col-md-8">
                                                            <label class="title-manufacture2"><?= $P_CRJ ?></label>
                                                          </div>
                                                        </div>
                                                      </div>

                                                        <!-- <div class="text-place">
                                                            <h3 style="color: #F2C573;"><?= $CRJ1000 ?></h3>
                                                            <span class="small-title <?= (($WCRJ1000 == "#ff0505") ? "down" : "up") ?>">
                                                  <?= (($WCRJ1000 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;"></i>') ?>
                                                </span>
                                                        </div> -->

                                                    </div>
                                                    <div class="widget-user-image title-image">
                                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                             alt="User Avatar"
                                                             style="border: transparent;width: 100px;">
                                                    </div>
                                                    <div class="box-footer footer-banner">
                                                        <div class="row" style="">
                                                            <div class="col-sm-12 text-place">
                                                                CRJ1000
                                                                <i class="fa fa-ellipsis-v pull-right"
                                                                   style="margin-top:  4px;"> </i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($B7M8 > 0) : ?>
                                                <div onclick="detail_panel('B7M8')"
                                                     class="box box-widget widget-user box-banner" style="">
                                                    <div class="widget-user-header bg-navy box-header" style="">

                                                      <div class="col-md-12" style="padding-top:15px;padding-left: 0px;padding-right: 0px;">
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Actual</h4>
                                                          </div>
                                                            <div class="col-md-8">

                                                              <label class="title-manufacture2" style="color:#F2C573;"><?= $B7M8 ?></label>
                                                              <span>
                                                                <?= (($WB7M8 == "#ff0505") ? '<i class="fa fa-sort-down" style="color: #f24738; display: initial;font-size:24px;"></i>' : '<i class="fa fa-sort-up" style="color: #8ec3a7; display: initial;font-size:24px;"></i>') ?>
                                                              </span>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <div class="col-md-4 row-md4-title">
                                                            <h4 class="title_panel_h3">Pulsa</h4>
                                                          </div>
                                                          <div class="col-md-8">
                                                            <label class="title-manufacture2"><?= $P_B7M8 ?></label>
                                                          </div>
                                                        </div>
                                                      </div>

                                                    </div>
                                                    <div class="widget-user-image title-image">
                                                        <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                                             alt="User Avatar"
                                                             style="border: transparent;width: 100px;">
                                                    </div>
                                                    <div class="box-footer footer-banner">
                                                        <div class="row" style="">
                                                            <div class="col-sm-12 text-place">
                                                                B7M8
                                                                <i class="fa fa-ellipsis-v pull-right"
                                                                   style="margin-top:  4px;"> </i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            
                                        </div>
                                    </div>
                                </div>
                                <hr class="garis_hr">

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">

                                            <label style="    margin-top: 8px;padding-left:0px;" for="inputEmail3"
                                                   class="col-sm-3">Start Date</label>

                                            <div class="col-sm-9" style="padding-right:0px;padding-left:0px;">
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>

                                                    <input type="text" class="form-control pull-right" id="date_start"
                                                           value="<?= date('d-m-Y', strtotime(" -1 month", strtotime(date("d-m-Y")))); ?>">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">

                                            <label style="    margin-top: 8px;padding-left:0px;" for="inputEmail3"
                                                   class="col-sm-3">End Date</label>

                                            <div class="col-sm-9"  style="padding-right:0px;padding-left:0px;">
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>

                                                    <input type="text" class="form-control pull-right" id="date_end"
                                                           value="<?= date('d-m-Y') ?>">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <button class="btn btn-flat bg-navy" id="search">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                            <button class="btn btn-flat bg-navy" id="uploadfile" ><i class="fa fa-upload"> </i> Upload Data </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-2"></div>
                                    <br><br><br>
                                </div>
                                <div id="grafik_down">
                                    <div class="row" style="padding-right:10px; padding-left:10px;">
                                        <div class="col-md-3 "
                                             id="grafik1Responsive" style="height:301px;">
                                            <div class="overlay-box" id="loader1"
                                                 style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                <div style="" class="loading"></div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <div id="grafik1"></div>
                                              </div>
                                            </div>
                                            <div class="row" style="padding-bottom:10px;">
                                              <div class="col-md-12">
                                                <div class="col-md-6" style="padding-left: 0px;padding-right: 5px;">
                                                  <button type="button" class="btn btn-block btn-success btn-flat" onclick="zoom_grafik('A333')">View Detail</button>
                                                </div>
                                                <div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
                                                  <button type="button" class="btn btn-block btn-primary btn-flat" onclick="top_remarks3('A333')">Top 3 Trouble</button>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 "
                                             id="grafik2Responsive" style="height:301px;">
                                            <div class="overlay-box" id="loader2"
                                                 style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                <div style="" class="loading"></div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <div id="grafik2"></div>
                                              </div>
                                            </div>
                                            <div class="row" style="padding-bottom:10px;">
                                              <div class="col-md-12">
                                                <div class="col-md-6" style="padding-left: 0px;padding-right: 5px;">
                                                  <button type="button" class="btn btn-block btn-success btn-flat" onclick="zoom_grafik('A332')">View Detail</button>
                                                </div>
                                                <div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
                                                  <button type="button" class="btn btn-block btn-primary btn-flat" onclick="top_remarks3('A332')">Top 3 Trouble</button>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 "
                                             id="grafik3Responsive" style="height:301px;">
                                            <div class="overlay-box" id="loader3"
                                                 style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                <div style="" class="loading"></div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <div id="grafik3"></div>
                                              </div>
                                            </div>
                                            <div class="row" style="padding-bottom:10px;">
                                              <div class="col-md-12">
                                                <div class="col-md-6" style="padding-left: 0px;padding-right: 5px;">
                                                  <button type="button" class="btn btn-block btn-success btn-flat" onclick="zoom_grafik('ATR72')">View Detail</button>
                                                </div>
                                                <div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
                                                  <button type="button" class="btn btn-block btn-primary btn-flat" onclick="top_remarks3('ATR72')">Top 3 Trouble</button>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 "
                                             id="grafik4Responsive" style="height:301px;">
                                            <div class="overlay-box" id="loader4"
                                                 style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                <div style="" class="loading"></div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <div id="grafik4"></div>
                                              </div>
                                            </div>
                                            <div class="row" style="padding-bottom:10px;">
                                              <div class="col-md-12">
                                                <div class="col-md-6" style="padding-left: 0px;padding-right: 5px;">
                                                  <button type="button" class="btn btn-block btn-success btn-flat" onclick="zoom_grafik('B777')">View Detail</button>
                                                </div>
                                                <div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
                                                  <button type="button" class="btn btn-block btn-primary btn-flat" onclick="top_remarks3('B777')">Top 3 Trouble</button>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-right:10px; padding-left:10px;">
                                        <div class="col-md-3 "
                                             id="grafik5Responsive" style="height:301px;">
                                            <div class="overlay-box" id="loader5"
                                                 style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                <div style="" class="loading"></div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <div id="grafik5"></div>
                                              </div>
                                            </div>
                                            <div class="row" style="padding-bottom:10px;">
                                              <div class="col-md-12">
                                                <div class="col-md-6" style="padding-left: 0px;padding-right: 5px;">
                                                  <button type="button" class="btn btn-block btn-success btn-flat" onclick="zoom_grafik('B744')">View Detail</button>
                                                </div>
                                                <div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
                                                  <button type="button" class="btn btn-block btn-primary btn-flat" onclick="top_remarks3('B744')">Top 3 Trouble</button>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 "
                                             id="grafik6Responsive" style="height:301px;">
                                            <div class="overlay-box" id="loader6"
                                                 style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                <div style="" class="loading"></div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <div id="grafik6"></div>
                                              </div>
                                            </div>
                                            <div class="row" style="padding-bottom:10px;">
                                              <div class="col-md-12">
                                                <div class="col-md-6" style="padding-left: 0px;padding-right: 5px;">
                                                  <button type="button" class="btn btn-block btn-success btn-flat" onclick="zoom_grafik('B738')">View Detail</button>
                                                </div>
                                                <div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
                                                  <button type="button" class="btn btn-block btn-primary btn-flat" onclick="top_remarks3('B738')">Top 3 Trouble</button>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 "
                                             id="grafik7Responsive" style="height:301px;">
                                            <div class="overlay-box" id="loader7"
                                                 style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                <div style="" class="loading"></div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <div id="grafik7"></div>
                                              </div>
                                            </div>
                                            <div class="row" style="padding-bottom:10px;">
                                              <div class="col-md-12">
                                                <div class="col-md-6" style="padding-left: 0px;padding-right: 5px;">
                                                  <button type="button" class="btn btn-block btn-success btn-flat" onclick="zoom_grafik('B735')">View Detail</button>
                                                </div>
                                                <div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
                                                  <button type="button" class="btn btn-block btn-primary btn-flat" onclick="top_remarks3('B735')">Top 3 Trouble</button>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 "
                                             id="grafik8Responsive" style="height:301px;">
                                            <div class="overlay-box" id="loader8"
                                                 style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                <div style="" class="loading"></div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <div id="grafik8"></div>
                                              </div>
                                            </div>
                                            <div class="row" style="padding-bottom:10px;">
                                              <div class="col-md-12">
                                                <div class="col-md-6" style="padding-left: 0px;padding-right: 5px;">
                                                  <button type="button" class="btn btn-block btn-success btn-flat" onclick="zoom_grafik('CRJ1000')">View Detail</button>
                                                </div>
                                                <div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
                                                  <button type="button" class="btn btn-block btn-primary btn-flat" onclick="top_remarks3('CRJ1000')">Top 3 Trouble</button>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 "
                                             id="grafik9Responsive" style="height:301px;">
                                            <div class="overlay-box" id="loader9"
                                                 style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                                                <div style="" class="loading"></div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <div id="grafik9"></div>
                                              </div>
                                            </div>
                                            <div class="row" style="padding-bottom:10px;">
                                              <div class="col-md-12">
                                                <div class="col-md-6" style="padding-left: 0px;padding-right: 5px;">
                                                  <button type="button" class="btn btn-block btn-success btn-flat" onclick="zoom_grafik('B7M8')">View Detail</button>
                                                </div>
                                                <div class="col-md-6" style="padding-left: 5px;padding-right: 0px;">
                                                  <button type="button" class="btn btn-block btn-primary btn-flat" onclick="top_remarks3('B7M8')">Top 3 Trouble</button>
                                                </div>
                                              </div>
                                            </div><br>
                                        </div>
                                        <br>
                                    </div>
                                </div>

                                <div class="box" id="grafik_big" style="display:none;">
                                    <input style='display:none' type='text' id='status_view' value='0'>
                                    <input style='display:none' type='text' id='tipe_view'>
                                    <div class="box-header with-border">
                                        <h3 class="box-title-zoom"></h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-default btn-flat" id="back"><i
                                                        class="fa fa-arrow-left"></i> Back
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div id="grafikzoom" style="height:400px;width:100%"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                    </section>
                </div>


                <!-- <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <section class="content">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 style="font-size: 17px;" class="box-title"><?= $title2 ?></h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"
                                            data-toggle="tooltip"
                                            title="Collapse">
                                        <i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">


                            </div>
                        </div>
                </div> -->
</section>

<div class="modal fade" id="modal_knobchart" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg pull-center" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233">
                <h4 class="modal-title" id="title_grafik_panel" style="color: #fff"</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="modal_content_place1"
                         style="height:300px;">
                        <div class="overlay-box" id="loader_modal1"
                             style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                            <div style="" class="loading"></div>
                        </div>
                        <div id="grafik_panel"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i
                            class="fa fa-rotate-left"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_panel_grafik" tabindex="-1" role="dialog" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog modal-lg pull-center" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233">
                <h4 class="modal-title" id="title_detail_grafik_panel" style="color: #fff"</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="modal_content_place2"
                         style="height:300px;">
                        <div class="overlay-box" id="loader_modal2"
                             style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                            <div style="" class="loading"></div>
                        </div>
                        <div id="detail_grafik_panel"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i
                            class="fa fa-rotate-left"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_linechart" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233">
                <h4 class="modal-title" id="title-edit" style="color: #fff"</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i
                                class="fa fa-rotate-left"></i> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_grafik1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233">
                <h4 class="modal-title" style="color: #fff" id="title-detail"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover table-bordered" id="tabel_detail">
                            <thead class="bg-navy">
                            <tr>
                                <th class="text-center">Date</th>
                                <th class="text-center">Group</th>
                                <th class="text-center">Remarks</th>
                                <th class="text-center">Reg</th>
                                <th class="text-center">Problem Rect.</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><i
                            class="fa fa-rotate-left"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail-panel4" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" id="header-detail-panel4">
                <h4 class="modal-title" id="title-detail-panel4"></h4>
            </div>
            <div class="modal-body">
              <div class="overlay-box" id="loader98"
                   style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                  <div style="" class="loading"></div>
              </div>
                <table class="table table-hover table-bordered" id="detail-table-topremarks">
                    <thead class="bg-navy">
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Category</th>
                        <th class="text-center">Total Accident</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-rotate-left"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_upload">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0e1f2e;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Form Upload</h4>
            </div>
            <form id="form-upload-service" method="post" action="" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Choose File </label>
                            <input type="file" class="form-control" name="files" id="files" required >
                        </div> 
                        <div class="row"  id="typedataseleted">
                           <div class="form-group col-md-6">
                                <label>Start Rows</label>
                                <input type="number" name="start_rows" id="start_rows" class="form-control" required placeholder="Min Value 4">
                            </div>
                            <div class="form-group col-md-6">
                                <label>End Rows</label>
                                <input type="number" name="end_rows" id="end_rows" class="form-control" required placeholder="last row data">
                            </div> 
                        </div> 
                    </div>
                    <div class="box-body">
                        <div class="form-group col-md-12" style="border-bottom: 1px solid #f4f4f4;">
                        	<label for="inputEmail4"><i class="fa fa-info-circle"></i> Download Template</label>
                        </div>
                        <div class="form-group col-md-4">
                            <a class="btn btn-sm btn-success" href="<?php echo base_url(); ?>assets/upload-service-avaibility/template/tempalte_service_detailbus.xlsx"><i class="fa fa-file-excel-o"></i> <label for="inputPassword4" style="font-weight: normal"> Template Detail</a>           
                        </div>
                        <div class="form-group col-md-6">
                            <a class="btn btn-sm bg-olive" href="<?php echo base_url(); ?>assets/upload-service-avaibility/template/tempalte_service_exclude.xlsx"><i class="fa fa-file-excel-o"></i> <label for="inputPassword4" style="font-weight: normal"> Template Performance</a>  
                        </div>
                    </div>
                </div>                                
                <div class="modal-footer">
                    <button type="submit" id="btn-upload-data" class="btn btn-flat bg-navy"><i class="fa fa-save"></i> Save </button>
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-rotate-left"></i> Close </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    //Date range picker
    $('#date_end, #date_start').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true
    });

    $(".sidebar-toggle").on("click", function (e) {
        grafik_type("A333", "1", function (out) {

        }, false);
        grafik_type("A332", "2", function (out) {

        }, false);
        grafik_type("ATR72", "3", function (out) {

        }, false);
        grafik_type("B777", "4", function (out) {

        }, false);
        grafik_type("B744", "5", function (out) {

        }, false);
        grafik_type("B738", "6", function (out) {

        }, false);
        grafik_type("B735", "7", function (out) {

        }, false);
        grafik_type("CRJ1000", "8", function (out) {

        }, false);
        grafik_type("B7M8", "9", function (out) {

        }, false);

        zoom_grafik("A333", false);
        zoom_grafik("A332", false);
        zoom_grafik("ATR72", false);
        zoom_grafik("B777", false);
        zoom_grafik("B744", false);
        zoom_grafik("B738", false);
        zoom_grafik("B735", false);
        zoom_grafik("CRJ1000", false);
        zoom_grafik("B7M8", false);
    });

    $(document).ready(function () {

        // Change Form Upload Data

        function checkExtention(ini) {
            var namefile=ini.val();
            var ext=namefile.replace(/^.*\./, '');
            ext=ext.toLowerCase();
            extList=["xls","xlsx"];
            return ($.inArray(ext,extList)== -1) ? false:true;
        }
        $(document).on("change", "#files", function (e) {
            // console.log(checkExtention($(this)));
            if(this.files[0].size > 1300000 || checkExtention($(this)) == false){
            notif("error", "File Must Excel Extention");
            $("#files").val('');
            }
        });

        $('#uploadfile').on('click', function (e) {
            e.preventDefault();
            $('#modal_upload').modal('show');
            
        });

        // $('#form-upload-service').submit(function (e) { 
        //     e.preventDefault();
        //     var formData=new FormData($('#form-upload-service')[0]);
        //     $.ajax({
        //         url: "<?= base_url() ?>index.php/Aircraft_Service/storefile",
        //         type: "POST",
        //         processData: false,
        //         contentType: false,
        //         data: formData,  
        //         beforeSend: function () {
        //             $('#submit_file').html('<i class="fa fa-spinner fa-spin"></i> Processing').attr('disabled',true);
        //         }
        //     })
        //     .done(function(resp){
        //         var data= JSON.parse(resp);
        //         notif(data.info, data.msg);
        //          if(data.info=='success'){
        //              $('#modal_upload').modal('hide');
        //          }
        //     })
        //     .fail(function(){
        //         notif('error', 'Something Error');
        //     })
        //     .always(function(){
        //         $('#submit_file').html('<i class="fa fa-upload"></i> Upload').attr('disabled',false);
        //         $("#form_upload")[0].reset();
        //     });
            
        // });


        // End Form Upload Data






        grafik_type("A333", "1", function (out) {

        });
        grafik_type("A332", "2", function (out) {

        });
        grafik_type("ATR72", "3", function (out) {

        });
        grafik_type("B777", "4", function (out) {

        });
        grafik_type("B744", "5", function (out) {

        });
        grafik_type("B738", "6", function (out) {

        });
        grafik_type("B735", "7", function (out) {

        });
        grafik_type("CRJ1000", "8", function (out) {

        });
        grafik_type("B7M8", "9", function (out) {

        });

        $(function () {
            var cuk;
            $(document).on("click", "#search", function (e) {
                var startdate = moment($("#date_start").val(), "DD-MM-YYYY");
                var enddate = moment($("#date_end").val(), "DD-MM-YYYY");
                console.log(startdate);
                console.log(enddate);
                if (startdate > enddate) {
                    notif('warning', 'End date must be larger than start date!');

                } else {
                    var status_view = $("#status_view").val();
                    var tipe = $("#tipe_view").val();
                    if (status_view == '0') {
                        // if(grafik_type("A333", "1") < 1 && grafik_type("A332", "2") < 1 &&
                        //    grafik_type("ATR72", "3") < 1 && grafik_type("B777", "4") < 1 &&
                        //    grafik_type("B744", "5") < 1 && grafik_type("B738", "6") < 1 &&
                        //    grafik_type("B735", "7") < 1 && grafik_type("CRJ1000", "8") < 1){
                        //      alert("jancuk");
                        //    }
                        var arr = [];
                        $("#notifGrafik").text("");
                        var A333 = grafik_type("A333", "1", function (out) {
                            return out;
                            arr.push[out];
                        });
                        var A332 = grafik_type("A332", "2", function (out) {
                            $("#notifGrafik").text(out);
                            return out;
                            arr.push[out];
                        });
                        var ATR72 = grafik_type("ATR72", "3", function (out) {
                            return out;
                            arr.push[out];
                        });
                        var B777 = grafik_type("B777", "4", function (out) {
                            return out;
                            arr.push[out];
                        });
                        var B744 = grafik_type("B744", "5", function (out) {
                            return out;
                            arr.push[out];
                        });
                        var B738 = grafik_type("B738", "6", function (out) {
                            return out;
                            arr.push[out];
                        });
                        var B7M8 = grafik_type("B7M8", "9", function (out) {
                            return out;
                            arr.push[out];
                        });
                        var B735 = grafik_type("B735", "7", function (out) {
                            return out;
                            arr.push[out];
                        });
                        var CRJ1000 = grafik_type("CRJ1000", "8", function (out) {
                            console.log(out);
                            if (out < 1) {
                                notif("warning", "data not found");
                            }
                            return out;
                            arr.push[out];
                        });

                    } else {
                        zoom_grafik(tipe)
                    }
                }
            });
        });


        $(document).on("click", "#back", function (e) {
            $("#tipe_view").val('');
            $("#status_view").val('0');
            $("#grafik_big").hide();
            $("#grafik_down").show();
        });


    });

    function zoom_grafik(tipe, notRevlow = true) {
          $("#grafik_down").hide();
          $("#grafik_big").show();
          $("#status_view").val('1');
          $("#tipe_view").val($(this).text());

        if (notRevlow != true) {
            if ($("body").hasClass('sidebar-collapse')) {
                $("#grafikzoom").highcharts().setSize(980);
            } else {
                $("#grafikzoom").highcharts().setSize(0);
            }
            return true;
        }
        var start = $("#date_start").val();
        var end = $("#date_end").val();
        var colorGrafik = "#0D2633";
        $(".box-title-zoom").html(tipe);
        $.ajax({
            url: "<?= base_url() ?>index.php/Aircraft_Service/view_grafik",
            type: "POST",
            data: {
                'start': start,
                'end': end,
                'tipe': tipe,
            },
            dataType: "JSON",
            success: function (data) {

                var chart = new Highcharts.chart('grafikzoom', {
                    chart: {
                        height: 400
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: tipe
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: data.grafik[0].minimal,
                        max: 120,
                        title: {
                            text: 'Percent (%)'
                        },
                        // plotLines: [{
                        // value: data.target,
                        // color: 'red',
                        // dashStyle: 'shortdash',
                        // width: 2,
                        // label: {
                        // text: 'Target '+data.target,
                        // }
                        // }],
                    },
                    xAxis: {
                        categories: data.grafik[0].datat
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal',
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                        }
                    },

                    series: [{
                        name: tipe,
                        type: 'areaspline',
                        data: data.grafik[0].data,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    detail_grafik(tipe, this.category)
                                }
                            }
                        },
                        dataLabels: {
                            enabled: true,
                            color: "#05354D",
                            align: 'center',
                            enabled: true,
                            x: 0,
                            y: -20,
                            rotation: 270

                        },
                        color: "#05354D",
                        fillColor: {
                            linearGradient: [0, 0, 0, 140],
                            stops: [
                                [0, "#05354D"],
                                [1, "#fff3"]
                            ]
                        },
                        lineWidth: 1,
                    },
                        {
                            name: "Target",
                            data: data.grafik[0].datap,
                            color: "#f7ca78",
                            lineWidth: 1,
                            marker: {
                                enabled: false
                            }
                        }
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal',

                                }
                            }
                        }]
                    }
                });


                var series = chart.series[0];

                series.color = colorGrafik;
                console.log(colorGrafik);
                series.graph.attr({
                    stroke: colorGrafik
                });

                chart.legend.colorizeItem(series, series.visible);
                $.each(series.data, function (i, point) {
                    point.graphic.attr({
                        fill: colorGrafik
                    });
                });
                series.redraw();


            }
        });
    }

    function detail_panel(tipe) {
        $('#modal_knobchart').modal("show");
        $('#title_grafik_panel').html(tipe);

        $.ajax({
            url: "<?= base_url() ?>index.php/Aircraft_Service/view_detail_panel",
            type: "POST",
            data: {
                'tipe': tipe,
            },
            dataType: "JSON",
            beforeSend: function () {
                $("#loader_modal1").fadeIn("fast");
            },
            success: function (data) {
                console.log(data.grafik[0].datat);
                $("#loader_modal1").fadeOut(1000);
                $("#modal_content_place1").css('height', '100%');
                Highcharts.chart('grafik_panel', {
                    chart: {
                        height: 300,
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: tipe
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: data.grafik[0].minimal,
                        max: 120,
                        title: {
                            text: 'Percent (%)'
                        },
                        // plotLines: [{
                        // value: data.target,
                        // color: 'red',
                        // dashStyle: 'shortdash',
                        // width: 2,
                        // label: {
                        // text: 'Target '+data.target
                        // }
                        // }],
                    },
                    xAxis: {
                        categories: data.grafik[0].datat
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal',
                    },


                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                        }
                    },

                    series: [{
                        name: tipe,
                        type: 'areaspline',
                        data: data.grafik[0].data,
                        cursor: 'pointer',
                        fillColor: {
                            linearGradient: [0, 0, 0, 140],
                            stops: [
                                [0, "#05354D"],
                                [1, "#fff3"]
                            ]
                        },
                        dataLabels: {
                            enabled: true,
                            color: '#05354D',
                            align: 'center',
                            y: -5, // 10 pixels down from the top
                            x: 3, // 10 pixels down from the top
                        },
                        lineWidth: 1,
                        point: {
                            events: {
                                click: function () {
                                    detail_panel_grafik(tipe, this.category)
                                }
                            }
                        },
                        color: "#05354D"
                    },
                        {
                            name: "Target",
                            data: data.grafik[0].datap,
                            color: "#f7ca78",
                            lineWidth: 1,
                            marker: {
                                enabled: false
                            }
                        }
                    ],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal',

                                }
                            }
                        }]
                    }
                });


            }
        });

    }


    function detail_panel_grafik(tipe, bulan) {
        $('#modal_panel_grafik').modal("show");
        $('#title_detail_grafik_panel').html(bulan);

        $.ajax({
            url: "<?= base_url() ?>index.php/Aircraft_Service/view_detail_panel_grafik",
            type: "POST",
            data: {
                'tipe': tipe,
                'bulan': bulan,
            },
            dataType: "JSON",
            beforeSend: function () {
                $("#loader_modal2").fadeIn("fast");
            },
            success: function (data) {
                console.log(data.grafik[0].datat);
                $("#modal_content_place2").css('height', '100%');
                $("#loader_modal2").fadeOut(1000);
                Highcharts.chart('detail_grafik_panel', {
                    chart: {
                        height: 300
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: tipe
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: data.grafik[0].minimal,
                        max: 120,
                        title: {
                            text: 'Percent (%)'
                        },
                        // plotLines: [{
                        // value: data.target,
                        // color: 'red',
                        // dashStyle: 'shortdash',
                        // width: 2,
                        // label: {
                        // text: 'Target '+data.target,
                        // }
                        // }],
                    },
                    xAxis: {
                        categories: data.grafik[0].datat
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal',
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                        }
                    },

                    series: [{
                        name: tipe,
                        type: 'areaspline',
                        data: data.grafik[0].data,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    detail_grafik(tipe, this.category)
                                }
                            }
                        },
                        color: "#05354D",
                        fillColor: {
                            linearGradient: [0, 0, 0, 140],
                            stops: [
                                [0, "#4cc3d9"],
                                [1, "#fff3"]
                            ]
                        },
                        dataLabels: {
                            enabled: true,
                            color: "#05354D",
                            align: 'center',
                            enabled: true,
                            x: 0,
                            y: -20,
                            rotation: 270
                        },
                        lineWidth: 1,
                    },
                        {
                            name: "Target",
                            data: data.grafik[0].datap,
                            color: "#f7ca78",
                            lineWidth: 1,
                            marker: {
                                enabled: false
                            }
                        }
                    ],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal',

                                }
                            }
                        }]
                    }
                });


            }
        });

    }


    function detail_grafik(tipe, tanggal) {
        $("#modal_grafik1").modal("show");
        $("#title-detail").html(tipe + " - " + tanggal);
        $("#tabel_detail").DataTable({
            autoWidth: false,
            destroy: true,
            ordering: true,
            searching: false,
            processing: true,
            serverSide: true,
            // pageLength: 2,
            ajax: {
                url: "<?= base_url() ?>index.php/Aircraft_Service/datatable",
                type: 'POST',
                data: {
                    'tipe': tipe,
                    'tanggal': tanggal,
                },
            },
            "aoColumns": [
                {
                    "mData": "0",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    },
                    "width": "20%"
                },
                {
                    "mData": "1",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    },
                    "width": "20%"
                },
                {
                    "mData": "2",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    },
                    "width": "20%"
                },
                {
                    "mData": "3",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    },
                    "width": "20%"
                },
                {
                    "mData": "4",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    },
                    "width": "20%"
                },
            ]
        });


    }

    function myFunction() {
        alert(1)
    }


    function grafik_type(tipe, no, handleData, notRevlow = true) {
        var start = $('#date_start').val();
        var end = $('#date_end').val();
        var chart;
        var colorGrafik = "#0D2633";
        if (notRevlow != true) {
            console.log($("body").hasClass('sidebar-collapse'));
            if ($("body").hasClass('sidebar-collapse')) {
                $("#grafik" + no).highcharts().setSize(200);
            } else {
                $("#grafik" + no).highcharts().setSize(0);
            }
            return true;
        }
        $.ajax({
            url: "<?= base_url() ?>index.php/Aircraft_Service/view_grafik",
            type: "POST",
            data: {
                'start': start,
                'end': end,
                'tipe': tipe,
            },
            dataType: "JSON",
            beforeSend: function () {
                $("#search").prop('disabled', true).html("<i class='fa fa-refresh fa-spin'></i> Please Wait ..");
                // 1
                $("#grafik1Responsive").fadeIn("fast");
                $("#loader1").fadeIn("fast");
                // 2
                $("#grafik2Responsive").fadeIn("fast");
                $("#loader2").fadeIn("fast");
                // 3
                $("#grafik3Responsive").fadeIn("fast");
                $("#loader3").fadeIn("fast");
                // 4
                $("#grafik4Responsive").fadeIn("fast");
                $("#loader4").fadeIn("fast");
                // 5
                $("#grafik5Responsive").fadeIn("fast");
                $("#loader5").fadeIn("fast");
                // 6
                $("#grafik6Responsive").fadeIn("fast");
                $("#loader6").fadeIn("fast");
                // 7
                $("#grafik7Responsive").fadeIn("fast");
                $("#loader7").fadeIn("fast");
                // 8
                $("#grafik8Responsive").fadeIn("fast");
                $("#loader8").fadeIn("fast");
                //9
                $("#grafik9Responsive").fadeIn("fast");
                $("#loader9").fadeIn("fast");
            },
            success: function (data) {
                $("#search").prop('disabled', false).html('<i class="fa fa-search"></i> Search');
                if (data.grafik[0].data.length < 1) {
                    // notif("warning", "Data not found");
                    //   // swal({
                    //   //   type: 'error',
                    //   //   title: 'Oops...',
                    //   //   text: 'Something went wrong!',
                    //   //   footer: '<a href>Why do I have this issue?</a>'
                    //   // }, function(){
                    //   //
                    //   // })
                }
                
                if (tipe == "A333") {
                    colorGrafik = "#93648d";
                    if (data.grafik[0].data.every(isBelowThreshold)) {
                    //     $("#grafik1Responsive").hide();
                    //     $("#loader1").fadeOut("fast");
                    // } else {
                        $("#grafik1Responsive").show();
                        $("#grafik1Responsive").css('height', '100%');
                    }
                        $("#loader1").fadeOut("fast");
                }
                 else if (tipe == "B7M8") {
                    colorGrafik = "#77c3d9";
                    if (data.grafik[0].data.every(isBelowThreshold)) {
                    //     $("#grafik2Responsive").hide();
                    //     $("#loader2").fadeOut("fast");
                    // } else {
                        $("#grafik9Responsive").show();
                        $("#grafik9Responsive").css('height', '100%');
                    }
                        $("#loader9").fadeOut("fast");
                }
                else if (tipe == "A332") {
                    colorGrafik = "#4cc3d9";
                    if (data.grafik[0].data.every(isBelowThreshold)) {
                    //     $("#grafik2Responsive").hide();
                    //     $("#loader2").fadeOut("fast");
                    // } else {
                        $("#grafik2Responsive").show();
                        $("#grafik2Responsive").css('height', '100%');
                    }
                        $("#loader2").fadeOut("fast");
                }
                else if (tipe == "ATR72") {
                    colorGrafik = "#05354d";
                    if (data.grafik[0].data.every(isBelowThreshold)) {
                    //     $("#grafik3Responsive").hide();
                    //     $("#loader3").fadeOut("fast");
                    // } else {
                        $("#grafik3Responsive").show();
                        $("#grafik3Responsive").css('height', '100%');
                    }
                        $("#loader3").fadeOut("fast");
                }
                else if (tipe == "B777") {
                    colorGrafik = "#1f7f91";
                    if (data.grafik[0].data.every(isBelowThreshold)) {
                    //     $("#grafik4Responsive").hide();
                    //     $("#loader4").fadeOut("fast");
                    // } else {
                        $("#grafik4Responsive").show();
                        $("#grafik4Responsive").css('height', '100%');
                    }
                        $("#loader4").fadeOut("fast");
                }
                else if (tipe == "B744") {
                    colorGrafik = "#074b6d";
                    if (data.grafik[0].data.every(isBelowThreshold)) {
                    //     $("#grafik5Responsive").hide();
                    //     $("#loader5").fadeOut("fast");
                    // } else {
                        $("#grafik5Responsive").show();
                        $("#grafik5Responsive").css('height', '100%');
                    }
                        $("#loader5").fadeOut("fast");
                }
                else if (tipe == "B738") {
                    colorGrafik = "#7bc8a4";
                    if (data.grafik[0].data.every(isBelowThreshold)) {
                    //     $("#grafik6Responsive").hide();
                    //     $("#loader6").fadeOut("fast");
                    // } else {
                        $("#grafik6Responsive").show();
                        $("#grafik6Responsive").css('height', '100%');
                    }
                        $("#loader6").fadeOut("fast");
                }
                else if (tipe == "B735") {
                    colorGrafik = "#d18700";
                    if (data.grafik[0].data.every(isBelowThreshold)) {
                    //     $("#grafik7Responsive").hide();
                    //     $("#loader7").fadeOut("fast");
                    // } else {
                        $("#grafik7Responsive").show();
                        $("#grafik7Responsive").css('height', '100%');
                    }
                        $("#loader7").fadeOut("fast");
                }
                else if (tipe == "CRJ1000") {
                    colorGrafik = "#ffdd9e";
                    if (data.grafik[0].data.every(isBelowThreshold)) {
                    //     $("#grafik8Responsive").hide();
                    //     $("#loader8").fadeOut("fast");
                    // } else {
                        $("#grafik8Responsive").show();
                        $("#grafik8Responsive").css('height', '100%');
                        $("#loader8").fadeOut("fast");
                    }
                     $("#loader8").fadeOut("fast");
                }
                chart = new Highcharts.chart('grafik' + no, {
                    chart: {
                        height: 300
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                      text: tipe
                        // useHTML: true,
                        // text: '<a class="hovertest" style="cursor:pointer; text-decoration: underline;" data-toggle="tooltip" title="Click for zoom" href="javascript:void(0)" onclick="zoom_grafik(\'' + tipe + '\')">' + tipe + '</a>'
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        min: data.grafik[0].minimal,
                        max: 200,
                        title: {
                            text: 'Percent (%)'
                        },
                        // plotLines: [{
                        // value: data.target,
                        // color: 'red',
                        // dashStyle: 'shortdash',
                        // width: 2,
                        // label: {
                        // text: 'Target '+data.target,
                        // }
                        // }],
                    },
                    xAxis: {
                        categories: data.grafik[0].datat
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal',
                    },


                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                        }
                    },

                    series: [{
                        name: tipe,
                        data: data.grafik[0].data,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    detail_grafik(tipe, this.category)
                                }
                            }
                        },
                        color: colorGrafik,
                        fillColor: {
                            linearGradient: [0, 0, 0, 140],
                            stops: [
                                [0, colorGrafik],
                                [1, "#fff3"]
                            ]
                        },
                        lineWidth: 1,
                        type: 'areaspline'
                    },
                        {
                            name: "Target",
                            data: data.grafik[0].datap,
                            color: "#fd9214",
                            lineWidth: 1,
                            marker: {
                                enabled: false
                            },
                            type: 'line'
                        }
                    ],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal',
                                }
                            }
                        }]
                    }
                }
                , function (chart) {
                    // $('.highcharts-title').click(function () {
                    //     $("#grafik_down").hide();
                    //     $("#grafik_big").show();
                    //     $("#status_view").val('1');
                    //     $("#tipe_view").val($(this).text());
                    //     // console.log(this);
                    //
                    // });
                });


                var series = chart.series[0];

                series.color = colorGrafik;
                console.log(colorGrafik);
                // series.graph.attr({
                //     stroke: colorGrafik
                // });

                chart.legend.colorizeItem(series, series.visible);
                $.each(series.data, function (i, point) {
                    // point.graphic.attr({
                    //     fill: colorGrafik
                    // });
                });
                series.redraw();
                handleData(data.grafik[0].data.length);
            }
        });
    }

    function zoom() {
        alert(1)
    }

    $(".knob").knob({
        draw: function () {

            // "tron" case
            if (this.$.data('skin') == 'tron') {

                var a = this.angle(this.cv)  // Angle
                    , sa = this.startAngle          // Previous start angle
                    , sat = this.startAngle         // Start angle
                    , ea                            // Previous end angle
                    , eat = sat + a                 // End angle
                    , r = true;

                this.g.lineWidth = this.lineWidth;

                this.o.cursor
                && (sat = eat - 0.3)
                && (eat = eat + 0.3);

                if (this.o.displayPrevious) {
                    ea = this.startAngle + this.angle(this.value);
                    this.o.cursor
                    && (sa = ea - 0.3)
                    && (ea = ea + 0.3);
                    this.g.beginPath();
                    this.g.strokeStyle = this.previousColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                    this.g.stroke();
                }
                this.g.beginPath();
                this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                this.g.stroke();

                this.g.lineWidth = 2;
                this.g.beginPath();
                this.g.strokeStyle = this.o.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                this.g.stroke();
                return false;
            }
        }
    });

    function isBelowThreshold(currentValue) {
        return currentValue == 0;
    }

    function top_remarks3(tipe){
      $("#modal-detail-panel4").modal('show');
      $('#title-detail-panel4').css('color', '#fff');
      $('#header-detail-panel4').css('background-color', '#0f2233');
      $("#title-detail-panel4").html('<i class="fa fa-list-ol"></i>  TOP 3 TROUBLE '+tipe);
      var date_start = $("#date_start").val();
      var date_end = $("#date_end").val();
      $("#detail-table-topremarks").hide();

      $("#detail-table-topremarks").DataTable().clear().draw();
      $('#detail-table-topremarks').DataTable().destroy();
      $.ajax({
        url: '<?= base_url() ?>index.php/Aircraft_Service/datatable_top_trouble',
        type: 'POST',
        dataType: 'JSON',
        data:{
          tipe: tipe,
          date_start: date_start,
          date_end: date_end
        },
        beforeSend: function(){
          $("#loader98").fadeIn("fast");
        }
      })
      .done(function(data) {
        $("#loader98").fadeOut("fast");
        $("#detail-table-topremarks").show();
        var no = 1;
        $.each(data, function(index, list){
          var row = $("#detail-table-topremarks").DataTable().row.add(
            [
              "<center>"+no+"</center>",
              list.kejadian,
              "<center>"+list.total+"</center>"
            ]
          ).draw().node();
          no++;
        })

        notif('success', 'Data '+tipe+' successfully displayed');
      })
      .fail(function() {
        console.log("error");
      })
      .always(function(data) {
        console.log(data);
      });


      // table = $('#detail-table-topremarks').DataTable({
      //               "ajax": {
      //                   "type":'POST',
      //                   "url": '<?= base_url() ?>index.php/Aircraft_Service/datatable_top_trouble',
      //                   "data": {
      //                       "tipe": tipe,
      //                       "date_start": date_start,
      //                       "date_end": date_end
      //                   },
      //
      //               },
      //               "columns": [
      //                   {"data": "NO"},
      //                   {"data": "CATEGORY"},
      //                   {"data": "ACCIDENT"}
      //               ]
      //           });



    }

    $("#start_rows, #end_rows").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            notif('warning','Only Number');
            return false;
        }
    });

    $('#form-upload-service').submit(function (e) { 
            e.preventDefault();
            var formData=new FormData($('#form-upload-service')[0]);
            $.ajax({
                url: "<?= base_url() ?>index.php/Aircraft_Service/storefile",
                type: "POST",
                processData: false,
                contentType: false,
                data: formData,                
                beforeSend: function () {
                    $('#btn-upload-data').html('<i class="fa fa-spinner fa-spin"></i> Processing').attr('disabled',true);
                }
            })
            .done(function(resp){
                var data= JSON.parse(resp);
                 notif(data.info, data.msg);
                 if(data.info=='success'){
                     $('#modal_upload').modal('hide');
                 }
            })
            .fail(function (){
                notif('error', 'Somethings Eror Connections');
            })
            .always(function(){
                $('#btn-upload-data').html('<i class="fa fa-save"></i> Save ').attr('disabled',false);
                $("#form-upload-service")[0].reset();
            });
            
        });

</script>
