<script src="<?= base_url() ?>assets/plugins/rounded-corner/rounded-corner.js"></script>

<style>
    .wadah {
        padding-left: 5px;
        padding-right: 5px;
    }

    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 18px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }

    .form-group {
        margin-bottom: 8px;
    }

    .box {
        position: relative;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 0px;
        width: 100%;
        box-shadow: 0 8px 12px 4px rgba(0, 0, 0, 0.2);
        margin-top: 0px;
    }

    .widget-user .widget-user-header {
        padding: 1px;
        height: 30px;
        border-top-right-radius: 11px;
        border-top-left-radius: 11px;
    }

    .title-manufacture {
        margin-top: 5px;
        text-align: center;
        color: #F2C573;
        font-size: 16px;
    }

    .widget-user .widget-user-image {
        position: absolute;
        top: 5px;
        left: 36%;
    }

    .widget-user .box-footer {
        padding-top: 0px;
        border-bottom-right-radius: 11px;
        border-bottom-left-radius: 11px;
    }

    .h1, .h2, .h3, h1, h2, h3 {
        margin-top: 0px;
        margin-bottom: 10px;
    }

    .description-block {
        display: block;
        margin: 2px -15px;
        text-align: center;
    }

    .title-group {
        text-align: center;
        font-weight: 600;
        font-size: 20px
    }

    .title-label {
        font-size: 11px
    }

    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
        font-size: 20px;
    }

    .label-left {
        margin-left: -10px;
    }

    /*CSS LOADING*/
    .spinner {
        margin: 25px auto;
        /* margin-bottom: 161px; */
        padding-bottom: -13px;
        /* bottom: 100px; */
        width: 50px;
        height: 40px;
        text-align: center;
        font-size: 10px;
    }

    .spinner > div {
        background-color: #333;
        height: 100%;
        width: 6px;
        display: inline-block;

        -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
        animation: sk-stretchdelay 1.2s infinite ease-in-out;
    }

    .spinner .rect2 {
        -webkit-animation-delay: -1.1s;
        animation-delay: -1.1s;
    }

    .spinner .rect3 {
        -webkit-animation-delay: -1.0s;
        animation-delay: -1.0s;
    }

    .spinner .rect4 {
        -webkit-animation-delay: -0.9s;
        animation-delay: -0.9s;
    }

    .spinner .rect5 {
        -webkit-animation-delay: -0.8s;
        animation-delay: -0.8s;
    }

    @-webkit-keyframes sk-stretchdelay {
        0%,
        40%,
        100% {
            -webkit-transform: scaleY(0.4)
        }
        20% {
            -webkit-transform: scaleY(1.0)
        }
    }

    @keyframes sk-stretchdelay {
        0%,
        40%,
        100% {
            transform: scaleY(0.4);
            -webkit-transform: scaleY(0.4);
        }
        20% {
            transform: scaleY(1.0);
            -webkit-transform: scaleY(1.0);
        }
    }
</style>

<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?> <span id="sub_title" style="font-weight: 15;"></span> </h3>
            <div class="box-tools pull-right">
              <label class="control-label" style="display: inline-block;
              max-width: 100%;
              margin-bottom: 5px;
              font-weight: 700;font-size: 14px;">
                  <i class="fa  fa-calendar"></i> Year To Date : &nbsp;
              </label>
              <label style="display: inline-block;
              max-width: 100%;
              margin-bottom: 5px;
              font-weight: 700;font-size: 14px;" class="control-label pull-right">
                <?= date("d")."-".convert_month(date("m"))."-".date("Y") ?>
              </label>
                <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button> -->
            </div>
        </div>
        <div class="box-body">
            <div id="loadingApprove2" class="back-loading"
                 style="background: white;position: absolute;z-index: 9999;width: 100%;height: 100%;margin: 0px auto;left: 0px;right: 0px;bottom: 0px;">
                <div id="loading" name="loading"
                     style="background-color: white;z-index: 9999;height: 140px;margin: 0 auto;    left: 0;right: 0;bottom: 0;">
                    <h4 style="text-align:  center;margin-top:  50px;font-size: 20px;">Loading Data</h4>
                    <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="content-kpi-corp"></div>
            </div>
        </div>
    </div>
</section>


<script>
    var yAxisLimit = [0, 25, 50, 75, 100, 125, 150];
    var base_url = '<?php echo base_url(); ?>index.php/';
    var date = new Date();
    var month = [];
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";
    var bulan = month[(date.getUTCMonth() == 0) ? 11 : (date.getUTCMonth() - 1)];
    var bulan_jalan = month[date.getUTCMonth()];
    var tahun = (date.getUTCMonth() == 0) ? date.getUTCFullYear() - 1 : date.getUTCFullYear();
    $(document).ready(function () {
        load_data();
    });

    function load_data() {
        var content = '';
        $.ajax({
            url: base_url + 'c_kpi_corporate/calculate',
            type: "GET",
            dataType: "JSON",
            beforeSend: function () {
                $("#loadingApprove2").show('slow/400/fast', function () {
                });
                $("#content-kpi-corp").hide('slow/400/fast', function () {
                });
            },
            error: function () {
                notif('error', 'Connection disorders occurred, Please try again');
            },
            success: function (data) {
              $("#sub_title").html('&nbsp; ( Total Score: '+data.totalScore+'% )');
                $("#loadingApprove2").hide('slow/400/fast', function () {
                });
                $("#content-kpi-corp").show('slow/400/fast', function () {
                });
                notif('success', 'Data successfully displayed');
                $.each(data.data, function (key, val) {
                    if (val.group.name == 'Financial') {
                        var icon = 'fa fa-dollar';
                    } else if (val.group.name == 'Customer') {
                        var icon = 'fa fa-user';
                    } else if (val.group.name == 'Internal Process') {
                        var icon = 'fa fa-gear';
                    } else if (val.group.name == 'Learning & Growth') {
                        var icon = 'fa fa-file-text-o';
                    }
                    content += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">' +
                        '<div class="box box-primary">' +
                        '<div class="box-header with-border">' +
                        '<h3 class="box-title"><i class="' + icon + '"></i>' + ' ' + val.group.name + '</h3>' +
                        '</div>' +
                        '<div class="box-body">';
                    if (val.group.data.length == 0) {
                        content += '<p style="margin-left: 20px;">No Data Available</p>';
                    } else {
                        $.each(val.group.data, function (keyz, valz) {
                            // grafik(valz.name);
                            grafik_edited(valz.name,valz.chart.target.isStabilize);
                            var growth_monthly = (valz.monthly.actual - valz.monthly.last_year_date) / valz.monthly.last_year_date;

                            if (isNaN(growth_monthly)) {
                                var con_growth_monthly = 0;
                            } else if (growth_monthly == 'Infinity') {
                                var con_growth_monthly = 0;
                            } else {
                                var con_growth_monthly = growth_monthly;
                            }

                            var growth_ytd = (((valz.ytd.actual_ytd - valz.ytd.last_year_date) / valz.ytd.last_year_date == undefined) ? 0 : (valz.ytd.actual_ytd - valz.ytd.last_year_date) / valz.ytd.last_year_date);
                            if (isNaN(growth_ytd)) {
                                var con_growth_ytd = 0;
                            } else if (growth_ytd == 'Infinity') {
                                var con_growth_ytd = 0;
                            } else if (growth_ytd == Infinity) {
                                var con_growth_ytd = 0;
                            } else if (growth_ytd == undefined) {
                                var con_growth_ytd = 0;
                            } else if (isFinite(growth_ytd)) {
                                var con_growth_ytd = 0;
                            } else {
                                var con_growth_ytd = ((((valz.ytd.actual_ytd - valz.ytd.last_year_date) / valz.ytd.last_year_date) == undefined) ? 0 : (valz.ytd.actual_ytd - valz.ytd.last_year_date) / valz.ytd.last_year_date);
                            }
                            var target_monthly = (valz.monthly.target == null) ? 0 : valz.monthly.target;
                            var target_ytd = (valz.ytd.target_ytd == null) ? 0 : valz.ytd.target_ytd;
                            var actual_monthly = (valz.monthly.actual == null) ? 0 : valz.monthly.actual;
                            var actual_ytd = (valz.ytd.actual_ytd == null) ? 0 : valz.ytd.actual_ytd;
                            var ach_monthly = (valz.monthly.achievment == null) ? 0 : valz.monthly.achievment;
                            var ach_ytd = (valz.ytd.achievment_ytd == null) ? 0 : valz.ytd.achievment_ytd;
                            var last_year_monthly = (valz.monthly.last_year_date == null) ? 0 : valz.monthly.last_year_date;
                            var last_year_ytd = (valz.ytd.last_year_date == null) ? 0 : valz.ytd.last_year_date;
                            
                            
                          
                                con_growth_monthly=actual_monthly-last_year_monthly;
                                con_growth_ytd=actual_ytd-last_year_ytd;
                           


                            content += '<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">' +
                                '<div class="box box-success">' +
                                '<div class="box-header with-border">' +
                                '<h3 class="box-title"><i class="fa fa-info-circle"></i>' + ' ' + valz.name + '</h3>' +
                                '</div>' +
                                '<div class="box-body">' +
                                '<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;padding-right: 8px;">' +
                                '<div class="box box-widget widget-user" style="border-radius: 11px">' +
                                '<div class="widget-user-header bg-navy">' +
                                '<h3 class="title-manufacture"><i class="fa fa-calendar"></i> Monthly</h3>' +
                                '</div>' +
                                '<div class="box-footer">' +
                                '<div class="row">' +
                                '<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">' +
                                '<div class="col-sm-4 border-right" style="border-bottom: 1px solid #f4f4f4;">' +
                                '<div class="description-block">' +
                                '<span class="description-text title-label">Target</span>' +
                                '<div class="col-lg-12">' +
                                '<label class="label-panel label-left" style="color: #F2c573;">' + target_monthly + '%</label>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-4 border-right" style="border-bottom: 1px solid #f4f4f4;">' +
                                '<div class="description-block">' +
                                '<span class="description-text title-label">Actual</span>' +
                                '<div class="col-lg-12">' +
                                '<label class="label-panel label-left" style="color: '+((actual_monthly >= parseInt(target_monthly)) ? '#8EC3A7;' : '#F24738;')+'">' + actual_monthly + '%</label>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-4" style="border-bottom: 1px solid #f4f4f4;">' +
                                '<div class="description-block">' +
                                '<span class="description-text title-label">Achievement</span>' +
                                '<div class="col-lg-12">' +
                                // '<label class="label-panel label-left" style="color: '+((actual_monthly >= 100) ? '#8EC3A7;' : '#F24738;')+'">' + ach_monthly + '%</label>' +
                                '<label class="label-panel label-left" style="color: '+((actual_monthly >= parseInt(target_monthly)) ? '#8EC3A7;' : '#F24738;')+'">' + ach_monthly + '%</label>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-8 border-right">' +
                                '<div class="description-block">' +
                                '<span class="description-text title-label">Last Year Date</span>' +
                                '<div class="col-lg-12">' +
                                '<label class="label-panel label-left" style="color: #8EC3A8;">' + last_year_monthly + '%</label>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-4">' +
                                '<div class="description-block">' +
                                '<span class="description-text title-label">Growth</span>' +
                                '<div class="col-lg-12">' +
                                '<label class="label-panel" style="color: #0f2233;">' + con_growth_monthly + '%</label>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="padding-left: 8px;padding-right: 0px;">' +
                                '<div class="box box-widget widget-user" style="border-radius: 11px">' +
                                '<div class="widget-user-header bg-navy">' +
                                '<h3 class="title-manufacture"><i class="fa fa-calendar"></i> Year to Date</h3>' +
                                '</div>' +
                                '<div class="box-footer">' +
                                '<div class="row">' +
                                '<div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12">' +
                                '<div class="col-sm-4 border-right" style="border-bottom: 1px solid #f4f4f4;">' +
                                '<div class="description-block">' +
                                '<span class="description-text title-label">Target</span>' +
                                '<div class="col-lg-12">' +
                                '<label class="label-panel label-left" style="color: #F2c573;">' + target_ytd + '%</label>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-4 border-right" style="border-bottom: 1px solid #f4f4f4;">' +
                                '<div class="description-block">' +
                                '<span class="description-text title-label">Actual</span>' +
                                '<div class="col-lg-12">' +
                                '<label class="label-panel label-left" style="color: '+((actual_ytd >= parseInt(target_ytd)) ? '#8EC3A7;' : '#F24738;')+'">' + actual_ytd + '%</label>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-4" style="border-bottom: 1px solid #f4f4f4;">' +
                                '<div class="description-block">' +
                                '<span class="description-text title-label">Achievement</span>' +
                                '<div class="col-lg-12">' +
                                '<label class="label-panel label-left" style="color: '+((actual_ytd >= parseInt(target_ytd)) ? '#8EC3A7;' : '#F24738;')+'">' + ach_ytd + '%</label>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-8 border-right">' +
                                '<div class="description-block">' +
                                '<span class="description-text title-label">Last Year Date</span>' +
                                '<div class="col-lg-12">' +
                                '<label class="label-panel label-left" style="color: #8EC3A8;">' + last_year_ytd + '%</label>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-4">' +
                                '<div class="description-block">' +
                                '<span class="description-text title-label">Growth</span>' +
                                '<div class="col-lg-12">' +
                                '<label class="label-panel" style="color: #0f2233;">' + con_growth_ytd + '%</label>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-lg-12">' +
                                '<br>' +
                                '<div id="grafik-' + valz.name + '" style="width:100%;height:250px"></div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';

                        });
                    }
                    content += '</div>' +
                        '</div>' +
                        '</div>';
                });
                $('#content-kpi-corp').html(content);
            }
        });
    }

    function grafik_edited(inc,stablize) {
        var tampung_bulan = [];
        for (var i = 0; i <= date.getUTCMonth(); i++) {
            var arr = month[i] + '-' + tahun;
            tampung_bulan.push(arr)
        }
        $.ajax({
            url: base_url + 'c_kpi_corporate/getGrafikValue',
            type: "POST",
            data:{
                names:inc
            },
            success: function (resp) {
                var datas=JSON.parse(resp);
                console.log(datas.actual);
               if (stablize == false) {
                    var chart = new Highcharts.chart('grafik-' + inc, {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: ''
                        },
                        credits: {
                            enabled: false
                        },
                        exporting: {
                            enabled: false
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Percentage(%)'
                            },
                            // tickPositioner: function () {
                            //     return yAxisLimit;
                            // },
                        },
                        xAxis: {
                            categories: tampung_bulan,
                        },
                        credits: {
                            enabled: false
                        },
                        // tooltip: {
                        //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        //         '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                        //     footerFormat: '</table>',
                        //     shared: true,
                        //     useHTML: true
                        // },
                        plotOptions: {
                            column: {
                                pointPadding: 0,
                                groupPadding: 0.2,
                                borderRadiusTopLeft: '3px',
                                borderRadiusTopRight: '3px'
                            },
                        },
                        series: [{
                            name: 'Target',
                            data: datas.target,
                            color: '#f2c573'

                        }, {
                            name: 'Actual',
                            data: datas.actual,
                            color: '#0f2233'

                        }, {
                            name: 'Actual YTD',
                            data: datas.actualYtd,
                            color: '#8ec3a7',
                            type: 'line'

                        }],
                    });
                } else {
                    var chart = new Highcharts.chart('grafik-' + inc, {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: ''
                        },
                        credits: {
                            enabled: false
                        },
                        exporting: {
                            enabled: false
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Percentage(%)'
                            },
                            // tickPositioner: function () {
                            //     return yAxisLimit;
                            // },
                        },
                        xAxis: {
                            categories: tampung_bulan,
                        },
                        credits: {
                            enabled: false
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0,
                                groupPadding: 0.2,
                                borderRadiusTopLeft: '3px',
                                borderRadiusTopRight: '3px'
                            },
                        },
                        series: [ {
                            name: 'Target Upper',
                            data: datas.target.up,
                            color: '#fd9214'

                        },
                        {
                            name: 'Target Lower',
                            data: datas.target.low,
                            color: '#ffc44f'

                        },
                        {
                            name: 'Actual',
                            data: datas.actual,
                            color: '#0f2233'

                        }, {
                            name: 'Actual YTD',
                            data: datas.actualYtd,
                            color: '#8ec3a7',
                            type: 'line'
                        }],
                    });
                }
            }
        });
    }

    function grafik(inc) {
        var tampung_bulan = [];
        for (var i = 0; i <= date.getUTCMonth(); i++) {
            var arr = month[i] + '-' + tahun;
            tampung_bulan.push(arr)
        }
        $.ajax({
            url: base_url + 'c_kpi_corporate/calculate',
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $.each(data.data, function (key, val) {
                    $.each(val.group.data, function (keyz, valz) {
                        if (inc == valz.name) {
                            if (valz.chart.target.isStabilize == false) {
                                var chart = new Highcharts.chart('grafik-' + inc, {
                                    chart: {
                                        type: 'column'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    credits: {
                                        enabled: false
                                    },
                                    exporting: {
                                        enabled: false
                                    },
                                    yAxis: {
                                        min: 0,
                                        title: {
                                            text: 'Percentage(%)'
                                        },
                                        // tickPositioner: function () {
                                        //     return yAxisLimit;
                                        // },
                                    },
                                    xAxis: {
                                        categories: tampung_bulan,
                                    },
                                    credits: {
                                        enabled: false
                                    },
                                    // tooltip: {
                                    //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                    //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                    //         '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                                    //     footerFormat: '</table>',
                                    //     shared: true,
                                    //     useHTML: true
                                    // },
                                    plotOptions: {
                                        column: {
                                            pointPadding: 0,
                                            groupPadding: 0.2,
                                            borderRadiusTopLeft: '3px',
                                            borderRadiusTopRight: '3px'
                                        },
                                    },
                                    series: [{
                                        name: 'Target',
                                        data: data.target,
                                        color: '#f2c573'

                                    }, {
                                        name: 'Actual',
                                        data: data.actual,
                                        color: '#0f2233'

                                    }, {
                                        name: 'Actual YTD',
                                        data: data.actualYtd,
                                        color: '#8ec3a7',
                                        type: 'line'

                                    }],
                                });
                            } else {
                                var chart = new Highcharts.chart('grafik-' + inc, {
                                    chart: {
                                        type: 'column'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    credits: {
                                        enabled: false
                                    },
                                    exporting: {
                                        enabled: false
                                    },
                                    yAxis: {
                                        min: 0,
                                        title: {
                                            text: 'Percentage(%)'
                                        },
                                        // tickPositioner: function () {
                                        //     return yAxisLimit;
                                        // },
                                    },
                                    xAxis: {
                                        categories: tampung_bulan,
                                    },
                                    credits: {
                                        enabled: false
                                    },
                                    tooltip: {
                                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                        '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                                        footerFormat: '</table>',
                                        shared: true,
                                        useHTML: true
                                    },
                                    plotOptions: {
                                        column: {
                                            pointPadding: 0,
                                            groupPadding: 0.2,
                                            borderRadiusTopLeft: '3px',
                                            borderRadiusTopRight: '3px'
                                        },
                                    },
                                    series: [ {
                                        name: 'Target Upper',
                                        data: valz.chart.target.upper_target,
                                        color: '#fd9214'

                                    },
                                    {
                                        name: 'Target Lower',
                                        data: valz.chart.target.lower_target,
                                        color: '#ffc44f'

                                    },
                                    {
                                        name: 'Actual',
                                        data: valz.chart.actual,
                                        color: '#0f2233'

                                    }, {
                                        name: 'Actual YTD',
                                        data: valz.chart.actualYtd,
                                        color: '#8ec3a7',
                                        type: 'line'
                                    }],
                                });
                            }
                        }
                    });
                });
            }
        });
    }
</script>
