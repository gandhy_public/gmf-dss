<style>
    .wadah {
        padding-top: 0px;
        padding-left: 10px;
        padding-right: 10px;
    }
    a:hover {
        background-color: #ffffff;
        color: #0e0e0e;
    }
    .hyperlink:hover {
        color: #FFFFFF;
    }
    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 10px;
        width: 100%;
        box-shadow: 0 8px 12px 4px rgba(0,0,0,0.2);
    }
</style>
<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <h3 class="pull-right" style="text-align: center;margin-top: 0px;margin-bottom: 5px;"><?= $title ?></h3>
                        </div>
                    </div>
                    <span class="pull-right" id="date"></span>                    
                    <div class="col-lg-12 col-md-10 col-sm-12 col-xs-12">
                        <div id="grafik_utama"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <!-- FILTER GRAFIK -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label class="col-lg-5">Start Month</label>
                            <div class="col-lg-7" style="padding-left: 0px;padding-right: 0px;">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="date_start">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label class="col-lg-5">End Month</label>
                            <div class="col-lg-7" style="padding-left: 0px;padding-right: 0px;">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="date_end">
                                </div>                               
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <button class="btn btn-block btn-flat bg-navy" id="pencarian" style="margin-top: 0px;">
                                <i class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </div>
                </div>

                <!-- GRAFIK GA DAN NON GA-->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" onclick="detail('GA')">
                        <div id="chart-ga" style="margin-top: -15px"></div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" onclick="detail('NON-GA')">
                        <div id="chart-non-ga" style="margin-top: -15px"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>



<!-- Start JScript -->
<script type="text/javascript">
    var date = new Date();
    var month = [];
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var tgl = date.getDate();
    var bulan = month[date.getUTCMonth()];
    var tahun = date.getUTCFullYear();
    var bulan1 = (date.getMonth() - 1, 1);
    var tahun1 = date.getFullYear();
    var bulan2 = date.getMonth() + 1;
    var tahun2 = date.getFullYear();
    newDate1 = bulan1 + '-' + tahun1;
    newDate2 = bulan2 + '-' + tahun2;
    var yAxisLimit = [0, 25, 50, 75, 100];

    $(document).ready(function () {

        $('#date').html('<i class="fa  fa-calendar"></i> YTD : ' + tgl + ' ' + bulan + ' ' + tahun);
        $('#date_start').val(newDate1);
        $('#date_end').val(newDate2);
        $('#date_end, #date_start').datepicker({
            todayBtn: "linked",
            format: 'mm-yyyy',
            viewMode: 'months',
            minViewMode: 'months',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
        grafik_utama();
        grafik_ga();
        grafik_non_ga();
        search();
        // $(function(){
        //     $('#modal-ga').on('hidden.bs.modal', function (e) {
        //         $('#modal-ga').remove();
        //         $('#box-garuda').html("");
        //     });
        //     $('#modal-non-ga').on('hidden.bs.modal', function (e) {
        //         $('#modal-non-ga').remove();
        //         $('#box-non-garuda').html("");
        //     });
        // });
         $('#modal').on('hidden.bs.modal', function (e) {
                // $('#modal-garuda').remove();
                $('#box').html("");
            });
    });


function search(){
        $('#pencarian').click(function (e) {
                    $("#wadah-detail").hide();
        $("#box-garuda").hide();
        $("#box-non-garuda").hide();
            var tgl1 = $('#date_start').val();
            var tgl2 = $('#date_end').val();
            var get_year1 = tgl1.split('-')[1];
            var get_year2 = tgl2.split('-')[1];
            var get_month1 = tgl1.split('-')[0];
            var get_month2 = tgl2.split('-')[0];

            if (get_year1 == get_year2 && get_month1 == get_month2) {
                grafik_utama();
                grafik_ga();
                grafik_non_ga();
            } else if (get_year1 == get_year2 && get_month1 < get_month2) {
                grafik_utama();
                grafik_ga();
                grafik_non_ga();
            }
             else if (get_year1 == get_year2 && get_month1 > get_month2) {
                grafik_utama();
                grafik_ga();
                grafik_non_ga();
            } else {
                notif('error', "Sorry can't, check your date");
            }
        });

}

    function grafik_utama() {
        var date_start = $("#date_start").val();
        var date_end = $("#date_end").val();
        $.ajax({
            url: "<?= base_url(); ?>index.php/kpi_slabaseops/get_chart_all",
            type: 'POST',
            data: {
                'date_start': date_start,
                'date_end': date_end
            },
            dataType: "JSON",
            success: function (data) {
                notif('success', 'Data successfully displayed');
                Highcharts.chart('grafik_utama', {
                    chart: {
                        height: 150,
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        title: {
                            text: 'Percentage (%)'
                        },
                        // tickPositioner: function () {
                        //     return yAxisLimit;
                        // },
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },
                    series: [
                    {
                        name: data.series[0].name,
                        data: data.series[0].data,
                        color: data.series[0].color,
                    },
                    {
                        type: 'areaspline',
                        fillColor: {
                            linearGradient: [0, 0, 0, 200],
                            stops: [
                            [0, "#05354D"],
                            [1, "#fff3"]
                            ]
                        },
                        lineWidth: 1,
                        name: "Actual",
                        data: data.series[1].data,
                        color: '#05354D',

                    },
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
    }

    function grafik_ga() {
        var date_start  = $("#date_start").val();
        var date_end    = $("#date_end").val();
        $.ajax({
            url: "<?= base_url(); ?>index.php/kpi_slabaseops/get_chart_ga",
            type: 'POST',
            data: {
                'date_start': date_start,
                'date_end': date_end
            },
            dataType: "JSON",
            success: function (data) {
                Highcharts.chart('chart-ga', {
                    chart: {
                        height: 200,
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: "<p class='hovertest' style='cursor:pointer; text-decoration: underline; '>GA</p>",
                        margin: 0
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        title: {
                            text: 'Percentage (%)'
                        },
                        // tickPositioner: function () {
                        //     return yAxisLimit;
                        // },
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            cursor: 'pointer'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },
                    series: [{
                        name: data.series[0].name,
                        data: data.series[0].data,
                        color: data.series[0].color
                    },
                    {
                        type: 'areaspline',
                        fillColor: {
                            linearGradient: [0, 0, 0, 200],
                            stops: [
                            [0, "#05354D"],
                            [1, "#fff3"]
                            ]
                        },
                        lineWidth: 1,
                        name: "Actual",
                        data: data.series[1].data,
                        color: "#05354D"
                    }],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }
                });
            }
        });
    }

    function grafik_non_ga() {
        var date_start  = $("#date_start").val();
        var date_end    = $("#date_end").val();
        $.ajax({
            url: "<?= base_url(); ?>index.php/kpi_slabaseops/get_chart_nga",
            type: 'POST',
            data: {
                'date_start': date_start,
                'date_end': date_end
            },
            dataType: "JSON",
            success: function (data) {
                Highcharts.chart('chart-non-ga', {
                    chart: {
                        height: 200,
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                     useHTML: true,
                     text: "<p class='hovertest' style='cursor:pointer; text-decoration: underline; '>NON-GA</p>",
                     margin: 0
                 },
                 credits: {
                    enabled: false
                },
                yAxis: {
                    title: {
                        text: 'Percentage (%)'
                    },
                    // tickPositioner: function () {
                    //     return yAxisLimit;
                    // },
                },
                xAxis: {
                    categories: data.categories,
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        cursor: 'pointer'
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
                series: [{
                    name: data.series[0].name,
                    data: data.series[0].data,
                    color: data.series[0].color,
                },
                {
                    type: 'areaspline',
                    fillColor: {
                        linearGradient: [0, 0, 0, 200],
                        stops: [
                        [0, "#05354D"],
                        [1, "#fff3"]
                        ]
                    },
                    lineWidth: 1,
                    name: "Actual",
                    data: data.series[1].data,
                    color: "#05354D"
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
            }
        });
    }

    function detail(tipe){
        if(tipe == 'GA'){
            $('#modal').modal('show');
            $('#title').text('Detail GA');
            detail_chart(tipe);
        }else{
           $('#modal').modal('show');
           $('#title').text('Detail NON-GA');
           detail_chart(tipe);
       }
   }

   function build_box(id, type) {
    var chart_id = 'chart_'+id;
    var txt1 = '<div class="col-lg-4 col-sm-12 col-md-12 col-xs-12">'+
    '<div class="small-box bg-deafult">'+
    '<div id='+chart_id+'></div>'+
    '</div>'+
    '</div>';
    if (type=="GA") {
      $("#box").append(txt1);
  }else{
      $("#box").append(txt1);
  }
}

function detail_chart(type){
    var date_start  = $("#date_start").val();
    var date_end    = $("#date_end").val(); 
    $("#box-garuda").html("");
    $("#box-non-garuda").html("");    
    if(type == 'GA'){
        $.ajax({
            type: "POST",
            url: "<?= base_url(); ?>index.php/kpi_slabaseops/get_item_chart",
            data: {
              'type': 'SLA Base Operations',
              'date_start' : date_start,
              'date_end' : date_end
          },
          dataType: "json",
          success: function (response) {
            for (var i=0; i<response.length; i++) {
              detail_grafik(i, response.data[i].kia_id, response.data[i].ki_id, response.data[i].ki_name, 'GA');
          }
      }
  });
    }else{
        $.ajax({
            type: "POST",
            url: "<?= base_url(); ?>index.php/kpi_slabaseops/get_item_chart",
            data: {
              'type': 'CSF NGA',
              'date_start' : date_start,
              'date_end' : date_end
          },
          dataType: "json",
          success: function (response) {
            for (var i=0; i<response.length; i++) {
              detail_grafik(i, response.data[i].kia_id, response.data[i].ki_id, response.data[i].ki_name, 'NON-GA');
          }
      }
  });
    }
}


function detail_grafik(div_id, id_item, ki_id, name_graph, air_type){
    var name_graph = name_graph;
    var date_start = $("#date_start").val();
    var date_end = $("#date_end").val(); 
    $.ajax({
      url: "<?= base_url(); ?>index.php/kpi_slabaseops/get_chart_detail",
      type: 'POST',
      data: {
        'id_item': id_item, 
        'ki_id': ki_id,
        'date_start' : date_start,
        'date_end' : date_end
      },
      dataType: "JSON",
            beforeSend: function () {
              $('#loadings').fadeIn(1000);
            },      
      success: function (data) {
        $('#loadings').fadeOut(1000);
        build_box(div_id, air_type)
        
        Highcharts.chart('chart_'+div_id, {
          chart: {
              height: 250,
              type: 'line'
          },
          exporting: {
              enabled: false
          },
          title: {
              text: name_graph
          },
          credits: {
              enabled: false
          },
          yAxis: {
              title: {
                  text: data.title_y
              }
          },
          xAxis: {
              categories: data.categories,
          },
          legend: {
              layout: 'vertical',
              align: 'right',
              verticalAlign: 'middle'
          },

          plotOptions: {
              series: {
                  label: {
                      connectorAllowed: false
                  },
              }
          },
          series:
              data.series
          ,
          responsive: {
              rules: [{
                  condition: {
                      maxWidth: 500
                  },
                  chartOptions: {
                      legend: {
                          layout: 'horizontal',
                          align: 'center',
                          verticalAlign: 'bottom'
                      }
                  }
              }]
          }

        });
      }
    });
  }
</script>


<!-- MODAL DETAIL -->
<div class="modal bounceIn" id="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0f2233;color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-detail" id="title"></h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
          <div id="box"></div>
      </div>
  </div>
</div>      
</div>
</div>
</div>


