
<style>
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin: 0;
  line-height: 1;
}
.box-header {
    color: #444;
    display: block;
    padding: 0px;
    position: relative;
}
.wadah {
    padding-top: 0px;
    padding-left: 10px;
    padding-right: 10px;
}
a:hover {
    background-color: #ffffff;
    color: #0e0e0e;
}
.hyperlink:hover {
    color: #FFFFFF;
}
.box {
    position: relative;
    border-radius: 3px;
    background: #ffffff;
    border-top: 3px solid #d2d6de;
    margin-bottom: 10px;
    width: 100%;
    box-shadow: 0 8px 12px 4px rgba(0,0,0,0.2);
}
</style>
<section class="content">
  <div class="box">
      <div class="box-body">
          <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                      <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                      <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                      <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                          <h3 class="pull-right" style="text-align: center;margin-top: 0px;margin-bottom: 5px;"><?= $title ?></h3>
                      </div>
                  </div>
                  <span class="pull-right" id="date"></span>
                  <div class="col-lg-12 col-md-10 col-sm-12 col-xs-12">
                      <div id="grafik_utama"></div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <div class="box">
    <div class="box-body">
        <div class="row">
            <!-- FILTER GRAFIK -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label class="col-lg-5">Start Month</label>
                            <div class="col-lg-7" style="padding-left: 0px;padding-right: 0px;">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="date_start">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label class="col-lg-5">End Month</label>
                            <div class="col-lg-7" style="padding-left: 0px;padding-right: 0px;">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="date_end" value="<?= date('m-Y'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <button class="btn btn-block btn-flat bg-navy" id="pencarian" style="margin-top: 0px;">
                                <i class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </div>
                </div>

            <!-- GRAFIK GA DAN NON GA-->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" onclick="detail('garuda')">
                    <div id="ga" style="margin-top: -15px"></div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" onclick="detail('citilink')">
                    <div id="non-ga" style="margin-top: -15px"></div>
                </div>
            </div>

        </div>
    </div>
  </div>
</section>



<!-- Start JScript -->
<script type="text/javascript">
    var date = new Date();
    var month = [];
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var tgl = date.getDate();
    var bulan = month[date.getUTCMonth()];
    var tahun = date.getUTCFullYear();
    var bulan1 = (date.getMonth() - 1, 1);
    var tahun1 = date.getFullYear();
    var bulan2 = date.getMonth() + 1;
    var tahun2 = date.getFullYear();
    newDate1 = bulan1 + '-' + tahun1;
    newDate2 = bulan2 + '-' + tahun2;
    var yAxisLimit = [0, 25, 50, 75, 100];

    $(document).ready(function () {

        $('#date').html('<i class="fa  fa-calendar"></i> YTD : ' + tgl + ' ' + bulan + ' ' + tahun);
        $('#date_start').val(newDate1);
        $('#date_end').val(newDate2);
        $('#date_end, #date_start').datepicker({
            todayBtn: "linked",
            format: 'mm-yyyy',
            viewMode: 'months',
            minViewMode: 'months',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

        grafik_utama('general', 'general');
        grafik_utama('garuda', '');
        grafik_utama('citilink', '');

        $(function(){
            $('#modal-garuda').on('hidden.bs.modal', function (e) {
                // $('#modal-garuda').remove();
                $('#box-detail').html("");
            });
            $('#modal-citilink').on('hidden.bs.modal', function (e) {
                // $('#modal-citilink').remove();
                $('#box-detail').html("");
            });
        });

        $('#pencarian').click(function(){
         var start  = $("#date_start").val().split("-");
         var start  = start[1];
         var end    = $("#date_end").val().split("-");
         var end    = end[1];
           if(start!=end){
              notif("warning", "Tahun Harus Sama!!!");
           }else{
          grafik_utama('garuda', '');
          grafik_utama('citilink', '');
           }
        });

    });



  function grafik_utama(aircraft, tipe) {
    var start   = $("#date_start").val();
    var end     = $("#date_end").val();
    if(aircraft =='garuda'){
      var gettitle      = 'ga';
      var tinggi        = 200;
      var tittle_chart  = "<p class='hovertest' style='cursor:pointer; text-decoration: underline;' title='Click for detail'>GARUDA</p>";
      var title_notif   = 'garuda';
    }else if(aircraft =='citilink'){
      var gettitle      = 'non-ga';
      var tinggi        = 200;
      var tittle_chart  = "<p class='hovertest' style='cursor:pointer; text-decoration: underline;' title='Click for detail'>CITILINK</p>";
      var title_notif   = 'citilink';
    }else{
      var gettitle      = 'grafik_utama';
      var tinggi        = 200;
      var tittle_chart  = '';
      var title_notif   = 'line ops';
    }
      $.ajax({
        url: "<?= base_url() ?>index.php/Kpi_slalineops/grafik_parent",
        type: "POST",
        data: {
          'start' : start,
          'end' : end,
          'aircraft' : aircraft,
          'tipe' : tipe,
        },
        dataType: "JSON",
        beforeSend: function () {
           $('#loadings').fadeIn(1000);
        },
        success: function (data) {
          notif('success', 'Data '+title_notif+' successfully displayed');
          var chart = new Highcharts.chart(gettitle, {
            chart: {
              height: tinggi,
              type: 'line'
            },
              title: {
                text: tittle_chart,
                useHTML: true,
                margin: 0
              },
              exporting: {
                enabled: false
              },
              credits: {
                enabled: false
              },
              yAxis: {
                title: {
                  text: 'Percentage (%)'
                },
                // tickPositioner: function () {
                //   return yAxisLimit;
                // },
              },
              xAxis: {
                categories: data.grafik[0].datat
              },
              legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
              },
              series: [
            {
                name: 'Target',
                color: '#fd9214',
                data: data.grafik[0].datag,
              },
            {
                type: 'areaspline',
                fillColor: {
                  linearGradient: [0, 0, 0, 200],
                  stops: [
                    [0, '#05354D'],
                    [1, "#fff3"]
                  ]
                },
                lineWidth: 1,
                name: 'Actual',
                color: '#05354D',
                data: data.grafik[0].data
              }
            ],
              responsive: {
                rules: [{
                  condition: {
                    maxWidth: 500
                  },
                  chartOptions: {
                    legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                    }
                  }
                }]
              }
            },
          );
         $('#loadings').fadeOut(1000);
        }
      });
    }

  function ucwords (str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
      return $1.toUpperCase();
    });
  }

    function detail(tipe){
      if(tipe == 'garuda'){
        // alert('a');
        $('#title').text('Detail ' + ucwords(tipe))
        $('#modal-garuda').modal('show');
        detail_grafik(tipe);
      }else{
        // alert('b');
        $('#title').text('Detail ' + ucwords(tipe))
        $('#modal-garuda').modal('show');
        detail_grafik(tipe);
      }

    }

  function detail_grafik(aircraft){
    var start = $("#date_start").val();
    $.ajax({
      url: "<?= base_url() ?>index.php/Kpi_slalineops/grafik_tot_detail",
      type: "POST",
      data: {
        'aircraft' : aircraft,
        'start' : start,
      },
      beforeSend:function(){
        $("#box-detail").html("");
      },
      dataType: "JSON",
      success: function (data) {
        var list_grafik='';
         for(var i=0;i< data.grafik.length; i++){
           
           list_grafik +='<div class="col-lg-4 col-sm-12 col-md-12 col-xs-12"><div class="small-box bg-deafult">';
           list_grafik += '<div id="grafik'+i+'">';
           list_grafik +='</div></div></div>';
          }
          $("#box-detail").html(list_grafik);
          for(var i=0;i< data.grafik.length; i++){
            grafik_detail_bawah(i, data.grafik[i].kia_id, data.grafik[i].ki_id, data.grafik[i].name);
          }
      }
    });
  }


  function grafik_detail_bawah(urut, kia_id, ki_id, name){
    var start   = $("#date_start").val();
    var end     = $("#date_end").val();

    $.ajax({
      url: "<?= base_url() ?>index.php/Kpi_slalineops/cek_tipe_formula/" + ki_id,
      type: "GET",
      dataType: "JSON",
      success: function (data) {
        if(data.fa_function_name == 'Stabilize'){
          $.ajax({
            url: "<?= base_url() ?>index.php/Kpi_slalineops/grafik_detail",
            type: "POST",
            data: {
              'start'   : start,
              'end'     : end,
              'kia_id'  : kia_id,
              'ki_id'   : ki_id,
              'tipe'    : 0
            },
            dataType: "JSON",
            beforeSend: function () {
              $('#loadings').fadeIn(1000);
            },
            success: function (data) {
              console.log(urut);
              var chart = new Highcharts.chart('grafik'+urut, {
                title: {
                  text:  name
                },
                credits: {
                  enabled: false
                },
                exporting: {
                  enabled:false
                },
                yAxis: {
                  title: {
                    text:  'Percentage (%)'
                  },
                  // tickPositioner: function () {
                  //   return yAxisLimit;
                  // },
                },
                xAxis: {
                  categories: data.date
                },
                legend: {
                  layout: 'vertical',
                  align: 'right',
                  verticalAlign: 'middle'
                },
                series: [
                  {
                    name: "Lower",
                    data: data.lower,
                    color : "#ffc44f"
                  },
                  {
                    name: "Upper",
                    data: data.upper,
                    color : "#fd9214"
                  },
                  {
                    type: 'areaspline',
                    fillColor: {
                      linearGradient: [0,0,0,200],
                      stops: [
                        [0, "#05354D"],
                        [1, "#fff3"]
                      ]
                    },
                    lineWidth: 1,
                    name: "Actual",
                    data: data.actual,
                    color : "#05354D"
                  },
                ],
                responsive: {
                  rules: [{
                    condition: {
                    maxWidth: 500
                    },
                    chartOptions: {
                    legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                    }
                    }
                  }]
                }
              },
            );
            $('#loadings').fadeOut(1000);
            }
          });
        }else{
          $.ajax({
            url: "<?= base_url() ?>index.php/Kpi_slalineops/grafik_detail",
            type: "POST",
            data: {
              'start'   : start,
              'end'     : end,
              'kia_id'  : kia_id,
              'ki_id'   : ki_id,
              'tipe'    : 1
            },
            dataType: "JSON",
            beforeSend: function () {
              $('#loadings').fadeIn(1000);
            },
            success: function (data) {
              var chart = new Highcharts.chart('grafik'+urut, {
                title: {
                  text:  name
                },
                credits: {
                  enabled: false
                },
                exporting: {
                  enabled:false
                },
                yAxis: {
                  title: {
                    text:  data.y_tittle
                  },
                  // tickPositioner: function () {
                  //   return yAxisLimit;
                  // },
                },
                xAxis: {
                  categories: data.date
                },
                legend: {
                  layout: 'vertical',
                  align: 'right',
                  verticalAlign: 'middle'
                },
                series: [
                  {
                    name: "Target",
                    data: data.target,
                    color : "#fd9214"
                  },
                  {
                    type: 'areaspline',
                    fillColor: {
                      linearGradient: [0,0,0,200],
                      stops: [
                        [0, "#05354D"],
                        [1, "#fff3"]
                      ]
                    },
                    lineWidth: 1,
                    name: "Actual",
                    data: data.actual,
                    color : "#05354D"
                  },
                ],
                responsive: {
                  rules: [{
                    condition: {
                    maxWidth: 500
                    },
                    chartOptions: {
                    legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                    }
                    }
                  }]
                }
              },
            );
            $('#loadings').fadeOut(1000);
            }
          });
        }
      }
    });


  }

</script>


<!-- MODAL DETAIL GA -->
<div class="modal bounceIn" id="modal-garuda" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document" style="width: 950px;">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0f2233;color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-detail" id="title">Detail GA</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
          <div id="box-detail"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- MODAL DETAIL CITILINK -->
<div class="modal bounceIn" id="modal-citilink" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document" style="width: 950px;">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0f2233;color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-detail" id="title"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div id="box-detail"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
