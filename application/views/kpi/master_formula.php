<style type="text/css" media="screen">
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin: 0;
  line-height: 1;
}
    #editor {
        position: relative;
        width: 100%;
        height: 350px;
    }
  </style>
<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
        <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-body">
      <form id="formula" action="index.html" method="post">
        <div class="row">
          <div class="col-md-6">
            <div class="box" style="box-shadow: none; border-top: none;">
              <div class="box-body">
                <div class="form-group">
                  <label>Formula Name</label>
                  <input type="text" name="kf_name" id="kf_name" class="form-control" required>
                </div>
                <div class="form-group">
                  <label>Case Name</label>
                  <input type="text" name="fa_function_name" id="fa_function_name" class="form-control" required>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <pre id="editor">
              <?= $load_file ?>
            </pre>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <button type="submit" class="btn btn-flat bg-navy pull-right" id="save" name="button">Save <i class="fa fa-save"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.3.3/ace.js" charset="utf-8"></script>
<script type="text/javascript">
  	ace.require("ace/ext/language_tools");
  	var editor = ace.edit("editor");
  	editor.setTheme("ace/theme/monokai");
  	editor.session.setMode("ace/mode/php");
  	var code = editor.getValue();
  	editor.getSession().on('change', function() {
  		console.log(code);
  	});

    $("#formula").parsley();
    $("#formula").on("submit", function(e){
      e.preventDefault();
      var code = editor.getValue();
      var kf_name = $("#kf_name").val();
      var fa_function_name = $("#fa_function_name").val();
      $.ajax({
        url: '<?= base_url() ?>index.php/Kpi_master_formula/file',
        type: 'POST',
        dataType: 'JSON',
        data: {code: code, kf_name: kf_name, fa_function_name: fa_function_name},
        beforeSend: function(){
          $("#loadings").fadeIn(1000);
        }
      })
      .done(function(data) {
        console.log(data);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        $("#loadings").fadeOut(1000);
      });
    })
</script>
