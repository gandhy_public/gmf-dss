<style media="screen">
  .box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
    display: inline-block;
    font-size: 20px;
    font-weight: bold;
    margin: 0;
    line-height: 1;
  }
  span.select2{
    width: 100% !important;
  }

  option span {
    font-weight: bold;
  }

  .optionChild {
    padding-left: 15px;
  }

    /* Add animation to "page content" */
    .animate-bottom {
      position: relative;
      -webkit-animation-name: animatebottom;
      -webkit-animation-duration: 1s;
      animation-name: animatebottom;
      animation-duration: 1s
    }

    @-webkit-keyframes animatebottom {
      from { bottom:-100px; opacity:0 }
      to { bottom:0px; opacity:1 }
    }

    @keyframes animatebottom {
      from{ bottom:-100px; opacity:0 }
      to{ bottom:0; opacity:1 }
    }

    #myDiv {
      display: none;
      text-align: center;
    }

    .treegrid-indent {
        width: 0px;
        height: 16px;
        display: inline-block;
        position: relative;
    }

    .treegrid-expander {
        width: 0px;
        height: 16px;
        display: inline-block;
        position: relative;
        left:-17px;
        cursor: pointer;
    }
</style>

<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>


<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Management Item KPI</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Year</label>
            <input type="text" class="form-control" id="year" name="year" value="<?= date("Y") ?>" readonly>
            <input type="hidden" class="form-control" id="user_id" name="user_id" value="" readonly>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-2">
          <button type="button" class="btn bg-navy btn-flat form-control" id="addKpi" name="button"><i class="fa fa-plus-circle"></i> &nbsp; Add KPI </button>
        </div>
        <div class="col-md-6">
          <div class="box box-solid" id="boxForm" style="display:none;">
            <div class="box-header bg-navy">
              <h3 class="box-title">Input KPI</h3>
            </div>
            <div class="box-body">

              <form action="index.html" method="post" id="addKpiForm">
                    <div class="col-md-12">
                      <div class="form-group">
                          <label>KPI</label>
                          <input type="text" class="form-control" id="kpi" name="kpi" placeholder="KPI" required>
                          <input type="hidden" class="form-control" id="kpi_id" name="kpi_id" placeholder="KPI" required>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label >UoM</label>
                        <select class="form-control " id="uom" name="uom" required>
                          <option value="">Chose UoM</option>
                          <option value="percent">percent (%)</option>
                          <option value="index">index</option>
                          <option value="MUSD">MUSD</option>
                          <option value="USD">USD</option>
                          <option value="Times">Times</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label >Parent</label>
                        <select class="form-control" id="parent" name="level">
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label >Formula</label>
                        <select class="form-control" id="formula" name="formula" required>
                          <option value="">Chose Formula</option>
                          <?php foreach ($formula as $data): ?>
                            <option value="<?= $data->kf_id ?>"><?= $data->kf_name ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div id="group" class="col-md-12">
                      <div class="form-group">
                        <label>Group</label>
                        <select class="form-control" id="kpi_group_id_select" name="kpi_group_id_select">
                          <option value="">Chose..</option>
                        </select>
                      </div>
                    </div>
                    <div id="inputKiWeight" style="display:none;" class="col-md-12">
                      <div class="form-group">
                          <label>Weight</label>
                          <input type="text" class="form-control" id="ki_weight" name="ki_weight" placeholder="Input Weight" required value="0">
                      </div>
                    </div>
                    <div id="inputKiTarget" style="display:none;" class="col-md-12">
                      <div class="form-group">
                          <label>Target</label>
                          <input type="text" class="form-control" id="ki_target" name="ki_target" placeholder="Input Target" required value="0">
                      </div>
                    </div>
                    <div id="inputKiTargetYtd" style="display:none;" class="col-md-12">
                      <div class="form-group">
                          <label>Target Year to Date</label>
                          <input type="text" class="form-control" id="ki_target_ytd" name="ki_target_ytd" placeholder="Input Target" required value="0">
                      </div>
                    </div>
                    <div id="inputKiTargetStabilize" style="display:none;">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Target Upper</label>
                            <input type="text" class="form-control" id="target_upper" name="target_upper" placeholder="Input Target"  value="0">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Target Lower</label>
                            <input type="text" class="form-control" id="target_lower" name="target_lower" placeholder="Input Target"  value="0">
                        </div>
                      </div>
                    </div>
                    <div id="inputKiTargetStabilizeYtd" style="display:none;">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Target Upper Year to Date</label>
                            <input type="text" class="form-control" id="target_upper_ytd" name="target_upper_ytd" placeholder="Input Target"  value="0">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Target Lower Year to Date</label>
                            <input type="text" class="form-control" id="target_lower_ytd" name="target_lower_ytd" placeholder="Input Target"  value="0">
                        </div>
                      </div>
                    </div>
                      <button type="submit" class="btn btn-flat bg-navy pull-right" id="save" name="save"><i class="fa fa-save"></i> &nbsp; Save</button>
                      <button type="button" style="margin-right:10px;" class="btn btn-flat btn-default pull-right" id="cancel" name="cancel"><i class="fa fa-arrow-left"></i> &nbsp; Cancel</button>
                  </form>
            </div>
            <!-- Loading (remove the following to stop the loading)-->
            <div id="loadingUom" class="overlay" >
              <i class="fa fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
          </div>
        </div>
        <div class="col-md-4">
          <div class="pull-right">
            <div id="button_lock"></div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <p style="font-size: 20px;">LIST KPI</p>
          <button type="button" name="button" id="copy" class="btn bg-navy btn-flat"><i class="fa fa-copy"></i> &nbsp; Duplicate KPI from other years</button>
          <br><br>
          <div class="isi" style="position:relative;">
            <div class="overlay-box" id="loader" style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
              <div style="" class="loading"></div>
            </div>
            <table class="table table-hover table-bordered" id="datatableScrollale">
              <thead style="background-color:#05354D; color: #fff;">
                <tr>
                    <th style="text-align:center;">KPI</th>
                    <th style="text-align:center;">UoM</th>
                    <th style="text-align:center;">Formula</th>
                    <th style="text-align:center;">Weight</th>
                    <th style="text-align:center;">Target</th>
                    <th style="text-align:center;">Target YTD</th>
                    <th style="text-align:center;">Upper</th>
                    <th style="text-align:center;">Lower</th>
                    <th style="text-align:center;">Upper YTD</th>
                    <th style="text-align:center;">Lower YTD</th>
                    <th style="text-align:center;">Limit</th>
                    <th style="text-align:center;">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Management Grouping</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-2">
          <button type="button" id="add_group" class="btn btn-flat bg-navy form-control" name="button"> <i class="fa fa-plus-circle"></i>  &nbsp;&nbsp; Add Group </button>
        </div>
        <div class="col-md-6">
          <div class="box box-solid" id="formGroup" style="display:none;">
            <div class="box-header bg-navy">
              <h3 class="box-title">Form Group</h3>
            </div>
            <div class="box-body">
              <form id="grouping" action="index.html" method="post">
                <div class="form-group">
                  <label for="">Group Name</label>
                  <input type="text" class="form-control" id="kpi_group_name" name="kpi_group_name" value="" required>
                  <input type="hidden" class="form-control" id="kpi_group_id" name="kpi_group_id" value="" required>
                </div>
                <button type="submit" class="btn btn-flat bg-navy pull-right" id="saveGroup" name="saveGroup"><i class="fa fa-save"></i> &nbsp; Save</button>
                <button type="button" style="margin-right:10px;" class="btn btn-flat btn-default pull-right" id="cancelFormGroup" name="cancelFormGroup"><i class="fa fa-arrow-left"></i> &nbsp; Cancel</button>
              </form>
            </div>
            <div class="overlay" id="loadingGroup">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <p style="font-size: 20px;">LIST GROUP</p>
          <div class="isi" style="position:relative;">
            <div class="overlay-box" id="loader2" style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
              <div style="" class="loading"></div>
            </div>
            <table class="table table-hover table-bordered" id="groupList" style="width:70%; margin:0 auto;">
              <thead class="bg-navy">
                <tr>
                  <th>No</th>
                  <th>Group Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Management User KPI</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-2">
          <button type="button" class="btn btn-flat bg-navy form-control" id="addUnit" name="addUnit"><i class="fa fa-plus-circle"></i> &nbsp;&nbsp; Add Unit</button>
        </div>
        <div class="col-md-6">
          <div class="box box-solid" id="boxFormUser" style="display:none;">
            <div class="box-header bg-navy">
              <h3 class="box-title">KPI Unit</h3>
            </div>
            <div class="box-body">
              <form id="formAddUser" action="index.html" method="post">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="unit_name" id="unit_name" value="" required>
                    <input type="hidden" class="form-control" name="kpi_unit_id" id="kpi_unit_id" value="">
                  </div>
                  <br>
                  Add User to Unit ..
                  <hr style="margin-top: 0px;">
                  <button type="button" id="append_user" name="button" class="btn bg-navy btn-flat"><i class="fa fa-plus"></i> Add User</button>
                  <table class="table">
                    <tbody id="content_user">

                    </tbody>
                  </table>
                </div>
                <button type="submit" class="btn btn-flat bg-navy pull-right" id="save" name="saveFormUser"><i class="fa fa-save"></i> &nbsp; Save</button>
                <button type="button" style="margin-right:10px;" class="btn btn-flat btn-default pull-right" id="cancelFormUser" name="cancelFormUser"><i class="fa fa-arrow-left"></i> &nbsp; Cancel</button>
              </form>
            </div>
            <div id="loadingUomUser" class="overlay">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <p style="font-size: 20px;">LIST UNIT</p>
          <div class="isi" style="position:relative;">
            <div class="overlay-box" id="loader1" style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
              <div style="" class="loading"></div>
            </div>
            <table style="width: 70%;margin: 0 auto;" class="table table-hover table-bordered" id="unitTable">
            <thead style="background-color:#05354D; color: #fff; text-align:center;">
              <tr>
                <th style="text-align:center;">No</th>
                <th style="text-align:center;">Name</th>
                <th style="text-align:center;">Action</th>
              </tr>
            </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Management Actual KPI</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div style="border: 1px solid #bfbcbc; padding:  10px;">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Months</label>
                  <select class="form-control " name="actualMonth" id="actualMonth">
                    <?php
                      $months = range(1, 12);
                      foreach ($months as $month) {
                        $selected = (date("m") == $month) ? "selected" : "";
                        echo "<option value='".$month."' ".$selected.">".convert_month($month)."</option>";
                      }
                     ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row" style="margin-top: 20px;">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-2">
            	<p style="font-size:20px;">List KPI</p>
              <hr style="border: 0;border-top: 1px solid #bfbcbc;margin-top:0px;margin-bottom:0px;">
            </div>
          </div>
          <div class="isi" style="position:relative;">
            <div class="overlay-box" id="loaderListUnit" style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
              <div style="" class="loading"></div>
            </div>
            <div id="all_total_score"></div>
            <table class="table table-hover table-bordered" id="actualTable">
            <thead style="background-color:#05354D; color: #fff; text-align:center;">
              <tr>
                <th style="text-align:center;">KPI</th>
                <th style="text-align:center;">UoM</th>
                <th style="text-align:center;">Actual</th>
                <th style="text-align:center;">Target</th>
                <th style="text-align:center;">Achievement</th>
                <th style="text-align:center;">Score</th>
                <th style="text-align:center;">Final Score</th>
                <th style="text-align:center;">Actual YTD</th>
                <th style="text-align:center;">Target YTD</th>
                <th style="text-align:center;">Achievement YTD</th>
                <th style="text-align:center;">Score YTD</th>
                <th style="text-align:center;">Final Score YTD</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="detailItem" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form class="" action="index.html" id="addDetail" method="post">
        <div class="modal-header">
          <button type="button" class="close" id="closemodaldetail" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Edit KPI</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="author_detail" id="author_detail" value="">
          <input type="hidden" name="year_input" id="year_input" value="">
          <input type="hidden" name="ki_id" id="ki_id" value="">
          <input type="hidden" name="ki_uom" id="ki_uom" value="">
          <div class="row">
            <div class="col-md-2">
              <p>KPI</p>
              <p>UOM</p>
            </div>
            <div class="col-md-10">
              <p id="ki_name_label">: KPI</p>
              <p id="ki_uom_label">UOM</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div id="detailKpi"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" id="closeModalDetail" data-dismiss="modal">Close</button>
          <button type="submit" class="btn bg-navy btn-flat" id="save_form_detail" >Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="assigmentModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="margin-left: 50%; margin-top: 20%;">
    <form id="assigmentForm" action="index.html" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id=""></h4>
        </div>
        <div class="modal-body">
          <div style="display:none;" class="loading" id="loaderModalAssign"></div>
          <div id="content" class="row" style="display:none;">
            <div class="col-md-12">
              <button type="button" class="btn btn-flat btn-navy" id="appendKpi" name="appendKpi"><i class="fa fa-plus-circle"></i> &nbsp; Add</button>
              <input type="hidden" name="role_id" id="role_id" value="">
              <table class="table table-hover" id="listAssignKpi">
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
          <button type="submit" id="submitAssign" class="btn btn-flat bg-navy">Save</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade" id="copy_modal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Duplicate Engine</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>From year</label>
              <input type="text" class="form-control" id="copy_year1" value="<?= date("Y", strtotime(date("Y", strtotime(date("Y-m-d"))) . " - 1 year")) ?>" readonly>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>To Year</label>
              <input type="text" class="form-control" id="copy_year2" value="<?= date("Y") ?>" readonly>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
        <button type="button" id="save_copy" class="btn btn-flat bg-navy ">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_group" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id=""></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form id="add_to_group" action="index.html" method="post">
            <div class="col-md-6">
              <div class="form-group">
                <label>Chose KPI</label>
                <input type="hidden" id="ki_id_select" value="">
                <select class="form-control" id="kpi_group_id_select_dua" required>
                  <option value="">Chose..</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <label>&nbsp;</label>
              <button type="submit" class="form-control btn btn-flat bg-navy" id="add_list"> <i class="fa fa-save"></i> Add to group </button>
            </div>
          </form>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="isi" style="position:relative;">
              <div class="overlay-box" id="loaderMember" style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                <div style="" class="loading"></div>
              </div>
              <table id="table_group_member" class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">KPI Name</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  function button_lock()
  {
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/check_lock',
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
        $("#button_lock").html("<p style='margin-right:20px;'>Please wait ..</p>");
      }
    })
    .done(function(data) {
      var button = '<button type="button"'+
                      'data-toggle="tooltip"'+
                      'title="'+((data.isLocked == false) ? 'Click to lock' : 'Click to unlock')+'"'+
                      'class="btn '+((data.isLocked == false) ? 'bg-navy' : 'btn-danger')+' btn-flat" data-islocked="'+data.isLocked+'"'+
                      'id="lock_button"'+
                      'name="lock_button"'+
                      'style="margin-right: 10px;padding: 10px 16px;font-size: 27px;line-height: 1.333333;border-radius: 6px;">'+
                      '<i class="fa '+((data.isLocked == false) ? 'fa-unlock' : 'fa-lock')+' "></i>'+
                    '</button>';
        $("#button_lock").html(button);
    })
    .fail(function() {
      notif("error", "Failed to load lock key");
    })
    .always(function() {
      // console.log("complete");
    });
  }

  function load_all_group(kpi_group_id = null)
  {
    $("#kpi_group_id_select").find('option').remove();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/jsonGroup',
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
          $("#kpi_group_id_select").html("<option value=''>Please Wait</option>");
      }
    })
    .done(function(data) {
      $("#kpi_group_id_select").find('option').remove();
      $("#kpi_group_id_select").append("<option value=''>Chose</option>");
      $.each(data, function(index, list){
        $("#kpi_group_id_select").append("<option value='"+list.kpi_group_id+"' "+((kpi_group_id == list.kpi_group_id) ? "selected" : "")+">"+list.kpi_group_name+"</option>");
      })
    })
    .fail(function() {
      notif("error", "Failed to load group");
      $("#kpi_group_id_select").find('option').remove();
      $("#kpi_group_id_select").append("<option value=''>Error</option>");
    })
    .always(function() {

    });

  }

  function all_total_score()
  {
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/get_all_total',
      type: 'GET',
      dataType: 'JSON',
      data: {
        month: $("#actualMonth").val()
      }
    })
    .done(function(data) {
      $("#all_total_score").html("<h4>Total Score : "+data.total+"%</h4>")
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  }

  $(document).on("click", "#lock_button", function(e){
    e.preventDefault();
    var isLocked = $(this).data("islocked");
    var params = ((isLocked == true) ? '0' : '1');
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/change_lock',
      type: 'POST',
      dataType: 'JSON',
      data: {params: params}
    })
    .done(function(data) {
      if(data.success){
        notif("success", "Success");
      } else {
        notif("error", "Something Wrong");
      }
    })
    .fail(function() {
      notif("error", "Something Wrong");
    })
    .always(function() {
      button_lock();
    });

  })

  $("#add_group").on("click", (e) => {
    e.preventDefault();
    $("#formGroup").fadeIn(1000, function() {
      $("#loadingGroup").fadeOut(1000);
    });
  });

  $("#cancelFormGroup").on("click", function(e){
    e.preventDefault();
    $('#formGroup').fadeOut(1000, function() {
      $("#loadingGroup").fadeIn(1000);
      $("#kpi_group_name").val("");
      $("#kpi_group_id").val("");
    });
  });

  $("#grouping").parsley();
  $("#grouping").on("submit", function(e){
    e.preventDefault();
    var kpi_group_name = $("#kpi_group_name").val();
    var kpi_group_id = $("#kpi_group_id").val();
    $.ajax({
      url: '<?= base_url()?>index.php/kpi_sla/saveGroup',
      type: 'POST',
      dataType: 'JSON',
      data: {kpi_group_name: kpi_group_name, kpi_group_id: kpi_group_id},
      beforeSend: function(){
        $("#saveGroup").prop('disabled', true).html('<i class="fa fa-refresh fa-spin"></i> &nbsp; Please Wait..')
      }
    })
    .done(function(data) {
      if(data.success){
        notif("success", "Data Saved");
      } else{
        notif("error", "Failed to Saved");
      }
    })
    .fail(function() {
      notif("error", "Failed to Saved");
    })
    .always(function(data) {
      $('#formGroup').fadeOut(1000, function() {
        $("#loadingGroup").fadeIn(1000);
        $("#kpi_group_name").val("");
        $("#kpi_group_name").val("");
        $("#kpi_group_id").val("");
        $("#saveGroup").prop('disabled', false).html('<i class="fa fa-save"></i> &nbsp; Save')
      });
      load_all_group();
      loadGrup();
    });

  });

  $(document).on("click", "#edit_group", function(e){
    e.preventDefault();
    var id = $(this).data("id");
    $.ajax({
      url: '<?= base_url()?>index.php/kpi_sla/edit/'+id,
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
        $("#loadingGroup").fadeIn(1000);
      }
    })
    .done(function(data) {
      $("#formGroup").fadeIn(1000, function(){
        $('html, body').animate({
            scrollTop: $("#formGroup").offset().top
        }, 2000);
        $("#loadingGroup").fadeOut(1000);
        $("#kpi_group_id").val(data.kpi_group_id);
        $("#kpi_group_name").val(data.kpi_group_name);
      });
    })
    .fail(function() {
      notif("error", "failed to load data");
    })
    .always(function() {
      load_all_group();
    });

  })

  $(document).on("click", "#manage_grup", function(e){
    e.preventDefault();
    var id = $(this).data("id");
    var name = $(this).data("name");
    var thn = $('#year').val();
    load_member(id, thn);
    openModal(id, name);
  })

  $(document).on("click", "#delete_group", function(e){
    e.preventDefault();
    var id = $(this).data("id");

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn bg-navy btn-flat',
      cancelButtonClass: 'btn btn-default btn-flat',
      buttonsStyling: false,
      reverseButtons: true
    }).then(function(result) {
      if (result) {
        $.ajax({
          url: '<?= base_url() ?>index.php/kpi_sla/delete_group',
          type: 'POST',
          dataType: 'JSON',
          data: {
            id: id
          }
        })
        .done(function(data) {
          if(data.success){
            swal(
              'Deleted!',
              'Your data has been deleted.',
              'success'
            )
            AllListUnit();
            getAllRole();
          } else{
            notif("error", "sorry.. failed to delete");
          }
        })
        .fail(function() {
          notif("error", "sorry.. failed to delete");
        })
        .always(function() {
          loadGrup();
          load_all_group();
        });
      }
    })
  })

  $("#add_to_group").parsley();
  $("#add_to_group").on("submit", function(e){
    e.preventDefault();
    submit_member();
  })

  $(document).on("click", "#remove_member", function(e){
    e.preventDefault();
    var id = $(this).data("id");
    var ki_id_select = $("#ki_id_select").val();
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn bg-navy btn-flat',
      cancelButtonClass: 'btn btn-default btn-flat',
      buttonsStyling: false,
      reverseButtons: true
    }).then(function(result) {
      if (result) {
        $.ajax({
          url: '<?= base_url() ?>index.php/kpi_sla/remove_member',
          type: 'POST',
          dataType: 'JSON',
          data: {
            id: id
          }
        })
        .done(function(data) {
          if(data.success){
            swal(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            AllListUnit();
            getAllRole();
          } else{
            notif("error", "sorry.. failed to delete");
          }
        })
        .fail(function() {
          notif("error", "sorry.. failed to delete");
        })
        .always(function() {
          list_parrent_kpi();
          var thn = $('#year').val();
          load_member(ki_id_select, thn);
        });
      }
    })

  })

  function submit_member()
  {
    var kpi_group_id = $("#kpi_group_id_select_dua").val();
    var ki_id = $("#ki_id_select").val();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/save_member',
      type: 'POST',
      dataType: 'JSON',
      data: {kpi_group_id: kpi_group_id, ki_id: ki_id},
      beforeSend: function(){
        $("#add_list").prop('disabled', true).html("<i class='fa fa-refresh fa-spin'></i> Please Wait ..");
      }
    })
    .done(function(data) {
      if(data.success){
        notif("success", "Data Saved");
      } else{
        notif("success", "Failed to save");
      }
    })
    .fail(function() {
      notif("success", "Failed to save");
    })
    .always(function() {
      $("#add_list").prop('disabled', false).html("<i class='fa fa-save'></i> Save");
      list_parrent_kpi();
      var thn = $('#year').val();
      load_member(ki_id, thn);
    });

  }

  function datatable_group(id)
  {
    $("#table_group_member").DataTable({
      ordering: true,
      processing: true,
      serverSide: true,
      responsive: true,
      pageLength: 10,
      ajax: {
        url: "<?= base_url() ?>index.php/kpi_sla/member_group_datatable/"+id,
        type:'POST',
      },
      "aoColumns": [
        {
          "mData": "1",
          "mRender": function ( data, type, row ) {
            return "<b>"+data+"</b>";
          }
        }
      ]
    })
  }

  function list_parrent_kpi()
  {
    var thn = $('#year').val();
    $('#kpi_group_id_select_dua').find('option').remove();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/list_parent/' + thn,
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
        $('#kpi_group_id_select_dua').append($('<option>', {value:'', text:'Please Wait ..'}));
      }
    })
    .done(function(data) {
      console.log(data)
      $('#kpi_group_id_select_dua').find('option').remove();
      $('#kpi_group_id_select_dua').append($('<option>', {value: '', text: 'Chose'}));
      $.each(data, function(index, list){
        $('#kpi_group_id_select_dua').append($('<option>', {value: list.ki_id, text: list.ki_name}));
      })
    })
    .fail(function() {
      notif("error", "falied load parent");
      $('#kpi_group_id_select_dua').find('option').remove();
      $('#kpi_group_id_select_dua').append($('<option>', {value: "", text: "error"}));
    })
    .always(function(data) {

    });

  }

  function load_member(id,thn)
  {
    $("#table_group_member").DataTable().clear().draw();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/member_group/'+id+'/'+thn,
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
        $("#loaderMember").fadeIn("fast");
      }
    })
    .done(function(data) {
      $("#loaderMember").fadeOut(2000);
      var no = 1;
      $.each(data, function(index, list){
        var row = $("#table_group_member").DataTable().row.add(
          [
            "<center>"+no+"</center>",
            "<center>"+list.ki_name+"</center>",
            "<center><button type='button' data-id='"+list.ki_id+"' class='btn btn-flat btn-danger' id='remove_member'><i class='fa fa-trash'></i></button></center>"
          ]
        ).draw().node();
        no++;
      })
    })
    .fail(function() {
      console.log("error");
    })
    .always(function(data) {
      console.log(data);
    });

  }

  function openModal(id, name)
  {
      $("#modal_group").modal("show");
      $("#modal_group").find('.modal-title').text("Manage "+name);
      list_parrent_kpi();
      $("#ki_id_select").val(id);
  }

  function loadGrup()
  {
    $("#groupList").find('tbody tr').remove();
    $.ajax({
      url: '<?= base_url()?>index.php/kpi_sla/listGrup',
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
        $("#loader2").fadeIn(1000);
      }
    })
    .done(function(data) {
      $("#groupList").find('tbody').append(data.html);
    })
    .fail(function() {
      notif("error", "failed to load group list");
    })
    .always(function(data) {
      $("#loader2").fadeOut(1000);
    });

  }

  function getAllRole()
  {
    $("#list_user").find('option').remove();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/getAllRole',
      type: 'GET',
      dataType: 'JSON'
    })
    .done(function(data) {
      $("#list_user").append("<option value=''>Chose User..</option>");
      $.each(data, function(index, result){
        var string = "<option value='"+result.role_id+"'>"+result.role_name+"</option>";
        $("#list_user").append(string);
      })
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  }

  function dataParent()
  {
      $("#parent").html("");
      $("#parent").find('option.item').remove();
      var year = $("#year").val();
      $.ajax({
        url: '<?= base_url() ?>index.php/kpi_sla/treeKpiParent/'+year,
        type: 'GET',
        dataType: 'JSON'
    })
    .done(function(data) {
      var combo = '<option data-level="1" value="">Is Parent</option>';
          combo += data.tree;
        $("#parent").html(combo);
    })
    .fail(function() {
      notif("error", "Failed to load data parent");
    })
    .always(function() {

    });

  }

  function allListWithDetail()
  {
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/list_with_detail',
      type: 'POST',
      dataType: 'JSON',
      data: {
        year: $("#year").val()
      },
      beforeSend: function(){
        $("#loader").fadeIn(1000);
      }
    })
    .done(function(data) {
      $("#loader").fadeOut(1000);
      $("#datatableScrollale").find('tbody').html(data.htmlBuild);
    })
    .fail(function() {
      notif("error", "failed to load list kpi");
    })
    .always(function() {
      var
          $table = $('#datatableScrollale'),
          rows = $table.find('tr');

      rows.each(function (index, row) {
          var
              $row = $(row),
              level = $row.data('level'),
              id = $row.data('id'),
              $columnName = $row.find('td[data-column="name"]'),
              children = $table.find('tr[data-parent="' + id + '"]');

          if (children.length) {
              var expander = $columnName.prepend('' +
                  '<span class="treegrid-expander glyphicon glyphicon-chevron-down"></span>' +
                  '');

              // children.hide();

              expander.on('click', function (e) {
                  var $target = $(e.target);
                  if ($target.hasClass('glyphicon-chevron-right')) {
                      $target
                          .removeClass('glyphicon-chevron-right')
                          .addClass('glyphicon-chevron-down');

                      children.show();
                  } else {
                      $target
                          .removeClass('glyphicon-chevron-down')
                          .addClass('glyphicon-chevron-right');

                      reverseHide($table, $row);
                  }
              });
          }

          $columnName.prepend('' +
              '<span class="treegrid-indent" style="width:' + 15 * level + 'px"></span>' +
              '');
      });

      reverseHide = function (table, element) {
          var
              $element = $(element),
              id = $element.data('id'),
              children = table.find('tr[data-parent="' + id + '"]');

          if (children.length) {
              children.each(function (i, e) {
                  reverseHide(table, e);
              });

              $element
                  .find('.glyphicon-chevron-down')
                  .removeClass('glyphicon-chevron-down')
                  .addClass('glyphicon-chevron-right');

              children.hide();
          }
      };
    });

  }

  function getMonthName(month)
  {
    switch (month) {
      case 1: return 'Jan'; break;
      case 2: return 'Feb'; break;
      case 3: return 'Mar'; break;
      case 4: return 'Apr'; break;
      case 5: return 'May'; break;
      case 6: return 'Jun'; break;
      case 7: return 'Jul'; break;
      case 8: return 'Aug'; break;
      case 9: return 'Sep'; break;
      case 10: return 'Oct'; break;
      case 11: return 'Nov'; break;
      case 12: return 'Dec'; break;
    }
  }

  function changeYear()
  {
    var year = $("#year").val();
    $("#year_input").val(year);
  }

  function AllListUnit()
  {
    $("#unitTable").find('tr.data').remove();
    $.ajax({
      url: '<?= base_url()?>index.php/kpi_sla/all_list_unit',
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
        $("#loader1").fadeIn(1000);
      }
    })
    .done(function() {
      $("#loader1").fadeOut(1000);
    })
    .fail(function() {
      notif("error", "failed to load list unit");
    })
    .always(function(allData) {
      var no = 1;
      $.each(allData, function(i, data){
        var row = "<tr class='data'>"+
                      "<td style='text-align: center;'>"+no+"</td>"+
                      "<td style='text-align: center;'>"+data.kpi_unit_name+"</td>"+
                      "<td style='text-align: center;'>"+
                        "<button type='button' id='assignKpi' data-toggle='modal' data-target='#assigmentModal' class='btn btn-flat bg-navy' data-role_name='"+data.kpi_unit_name+"' data-role_id='"+data.kpi_unit_id+"'><i class='fa fa-pencil'></i></button>"+
                        "<button type='button' id='deleteUnitKpi' class='btn btn-flat bg-red' data-role_name='"+data.kpi_unit_name+"' data-role_id='"+data.kpi_unit_id+"'><i class='fa fa-trash'></i></button>"+
                        "<button type='button' id='settingUnit' class='btn btn-flat btn-success' data-role_name='"+data.kpi_unit_name+"' data-role_id='"+data.kpi_unit_id+"'><i class='fa fa-wrench'></i></button>"+
                      "</td>"+
                   "</tr>";
         $("#unitTable").append(row);
        no++;
      });
    });

  }

  function getDataHierarchyActual()
  {
    all_total_score();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/getDataActual',
      type: 'GET',
      dataType: 'JSON',
      data: {
        month: $("#actualMonth").val(),
      },
      beforeSend: function(){
        $("#loaderListUnit").fadeIn(1000);
      }
    })
    .done(function(data) {
      $("#loaderListUnit").fadeOut(1000);
      $("#actualTable").find('tbody').html(data.content);
    })
    .fail(function() {
      notif("error", "Something make your data failed to load");
    })
    .always(function() {
      var
          $table = $('#actualTable'),
          rows = $table.find('tr');

      rows.each(function (index, row) {
          var
              $row = $(row),
              level = $row.data('level'),
              id = $row.data('id'),
              $columnName = $row.find('td[data-column="name"]'),
              children = $table.find('tr[data-parent="' + id + '"]');

          if (children.length) {
              var expander = $columnName.prepend('' +
                  '<span class="treegrid-expander glyphicon glyphicon-chevron-down"></span>' +
                  '');

              // children.hide();

              expander.on('click', function (e) {
                  var $target = $(e.target);
                  if ($target.hasClass('glyphicon-chevron-right')) {
                      $target
                          .removeClass('glyphicon-chevron-right')
                          .addClass('glyphicon-chevron-down');

                      children.show();
                  } else {
                      $target
                          .removeClass('glyphicon-chevron-down')
                          .addClass('glyphicon-chevron-right');

                      reverseHide($table, $row);
                  }
              });
          }

          $columnName.prepend('' +
              '<span class="treegrid-indent" style="width:' + 15 * level + 'px"></span>' +
              '');
      });

      reverseHide = function (table, element) {
          var
              $element = $(element),
              id = $element.data('id'),
              children = table.find('tr[data-parent="' + id + '"]');

          if (children.length) {
              children.each(function (i, e) {
                  reverseHide(table, e);
              });

              $element
                  .find('.glyphicon-chevron-down')
                  .removeClass('glyphicon-chevron-down')
                  .addClass('glyphicon-chevron-right');

              children.hide();
          }
      };
    });

  }

  function toggleTargetWeight()
  {
    var uom = $("#uom").val();
    var ki_parent_id = $("#parent").val();
    var level = $("#parent").find(':selected').data("level");
    var ki_id = $("#parent").find(':selected').data("ki_id");
    var totalChild = 0
    $("#parent").find('option[data-ki_parent_id='+ki_id+']').each(function() {
      totalChild++;
    });
    console.log(totalChild);
    var formula = $("#formula").find(':selected').text();
    if(uom == ""){
      // $("#inputKiWeight").fadeOut("fast");
      $("#inputKiTarget").fadeOut("fast");
      $("#inputKiTargetYtd").fadeOut("fast");
      $("#inputKiTargetStabilize").fadeOut("fast");
      $("#inputKiTargetStabilizeYtd").fadeOut("fast");
      // $("#inputKiWeight").find('#ki_weight').val("0");
      $("#inputKiTarget").find('#ki_target').val("0");
      $("#inputKiTargetYtd").find('#ki_target_ytd').val("0");
      $("#group").find('#kpi_group_id_select').val("");
    } else{
      console.log(level, "level");
      if(level == "1"){
        $("#group").fadeIn("fast");
        if(formula == "Stabilize"){
          $("#inputKiTargetStabilize").fadeIn("fast");
          $("#inputKiTargetStabilizeYtd").fadeIn("fast");
          $("#inputKiWeight").fadeOut("fast");
          $("#inputKiTarget").fadeOut("fast");
          $("#inputKiTargetYtd").fadeOut("fast");
          $("#inputKiWeight").find('#ki_weight').val("0");
          $("#inputKiTarget").find('#ki_target').val("0|0");
          $("#inputKiTargetYtd").find('#ki_target_ytd').val("0|0");
        } else {
          $("#inputKiTarget").fadeIn("fast");
          $("#inputKiTargetYtd").fadeIn("fast");
          $("#inputKiTargetStabilize").fadeOut("fast");
          $("#inputKiTargetStabilizeYtd").fadeOut("fast");
          $("#inputKiWeight").fadeOut("fast");
          $("#inputKiTarget").find('#ki_target').val("0");
          $("#inputKiTargetYtd").find('#ki_target_ytd').val("0");
          $("#inputKiWeight").find('#ki_weight').val("0");
        }
      } else if(level == "2" || level == "3" || level == "4"){
        $("#group").fadeOut("fast");
        $("#inputKiWeight").fadeIn("fast");
        $("#inputKiTargetStabilize").fadeOut("fast");
        $("#inputKiTargetStabilizeYtd").fadeOut("fast");
        $("#inputKiTarget").fadeOut("fast");
        $("#inputKiTargetYtd").fadeOut("fast");
        $("#inputKiTarget").find('#ki_target').val("0");
        $("#inputKiTargetYtd").find('#ki_target_ytd').val("0");
      } else{
        $("#group").fadeOut("fast");
        $("#inputKiWeight").fadeOut("fast");
        $("#inputKiTargetStabilize").fadeOut("fast");
        $("#inputKiTargetStabilizeYtd").fadeOut("fast");
        $("#inputKiTarget").fadeOut("fast");
        $("#inputKiTargetYtd").fadeOut("fast");
        $("#inputKiWeight").find('#ki_weight').val("0");
        $("#inputKiTarget").find('#ki_target').val("0");
        $("#inputKiTargetYtd").find('#ki_target_ytd').val("0");
      }
    }
  }

  $(document).ready(function() {
    $("#user_id").val(localStorage.getItem("user_id"));
    $("#author_detail").val(localStorage.getItem("user_id"));
    $("#table_group_member").DataTable();
    allListWithDetail();
    changeYear();
    dataParent();
    //UNIT
    AllListUnit();
    loadGrup();
    getDataHierarchyActual();
    all_total_score();
    button_lock();
  });

  $(document).on("click", "#settingUnit", function(e){
    e.preventDefault();
    $("#addUnit").trigger('click');
    var kpi_unit_id = $(this).data("role_id");
    var kpi_unit_name = $(this).data("role_name");
    $("#unit_name").val(kpi_unit_name);
    $("#kpi_unit_id").val(kpi_unit_id);
    append_unit(kpi_unit_id);
  })

  $(document).on("click", "#deleteUnitKpi", function(e){
    e.preventDefault();
    var role_id = $(this).data("role_id");
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn bg-navy btn-flat',
      cancelButtonClass: 'btn btn-default btn-flat',
      buttonsStyling: false,
      reverseButtons: true
    }).then(function(result) {
      if (result) {
        $.ajax({
          url: '<?= base_url() ?>index.php/kpi_sla/delete_unit',
          type: 'POST',
          dataType: 'JSON',
          data: {
            role_id: role_id
          }
        })
        .done(function(data) {
          if(data.success){
            swal(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            AllListUnit();
            getAllRole();
          } else{
            notif("error", "sorry.. failed to delete");
          }
        })
        .fail(function() {
          notif("error", "sorry.. failed to delete");
        })
        .always(function() {
          console.log("complete");
        });
      }
    })
  })

  $("#copy").on("click", function(e){
    e.preventDefault();
    $("#copy_modal").modal("show");
  })

  $("#copy_year1, #copy_year2").datepicker({
      format: "yyyy",
      viewMode: "years",
      minViewMode: "years"
  });

  $("#copy_year1, #copy_year2").on("change", function(){
    var copy_year1 = $("#copy_year1").val();
    var copy_year2 = $("#copy_year2").val();
    if(copy_year1 == copy_year2){
      notif("warning", "The year selection should not be the same");
      $("#copy_year1").val(copy_year2-1);
    }
  })

  $("#save_copy").on("click", function(e){
    e.preventDefault();
    var copy_year1 = $("#copy_year1").val();
    var copy_year2 = $("#copy_year2").val();
    if(copy_year1 == "" || copy_year2 == ""){
      notif("warning", "Please insert the input..");
      return;
    }
    var ini = $(this);
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/duplicate',
      type: 'POST',
      dataType: 'JSON',
      data: {copy_year1: $("#copy_year1").val(), copy_year2: $("#copy_year2").val()},
      beforeSend: function(){
        ini.attr('disabled', true).text("please wait..");
      }
    })
    .done(function(data) {
      if(data.success){
        notif("success", "Data successfully duplicated");
      } else{
        notif("error", "Data failed duplicated");
      }
    })
    .fail(function() {
      notif("error", "Data failed duplicated");
    })
    .always(function() {
      ini.attr('disabled', false).text("Save");
      $("#copy_modal").modal("hide");
    });

  })

  $(document).on("click", "#delete", function(e){
    e.preventDefault();
    var ki_id = $(this).data("id");
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn bg-navy btn-flat',
      cancelButtonClass: 'btn btn-default btn-flat',
      buttonsStyling: false,
      reverseButtons: true
    }).then(function(result) {
      if (result) {
        $.ajax({
          url: '<?= base_url() ?>index.php/kpi_sla/delete_kpi',
          type: 'POST',
          dataType: 'JSON',
          data: {
            ki_id: ki_id
          }
        })
        .done(function(data) {
          if(data.success){
            swal(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            window.location.reload();
          } else{
            notif("error", "sorry.. failed to delete");
          }
        })
        .fail(function() {
          notif("error", "sorry.. failed to delete");
        })
        .always(function() {
          console.log("complete");
        });
      }
    })
  })

  var hardCode = ["SLA Line Operations", "SLA GA", "SLA QG", "SLA Base Operations", "CSF GA", "CSF NGA", "SLA BGA"];
  $("#kpi").autocomplete({
      source: hardCode
    });

  $("#formula").on("change", function(){
    toggleTargetWeight();
  })

  $("#target_upper_ytd, #target_lower_ytd").on("keyup keydown", function(e){
    $("#ki_target_ytd").val($("#target_upper_ytd").val()+"|"+$("#target_lower_ytd").val());
    if (event.shiftKey == true) {
        event.preventDefault();
    }
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190)
    {
      var value = $(this).val();
      var uom = $("#uom").val();
      // if(uom == "percent"){
      //   if(value > 100){
      //     $(this).val("0");
      //     notif("warning", "Value must less than 100");
      //   }
      // } else if(uom == "index"){
      //   if(value > 1){
      //     $(this).val("0");
      //     notif("warning", "Value must less than 1");
      //   }
      // }
    } else {
        event.preventDefault();
    }
    if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
  })

  $("#target_upper, #target_lower").on("keyup keydown", function(e){
    $("#ki_target").val($("#target_upper").val()+"|"+$("#target_lower").val());
    if (event.shiftKey == true) {
        event.preventDefault();
    }
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190)
    {
      var value = $(this).val();
      var uom = $("#uom").val();
      // if(uom == "percent"){
      //   if(value > 100){
      //     $(this).val("0");
      //     notif("warning", "Value must less than 100");
      //   }
      // } else if(uom == "index"){
      //   if(value > 1){
      //     $(this).val("0");
      //     notif("warning", "Value must less than 1");
      //   }
      // }
    } else {
        event.preventDefault();
    }
    if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
  })

  $("#year").on("change", function(e){
    allListWithDetail();
  })



  $(document).on("click", "#editKpi", function(e){
    e.preventDefault();
    var ki_id = $(this).data("id");
    var level = $(this).data("level");
    var formula = $(this).data("formula");
    $('html, body').animate({
      scrollTop: $("#boxForm").position().top
    }, 500);
    $("#boxForm").fadeIn(1000, function() {
      $.ajax({
        url: '<?= base_url() ?>index.php/kpi_sla/edit_item/'+ki_id,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function(){
          $("#loadingUom").fadeIn(1000);

          // if(level == "1"){
          //   if(formula == "Stabilize"){
          //     $("#inputKiTargetStabilize").fadeIn(1000);
          //     $("#inputKiTarget").fadeOut(1000);
          //     $("#inputKiWeight").fadeOut(1000);
          //   } else{
          //     $("#inputKiTarget").fadeIn(1000);
          //     $("#inputKiWeight").fadeOut(1000);
          //     $("#inputKiTargetStabilize").fadeOut(1000);
          //   }
          // } else if (level == "2") {
          //   $("#inputKiWeight").fadeIn(1000);
          //   $("#inputKiTarget").fadeOut(1000);
          //   $("#inputKiTargetStabilize").fadeOut(1000);
          // } else {
          //   $("#inputKiWeight").fadeOut(1000);
          //   $("#inputKiTarget").fadeOut(1000);
          //   $("#inputKiTargetStabilize").fadeOut(1000);
          // }
        }
      })
      .done(function(data) {
        $("#kpi_id").val(data.ki_id);
        $("#kpi").val(data.ki_name);
        $("#uom").val(data.ki_uom).attr("disabled", true);
        $("#formula").val(data.kf_id).attr("disabled", true);
        $("#ki_weight").val(data.ki_weight);
        load_all_group(data.kpi_group_id);
        // $("#kpi_group_id_select").val();
        if(level == "1" ||  data.ki_level=="deep_child"){
          console.log(data.ki_target);
          if(formula == "Stabilize"){
            var split_target = data.ki_target.split("|");
            var split_target_ytd = data.ki_target_ytd.split("|");
            $("#target_upper").val(split_target[0]);
            $("#target_lower").val(split_target[1]);
            $("#target_upper_ytd").val(split_target_ytd[0]);
            $("#target_lower_ytd").val(split_target_ytd[1]);
          } else {
            $("#ki_target").val(data.ki_target);
            $("#ki_target_ytd").val(data.ki_target_ytd);
            $("#target_upper").val(0);
            $("#target_lower").val(0);
            $("#target_upper_ytd").val(0);
            $("#target_lower_ytd").val(0);
          }
        }
        $("#parent").val((data.ki_parent_id == "0") ? "" : data.ki_parent_id);
      })
      .fail(function() {
        notif("error", "Failed to load data");
      })
      .always(function(data) {
        $("#loadingUom").fadeOut(1000);
        toggleTargetWeight();
        if(level == "1" || data.ki_level=="deep_child"){
          console.log(data.ki_target);
          if(formula == "Stabilize"){
            var split_target = data.ki_target.split("|");
            var split_target_ytd = data.ki_target_ytd.split("|");
            $("#target_upper").val(split_target[0]);
            $("#target_lower").val(split_target[1]);
            $("#target_upper_ytd").val(split_target_ytd[0]);
            $("#target_lower_ytd").val(split_target_ytd[1]);
            $("#ki_target").val(data.ki_target);
            $("#ki_target_ytd").val(data.ki_target_ytd);
          } else {
            $("#ki_target").val(data.ki_target);
            $("#ki_target_ytd").val(data.ki_target_ytd);
            $("#target_upper").val(0);
            $("#target_lower").val(0);
            $("#target_upper_ytd").val(0);
            $("#target_lower_ytd").val(0);
          }
        }
        // if(data.ki_level == "deep_child"){
        //   $("#target_upper").val(0);
        //   $("#target_lower").val(0);
        //   $("#target_upper_ytd").val(0);
        //   $("#target_lower_ytd").val(0);
        //   $("#ki_target").val(0);
        //   $("#ki_target_ytd").val(0);
        //   $("#ki_weight").val(0);
        //   $("#inputKiWeight").fadeOut(1000);
        //   $("#inputKiTarget").fadeOut(1000);
        //   $("#inputKiTargetYtd").fadeOut(1000);
        //   $("#inputKiTargetStabilize").fadeOut(1000);
        //   $("#inputKiTargetStabilizeYtd").fadeOut(1000);
        // }else{
        //    $("#inputKiWeight").fadeIn(1000);
        // }
      });
    });
    return false;
  });

  $("#assigmentForm").on("submit", function(e){
    e.preventDefault();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/assign_kpi',
      type: 'POST',
      dataType: 'JSON',
      data: new FormData(this),
      contentType: false,
      processData: false,
      cache: false,
      beforeSend: function(){
        $("#submitAssign").attr("disabled", "disabled").html("Please Wait");
      }
    })
    .done(function(data) {
      if(data.success){
        notif("success", "data inserted");
        window.location.reload();
      } else{
        notif("error", "sorry, failed to insert");
      }
    })
    .fail(function() {
      notif("error", "sorry, failed to insert");
    })
    .always(function() {
      $("#submitAssign").attr("disabled", false).html("Save");
    });

  })

  function task_asigment()
  {
    var option = "";
  }

  $("#appendKpi").on("click", function(e){
    e.preventDefault();
    var year = $("#year").val();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/task_asigment',
      type: 'GET',
      dataType: 'JSON',
      data: {
        year: year
      }
    })
    .done(function(data) {
      var option = "";
      $.each(data, function(index, result){
        option += "<option value='"+result.ki_id+"'>"+result.ki_name+"</option>";
      })
      var content = '<tr>'+
                      '<td>'+
                        '<select class="form-control" id="kpi" name="kpi[]">'+
                          option+
                        '</select>'+
                      '</td>'+
                      '<td style="font-size: 25px;"> <a href="#" id="delKpi" class="text-danger">X</a> </td>'+
                    '</tr>';
      $("#listAssignKpi").find('tbody').prepend(content);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  });

  $(document).on("change", "#kpi", function(e){
    var ini = $(this);
    ini.closest('tbody').find('tr').each(function(index, el){
      var otherValKpi = $(this).find('#kpi').not(ini).val();
      var this_value = ini.val();
      console.log(otherValKpi, this_value);
      if(otherValKpi == this_value){
        notif("warning", "the KPI item has been selected");
        ini.closest('tr').remove();
      }
    })
  })

  $(document).on("click", "#delKpi", function(e){
    e.preventDefault();
    var ini = $(this);
    var kia_id = $(this).data("kia_id");
    if(kia_id != undefined){
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn bg-navy btn-flat',
        cancelButtonClass: 'btn btn-default btn-flat',
        buttonsStyling: false,
        reverseButtons: true
      }).then(function(result) {
        if (result) {
          $.ajax({
            url: '<?= base_url() ?>index.php/kpi_sla/delete_assign',
            type: 'POST',
            dataType: 'JSON',
            data: {
              kia_id: kia_id
            }
          })
          .done(function(data) {
            if(data.success){
              ini.closest('tr').remove();
              swal(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
            } else{
              notif("error", "sorry.. failed to delete");
            }
          })
          .fail(function() {
            notif("error", "sorry.. failed to delete");
          })
          .always(function() {
            console.log("complete");
          });
        }
      })
    } else{
      $(this).closest('tr').remove();
    }
  })

  $("#year").on("change", function(e){
    e.preventDefault();
    changeYear();
    dataParent();
  })

  $("#assigmentModal").on("show.bs.modal", function(e){
    var role_name = $(e.relatedTarget).data("role_name");
    var role_id = $(e.relatedTarget).data("role_id");
    $(this).find('.modal-title').text("Assign to "+role_name);
    $(this).find('#role_id').val(role_id);
    $(this).find('#listAssignKpi tbody tr').remove();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/get_exist_assigment/'+role_id,
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function()
      {
        $("#loaderModalAssign").fadeIn("fast");
      }
    })
    .done(function(data) {
      if(data.length > 0){
        $.each(data, function(i, item){
          var content = '<tr>'+
                          '<td>'+
                            '<input type="text" id="" class="form-control" readonly value="'+item.kia_name+'">'+
                            '<input type="hidden" id="kpi" class="form-control" readonly value="'+item.ki_id+'">'+
                          '</td>'+
                          '<td style="font-size: 25px;"> <a href="#" id="delKpi" data-kia_id="'+item.kia_id+'" class="text-danger">X</a> </td>'+
                        '</tr>';
          $("#listAssignKpi").find('tbody').prepend(content);
        })
      }
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      $("#loaderModalAssign").fadeOut(1000, function() {
        $("#content").show("fast");
      });
    });

  })

  $("#detailItem").on("show.bs.modal", function(e){
    var uom = $(e.relatedTarget).data("uom");
    var ki_name = $(e.relatedTarget).data("ki_name");
    var formula = $(e.relatedTarget).data("formula");
    var ki_id = $(e.relatedTarget).data("id");
    var target = $(e.relatedTarget).data("ki_target");
    var target_ytd = $(e.relatedTarget).data("ki_target_ytd");
    var weight = $(e.relatedTarget).data("weight");
    $(this).find('#ki_name_label').html(": &nbsp;"+ki_name);
    $(this).find('#ki_uom_label').html(": &nbsp;"+uom);
    $(this).find('#ki_uom').val(uom);
    $(this).find('#ki_id').val(ki_id);
    var ini = $(this);
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/detail_item/'+ki_id+'/'+$("#year").val(),
      type: 'GET',
      dataType: 'JSON',
    })
    .done(function(data) {
      var table = "";
      table += '<table class="table table-hover">';
      table += '<thead style="background-color:#05354D; color:#fff;">';
      table += '<tr>';
      table += '<th style="text-align:center;">Month</th>';
      table += '<th style="text-align:center;">Weight</th>';
      if(formula == 'Stabilize'){
          table += '<th style="text-align:center;">Upper</th>';
          table += '<th style="text-align:center;">Lower</th>';
          table += '<th style="text-align:center;">Upper YTD</th>';
          table += '<th style="text-align:center;">Lower YTD</th>';
      } else{
          table += '<th style="text-align:center;">Target</th>';
          table += '<th style="text-align:center;">Target YTD</th>';
      }
      // table += '<th>Weight</th>';
      table += '<th style="text-align:center;">Limit</th>';
      table += '</tr>';
      table += '</thead>';
      table += '<tbody>';
      if(data.length > 0){
        var arrayNo = 0;
        for (var i = 1; i <= 12; i++) {
          table += '<tr>';
          var valueId = (i == data[arrayNo].month) ? data[arrayNo].kid_id : "";
          table += '<td><input type="hidden" name="kid_id[]" value="'+valueId+'">'+getMonthName(i)+'</td>';
          var valueWeight = (i == data[arrayNo].month) ? data[arrayNo].kid_weight : "";
          table += '<td><input type="text" class="form-control free" name="weight[]" id="weight" value="'+valueWeight+'"/></td>';
          if(formula == 'Stabilize'){
              var valueTarget = (i == data[arrayNo].month) ? data[arrayNo].kid_target.split("|") : "";
              var valueTargetYtd = (i == data[arrayNo].month) ? data[arrayNo].kid_target_ytd.split("|") : "";
              table += '<td><input type="text" class="form-control no" name="upper[]" id="upper" value="'+((valueTarget[0] == "" || valueTarget[0] == undefined) ? "0" : valueTarget[0])+'"/></td>';
              table += '<td><input type="text" class="form-control no" name="lower[]" id="lower" value="'+((valueTarget[1] == "" || valueTarget[1] == undefined) ? "0" : valueTarget[1])+'"/></td>';
              table += '<td><input type="text" class="form-control no" name="upper_ytd[]" id="upper_ytd" value="'+((valueTargetYtd[1] == "" || valueTargetYtd[1] == undefined) ? "0" : valueTargetYtd[1])+'"/></td>';
              table += '<td><input type="text" class="form-control no" name="lower_ytd[]" id="lower_ytd" value="'+((valueTargetYtd[1] == "" || valueTargetYtd[1] == undefined) ? "0" : valueTargetYtd[1])+'"/></td>';
          } else{
              var valueTarget = (i == data[arrayNo].month) ? data[arrayNo].kid_target : "";
              var valueTargetYtd = (i == data[arrayNo].month) ? data[arrayNo].kid_target_ytd : "";
              table += '<td><input type="text" class="form-control no" name="target[]" id="target" value="'+((valueTarget == "") ? "0" : valueTarget)+'"/></td>';
              table += '<td><input type="text" class="form-control no" name="target_ytd[]" id="target_ytd" value="'+((valueTargetYtd === "" || valueTargetYtd === undefined  || valueTargetYtd === 'undefined') ? "0" : valueTargetYtd)+'"/></td>';
          }
          var valueLimit = (i == data[arrayNo].month) ? data[arrayNo].kid_limit : "";
          table += '<td><input type="text" class="form-control no" name="limit[]" id="limit" value="'+((valueLimit == "" || valueLimit === undefined  || valueLimit === 'undefined') ? "0" : valueLimit)+'"/></td>';
          table += '</tr>';
          arrayNo++;
        }
      } else {
        for (var i = 1; i <= 12; i++) {
          table += '<tr>';
          table += '<td>'+getMonthName(i)+'</td>';
          table += '<td><input type="text" class="form-control free" name="weight[]" value="'+weight+'" id="upper"/></td>';
          if(formula == 'Stabilize'){
              var newTarget = [0, 0];
              var newTargetYtd = [0, 0];
              if(target != "" && target != "0" && target != undefined){
                newTarget = target.split("|");
                newTargetYtd = target_ytd.split("|");
              }
              table += '<td><input type="text" class="form-control no" name="upper[]" value="'+newTarget[0]+'" id="upper"/></td>';
              table += '<td><input type="text" class="form-control no" name="lower[]" value="'+newTarget[1]+'" id="lower"/></td>';
              table += '<td><input type="text" class="form-control no" name="upper_ytd[]" value="'+newTargetYtd[0]+'" id="upper_ytd"/></td>';
              table += '<td><input type="text" class="form-control no" name="lower_ytd[]" value="'+newTargetYtd[1]+'" id="lower_ytd"/></td>';
          } else{
              table += '<td><input type="text" class="form-control no" name="target[]" value="'+(target === undefined ? 0 : target)+'" id="target"/></td>';
              table += '<td><input type="text" class="form-control no" name="target_ytd[]" value="'+(target_ytd === undefined ? 0 : target_ytd)+'" id="target_ytd"/></td>';
          }
          // table += '<td><input type="text" class="form-control no-free" name="weight[]" id="weight" /></td>';
          table += '<td><input type="text" class="form-control no" name="limit[]" id="limit"/></td>';
          table += '</tr>';
          arrayNo++;
        }
      }
      table += '</tbody>';
      table += '</table>';
      ini.find('#detailKpi').html(table);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function(data) {
      $("body").addClass('modal-open');
      $("#detailItem").css('display', 'block');
      $("#detailItem").css('padding-left', '17px');
    });

  });

  $("#addDetail").on("submit", function(e){
    e.preventDefault();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/manage_detail',
      type: 'POST',
      dataType: 'JSON',
      cache: false,
      processData: false,
      contentType: false,
      data: new FormData(this),
      beforeSend: function(){
        $("#save_form_detail").attr("disabled", "disabled").html("Please Wait");
      }
    })
    .done(function(data) {
      if(data.success){
        notif("success", "data saved");
      } else{
        notif("warning", "Internal Server Error");
      }
    })
    .fail(function() {
      notif("error", "Fatal Error");
    })
    .always(function() {
      $("#save_form_detail").attr("disabled", false).html("Save");
      $('#detailItem').modal('hide');
      $("body").removeClass('modal-open');
      $('.modal-backdrop').remove();
      allListWithDetail();
    });

  })

  // $(document).on("keyup keydown", ".no", function(e){
  //   if (event.shiftKey == true) {
  //       event.preventDefault();
  //   }
  //   if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190)
  //   {
  //     var value = $(this).val();
  //     var uom = $("#ki_uom").val();
      // if(uom == "percent"){
      //   if(value > 100){
      //     $(this).val("0");
      //     notif("warning", "Value must less than 100");
      //   }
      // } else if(uom == "index"){
      //   if(value > 1){
      //     $(this).val("0");
      //     notif("warning", "Value must less than 1");
      //   }
      // }
  //   } else {
  //       event.preventDefault();
  //   }
  //   if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
  //       event.preventDefault();
  // });

  $("#uom").on("change", function(e){
    toggleTargetWeight();
  });

  $("#parent").on("change", function(e){
    toggleTargetWeight();
  });

  $(document).on("keyup keydown", "#ki_weight", function(e){
    if (event.shiftKey == true) {
        event.preventDefault();
    }
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190)
    {
      var value = $(this).val();
      if(value > 100){
        $(this).val("0");
        notif("warning", "Value must less than 100");
      }
    } else {
        event.preventDefault();
    }
    if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
  });

  $(document).on("keyup keydown", "#ki_target", function(e){
    if (event.shiftKey == true) {
        event.preventDefault();
    }
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190)
    {
      var value = $(this).val();
      var uom = $("#uom").val();
      // if(uom == "percent"){
      //   if(value > 100){
      //     $(this).val("0");
      //     notif("warning", "Value must less than 100");
      //   }
      // } else if(uom == "index"){
      //   if(value > 1){
      //     $(this).val("0");
      //     notif("warning", "Value must less than 1");
      //   }
      // }
    } else {
        event.preventDefault();
    }
    if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
  });

  $(document).on("keyup keydown", ".no-free", function(e){
    if (event.shiftKey == true) {
        event.preventDefault();
    }
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190)
    {
      var value = $(this).val();
      var uom = $("#ki_uom").val();
      if(uom == "percent"){
        if(value > 100){
          $(this).val("0");
          notif("warning", "Value must less than 100");
        }
      } else if(uom == "index"){
        if(value > 1){
          $(this).val("0");
          notif("warning", "Value must less than 1");
        }
      }
    } else {
        event.preventDefault();
    }
    if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
  })

  /*$(document).on("keyup keydown", ".free", function(e){
    if (event.shiftKey == true) {
        event.preventDefault();
    }
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190)
    {
      var value = $(this).val();
      if(value > 100){
        $(this).val("0");
        notif("warning", "Value must less than 100");
      }
    } else {
        event.preventDefault();
    }
    if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
  })*/
  $('.free').keypress(function(event) {
    if((event.which > 47 && event.which < 58) || (event.which == 46 || event.which == 8)){
    }else{
    event.preventDefault();
    }
    }).on('paste', function(event) {
        event.preventDefault();
  });

  $('.no').keypress(function(event) {
    if((event.which > 47 && event.which < 58) || (event.which == 46 || event.which == 8)){
    }else{
    event.preventDefault();
    }
    }).on('paste', function(event) {
        event.preventDefault();
  });



  $("#actualMonth").on("change", function(e){
    e.preventDefault();
    getDataHierarchyActual();
  })


  function append_unit(kpi_unit_id = null)
  {
    var url_plus = ((kpi_unit_id != null) ? "/"+kpi_unit_id : "");
    $.ajax({
      url: '<?= base_url() ?>index.php/user_master/user_json',
      type: 'GET',
      dataType: 'JSON',
    })
    .done(function(data) {
      if(kpi_unit_id == null){
        var select = "<select class='form-control' name='user_id[]' required>";
        select += "<option value=''>Chose User..</option>";
        $.each(data, function(index, result){
          select += "<option value='"+result.user_id+"'>"+result.username+"</option>";
        })
        select += "</select>";
        var content = "<tr>"+
        "<td>"+select+"</td>"+
        "<td> <button class='btn btn-danger btn-flat' id='delete_user'><i class='fa fa-trash'></i></button> </td>"+
        "</tr>";
        $("#content_user").append(content);
      } else {
        $.ajax({
          url: '<?= base_url() ?>index.php/user_master/user_json'+url_plus,
          type: 'GET',
          dataType: 'JSON',
        })
        .done(function(data_dua) {

          $.each(data_dua, function(index, result_dua) {
            console.log(result_dua)
            var select = "<select class='form-control' name='user_id[]' required>";
            select += "<option value=''>Chose User..</option>";
            select += "<option value='"+result_dua.user_id+"' selected>"+result_dua.username+"</option>";
            $.each(data, function(index, result){
              select += "<option value='"+result.user_id+"'>"+result.username+"</option>";
            })
            select += "</select>";
            var content = "<tr>"+
            "<td>"+select+"</td>"+
            "<td> <button class='btn btn-danger btn-flat' id='delete_user' data-kpi_unit_id = '"+result_dua.user_id+"'><i class='fa fa-trash'></i></button> </td>"+
            "</tr>";
            $("#content_user").append(content);
          });
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });
      }
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  }


  $("#append_user").on("click", function(e){
    e.preventDefault();
    append_unit();
  })

  function delete_user(ini, id)
  {
    $.ajax({
      url: '<?= base_url() ?>index.php/user_master/delete_unit',
      type: 'POST',
      dataType: 'JSON',
      data: {id: id}
    })
    .done(function(data) {
      ini.closest('tr').remove();
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  }

  $(document).on("click", "#delete_user", function(e){
    e.preventDefault();
    var id = $(this).data("kpi_unit_id");
    if(id != undefined || id != null){
      var r = confirm("Are you sure ??");
      if (r == true) {
        delete_user($(this), id);
      }
    } else {
      $(this).closest('tr').remove();
    }
  })

  $("#formAddUser").parsley();
  $("#formAddUser").on("submit", function(e){
    e.preventDefault();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/insert_unit_kpi',
      type: 'POST',
      dataType: 'JSON',
      cache: false,
      processData: false,
      contentType: false,
      data: new FormData(this),
      beforeSend: function(){
        $("#formAddUser").find('#save').attr('disabled', 'disabled');
      }
    })
    .done(function(data) {
      if(data.success){
        notif("success", "Data inserted");
        $("#cancelFormUser").trigger('click');
      }
    })
    .fail(function() {
      $("#formAddUser").find('#save').attr('disabled', false);
      $("#list_user").val("");
      $("#cancelFormUser").trigger('click');
    })
    .always(function() {
      $("#formAddUser").find('#save').attr('disabled', false);
      $("#list_user").val("");
      AllListUnit();
    });

  })

  $("#addKpiForm").parsley();
  $("#addKpiForm").on("submit", function(e){
    e.preventDefault();
    if($("#user_id").val() == ""){
      notif("error", "user not defined");
    } else{
      $.ajax({
        url: '<?= base_url() ?>index.php/kpi_sla/insert',
        type: 'POST',
        dataType: 'JSON',
        data: {
          ki_id : $("#kpi_id").val(),
          ki_name : $("#kpi").val(),
          ki_uom : $("#uom").val(),
          ki_parent_id : $("#parent").val(),
          kf_id : $("#formula").val(),
          ki_year: $("#year").val(),
          ki_author: $("#user_id").val(),
          ki_weight: $("#ki_weight").val(),
          ki_target: $("#ki_target").val(),
          ki_target_ytd: $("#ki_target_ytd").val(),
          kpi_group_id: $("#kpi_group_id_select").val()
        },
        beforeSend: function(){
          $("#addKpiForm").find('#save').attr('disabled', 'disabled');
        }
      })
      .done(function(data) {
        $("#parent").find('.item').remove();
        if(data.success){
          notif("success", "Data inserted");
          $("#parent").append(data.newData);
          getDataHierarchyActual();
        }
      })
      .fail(function() {
        notif("error", "Sorry, data failed to insert");
      })
      .always(function() {
        $("#cancel").trigger('click');
        console.log("complete");
        $("#addKpiForm").find('#save').attr('disabled', false);
        $("#kpi").val("");
        $("#uom").val("").attr('disabled', false);
        $("#parent").val("");
        $("#formula").val("").attr('disabled', false);
        $("#ki_weight").val("");
        $("#inputKiWeight").fadeOut();
        $("#inputKiTarget").fadeOut();
        $("#inputKiTargetYtd").fadeOut();
        $("#inputKiTargetStabilize").fadeOut();
        $("#inputKiTargetStabilizeYtd").fadeOut();
        $("#kpi_id").val("");
        $("#ki_target").val("");
        $("#ki_target_ytd").val("");
        $("#target_upper").val("");
        $("#target_upper_ytd").val("");
        $("#target_lower").val("");
        $("#target_lower_ytd").val("");
        allListWithDetail();
      });
    }

  })

  $("#addKpi").on("click", function(e){
    e.preventDefault();
    $("#boxForm").fadeIn(1000, function() {
      load_all_group();
      $("#loadingUom").fadeOut(1000);
    });
  })

  $("#addUnit").on("click", function(e){
    e.preventDefault();
    $("#list_user").addClass('select2');
    $("#boxFormUser").fadeIn(1000, function() {
      $("#loadingUomUser").fadeOut(1000);
    });
  })

  $("#cancelFormUser").on("click", function(e){
    e.preventDefault();
    $("#unit_name").val("");
    $("#kpi_unit_id").val("");
    $("#content_user").find('tr').remove();
    $(this).closest('#boxFormUser').fadeOut(1000, function() {
      $("#loadingUomUser").fadeIn(1000);``
    });
  })

  $("#cancel").on("click", function(e){
    e.preventDefault();
    $(this).closest('#boxForm').fadeOut(1000, function() {
      $("#loadingUom").fadeIn(1000);
      $("#addKpiForm").find('#save').attr('disabled', false);
      $("#kpi").val("");
      $("#uom").val("").attr('disabled', false);
      $("#parent").val("");
      $("#formula").val("").attr('disabled', false);
      $("#ki_weight").val("");
      $("#inputKiWeight").fadeOut();
      $("#kpi_id").val("");
    });
  })

  $("#year").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years"
  });

  $('#year').on('changeDate', function(ev){
      $(this).datepicker('hide');
  });



</script>
