<style media="screen">
  .box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
    display: inline-block;
    font-size: 20px;
    font-weight: bold;
    margin: 0;
    line-height: 1;
  }
  span.select2{
    width: 100% !important;
  }

  option span {
    font-weight: bold;
  }

  .table-gmf thead {
    background-color: #05354D;
    color: #fff;
  }

  .optionChild {
    padding-left: 15px;
  }

  .loading {
      position: absolute;
      left: 50%;
      top: 50%;
      z-index: 1;
      width: 150px;
      height: 150px;
      margin: -75px 0 0 -75px;
      border: 16px solid #f3f3f3;
      border-radius: 50%;
      border-top: 16px solid #3498db;
      width: 120px;
      height: 120px;
      -webkit-animation: spin 2s linear infinite;
      animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }

    /* Add animation to "page content" */
    .animate-bottom {
      position: relative;
      -webkit-animation-name: animatebottom;
      -webkit-animation-duration: 1s;
      animation-name: animatebottom;
      animation-duration: 1s
    }

    @-webkit-keyframes animatebottom {
      from { bottom:-100px; opacity:0 }
      to { bottom:0px; opacity:1 }
    }

    @keyframes animatebottom {
      from{ bottom:-100px; opacity:0 }
      to{ bottom:0; opacity:1 }
    }

    #myDiv {
      display: none;
      text-align: center;
    }

    .treegrid-indent {
        width: 0px;
        height: 16px;
        display: inline-block;
        position: relative;
    }

    .treegrid-expander {
        width: 0px;
        height: 16px;
        display: inline-block;
        position: relative;
        left:-17px;
        cursor: pointer;
    }

    /* button float */

    .label-container{
    	position:fixed;
    	bottom:48px;
    	right:105px;
    	display:table;
    	visibility: hidden;
    }

    .label-text{
    	color:#FFF;
    	background:rgba(51,51,51,0.5);
    	display:table-cell;
    	vertical-align:middle;
    	padding:10px;
    	border-radius:3px;
    }

    .label-arrow{
    	display:table-cell;
    	vertical-align:middle;
    	color:#333;
    	opacity:0.5;
    }

    .float{
    	position:fixed;
    	width:60px;
    	height:60px;
    	bottom:40px;
    	right:40px;
    	background-color:#06C;
    	color:#FFF;
    	border-radius:50px;
    	text-align:center;
    	box-shadow: 2px 2px 3px #999;
    }

    .my-float{
    	font-size:24px;
    	margin-top:18px;
    }

    a.float + div.label-container {
      visibility: hidden;
      opacity: 0;
      transition: visibility 0s, opacity 0.5s ease;
    }

    a.float:hover + div.label-container{
      visibility: visible;
      opacity: 1;
    }
</style>

<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>


<section class="content">
  <div class="box">
    <div class="box-body">
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Year</label>
            <input type="text" class="form-control" id="year" name="year" value="<?= date("Y") ?>">
            <input type="hidden" class="form-control" id="user_id" name="user_id" value="" readonly>
            <input type="hidden" class="form-control" id="role_id" name="role_id" value="" readonly>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Management Item KPI</h3>
      <div class="box-tools pull-right">
        <?php if ($isLocked == false || $role_access == "Super Admin" || $new_role_access == "td_corporate"): ?>
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
        <?php endif; ?>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <?php if($isLocked == false || $role_access == "Super Admin"){ ?>
          <div class="col-md-2">
          <button type="button" class="btn bg-navy btn-flat form-control" id="addKpi" name="button"><i class="fa fa-plus-circle"></i> &nbsp; Add KPI </button>
        </div>
        <?php }else{?>
        <?php } ?>
        <div class="col-md-6">
          <div class="box box-solid" id="boxForm" style="display:none;">
            <div class="box-header bg-navy">
              <h3 class="box-title">Input KPI</h3>
            </div>
            <div class="box-body">

              <form action="index.html" method="post" id="addKpiForm">
                <input type="hidden" id="kia_id_detail" name="kia_id_detail" hidden>
                    <div class="col-md-12">
                      <div class="form-group">
                          <label>KPI</label>
                          <input type="text" class="form-control" id="kpi" name="kpi" placeholder="KPI" required>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label >UoM</label>
                        <select class="form-control " id="uom" name="uom" required>
                          <option value="">Chose UoM</option>
                          <option value="percent">percent (%)</option>
                          <option value="index">index</option>
                          <option value="MUSD">MUSD</option>
                          <option value="USD">USD</option>
                          <option value="Times">Times</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label >Parent</label>
                        <div class="load_parent">
                        </div>
                        <!-- <select class="form-control" id="parent" name="level">
                          <option data-level="1" value="">Is Parent</option>
                          <?php
                            echo(generatePageTreeUnit($allItem));
                           ?>
                        </select> -->
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label >Formula</label>
                        <input type="hidden" id="parent_save" name="parent_save" value="1">
                        <select class="form-control" id="formula" name="formula" required>
                          <option value="">Chose Formula</option>
                          <?php foreach ($formula as $data): ?>
                            <option value="<?= $data->kf_id ?>"><?= $data->kf_name ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div id="group" class="col-md-12">
                      <div class="form-group">
                        <label>Group</label>
                        <select class="form-control" id="kpi_group_id_select_insert" name="kpi_group_id_select_insert">
                          <option value="">Chose..</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12" id="target_field">
                      <div class="form-group">
                          <label>Target</label>
                          <input type="text" class="form-control" id="target_unit" value="0" name="target_unit" placeholder="Target" required>
                      </div>
                    </div>
                    <div class="col-md-12" id="target_field_ytd">
                      <div class="form-group">
                          <label>Target Year to Date</label>
                          <input type="text" class="form-control" id="target_unit_ytd" value="0" name="target_unit_ytd" placeholder="Target" required>
                      </div>
                    </div>
                    <div class="col-md-12" id="upper_field">
                      <div class="form-group">
                          <label>Upper</label>
                          <input type="text" class="form-control" id="upper_unit" value="0" name="upper_unit" placeholder="Upper" required>
                      </div>
                    </div>
                    <div class="col-md-12" id="lower_field">
                      <div class="form-group">
                          <label>Lower</label>
                          <input type="text" class="form-control" id="lower_unit" value="0" name="lower_unit" placeholder="Lower" required>
                      </div>
                    </div>
                    <div class="col-md-12" id="upper_field_ytd">
                      <div class="form-group">
                          <label>Upper Year to Date</label>
                          <input type="text" class="form-control" id="upper_unit_ytd" value="0" name="upper_unit_ytd" placeholder="Upper" required>
                      </div>
                    </div>
                    <div class="col-md-12" id="lower_field_ytd">
                      <div class="form-group">
                          <label>Lower Year to Date</label>
                          <input type="text" class="form-control" id="lower_unit_ytd" value="0" name="lower_unit_ytd" placeholder="Lower" required>
                      </div>
                    </div>
                    <div class="col-md-12" id="weight_field">
                      <div class="form-group">
                          <label>Weight</label>
                          <input type="text" class="form-control" id="weight_unit" value="0" name="weight_unit" placeholder="Weight" required>
                      </div>
                    </div>
                      <button type="submit" class="btn btn-flat bg-navy pull-right" id="save" name="save"><i class="fa fa-save"></i> &nbsp; Save</button>
                      <button type="button" style="margin-right:10px;" class="btn btn-flat btn-default pull-right" id="cancel" name="cancel"><i class="fa fa-arrow-left"></i> &nbsp; Cancel</button>
                  </form>
            </div>
            <!-- Loading (remove the following to stop the loading)-->
            <div id="loadingUom" class="overlay" >
              <i class="fa fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
          </div>
        </div>

      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <p style="font-size: 20px;">LIST KPI</p>
          <?php if ($isLocked == false || $role_access == "Super Admin" || $new_role_access == "td_corporate"): ?>
            <button type="button" name="button" id="copy" class="btn bg-navy btn-flat"><i class="fa fa-copy"></i> &nbsp; Duplicate KPI from other years</button>
          <?php endif; ?>
          <br><br>
          <div style="display:none;" class="loading" id="loader"></div>
          <table style="display:none;" class="table table-hover table-bordered table-gmf" id="datatableScrollale">
            <thead>
              <tr>
                  <th>KPI</th>
                  <th>UoM</th>
                    <th>Formula</th>
                    <th>Weight</th>
                    <th>Target</th>
                    <th>Target YTD</th>
                    <th>Upper</th>
                    <th>Lower</th>
                    <th>Upper YTD</th>
                    <th>Lower YTD</th>
                    <th>Limit</th>

                    <?php if ($isLocked == false || $role_access == "Super Admin" || $new_role_access == "td_corporate"): ?>
                      <th>Action</th>
                    <?php endif; ?>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td></td>
                </tr>
              </tbody>
        </table>
          </div>
        </div>
      </div>
    </div>

  <!-- KPI GROUPING -->
  <?php if ($isLocked == false || $role_access == "Super Admin" || $new_role_access == "td_corporate"): ?>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Management Grouping</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <p style="font-size: 20px;">LIST GROUP</p>
            <div class="isi" style="position:relative;">
              <div class="overlay-box" id="loader2" style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                <div style="" class="loading"></div>
              </div>
              <table class="table table-hover table-bordered" id="groupList" style="width:70%; margin:0 auto;">
                <thead class="bg-navy">
                  <tr>
                    <th>No</th>
                    <th>Group Name</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <!-- actual KPI Unit-->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Management Actual KPI</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>

    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div style="border: 1px solid #bfbcbc; padding:  10px;">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Months</label>
                  <select class="form-control " name="actualMonth" id="actualMonth">
                    <?php
                      $months = range(1, 12);
                      foreach ($months as $month) {
                        $selected = (date("m") == $month) ? "selected" : "";
                        echo "<option value='".$month."' ".$selected.">".convert_month($month)."</option>";
                      }
                     ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row" style="margin-top: 20px;">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-2">
              <p style="font-size:20px;">List KPI</p>
              <hr style="border: 0;border-top: 1px solid #bfbcbc;margin-top:0px;margin-bottom:0px;">
            </div>
          </div>
          <div style="display:none;" class="loading" id="loaderListUnit"></div>
          <!-- <table class="table table-hover table-bordered table-gmf" id="actualTable">
            <thead>
              <tr>
                <th>KPI</th>
                <th>UoM</th>
                <th>Actual</th>
                <th>Actual YTD</th>
                <th>Achievement YTD</th>
                <th>Score YTD</th>
                <th>Total Score YTD</th>
                <th>Achievement</th>
                <th>Score</th>
                <th>Total Score</th>
                <th>Action </th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table> -->
          <div id="all_total_score"></div>
          <table class="table table-hover table-bordered table-gmf" id="actualTable">
            <thead>
              <tr>
                <th>KPI</th>
                <th>UoM</th>

                <th>Actual</th>
                <th>Target</th>
                <th>Achievement</th>
                <th>Score</th>
                <th>Final Score</th>

                <th>Actual YTD</th>
                <th>Target YTD</th>
                <th>Achievement YTD</th>
                <th>Score YTD</th>
                <th>Final Score YTD</th>

                <th>Action </th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


  </section>

  <div class="modal fade" id="detailItem" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form class="" action="index.html" id="addDetail" method="post">
          <div class="modal-header">
            <button type="button" class="close" id="closemodaldetail" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="">Edit KPI</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" name="author_detail" id="author_detail" value="">
            <input type="hidden" name="year_input" id="year_input" value="">
            <input type="hidden" name="ki_id" id="ki_id" value="">
            <input type="hidden" name="ki_uom" id="ki_uom" value="">
            <div class="row">
              <div class="col-md-2">
                <p>KPI</p>
                <p>UOM</p>
                <!-- <p>Level</p> -->
              </div>
              <div class="col-md-10">
                <p id="ki_name_label">: KPI</p>
                <p id="ki_uom_label">UOM</p>
                <!-- <p><select class="form-control" id="level_change" name="level_change">
                  <option value="">Is Parent</option>
                  <?php
                    // echo(generatePageTreeUnit($allItem));
                   ?>
                </select></p> -->
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div id="detailKpi"></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
            <button type="submit" class="btn bg-navy btn-flat" id="save_form_detail" >Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div id="button_float_place"></div>


  <!-- <div class="modal fade" id="assigmentModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="margin-left: 50%; margin-top: 20%;">
      <form id="assigmentForm" action="index.html" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id=""></h4>
          </div>
          <div class="modal-body">
            <div style="display:none;" class="loading" id="loaderModalAssign"></div>
            <div id="content" class="row" style="display:none;">
              <div class="col-md-12">
                <button type="button" class="btn btn-flat btn-navy" id="appendKpi" name="appendKpi"><i class="fa fa-plus-circle"></i> &nbsp; Add</button>
                <input type="hidden" name="role_id" id="role_id" value="">
                <table class="table table-hover" id="listAssignKpi">
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
            <button type="submit" id="submitAssign" class="btn btn-flat bg-navy">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div> -->
  <div class="modal fade" id="actualModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="submit_actual" action="index.html" method="post">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="">Actual KPI</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <table class="table table-hover">
                  <tbody>
                    <tr>
                      <th class="text-left">KPI Name</th>
                      <th class="text-center">Actual</th>
                      <th class="text-center">Actual Year to Date</th>
                    </tr>
                    <tr>
                      <th class="text-left" id="kpi_name"></th>
                      <th class="text-center"><input type="text" name="kiad_actual" id="kiad_actual" class="form-control" required/></th>
                      <th class="text-center">
                        <input type="text" name="kiad_actual_ytd" id="kiad_actual_ytd" class="form-control" required readonly/>
                        <div id="loading" style="display:none;">
                          <i class="fa fa-refresh fa-spin"></i> Please Wait ..
                        </div>
                      </th>
                      <th>
                          <input type="hidden" name="uom" id="uom_act" class="form-control" required/>
                          <input type="hidden" name="month_act" id="month_act" class="form-control" required/>
                          <input type="hidden" name="kiad_id_act" id="kiad_id_act" class="form-control" required/>
                          <input type="hidden" name="kia_name_act" id="kia_name_act" class="form-control" required/>
                          <input type="hidden" name="fa_function_name" id="fa_function_name" class="form-control" required/>
                      </th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
            <button type="submit" id="submit_actual_button" class="btn bg-navy btn-flat">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="submit_delete" action="index.html" method="post">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="">Validation</h4>
          </div>
          <div class="modal-body">
            <h3>Are you sure to delete this data?</h3>
            <input type="hidden" id="kiad_id_delete" name="kiad_id_delete">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn bg-navy btn-flat" data-dismiss="modal">No</button>
            <button type="submit" class="btn btn-default btn-flat">Yes</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="copy_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Duplicate Engine</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>From year</label>
                <input type="text" class="form-control" id="copy_year1" value="<?= date("Y") ?>" readonly>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>To Year</label>
                <input type="text" class="form-control" id="copy_year2" value="<?= date("Y") ?>" readonly>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
          <button type="button" id="save_copy" class="btn btn-flat bg-navy ">Save</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="chose_unit" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-navy">
          <h4 class="modal-title" id="">Chose unit to display the KPI item</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Chose Unit </label>
                <select class="form-control select2" id="userSelected" name="userSelected">
                  <option value="">Chose..</option>
                  <?php foreach ($allRole as $role): ?>
                    <option value="<?= $role->kpi_unit_id ?>"><?= $role->kpi_unit_name ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="choseUnit" class="btn btn-flat bg-navy">Chose</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_group" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id=""></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <form id="add_to_group" action="index.html" method="post">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Chose KPI</label>
                  <input type="hidden" id="ki_id_select" value="">
                  <select class="form-control" id="kpi_group_id_select" required>
                    <option value="">Chose..</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <label>&nbsp;</label>
                <button type="submit" class="form-control btn btn-flat bg-navy" id="add_list"> <i class="fa fa-save"></i> Add to group </button>
              </div>
            </form>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="isi" style="position:relative;">
                <div class="overlay-box" id="loaderMember" style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                  <div style="" class="loading"></div>
                </div>
                <table id="table_group_member" class="table table-hover table-bordered">
                  <thead>
                    <tr>
                      <th class="text-center">No</th>
                      <th class="text-center">KPI Name</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">

  function load_all_group()
  {
    $("#kpi_group_id_select_insert").find('option').remove();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_sla/jsonGroup',
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
          $("#kpi_group_id_select_insert").html("<option value=''>Please Wait</option>");
      }
    })
    .done(function(data) {
      $("#kpi_group_id_select_insert").find('option').remove();
      $("#kpi_group_id_select_insert").append("<option value=''>Chose</option>");
      $.each(data, function(index, list){
        $("#kpi_group_id_select_insert").append("<option value='"+list.kpi_group_id+"'>"+list.kpi_group_name+"</option>");
      })
    })
    .fail(function() {
      notif("error", "Failed to load group");
      $("#kpi_group_id_select").find('option').remove();
      $("#kpi_group_id_select").append("<option value=''>Error</option>");
    })
    .always(function() {

    });

  }

  function all_total_score()
  {
    $.ajax({
      url: '<?= base_url() ?>index.php/Kpi_unit_temp/get_all_total',
      type: 'GET',
      dataType: 'JSON',
      data: {
        month: $("#actualMonth").val(),
        role_id: (($("#userSelected").val() == "") ? localStorage.getItem("new_role_id") : $("#userSelected").val()),
        year : $("#year").val()
      }
    })
    .done(function(data) {
      $("#all_total_score").html("<h4>Total Score : "+data.total+"%</h4>")
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  }

  $(document).ready(function() {

    showModal();
    $("#user_id").val(localStorage.getItem("user_id"));
    $("#author_detail").val(localStorage.getItem("user_id"));
    $("#role_id").val(localStorage.getItem("new_role_id"));
    allListWithDetail();
    changeYear();
    $("#weight_field").addClass("hidden");
    $("#target_field").removeClass("hidden");
    $("#upper_field").addClass("hidden");
    $("#lower_field").addClass("hidden");
    $("#target_field_ytd").removeClass("hidden");
    $("#upper_field_ytd").addClass("hidden");
    $("#lower_field_ytd").addClass("hidden");
    //UNIT
    year_load();
    loadGrup();
    getDataHierarchyActual();
    all_total_score();
    load_all_group();
  });

  function year_load(){
    var year = $("#year").val();
    var role = (($("#userSelected").val() != "") ? $("#userSelected").val() : $('#role_id').val());
    $('.load_parent').load('<?= base_url() ?>index.php/kpi_unit/allkpi/'+year+'/'+role);
  }
  $("#copy").on("click", function(e){
    e.preventDefault();
    $("#copy_modal").modal("show");
  })

    $("#choseUnit").on("click", (e) => {
      e.preventDefault();
      var userSelected = $("#userSelected").val();
      if(userSelected == ""){
        notif("warning", "Please select unit");
      } else{
        $("#chose_unit").modal("hide");
        allListWithDetail();
        getDataHierarchyActual();
        loadGrup();
        year_load();
      }
    })

    $(document).on("click", "#choseUnitButton", (e) => {
      e.preventDefault();
      showModal();
    })

    $("#add_to_group").parsley();
    $("#add_to_group").on("submit", function(e){
      e.preventDefault();
      submit_member();
    })

    function submit_member()
    {
      var kpi_group_id = $("#kpi_group_id_select").val();
      var ki_id = $("#ki_id_select").val();
      var role = (($("#userSelected").val() == "") ? $('#role_id').val() : $("#userSelected").val());
      $.ajax({
        url: '<?= base_url() ?>index.php/kpi_unit/save_member',
        type: 'POST',
        dataType: 'JSON',
        data: {kpi_group_id: kpi_group_id, ki_id: ki_id, role: role},
        beforeSend: function(){
          $("#add_list").prop('disabled', true).html("<i class='fa fa-refresh fa-spin'></i> Please Wait ..");
        }
      })
      .done(function(data) {
        if(data.success){
          notif("success", "Data Saved");
        } else{
          notif("success", "Failed to save");
        }
      })
      .fail(function() {
        notif("success", "Failed to save");
      })
      .always(function() {
        $("#add_list").prop('disabled', false).html("<i class='fa fa-save'></i> Save");
        list_parrent_kpi(role);
        load_member(ki_id,role);
      });

    }

    function loadGrup()
    {
      $("#groupList").find('tbody tr').remove();
      $.ajax({
        url: '<?= base_url()?>index.php/kpi_unit/listGrup',
        type: 'GET',
        dataType: 'JSON',
        data : {
          role: (($("#userSelected").val() == "") ? $('#role_id').val() : $("#userSelected").val())
        },
        beforeSend: function(){
          $("#loader2").fadeIn(1000);
        }
      })
      .done(function(data) {
        $("#groupList").find('tbody').append(data.html);
      })
      .fail(function() {
        notif("error", "failed to load group list");
      })
      .always(function(data) {
        $("#loader2").fadeOut(1000);
      });

    }

    function showModal()
    {

      var new_role = localStorage.getItem("new_role");
      var role = localStorage.getItem("role");
      if(role == "Super Admin" || new_role == "td_corporate"){
        console.log(new_role, role);
        $("#chose_unit").modal({
          backdrop: 'static',
          keyboard: false
        });
        $("#chose_unit").modal("show");
        var button = '<a href="#" id="choseUnitButton" class="bg-navy float"><i class="fa fa-users my-float"></i></a>'+
                      '<div class="label-container">'+
                        '<div class="label-text">Click to chose unit</div>'+
                        '<i class="fa fa-play label-arrow"></i>'+
                       '</div>';
        $("#button_float_place").html(button);
      }
    }

    $("#copy_year1, #copy_year2").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });

    $(document).on("click", "#manage_grup", function(e){
      e.preventDefault();
      var id = $(this).data("id");
      var name = $(this).data("name");
      var role = $(this).data("roleid");
      load_member(id, role);
      openModal(id, name, role);
    })

    function openModal(id, name, role)
    {
        $("#modal_group").modal("show");
        $("#modal_group").find('.modal-title').text("Manage "+name);
        list_parrent_kpi(role);
        $("#ki_id_select").val(id);
    }

    function load_member(id, role)
    {
      $("#table_group_member").DataTable().clear().draw();
      $.ajax({
        url: '<?= base_url() ?>index.php/kpi_unit/member_group/'+id+'/'+role,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function(){
          $("#loaderMember").fadeIn("fast");
        }
      })
      .done(function(data) {
        $("#loaderMember").fadeOut(2000);
        var no = 1;
        $.each(data, function(index, list){
          var row = $("#table_group_member").DataTable().row.add(
            [
              "<center>"+no+"</center>",
              "<center>"+list.kia_name+"</center>",
              "<center><button type='button' class='btn btn-flat btn-danger' onclick=remove_member("+list.kia_id+")><i class='fa fa-trash'></i></button></center>"
            ]
          ).draw().node();
          no++;
        })
      })
      .fail(function() {
        console.log("error");
      })
      .always(function(data) {
        console.log(data);
      });

    }

    function list_parrent_kpi(role)
    {
      $('#kpi_group_id_select').find('option').remove();
      $.ajax({
        url: '<?= base_url() ?>index.php/kpi_unit/list_parent/'+role,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function(){
          $('#kpi_group_id_select').append($('<option>', {value:'', text:'Please Wait ..'}));
        }
      })
      .done(function(data) {
        $('#kpi_group_id_select').find('option').remove();
        $('#kpi_group_id_select').append($('<option>', {value: '', text: 'Chose'}));
        $.each(data, function(index, list){
          $('#kpi_group_id_select').append($('<option>', {value: list.kia_id, text: list.kia_name}));
        })
      })
      .fail(function() {
        notif("error", "falied load parent");
        $('#kpi_group_id_select').find('option').remove();
        $('#kpi_group_id_select').append($('<option>', {value: "", text: "error"}));
      })
      .always(function(data) {

      });

    }

    $("#save_copy").on("click", function(e){
      e.preventDefault();
      var start = $("#copy_year1").val();
      var end = $("#copy_year2").val();

      if(start == end){
        notif("error", "Sorry Can Not Duplicate In The Same Year");
        return;
      }

      var ini = $(this);
      $.ajax({
        url: '<?= base_url() ?>index.php/kpi_unit/duplicate',
        type: 'POST',
        dataType: 'JSON',
        data: {copy_year1: $("#copy_year1").val(), copy_year2: $("#copy_year2").val()},
        beforeSend: function(){
          ini.attr('disabled', true).text("please wait..");
        }
      })
      .done(function(data) {
        if(data.success){
          notif("success", "Data successfully duplicated");
        } else{
          notif("error", "Data failed duplicated");
        }
      })
      .fail(function() {
        notif("error", "Data failed duplicated");
      })
      .always(function() {
        ini.attr('disabled', false).text("Save");
        $("#copy_modal").modal("hide");
      });

      allListWithDetail();
      getDataHierarchyActual();

    })

  $("#year").on("change",function(e){
    e.preventDefault();
    year_load();
    allListWithDetail();
    getDataHierarchyActual();
  })

    function allListWithDetail()
    {
      $.ajax({
        url: '<?= base_url() ?>index.php/kpi_unit/list_with_detail',
        type: 'POST',
        dataType: 'JSON',
        data: {
          year: $("#year").val(),
          role: (($("#userSelected").val() == "") ? $('#role_id').val() : $("#userSelected").val())
        },
        beforeSend: function(){
          $("#loader").fadeIn(1000);
        }
      })
      .done(function(data) {
        $("#loader").fadeOut(1000, function() {
          $("#datatableScrollale").fadeIn(1000);
        });
        $("#datatableScrollale").find('tbody').html(data.htmlBuild);
      })
      .fail(function() {
        notif("error", "failed to load list kpi");
      })
      .always(function() {
        var
            $table = $('#datatableScrollale'),
            rows = $table.find('tr');

        rows.each(function (index, row) {
            var
                $row = $(row),
                level = $row.data('level'),
                id = $row.data('id'),
                $columnName = $row.find('td[data-column="name"]'),
                children = $table.find('tr[data-parent="' + id + '"]');

            if (children.length) {
                var expander = $columnName.prepend('' +
                    '<span class="treegrid-expander glyphicon glyphicon-chevron-down"></span>' +
                    '');

                // children.hide();

                expander.on('click', function (e) {
                    var $target = $(e.target);
                    if ($target.hasClass('glyphicon-chevron-right')) {
                        $target
                            .removeClass('glyphicon-chevron-right')
                            .addClass('glyphicon-chevron-down');

                        children.show();
                    } else {
                        $target
                            .removeClass('glyphicon-chevron-down')
                            .addClass('glyphicon-chevron-right');

                        reverseHide($table, $row);
                    }
                });
            }

            $columnName.prepend('' +
                '<span class="treegrid-indent" style="width:' + 15 * level + 'px"></span>' +
                '');
        });

        reverseHide = function (table, element) {
            var
                $element = $(element),
                id = $element.data('id'),
                children = table.find('tr[data-parent="' + id + '"]');

            if (children.length) {
                children.each(function (i, e) {
                    reverseHide(table, e);
                });

                $element
                    .find('.glyphicon-chevron-down')
                    .removeClass('glyphicon-chevron-down')
                    .addClass('glyphicon-chevron-right');

                children.hide();
            }
        };
      });

    }




    function getDataHierarchyActual()
    {
      all_total_score();
      $.ajax({
        url: '<?= base_url() ?>index.php/Kpi_unit_temp/getDataActual',
        type: 'GET',
        dataType: 'JSON',
        data: {
              month: $("#actualMonth").val(),
              role_id: (($("#userSelected").val() == "") ? localStorage.getItem("new_role_id") : $("#userSelected").val()),
              year : $("#year").val()
            },
            beforeSend: function(){
              $("#loaderListUnit").fadeIn();
            }
          })
          .done(function(data) {
          $("#loaderListUnit").fadeOut();
          $("#actualTable").find('tbody').html(data.content);
        })
        .fail(function() {
          notif("error", "Something make your data failed to load");
        })
        .always(function() {
          var
              $table = $('#actualTable'),
              rows = $table.find('tr');

          rows.each(function (index, row) {
              var
                  $row = $(row),
                  level = $row.data('level'),
                  id = $row.data('id'),
                  $columnName = $row.find('td[data-column="name"]'),
                  children = $table.find('tr[data-parent="' + id + '"]');

              if (children.length) {
                  var expander = $columnName.prepend('' +
                      '<span class="treegrid-expander glyphicon glyphicon-chevron-down"></span>' +
                      '');

                  // children.hide();

                  expander.on('click', function (e) {
                      var $target = $(e.target);
                      if ($target.hasClass('glyphicon-chevron-right')) {
                          $target
                              .removeClass('glyphicon-chevron-right')
                              .addClass('glyphicon-chevron-down');

                          children.show();
                      } else {
                          $target
                              .removeClass('glyphicon-chevron-down')
                              .addClass('glyphicon-chevron-right');

                          reverseHide($table, $row);
                      }
                  });
              }

              $columnName.prepend('' +
                  '<span class="treegrid-indent" style="width:' + 15 * level + 'px"></span>' +
                  '');
          });

          reverseHide = function (table, element) {
              var
                  $element = $(element),
                  id = $element.data('id'),
                  children = table.find('tr[data-parent="' + id + '"]');

              if (children.length) {
                  children.each(function (i, e) {
                      reverseHide(table, e);
                  });

                  $element
                      .find('.glyphicon-chevron-down')
                      .removeClass('glyphicon-chevron-down')
                      .addClass('glyphicon-chevron-right');

                  children.hide();
              }
          };
        });

    }

  $("#actualMonth").on("change", function(e){
    e.preventDefault();
    getDataHierarchyActual();
  })

  $(document).on("click", "#delete",function(e){
    e.preventDefault();
    var kiad_id = $(this).data("id");
    $("#kiad_id_delete").val(kiad_id);
    $("#deleteModal").modal("show");
    //modal hapus
  })

  $("#submit_delete").on("submit",function(e){
    e.preventDefault();
    var kiad_id = $("#kiad_id_delete").val();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_unit/delete_item/'+kiad_id,
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
        $("#loadingUom").fadeIn(1000);
      }
    })
    .done(function(data) {
      notif("success", "Success Delete Data");

      allListWithDetail();
      getDataHierarchyActual();
      $("#deleteModal").modal("hide");
    })
    .fail(function() {
      notif("error", "Failed to Delete data");
    })
    .always(function() {
      $("#loadingUom").fadeOut(1000);
    });
  })

  $(document).on("change blur", "#kiad_actual", function(e){
    var act = $(this).val();
    var kia_name_act = $("#kia_name_act").val();
    var month = $("#actualMonth").val();
    var year = $("#year").val();
    var fa_function_name = $("#fa_function_name").val();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_unit/get_ytd',
      type: 'POST',
      dataType: 'JSON',
      data: {act: act, kia_name_act: kia_name_act, month: month, year: year, fa_function_name: fa_function_name},
      beforeSend: function(){
        $("#kiad_actual_ytd").fadeOut('fast', function(){
            $("#loading").fadeIn('fast');
        });
        $("#submit_actual_button").prop('disabled', true).html("<i class='fa fa-refresh fa-spin'></i> Please Wait ..");
      }
    })
    .done(function(data) {
      $("#kiad_actual_ytd").val(data.kiad_actual_ytd);
    })
    .fail(function() {
      notif("error", "Something error to load value");
    })
    .always(function() {
      $("#loading").fadeOut('fast', function(){
          $("#kiad_actual_ytd").fadeIn('fast');
      });
      $("#submit_actual_button").prop('disabled', false).html("Save");
    });

  })

  $(document).on("click", "#add_actual", function(e){
    e.preventDefault();
    var kiad_id = $(this).data("id");
    var kia_name = $(this).data("kia_name");
    var kia_uom = $(this).data("uom");
    var actual = $(this).data("actual");
    var actual_ytd = $(this).data("actual_ytd");
    var formula = $(this).data("formula");

    var month_kpi = $("#actualMonth").val();
    $('#kiad_id_act').val(kiad_id);
    $('#month_act').val(month_kpi);
    $("#uom_act").val(kia_uom);
    $("#kiad_actual").val(actual);
    $("#kiad_actual_ytd").val(actual_ytd);
    if(kiad_id == undefined || kiad_id == ""){
      notif("warning", "You must add detail value on the top");
    } else{
      $("#kia_name_act").val(kia_name);
      $("#fa_function_name").val(formula);
      $("#actualModal").modal("show");
      $("#kpi_name").text(kia_name);
      $("#kiad_actual, #kiad_actual_ytd").on("keyup keydown", function(){
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190)
        {
          var value = $(this).val();
          var uom = $("#uom_act").val();
          if(uom == "percent"){
            if(value > 100){
              $(this).val("0");
              notif("warning", "Value must less than 100");
            }
          } else if(uom == "index"){
            if(value > 1){
              $(this).val("0");
              notif("warning", "Value must less than 1");
            }
          }
        } else {
            event.preventDefault();
        }
        if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
            event.preventDefault();
      })
    }
  })

  $(document).on("click", "#editKpi", function(e){
    e.preventDefault();
    var ki_id = $(this).data("id");
    var level = $(this).data("level");
    year_load();

    $('html, body').animate({
      scrollTop: $("#boxForm").position().top
    }, 500);
    $("#boxForm").fadeIn(5000, function() {
      $.ajax({
        url: '<?= base_url() ?>index.php/kpi_unit/edit_item/'+ki_id,
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function(){
          $("#loadingUom").fadeIn(1000);
          $("#inputKiWeight").fadeIn(1000);
        }
      })
      .done(function(data) {
          //year_load();
          var hideoption = $("#parent option[value='']");

        if(level == "1" && data.kf_id != "5"){
          $("#weight_field").addClass("hidden");
          $("#target_field").removeClass("hidden");
          $("#upper_field").addClass("hidden");
          $("#lower_field").addClass("hidden");
          $("#target_field_ytd").removeClass("hidden");
          $("#upper_field_ytd").addClass("hidden");
          $("#lower_field_ytd").addClass("hidden");
          if(data.kia_level == "deep_child"){
              $("#target_field_ytd").addClass("hidden");
              $("#target_field").addClass("hidden");
              $("#weight_field").addClass("hidden");
              $("#group").addClass("hidden");
          }
          hideoption.show();
        }else if(level == "1" && data.kf_id == "5"){
          $("#weight_field").addClass("hidden");
          $("#target_field").addClass("hidden");
          $("#upper_field").removeClass("hidden");
          $("#lower_field").removeClass("hidden");
          $("#target_field_ytd").addClass("hidden");
          $("#upper_field_ytd").removeClass("hidden");
          $("#lower_field_ytd").removeClass("hidden");
          if(data.kia_level == "deep_child"){
            $("#upper_field").addClass("hidden");
            $("#lower_field").addClass("hidden");
            $("#upper_field_ytd").addClass("hidden");
            $("#lower_field_ytd").addClass("hidden");
             $("#weight_field").addClass("hidden");
              $("#group").addClass("hidden");
          }
          hideoption.show();
        }else{
          hideoption.show();
          $("#weight_field").addClass("hidden");
          $("#target_field").addClass("hidden");
          $("#upper_field").addClass("hidden");
          $("#lower_field").addClass("hidden");
          $("#target_field_ytd").addClass("hidden");
          $("#upper_field_ytd").addClass("hidden");
          $("#lower_field_ytd").addClass("hidden");
          $("#weight_field").addClass("hidden");
          $("#group").addClass("hidden");
         
        }
        if(data.kia_level != "deep_child" && data.kia_parent_id!=0){
            // $("#target_field_ytd").removeClass("hidden");
            // $("#target_field").removeClass("hidden");
            $("#weight_field").removeClass("hidden");
            // $("#group").removeClass("hidden");
        }
        if(data.kia_level == "deep_child" && data.kia_parent_id==0){
            $("#target_field_ytd").removeClass("hidden");
            $("#target_field").removeClass("hidden");
            $("#weight_field").addClass("hidden");
            $("#group").removeClass("hidden");
        }
        var str = data.kia_target;
        var str_ytd = data.kia_target_ytd;
        var upper = "";
        var lower = "";
        var upper_ytd = "";
        var lower_ytd = "";
        if(str == null || str == "0" || str == "" || data.kf_id != "5"){
          upper = 0;
          lower = 0;
          upper_ytd = 0;
          lower_ytd = 0;
          str = data.kia_target;
          str_ytd = data.kia_target_ytd;
        }else{
            var check_spar = str.includes("|");
            var check_spar_ytd = str_ytd.includes("|");
          if(check_spar && check_spar_ytd){
            var spl = str.split("|");
            var spl_ytd = str_ytd.split("|");
            upper = spl[0];
            lower = spl[1];
            upper_ytd = spl_ytd[0];
            lower_ytd = spl_ytd[1];
          } else{
            upper = 0;
            lower = 0;
            upper_ytd = 0;
            lower_ytd = 0;
           }
         }

        $("#kia_id_detail").val(data.kia_id);
        $("#kpi").val(data.kia_name);
        $("#uom").val(data.kia_uom).attr("disabled", true);
        $("#formula").val(data.kf_id).attr("disabled", true);
        $("#weight_unit").val(data.kia_weight);
        $("#target_unit").val(str);
        $("#target_unit_ytd").val(str_ytd);
        $("#upper_unit").val(upper);
        $("#lower_unit").val(lower);
        $("#upper_unit_ytd").val(upper_ytd);
        $("#lower_unit_ytd").val(lower_ytd);
        $("#parent").val((data.kia_parent_id == "0") ? "" : data.kia_parent_id);
        $("#kpi_group_id_select_insert").val(data.kpi_group_id);
        getDataHierarchyActual();

      })
      .fail(function() {
        notif("error", "Failed to load data");
      })
      .always(function() {
        $("#loadingUom").fadeOut(1000);
      });
    });
    return false;
  });


  function getMonthName(month)
  {
    switch (month) {
      case 1: return 'Jan'; break;
      case 2: return 'Feb'; break;
      case 3: return 'Mar'; break;
      case 4: return 'Apr'; break;
      case 5: return 'May'; break;
      case 6: return 'Jun'; break;
      case 7: return 'Jul'; break;
      case 8: return 'Aug'; break;
      case 9: return 'Sep'; break;
      case 10: return 'Oct'; break;
      case 11: return 'Nov'; break;
      case 12: return 'Dec'; break;
    }
  }

  function changeYear()
  {
    var year = $("#year").val();
    $("#year_input").val(year);
  }

  function AllListUnit()
  {
    $("#unitTable").find('tr.data').remove();
    $.ajax({
      url: '<?= base_url()?>index.php/kpi_unit/all_list_unit',
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
        $("#loader1").fadeIn(1000);
      }
    })
    .done(function() {
      $("#loader1").fadeOut(1000, function() {
        $("#unitTable").fadeIn(1000);
      });
    })
    .fail(function() {
      notif("error", "failed to load list unit");
    })
    .always(function(allData) {
      var no = 1;
      $.each(allData, function(i, data){
        var row = "<tr class='data'>"+
                      "<td style='text-align: center;'>"+no+"</td>"+
                      "<td style='text-align: center;'>"+data.role_name+"</td>"+
                      "<td style='text-align: center;'>"+
                        "<button type='button' id='assignKpi' data-toggle='modal' data-target='#assigmentModal' class='btn btn-flat bg-navy' data-role_name='"+data.role_name+"' data-role_id='"+data.role_id+"'><i class='fa fa-pencil'></i></button>"+
                        "<button type='button' id='deleteUnitKpi' class='btn btn-flat bg-red' data-role_name='"+data.role_name+"' data-role_id='"+data.role_id+"'><i class='fa fa-trash'></i></button>"+
                      "</td>"+
                   "</tr>";
         $("#unitTable").append(row);
        no++;
      });
    });

  }

  $("#assigmentForm").on("submit", function(e){
    e.preventDefault();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_unit/assign_kpi',
      type: 'POST',
      dataType: 'JSON',
      data: new FormData(this),
      contentType: false,
      processData: false,
      cache: false,
      beforeSend: function(){
        $("#submitAssign").attr("disabled", "disabled").html("Please Wait");
      }
    })
    .done(function(data) {
      if(data.success){
        notif("success", "data inserted");
        window.location.reload();
      } else{
        notif("error", "sorry, failed to insert");
      }
    })
    .fail(function() {
      notif("error", "sorry, failed to insert");
    })
    .always(function() {
      $("#submitAssign").attr("disabled", false).html("Save");
    });

  })

  function remove_member(id){
    var ki_id = $("#ki_id_select").val();
    var role = (($("#userSelected").val() == "") ? $('#role_id').val() : $("#userSelected").val());
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_unit/remove_group/' + id,
      type: 'GET',
      dataType: 'JSON',
      cache: false,
      success: function(data){
        load_member(ki_id, role);
        list_parrent_kpi(role);
      }
    });
  }

  $("#appendKpi").on("click", function(e){
    e.preventDefault();
    var content = '<tr>'+
                    '<td>'+
                      '<select class="form-control" id="kpi" name="kpi[]">'+
                        <?php foreach ($deep_child as $item): ?>
                        '<option value="<?= $item->kia_id ?>"><?= $item->kia_name ?></option>'+
                        <?php endforeach; ?>
                      '</select>'+
                    '</td>'+
                    '<td style="font-size: 25px;"> <a href="#" id="delKpi" class="text-danger">X</a> </td>'+
                  '</tr>';
    $("#listAssignKpi").find('tbody').prepend(content);
  });

  $(document).on("change", "#kpi", function(e){
    var ini = $(this);
    ini.closest('tbody').find('tr').each(function(index, el){
      var otherValKpi = $(this).find('#kpi').not(ini).val();
      var this_value = ini.val();
      console.log(otherValKpi, this_value);
      if(otherValKpi == this_value){
        notif("warning", "the KPI item has been selected");
        ini.closest('tr').remove();
      }
    })
  })

  $(document).on("click", "#delKpi", function(e){
    e.preventDefault();
    var ini = $(this);
    var kia_id = $(this).data("kia_id");
    if(kia_id != undefined){
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn bg-navy btn-flat',
        cancelButtonClass: 'btn btn-default btn-flat',
        buttonsStyling: false,
        reverseButtons: true
      }).then(function(result) {
        if (result) {
          $.ajax({
            url: '<?= base_url() ?>index.php/kpi_unit/delete_assign',
            type: 'POST',
            dataType: 'JSON',
            data: {
              kia_id: kia_id
            }
          })
          .done(function(data) {
            if(data.success){
              ini.closest('tr').remove();
              swal(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
            } else{
              notif("error", "sorry.. failed to delete");
            }
          })
          .fail(function() {
            notif("error", "sorry.. failed to delete");
          })
          .always(function() {
            console.log("complete");
          });
        }
      })
    } else{
      $(this).closest('tr').remove();
    }
  })

  $("#year").on("change", function(e){
    e.preventDefault();
    changeYear();
  })

  $("#assigmentModal").on("show.bs.modal", function(e){
    var role_name = $(e.relatedTarget).data("role_name");
    var role_id = $(e.relatedTarget).data("role_id");
    $(this).find('.modal-title').text("Assign to "+role_name);
    $(this).find('#role_id').val(role_id);
    $(this).find('#listAssignKpi tbody tr').remove();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_unit/get_exist_assigment/'+role_id,
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function()
      {
        $("#loaderModalAssign").fadeIn("fast");
      }
    })
    .done(function(data) {
      if(data.length > 0){
        $.each(data, function(i, item){
          var content = '<tr>'+
                          '<td>'+
                            '<input type="text" id="" class="form-control" readonly value="'+item.kia_name+'">'+
                            '<input type="hidden" id="kpi" class="form-control" readonly value="'+item.ki_id+'">'+
                          '</td>'+
                          '<td style="font-size: 25px;"> <a href="#" id="delKpi" data-kia_id="'+item.kia_id+'" class="text-danger">X</a> </td>'+
                        '</tr>';
          $("#listAssignKpi").find('tbody').prepend(content);
        })
      }
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      $("#loaderModalAssign").fadeOut(1000, function() {
        $("#content").show("fast");
      });
    });

  })

  function detail_item(function_name,uom,name,kia_id,level, target_ytd, target){
    // alert(level);
    $("#detailItem").modal("show");
    var uom = uom;
    var ki_name = name;
    var formula = function_name;
    var ki_id = kia_id;
    $('#ki_name_label').html(": &nbsp;"+ki_name);
    $('#ki_uom_label').html(": &nbsp;"+uom);
    $('#ki_uom').val(uom);
    $('#ki_id').val(ki_id);
    var ini = $(this);
    // console.log(ini);
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_unit/detail_item/'+ki_id+'/'+$("#year").val(),
      type: 'GET',
      dataType: 'JSON',
    })
    .done(function(data) {
      // console.log(data);
      // if(data.length == 0){
      //
      // }else{
      //   console.log("ada");
      // }
      var table = "";
      table += '<table class="table table-hover table-gmf">';
      table += '<thead>';
      table += '<tr>';
      table += '<th>Month</th>';
      if(formula == 'Stabilize'){
          table += '<th>Upper</th>';
          table += '<th>Lower</th>';
          table += '<th>Upper Ytd</th>';
          table += '<th>Lower Ytd</th>';
      } else{
          table += '<th>Target</th>';
          table += '<th>Target Ytd</th>';
      }
      table += '<th>Weight</th>';
      table += '<th>Limit</th>';
      table += '</tr>';
      table += '</thead>';
      table += '<tbody>';
      if(data.length > 0){
        var arrayNo = 0;
        for (var i = 1; i <= 12; i++) {
          table += '<tr>';
          var valueId = (i == data[arrayNo].month) ? data[arrayNo].kiad_id : "";
          table += '<td><input type="hidden" name="kid_id[]" value="'+valueId+'">'+getMonthName(i)+'</td>';
          if(formula == 'Stabilize'){
              var valueTarget = (i == data[arrayNo].month) ? data[arrayNo].kiad_target.split("|") : "";
              table += '<td><input type="text" class="form-control no" name="upper[]" id="upper" value="'+((valueTarget[0] == "" || valueTarget[0] == undefined) ? "0" : valueTarget[0])+'"/></td>';
              table += '<td><input type="text" class="form-control no" name="lower[]" id="lower" value="'+((valueTarget[1] == "" || valueTarget[1] == undefined) ? "0" : valueTarget[1])+'"/></td>';
              var valueTargetYtd = (i == data[arrayNo].month) ? data[arrayNo].kiad_target_ytd.split("|") : "";
              table += '<td><input type="text" class="form-control no" name="upper_ytd[]" id="upper_ytd" value="'+((valueTargetYtd[0] == "" || valueTargetYtd[0] == undefined) ? "0" : valueTargetYtd[0])+'"/></td>';
              table += '<td><input type="text" class="form-control no" name="lower_ytd[]" id="lower_ytd" value="'+((valueTargetYtd[1] == "" || valueTargetYtd[1] == undefined) ? "0" : valueTargetYtd[1])+'"/></td>';
          } else{
              var valueTarget = (i == data[arrayNo].month) ? data[arrayNo].kiad_target : "";
              table += '<td><input type="text" class="form-control no" name="target[]" id="target" value="'+((valueTarget == "") ? "0" : valueTarget)+'"/></td>';
              var valueTargetYtd = (i == data[arrayNo].month) ? data[arrayNo].kiad_target_ytd : "";
              table += '<td><input type="text" class="form-control no" name="target_ytd[]" id="target_ytd" value="'+((valueTargetYtd == "" || valueTargetYtd === undefined  || valueTargetYtd === 'undefined') ? "0" : valueTargetYtd)+'"/></td>';
          }
          var valueWeight = (i == data[arrayNo].month) ? data[arrayNo].kiad_weight : "";
          table += '<td><input type="text" class="form-control no" name="weight[]" id="weight" value="'+valueWeight+'"/></td>';
          var valueLimit = (i == data[arrayNo].month) ? data[arrayNo].kiad_limit : "";
          table += '<td><input type="text" class="form-control no" name="limit[]" id="limit" value="'+((valueLimit == "" || valueLimit === undefined  || valueLimit === 'undefined') ? "0" : valueLimit)+'"/></td>';
          table += '</tr>';
          arrayNo++;
        }
      } else {

        var dta_weight = null;

        $.ajax({
          url: '<?= base_url() ?>index.php/kpi_unit/get_weight_item/'+ki_id,
          type: 'GET',
          dataType: 'JSON',
          async: false,
          success: function (data) {
            dta_weight=data.kia_weight;
          }
        });

        for (var i = 1; i <= 12; i++) {
          table += '<tr>';
          table += '<td>'+getMonthName(i)+'</td>';
          if(formula == 'Stabilize'){
              var newTarget = [0, 0];
              var newTargetYtd = [0, 0];
              if(target != "" && target != "0" && target != undefined){
                newTarget = target.split("|");
              } else if (target_ytd != "" && target_ytd != "0" && target_ytd != undefined) {
                newTargetYtd = target_ytd.split("|");
              }
              table += '<td><input type="text" class="form-control no" name="upper[]" id="upper" value="'+newTarget[0]+'"/></td>';
              table += '<td><input type="text" class="form-control no" name="lower[]" id="lower" value="'+newTarget[1]+'"/></td>';
              table += '<td><input type="text" class="form-control no" name="upper_ytd[]" id="upper_ytd" value="'+newTargetYtd[0]+'"/></td>';
              table += '<td><input type="text" class="form-control no" name="lower_ytd[]" id="lower_ytd" value="'+newTargetYtd[1]+'"/></td>';
          } else{
              table += '<td><input type="text" class="form-control no" name="target[]" id="target" value="'+(target === undefined ? 0 : target)+'"/></td>';
              table += '<td><input type="text" class="form-control no" name="target_ytd[]" id="target_ytd" value="'+(target_ytd === undefined ? 0 : target_ytd)+'"/></td>';
          }
          table += '<td><input type="text" class="form-control no " name="weight[]" value="'+dta_weight+'" id="weight" /></td>';
          table += '<td><input type="text" class="form-control no " name="limit[]" id="limit" value="0"/></td>';
          table += '</tr>';
          arrayNo++;
        }
      }
      table += '</tbody>';
      table += '</table>';
      $('#detailKpi').html(table);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function(data) {
      console.log("complete");
    });
  }

  $("#addDetail").on("submit", function(e){
    e.preventDefault();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_unit/manage_detail',
      type: 'POST',
      dataType: 'JSON',
      cache: false,
      processData: false,
      contentType: false,
      data: new FormData(this),
      beforeSend: function(){
        $("#save_form_detail").attr("disabled", true).html("Please Wait");
      }
    })
    .done(function(data) {
      if(data.success){
        notif("success", "data saved");
      } else{
        notif("warning", "Internal Server Error");
      }
    })
    .fail(function() {
      notif("error", "Fatal Error");
    })
    .always(function() {
      $("#detailItem").modal("hide");
      // $(".modal-backdrop").remove();
      // $("body").removeClass("modal-open");
      $("#save_form_detail").attr("disabled", false).html("Save");
      allListWithDetail();
      getDataHierarchyActual();
    });

  })

  /*$(document).on("keyup keydown", ".no", function(e){
    if (event.shiftKey == true) {
        event.preventDefault();
    }
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190)
    {
      var value = $(this).val();
      var uom = $("#ki_uom").val();
      if(uom == "percent"){
        if(value > 100){
          $(this).val("0");
          notif("warning", "Value must less than 100");
        }
      } else if(uom == "index"){
        if(value > 1){
          $(this).val("0");
          notif("warning", "Value must less than 1");
        }
      }
    } else {
        event.preventDefault();
    }
    if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
  });*/

  $('.no').keypress(function(event) {
    if((event.which > 47 && event.which < 58) || (event.which == 46 || event.which == 8)){
    }else{
    event.preventDefault();
    }
    }).on('paste', function(event) {
        event.preventDefault();
  });

  /*$(document).on("keyup keydown", ".no-free", function(e){
    if (event.shiftKey == true) {
        event.preventDefault();
    }
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190)
    {
      var value = $(this).val();
      var uom = $("#ki_uom").val();
      if(uom == "percent"){
        if(value > 100){
          $(this).val("0");
          notif("warning", "Value must less than 100");
        }
      } else if(uom == "index"){
        if(value > 1){
          $(this).val("0");
          notif("warning", "Value must less than 1");
        }
      }
    } else {
        event.preventDefault();
    }
    if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
  })*/

  function toggleGroup(level){
    console.log(level);
    if(level == "1"){
      $("#group").fadeIn("fast");
    } else {
      $("#group").fadeOut("fast");
    }
  }

  $("#formula").on("change",function(){
    var formula = $(this).val();
    var level = $("#parent_save").val();
    console.log("formula : "+formula+"-"+"level : "+level);
    $("#group").find('#kpi_group_id_select_insert').val("");
    toggleGroup(level);
    if(level == "1" && formula == "5"){
      $("#upper_field").removeClass("hidden");
      $('#upper_field').prop('required',false);
      $("#lower_field").removeClass("hidden");
      $('#lower_field').prop('required',false);
      $("#target_field").addClass("hidden");
      $('#target_field').prop('required',true);
      $("#weight_field").addClass("hidden");
      $('#weight_field').prop('required',true);
      $("#upper_field_ytd").removeClass("hidden");
      $('#upper_field_ytd').prop('required',false);
      $("#lower_field_ytd").removeClass("hidden");
      $('#lower_field_ytd').prop('required',false);
      $("#target_field_ytd").addClass("hidden");
      $('#target_field_ytd').prop('required',true);
    }else if(level == "1" && formula != "5"){
      $("#upper_field").addClass("hidden");
      $('#upper_field').prop('required',true);
      $("#lower_field").addClass("hidden");
      $('#lower_field').prop('required',true);
      $("#target_field").removeClass("hidden");
      $('#target_field').prop('required',false);
      $("#upper_field_ytd").addClass("hidden");
      $('#upper_field_ytd').prop('required',true);
      $("#lower_field_ytd").addClass("hidden");
      $('#lower_field_ytd').prop('required',true);
      $("#target_field_ytd").removeClass("hidden");
      $('#target_field_ytd').prop('required',false);
      $("#weight_field").addClass("hidden");
      $('#weight_field').prop('required',true);
    }else if(level != "1"){
      $("#weight_field").removeClass("hidden");
      $('#weight_field').prop('required',false);
      $("#upper_field").addClass("hidden");
      $('#upper_field').prop('required',true);
      $("#lower_field").addClass("hidden");
      $('#lower_field').prop('required',true);
      $("#target_field").addClass("hidden");
      $('#target_field').prop('required',true);
      $("#upper_field_ytd").addClass("hidden");
      $('#upper_field_ytd').prop('required',true);
      $("#lower_field_ytd").addClass("hidden");
      $('#lower_field_ytd').prop('required',true);
      $("#target_field_ytd").addClass("hidden");
      $('#target_field_ytd').prop('required',true);
    }

  })

  $("#upper_unit").on("keyup",function(){
    var uper = $(this).val();
    $("#target_unit").val(uper+"|0");
  })
  $("#lower_unit").on("keyup", function (){
    var low = $(this).val();
    var upp = $("#upper_unit").val();
    var gab = upp+"|"+low;
    $("#target_unit").val(gab);
  })
  $("#upper_unit_ytd").on("keyup",function(){
    var uper = $(this).val();
    $("#target_unit_ytd").val(uper+"|0");
  })
  $("#lower_unit_ytd").on("keyup", function (){
    var low = $(this).val();
    var upp = $("#upper_unit").val();
    var gab = upp+"|"+low;
    $("#target_unit_ytd").val(gab);
  })



  $("#formAddUser").parsley();
  $("#formAddUser").on("submit", function(e){
    e.preventDefault();
    $.ajax({
      url: '<?= base_url() ?>index.php/kpi_unit/insert_unit_kpi',
      type: 'POST',
      dataType: 'JSON',
      data: {role_id: $("#list_user").val()},
      beforeSend: function(){
        $("#formAddUser").find('#save').attr('disabled', 'disabled');
      }
    })
    .done(function(data) {
      if(data.success){
        notif("success", "Data inserted");
      }
    })
    .fail(function() {
      notif("error", "Sorry, data failed to insert");
    })
    .always(function() {

      $("#formAddUser").find('#save').attr('disabled', false);
      $("#list_user").val("");
      AllListUnit();
    });

  })

  $("#addKpiForm").parsley();
  $("#addKpiForm").on("submit", function(e){
    e.preventDefault();
    var lev = $("#parent").find(':selected').data("level");
    if(lev == 1){
      $('#formula').prop('required',false);
      $('#weight_unit').prop('required',false);
    }else{
      $('#formula').prop('required',true);
      $('#weight_unit').prop('required',true);
    }

    if($("#user_id").val() == ""){
      notif("error", "user not defined");
    } else{
      $.ajax({
        url: '<?= base_url() ?>index.php/kpi_unit/insert',
        type: 'POST',
        dataType: 'JSON',
        data: {
          ki_name : $("#kpi").val(),
          ki_uom : $("#uom").val(),
          ki_parent_id : $("#parent").val(),
          kf_id : $("#formula").val(),
          ki_year: $("#year").val(),
          ki_author: $("#user_id").val(),
          ki_weight : $("#weight_unit").val(),
          ki_role : (($("#userSelected").val() != "") ? $("#userSelected").val() : $('#role_id').val()),
          ki_id : $('#kia_id_detail').val(),
          ki_target : $('#target_unit').val(),
          ki_target_ytd : $('#target_unit_ytd').val(),
          kpi_group_id: $("#kpi_group_id_select_insert").val()
        },
        beforeSend: function(){
          $("#addKpiForm").find('#save').attr('disabled', 'disabled');
        }
      })
      .done(function(data) {
        $("#parent").find('.item').remove();
        if(data.success){
          notif("success", "Data inserted");
          // $("#parent").append(data.newData);
        }
      })
      .fail(function() {
        notif("error", "Sorry, data failed to insert");
      })
      .always(function() {
        console.log("complete");
        $("#addKpiForm").find('#save').attr('disabled', false);
        $("#kpi").val("");
        $("#uom").val("").attr("disabled", false);
        $("#parent").val("");
        $("#formula").val("").attr("disabled", false);
        $("#kia_id_detail").val("");
        $('#weight_unit').val("0");
        $('#target_unit').val("0");
        allListWithDetail();
        getDataHierarchyActual();

        $("#boxForm").fadeOut(1000, function() {
          $('#addKpiForm').parsley().reset();
        });

      });
    }

  })

  $("#submit_actual").parsley();
  $("#submit_actual").on("submit", function(e){
    e.preventDefault();
    $.ajax({
      url: '<?= base_url() ?>index.php/Kpi_unit_temp/updateActual/'+$("#kiad_id_act").val(),
      type: 'post',
      dataType: 'json',
      data: {
        kiad_actual: $("#kiad_actual").val(),
        kiad_actual_ytd: $("#kiad_actual_ytd").val()
      },
      beforeSend: function(){
        $("#save_actual").prop('disabled', true).text('please wait');
      }
    })
    .done(function(data) {
      if(data.success){
        notif("success", "data updated");
      } else{
        notif("error", "data failed to update");
      }
    })
    .fail(function() {
      notif("error", "data failed to update");
    })
    .always(function() {
      $("#save_actual").prop('disabled', false).text("Save");
      $("#actualModal").modal("hide");
      getDataHierarchyActual();
    });

  })

  $("#addKpi").on("click", function(e){
    e.preventDefault();
    $('#addKpiForm').parsley().reset();
    $("#addKpiForm").find('#save').attr('disabled', false);
    $("#uom").val("").attr("disabled", false);
    $("#formula").val("").attr("disabled", false);
    $("#kia_id_detail").val("");
    load_all_group();
    year_load();
    $("#boxForm").fadeIn(1000, function() {
      $("#addKpiForm")[0].reset();
      $("#loadingUom").fadeOut(1000);
    });
  })

  $("#addUnit").on("click", function(e){
    e.preventDefault();
    $("#list_user").addClass('select2');
    $("#boxFormUser").fadeIn(1000, function() {
      $("#loadingUomUser").fadeOut(1000);
    });
  })

  $("#cancelFormUser").on("click", function(e){
    e.preventDefault();
    $(this).closest('#boxFormUser').fadeOut(1000, function() {
      $("#loadingUomUser").fadeIn(1000);
    });
  })

  $("#cancel").on("click", function(e){
    e.preventDefault();
    $(this).closest('#boxForm').fadeOut(1000, function() {
      $("#loadingUom").fadeIn(1000);
    });
  })

  $(document).on("change", "#parent", function(e){
    var paren = $(this).val();
    var lev = $("#parent").find(':selected').data("level");
    toggleGroup(lev);
    var form = $("#formula").val();
    $("#parent_save").val(lev);
    if(lev == "1" && form == "5"){
      $("#weight_field").addClass("hidden");
      $("#weight_field").prop('required',false);
      $("#target_field").addClass("hidden");
      $("#target_field").prop('required',false);
      $("#target_field_ytd").addClass("hidden");
      $("#target_field_ytd").prop('required',false);
      $("#upper_field").removeClass("hidden");
      $("#upper_field").prop('required',true);
      $("#lower_field").removeClass("hidden");
      $("#lower_field").prop('required',true);
    }else if(lev == "1" &&  form != "5"){
      $("#weight_field").addClass("hidden");
      $("#weight_unit").prop('required',false);
      $("#target_field").removeClass("hidden");
      $("#target_unit").prop('required',true);
      $("#target_field_ytd").removeClass("hidden");
      $("#target_unit_ytd").prop('required',true);
      $("#upper_field").addClass("hidden");
      $("#upper_field").prop('required',false);
      $("#lower_field").addClass("hidden");
      $("#lower_field").prop('required',false);
    }else if(lev != "1"){
      $("#weight_field").removeClass("hidden");
      $("#weight_unit").prop('required',true);
      $("#target_field").addClass("hidden");
      $("#target_unit").prop('required',false);
      $("#target_field_ytd").addClass("hidden");
      $("#target_unit_ytd").prop('required',false);
      $("#upper_field").addClass("hidden");
      $("#upper_unit").prop('required',false);
      $("#lower_field").addClass("hidden");
      $("#lower_unit").prop('required',false);
      $("#upper_unit_ytd").prop('required',false);
      $("#lower_unit_ytd").prop('required',false);
    }

  })



  $("#year").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years"
  });

  $('#year').on('changeDate', function(ev){
      $(this).datepicker('hide');
  });
</script>
