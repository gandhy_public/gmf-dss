<style>
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin: 0;
  line-height: 1;
}

  span.select2{
    width: 100% !important;
  }

  option span {
    font-weight: bold;
  }

  .optionChild {
    padding-left: 15px;
  }

  .loading {
    position: absolute;
    left: 50%;
    top: 50%;
    z-index: 1;
    width: 150px;
    height: 150px;
    margin: -75px 0 0 -75px;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }

  @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }

  /* Add animation to "page content" */
  .animate-bottom {
    position: relative;
    -webkit-animation-name: animatebottom;
    -webkit-animation-duration: 1s;
    animation-name: animatebottom;
    animation-duration: 1s
  }

  @-webkit-keyframes animatebottom {
    from { bottom:-100px; opacity:0 }
    to { bottom:0px; opacity:1 }
  }

  @keyframes animatebottom {
    from{ bottom:-100px; opacity:0 }
    to{ bottom:0; opacity:1 }
  }

  #myDiv {
    display: none;
    text-align: center;
  }

  .treegrid-indent {
      width: 0px;
      height: 16px;
      display: inline-block;
      position: relative;
  }

  .treegrid-expander {
      width: 0px;
      height: 16px;
      display: inline-block;
      position: relative;
      left:-17px;
      cursor: pointer;
  }
</style>

<section class="content-header">
  <h1>
    <?= $title ?>
    <small><?= $small_tittle ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
    <?php foreach ($breadcrumb as $data): ?>
      <li><?= $data ?></li>
    <?php endforeach; ?>
  </ol>
</section>


<section class="content">


  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Management Actual KPI</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div style="border: 1px solid #bfbcbc; padding:  10px;">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Months</label>
                  <select class="form-control " name="actualMonth" id="actualMonth">
                    <?php
                      $months = range(1, 12);
                      foreach ($months as $month) {
                        $selected = (date("m") == $month) ? "selected" : "";
                        echo "<option value='".$month."' ".$selected.">".convert_month($month)."</option>";
                      }
                     ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row" style="margin-top: 20px;">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-2">
            	<p style="font-size:20px;">List Unit</p>
              <hr style="border: 0;border-top: 1px solid #bfbcbc;margin-top:0px;margin-bottom:0px;">
            </div>
          </div>
          <div style="display:none;" class="loading" id="loaderListUnit"></div>
          <table class="table table-hover table-bordered table-gmf" id="actualTable">
            <thead>
              <tr>
                <th>KPI</th>
                <th>Actual</th>
                <th>UoM</th>
                <th>Achievement</th>
                <th>Score</th>
                <th>Total Score</th>
                <th>Action </th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>


</section>

<div class="modal fade" id="actualModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="submit_actual" action="index.html" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Actual KPI</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table">
                <tbody>
                  <tr>
                    <th>KPI</th>
                    <th>Actual</th>
                  </tr>
                  <tr>
                    <th id="kpi_name"></th>
                    <th><input type="text" name="kiad_actual" id="kiad_actual" class="form-control" required/></th>
                    <th>
                      <input type="hidden" name="uom" id="uom" class="form-control" required/>
                      <input type="hidden" name="kiad_id" id="kiad_id" class="form-control" required/>
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
          <button type="submit" id="save_actual" class="btn bg-navy btn-flat">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>

  $("#submit_actual").parsley();
  $("#submit_actual").on("submit", function(e){
    e.preventDefault();
    $.ajax({
      url: '<?= base_url() ?>index.php/Kpi_unit_temp/updateActual/'+$("#kiad_id").val(),
      type: 'post',
      dataType: 'json',
      data: {
        kiad_actual: $("#kiad_actual").val()
      },
      beforeSend: function(){
        $("#save_actual").prop('disabled', true).text('please wait');
      }
    })
    .done(function(data) {
      if(data.success){
        notif("success", "data updated");
      } else{
        notif("error", "data failed to update");
      }
    })
    .fail(function() {
      notif("error", "data failed to update");
    })
    .always(function() {
      $("#save_actual").prop('disabled', false).text("Save");
      $("#actualModal").modal("hide");
      getDataHierarchyActual();
    });

  })

  $(document).ready(function() {
    getDataHierarchyActual();
  });

  $(document).on("click", "#add_actual", function(e){
    e.preventDefault();
    var kiad_id = $(this).data("id");
    var kiad_actual = $(this).data("actual");
    var kia_name = $(this).data("kia_name");
    var kia_uom = $(this).data("uom");
    $("#uom").val(kia_uom);
    if(kiad_id == undefined || kiad_id == ""){
      notif("warning", "You must add detail value on the top");
    } else{
      $("#kiad_actual").val(kiad_actual);
      $("#kiad_id").val(kiad_id);
      $("#actualModal").modal("show");
      $("#kpi_name").text(kia_name);
      $("#kiad_actual").on("keyup keydown", function(){
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190)
        {
          var value = $(this).val();
          var uom = $("#uom").val();
          if(uom == "percent"){
            if(value > 100){
              $(this).val("0");
              notif("warning", "Value must less than 100");
            }
          } else if(uom == "index"){
            if(value > 1){
              $(this).val("0");
              notif("warning", "Value must less than 1");
            }
          }
        } else {
            event.preventDefault();
        }
        if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
            event.preventDefault();
      })
    }
  })

  $("#actualMonth").on("change", function(e){
    e.preventDefault();
    getDataHierarchyActual();
  })

  function showModal()
  {

  }

  function getDataHierarchyActual()
  {
    $.ajax({
      url: '<?= base_url() ?>index.php/Kpi_unit_temp/getDataActual',
      type: 'GET',
      dataType: 'JSON',
      data: {
        month: $("#actualMonth").val(),
        role_id: localStorage.getItem("role_id")
      },
      beforeSend: function(){
        $("#loaderListUnit").fadeIn();
      }
    })
    .done(function(data) {
      $("#loaderListUnit").fadeOut();
      $("#actualTable").find('tbody').html(data.content);
    })
    .fail(function() {
      notif("error", "Something make your data failed to load");
    })
    .always(function() {
      var
          $table = $('#actualTable'),
          rows = $table.find('tr');

      rows.each(function (index, row) {
          var
              $row = $(row),
              level = $row.data('level'),
              id = $row.data('id'),
              $columnName = $row.find('td[data-column="name"]'),
              children = $table.find('tr[data-parent="' + id + '"]');

          if (children.length) {
              var expander = $columnName.prepend('' +
                  '<span class="treegrid-expander glyphicon glyphicon-chevron-right"></span>' +
                  '');

              children.hide();

              expander.on('click', function (e) {
                  var $target = $(e.target);
                  if ($target.hasClass('glyphicon-chevron-right')) {
                      $target
                          .removeClass('glyphicon-chevron-right')
                          .addClass('glyphicon-chevron-down');

                      children.show();
                  } else {
                      $target
                          .removeClass('glyphicon-chevron-down')
                          .addClass('glyphicon-chevron-right');

                      reverseHide($table, $row);
                  }
              });
          }

          $columnName.prepend('' +
              '<span class="treegrid-indent" style="width:' + 15 * level + 'px"></span>' +
              '');
      });

      reverseHide = function (table, element) {
          var
              $element = $(element),
              id = $element.data('id'),
              children = table.find('tr[data-parent="' + id + '"]');

          if (children.length) {
              children.each(function (i, e) {
                  reverseHide(table, e);
              });

              $element
                  .find('.glyphicon-chevron-down')
                  .removeClass('glyphicon-chevron-down')
                  .addClass('glyphicon-chevron-right');

              children.hide();
          }
      };
    });

  }
</script>
