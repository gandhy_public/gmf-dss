<!--
START EDIT
BY DIMAS
Second Edit by
Yoko
-->

<style>
    .box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }

    .wadah {
        padding-left: 5px;
        padding-right: 5px;
    }

    .title-manufacture {
        color: #F2C573;
        text-align: center;
        font-size: 27px;
        font-weight: bold;
        margin-top: 27px;
        margin-left: 11px;
        margin-bottom:0px;
    }
    .title-manufacture2 {
        color: #F2C573;
        text-align: center;
        font-size: 20px;
        font-weight: bold;
        margin-top: 12px;
        margin-bottom:0px;
        margin-left: 11px;
        cursor:pointer;
    }

    .description-block > .description-header {
        margin: 0;
        padding: 0;
        font-size: 28px;
        font-weight: bold;
    }

    .inner_in {
        padding: 0px;
        border-radius: 10px;
    }

    .small-box > .inner {
        padding: 3px;
    }

    .widget-user .widget-user-header {
        padding: 1px;
        height: 90px;
        border-top-right-radius: 11px;
        border-top-left-radius: 11px;
        background: linear-gradient(to bottom, #001f3f, #111111);
    }

    .widget-user .widget-user-image {
        position: absolute;
        top: 5px;
        left: 45%;
    }

    .box .border-right {
        border-right: 1px solid #eac8c8;
    }

    .form-group {
        margin-bottom: 5px;
    }

    .widget-user .box-footer {
        padding-top: 0px;
    }

    .box-footer {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 11px;
        border-bottom-left-radius: 11px;
        border-top: 1px solid #f4f4f4;
        padding: 0px;
        background-color: #fff;
    }

    .mini-icon {
        margin-top: 3px;
        margin-right: 12px;
    }

    .description-block {
        display: block;
        margin: 2px 0;
        text-align: center;
    }

    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 10px;
        width: 100%;
        box-shadow: 0 8px 12px 4px rgba(0, 0, 0, 0.2);
    }

    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0e1f2e;
    }

    .hoverbox:hover {
        opacity: 0.8;
    }

    .text-center {
        text-align: center;
    }

    .row-md4-title{
      padding-right: 0px;
      padding-left: 40px;
      padding-top: 5px;
    }
    .title_panel_h3{
      margin-top:10px;
      margin-bottom:0px;
      font-size:17px;
    }
    .button_view_hijau{
      height: 40px;
      background-color: green;
      color:white;
      cursor: pointer;
    }
    .button_view_biru{
      height: 40px;
      background-color: #0ac1de;
      color:white;
      cursor: pointer;
    }
    .tengah{
      text-align: center;
      padding-top:10px;
      padding-left: 110px;
      cursor: pointer;

    }
</style>

<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Aircraft Availability</h3>
            <div class="box-tools pull-right">
                <label class="control-label" style="font-weight: 100;"><i class="fa  fa-calendar"></i> Year To Date
                    :</label>
                <label class="control-label"><?= $ytd ?></label>
            </div>
        </div>
        <div class="box-body" style="padding: 3px;">
            <div class="box-header with-border" style="padding: 0px;margin-bottom: 10px;">
                <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12">
                    <div class="box box-widget widget-user" style="border-radius: 11px;">
                        <div class="widget-user-header bg-navy">
                        </div>
                        <div class="widget-user-image" style="top: 5px;left: 40%;">
                            <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                 style="border: transparent;width: 150px;">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-6 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header" style="margin-left: 15px;">
                                            <?= $sekarang ?>
                                            <?php
                                            if ($sekarang >= 100) {
                                                echo '<i class="fa fa-caret-up" style="color:#8ec3a7"></i>';
                                            } else {
                                                echo '<i class="fa fa-caret-down" style="color:#f24738"></i>';
                                            }
                                            ?>
                                        </h5>
                                        <span class="description-text">Actual</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="description-block">
                                        <h5 class="description-header">100% <i class="fa fa-minus"
                                                                               style="color:#F2C573;"></i></h5>
                                        <span class="description-text" style="margin-left: -10px;">Target</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-12 col-md-12 col-xs-12">
                    <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12" style="display: none" id="a320Header">
                        <div class="box box-widget widget-user hoverbox" style="border-radius: 11px;cursor: pointer"
                             onclick="detail_panel('a320')">
                            <div class="widget-user-header bg-navy">

                              <div class="row" style="margin-top:30px;">
                                <div class="col-md-4 row-md4-title">
                                  <h3 class="title_panel_h3">Actual</h3>
                                </div>
                                <div class="col-md-8">
                                  <label class="title-manufacture2">

                                      <?= $A320 ?>
                                      <?php
                                      if ($A320 >= 100) {
                                          echo '<i class="fa fa-caret-up" style="color:#8ec3a7;font-size: inherit;"></i>';
                                      } else {
                                          echo '<i class="fa fa-caret-down" style="color:#f24738;font-size: inherit;"></i>';
                                      }
                                      ?>
                                    </label>
                                  </div>
                              </div>
                              <!-- <div class="row">
                                <div class="col-md-4 row-md4-title">
                                  <h3 class="title_panel_h3">Pulsa</h3>
                                </div>
                                <div class="col-md-8">
                                  <label class="title-manufacture2"><?php echo $pulsaA320?></label>
                                </div>
                              </div> -->

                            </div>
                            <div class="widget-user-image">
                                <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                     style="border: transparent;width: 100px;">
                            </div>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="description-block">

                                            <span class="description-text">A320</span>
                                            <i class="fa fa-ellipsis-v pull-right mini-icon"
                                               title="Click for detail"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12" style="display: none" id="classicHeader">
                        <div class="box box-widget widget-user hoverbox" style="border-radius: 11px;cursor: pointer; "
                             onclick="detail_panel('classic')">
                            <div class="widget-user-header bg-navy">

                              <div class="row" style="margin-top:30px;">
                                <div class="col-md-4 row-md4-title">
                                  <h3 class="title_panel_h3">Actual</h3>
                                </div>
                                <div class="col-md-8">
                                  <label class="title-manufacture2">
                                        <?= $Classic ?>
                                        <?php
                                        if ($Classic >= 100) {
                                            echo '<i class="fa fa-caret-up" style="color:#8ec3a7;font-size: inherit;"></i>';
                                        } else {
                                            echo '<i class="fa fa-caret-down" style="color:#f24738;font-size: inherit;"></i>';
                                        }
                                        ?>
                                    </label>
                                  </div>
                              </div>
                              <!-- <div class="row">
                                <div class="col-md-4 row-md4-title">
                                  <h3 class="title_panel_h3">Pulsa</h3>
                                </div>
                                <div class="col-md-8">
                                  <label class="title-manufacture2"><?php echo $pulsaClassic?></label>
                                </div>
                              </div> -->

                            </div>
                            <div class="widget-user-image">
                                <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png"
                                     style="border: transparent;width: 100px;">
                            </div>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="description-block">
                                            <h5 class="description-header box-header" style="margin-left: 25px;">

                                            </h5>
                                            <span class="description-text">CLASSIC</span>
                                            <i class="fa fa-ellipsis-v pull-right mini-icon"
                                               title="Click for detail"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="col-sm-3" style="margin-top: 5px;padding-right:0px;">Start Date</label>
                        <div class="col-sm-9" style="padding-right:0px;padding-left:0px;">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="date_start"
                                       value="<?= date('d-m-Y', strtotime(" -1 month", strtotime(date("d-m-Y")))); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="margin-top: 5px;padding-right:0px;">End Date</label>
                        <div class="col-sm-9" style="padding-right:0px;padding-left:0px;">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="date_end"
                                       value="<?= date('d-m-Y') ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <button class="btn btn-flat bg-navy" id="search" onclick="search()">
                            <i class="fa fa-search"></i> Search
                        </button>
                        <button class="btn btn-flat bg-navy" id="uploadfile" ><i class="fa fa-upload"> </i> Upload Template </button>
                    </div>
                </div>

                <div id="grafik_down">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border" style="display: none"
                         id="grafik1Responsive">
                         <div class="row">
                             <div class="col-md-12">
                              <div id="grafik1" style="width:100%"></div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="col-md-6">
                                <button type="button" class="btn btn-block btn-success btn-flat" onclick="zoom_grafik('A320')">View Detail</button>
                                <!-- <a href="javascript:void(0)" onclick="zoom_grafik(A320)" title="Click for zoom"> -->
                                <!-- <div class="button_view_hijau" onclick="zoom_grafik('A320')">
                                  <label class="tengah">View Detail</label>
                                  <label style="padding-top:15px;padding-right:12px;color:white;" class="pull-right">

                                </label>
                                </div> -->
                                <!-- </a> -->
                              </div>
                              <div class="col-md-6">
                                <button type="button" class="btn btn-block btn-primary btn-flat" onclick="top_remarks3('A320')">Top 3 Trouble</button>

                                <!-- <div class="button_view_biru" onclick="top_remarks3('A320')">
                                  <label class="tengah">Top 3 Trouble</label>
                                  <label style="padding-top:15px;padding-right:12px;color:white;" class="pull-right">

                                </label>
                                </div> -->
                              </div>
                            </div>
                          </div>
                    </div>
                    <div style="display: none" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border"
                         id="grafik2Responsive">
                         <div class="row">
                             <div class="col-md-12">
                               <div id="grafik2" style="width:100%"></div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="col-md-6">
                                <button type="button" class="btn btn-block btn-success btn-flat" onclick="zoom_grafik('Classic')">View Detail</button>

                                <!-- <a href="javascript:void(0)" onclick="zoom_grafik(A320)" title="Click for zoom"> -->
                                <!-- <div class="button_view_hijau" onclick="zoom_grafik('Classic')">
                                  <label class="tengah">View Detail</label>
                                  <label style="padding-top:15px;padding-right:12px;color:white;" class="pull-right">

                                </label>
                                </div> -->
                                <!-- </a> -->
                              </div>
                              <div class="col-md-6">
                                <button type="button" class="btn btn-block btn-primary btn-flat" onclick="top_remarks3('Classic')">Top 3 Trouble</button>

                                <!-- <div class="button_view_biru" onclick="top_remarks3('Classic')">
                                  <label class="tengah">Top 3 Trouble</label>
                                  <label style="padding-top:15px;padding-right:12px;color:white;" class="pull-right">

                                </label>
                                </div> -->
                              </div>
                            </div>
                          </div>
                    </div>
                </div>
                <div id="grafik_big" style="display:none;">
                    <input type='hidden' id='status_view' value='0'>
                    <input type='hidden' id='tipe_view'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12"></div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-default btn-sm" id="back">
                                    <i class="fa fa-arrow-left"></i> Back
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div id="grafikzoom" style="height:300px;width:100%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal_upload">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0e1f2e;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Form Upload Template</h4>
            </div>
            <form id="form_upload" name="form_upload" method="post" action="" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Choose File </label>
                            <input type="file" class="form-control" name="files" id="files" required >     
                             <select name="dataselected" id="dataselected" class="form-control" style="display:none;">
                                <option value="2">Define Data</option>
                            </select>                    
                        </div>
                       
                         <div class="form-row"  id="typedataseleted">
                           <div class="form-group col-md-6">
                                <label>Start Rows</label>
                                <input type="number" name="start_rows" id="start_rows" class="form-control" required placeholder="Min Value 2">
                            </div>

                            <div class="form-group col-md-6">
                                <label >End Rows</label>
                                <input type="number" name="end_rows" id="end_rows" class="form-control" required placeholder="last row data">
                            </div> 
                            <hr>                   
                        </div> 
                    </div>
                    <div class="box-body">
                        <div class="form-group col-md-12" style="border-bottom: 1px solid #f4f4f4;">
                            <label for="inputEmail4"><i class="fa fa-info-circle"></i> Download Template</label>
                        </div>
                        <div class="form-group col-md-4">
                            <a class="btn btn-sm btn-success" href="<?php echo base_url(); ?>assets/templates/template_aicraft_avability.xlsx"><i class="fa fa-file-excel-o"></i> <label for="inputPassword4" style="font-weight: normal"> Template Detail</label></a>           
                        </div>
                        <div class="form-group col-md-6">
                            <a class="btn btn-sm bg-olive" href="<?php echo base_url(); ?>assets/templates/template_performance_avability.xlsx"><i class="fa fa-file-excel-o"></i> <label for="inputPassword4" style="font-weight: normal"> Template Performance </label> </a>  
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="submit" name="submit" id="submit_file" class="btn btn-flat bg-navy"><i class="fa fa-save"></i> Save </button>
                    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-rotate-left"></i> Close </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var date = new Date();
    var month = [];
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var tgl = date.getDate();
    var bulan = month[date.getUTCMonth()];
    var tahun = date.getUTCFullYear();
    var base_url = '<?php echo base_url(); ?>index.php/';
    var yAxisLimit = [0, 25, 50, 75, 100];

    $(document).ready(function () {

        // =============================================================================
        function checkExtention(ini) {
            var namefile=ini.val();
            var ext=namefile.replace(/^.*\./, '');
            ext=ext.toLowerCase();
            extList=["xls","xlsx"];
            return ($.inArray(ext,extList)== -1) ? false:true;
        }
        $(document).on("change", "#files", function (e) {
            // console.log(checkExtention($(this)));
            if(this.files[0].size > 1300000 || checkExtention($(this)) == false){
            notif("error", "File Must Excel Extention");
            $("#files").val('');
            }
        });
        // =============================================================================

        // initial Hide Rows Selected
        // $('#typedataseleted').hide();
        
        // Initial Selected Choose Data
        // $('#dataselected').on('change', function (e) {
        //     e.preventDefault();
        //     if($(this).val()==1){
        //          $('#typedataseleted').hide();
        //          $('#start_rows').val('');
        //          $('#end_rows').val('');
        //     }else{
        //          $('#typedataseleted').show();
        //     }
        // });

        $("#start_rows, #end_rows").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                notif('warning','Only Number');
               return false;
            }
        });
        $('.tempaltes').on('click', function (e) {
            e.preventDefault();
            var link=$(this).data('files');
             window.location.href = link;
            
        });
       

        // Initial Upload Submit
        $('#form_upload').submit(function (e) { 
            e.preventDefault();
            var formData=new FormData($('#form_upload')[0]);
            $.ajax({
                url: "<?= base_url() ?>index.php/Aircraft_Avaibility/storeupload",
                type: "POST",
                processData: false,
                contentType: false,
                data: formData,                
                beforeSend: function () {
                    $('#submit_file').html('<i class="fa fa-spinner fa-spin"></i> Processing').attr('disabled',true);
                }
            })
            .done(function(resp){
                var data= JSON.parse(resp);
                 notif(data.info, data.msg);
                 if(data.info=='success'){
                     $('#modal_upload').modal('hide');
                 }
            })
            .fail(function (){
                notif('error', 'Somethings Eror Connections');
            })
            .always(function(){
                $('#submit_file').html('Save <i class="fa fa-save"></i> ').attr('disabled',false);
                 $("#form_upload")[0].reset();
                //   $('#typedataseleted').hide();
            });
            
        });

        // Uplaoad File Template 
        $(document).on('click','#uploadfile', function (e) {
            e.preventDefault();
            $('#modal_upload').modal('show');
        });



        //Date range picker
        $('#date_end, #date_start').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true
        });
        search();

        $(document).on("click", "#back", function (e) {
            $("#tipe_view").val('');
            $("#status_view").val('0');
            $("#grafik_big").hide();
            $("#grafik_down").show();
        });
    });


    function search() {
        var start_date = $('#date_start').val();
        var end_date = $('#date_end').val();
        var date1 = start_date.split('-')[0];
        var date2 = end_date.split('-')[0];
        var month1 = start_date.split('-')[1];
        var month2 = end_date.split('-')[1];
        var year1 = start_date.split('-')[2];
        var year2 = end_date.split('-')[2];
        var startdate = moment($("#date_start").val(), "DD-MM-YYYY");
        var enddate = moment($("#date_end").val(), "DD-MM-YYYY");

        if (startdate > enddate) {
            notif('warning', 'End date must be larger than start date!');
        }else{

        if (date1 <= date2 && month1 <= month2 && year1 <= year2) {
            grafik_type("A320", "1");
            grafik_type("Classic", "2");
        } else if (date1 >= date2 && month1 <= month2 && year1 <= year2) {
            grafik_type("A320", "1");
            grafik_type("Classic", "2");
        } else if (date1 >= date2 && month1 >= month2 && year1 <= year2) {
            grafik_type("A320", "1");
            grafik_type("Classic", "2");
        } else if (date1 <= date2 && month1 >= month2 && year1 <= year2) {
            grafik_type("A320", "1");
            grafik_type("Classic", "2");
        } else if (date1 >= date2 && month1 == month2 && year1 == year2) {
            notif('error', "Sorry can't, Please check your format date");
        } else {
            notif('error', "Sorry can't, Please check your format date");
        }
      }
    }

    function detail_panel(tipe) {
                    console.log(tipe);
        $('#modal-detail-panel1').modal("show");
        $('#title-detail-panel1').css('color', '#fff');
        $('#header-detail-panel1').css('background-color', '#0f2233');
        if (tipe == 'a320') {
            $('#div-grafik-type1').show();
            $('#div-grafik-type2').hide();
            var con_tipe = 'A320';
            $('#title-detail-panel1').html('<i class="fa fa-line-chart"></i> Tipe A320');
            $.ajax({
                url: "<?= base_url() ?>index.php/Aircraft_Avaibility/view_detail_panel",
                type: "POST",
                data: {
                    'tipe': con_tipe,
                }
            })
            .done(function(resp){
                var data=JSON.parse(resp);
                // console.log(resp.grafik[0].datat);
                notif('success', 'Data successfully displayed');
                    Highcharts.chart('grafik_tipe1', {
                        chart: {
                            height: 300,
                            type: 'line'
                        },
                        exporting: {
                            enabled: false
                        },
                        title: {
                            text: ''
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            min: 0,
                            tickPositioner: function () {
                                return yAxisLimit;
                            },
                            title: {
                                text: 'Percent (%)',
                                style: {
                                    fontSize: '12px',
                                    fontWeight: 'bold',
                                    color: '#333333'
                                }
                            },
                        },
                        xAxis: {
                            categories: data.grafik[0].datat
                        },
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            layout: 'horizontal',
                        },
                        plotOptions: {
                            series: {
                                label: {
                                    connectorAllowed: false
                                },
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function () {
                                            detail_panel_bulanan(con_tipe, this.category)
                                        }
                                    }
                                },
                                color: "#05354D"
                            }
                        },

                        series: [{
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354D"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: con_tipe,
                            data: data.grafik[0].data,
                            dataLabels: {
                                enabled: true,
                                color: '#fff',
                                align: 'center',
                                x: 0,
                                y: 0,
                                rotation: 0,
                            }
                        }],

                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        align: 'center',
                                        verticalAlign: 'bottom',
                                        layout: 'horizontal',
                                    }
                                }
                            }]
                        }
                    });
            })
            .fail(function (){
                console.log('fail');
            })
            .always(function(){
                console.log('always');
            });
            // $.ajax({
            //     url: "<?= base_url() ?>index.php/Aircraft_Avaibility/view_detail_panel",
            //     type: "POST",
            //     data: {
            //         'tipe': con_tipe,
            //     },
            //     dataType: "JSON",
            //     error: function (jqXHR, textStatus, errorThrown) {
            //         notif('error', "Sorry cannot load data, please check your connection");
            //     },
            //     success: function (data) {
            //         notif('success', 'Data successfully displayed');
            //         Highcharts.chart('grafik_tipe1', {
            //             chart: {
            //                 height: 300,
            //                 type: 'line'
            //             },
            //             exporting: {
            //                 enabled: false
            //             },
            //             title: {
            //                 text: ''
            //             },
            //             credits: {
            //                 enabled: false
            //             },
            //             yAxis: {
            //                 min: 0,
            //                 tickPositioner: function () {
            //                     return yAxisLimit;
            //                 },
            //                 title: {
            //                     text: 'Percent (%)',
            //                     style: {
            //                         fontSize: '12px',
            //                         fontWeight: 'bold',
            //                         color: '#333333'
            //                     }
            //                 },
            //             },
            //             xAxis: {
            //                 categories: data.grafik[0].datat
            //             },
            //             legend: {
            //                 align: 'center',
            //                 verticalAlign: 'bottom',
            //                 layout: 'horizontal',
            //             },
            //             plotOptions: {
            //                 series: {
            //                     label: {
            //                         connectorAllowed: false
            //                     },
            //                     cursor: 'pointer',
            //                     point: {
            //                         events: {
            //                             click: function () {
            //                                 detail_panel_bulanan(con_tipe, this.category)
            //                             }
            //                         }
            //                     },
            //                     color: "#05354D"
            //                 }
            //             },

            //             series: [{
            //                 type: 'areaspline',
            //                 fillColor: {
            //                     linearGradient: [0, 0, 0, 200],
            //                     stops: [
            //                         [0, "#05354D"],
            //                         [1, "#fff3"]
            //                     ]
            //                 },
            //                 lineWidth: 1,
            //                 name: con_tipe,
            //                 data: data.grafik[0].data,
            //                 dataLabels: {
            //                     enabled: true,
            //                     color: '#fff',
            //                     align: 'center',
            //                     x: 0,
            //                     y: 0,
            //                     rotation: 0,
            //                 }
            //             }],

            //             responsive: {
            //                 rules: [{
            //                     condition: {
            //                         maxWidth: 500
            //                     },
            //                     chartOptions: {
            //                         legend: {
            //                             align: 'center',
            //                             verticalAlign: 'bottom',
            //                             layout: 'horizontal',
            //                         }
            //                     }
            //                 }]
            //             }
            //         });
            //     }
            // });
        } else {
            $('#div-grafik-type1').hide();
            $('#div-grafik-type2').show();
            var con_tipe = 'Classic';
            $('#title-detail-panel1').html('<i class="fa fa-line-chart"></i> Tipe Classic');
            $('#grafik_type2').show();
            $.ajax({
                url: "<?= base_url() ?>index.php/Aircraft_Avaibility/view_detail_panel",
                type: "POST",
                data: {
                    'tipe': con_tipe,
                }
            })
            .done(function(resp){
                var data=JSON.parse(resp);
                notif('success', 'Data successfully displayed');
                    Highcharts.chart('grafik_tipe2', {
                        chart: {
                            height: 300,
                            type: 'line'
                        },
                        exporting: {
                            enabled: false
                        },
                        title: {
                            text: ''
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            min: 0,
                            tickPositioner: function () {
                                return yAxisLimit;
                            },
                            title: {
                                text: 'Percent (%)',
                                style: {
                                    fontSize: '12px',
                                    fontWeight: 'bold',
                                    color: '#333333'
                                }
                            },
                        },
                        xAxis: {
                            categories: data.grafik[0].datat
                        },
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            layout: 'horizontal',
                        },

                        plotOptions: {
                            series: {
                                label: {
                                    connectorAllowed: false
                                },
                                cursor: 'pointer',
                                point: {
                                    events: {
                                        click: function () {
                                            detail_panel_bulanan(con_tipe, this.category)
                                        }
                                    }
                                },
                                color: "#05354D"
                            }
                        },

                        series: [{
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354D"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: con_tipe,
                            data: data.grafik[0].data,
                            dataLabels: {
                                enabled: true,
                                color: '#fff',
                                align: 'center',
                                x: 0,
                                y: 0,
                                rotation: 0,
                            }
                        }],

                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        align: 'center',
                                        verticalAlign: 'bottom',
                                        layout: 'horizontal',
                                    }
                                }
                            }]
                        }
                    });

            })
            .fail(function(){
                console.log('fail');
                notif('error', "Sorry cannot load data, please check your connection");
            })
            .always(function(){
                console.log('always');
            });
            // $.ajax({
            //     url: "<?= base_url() ?>index.php/Aircraft_Avaibility/view_detail_panel",
            //     type: "POST",
            //     data: {
            //         'tipe': con_tipe,
            //     },
            //     dataType: "JSON",
            //     error: function (jqXHR, textStatus, errorThrown) {
            //         notif('error', "Sorry cannot load data, please check your connection");
            //     },
            //     success: function (data) {
            //         notif('success', 'Data successfully displayed');
            //         Highcharts.chart('grafik_tipe2', {
            //             chart: {
            //                 height: 300,
            //                 type: 'line'
            //             },
            //             exporting: {
            //                 enabled: false
            //             },
            //             title: {
            //                 text: ''
            //             },
            //             credits: {
            //                 enabled: false
            //             },
            //             yAxis: {
            //                 min: 0,
            //                 tickPositioner: function () {
            //                     return yAxisLimit;
            //                 },
            //                 title: {
            //                     text: 'Percent (%)',
            //                     style: {
            //                         fontSize: '12px',
            //                         fontWeight: 'bold',
            //                         color: '#333333'
            //                     }
            //                 },
            //             },
            //             xAxis: {
            //                 categories: data.grafik[0].datat
            //             },
            //             legend: {
            //                 align: 'center',
            //                 verticalAlign: 'bottom',
            //                 layout: 'horizontal',
            //             },

            //             plotOptions: {
            //                 series: {
            //                     label: {
            //                         connectorAllowed: false
            //                     },
            //                     cursor: 'pointer',
            //                     point: {
            //                         events: {
            //                             click: function () {
            //                                 detail_panel_bulanan(con_tipe, this.category)
            //                             }
            //                         }
            //                     },
            //                     color: "#05354D"
            //                 }
            //             },

            //             series: [{
            //                 type: 'areaspline',
            //                 fillColor: {
            //                     linearGradient: [0, 0, 0, 200],
            //                     stops: [
            //                         [0, "#05354D"],
            //                         [1, "#fff3"]
            //                     ]
            //                 },
            //                 lineWidth: 1,
            //                 name: con_tipe,
            //                 data: data.grafik[0].data,
            //                 dataLabels: {
            //                     enabled: true,
            //                     color: '#fff',
            //                     align: 'center',
            //                     x: 0,
            //                     y: 0,
            //                     rotation: 0,
            //                 }
            //             }],

            //             responsive: {
            //                 rules: [{
            //                     condition: {
            //                         maxWidth: 500
            //                     },
            //                     chartOptions: {
            //                         legend: {
            //                             align: 'center',
            //                             verticalAlign: 'bottom',
            //                             layout: 'horizontal',
            //                         }
            //                     }
            //                 }]
            //             }
            //         });
            //     }
            // });
        }
    }

    function detail_panel_bulanan(tipe, bulan) {
        $('#modal-detail-panel2').modal("show");
        $('#title-detail-panel2').css('color', '#fff');
        $('#header-detail-panel2').css('background-color', '#0f2233');
        $('#title-detail-panel2').html('<i class="fa fa-line-chart"></i> Tipe' + ' ' + tipe + ' ' + '|' + ' ' + 'Bulan' + ' ' + bulan);
        $.ajax({
            url: "<?= base_url() ?>index.php/Aircraft_Avaibility/view_detail_panel_grafik",
            type: "POST",
            data: {
                'tipe': tipe,
                'bulan': bulan,
            },
            dataType: "JSON",
            error: function (jqXHR, textStatus, errorThrown) {
                notif('error', "Sorry cannot load data, please check your connection");
            },
            success: function (data) {
                notif('success', 'Data successfully displayed');
                Highcharts.chart('grafik_tipe_bulanan', {
                    chart: {
                        height: 350,
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        useHTML: true,
                        text: ''
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        title: {
                            text: 'Total Aircraft',
                            style: {
                                fontSize: '12px',
                                fontWeight: 'bold',
                                color: '#333333'
                            }
                        },
                        min: 0,
                    },
                    xAxis: {
                        categories: data.grafik[0].datat
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal',
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                        }
                    },
                    series: [
                        {
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354D"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "Actual",
                            data: data.grafik[0].data,
                            color: "#05354D",
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function () {
                                        detail_panel_tabel_perbulan(tipe, this.category)
                                    }
                                }
                            },
                            dataLabels: {
                                enabled: true,
                                color: "#05354D",
                                align: 'center',
                                enabled: true,
                                x: -5,
                                y: 3
                            }
                        },
                        {
                            name: "Target",
                            data: data.grafik[0].datap,
                            color: "#F2C573",
                            lineWidth: 1,
                            marker: {
                                enabled: false
                            },
                            // dataLabels: {
                            //     enabled: true,
                            //     color: '#F2C573',
                            //     align: 'center',
                            //     x: 0,
                            //     y: 35,
                            //     rotation: 0,
                            // }
                        }
                    ],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal',
                                }
                            }
                        }]
                    }
                });
            }
        });
    }

    function top_remarks3(tipe){
      $("#modal-detail-panel4").modal('show');
      $('#title-detail-panel4').css('color', '#fff');
      $('#header-detail-panel4').css('background-color', '#0f2233');
      $("#title-detail-panel4").html('<i class="fa fa-list-ol"></i>  TOP 3 TROUBLE '+tipe);
      var date_start = $("#date_start").val();
      var date_end = $("#date_end").val();
      notif('success', 'Data '+tipe+' successfully displayed');

      $("#detail-table-topremarks").DataTable().clear().draw();
      $('#detail-table-topremarks').DataTable().destroy();
      $.ajax({
        url: '<?= base_url() ?>index.php/Aircraft_Avaibility/datatable_top_trouble',
        type: 'POST',
        dataType: 'JSON',
        data:{
          tipe: tipe,
          date_start: date_start,
          date_end: date_end
        },
        beforeSend: function(){
          $("#loader98").fadeIn("fast");
        }
      })
      .done(function(data) {
        $("#loader98").fadeOut("fast");
        $("#detail-table-topremarks").show();
        var no = 1;
        $.each(data, function(index, list){
          var row = $("#detail-table-topremarks").DataTable().row.add(
            [
              "<center>"+no+"</center>",
              list.kejadian,
              "<center>"+list.total+"</center>"
            ]
          ).draw().node();
          no++;
        })

        notif('success', 'Data '+tipe+' successfully displayed');
      })
      .fail(function() {
        console.log("error");
      })
      .always(function(data) {
        console.log(data);
      });

      // $('#detail-table-topremarks').DataTable().destroy();
      // table = $('#detail-table-topremarks').DataTable({
      //               "ajax": {
      //                   "type":'POST',
      //                   "url": '<?= base_url() ?>index.php/Aircraft_Avaibility/datatable_top_trouble',
      //                   "data": {
      //                       "tipe": tipe,
      //                       "date_start": date_start,
      //                       "date_end": date_end
      //                   }
      //
      //               },
      //               "columns": [
      //                   {"data": "NO"},
      //                   {"data": "CATEGORY"},
      //                   {"data": "ACCIDENT"}
      //               ]
      //           });

    }

    function detail_panel_tabel_perbulan(tipe, tanggal) {
        $('#modal-detail-panel3').modal("show");
        $('#title-detail-panel3').css('color', '#fff');
        $('#header-detail-panel3').css('background-color', '#0f2233');
        $('#title-detail-panel3').html('<i class="fa fa-calendar"></i>' + ' ' + tanggal);
        notif('success', 'Data successfully displayed');
        $("#detail-table-perbulan").DataTable({
            destroy: true,
            ordering: true,
            searching: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: {
                url: "<?= base_url() ?>index.php/Aircraft_Avaibility/datatable",
                type: 'POST',
                data: {
                    'tipe': tipe,
                    'tanggal': tanggal,
                },
            },
            "aoColumns": [
                {
                    "mData": "0",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "1",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "2",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "3",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "4",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "5",
                    "mRender": function (data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
            ]
        });
    }


    function grafik_type(tipe, no) {
        var start = $('#date_start').val();
        var end = $('#date_end').val();
        $.ajax({
            url: "<?= base_url() ?>index.php/Aircraft_Avaibility/view_grafik",
            type: "POST",
            data: {
                'start': start,
                'end': end,
                'tipe': tipe,
            },
            dataType: "JSON",
            error: function (jqXHR, textStatus, errorThrown) {
                notif('error', "Sorry cannot load data, please check your connection");
            },
            beforeSend: function () {
                $("#search").prop('disabled', true).html("<i class='fa fa-refresh fa-spin'></i> Please Wait ..");
            },
            success: function (data) {
              $("#search").prop('disabled', false).html('<i class="fa fa-search"></i> Search');
                if (data.grafik[0].data.length == 0) {
                    notif('warning', "No data available");
                } else {
                    var colorGrafik = "#0D2633";
                    if (tipe == "A320") {
                        var showOrHide1 = '';
                        colorGrafik = "#0D2633";
                        if (data.grafik[0].data.every(isBelowThreshold)) {
                            $("#grafik1Responsive").hide();
                            $('#a320Header').hide();
                        } else {
                            $("#grafik1Responsive").show();
                            $('#a320Header').show();
                            notif('success', "Data A320 successfully displayed");
                            $("#search").prop('disabled', false).html('<i class="fa fa-search"></i> Search');

                        }

                    } else if (tipe == "Classic") {
                        var showOrHide2 = '';
                        colorGrafik = "#0535ED";
                        if (data.grafik[0].data.every(isBelowThreshold)) {
                            $("#grafik2Responsive").hide();
                            $('#classicHeader').hide();
                        } else {
                            $("#grafik2Responsive").show();
                            $('#classicHeader').show();
                            notif('success', "Data Classic successfully displayed");
                            $("#search").prop('disabled', false).html('<i class="fa fa-search"></i> Search');
                        }

                    }

                    if (showOrHide1 == true && showOrHide2 == true) {
                        $('div[id^="grafik1Responsive"]').removeClass('col-lg-12').removeClass('col-md-12').addClass('col-lg-6').addClass('col-md-6');
                        $('div[id^="grafik2Responsive"]').removeClass('col-lg-12').removeClass('col-md-12').addClass('col-lg-6').addClass('col-md-6');
                    }
                    if (showOrHide1 == true && showOrHide2 == false) {
                        $('div[id^="grafik1Responsive"]').removeClass('col-lg-6').removeClass('col-md-6').addClass('col-lg-12').addClass('col-md-12');
                    }
                    if (showOrHide1 == false && showOrHide2 == true) {
                        $('div[id^="grafik2Responsive"]').removeClass('col-lg-6').removeClass('col-md-6').addClass('col-lg-12').addClass('col-md-12');
                    }

                    var chart = new Highcharts.chart('grafik' + no, {
                        chart: {
                            height: '300',
                            //width: '500',
                            type: 'line'
                        },
                        exporting: {
                            enabled: false
                        },
                        title: {
                          text : tipe
                            // useHTML: true,
                            // margin: 0,
                            // text: '<a class="hovertest" style="cursor:pointer; text-decoration: underline;" data-toggle="tooltip" title="Click for zoom" href="javascript:void(0)" onclick="zoom_grafik(\'' + tipe + '\')">' + tipe + '</a>'
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            title: {
                                text: 'Total Aircraft',
                                style: {
                                    fontSize: '12px',
                                    fontWeight: 'bold',
                                    color: '#333333'
                                }
                            },
                            min: 0,
                        },
                        xAxis: {
                            categories: data.grafik[0].datat
                        },
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            layout: 'horizontal',

                        },

                        plotOptions: {
                            series: {
                                label: {
                                    connectorAllowed: false
                                },
                            }
                        },
                        series: [{
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354D"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                            name: "Actual",
                            data: data.grafik[0].data,
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function () {
                                        detail_panel_tabel_perbulan(tipe, this.category)
                                    }
                                }
                            },
                            color: "#05354D",
                        },
                            {
                                name: "Target",
                                data: data.grafik[0].datap,
                                color: "#F2C573",
                                lineWidth: 1,
                                marker: {
                                    enabled: false
                                }
                            }
                        ],

                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        align: 'center',
                                        verticalAlign: 'bottom',
                                        layout: 'horizontal',
                                    }
                                }
                            }]
                        }
                    }
                    , function (chart) {
                        // $('.highcharts-title').click(function () {
                        //     $("#grafik_down").hide();
                        //     $("#grafik_big").show();
                        //     $("#status_view").val('1');
                        //     $("#tipe_view").val($(this).text());
                        //
                        // });
                    });
                }
            }
        });
    }

    function zoom_grafik(tipe) {
        var start = $("#date_start").val();
        var end = $("#date_end").val();
        var colorGrafik = "#0D2633";
        $.ajax({
            url: "<?= base_url() ?>index.php/Aircraft_Avaibility/view_grafik",
            type: "POST",
            data: {
                'start': start,
                'end': end,
                'tipe': tipe,
            },
            dataType: "JSON",
            success: function (data) {
              $("#grafik_down").hide();
              $("#grafik_big").show();
              $("#status_view").val('1');
              $("#tipe_view").val($(this).text());

                var chart = new Highcharts.chart('grafikzoom', {
                    chart: {
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        margin: 0,
                        useHTML: true,
                        text: ''
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        title: {
                            text: 'Total Aircraft',
                            style: {
                                fontSize: '12px',
                                fontWeight: 'bold',
                                color: '#333333'
                            }
                        },
                        min: 0,
                        // tickPositioner: function () {
                        //     return yAxisLimit;
                        // },
                    },
                    xAxis: {
                        categories: data.grafik[0].datat
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal',
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                        },
                        area: {
                            areaPadding: 3
                        }
                    },

                    series: [{
                        type: 'areaspline',
                        fillColor: {
                            linearGradient: [0, 0, 0, 200],
                            stops: [
                                [0, "#05354D"],
                                [1, "#fff3"]
                            ]
                        },
                        lineWidth: 1,
                        name: "Actual",
                        data: data.grafik[0].data,
                        color: "#05354D",
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    detail_panel_tabel_perbulan(tipe, this.category)
                                }
                            }
                        },
                        dataLabels: {
                            enabled: true,
                            color: '#05354D',
                            align: 'center',
                            x: 0,
                            // y: -20,
                            rotation: 0,
                        }
                    },
                        {
                            name: "Target",
                            data: data.grafik[0].datap,
                            color: "#F2C573",
                            lineWidth: 1,
                            marker: {
                                enabled: false
                            },
                            // dataLabels: {
                            //     enabled: true,
                            //     color: '#F2C573',
                            //     align: 'center',
                            //     x: 0,
                            //     y: 35,
                            //     rotation: 0,
                            // }
                        }
                    ],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal',
                                }
                            }
                        }]
                    }
                });

            }
        });
    }

    function isBelowThreshold(currentValue) {
        return currentValue == 0;
    }
</script>


<div class="modal fade" id="modal-detail-panel1" tabindex="-1" role="dialog" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" id="header-detail-panel1">
                <h4 class="modal-title" id="title-detail-panel1"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="div-grafik-type1" style="display: none">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div id="grafik_tipe1"></div>
                        </div>
                    </div>
                    <div id="div-grafik-type2" style="display: none">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div id="grafik_tipe2"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-rotate-left"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail-panel2" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" id="header-detail-panel2">
                <h4 class="modal-title" id="title-detail-panel2"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div id="grafik_tipe_bulanan"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-rotate-left"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-detail-panel3" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" id="header-detail-panel3">
                <h4 class="modal-title" id="title-detail-panel3"></h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover table-bordered" id="detail-table-perbulan">
                    <thead class="bg-navy">
                    <tr>
                        <th class="text-center" style="width: 50px">Date</th>
                        <th class="text-center">Act Serv.</th>
                        <th class="text-center">Target</th>
                        <th class="text-center">Availability</th>
                        <th class="text-center">Reg</th>
                        <th class="text-center" style="width: 250px;">Remarks</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-rotate-left"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail-panel4" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" id="header-detail-panel4">
                <h4 class="modal-title" id="title-detail-panel4"></h4>
            </div>
            <div class="modal-body">
              <div class="overlay-box" id="loader98"
                   style="display: none; position:  absolute;background-color: #ffffff;/* margin: auto; */top: 100 !important;left: 0;right: 0;width: 100%;height: 100%;z-index: 9;">
                  <div style="" class="loading"></div>
              </div>
                <table class="table table-hover table-bordered" id="detail-table-topremarks">
                    <thead class="bg-navy">
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Category</th>
                        <th class="text-center">Total Accident</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-rotate-left"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>

<!--
END EDIT
BY DIMAS ISLAMI
-->
