<!--
START EDIT
BY DIMAS ISLAMI
-->

<style>
    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 10px;
        width: 100%;
        box-shadow: 0 8px 12px 4px rgba(0,0,0,0.2);
        height: 400px;
    }
    .box-body {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        padding: 5px;
    }
    .box-header {
        color: #444;
        display: block;
        padding: 8px;
        position: relative;
    }
    .box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
        display: inline-block;
        font-size: 20px;
        font-weight: bold;
        margin: 0;
        line-height: 1;
    }
    .wadah {
        padding-left: 5px;
        padding-right: 5px;
    }
    .btn-box-tool {
        padding: 5px;
        font-size: 12px;
        background: transparent;
        color: #0f2233;
    }
    .loading-mro {
        position: absolute;
        left: 50%;
        top: 100%;
        z-index: 1;
        width: 150px;
        height: 150px;
        margin: 80px 0 0 -70px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }
    .loading-mro-zoom {
        position: absolute;
        left: 50%;
        top: 10%;
        z-index: 1;
        width: 150px;
        height: 150px;
        margin: 80px 0 0 -70px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }
    .hoverbox:hover{
        opacity: 0.8;
    }
    .back{
        background: grey;
        color: #fff;
    }
    .size-box{
        height: 400px;
    }
</style>


<section class="content-header">
    <h1>
        <b><?= $title?></b>
        <small><?= $small_tittle ?></small>
    </h1>
</section>

<section class="content">
    <div class="row">

        <!-- GRAFIK YEAR-->
        <div class="wadah col-lg-4 col-md-12 col-sm-12 col-xs-12" id="head-year">
            <div class="box box-default hoverbox" onclick="hide('year')" style="cursor: pointer;">
                <div class="box-header with-border">
                    <center><h3 class="box-title" style="font-weight: bold;"><i class="fa fa-bar-chart"></i> Chart by Year</h3></center>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"><i class="fa fa-bars"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="chart-responsive">
                                <input type="hidden" id="year1" value="<?= $year_from ?>">
                                <input type="hidden" id="year2" value="<?= $year_to ?>">
                                <select id="g1_aircraft_type" style="display:none">
                                    <option value="all" disabled selected>Aircraft Types</option>
                                    <option value="all">All</option>
                                    <?php foreach ($aircraft_types as $item): ?>
                                        <option value="<?php echo $item ?>">
                                            <?= $item; ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                                <select id="g1_engine_type" style="display: none">
                                    <option value="all" disabled selected>Engine Type</option>
                                    <option value="all">All</option>
                                    <?php foreach ($engine_types as $item): ?>
                                        <option value="<?php echo $item['id_engine_family'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                                <select id="g1_region" style="display: none">
                                    <option value="all" disabled selected>Region</option>
                                    <option value="all">All</option>
                                    <?php foreach ($regions as $item): ?>
                                        <option value="<?php echo $item['id_region'] ?>">
                                            <?php echo $item['region'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                                <select id="g1_operator" style="display: none">
                                    <option value="all" disabled selected>Operator Name</option>
                                    <option value="all">All</option>
                                    <?php foreach ($operators as $item): ?>
                                        <option value="<?php echo $item['id_operator'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                                <div style="display: none;" class="loading-mro" id="loader1" style=""></div>
                                <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="grafik1div" style="display: none">
                                    <div id="grafik1" style="height: 300px;width: 100%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- GRAFIK YEAR ZOOM-->
        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="detail-year" style="display: none">
            <div class="box box-default">
                <div class="box-header with-border">
                    <center><h3 class="box-title"><i class="fa fa-bar-chart"></i> Chart by Year</h3></center>
                    <div class="box-tools pull-right" style="top: 3px;">
                        <button type="button" class="btn btn-sm btn-default pull-right" onclick="back('year')">
                            <i class="fa  fa-arrow-circle-left"></i> Back
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="year1_detail" value="<?= $year_from ?>" placeholder="Start Year">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="year2_detail" value="<?= $year_to ?>" placeholder="End Year">
                                </div>
                            </div>
                            <div class="form-group">
                                <select id="g1_aircraft_type_detail" class="form-control select2" style="width: 100%;">
                                    <option value="all" disabled selected>Aircraft Types</option>
                                    <option value="all">All</option>
                                    <?php foreach ($aircraft_types as $item): ?>
                                        <option value="<?php echo $item ?>">
                                            <?= $item; ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="g1_engine_type_detail" class="form-control select2" style="width: 100%;">
                                    <option value="all" disabled selected>Engine Type</option>
                                    <option value="all">All</option>
                                    <?php foreach ($engine_types as $item): ?>
                                        <option value="<?php echo $item['id_engine_family'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="g1_region_detail" class="form-control select2" style="width: 100%;">
                                    <option value="all" disabled selected>Region</option>
                                    <option value="all">All</option>
                                    <?php foreach ($regions as $item): ?>
                                        <option value="<?php echo $item['id_region'] ?>">
                                            <?php echo $item['region'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="g1_operator_detail" class="form-control select2" style="width: 100%;">
                                    <option value="all" disabled selected>Operator Name</option>
                                    <option value="all">All</option>
                                    <?php foreach ($operators as $item): ?>
                                        <option value="<?php echo $item['id_operator'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <button onclick="search('year')" class="btn btn-block bg-navy">
                                    <i class="fa fa-search"></i> Search
                                </button>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                            <div class="chart-responsive">
                                <div style="display: none;" class="loading-mro-zoom" id="loader1d"></div>
                                <div id="grafik1ddiv" style="display: none">
                                    <div id="grafik1d" style="height:300px;width: 100%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- GRAFIK REGION-->
        <div class="wadah col-lg-4 col-md-12 col-sm-12 col-xs-12" id="head-region">
            <div class="box box-default hoverbox" onclick="hide('region')" style="cursor: pointer;">
                <div class="box-header with-border">
                    <center><h3 class="box-title" style="font-weight: bold;"><i class="fa fa-bar-chart"></i> Chart by Region</h3></center>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"><i class="fa fa-bars"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="chart-responsive">
                                <input type="hidden" id="year3" value="<?= $year_from ?>">
                                <input type="hidden" id="year4" value="<?= $year_to ?>">
                                <select id="g2_aircraft_type" style="display: none">
                                    <option value="all" disabled selected>Aircraft Types</option>
                                    <option value="all">All</option>
                                    <?php foreach ($aircraft_types as $item): ?>
                                        <option value="<?php echo $item ?>">
                                            <?= $item; ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                                <select id="g2_engine_type" style="display: none">
                                    <option value="all" disabled selected>Engine Type</option>
                                    <option value="all">All</option>
                                    <?php foreach ($engine_types as $item): ?>
                                        <option value="<?php echo $item['id_engine_family'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                                <select id="g2_operator" style="display: none">
                                    <option value="all" disabled selected>Operator Name</option>
                                    <option value="all">All</option>
                                    <?php foreach ($operators as $item): ?>
                                        <option value="<?php echo $item['id_operator'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                                <div style="display: none;" class="loading-mro" id="loader2"></div>
                                <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="grafik2div" style="display: none">
                                    <div id="grafik2" style="height: 300px;width: 100%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- GRAFIK REGION ZOOM-->
        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="detail-region" style="display: none">
            <div class="box box-default">
                <div class="box-header with-border">
                    <center><h3 class="box-title"><i class="fa fa-bar-chart"></i> Chart by Region</h3></center>
                    <div class="box-tools pull-right" style="top: 3px;">
                        <button type="button" class="btn btn-sm btn-default pull-right" onclick="back('region')">
                            <i class="fa  fa-arrow-circle-left"></i> Back
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="year3_detail" value="<?= $year_from ?>" placeholder="Start Year">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="year4_detail" value="<?= $year_to ?>" placeholder="End Year">
                                </div>
                            </div>
                            <div class="form-group">
                                <select id="g2_aircraft_type_detail" class="form-control select2" style="width: 100%;">
                                    <option value="all" disabled selected>Aircraft Types</option>
                                    <option value="all">All</option>
                                    <?php foreach ($aircraft_types as $item): ?>
                                        <option value="<?php echo $item ?>">
                                            <?= $item; ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="g2_engine_type_detail" class="form-control select2" style="width: 100%;">
                                    <option value="all" disabled selected>Engine Type</option>
                                    <option value="all">All</option>
                                    <?php foreach ($engine_types as $item): ?>
                                        <option value="<?php echo $item['id_engine_family'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="g2_operator_detail" class="form-control select2" style="width: 100%;">
                                    <option value="all" disabled selected>Operator Name</option>
                                    <option value="all">All</option>
                                    <?php foreach ($operators as $item): ?>
                                        <option value="<?php echo $item['id_operator'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <button onclick="search('region')" class="btn btn-block bg-navy">
                                    <i class="fa fa-search"></i> Search
                                </button>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                            <div class="chart-responsive">
                                <div style="display: none;" class="loading-mro-zoom" id="loader2d"></div>
                                <div id="grafik2ddiv" style="display: none">
                                    <div id="grafik2d" style="height:300px;width: 100%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- GRAFIK BUSINESS SEGMENT-->
        <div class="wadah col-lg-4 col-md-12 col-sm-12 col-xs-12" id="head-bs">
            <div class="box box-default hoverbox" onclick="hide('business_segment')" style="cursor: pointer;">
                <div class="box-header with-border">
                    <center><h3 class="box-title" style="font-weight: bold;"><i class="fa fa-bar-chart"></i> Chart by Business</h3></center>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"><i class="fa fa-bars"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="chart-responsive">
                                <input type="hidden" id="year5" value="<?= $year_from ?>">
                                <input type="hidden" id="year6" value="<?= $year_to ?>">
                                
                                <select id="g3_aircraft_type" style="display: none">
                                    <option value="all" disabled selected>Aircraft Types</option>
                                    <option value="all">All</option>
                                    <?php foreach ($aircraft_types as $item) : ?>
                                        <option value="<?php echo $item ?>">
                                            <?= $item; ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            
                                <select id="g3_engine_type" style="display: none">
                                    <option value="all" disabled selected>Engine Type</option>
                                    <option value="all">All</option>
                                    <?php foreach ($engine_types as $item): ?>
                                        <option value="<?php echo $item['id_engine_family'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                                <select id="g3_region" style="display: none">
                                    <option value="all" disabled selected>Region</option>
                                    <option value="all">All</option>
                                    <?php foreach ($regions as $item): ?>
                                        <option value="<?php echo $item['id_region'] ?>">
                                            <?php echo $item['region'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                                <select id="g3_operator" style="display: none">
                                    <option value="all" disabled selected>Operator Name</option>
                                    <option value="all">All</option>
                                    <?php foreach ($operators as $item): ?>
                                        <option value="<?php echo $item['id_operator'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                                <div style="display: none;" class="loading-mro" id="loader3"></div>
                                <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="grafik3div" style="display: none">
                                    <div id="grafik3" style="height: 300px;width: 100%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- GRAFIK BUSINESS SEGMENT ZOOM-->
        <div class="wadah col-lg-12 col-md-12 col-sm-12 col-xs-12" id="detail-bs" style="display: none">
            <div class="box box-default">
                <div class="box-header with-border">
                    <center><h3 class="box-title"><i class="fa fa-bar-chart"></i> Chart by Business</h3></center>
                    <div class="box-tools pull-right" style="top: 3px;">
                        <button type="button" class="btn btn-sm btn-default pull-right" onclick="back('business_segment')">
                            <i class="fa  fa-arrow-circle-left"></i> Back
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="year5_detail" value="<?= $year_from ?>" placeholder="Start Year">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="year6_detail" value="<?= $year_to ?>" placeholder="End Year">
                                </div>
                            </div>
                            <div class="form-group">
                                <select id="g3_aircraft_type_detail" class="form-control select2" style="width: 100%;">
                                    <option value="all" disabled selected>Aircraft Types</option>
                                    <option value="all">All</option>
                                    <?php foreach ($aircraft_types as $item) : ?>
                                        <option value="<?php echo $item ?>">
                                            <?= $item; ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="g3_engine_type_detail" class="form-control select2" style="width: 100%;">
                                    <option value="all" disabled selected>Engine Type</option>
                                    <option value="all">All</option>
                                    <?php foreach ($engine_types as $item): ?>
                                        <option value="<?php echo $item['id_engine_family'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="g3_region_detail" class="form-control select2" style="width: 100%;">
                                    <option value="all" disabled selected>Region</option>
                                    <option value="all">All</option>
                                    <?php foreach ($regions as $item): ?>
                                        <option value="<?php echo $item['id_region'] ?>">
                                            <?php echo $item['region'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="g3_operator_detail" class="form-control select2" style="width: 100%;">
                                    <option value="all" disabled selected>Operator Name</option>
                                    <option value="all">All</option>
                                    <?php foreach ($operators as $item): ?>
                                        <option value="<?php echo $item['id_operator'] ?>">
                                            <?php echo $item['name'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <button onclick="search('business_segment')" class="btn btn-block bg-navy">
                                    <i class="fa fa-search"></i> Search
                                </button>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                            <div class="chart-responsive">
                                <div style="display: none;" class="loading-mro-zoom" id="loader3d"></div>
                                <div id="grafik3ddiv" style="display: none">
                                    <div id="grafik3d" style="height:300px;width: 100%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>


<script>

    // Loop untuk tahun di grafik awal
    for (var i = 1; i <= 6; i++) {
        $('#year' + i).datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });
    }

    // Loop untuk tahun di grafik zoom
    for (var i = 1; i <= 6; i++) {
        $('#year' + i + '_detail').datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });
    }

    // Load pertama kali
    $(document).ready(function () {
        load_chart_year();
        load_chart_region();
        load_chart_business_segments();
        //back_grafik_year();

    });


//      $("a.sidebar-toggle").on('click', function (e) {
//          $(this).closest('body').hasClass('sidebar-toggle');
//          console.log($(e.target).is('a:not(".sidebar-toggle")'))
// //          if($(this).closest('body').hasClass('sidebar-toggle')){
// //              alert('ii')
// //          }else{
// //              alert('ppp')
// //          }
// //         alert('xxx')
//      });

//      $(function () {
//   $('.sidebar-toggle').on('click', function (e) {
//     if($('.sidebar-collapse').toggleClass('open')){
//         console.log('open')
//     }
//     console.log('close')

//   });

//   // $('*:not(.sidebar-toggle)').on('click', function () {
//   //   $('.sidebar-collapse').removeClass('open');
//   //   console.log('close')
//   // });

// });

    // Searching data
    function search(tipe) {
        if (tipe == 'year') {
            load_chart_year_detail();
        } else if (tipe == 'region') {
            load_chart_region_detail();
        } else {
            load_chart_business_segments_detail();
        }
    }

    // Hide chart dan div awal
    function hide(tipe) {
        if (tipe == 'year') {
            $('#head-year').hide();
            $('#head-region').hide();
            $('#head-bs').hide();
            load_chart_year_detail();
            $('#detail-year').show();
        } else if (tipe == 'region') {
            $('#head-year').hide();
            $('#head-region').hide();
            $('#head-bs').hide();
            load_chart_region_detail();
            $('#detail-region').show();
        } else {
            $('#head-year').hide();
            $('#head-region').hide();
            $('#head-bs').hide();
            load_chart_business_segments_detail();
            $('#detail-bs').show();
        }
    }
    // Show and hide chart dan div
    function back(tipe) {
        if (tipe == 'year') {
            $('#head-year').show();
            $('#head-region').show();
            $('#head-bs').show();
            $('#detail-year').hide();
        } else if (tipe == 'region') {
            $('#head-year').show();
            $('#head-region').show();
            $('#head-bs').show();
            $('#detail-region').hide();
        } else {
            $('#head-year').show();
            $('#head-region').show();
            $('#head-bs').show();
            $('#detail-bs').hide();
        }
    }

    // Load data chart year
    function load_chart_year() {
        var year_from = $("#year1").val();
        var year_to = $("#year2").val();
        var aircraft_type = $('#g1_aircraft_type').find(":selected").val();
        var engine_type = $('#g1_engine_type').find(":selected").val();
        var region = $('#g1_region').find(":selected").val();
        var operator = $('#g1_operator').find(":selected").val();

        $.ajax({
            url: "<?= base_url(); ?>index.php/mro_dashboard/get_data_by_year",
            data: {
                'year_from': year_from,
                'year_to': year_to,
                'aircraft_type': aircraft_type,
                'engine_type_id': engine_type,
                'region_id': region,
                'operator_id': operator,
                'tipe': 0
            },
            type: 'POST',
            dataType: "JSON",
            beforeSend: function () {
                $("#loader1").fadeIn(1000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                notif('error', "Sorry cannot load data, please check your connection");
            },
            success: function (data) {
                notif('success', 'Data year successfully displayed');
                Highcharts.chart('grafik1', {
                    chart: {
                        type: 'column',
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: '',
                        style: {
                            font: '25px Arial, sans-serif',
                            fontWeight: 'bold',
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        categories: data.categories
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Cost',
                            style: {
                                fontSize: '12px',
                                fontWeight: 'bold',
                                color: '#333333'
                            }
                        },
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>USD {point.y}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false,
                                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                            }
                        }
                    },
                    series: data.series,
                });
            }
        })
                .done(function () {
                    $("#loader1").fadeOut(1000, function () {
                        $("#grafik1div").fadeIn(1000);
                    });
                })
    }

    // Load data chart year tampilan zoom
    function load_chart_year_detail() {
        var year_from = $("#year1_detail").val();
        var year_to = $("#year2_detail").val();
        var aircraft_type = $('#g1_aircraft_type_detail').find(":selected").val();
        var engine_type = $('#g1_engine_type_detail').find(":selected").val();
        var region = $('#g1_region_detail').find(":selected").val();
        var operator = $('#g1_operator_detail').find(":selected").val();

        $.ajax({
            url: "<?= base_url(); ?>index.php/mro_dashboard/get_data_by_year",
            data: {
                'year_from': year_from,
                'year_to': year_to,
                'aircraft_type': aircraft_type,
                'engine_type_id': engine_type,
                'region_id': region,
                'operator_id': operator,
                'tipe': 1
            },
            type: 'POST',
            dataType: "JSON",
            beforeSend: function () {
                $("#loader1d").fadeIn(1000);
            },
            success: function (data) {
                Highcharts.chart('grafik1d', {
                    chart: {
                        type: 'column',
                        // height: '320',
                        // width: '750',
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: '',
                        style: {
                            font: '25px Arial, sans-serif',
                            fontWeight: 'bold',
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        categories: data.categories
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Cost',
                            style: {
                                fontSize: '12px',
                                fontWeight: 'bold',
                                color: '#333333'
                            }
                        },
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>USD {point.y}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false,
                                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                            }
                        }
                    },
                    legend: {
                        enabled: true
                    },
                    series: data.series
                });
            }
        })
                .done(function () {
                    $("#loader1d").fadeOut(1000, function () {
                        $("#grafik1ddiv").fadeIn(1000);
                    });
                })
    }

    // Load data chart region
    function load_chart_region() {
        var year_from = $("#year3").val();
        var year_to = $("#year4").val();
        var aircraft_type = $('#g2_aircraft_type').find(":selected").val();
        var engine_type = $('#g2_engine_type').find(":selected").val();
        var operator = $('#g2_operator').find(":selected").val();
        $.ajax({
            url: "<?= base_url(); ?>index.php/mro_dashboard/get_data_by_region",
            data: {
                'year_from': year_from,
                'year_to': year_to,
                'aircraft_type': aircraft_type,
                'engine_type_id': engine_type,
                'operator_id': operator,
                'tipe': 0
            },
            type: 'POST',
            dataType: "JSON",
            beforeSend: function () {
                $("#loader2").fadeIn(1000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                notif('error', "Sorry cannot load data, please check your connection");
            },
            success: function (data) {
                notif('success', 'Data region successfully displayed');
                Highcharts.chart('grafik2', {
                    chart: {
                        type: 'column',
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: '',
                        style: {
                            font: '25px Arial, sans-serif',
                            fontWeight: 'bold',
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        categories: data.categories,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Cost',
                            style: {
                                fontSize: '12px',
                                fontWeight: 'bold',
                                color: '#333333'
                            }
                        },
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>USD {point.y}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: data.series
                });
            }
        })
                .done(function () {
                    $("#loader2").fadeOut(1000, function () {
                        $("#grafik2div").fadeIn(1000);
                    });
                })
    }

    // Load data chart region tampilan zoom
    function load_chart_region_detail() {
        var year_from = $("#year3_detail").val();
        var year_to = $("#year4_detail").val();
        var aircraft_type = $('#g2_aircraft_type_detail').find(":selected").val();
        var engine_type = $('#g2_engine_type_detail').find(":selected").val();
        var operator = $('#g2_operator_detail').find(":selected").val();
        $.ajax({
            url: "<?= base_url(); ?>index.php/mro_dashboard/get_data_by_region",
            data: {
                'year_from': year_from,
                'year_to': year_to,
                'aircraft_type': aircraft_type,
                'engine_type_id': engine_type,
                'operator_id': operator,
                'tipe': 1
            },
            type: 'POST',
            dataType: "JSON",
            beforeSend: function () {
                $("#loader2d").fadeIn(1000);
            },
            success: function (data) {
                Highcharts.chart('grafik2d', {
                    chart: {
                        type: 'column',
                        //  height: '320',
                        //  width: '750',
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: '',
                        style: {
                            font: '25px Arial, sans-serif',
                            fontWeight: 'bold',
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        categories: data.categories,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Cost',
                            style: {
                                fontSize: '12px',
                                fontWeight: 'bold',
                                color: '#333333'
                            }
                        },
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>USD {point.y}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    legend: {
                        enabled: true
                    },
                    series: data.series
                });
            }
        })
                .done(function () {
                    $("#loader2d").fadeOut(1000, function () {
                        $("#grafik2ddiv").fadeIn(1000);
                    });
                })
    }

    // Load data chart business segment
    function load_chart_business_segments() {
        var year_from = $("#year5").val();
        var year_to = $("#year6").val();
        var engine_type = $('#g3_engine_type').find(":selected").val();
        var aircraft_type = $('#g3_aircraft_type').find(":selected").val();
        var region_id = $('#g3_region').find(":selected").val();
        var operator = $('#g3_operator').find(":selected").val();
        $.ajax({
            url: "<?= base_url(); ?>index.php/mro_dashboard/get_data_by_segment",
            data: {
                'aircraft': aircraft_type,
                'year_from': year_from,
                'year_to': year_to,
                'engine_type_id': engine_type,
                'region_id': region_id,
                'operator_id': operator,
                'tipe': 0
            },
            type: 'POST',
            dataType: "JSON",
            beforeSend: function () {
                $("#loader3").fadeIn(1000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                notif('error', "Sorry cannot load data, please check your connection");
            },
            success: function (data) {
                notif('success', 'Data business segment successfully displayed');
                Highcharts.chart('grafik3', {
                    chart: {
                        type: 'column',
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: '',
                        style: {
                            font: '25px Arial, sans-serif',
                            fontWeight: 'bold',
                        }
                    },
                    xAxis: {
                        categories: data.categories,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Cost',
                            style: {
                                fontSize: '12px',
                                fontWeight: 'bold',
                                color: '#333333'
                            }
                        },
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0"></td>' +
                                '<td style="padding:0"><b>USD {point.y}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    legend: {
                        enabled: false
                    },
                    series: [{
                            name: data.name,
                            data: data.data,
                            color: data.color
                        }]
                });
            }
        })
                .done(function () {
                    $("#loader3").fadeOut(1000, function () {
                        $("#grafik3div").fadeIn(1000);
                    });
                })
    }

    // Load data chart business segment tampilan zoom
    function load_chart_business_segments_detail() {
        var aircraft_type = $('#g3_aircraft_type_detail').find(":selected").val();
        var year_from = $("#year5_detail").val();
        var year_to = $("#year6_detail").val();
        var engine_type = $('#g3_engine_type_detail').find(":selected").val();
        var region_id = $('#g3_region_detail').find(":selected").val();
        var operator = $('#g3_operator_detail').find(":selected").val();
        $.ajax({
            url: "<?= base_url(); ?>index.php/mro_dashboard/get_data_by_segment",
            data: {
                'aircraft':aircraft_type,
                'year_from': year_from,
                'year_to': year_to,
                'engine_type_id': engine_type,
                'region_id': region_id,
                'operator_id': operator,
                'tipe': 0
            },
            type: 'POST',
            dataType: "JSON",
            beforeSend: function () {
                $("#loader3d").fadeIn(1000);
            },
            success: function (data) {
                Highcharts.chart('grafik3d', {
                    chart: {
                        type: 'column',
                        //   height: '320',
                        //    width: '750',
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: '',
                        style: {
                            font: '25px Arial, sans-serif',
                            fontWeight: 'bold',
                        }
                    },
                    xAxis: {
                        categories: data.categories,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Cost',
                            style: {
                                fontSize: '12px',
                                fontWeight: 'bold',
                                color: '#333333'
                            }
                        },
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:12px;font-weight:bold">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0"></td>' +
                                '<td style="padding:0"><b>USD {point.y}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    legend: {
                        enabled: true
                    },
                    series: [{
                            name: data.name,
                            data: data.data,
                            color: data.color
                        }]
                });
            }
        })
                .done(function () {
                    $("#loader3d").fadeOut(1000, function () {
                        $("#grafik3ddiv").fadeIn(1000);
                    });
                });
    }
</script>

<!--
END EDIT
BY DIMAS ISLAMI
-->
