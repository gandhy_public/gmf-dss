<style media="screen">
.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  display: inline-block;
  font-size: 20px;
  font-weight: bold;
  margin: 0;
  line-height: 1;
}
</style>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="button-add-place pull-left" style="margin-right:10px;">
                        <a class="btn btn-flat bg-navy" id="add" name="button">
                            Add new user  &nbsp; <i class="fa fa-plus"></i>
                        </a>
                    </div>
                    <div class="button-add-place pull-left">
                        <a class="btn btn-flat bg-navy" data-toggle="modal" data-target="#modal_approval" id="add" name="button" style="position:relative;">
                          <span class="badge bg-green" style="position: absolute;top: -10px;right: -10px;" id="count_approval"></span>
                            Approval User &nbsp; <i class="fa fa-search"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!--<form id="filter_user" action="index.html" method="post">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Filter by Role</label>
                          <select class="form-control" id="filter_role" name="filter_role">
                            <option value="">Chose..</option>
                            <?php foreach ($data_role as $data): ?>
                            <option value="<?= $data["role_id"] ?>"><?= $data["role_name"] ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label>&nbsp;</label>
                          <button class="btn btn-flat bg-navy form-control" id="search_role" type="button" name="button"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                </form>-->
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Filter by Role</label>
                        <select class="form-control" id="filter_role" name="filter_role">
                            <option value="">Choose..</option>
                            <?php foreach ($data_role as $data): ?>
                                <option value="<?= $data["role_id"] ?>"><?= $data["role_name"] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover table-gmf" id="userTable">
                        <thead class="bg-navy">
                            <tr>
                                <th style="text-align:center;">No</th>
                                <th style="text-align:center;">Username</th>
                                <th style="text-align:center;">Role</th>
                                <th style="text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal_approval" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Approval User</h4>
      </div>
      <div class="modal-body">
        <h5 style="display:none;" id="loading_approval">Please Wait ..</h5>
        <table class="table table-hover" id="table_approval" style="display:none;">
          <thead>
            <tr>
              <th>Username</th>
              <th>Role</th>
              <th>Description</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_add" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233;color: #fff;">
                <h4 class="modal-title" id="title-edit"><span class="title_modal"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" id="id_user" >
                                <input type="text" class="form-control" id="username" >
                                <span style="display:none;" class="text-danger" id="uniqueMessage">Username is already to use</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Role</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="role">
                                    <option value="">Chose..</option>
                                    <?php foreach ($data_role as $data): ?>
                                        <option value="<?= $data["role_id"] ?>"><?= $data["role_name"] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat btn-default"  data-dismiss="modal"><i class="fa fa-rotate-left"></i> Cancel</button>
                <span class="tombol"></span>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 var table;
 $(document).ready(function(){
    user_new();
    count_approval();
    $('#filter_role').change(function() {
     user_new();
    });
 });

  $("#modal_approval").on("show.bs.modal", function(e){
    var ini = $(this);

    $.ajax({
      url: '<?= base_url() ?>index.php/user_master/show_user_approval',
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
        $("#loading_approval").fadeIn(1000);
        $("#table_approval").find("tbody tr").remove();
      }
    })
    .done(function(datas) {
      $("#table_approval").fadeIn(1000);
      $.each(datas, function(i, data){
        var content = '<tr>'+
                          '<td>'+data.username+'</td>'+
                          '<td>'+data.role_name+'</td>'+
                          '<td>'+data.user_description_register+'</td>'+
                          '<td><button class="btn btn-flat bg-navy" id="approve" data-id="'+data.user_id+'"> Approve </button></td>'+
                     '</tr>';
        $("#table_approval").find("tbody").append(content);
      })
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      $("#loading_approval").fadeOut(1000);
    });

  })

  $(document).on("click", "#approve", function(e) {
    e.preventDefault();
    var user_id = $(this).data("id");
    var ini = $(this);
    $.ajax({
      url: '<?= base_url() ?>index.php/user_master/approved',
      type: 'POST',
      dataType: 'JSON',
      data: {user_id: user_id},
      beforeSend: function(){
        ini.prop('disabled', true).html("<i class='fa fa-spin fa-refresh'></i>");
      }
    })
    .done(function(data) {
      if(data.success){
        ini.closest('tr').remove();
        user_new();
        count_approval();
      } else{
        notif("error", "failed to approve");
      }
    })
    .fail(function() {
      notif("error", "failed to approve");
    })
    .always(function() {
      ini.prop('disabled', false).html("Approve");
    });

  })

    $('#add').click(function(e) {
        $('.form-horizontal')[0].reset();
        $('#modal_add').modal("show");
        $('#username').attr('readonly', false);
        $("#password_form").show("fast");
        $('#name, #username, #email, #address, #phone, #role, #password').val('');
        $(".tombol").html('&nbsp;<button type="button" class="btn btn-flat bg-navy" id="s_add"><i class="fa fa-save"></i> Save</button>');
        $(".title_modal").html("Add User")
        $("#id_user").val("");
    });


    function count_approval()
    {
      $.ajax({
        url: '<?= base_url() ?>index.php/user_master/count_approved',
        type: 'GET',
        dataType: 'JSON',
      })
      .done(function(data) {
        $("#count_approval").text(data.total);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });

    }

    function user() {
        var tipe = $("#filter_role").val();
        $("#userTable").DataTable({
            destroy: true,
            ordering: true,
            processing: true,
            serverSide: true,
            pageLength: 25,
            language: {
                url: "<?= base_url() ?>assets/plugins/datatables/indo.lang"
            },
            ajax: {
                url: "<?= base_url() ?>index.php/user_master/datatable",
                type: 'POST',
                data: {
                    'tipe': tipe,
                },
            },
            "aoColumns": [{
                    "mData": "2",
                    "mRender": function(data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "3",
                    "mRender": function(data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "4",
                    "mRender": function(data, type, row) {
                        return "<p class='text-center'>" + data + "</p>";
                    }
                },
                {
                    "mData": "1",
                    "mRender": function(data, type, row) {
                        var button_del = '<button type="button" data-id="' + data + '" class="btn btn-danger btn-flat" id="del_user" name="button"><i class="fa fa-trash-o"></i></button>';
                        var button_edit = '<a data-id="' + data + '"  class="btn btn-primary btn-flat" id="edit_user" name="button"><i class="fa fa-wrench"></i></a>';
                        return "<center> " + button_edit + " " + button_del + " </center>";
                    }
                }
            ]
        });
    }

//    $(document).on("click", "#edit_user", function(e) {
//        var id = $(this).data("id");
//        $(".title_modal").html("Edit User")
//        $(".tombol").html('<button type="button" class="btn btn-info pull-right" id="s_edit">Simpan</button>');
//        $("#password_form").hide("fast");
//        $.ajax({
//            url: "<?= base_url() ?>index.php/user_master/view",
//            type: "POST",
//            data: {
//                'id': id,
//            },
//            dataType: "JSON",
//            success: function(data) {
//                $('#id_user').val(data.user_id);
//                $('#name').val(data.full_name);
//                $('#username').val(data.username).attr('readonly', true);
//                $('#email').val(data.email);
//                $('#address').val(data.address);
//                $('#phone').val(data.phone);
//                $('#role').val(data.role_id);
//                $('#modal_add').modal("show");
//            },
//            error: function() {
//                alert("Gagal");
//            }
//        });
//    });


    $(document).on("click", "#s_add", function(e) {
        $("#username").closest('.form-group').removeClass('has-error');
        $("#uniqueMessage").fadeOut(1000);
        var ini = $(this);
        var username = $("#username").val();
        var role = $("#role").val();
        if (username == "" || role == "") {
            alert("Form Tidak Boleh Kosong.");
        } else {
            $.ajax({
                url: "<?= base_url() ?>index.php/user_master/add",
                type: "POST",
                data: {
                    'username': username,
                    'role': role
                },
                dataType: "JSON",
                beforeSend: function() {
                    ini.attr('disabled', true).text("please wait..");
                },
                success: function(results) {
                    if (results.message == true) {
                        $('#username, #role').val('');
                        notif("success", "Berhasil Add data User");
                        user_new()
                        $('#modal_add').modal("hide");
                    } else {
                        if (results.isUnique != undefined || results.isUnique == false) {
                            $("#username").closest('.form-group').addClass('has-error');
                            $("#uniqueMessage").fadeIn(1000);
                        } else {
                            notif("error", results.errors[0]);
                        }
                    }
                    ini.attr('disabled', false).text("save");
                },
                error: function() {
                    notif("error", "Gagal Add data User");
                    ini.attr('disabled', false).text("save");
                }
            });
        }
    });


    $(document).on("click", "#s_edit", function(e) {
        var id_user = $("#id_user").val();
        var username = $("#username").val();
        var role = $("#role").val();
        if (username == "" || role == "") {
            alert("Form Tidak Boleh Kosong.");
        } else {
            $.ajax({
                url: "<?= base_url() ?>index.php/user_master/edit",
                type: "POST",
                data: {
                    'id_user': id_user,
                    'username': username,
                    'role': role,
                },
                dataType: "JSON",
                success: function(results) {
                    if (results.message == "success") {
                        $('#username, #role').val(''),
                            notif("success", "Berhasil Edit data User");
                        user_new();
                        $('#modal_add').modal("hide");
                    } else {
                        notif("error", "Gagal Edit data User");
                    }
                },
                error: function() {
                    alert("Gagal");
                }
            });
        }
    });

    $(document).on("click", "#del_user", function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        var ini = $(this);
        swal({
            title: 'Are you sure?',
            text: "Delete this data..",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn bg-navy',
            cancelButtonClass: 'btn bg-red',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            $.ajax({
                    url: '<?= base_url() ?>' + "index.php/user_master/delete_user_new",
                    type: 'POST',
                    dataType: 'json',
                    data: {
                    'id_user': id,
                    }
                })
                .done(function(data) {
                    if (data.status) {
                        notif("success", "Data Deleted");
                        ini.closest('tr').remove();
                        user_new();
                    } else {
                        notif("error", "Failed to delete");
                    }
                })
                .fail(function() {
                    notif("error", "Failed to delete");
                })
                .always(function() {
                    console.log("complete");
                });
        })
    })

//DIMAS
 function user_new(){
     var tipe =  $("#filter_role").val();
     table = $('#userTable').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "destroy": true,
            "ajax": {
                "url": "<?= base_url() ?>index.php/user_master/datatables_new",
                "type": "POST",
                "data": {
  			'tipe' : tipe,
  			},
            }
        });
 }

 $(document).on("click", "#edit_user", function(e) {
        var id = $(this).data("id");
        $(".title_modal").html("Edit User")
        $(".tombol").html('&nbsp;<button type="button" class="btn btn-flat bg-navy" id="s_edit"><i class="fa fa-save"></i> Save</button>');
        $("#password_form").hide("fast");
        $.ajax({
            url: "<?= base_url() ?>index.php/user_master/detail_view",
            type: "POST",
            data: {
                'id': id,
            },
            dataType: "JSON",
            success: function(data) {
                $('#id_user').val(data.user_id);
                $('#name').val(data.full_name);
                $('#username').val(data.username).attr('readonly', true);
                $('#email').val(data.email);
                $('#address').val(data.address);
                $('#phone').val(data.phone);
                $('#role').val(data.role_id);
                $('#modal_add').modal("show");
            },
            error: function() {
                alert("Gagal");
            }
        });
    });

</script>
