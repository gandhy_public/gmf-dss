<style>
    .inner_in {
        padding: 0px;
        border-radius: 10px;
    }

    .small-box > .inner {
        padding: 3px;
    }

    .widget-user .widget-user-header {
        padding: 1px;
        height: 60px;
        border-top-right-radius: 11px;
        border-top-left-radius: 11px;
    }

    .widget-user .widget-user-image {
        position: absolute;
        top: 5px;
        left: 48%;
        margin-left: -45px;
    }

    .box .border-right {
        border-right: 1px solid #eac8c8;
    }

    .form-group {
        margin-bottom: 10px;
    }

    .widget-user .box-footer {
        padding-top: 0px;
    }

    .box-footer {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 11px;
        border-bottom-left-radius: 11px;
        border-top: 1px solid #f4f4f4;
        padding: 0px;
        background-color: #fff;
    }

    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 5px;
        width: 100%;
        box-shadow: 0 8px 12px 4px rgba(0, 0, 0, 0.2);
    }

    .box-body {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        padding: 5px;
    }

    .title-manufacture {
        color: #F2C573;
        text-align: center;
        font-size: 25px;
        font-weight: bold;
        margin-top: 30px;
    }

    .description-block {
        display: block;
        margin: 0px 0;
        text-align: center;
    }

    .center {
        text-align: center;
    }

    .red {
        color: #ed4545;
    }

    .modal {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 45px;
        z-index: 1050;
        display: none;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
    }
    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #0f2233;
        border-color: #0e1f2e;
    }
</style>

<section class="content">

    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3 class="box-title"
                                style="text-align: center; font-size:20px; font-weight:bold"><?= $title ?></h3>
                            <p style="text-align: center"><i class="fa fa-calendar"></i> YTD : <?= date('d/M/Y') ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div id="grafik_engine" style="height:250px;width:100%;"></div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div id="grafik_apu" style="height:250px;width:100%;"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Work In Process</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
                    <div class="box box-widget widget-user" style="cursor: pointer;border-radius: 11px;"
                         title="Show Detail" onclick="modal_detail_panel('CFM')">
                        <div class="widget-user-header bg-navy">
                            <h3 class="title-manufacture">ENGINE</h3>
                        </div>
                        <div class="widget-user-image">
                            <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png" alt="User Avatar"
                                 style="border: transparent;width: 100px;">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="description-block">
                                        <h5 style="font-size: 33px;font-weight: bold;margin-top: 0px;margin-bottom: 2px;"><?= $event_engine ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 col-md-12 col-xs-12">
                    <div class="box box-widget widget-user" style="cursor: pointer;border-radius: 11px;"
                         title="Show Detail" onclick="modal_detail_panel('GTCP')">
                        <div class="widget-user-header bg-navy">
                            <h3 class="title-manufacture">APU</h3>
                        </div>
                        <div class="widget-user-image">
                            <img src="<?= base_url() ?>/assets/dist/img/icon/logo.png" alt="User Avatar"
                                 style="border: transparent;width: 100px;">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="description-block">
                                        <h5 style="font-size: 33px;font-weight: bold;margin-top: 0px;margin-bottom: 2px;"><?= $event_apu ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>

</section>


<script>

    var yAxisLimit = [0, 25, 50, 75, 100];
    var base_url = '<?php echo base_url(); ?>index.php/';

    $(document).ready(function () {
        load_chart_engine();
        load_chart_apu();
    });

    // Start chart Engine
    function load_chart_engine() {
        $.ajax({
            url: "<?= base_url(); ?>index.php/tat_engine_apu_maintenance/getDataChart",
            type: 'POST',
            dataType: "JSON",
            data: {
                type: 'CFM'
            },
            error: function (jqXHR, textStatus, errorThrown) {
                notif('error', "Sorry cannot load data, please check your connection");
            },
            success: function (data) {
                notif('success', 'Data Engine successfully displayed');
                Highcharts.chart('grafik_engine', {
                    chart: {
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: 'Engine',
                        style: {
                            color: '#000000',
                            fontSize: '20px'
                        },
                        margin: 0
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        title: {
                            text: 'Percentage (%)'
                        },
                        min: 0,
                        tickPositioner: function () {
                            return yAxisLimit;
                        },
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },
                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            dataLabels: {
                                enabled: true
                            },
                        }
                    },
                    series: [
                    {
                        name: data.series[0].name,
                        data: data.series[0].data,
                        color: data.series[0].color,
                        marker: {enabled: false},
                        dataLabels: {
                            enabled: false
                        }
                    },
                        {
                            name: data.series[1].name,
                            data: data.series[1].data,
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function () {
                                        modal_detail_grafik("CFM", this.category);
                                    }
                                }
                            },
                            color: data.series[1].color,
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354D"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                        }],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
    }


    // Start chart APU
    function load_chart_apu() {
        $.ajax({
            url: "<?= base_url(); ?>index.php/tat_engine_apu_maintenance/getDataChart",
            type: 'POST',
            dataType: "JSON",
            data: {
                type: 'GTCP'
            },
            error: function (jqXHR, textStatus, errorThrown) {
                notif('error', "Sorry cannot load data, please check your connection");
            },
            success: function (data) {
                notif('success', 'Data APU successfully displayed');
                Highcharts.chart('grafik_apu', {
                    chart: {
                        type: 'line'
                    },
                    exporting: {
                        enabled: false
                    },
                    title: {
                        text: 'APU',
                        style: {
                            color: '#000000',
                            fontSize: '20px'
                        },
                        margin: 0
                    },
                    credits: {
                        enabled: false
                    },
                    yAxis: {
                        title: {
                            text: 'Percentage (%)'
                        },
                        min: 0,
                        tickPositioner: function () {
                            return yAxisLimit;
                        },
                    },
                    xAxis: {
                        categories: data.categories,
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },
                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            dataLabels: {
                                enabled: true
                            },
                        }
                    },
                    series: [{
                        name: data.series[0].name,
                        data: data.series[0].data,
                        color: data.series[0].color,
                        marker: {enabled: false},
                        dataLabels: {
                            enabled: false
                        }
                    },
                        {
                            name: data.series[1].name,
                            data: data.series[1].data,
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function (e) {
                                        modal_detail_grafik("GTCP", this.category);
                                    }
                                }
                            },
                            color: data.series[1].color,
                            type: 'areaspline',
                            fillColor: {
                                linearGradient: [0, 0, 0, 200],
                                stops: [
                                    [0, "#05354D"],
                                    [1, "#fff3"]
                                ]
                            },
                            lineWidth: 1,
                        }],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            }
        });
    }

    function modal_detail_grafik(type, month) {
        $("#modal-detail-grafik").modal('show');
        if (type == 'CFM') {
            var desc = 'Engine';
        } else {
            var desc = 'APU';
        }
        $("#detail-tittle-grafik").html("<i class='fa fa-info-circle'></i> Detail Maintenance " + desc);
        $('#table-closed').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "order": [],
            "ajax": {
                "url": base_url + 'tat_engine_apu_maintenance/data_list_closed/' + type + '/' + month,
                "type": "POST"
            },
            "createdRow": function (row, data, dataIndex) {
                if (data[10]) {
                    $(row).addClass('red');
                }
            },
            "columnDefs": [
                {
                    "targets": [-1],
                    "orderable": false
                },
                {
                    "targets": [10],
                    "visible": false,
                    "searchable": false
                },
            ],
        });
    }

    function modal_detail_panel(type) {
        $("#modal-detail-panel").modal('show');
        if (type == 'CFM') {
            var desc = 'Engine';
        } else {
            var desc = 'APU';
        }
        $("#detail-tittle-panel").html("<i class='fa fa-info-circle'></i> Detail Maintenance " + desc);
        $('#table-progress').DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "order": [],
            "ajax": {
                "url": base_url + 'tat_engine_apu_maintenance/data_list_progress/' + type,
                "type": "POST"
            },
            "createdRow": function (row, data, dataIndex) {
                if (data[9]) {
                    $(row).addClass('red');
                }
            },
            "columnDefs": [
                {
                    "targets": [-1],
                    "orderable": false
                },
                {
                    "targets": [9],
                    "visible": false,
                    "searchable": false
                },
            ],
        });
    }

</script>

<div class="modal bounceIn" id="modal-detail-grafik" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="width: 1000px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-detail-title-panel" id="detail-tittle-grafik"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-gmf" id="table-closed"
                                   style="width:100%;font-size: 12px">
                                <thead class="bg-navy">
                                <tr>
                                    <th class="center">Month</th>
                                    <th class="center">ESN</th>
                                    <th class="center">Maintenance Type</th>
                                    <th class="center">Customer</th>
                                    <th class="center">Customer Type</th>
                                    <th class="center">Product Type</th>
                                    <th class="center">Type</th>
                                    <th class="center">Target TAT</th>
                                    <th class="center">Actual</th>
                                    <th class="center">Achievement</th>
                                    <th class="center">color</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-rotate-left"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal bounceIn" id="modal-detail-panel" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="width: 1000px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0f2233;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-detail-title-panel" id="detail-tittle-panel"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-gmf" id="table-progress"
                                   style="width:100%;font-size: 12px">
                                <thead class="bg-navy">
                                <tr>
                                    <th class="center">Month</th>
                                    <th class="center">ESN</th>
                                    <th class="center">Maintenance Type</th>
                                    <th class="center">Customer</th>
                                    <th class="center">Customer Type</th>
                                    <th class="center">Product Type</th>
                                    <th class="center">Type</th>
                                    <th class="center">Target TAT</th>
                                    <th class="center">Total Days</th>
                                    <th class="center">color</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-rotate-left"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>
