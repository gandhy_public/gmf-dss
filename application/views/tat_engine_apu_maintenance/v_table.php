<style>
    .hijau {
        background-color: #00E676 !important;
    }

    .putih {
        background-color: #ffff !important;
    }
</style>
<section class="content">
  <h3><?php echo $title;?></h3>
    <div class="box">
      <div class="box-body">
        <h4> Mitigation </h4>
        <table id="table" class="table table-hover table-bordered table-gmf">
          <thead>
          <th class="text-center">START_DATE</th>
          <th class="text-center">ESN</th>
          <!-- <th class="text-center">CUST</th>
          <th class="text-center">CUST_TYPE</th>
          <th class="text-center">PROD_TYPE</th> -->
          <th class="text-center">MAINT_TYPE</th>
          <th class="text-center">TARG_TAT</th>
          <th class="text-center">ACTUAL</th>
          <th class="text-center">ACREG</th>
          <th class="text-center">ACHIEVEMENT</th>
          </thead>

        <tbody class="text-center">
        </tbody>
        </table>
      </div>
    </div>
</section>

<script>

  var table;
  var date_now = "<?= date('Y-m') ?>";
  $(document).ready(function(){
      table = $('#table').DataTable({ 
          "processing": true, 
          "serverSide": true, 
          "order": [],
          "ajax": {
              "url": "<?= base_url().'index.php/Tat_engine_apu_maintenance/data_list/'?>",
              "type": "POST"
          },
          "columnDefs": [
              { 
                  "targets": [ -1 ],
                  "orderable": false
              },
          ],

        });
  });

</script>
